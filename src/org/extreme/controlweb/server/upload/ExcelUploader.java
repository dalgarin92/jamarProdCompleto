package org.extreme.controlweb.server.upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.extreme.controlweb.server.db.Queries;
import org.extreme.controlweb.server.MyObservable;

@SuppressWarnings("serial")
public class ExcelUploader extends HttpServlet {

	Queries q = null;
	HttpSession session;
	private String proceso = null;
	private String usuario = null;
	String mensajeError = null;
	String mensajeExito = null;
	HashMap<String, HashMap<String, String>> stableFunctionValues;

	protected Queries getQueries() {
		try {
			if (q == null)
				q = new Queries();
			else
				q.checkConnection();
		} catch (Exception e) {
		}
		return q;
	}

	private void initialize() {
		try {

			/*
			 * q = new Queries((String) session.getAttribute("var1"), (String)
			 * session.getAttribute("var2"), (String) session
			 * .getAttribute("var3"));
			 */

			q = getQueries();

		} catch (Exception e) {
			/*
			 * try { q = new Queries("ControlCentral", "postgres", "extreme"); }
			 * catch (Exception e1) { e1.printStackTrace(); }
			 */
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("text/html");
		out.println(request.getAttributeNames());
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		session = request.getSession(false);
		initialize();
		processRequest(request, response);
	}

	/**
	 * 
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 * 
	 * @param request
	 *            servlet request
	 * 
	 * @param response
	 *            servlet response
	 */

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		stableFunctionValues = new HashMap<String, HashMap<String, String>>();

		String template = request.getParameter("template");
		// int offset = Integer.parseInt(request.getParameter("offset"));
		// String spacer = request.getParameter("spacer");
		proceso = request.getParameter("proceso");
		usuario = request.getParameter("usuario");
		boolean ok = getNSaveFile(request, template);

		String mensaje = "";

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		if (ok) {
			mensaje = "Archivo recibido correctamente.\n" + mensajeExito;
		} else {
			if (mensajeExito != null) {
				mensaje += mensajeExito;
				mensaje += "\n";
			}
			if (mensajeError != null) {
				mensaje += mensajeError;
			}
		}
		out.print(mensaje.replaceAll("\n", "@@"));
		out.close();
	}

	@SuppressWarnings("unchecked")
	public boolean getNSaveFile(HttpServletRequest request, String template) {

		try {
			// Chequea que sea un request multipart (que se este enviando un
			// file)
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);

			// System.out.println("Is multipart=" + isMultipart);
			if (isMultipart) {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				try {
					// maxima talla que sera guardada en memoria
					factory.setSizeThreshold(4096);
					// si se excede de la anterior talla, se ira guardando
					// temporalmente, en la sgte direccion
					File f = new File("d:/");
					if (f.exists())
						factory.setRepository(f);
					else {
						f = new File(System.getProperty("java.io.tmpdir"));
						factory.setRepository(f);
						throw new Exception(
								"d:\\ does not exist. File stored at "
										+ System.getProperty("java.io.tmpdir"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				ServletFileUpload upload = new ServletFileUpload(factory);
				// maxima talla permisible (si se excede salta al catch)

				upload.setSizeMax(500000000);
				List<FileItem> fileItems = upload.parseRequest(request);
				// obtiene el file enviado
				Iterator<FileItem> i = fileItems.iterator();
				FileItem fi = i.next();

				// graba el file enviado en el servidor local
				// path y Nombre del archivo destino (en el server)
				try {
					// String path = "/";
					String fileName = fi.getName();
					String[] f = fileName.split("\\\\");
					if (f.length < 2) {
						f = fileName.split("/");
					}
					File f1 = new File(factory.getRepository().getPath(),
							f[f.length - 1]);
					fi.write(f1);
					int offset = Integer.parseInt(request
							.getParameter("offset"));
					return readXls(template, f1, offset);
				} catch (Exception e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {

			return false;
		}
		// return true;
	}

	public boolean readXls(String template, File file, int offset) {
		try {
			// String filename = file;
			WorkbookSettings ws = new WorkbookSettings();
			ws.setLocale(new Locale("en", "EN"));
			Workbook workbook = Workbook.getWorkbook(file, ws);
			Sheet s = workbook.getSheet(0);
			System.out.println("Campos: "+template);
			String[][] campos = q.getCamposTemplates(template);
			String[] tabla = q.getTablaTemplates(template);
			mensajeError = tabla[1] == null ? "" : tabla[1];
			HashMap<String, HashMap<String, String>> camp = new HashMap<String, HashMap<String, String>>();

			for (String[] strings : campos) {
				HashMap<String, String> values = new HashMap<String, String>();
				values.put("nombre", strings[1]);
				System.out.println("nombre: "+strings[1]);
				values.put("columna", strings[0]);
				System.out.println("columna: "+strings[0]);
				values.put("defecto", strings[2]);
				values.put("funcion", strings[3]);
				values.put("pk", strings[4]);
				values.put("id", strings[5]);
				values.put("tipo", strings[6]);
				values.put("buscarconv", strings[7]);
				/*
				 * String indice = null; try { indice =
				 * Integer.parseInt(strings[0]) + ""; } catch (Exception e) {
				 * indice = strings[1]; }
				 */
				camp.put(strings[1], values);

			}

			boolean res = readDataSheet(s, offset, tabla[0], camp);
			workbook.close();

			return res;
		} catch (Exception e) {
		}
		return false;
	}

	private boolean readDataSheet(Sheet s, int offset, String tabla,
			HashMap<String, HashMap<String, String>> campos) throws Exception {
		// String[][] response= new String[s.getRows()-offset][s.getColumns()];
		int exito = 0;
		String[] r = new String[s.getColumns()];

		ArrayList<Integer> probRows = new ArrayList<Integer>();
		ArrayList<Exception> probExceptions = new ArrayList<Exception>();

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		sdf1.setTimeZone(TimeZone.getTimeZone("GMT"));
		// SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");

		boolean sw;

		for (int i = offset; i < s.getRows(); i++) {

			sw = false;

			for (int j = 0; j < s.getColumns(); j++) {
				Cell c = s.getCell(j, i);
				if (c.getType() == CellType.DATE)
					r[j] = sdf1.format(((DateCell) c).getDate());
				else
					r[j] = c.getContents();

				if (r[j] != null && !r[j].equals(""))
					sw = true;
			}

			if (!sw)
				break;

			try {
				processRow(r, tabla, campos);
				exito++;
			} catch (Exception e) {
				probExceptions.add(e);
				probRows.add(i + 1);
			}
		}
		if (probRows.size() == s.getRows()) {
			mensajeError += "No se ingreso ning\u00fan registro, por favor revise el archivo.";
		} else {
			mensajeExito = "Ingresado(s) " + exito + " registro(s).";
			if (probRows.size() > 0) {

				mensajeError = probRows.size()
						+ " registro(s) con problema(s). \n" + mensajeError;
				for (int i = 0; i < probRows.size(); i++)
					mensajeError += "\nFila: " + probRows.get(i) + " - "
							+ probExceptions.get(i).getMessage();
			}
		}
		return probRows.size() == 0;
	}

	class AddresSolver implements Observer {

		@Override
		public void update(Observable o, Object arg) {
			MyAddressBag ab = (MyAddressBag) o;
			if (!ab.isLatLongSolved()) {
				if (ab.getAddress() != null && ab.getCity() != null) {
					String geo[] = new String[] { null, null };
					try {
						geo = q.getGeo(ab.getAddress(), ab.getCity());
					} catch (Exception e) {
					}
					ab.setLat(geo[0]);
					ab.setLon(geo[1]);
					ab.setLatLongSolved(true);
				}
			}
		}

	}

	class MyAddressBag extends MyObservable {
		String address = null;
		String city = null;
		boolean latLongSolved = false;
		String lat = null;
		String lon = null;

		public boolean isLatLongSolved() {
			return latLongSolved;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public void setLon(String lon) {
			this.lon = lon;
		}

		public String getLat() {
			return lat;
		}

		public String getLon() {
			return lon;
		}

		public void setAddress(String address) {
			this.address = address;
			setChanged();
		}

		public void setCity(String city) {
			this.city = city;
			setChanged();
		}

		public String getAddress() {
			return address;
		}

		public String getCity() {
			return city;
		}

		public void setLatLongSolved(boolean latLongSolved) {
			this.latLongSolved = latLongSolved;
		}
	}

	private void processRow(String[] r, String tabla,
			HashMap<String, HashMap<String, String>> campos) throws Exception {
		AddresSolver a = new AddresSolver();
		MyAddressBag o = new MyAddressBag();
		o.addObserver(a);
		String fields = null;
		String values = null;
		String where = null;
		String updateValues = null;
		Set<String> keys = campos.keySet();
		for (String string : keys) {
			String columna = campos.get(string).get("columna");
			if (fields == null) {
				fields = "(";
				values = "(";
				updateValues = "";
			} else {
				fields += ",";
				values += ",";
				updateValues += ",";
			}
			fields += campos.get(string).get("nombre");
			int i = -1;
			String valor = null;
			try {
				if (campos.get(string).get("buscarconv").equals("t")) {
					try {
						i = Integer.parseInt(columna);
						valor = campos.get(string).get("tipo").equals("N") ? ""
								: "'";
						valor += q.getValorDetalleCampo(campos.get(string).get(
								"id"), r[i - 1]); // busca si hay una tabla de
													// conversion y la aplica
						valor += campos.get(string).get("tipo").equals("N") ? ""
								: "'";
					} catch (Exception e) {
					}
				}
				if (valor == null) {
					i = -1;
					i = Integer.parseInt(columna);
					if (campos.get(string).get("funcion") != null) {
						valor = getFunction(campos.get(string).get("funcion"),
								r[i - 1], o, campos.get(string).get("tipo"));
					} else {
						valor = getFunction(r[i - 1], "", o, campos.get(string)
								.get("tipo"));
					}
				}
			} catch (Exception e) {
				if (campos.get(string).get("defecto") != null) {
					valor = getFunction(campos.get(string).get("defecto"), "",
							o, campos.get(string).get("tipo"));
				} else {
					valor = getFunction(campos.get(string).get("funcion"), "",
							o, campos.get(string).get("tipo"));
				}
			}

			if(!(valor.startsWith("'") && !valor.endsWith("'")))
				valor = valor.startsWith("'") && valor.endsWith("'") ? "'"
						+ valor.substring(1, valor.length() - 1).replaceAll("'",
								"''") + "'" : valor.replaceAll("'", "''");

			if (campos.get(string).get("nombre").equals("municipio"))
				o.setCity(valor);

			values += valor;
			if (campos.get(string).get("pk").equals("t")) {
				if (where == null) {
					where = "where ";
				} else {
					where += " and ";
				}
				where += campos.get(string).get("nombre")
						+ (campos.get(string).get("tipo").equals("TF") ? "%"
								: "=") + valor + " ";
			}
			updateValues += campos.get(string).get("nombre") + "=" + valor
					+ " ";
		}
		fields += ")";
		values += ")";

		values = values.replace("$<_latitud_>", o.getLat() == null ? "" : o
				.getLat());
		values = values.replace("$<_longitud_>", o.getLon() == null ? "" : o
				.getLon());

		updateValues = updateValues.replace("$<_longitud_>",
				o.getLon() == null ? "" : o.getLon());
		updateValues = updateValues.replace("$<_latitud_>",
				o.getLat() == null ? "" : o.getLat());

		where = where.replace("$<_latitud_>", o.getLat() == null ? "" : o
				.getLat());
		where = where.replace("$<_latitud_>", o.getLon() == null ? "" : o
				.getLon());

		o = null;
		a = null;
		// System.gc();

		if (tabla.contains(".")) {
			String[] esq = tabla.split("\\.");
			tabla = esq[0] + ".\"" + esq[1] + "\"";
		} else
			tabla = "\"" + tabla + "\"";

		String insert = "Insert into " + tabla + " " + fields + " values "
				+ values;
		String update = "Update " + tabla + " set " + updateValues + " "
				+ where;
		String select = "select * from " + tabla + " " + where;
		q.executeValidatedInsert(select, update, insert);
	}

	private String getFunction(String idFunction, String value, MyAddressBag o, String tipo)
			throws Exception {
		
		if(idFunction == null)
			return "null";
		else if(idFunction.startsWith("__")){
			HashMap<String, String> temp = stableFunctionValues.get(idFunction);
			if(temp == null){
				stableFunctionValues.put(idFunction, new HashMap<String, String>());
			} else {
				String temp2 = temp.get(value);
				if(temp2 != null)
					return temp2;
			}
			
		}
		
		if (idFunction.equals("_proceso_"))
			return "'" + proceso + "'";
		else if (idFunction.equals("_usuario_"))
			return "'" + usuario + "'";
		else if (idFunction.equals("_fecha_"))
			return "localtimestamp";
		else if (idFunction.equals("_municipio_")) {
			o.setCity(value);
			return q.getMunicipio(value);
		} else if (idFunction.equals("_jornada_")) {
			try {
				return q.getJornada(value);
			} catch (Exception e) {
				return "null";
			}
		} else if (idFunction.equals("_latitud_")
				|| idFunction.equals("_longitud_")) {
			o.setAddress(value);
			if (idFunction.equals("_latitud_")) {
				return "'$<_latitud_>'";
			} else {
				return "'$<_longitud_>'";
			}
		} else if (idFunction.equals("__jornada")) {
			String res;
			try {
				res = q.getJornada(value);
			} catch (Exception e) {
				res = "null";
			}
			stableFunctionValues.get(idFunction).put(value, res);
			return res;
		} else if (idFunction.equals("__getidvehiculo")) {
			String res;
			try {
				res = "'" + q.getIdVehiculo(value) + "'";
			} catch (Exception e) {
				res = "null";
			}
			stableFunctionValues.get(idFunction).put(value, res);
			return res;
		} else {
			if(tipo.equals("D")) //sirve tanto para date como para timestamp
				return "'" + idFunction + "'::timestamp without time zone";
			else if(tipo.equals("H")) // sirve para columnas time
				return "'" + idFunction + "'::time";
			else if(tipo.equals("N")) //numeric
				return idFunction;
			else
				return "'" + idFunction + "'";
		}
	}
}
