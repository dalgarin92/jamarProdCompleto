package org.extreme.controlweb.server.notification;

import org.extreme.controlweb.server.util.ServerUtils;

import com.extreme.sender.pojo.vo.MensajeVO;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class XSenderClient {
	private WebResource  webTarget;
	private Client client;
	private static final String BASE_URI = ServerUtils.XSENDER_ULR;

	public XSenderClient() {
		client =Client.create();
		webTarget = client.resource(BASE_URI).path("mensajes/xcontrol");
	}

	public void send(MensajeVO messageVO) throws Exception {
try{
	String json = new Gson().toJson(messageVO);


	//webTarget.path("xcontrol").accept(MediaType.APPLICATION_JSON).post(String.class, json);
	String resp=webTarget.header("Content-type","application/json").post(String.class, json);

	//String r = response.getEntity(String.class);

	System.out.println(resp);
}
catch(Exception e){
	System.out.println(e.getMessage());
}
	}		

}
