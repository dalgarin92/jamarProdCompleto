package org.extreme.controlweb.server.services;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.server.db.Queries;
import org.extreme.controlweb.server.notification.XSenderClient;
import org.extreme.controlweb.server.util.ServerUtils;

import com.extreme.sender.pojo.vo.ContactoVO;
import com.extreme.sender.pojo.vo.MensajeVO;

@SuppressWarnings("serial")
public class AdminQueriesServiceImpl extends MyRemoteServiceServlet implements
AdminQueriesService {

	//private Queries q;
	HttpSession session;

	public String setSessionVars(String[] vars) {
		session = getThreadLocalRequest().getSession(true);
		for (int i = 0; i < vars.length; i++) {
			session.setAttribute("var" + (i + 1), vars[i]);
		}
		return "true";
	}

	/*private Queries getInstance() {
		//if (q == null) {
		//	initialize();
		//}
		//return q;
		return getQueries();
	}

	private void initialize() {
		try {
			session = this.getThreadLocalRequest().getSession(false);
			q = new Queries((String) session.getAttribute("var1"),
					(String) session.getAttribute("var2"), (String) session
							.getAttribute("var3"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	@Override
	public Boolean addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario,String conductor,String placa) throws Exception {
		return new Boolean(getQueries().addTrabajo(codtrab, codttrab, numviaje,
				codcli, nomcli, fecha, dir, munip, barrio, telefono, prioridad,
				desc, fechaentrega, jornada, hora, proceso, usuario,conductor,placa, ""));
	}

	@Override
	public Boolean updateTrabajo(String codtrab, String codttrab,
			String numviaje, String codcli, String nomcli, String fecha,
			String dir, String munip, String barrio, String telefono,
			String prioridad, String desc, String fechaentrega, String jornada,
			String hora, String usuario) throws Exception {
		return new Boolean(getQueries().updateTrabajo(codtrab, codttrab,
				numviaje, codcli, nomcli, fecha, dir, munip, barrio, telefono,
				prioridad, desc, fechaentrega, jornada, hora, usuario));
	}

	@Override
	public Boolean delTrabajo(String trabajo, String tipotrabajo)
			throws Exception {
		return new Boolean(getQueries().deleteTrabajo(trabajo, tipotrabajo));

	}

	@Override
	public Boolean delVehiculo(String id) throws Exception {
		return new Boolean(getQueries().deleteVeh(id));
	}

	@Override
	public Boolean addVehiculo(String idcont, String idrec, String idveh,
			String placa, String tipo, String model, String marca,
			String codprop, String tipomodem, String imei, String min,
			String puk, String activo) throws Exception {
		return new Boolean(getQueries().addVeh(idcont, idrec, idveh, placa,
				tipo, model, marca, codprop, tipomodem, imei, min, puk, activo));
	}

	@Override
	public Boolean delPermiso(String idusuario, String idproceso)
			throws Exception {
		boolean f = getQueries().deletePermiso(idusuario, idproceso);
		return new Boolean(f);
	}

	@Override
	public Boolean addPermiso(String idusuario, String idproceso)
			throws Exception {
		return new Boolean(getQueries().addPermiso(idusuario, idproceso));
	}

	@Override
	public Boolean addUser(String id, String nombre, String pass, String cargo,
			String idperfil) throws Exception {
		return new Boolean(getQueries().addUsuario(id, nombre, pass, cargo,
				idperfil));
	}

	@Override
	public Boolean updateUser(String id, String nombre, String pass,
			String cargo, String idperfil) throws Exception {
		return new Boolean(getQueries().updateUsuario(id, nombre, pass, cargo,
				idperfil));
	}
	@Override
	public Boolean updateDriver(String id, String nombre, String cedula, String celular
			) throws Exception {
		return new Boolean(getQueries().updateDriver(id, nombre, cedula, celular));
	}

	@Override
	public Boolean delUser(String id) throws Exception {
		return new Boolean(getQueries().deleteUsuario(id));
	}

	@Override
	public Boolean deleteClient(String id) throws Exception {
		return new Boolean(getQueries().deleteCliente(id));
	}
	@Override
	public Boolean deleteDriver(String id) throws Exception {
		return new Boolean(getQueries().deleteDriver(id));
	}

	@Override
	public Boolean addClient(String id, String nombre, String direccion,
			String municipio, String telefono) throws Exception {
		return new Boolean(getQueries().addCliente(id, nombre, direccion,
				municipio, telefono));
	}
	@Override
	public Boolean addDriver(String id, String nombre, String cedula, String celular) throws Exception {
		return new Boolean(getQueries().addDriver(id, nombre, cedula, celular
				));
	}

	@Override
	public Boolean delContratista(String asString) throws Exception {
		return new Boolean(getQueries().deleteContratista(asString));
	}

	@Override
	public Boolean addContratista(String idContratista, String nombreContratista)
			throws Exception {
		return new Boolean(getQueries().addContratista(idContratista,
				nombreContratista));
	}

	@Override
	public Boolean updateContratista(String id, String nombre) throws Exception {
		return new Boolean(getQueries().updateContratista(id, nombre));
	}

	@Override
	public Boolean delMunicipio(String id) throws Exception {
		return new Boolean(getQueries().deleteMunicipio(id));
	}

	@Override
	public Boolean addMunicipio(String id, String nombre) throws Exception {
		return new Boolean(getQueries().addMunicipio(id, nombre));
	}

	@Override
	public Boolean updateMunicipio(String id, String nombre) throws Exception {
		return new Boolean(getQueries().updateMunicipio(id, nombre));
	}

	@Override
	public Boolean addMunipTipoTrabajo(String munip, String ttrab)
			throws Exception {
		return new Boolean(getQueries().addMunTTrab(munip, ttrab));
	}

	@Override
	public Boolean delMunipTipoTrabajo(String munip, String ttrab)
			throws Exception {
		return new Boolean(getQueries().deleteMunTTrab(munip, ttrab));
	}

	@Override
	public Boolean delMunipTTrabajoContratista(String munip, String ttrab,
			String cont) throws Exception {
		return new Boolean(getQueries().deleteMunTTrabCont(munip, ttrab, cont));
	}

	@Override
	public Boolean addMunipTTrabajoContratista(String munip, String ttrab,
			String cont) throws Exception {
		return new Boolean(getQueries().addMunTTrabCont(munip, ttrab, cont));
	}

	@Override
	public Boolean delTTrabajo(String id) throws Exception {
		return new Boolean(getQueries().deleteTipoTrab(id));
	}

	@Override
	public Boolean addTipoTrabajo(String id, String nombre, boolean reporta)
			throws Exception {
		return new Boolean(getQueries().addTipoTrab(id, nombre, reporta));
	}

	@Override
	public Boolean updateTipoTrabajo(String id, String nombre, boolean reporta)
			throws Exception {
		return new Boolean(getQueries().updateTipoTrab(id, nombre, reporta));
	}

	@Override
	public Boolean deleteMRechazo(String id) throws Exception {
		return getQueries().deleteMRechazo(id);
	}

	@Override
	public Boolean addMRechazo(String id, String nombre) throws Exception {
		return getQueries().addMRechazo(id, nombre);
	}

	@Override
	public Boolean updateMRechazo(String id, String nombre) throws Exception {
		return getQueries().updateMRechazo(id, nombre);
	}

	@Override
	public Boolean addTRecurso(String id, String nombre, String capacidad)
			throws Exception {
		return getQueries().addTipoRec(id, nombre, capacidad);
	}

	@Override
	public Boolean delTRecurso(String id) throws Exception {
		return getQueries().deleteTipoRec(id);
	}

	@Override
	public Boolean updateTRecurso(String id, String nombre, String capacidad)
			throws Exception {
		return getQueries().updateTipoRec(id, nombre, capacidad);
	}

	@Override
	public Boolean deleteRecurso(String id, String cont) throws Exception {
		return getQueries().deleteRecurso(id, cont);
	}

	@Override
	public Boolean addTTrabajoProceso(String idproceso, String idttrab)
			throws Exception {
		return getQueries().addProcTTrab(idproceso, idttrab);
	}

	@Override
	public Boolean delProcesoTTrabajo(String proceso, String ttrab)
			throws Exception {
		return getQueries().deleteProcTTrab(proceso, ttrab);
	}

	@Override
	public Boolean delProcesoTRecurso(String proceso, String trecurso)
			throws Exception {
		return getQueries().deleteProcTRec(proceso, trecurso);
	}

	@Override
	public Boolean addProcesoTRecurso(String proceso, String trecurso)
			throws Exception {
		return getQueries().addProcTRec(proceso, trecurso);
	}

	@Override
	public Boolean addProcesoMunicipio(String asString, String value)
			throws Exception {
		return getQueries().addProcMunip(asString, value);
	}

	@Override
	public Boolean delProcesoMunicipio(String proceso, String municipio)
			throws Exception {
		return getQueries().deleteProcMunip(proceso, municipio);
	}

	@Override
	public Boolean addProcesoContratista(String proceso, String contratista)
			throws Exception {
		return getQueries().addProcCont(proceso, contratista);
	}

	@Override
	public Boolean delProcesoContratista(String proceso, String contratista)
			throws Exception {
		return getQueries().deleteProcCont(proceso, contratista);
	}

	@Override
	public Boolean updateProceso(String proceso, String nombre, boolean turnos,
			boolean contratistas, boolean central, int bflag, int oflag,
			int rflag) throws Exception {
		return getQueries().updateProcess(proceso, nombre, turnos ? "S" : "N",
				contratistas ? "S" : "N", central ? "S" : "N", bflag, oflag,
						rflag);
	}

	@Override
	public Boolean addProceso(String proceso, String nombre) throws Exception {
		return getQueries().addProceso(proceso, nombre);
	}

	@Override
	public Boolean addOpcionesCategoria(String categoria, String idOpcion,
			String nombreOpcion) throws Exception {
		return getQueries().addOpcListaToCategoria(idOpcion, nombreOpcion,
				categoria);
	}

	@Override
	public Boolean addCategoriaLista(String nomcategoria, String tipo) throws Exception {
		return getQueries().addCategoriasListasPrecierre(nomcategoria, tipo);
	}

	@Override
	public Boolean deleteOpcionCategoria(String opcion) throws Exception {
		return getQueries().deleteOpcLista(opcion);
	}

	@Override
	public Boolean deleteItemsPrecierres(String item) throws Exception {
		return getQueries().deleteItemPrecierre(item);
	}

	@Override
	public Boolean addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, int orden) throws Exception {
		return getQueries().addItemPrecierre(tipotrabajo, nomItem,
				noRealizacion, orden);
	}

	@Override
	public Boolean addCampoPrecierreLista(String idItemPrecierre,
			String nombreCampoPrecierre, String idcat, String tipo, int orden)
					throws Exception {
		return getQueries().addCampoPrecierreLista(nombreCampoPrecierre,
				orden, idItemPrecierre, idcat, tipo);
	}

	@Override
	public Boolean deleteCampoPrecierres(String idcampoprecierre)
			throws Exception {
		return getQueries().deleteCampoPrecierre(idcampoprecierre);
	}

	@Override
	public Boolean updateCampoPrecierreLista(String idCampo, String nombre,
			int orden) throws Exception {
		return getQueries().updateCampoPrecierre(idCampo, nombre, orden);
	}

	@Override
	public Boolean updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, int orden) throws Exception {
		return getQueries().updateItemPrecierre(idItem, nombre, noRealizacion,
				orden);
	}

	@Override
	public Boolean delCategoriaPrecierre(String idCategoria) throws Exception {
		return getQueries().deleteCategoriasListasPrecierre(idCategoria);
	}

	@Override
	public Boolean updateCategoriaPrecierre(String idCategoria,
			String nomCategoria, String tipo) throws Exception {
		return getQueries().updateCategoriasListasPrecierre(idCategoria,
				nomCategoria, tipo);
	}

	@Override
	public Boolean updateOpcionesCategoria(String id, String nombre)
			throws Exception {
		return getQueries().updateOpcionesCategoria(id, nombre);
	}

	@Override
	public Boolean reorderOpcionesCategoria(String asString, String asString2) throws Exception {
		return getQueries().reporderOpcionesCategoria(asString,asString2);
	}

	@Override
	public Boolean addTemplate(String archivo, String nombre, String formato,
			String proceso) throws Exception {
		return getQueries().addTemplate(archivo,nombre, formato,proceso);
	}

	@Override
	public Boolean updateTemplate(String id, String nombre,
			String formato, String proceso) throws Exception {
		return getQueries().updateTemplate(id, nombre, formato, proceso);
	}

	@Override
	public Boolean delTemplate(String id) throws Exception {
		return getQueries().delTemplate(id);
	}

	@Override
	public Boolean addCampoTemplate(String template, String columna,
			String campo) throws Exception {
		return getQueries().addCampoTemplate(template, columna, campo);
	}

	@Override
	public Boolean delCampoTemplate(String id) throws Exception {
		return getQueries().delCampoTemplate(id);
	}

	@Override
	public Boolean updateCampoTemplate(String id, String columna)
			throws Exception {
		return getQueries().updateCampoTemplate(id, columna);
	}

	@Override
	public Boolean addValorCampoTemplate(String iddetalle, String invalue,
			String outvalue, String label) throws Exception {
		return getQueries().addValorCampoTemplate(iddetalle,invalue,outvalue,label);
	}

	@Override
	public Boolean delValorCampoTemplate(String id) throws Exception {
		return getQueries().delValorCampoTemplate(id);
	}
	@Override
	public Boolean addAlarma(String idvehiculo, String idevento, String alarma, String gpio)
			throws Exception {
		return getInstance().addAlarma(idvehiculo,idevento,alarma,gpio);
	}


	private Queries getInstance() {
		return getQueries();
	}
	@Override
	public Boolean addVehiculo(String idvehiculo, String placa, String recurso,
			String contratista) throws Exception {

		return getInstance().addVehiculo(idvehiculo, placa, recurso, contratista);

	}

	@Override
	public Boolean addContactoVehiculo(String idvehiculo, String idContacto)
			throws Exception {
		return getInstance().addContactoVehiculo(idvehiculo, idContacto);
	}

	@Override
	public Boolean delContactoVehiculo(String idvehiculo, String idcontacto)
			throws Exception {
		return getInstance().delContactoVehiculo(idvehiculo, idcontacto);
	}

	@Override
	public Boolean updateInsurance(String idvehiculo,String placa, String tipo, String marca,
			String ciudad, String colorCarroceria, String modelo,
			String tipoCarroceria, String colorCabina, boolean asegurado,
			String aseguradora, String tel_emergencia, String fechaPoliza,
			String vigenciaPoliza, String poliza,boolean estado,
			String tipo_modem, String min, String puk, String imei,String propietario,
			String modemUser, String modemPasswd, String geocerca) throws Exception {
		return getInstance().updateInsurance(idvehiculo,placa, tipo, marca, ciudad,
				colorCarroceria, modelo, tipoCarroceria, colorCabina,
				asegurado, aseguradora, tel_emergencia, fechaPoliza,
				vigenciaPoliza, poliza, estado, tipo_modem, min, puk, imei,propietario, modemUser, modemPasswd, geocerca);
	}

	@Override
	public Boolean delAlarma(String idvehiculo, String idevento)
			throws Exception {
		return getInstance().deleteAlarma(idvehiculo,idevento);
	}

	@Override
	public Boolean addContacto(String idproceso, String nombre, String fijo,String fijo2,
			String celular, String celular2, String email, String parent) throws Exception {
		return getInstance().addContactoProceso(idproceso,nombre,fijo,fijo2,celular,celular2,email,parent);
	}

	@Override
	public Boolean updateContacto(String idContacto, String nombre,
			String fijo,String fijo2, String celular, String celular2, String email, String parent)
					throws Exception {
		return getInstance().updateContacto(idContacto,nombre,fijo2,fijo,celular,celular2,email,parent);
	}

	@Override
	public boolean addRegFlota(String fecha, String vehiculo,
			String muelle, String conductor,
			String destino, String horaentrada,
			String horasalida, String horaretorno, String observacion) throws Exception{
		return getQueries().addRegProgramacion(fecha, vehiculo, muelle, conductor,
				destino, horaentrada, horasalida, horaretorno, observacion);
		// TODO Auto-generated method stub

	}

	@Override
	public boolean updateRegFlota(String fecha2, String valueAsString,
			String valueAsString2, String valueAsString3,
			String valueAsString4, String valueAsString5,
			String valueAsString6, String valueAsString7, String observacion) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().updateRegProg(fecha2, valueAsString,valueAsString2, valueAsString3, valueAsString4, valueAsString5, valueAsString6, valueAsString7, observacion);
	}

	@Override
	public boolean deleteRegProg(String fecha, String placa) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Realizando eliminacion de registro.. 2");
		return getQueries().deleteRegProg(fecha, placa);
	}

	@Override
	public Boolean addVehiculo(String idvehiculo, String geocerca,
			String placa, String recurso, String contratista) throws Exception {
		// TODO Auto-generated method stub
		return new Boolean (getQueries().addVehiculo(idvehiculo, geocerca,
				placa,  recurso, contratista));
	}

	@Override
	public boolean addMuelle(String nombre, String desc) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().addMuelle(nombre,  desc);
	}

	@Override
	public boolean deleteMuelle(String asString) throws Exception {
		// TODO Auto-generated method stub

		return getQueries().deleteMuelles(asString);
	}

	@Override
	public boolean updateMuelle(String muelle, String desc) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().updateMuelle(muelle, desc);
	}

	@Override
	public boolean addDestino(String iddest, String nombre) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().addDestino(iddest, nombre);
	}

	@Override
	public boolean deleteDestino(String asString) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().deleteDestinos(asString);
	}

	@Override
	public boolean updateDestino(String iddest, String nombre) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().updateDestino(iddest, nombre);
	}

	@Override
	public boolean ActDatosGeocercas() throws Exception {
		// TODO Auto-generated method stub
		//idgeocerca, idingreso, idegreso, descripcion, idvehiculo, numgeocerca, latitud, longitud, radio, estado
		ArrayList<RowModel> result = new ArrayList<RowModel>();
		boolean resp=false;
		String[][] r=getQueries().getGeocercasCentral();
		for (String[] strings : r) {
			RowModelAdapter rma= new RowModelAdapter();
			System.out.println("GEOCERCA: "+strings[0]+", "+strings[3]);
			rma.addValue("idgeocerca", strings[0]);
			rma.addValue("idingreso", strings[1]);
			rma.addValue("idegreso", strings[2]);
			rma.addValue("descripcion", strings[3]);
			rma.addValue("idvehiculo", strings[4]);
			rma.addValue("numgeocerca", strings[5]);
			rma.addValue("latitud", strings[6]);
			rma.addValue("longitud", strings[7]);
			rma.addValue("radio", strings[8]);
			rma.addValue("estado", strings[9]);
			resp=getQueries().updateGeocercasJamar(strings[0], strings[1],strings[2],strings[3],strings[4],strings[5],strings[6],strings[7],strings[8],strings[9] );

			result.add(rma);
		}
		return resp;
	}

	@Override
	public boolean updateCaractColumnas(String campo, String h, String w)
			throws Exception {
		// TODO Auto-generated method stub
		return getQueries().caractColumnas(campo, h, w);
	}

	@Override
	public boolean updateCaractPantalla(String tipoletra, String tamletra,
			String sub, String negrita, String cursiva) throws Exception {
		// TODO Auto-generated method stub
		return getQueries().caractPantalla(tipoletra, tamletra, sub, negrita, cursiva);
	}



	final int [] camposval=new int[13];
	public String height="";
	public void obtenerColumnas(String[] campos) throws Exception{
		for(int i=0; i<campos.length; i++){
			String[][] r = getQueries().widthCampo(campos[i]);
			for (String[] strings : r) {
				camposval[i]=Integer.parseInt(strings[0]);
				height=strings[1];
			}

		}
	}

	final String [] caract=new String[5];
	public void obtenerCaracteristicas() throws Exception{
		String [][] r = getQueries().CaracteristicasPant();
		for (String[] strings : r) {
			caract[0]=strings[0];
			caract[1]=strings[1];
			caract[2]=strings[2];
			caract[3]=strings[3];
			caract[4]=strings[4];
		}


	}

	@Override
	public Boolean addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String placa) throws Exception {
		return new Boolean(getQueries().addTrabajo(codtrab, codttrab, numviaje,
				codcli, nomcli, fecha, dir, munip, barrio, telefono, prioridad,
				desc, fechaentrega, jornada, hora, proceso, usuario));
	}


	@Override
	public boolean updateResourceHour(String hourCode, String resource,String contractor) throws Exception {
		return getQueries().updateResourceHour(hourCode, resource,contractor);
	}

	@Override
	public boolean addResourceJob(ArrayList<RowModel> rm,String date, String resource,
			String contractor,String user,boolean sendMessage) throws Exception {

		boolean resp = getQueries().deleteAssigns(resource, contractor,date);

		for (RowModel row : rm) {
			Object [] record = row.getRowValues();

			if(record[17]!=null&&record[18]!=null){
				if(!resource.equals(record[17])&&!contractor.equals(record[12])) {
					getQueries().quitJob((String)record[2], (String)record[12]);
				}
			}

			if(getQueries().updateJob(
					(String)record[9],
					Integer.parseInt((String)record[11]),
					Integer.parseInt((String)record[21] == null ? "0":(String)record[21]),
					Integer.parseInt((String)record[20]== null ? "0":String.valueOf(record[20])),
					date,
					(String)record[0],
					(String)record[2],
					(String)record[12])){

				boolean sw = getQueries().AsignJob((String)record[2], (String)record[12],
						resource, contractor);

				if(sendMessage){
					if(sw){
						if(record[22]!=null&&record[23]!=null){
							ArrayList<ContactoVO> list = new ArrayList<ContactoVO>();
							ContactoVO cont = new ContactoVO(false, true, Long.parseLong((String)record[22]), "");
							list.add(cont);
							String mensaje=getQueries().getMensajeTrabajo((String)record[2],(String)record[12]);

							MensajeVO mje = new MensajeVO();
							mje.setCliente((String)record[4]);
							mje.setAsunto((String)record[23]);
							mje.setMensaje(mensaje);
							mje.setProceso((String)record[25]);
							mje.setContactos(list);
							mje.setOperador("C");
							mje.setTipo("S");
							mje.setProveedor(/*ServerUtils.PROVEEDOR*/"");

							sendMessage(mje);
						}
					}

				}
			}

		}

		return true;
	}

	private boolean sendMessage(MensajeVO messageVO)throws Exception {

		XSenderClient client = new XSenderClient();

		try {
			client.send(messageVO);
		} catch (Exception e) {
		}

		return true;
	}

	@Override
	public boolean deleteHorario(String codigo) throws Exception {

		return getQueries().deleteHorario(codigo);
	}

	@Override
	public boolean delJornadasHorarios(String horario, String idjornada)
			throws Exception {

		return getQueries().deleteHorarioJornada(horario, idjornada);
	}

	@Override
	public boolean addJornadasHorarios(String idjornada, String horario)
			throws Exception {

		return getQueries().addHorarioJornada(horario, idjornada);
	}

	@Override
	public boolean addHorario(String codigo, String nombre) throws Exception {

		return getQueries().addHorario(codigo, nombre);
	}

	@Override
	public boolean updateHorario(String codigo, String nombre) throws Exception {

		return getQueries().updateHorario(codigo, nombre);
	}

	@Override
	public boolean deleteJornada(String idjornada) throws Exception {

		return getQueries().deleteJornada(idjornada);
	}

	@Override
	public boolean addJornada(String codigo, String nombre, String horainicio,
			String horafin) throws Exception {

		return getQueries().addJornada(codigo, nombre, horainicio, horafin);
	}

	@Override
	public boolean updateJornada(String idjornada, String codigo,
			String nombre, String horainicio, String horafin) throws Exception {

		return getQueries().updateJornada(idjornada, codigo, nombre, horainicio, horafin);
	}

	@Override
	public boolean addRecurso(String id, String cont, String trec,
			String nombre, String usuario, String clave, String codigosap, String horario) throws Exception {
		return getQueries().addRecurso(id, nombre, trec, cont, usuario, clave, codigosap, horario);
	}


	@Override
	public Boolean deleteItemsPrecierres(String item, String ttrab) throws Exception {
		return getQueries().deleteItemPrecierre(item, ttrab);
	}

	@Override
	public Boolean deleteCampoPrecierres(String item,
			String idcampoprecierre)
					throws Exception {
		return getQueries().deleteCampoPrecierre(item, idcampoprecierre);
	}

	@Override
	public Boolean updateCampoPrecierreLista(String idCampo, String idItemPrecierre,
			String nombre, String codigo, int orden) throws Exception {
		getQueries().updateCampoItemP(idCampo, idItemPrecierre, orden);
		return getQueries().updateCampoPrecierre(idCampo, nombre, codigo);
	}

	@Override
	public Boolean addCampoItem(String campo, String item, int orden) throws Exception	{
		return getQueries().addCampoItemP(campo, item, orden);
	}

	@Override
	public Boolean addCampoPrecierreLista(String idItemPrecierre, String codigo,
			String nombreCampoPrecierre, String idcat, String tipo, int orden)
					throws Exception {
		String campo = getQueries().addCampoPrecierreLista(nombreCampoPrecierre,
				codigo, idcat, tipo);
		return getQueries().addCampoItemP(campo, idItemPrecierre, orden);
	}

	@Override
	public Boolean addTTrabajoItem(String tipotrabajo, String item, int orden) throws Exception	{
		return getQueries().addTTrabajoItemP(tipotrabajo, item, orden);
	}

	@Override
	public Boolean addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, String repeticion, int orden) throws Exception {
		String item = getQueries().addItemPrecierre(nomItem,
				noRealizacion, repeticion);
		return getQueries().addTTrabajoItemP(tipotrabajo, item, orden);
	}

	@Override
	public Boolean updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, String repeticion, int orden, String ttrab) throws Exception {
		getQueries().updateTTrabajoItem(idItem, ttrab, orden);
		return getQueries().updateItemPrecierre(idItem, nombre, noRealizacion, repeticion);
	}

	@Override
	public Boolean updateItemPrecierreOrden(String idItem, int orden, String ttrab) throws Exception {
		return getQueries().updateTTrabajoItem(idItem, ttrab, orden);
	}

	@Override
	public boolean  updateMessage(String subject, String from,String to,
			String dateDelivery,String jobDetails, String  timeTolerance ,int idMesaage) throws Exception {
		return getQueries().updateMessage(subject, from, to, dateDelivery, jobDetails, timeTolerance, idMesaage);
	}

	@Override
	public String addMessage(String subject, String from,String to,
			String dateDelivery, String jobDetails, String timeTolerance ) throws Exception {
		return getQueries().addMessage(subject, from, to, dateDelivery, jobDetails, timeTolerance);
	}

	@Override
	public boolean  updateMessageJobsType(String jobType, int idMesaage) throws Exception {
		return getQueries().updateMessageJobType(jobType, idMesaage);
	}

	@Override
	public Boolean addTipoTrabajo(String id, String nombre, boolean reporta, String maxduracion, String tiempopromedio)
			throws Exception {
		return new Boolean(getQueries().addTipoTrab(id, nombre, reporta, maxduracion, tiempopromedio));
	}

	@Override
	public Boolean updateTipoTrabajo(String id, String nombre, boolean reporta, String maxduracion, String tiempopromedio)
			throws Exception {
		return new Boolean(getQueries().updateTipoTrab(id, nombre, reporta, maxduracion, tiempopromedio));
	}

	@Override
	public boolean updateRecurso(String id, String cont, String trec,
			String nombre, String usuario, String clave, String codigosap,
			String horario) throws Exception {
		// TODO Auto-generated method stub
		return new Boolean(getQueries().updateRecurso(id, nombre, trec,
				cont, usuario,clave,  codigosap,
				horario));
	}
	
	@Override
	public boolean closeWorkAndAddJamarResult(String trabajo, String tipotrabajo,String calificacion,String causal,String fuente,String usuario) throws Exception {

		if(getQueries().closeWork(trabajo, tipotrabajo)){
			
			getQueries().addNovedad(trabajo, tipotrabajo, usuario, "PRECERRADO DESDE LA WEB", null, null);
			
			return getQueries().addJamarResult(trabajo, tipotrabajo, calificacion, causal, fuente);
			
		}
		
		return false;
	}


}