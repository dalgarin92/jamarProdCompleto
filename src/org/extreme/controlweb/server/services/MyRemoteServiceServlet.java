package org.extreme.controlweb.server.services;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.extreme.controlweb.server.db.Queries;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class MyRemoteServiceServlet extends RemoteServiceServlet {

	private Queries q;
	private String message;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			q = new Queries();
		} catch (Exception e) {
			message = e.getMessage();
		} 
	}
	
	protected Queries getQueries() {
		try {
			if (q == null)
				q = new Queries();
			else
				q.checkConnection();
		} catch (Exception e) {
		}
		return q;
	}
	
	protected String getMessage() {
		return message;
	}
}
