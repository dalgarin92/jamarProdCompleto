package org.extreme.controlweb.server.stats;

import java.util.HashMap;

import ChartDirector.BarLayer;
import ChartDirector.Chart;
import ChartDirector.XYChart;

public class MyBarChart implements MyChart {

	private XYChart c;
	private boolean isPercent;

	public MyBarChart(int graphWidth, int graphHeight,
			String yAxisTitle, String reportTitle, boolean isPercent) {
		this.isPercent = isPercent;
		Chart.setLicenseCode("HF8NDG3A5K5V9JYJA56D6F77");
        c = new XYChart(graphWidth, graphHeight, /*0xDFE8F6*/0xeeb8b8, 0x000000, 1);
        c.setRoundedFrame();

    	c.setPlotArea((int) (graphWidth * 0.06), (int) (graphHeight * 0.13),
				(int) (graphWidth * 0.916666), (int) (graphHeight * 0.73),
				0xffffff, -1, -1, 0xcccccc, 0xcccccc);

		/*c.addLegend((int) (graphWidth * 0.75), (int) (graphHeight * 0.1),
				false, "Arial Bold", 9).setBackground(Chart.Transparent);*/

		c.addTitle(reportTitle, "Times New Roman Bold Italic", 15, /*0x15428B*/ 0x8b1414)
				.setBackground(/*0xB8CFEE*/0xeeb8b8, /*0xABC7EC*/0xecabab, Chart.glassEffect());

        c.yAxis().setTitle(yAxisTitle);
        c.yAxis().setLabelFormat("{value|2~.}" + (isPercent ? "%" : ""));
	}
	
	@Override
	public byte[] getChart(HashMap<String, Object> params) {
		String[] xLabels = (String[]) params.get("labels");
		double[] values = (double[]) params.get("values");
		c.xAxis().setLabels(xLabels);

        BarLayer layer = c.addBarLayer3(values);
        layer.set3D(10);
        layer.setBarShape(Chart.CircleShape);
        
        c.setBorder(Chart.Transparent);
        c.xAxis().setLabelStyle().setFontAngle(270);
		return c.makeChart2(Chart.PNG);
	}

	@Override
	public String getImageMap() {
		return "<map name = 'mymap' id = 'mymap'>"
				+ c.getHTMLImageMap("#", " ", "title='{xLabel}: {value|2~.}"
						+ (isPercent ? "%'" : "'")) + "</map>";
	}
}
