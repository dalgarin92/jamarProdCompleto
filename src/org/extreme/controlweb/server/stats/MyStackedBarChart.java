package org.extreme.controlweb.server.stats;

import java.util.HashMap;

import ChartDirector.BarLayer;
import ChartDirector.Chart;
import ChartDirector.XYChart;

public class MyStackedBarChart implements MyChart {

	private XYChart c;

	public MyStackedBarChart(int graphWidth, int graphHeight,
			String yAxisTitle, String reportTitle) {
		Chart.setLicenseCode("HF8NDG3A5K5V9JYJA56D6F77");
        c = new XYChart(graphWidth, graphHeight, 0xDFE8F6, 0x000000, 1);
        c.setRoundedFrame();

    	c.setPlotArea((int) (graphWidth * 0.06), (int) (graphHeight * 0.13),
				(int) (graphWidth * 0.666666), (int) (graphHeight * 0.73),
				0xffffff, -1, -1, 0xcccccc, 0xcccccc);

    	c.addLegend((int) (graphWidth * 0.75), (int) (graphHeight * 0.1),
				false, "Arial Bold", 9).setBackground(Chart.Transparent);

		c.addTitle(reportTitle, "Times New Roman Bold Italic", 15, /*0x15428B*/0x8b1414)
				.setBackground(/*0xB8CFEE*/0xeeb8b8, /*0xABC7EC*/0xecabab, Chart.glassEffect());

        c.yAxis().setTitle(yAxisTitle); 
	}
	
	@Override
	public byte[] getChart(HashMap<String, Object> params) {
		String[] xLabels = (String[]) params.get("xLabels");
		String[] columnNames = (String[]) params.get("columnNames");
		double[][] valuesMatrix = (double[][]) params.get("valuesMatrix");
		c.xAxis().setLabels(xLabels);

        BarLayer layer = c.addBarLayer2(Chart.Stack, 12);
        
        for (int i = 0; i < columnNames.length; i++)
			layer.addDataSet(valuesMatrix[i], -1, columnNames[i]);

        layer.setAggregateLabelStyle();
        
        if(valuesMatrix.length > 1)
        	layer.setDataLabelStyle();
        
        c.setBorder(Chart.Transparent);
        c.xAxis().setLabelStyle().setFontAngle(270);
		return c.makeChart2(Chart.PNG);
	}

	@Override
	public String getImageMap() {
		return "<map name = 'mymap' id = 'mymap'>"
				+ c.getHTMLImageMap("#", " ",
						"title='{xLabel}: {dataSetName} -- Valor: {value}'")
				+ "</map>";
	}

}
