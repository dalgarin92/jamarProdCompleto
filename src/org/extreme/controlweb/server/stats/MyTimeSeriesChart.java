package org.extreme.controlweb.server.stats;

import java.util.Date;
import java.util.HashMap;

import ChartDirector.Chart;
import ChartDirector.LineLayer;
import ChartDirector.XYChart;

public class MyTimeSeriesChart implements MyChart{

	private XYChart c;

	public MyTimeSeriesChart(int graphWidth, int graphHeight,
			String yAxisTitle, String reportTitle) {
		Chart.setLicenseCode("HF8NDG3A5K5V9JYJA56D6F77");

		c = new XYChart(graphWidth, graphHeight, /*0xDFE8F6*/0xf6dfdf, 0x000000, 1);
		c.setRoundedFrame();

		c.setMonthNames(new String[] { "Ene", "Feb", "Mar", "Abr", "May",
				"Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" });

		c.setPlotArea((int) (graphWidth * 0.06), (int) (graphHeight * 0.13),
				(int) (graphWidth * 0.666666), (int) (graphHeight * 0.73),
				0xffffff, -1, -1, 0xcccccc, 0xcccccc);

		c.addLegend((int) (graphWidth * 0.73), (int) (graphHeight * 0.1),
				false, "Arial Bold", 9).setBackground(Chart.Transparent);

		c.addTitle(reportTitle, "Times New Roman Bold Italic", 15, /*0x15428B*/0x8b1414)
				.setBackground(/*0xB8CFEE*/0xeeb8b8, /*0xABC7EC*/0xecabab, Chart.glassEffect());

		c.yAxis().setTitle(yAxisTitle);
		c.xAxis().setDateScale();
		c.xAxis().setDateScale3("{value|MMM d}");
		c.xAxis().setLabelStyle().setFontAngle(270);
		// c.xAxis().setTitle("Fecha");
	}

	@Override
	public byte[] getChart(HashMap<String, Object> params) {
		Date[] dates = (Date[]) params.get("dates");
		String[] columnNames = (String[]) params.get("columnNames");
		double[][] valuesMatrix = (double[][]) params.get("valuesMatrix");
		
		c.xAxis().setLabels2(dates);
		LineLayer layer = c.addLineLayer2();
		layer.setGapColor(Chart.Transparent);
		layer.setLineWidth(2);

		for (int i = 0; i < columnNames.length; i++)
			layer.addDataSet(valuesMatrix[i], -1, columnNames[i])
					.setDataSymbol(Chart.CircleSymbol, 7);
		c.setBorder(Chart.Transparent);
		return c.makeChart2(Chart.PNG);

	}

	@Override
	public String getImageMap() {
		return "<map name = 'mymap' id = 'mymap'>"
				+ c.getHTMLImageMap("#", " ",
						"title='{xLabel|MMM-dd-yyyy} -- Valor: {value}'")
				+ "</map>";
	}
}
