package org.extreme.controlweb.server.stats;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*package org.extreme.controlweb.server.stats;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
public class SessionChartFacade extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");

		String tipo = request.getParameter("tipo");
		HttpSession session = request.getSession(false);
		OutputStream out = response.getOutputStream();

		if(tipo != null && tipo.equals("pdf")) {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\"Reporte.pdf\"");
		}else if(tipo != null && tipo.equals("xls")){
			response.setHeader("Content-Disposition", "attachment; filename=\"Reporte.xls\"");
			response.setContentType("application/vnd.ms-excel");
		} else {
			response.setContentType("image/png");
		}

		if (session != null) {
			//Object chart = session.getAttribute(tipo != null && tipo.equals("pdf") ? "report": "chart");
			Object chart = session.getAttribute(tipo != null
					&& (tipo.equals("pdf") || tipo.equals("txt")|| tipo.equals("xls")) ? "report"
							: "chart");

			if(chart != null) {
				out.write((byte[])chart);
				//session.setAttribute("chart", null);
			}
		}
		out.close();
	}
}*/

@SuppressWarnings("serial")
public class SessionChartFacade extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		
		String tipo = (String) request.getParameter("tipo");
		HttpSession session = request.getSession(false);
		OutputStream out = response.getOutputStream();
		
		if(tipo != null && tipo.equals("pdf"))
			response.setContentType("application/pdf");
		else  if(tipo != null && tipo.equals("xls")){
			response.setHeader("Content-Disposition", "attachment; filename=\"Reporte.xls\"");
			response.setContentType("application/vnd.ms-excel");
		} else {
			response.setContentType("image/png");
		}

		if (session != null) {
			//Object chart = session.getAttribute(tipo != null && tipo.equals("pdf") ? "report": "chart");
			Object chart = session.getAttribute(tipo != null
					&& (tipo.equals("pdf") || tipo.equals("txt")|| tipo.equals("xls")) ? "report"
							: "chart");

			if(chart != null) {
				out.write((byte[])chart);
				//session.setAttribute("chart", null);
			}
		}
		out.close();
	}
}
