package org.extreme.controlweb.server.stats;

import java.util.HashMap;

import ChartDirector.Chart;
import ChartDirector.PieChart;
import ChartDirector.TextBox;

public class My3DPieChart implements MyChart {

	private PieChart c;

	public My3DPieChart(int graphWidth, int graphHeight, String reportTitle) {
		Chart.setLicenseCode("HF8NDG3A5K5V9JYJA56D6F77");
		int minor = graphWidth < graphHeight ? graphWidth : graphHeight;

		c = new PieChart(graphWidth, graphHeight, /*0xDFE8F6*/0xEEB8B8, 0x000000, 1);
		c.setRoundedFrame();
		
		c.addTitle(reportTitle, "Times New Roman Bold Italic", 15, /*0x15428B*/0x8b1414)
				.setBackground(/*0xB8CFEE*/0xEEB8B8, /*0xABC7EC*/0xECABAB, Chart.glassEffect());
		

		c.setPieSize((int) (graphWidth * 0.5), (int) (graphHeight * 0.5),
				(int) (minor * 0.407));
		c.set3D(20);
		c.setLabelLayout(Chart.SideLayout);

		TextBox t = c.setLabelStyle();
		t.setBackground(Chart.SameAsMainColor, Chart.Transparent, Chart
				.glassEffect());
		
		
		t.setRoundedCorners(5);

		c.setLineColor(Chart.SameAsMainColor, 0x000000);
		c.setStartAngle(135);
	}

	@Override
	public byte[] getChart(HashMap<String, Object> params) {
		String[] labels = (String[]) params.get("labels");
		double[] values = (double[]) params.get("values");

		c.setData(values, labels);
		c.setBorder(Chart.Transparent);
		c.setExplode(0);

		return c.makeChart2(Chart.PNG);
	}

	@Override
	public String getImageMap() {
		return "<map name = 'mymap' id = 'mymap'>"
				+ c.getHTMLImageMap("#", " ",
						"title='{label}: {value} ({percent}%)'") + "</map>";
	}

}
