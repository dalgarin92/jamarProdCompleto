package org.extreme.controlweb.server.stats;

import java.util.HashMap;

public interface MyChart {
	public byte[] getChart(HashMap<String, Object> params);
	public String getImageMap();
}
