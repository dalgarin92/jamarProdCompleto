package org.extreme.controlweb.server;

import java.util.Observable;

public class MyObservable extends Observable{
	@Override
	public synchronized void setChanged() {		
		super.setChanged();
		super.notifyObservers();
	}

}
