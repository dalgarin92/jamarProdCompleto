package org.extreme.controlweb.server.exporter;

import java.io.ByteArrayOutputStream;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.extreme.controlweb.server.db.DBConnection;

public class ExportUtils {

	public static byte[] exportToPdf(ServletContext application,
			Map<String, Object> parameters, DBConnection db,
			String url_reports, String url_reportName) throws Exception {
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(application
					.getRealPath(url_reports + url_reportName), parameters, db
					.getConnection());
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	public static byte[] exportToExcel(ServletContext application,
			Map<String, Object> parameters, DBConnection db,
			String url_reports, String url_reportName) throws Exception {
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(application
					.getRealPath(url_reports + url_reportName), parameters, db
					.getConnection());

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			JRXlsExporter exporter = new JRXlsExporter();

			exporter.setParameter(JRXlsAbstractExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport();

			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

}