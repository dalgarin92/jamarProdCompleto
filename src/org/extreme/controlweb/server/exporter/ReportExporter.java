package org.extreme.controlweb.server.exporter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.server.db.Queries;
import org.extreme.controlweb.server.services.QueriesServiceImpl;

@SuppressWarnings("serial")
public class ReportExporter extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		//PrintWriter out = response.getWriter();
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Content-Disposition", "attachment; filename=\"Reporte.xls\"");
		
		response.setContentType("application/vnd.ms-excel");
		String tipo = request.getParameter("tipo");
		String proceso = request.getParameter("proceso");
		String ttrab = request.getParameter("ttrab");
		String desde = request.getParameter("desde");
		String hasta = request.getParameter("hasta");
		String recurso = request.getParameter("recurso");
		String contratista = request.getParameter("contratista");
		
		String mcb = request.getParameter("mcb");
		boolean criterio = Boolean.parseBoolean(request.getParameter("criterio"));

		if (session != null) {
			Queries q = null;
			try {
				/*q = new Queries((String) session.getAttribute("var1"),
						(String) session.getAttribute("var2"), 
						(String) session.getAttribute("var3"));*/
				q = new Queries();
				
				if(tipo.equals("repprecierre")) {
					//	generateReportPrecierres(proceso, desde, hasta, q, response);
					generateReportPrecierres(proceso, ttrab, desde, hasta, recurso, contratista, mcb, criterio, q, response);
				} if(tipo.equals("repeventos")) {
					String eventos = request.getParameter("eventos");
					if(eventos == null) 
						eventos = "";
					ArrayList<String> eventosAConsultar=new ArrayList<String>();
					if(!eventos.equals("*")){
						int i=1;
						String evento =null;
						do{
							evento = request.getParameter("evento"+i);
							if(evento!=null){
								eventosAConsultar.add(evento);
								i++;
							}
						}while(evento!=null);
					}
					
					generateEventsReport(recurso, contratista, desde, hasta,
							eventosAConsultar, q, response);
				}
			}catch(Exception e) {
				e.printStackTrace();
			} finally {
				if(q != null) q.closeConnection();
			}
		} else {
			//out.println("NO SESSION");
		}
	}

	private void generateEventsReport(String recurso, String contratista,
			String desde, String hasta, ArrayList<String> eventos,
			Queries q, HttpServletResponse response) throws Exception {
		String[][] info = q.getInfoTrackRecurso(recurso, contratista, desde, hasta, eventos);
		String[] columnHeaders = new String[] { "Latitud", "Longitud", "Fecha",
				"Velocidad", "Direcci\u00f3n", "Municipio", "Departamento",
				"Evento", "Placa" };
		
		XLSExporter exp = new XLSExporter(response.getOutputStream());
		exp.addSheet(columnHeaders.length, "Reporte de Eventos",
				"Veh\u00edculo - " + info[0][columnHeaders.length - 1], 1);
		exp.appendToSheet(info, columnHeaders, null);
		exp.writeAndClose();	
		
	}
	
	private void generateReportPrecierres(String proceso, String ttrab, String desde,
			String hasta, String recurso, String contratista, String mcb, boolean criterio,
			Queries q, HttpServletResponse response) throws Exception {
		
		
		String[] columnHeaders = new String[] { "Trabajo", "Codigo Cliente", "Nombre Cliente",
				"Direcci\u00f3n", "Municipio", "Fecha programacion", "Fecha descarga",
				"Fecha Inicio", "Fecha Precierre", "Tiempo Ejecucion" };
		
		ArrayList<RowModel> info;
		if (recurso != null) {
			info = new QueriesServiceImpl().getInfoTrabajosPrecerrados(proceso, desde, hasta, recurso, contratista, ttrab);
		} else {
			info = new QueriesServiceImpl().getInfoTrabajosPrecerrados(proceso, desde, hasta, ttrab, mcb, criterio);
		}
		
		 
		
		/*String trabs[][][] = q.getPrecierresExport(proceso, ttrab, desde, hasta);
		String[][] table = trabs[0];
		String[][] cats = trabs[1];
		String[] headers = new String[cats.length + 19];

		headers[0] = "Nro trabajo (OP)";
		headers[1] = "Tipo Trabajo";
		headers[2] = "Viaje";
		headers[3] = "Fecha";
		headers[4] = "Descripci\u00d3n";
		headers[5] = "ID Cliente";
		headers[6] = "Nombre Cliente";
		headers[7] = "Direccion";
		headers[8] = "Fecha programada";
		headers[9] = "Jornada programada";
		headers[10] = "Hora programada";
		headers[11] = "ID Conductor";
		headers[12] = "Nombre Conductor";
		headers[13] = "Fecha de inicio";
		headers[14] = "Fecha de precierre";
		headers[15] = "Validacion GPS";
		headers[16] = "Fecha GPS";
		headers[17] = "Distancia GPS";
	    headers[18] = "Atraso";
		for (int i = 0; i < cats.length; i++) 
			headers[i + 19] = cats[i][0];
		
		
		String[][] tt = q.getTiposTrabajoDeProceso(proceso);
		HashMap<String, String> tt2 = new HashMap<String, String>();
		
		for (String[] strings : tt) 
			tt2.put(strings[0], strings[1]);
		
		XLSExporter exp = new XLSExporter(response.getOutputStream());
		exp.addSheet(headers.length, ("Reporte de Precierres de "
				+ tt2.get(ttrab) + " entre " + desde.replaceAll("/", "-")
				+ " y " + hasta.replaceAll("/", "-")).toUpperCase(),
				("Precierres " + tt2.get(ttrab)).toUpperCase(), 1);
		exp.appendToSheet(table, headers, null);
		exp.writeAndClose();*/
		
		String[] columnHeaders2 = info.get(0).getColumnNames();

		XLSExporter exp = new XLSExporter(response.getOutputStream());

		exp.addSheet(columnHeaders2.length, "Reporte de Trabajos","Reporte", 2);

		exp.appendToSheet(info, columnHeaders2, null);
		exp.writeAndClose();
		
		
	}
	
	/*private void generateReportPrecierres(String proceso, String desde,
			String hasta, Queries q, HttpServletResponse response) throws Exception {
		String trabs[][] = q.getInfoTrabajosPrecerradosFechas(proceso, desde,
				hasta);	
		
		XLSExporter exp = new XLSExporter(response.getOutputStream());
		for (int i = 0; i < trabs.length; i++) {
			String[][] infopre = q.getInfoTrabajosPrecerrados(trabs[i][0], trabs[i][1]);
			exp.addSheet(7, "Reporte de Precierres", trabs[i][4] +" - " + trabs[i][8], 1);
			exp.appendToSheet(infopre, new String[] { "idprecierre", "trabajo",
					"tipotrabajo", "fecha", "opcion", "valor", "item" }, null);
			exp.writeAndClose();	
		}	
	}*/
}
