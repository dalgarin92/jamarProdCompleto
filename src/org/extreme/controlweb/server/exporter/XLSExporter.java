package org.extreme.controlweb.server.exporter;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class XLSExporter {

	private WritableWorkbook wb;

	private WritableCellFormat titleFormat;
	private WritableCellFormat headerFormat;
	private WritableCellFormat normalFormat;
	private WritableCellFormat itemFormat;
	
	private FontMetrics fm;
	private ArrayList<WritableSheet> sheets;
	private int[] maxWidth;
	private int currentRowCount;

	private int columnDataOffset;

	public XLSExporter(File file) {
		try {
			wb = Workbook.createWorkbook(file);
			initializeComponents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public XLSExporter(OutputStream stream) {
		try {
			wb = Workbook.createWorkbook(stream);
			initializeComponents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initializeComponents() {
		titleFormat = new WritableCellFormat(new WritableFont(
				WritableFont.ARIAL, 16, WritableFont.BOLD));
		headerFormat = new WritableCellFormat(new WritableFont(
				WritableFont.ARIAL, 12, WritableFont.BOLD));
		itemFormat = new WritableCellFormat(new WritableFont(
				WritableFont.ARIAL, 12, WritableFont.BOLD));
		normalFormat = new WritableCellFormat(new WritableFont(
				WritableFont.ARIAL, 12));
		// normalFormat.setWrap(true);
		try {
			titleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			titleFormat.setAlignment(Alignment.CENTRE);
			
			headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			headerFormat.setAlignment(Alignment.CENTRE);
			
			itemFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			itemFormat.setAlignment(Alignment.CENTRE);
		} catch (Exception e) {
			e.printStackTrace();
		}

		BufferedImage bufferedImage = new BufferedImage(2, 2,
				BufferedImage.TYPE_4BYTE_ABGR_PRE);
		Graphics2D g2d = (Graphics2D) (bufferedImage.createGraphics());
		fm = g2d.getFontMetrics(new Font("Times New Roman", Font.PLAIN,
				12));
		
		sheets = new ArrayList<WritableSheet>();
		
	}

	public void addSheet(int numCols, String title, String sheetName, int columnDataOffset){
		this.columnDataOffset = columnDataOffset; 
		if(sheets.size() > 0) {
			fitColumnWidth(sheets.get(sheets.size() - 1));
		}
			
		WritableSheet sheet = wb.createSheet(sheetName, sheets.size());
		sheets.add(sheet);
		currentRowCount = 2;
		
		try {
			sheet.addCell(new Label(0, currentRowCount, title, titleFormat));
			sheet.mergeCells(0, currentRowCount, numCols - 1, currentRowCount);
			maxWidth = new int[numCols];	
			currentRowCount++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean appendToSheet(String[][] data, String[] headers, String itemName){
		WritableSheet sheet = sheets.get(sheets.size() - 1);
		try {
			
			if(itemName != null){
				sheet.addCell(new Label(0, currentRowCount += 2, itemName, itemFormat));
				sheet.mergeCells(0, currentRowCount, 5, currentRowCount);
				currentRowCount++;
			}
			
			currentRowCount += 2;
			for (int i = 0; i < headers.length; i++) {
				sheet.addCell(new Label(i + columnDataOffset, currentRowCount, 
						headers[i].toUpperCase(), headerFormat));
				maxWidth[i] = fm.stringWidth(headers[i].toUpperCase());
			}
			
			currentRowCount += 2;
			
			for (int i = 0; i < data.length; i++) {
				for (int j = 0; j < data[i].length; j++) {
					sheet.addCell(new Label(j + columnDataOffset, currentRowCount, data[i][j], normalFormat));
					if(data[i][j] != null && maxWidth[j] < fm.stringWidth(data[i][j]))
						maxWidth[j] = fm.stringWidth(data[i][j]);
				}
				currentRowCount++;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean appendToSheet(ArrayList<RowModel> data, String[] headers, String itemName){
		String[][] data2 = new String[data.size()][data.get(0).getColumnNames().length];

		for (int i = 0; i < data.size(); i++) {
			for (int j = 0; j < data.get(i).getColumnNames().length; j++) {
				Object obj = data.get(i).getRowValues()[j];
				if(obj != null) {
					data2[i][j] = obj.toString();
				} else {
					data2[i][j] = "";
				}
			}
		}

		return appendToSheet(data2, headers, itemName);
	}
	
	private void fitColumnWidth(WritableSheet sheet) {
		for (int i = 0; i < maxWidth.length; i++) {
			sheet.setColumnView(i + columnDataOffset, (int)((maxWidth[i] + 12) * 5 / 24));
			/*CellView cv = new CellView();
			cv.setAutosize(true);
			sheet.setColumnView(i, cv);*/
		}
		
	}

	public void writeAndClose() {
		try {
			if(wb.getNumberOfSheets() > 0) {
				fitColumnWidth(sheets.get(sheets.size() - 1));
			}
			wb.write();
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

/*	public static void main(String[] args) {
		Queries q;
		try {
			q = new Queries();
			String trabs[][] = q.getInfoTrabajosPrecerradosFechas("vimarco",
					"2008-08-01 00:00", "2008-09-15 00:00");

			XLSExporter exp = new XLSExporter(new File("output.xls"));
			for (int i = 0; i < trabs.length; i++) {
				String[][] infopre = q.getInfoTrabajosPrecerrados(trabs[i][0],
						trabs[i][1]);
				exp.addSheet(7, "Reporte de Precierres", trabs[i][4] + " - "
						+ trabs[i][8], 1);
				exp.appendToSheet(infopre, new String[] { "idprecierre",
						"trabajo", "tipotrabajo", "fecha", "opcion", "valor",
						"item" }, null);
				exp.writeAndClose();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}*/
	
	public int getNumberOfSheets(){
		return wb.getNumberOfSheets();
	}

}
