package org.extreme.controlweb.server.util;

public class ServerUtils {

	public static final String DB_DRIVER = "org.postgresql.Driver";
	public static String CONNECTION_STRING;
	public static String CONNECTION_STRING_GEOC;

	// public static String KAGUA_CONNECTION_STRING;
	// public static String KAGUA_DB;
	// public static String KAGUA_USER_DB;
	// public static String KAGUA_PASSWD_DB;
	public static String POSTGRES_IP;
	public static String POSTGRES_IP_GEOC;

	public static String WEB_OTA_IP;
	public static final int WEB_OTA_PORT = 9090;
	public static final String XCONTROL_DB = "ControlJamar";
	//public static final String XCONTROL_DB = "ControlJamarTest";
	//public static final String XCONTROL_DB_GEOC = "xtrackcentral";//PRODUCTIVO
	public static final String XCONTROL_DB_GEOC = "xtrackcronos";//TEST

	// public static final String GEOSERVICE_URL
	// ="http://test.extreme.com.co/xgeocodingws/ws";
	// public static final String GEOSERVICE_URL
	// ="http://localhost:8080/xgeocodingws/ws";

	public static final String GEOSERVICE_URL = "http://xcontrol.extreme.com.co:9090/xgeocodingws/ws";
	// public static final String GEOSERVICE_URL =
	// "http://localhost:8089/trunk/ws";
	public static final String XSENDER_ULR = "http://xcontrol.extreme.com.co:9090/xsenderws/ws/";
	// public static final String XSENDER_ULR =
	// "http://test.extreme.com.co:81/xsenderws/ws/";
	// public static final String GEOSERVICE_URL
	// ="http://test.extreme.com.co:81/xgeocodingws/ws";

	// public static final String ORACLE_HOST="200.91.214.90";
	// public static final String ORACLE_HOST="localhost";
	// public static final String ORACLE_HOST="192.168.0.194";
	public static final String ORACLE_HOST = "192.168.70.12"; // (productivo)
	// public static final String ORACLE_HOST="192.168.85.228";
	// public static final String ORACLE_PORT="1521"; (productivo)
	public static final String ORACLE_PORT = "1521";
	// public static final String ORACLE_USER="ofimovil";
	public static final String ORACLE_USER = "user_extreme"; // (productivo)
	public static final String ORACLE_PASSWORD = "user_extreme"; // (productivo)
	// public static final String ORACLE_USER="jamar";
	// public static final String ORACLE_PASSWORD="stop2015";
	public static final String ORACLE_DB = "PROD_COLOMBIA"; // (productivo)
	// public static final String ORACLE_DB="PREP_COLOMBIA";
	// public static final String ORACLE_PASSWORD="of3xtr3me";
	// public static final String ORACLE_DB="PRUEBAS";
	public static String DB_DRIVER_ORACLE = "oracle.jdbc.OracleDriver";
	// ("jdbc:oracle:thin:@WIN01:1521:oracleBD", "user", "passw");
	public static String CONNECTION_STRING_ORACLE = "jdbc:oracle:thin:@" + ORACLE_HOST + ":" + ORACLE_PORT + ":ORCL";
	/*
	 * public static String
	 * CONNECTION_STRING_ORACLE="jdbc:oracle:thin:@"+ORACLE_HOST+":"+
	 * ORACLE_PORT+":XE";
	 */

	// jdbc:oracle:thin:@localhost:1521:XE

	public static String DB_USER;
	public static String DB_PASSWD;

	public static String DB_USER_GEOC;
	public static String DB_PASSWD_GEOC;

	public static final String FUENTE = "<font face='Verdana, Arial, Helvetica, sans-serif' size='-4'>";
	public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	public static final String DATE_FORMAT = "yyyy/MM/dd hh12:mi:ss AM";
	public static final String XML_OPEN_TAG = "<info>";
	public static final String XML_CLOSE_TAG = "</info>";

	public static final String CRYPTO_KEY = "extreme*2008";
	public static final String CONT_ICON = "icon-cont";
	public static final String COD_EN_TURNO = "1";
	public static final String COD_RECIEN = "0";
	public static final String COD_DESCANSO = "-1";
	public static final String PROC_ICON = "icon-root";

	private static boolean initialized = false;

	public static final int HOSTED_MODE = 1;
	public static final int DEPLOYED_BQ = 2;
	public static final int DEPLOYED_BTA = 3;
	//public static final int CURRENT_ENVIRONMENT = DEPLOYED_BTA;
	 public static final int CURRENT_ENVIRONMENT = DEPLOYED_BQ;
	// public static final int CURRENT_ENVIRONMENT = HOSTED_MODE;
	public static final double MIN_DISTANCE = 20;
	public static final String REPORT_PATH = "reports/";
	public static final String REPORT_LOGO_FILENAME = "logo.jpg";
	public static final String REPORT_LOGO_FILENAME1 = "jamar.jpg";
	public static final String REPORT_LOGO_FILENAME2 = "carro.jpg";
	public static final String REPORT_TRAFICO = "ReportedeTrafico.jasper";
	public static final String REPORT_PACKING = "ReportedePacking.jasper";

	public static void initialize(int env) {
		if (!initialized) {
			switch (env) {
			case HOSTED_MODE:
				// POSTGRES_IP="localdev.extreme.com.co";
				// POSTGRES_IP="190.144.145.150";
				POSTGRES_IP = "192.168.0.97";
				POSTGRES_IP_GEOC = "localdev.extreme.com.co";
				CONNECTION_STRING = "jdbc:postgresql://" + POSTGRES_IP + "/";
				// CONNECTION_STRING = "jdbc:postgresql://200.30.94.23/";
				// OLD: CONNECTION_STRING_GEOC = "jdbc:postgresql://" + POSTGRES_IP_GEOC + ":5432/";
				CONNECTION_STRING_GEOC ="jdbc:postgresql://" + "test.extreme.com.co" + ":25432/";
				DB_USER = "postgres";
				DB_USER_GEOC = "postgres";
				DB_PASSWD = "bdadmin";
				DB_PASSWD_GEOC = "3xtr3m3s3rv3r";
				WEB_OTA_IP = "localdev.extreme.com.co";
				break;
			case DEPLOYED_BQ:
				POSTGRES_IP = "192.168.0.97:5431";
				// POSTGRES_IP="dev.ex-technologies.net";
				POSTGRES_IP_GEOC = "test.extreme.com.co:25432";
				CONNECTION_STRING = "jdbc:postgresql://" + POSTGRES_IP + "/";
				CONNECTION_STRING_GEOC = "jdbc:postgresql://" + POSTGRES_IP_GEOC + "/";
				DB_USER = "postgres";
				DB_USER_GEOC = "postgres";
				DB_PASSWD = "admin";
				DB_PASSWD_GEOC = "3xtr3m3s3rv3r";
				WEB_OTA_IP = "192.168.0.20";
				break;
			case DEPLOYED_BTA:
				POSTGRES_IP = "192.168.70.15";
				POSTGRES_IP_GEOC = "190.147.134.120:5433";

				CONNECTION_STRING = "jdbc:postgresql://" + POSTGRES_IP + "/";
				//OLD: CONNECTION_STRING_GEOC = "jdbc:postgresql://" + POSTGRES_IP_GEOC + "/";
				CONNECTION_STRING_GEOC ="jdbc:postgresql://" + "test.extreme.com.co" + ":25432/";//TEST
				//CONNECTION_STRING_GEOC ="jdbc:postgresql://" + "209.160.28.139" + ":5432/";//PRODUCTIVO
				/*
				 * KAGUA_CONNECTION_STRING = "jdbc:postgresql://192.168.0.10/";
				 * KAGUA_DB = "slecturas"; KAGUA_USER_DB = "xcontrol";
				 * KAGUA_PASSWD_DB = "xcontrol";
				 */
				DB_USER = "postgres";
				DB_USER_GEOC = "jamar";//TEST
				DB_PASSWD = "bdadmin";
				DB_PASSWD_GEOC = "J4m4rXtrack#";//TEST
				WEB_OTA_IP = "200.75.38.10";
				break;
			}
		}
		initialized = true;
	}

	public static String encodeSymbols(String cad) {
		if (cad != null) {
			return cad.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;")
					.replaceAll("'", "&apos;").replaceAll("\"", "&quot;").replaceAll("\u00b0", "&#xB0;")
					.replaceAll("\u00e1", "&#xE1;") // aacute;
					.replaceAll("\u00e9", "&#xE9;") // eacute;
					.replaceAll("\u00ed", "&#xED;") // iacute;
					.replaceAll("\u00f3", "&#xF3;") // oacute;
					.replaceAll("\u00fa", "&#xFA;") // uacute;
					.replaceAll("\u00c1", "&#xC1;") // Aacute;
					.replaceAll("\u00c9", "&#xC9;") // Eacute;
					.replaceAll("\u00cd", "&#xCD;") // Iacute;
					.replaceAll("\u00d3", "&#xD3;") // Oacute;
					.replaceAll("\u00da", "&#xDA;") // Uacute;
					.replaceAll("\u00f1", "&#xF1;") // �acute;
					.replaceAll("\u00d1", "&#xD1;"); // �acute;
		} else {
			return cad;
		}
	}

	public static String decodeSymbols(String cad) {
		if (cad != null) {
			return cad.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&amp;", "&")
					.replaceAll("&apos;", "'").replaceAll("&quot;", "\"").replaceAll("&#xB0;", "\u00b0")
					.replaceAll("&#xE1;", "\u00e1") // aacute;
					.replaceAll("&#xE9;", "\u00e9") // eacute;
					.replaceAll("&#xED;", "\u00ed") // iacute;
					.replaceAll("&#xF3;", "\u00f3") // oacute;
					.replaceAll("&#xFA;", "\u00fa") // uacute;
					.replaceAll("&#xC1;", "\u00c1") // Aacute;
					.replaceAll("&#xC9;", "\u00c9") // Eacute;
					.replaceAll("&#xCD;", "\u00cd") // Iacute;
					.replaceAll("&#xD3;", "\u00d3") // Oacute;
					.replaceAll("&#xDA;", "\u00da") // Uacute;
					.replaceAll("&#xF1;", "\u00f1") // �acute;
					.replaceAll("&#xD1;", "\u00d1"); // �acute;
		} else {
			return cad;
		}
	}

	public static String nvl(String str) {
		if (str == null || str.equals("null") || str.trim().equals("")) {
			return "";
		} else {
			return str;
		}
	}

	public static double distance(double lat1, double lon1, double lat2, double lon2) {
		double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563; // WGS-84
																		// ellipsoid
		double L = toRad(lon2 - lon1);
		double U1 = Math.atan((1 - f) * Math.tan(toRad(lat1)));
		double U2 = Math.atan((1 - f) * Math.tan(toRad(lat2)));
		double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

		double lambda = L, lambdaP = 2 * Math.PI;
		double iterLimit = 20;
		double sinLambda, sinSigma = 0, cosLambda, cosSigma = 0, sigma = 0, sinAlpha, cosSqAlpha = 0, cos2SigmaM = 0, C;
		while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0) {
			sinLambda = Math.sin(lambda);
			cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda)
					+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda) * (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
			if (sinSigma == 0) {
				return 0; // co-incident points
			}
			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
			if (cos2SigmaM == Double.NaN) {
				cos2SigmaM = 0; // equatorial line: cosSqAlpha = 0
			}
			C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = L + (1 - C) * f * sinAlpha
					* (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
		}
		if (iterLimit == 0) {
			return Double.NaN; // formula failed to converge
		}

		double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		double deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)
				- B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		double s = b * A * (sigma - deltaSigma);

		return s;
	}

	private static double toRad(double deg) { // convert degrees to radians
		return deg * Math.PI / 180;
	}

	public static String wrap(String str, int wrapAfter, String wordsSeparator, String wrapString) {

		int lon = 0;
		StringBuffer sb = new StringBuffer();

		String s[] = str.split(wordsSeparator);
		for (int i = 0; i < s.length; i++) {
			if (lon + s[i].length() <= wrapAfter) {
				if (i > 0) {
					sb.append(" ");
					lon++;
				}
			} else if (i > 0) {
				sb.append(wrapString);
				lon = 0;
			}
			sb.append(s[i]);
			lon += s[i].length();
		}

		return sb.toString();
	}

	public static double getMax(double[] data, int fromIndex) {
		double res = Double.MIN_VALUE;
		for (int i = fromIndex; i < data.length; i++) {
			if (data[i] > res && data[i] != Double.NaN) {
				res = data[i];
			}
		}
		return res;
	}

	public static double getMin(double[] data, int fromIndex) {
		double res = Double.MAX_VALUE;
		for (int i = fromIndex; i < data.length; i++) {
			if (data[i] < res && data[i] != Double.NaN) {
				res = data[i];
			}
		}
		return res;
	}

	public static double getAvg(double[] data, int fromIndex) {
		double res = 0;
		for (int i = fromIndex; i < data.length; i++) {
			res += data[i];
		}
		res /= data.length - fromIndex;
		return res;
	}

	public static double getSum(double[] data, int fromIndex) {
		double res = 0;
		for (int i = fromIndex; i < data.length; i++) {
			res += data[i];
		}
		return res;
	}

	public static double[] convertToDoubleArray(String[] data) {
		double[] res = new double[data.length];
		for (int i = 0; i < data.length; i++) {
			try {
				res[i] = Double.parseDouble(data[i].replaceAll(",", "."));
			} catch (Exception e) {
				res[i] = Double.NaN;
			}
		}
		return res;
	}

	public static String[] extractColumn(String[][] data, int colIndex) {
		String[] res = new String[data.length];
		for (int i = 0; i < data.length; i++) {
			res[i] = data[i][colIndex];
		}
		return res;
	}

	public static String arrayToString(String[] data) {
		String res = "(";
		for (int i = 0; i < data.length; i++) {
			res += (i != 0 ? ", " : "") + "'" + data[i] + "'";
		}
		res += ")";
		return res;
	}
}
