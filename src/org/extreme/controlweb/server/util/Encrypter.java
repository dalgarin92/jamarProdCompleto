package org.extreme.controlweb.server.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;


public class Encrypter {
    private Cipher ecipher;
    private Cipher dcipher;
    public static final String AES = "AES";
    public static final String DES = "DES";
    

   public Encrypter(String algorithm) throws NoSuchAlgorithmException {
        try {
        	int keySize = 0;
        	KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
        	if(algorithm.equals(AES))
        		keySize = 128;
        	else if(algorithm.equals(DES))
        		keySize = 56;
        	else
        		throw new NoSuchAlgorithmException("Currently, there's no implementation of this algorithm.");
			keyGen.init(keySize, new SecureRandom(ServerUtils.CRYPTO_KEY.getBytes()));
			SecretKey key = keyGen.generateKey();
            ecipher = Cipher.getInstance(algorithm);
            dcipher = Cipher.getInstance(algorithm);
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);

        } catch (Exception e) {
        	if (e instanceof NoSuchAlgorithmException) {
				throw (NoSuchAlgorithmException)e;
			} else
				e.printStackTrace();
        }
    }

    public String encrypt(String data) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = data.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (Exception e) {
        	e.printStackTrace();
        } 
        return null;
    }

    public String decrypt(String data) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(data);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (Exception e) {
        	e.printStackTrace();
        } 
        return null;
    }
}


