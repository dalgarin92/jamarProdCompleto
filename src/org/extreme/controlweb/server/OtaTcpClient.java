package org.extreme.controlweb.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.extreme.controlweb.server.util.ServerUtils;


public class OtaTcpClient {

	private static PrintWriter out;
	private static BufferedReader in;
	private static Socket kkSocket;
	private static String fromServer;
	private static String ip = ServerUtils.WEB_OTA_IP;
	private static int port = ServerUtils.WEB_OTA_PORT;
	
	private static void openConnection() throws UnknownHostException, IOException{
		kkSocket = new Socket(ip, port);
		out = new PrintWriter(kkSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
	}
	
	private static void closeConnection() throws IOException{
		out.close();
		in.close();
		kkSocket.close(); 
	}
	
		
	public static String sendCustomOTAWeb(String idvehiculo,String OTA ) throws UnknownHostException, IOException{
		openConnection();
		System.out.println(OTA);
		out.println("udp%custom_at?id="+idvehiculo+"&at="+OTA);
		waitForResponse();		
		System.out.println("Respuesta:"+fromServer);
		closeConnection();
		return fromServer;
	}
	
	public static String sendOTAWeb(String idvehiculo,int ota ) throws UnknownHostException, IOException{
		openConnection();
		String comando="";
		switch(ota){
		case 1:
			comando="udp%ref_pos?id="+idvehiculo;
			break;
		default:
			comando="udp%custom_at?id="+idvehiculo+"&at=at";
			break;			
		}		
		out.println(comando);
		waitForResponse();		
		closeConnection();		
		return fromServer;
	}
	
	public static String sendGeoFence(String idvehiculo,int radio, double latitud, double longitud, String nombre, int geoFenceNumber) throws UnknownHostException, IOException{
		openConnection();
		String comando = "";
		if (!(geoFenceNumber >= 1 && geoFenceNumber <= 25)) {
			return "ERROR: GEOFENCE NUMBER OUT BOUNDS <1-25>";
		}
		if (!(radio >= 0 && radio <= 100000 && Math.round(radio) == radio)) {
			return "ERROR: RADIO OUT OF BOUNDS <0-100000>";
		}
		if (!(latitud >= -90 && latitud <= 90)) {
			return "ERROR: LAT OUT OF BOUNDS <-90,90>";
		}

		if (!(longitud >= -180 && longitud <= 180)) {
			return "ERROR: LONG OUT OF BOUNDS <-180, 180>";
		}
			
		comando = "udp%custom_at?id=" + idvehiculo + "&at=AT$GEOFNC="
				+ geoFenceNumber + "," + radio + "," + latitud + "," + longitud;
		System.out.println(comando);
		out.println(comando);
		waitForResponse();		
		if((fromServer.contains("ERROR"))||!fromServer.contains("OK")){
			return fromServer;
		}
		/*System.out.println(fromServer);
		comando = "udp%custom_at?id=" + idvehiculo + "&at=at&w";
		System.out.println(comando);
		out.println(comando);
		waitForResponse();
		if((fromServer.contains("ERROR"))||!fromServer.contains("OK")){
			return fromServer;
		}*/
		System.out.println(fromServer);
		closeConnection();
		return fromServer;
	}
	
	public static void waitForResponse() throws IOException {
		String buffer = "";
		fromServer = "";
		while ((buffer = in.readLine()) != null) {
			fromServer += buffer + "\n";
			//break;
		}
	}

	public static void main(String args[]) {
		try {
			System.out.println("sending");
			sendCustomOTAWeb("WB01","at");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}