package org.extreme.controlweb.server.mobile;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.extreme.controlweb.server.mobile.db.Queries;

@SuppressWarnings("serial")
public class Masters extends MyHttpServlet {

	@SuppressWarnings("deprecation")
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		String type = request.getParameter("type");
		String m = getMessage();
		if (m != null && !m.equals("") && !m.equals("null"))
			response.getWriter().print(m);

		Queries myQuery = null;
		if (type.compareTo("UMR") == 0) {
			try {
				myQuery = getQueries();
				String[][] table = myQuery.getMRechazos();
				if (table.length > 0) {
					response.getWriter().print(
							"UMRechazo@OK" + "�" + table[0][0] + "@"
									+ table[0][1]);
					if (table.length > 1) {
						for (int i = 1; i < table.length; i++)
							response.getWriter().print(
									"�" + table[i][0] + "@" + table[i][1]);
					}
				} else
					response.getWriter().print("UMRechazo@FALSE");
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("UMA") == 0) {
			try {
				myQuery = getQueries();
				String[][] table = myQuery.getMAplazos();
				if (table.length > 0) {
					response.getWriter().print(
							"UMAplazo@OK" + "�" + table[0][0] + "@"
									+ table[0][1]);
					if (table.length > 1) {
						for (int i = 1; i < table.length; i++)
							response.getWriter().print(
									"�" + table[i][0] + "@" + table[i][1]);
					}
				} else
					response.getWriter().print("UMAplazo@FALSE");
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("UMPS") == 0 || type.compareTo("UMPN") == 0) {
			try {
				String contratista = request.getParameter("contratista");

				int limit = 70, offset;
				String pagina = request.getParameter("pagina");
				offset = (Integer.parseInt(pagina) - 1) * limit;

				myQuery = getQueries();

				String[][] table = myQuery.getSpecializedMPrecierres(
						contratista, type.equals("UMPN"), limit, offset);
				if (table.length > 0) {
					String header = !type.equals("UMPN") ? "UMPreCierreS"
							: "UMPreCierreN";

					String estado = table.length == limit ? "OK" : "END";

					String ans = header + "@" + estado + "�" + table[0][0]
							+ "@" + table[0][1] + "@" + table[0][2] + "@"
							+ table[0][3] + "@" + table[0][4] + "@"
							+ table[0][5] + "@" + table[0][6];

					if (table.length > 1) {
						for (int i = 1; i < table.length; i++) {
							ans += "�" + table[i][0] + "@" + table[i][1] + "@"
									+ table[i][2] + "@" + table[i][3] + "@"
									+ table[i][4] + "@" + table[i][5] + "@"
									+ table[i][6];
						}
					}
					response.getWriter().print(ans);
				} else
					response.getWriter().print("UMPreCierre@END");
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("UItemsS") == 0 || type.compareTo("UItemsN") == 0) {
			try {
				String contratista = request.getParameter("contratista");

				int limit = 30, offset;
				String pagina = request.getParameter("pagina");
				offset = (Integer.parseInt(pagina) - 1) * limit;

				myQuery = getQueries();

				String[][] table = myQuery.getItemsForMPrecierres(contratista,
						type.equals("UItemsN"), limit, offset);
				String ans = "";
				if (table.length > 0) {
					String header = type;

					String estado = table.length == limit ? "OK" : "END";

					ans += header + "@" + estado + "�" + table[0][0] + "@"
							+ table[0][1] + "@" + table[0][2];
					for (int i = 1; i < table.length; i++)
						ans += "�" + table[i][0] + "@" + table[i][1] + "@"
								+ table[i][2];
					response.getWriter().print(ans);
				} else
					response.getWriter().print("UMPreCierre@END");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("UMunicipios") == 0) {
			try {
				myQuery = getQueries();
				String[][] table = myQuery.getMunicipios();
				if (table.length > 0) {
					response.getWriter().print(
							"UMunicipios@OK" + "�" + table[0][0] + "@"
									+ table[0][1]);
					if (table.length > 1) {
						for (int i = 1; i < table.length; i++) {
							response.getWriter().print(
									"�" + table[i][0] + "@" + table[i][1]);
						}
					}
				} else {
					response.getWriter().print("UMunicipios@EMPTY");
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("UTiposTrabajos") == 0) {
			try {
				myQuery = getQueries();
				String[][] table = myQuery.getTiposTrabajos();
				if (table.length > 0) {
					response.getWriter().print(
							"UTiposTrabajos@OK" + "�" + table[0][0] + "@"
									+ table[0][1]);
					if (table.length > 1) {
						for (int i = 1; i < table.length; i++) {
							response.getWriter().print(
									"�" + table[i][0] + "@" + table[i][1]);
						}
					}
				} else {
					response.getWriter().print("UTiposTrabajos@EMPTY");
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				// if (myQuery != null)
				// myQuery.closeConnection();
			}
		}

		if (type.compareTo("PrecierresSV2") == 0
				|| type.compareTo("PrecierresNV2") == 0) {
			try {
				String ttrab = request.getParameter("tipotrab");

				String ans = "";
				myQuery = getQueries();
				String table = myQuery.getPrecierresV2(ttrab,
						type.equals("PrecierresNV2"));
				String header = type;
				if (table.length() > 1) {
					ans += header + "@OK�" + table;
					response.getWriter().print(ans);
				} else
					response.getWriter().print("UMPreCierre@END");
			} catch (Exception e) {

			}
		}
	}
}
