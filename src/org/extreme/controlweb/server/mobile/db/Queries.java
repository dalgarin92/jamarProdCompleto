package org.extreme.controlweb.server.mobile.db;

import java.util.ArrayList;
import java.util.Vector;

import org.extreme.controlweb.server.db.DBConnection;
import org.extreme.controlweb.server.mobile.util.Util;
import org.extreme.controlweb.server.util.ServerUtils;

public class Queries {

	private DBConnection db;

	// private DBConnection dbCao;

	public Queries() throws Exception {
		db = new DBConnection(ServerUtils.CONNECTION_STRING,
				ServerUtils.XCONTROL_DB, ServerUtils.DB_USER,
				ServerUtils.DB_PASSWD);

		/*
		 * dbCao = new DBConnection(ServerUtils.KAGUA_CONNECTION_STRING,
		 * ServerUtils.KAGUA_DB, ServerUtils.KAGUA_USER_DB,
		 * ServerUtils.KAGUA_PASSWD_DB);
		 */
		// dbCao = new DBConnection(DBConnection.KAGUA);
	}

	public boolean checkConnection() throws Exception {
		db.checkConnection();
		// dbCao.checkConnection();
		return true;
	}

	public boolean validarUsuario(String user, String pass) {
		String sql = "SELECT R.clavemovil FROM \"Recursos\" AS R WHERE R.usuariomovil='"
				+ user + "'";
		if (db.executeQuery(sql)) {
			String[][] Table = db.getTable();
			if (Table.length > 0) {
				if (Table[0][0].compareTo(pass) == 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public String[][] getNuevoTrabajo(String recurso, String contratista,
			boolean online, ArrayList<String> trabs, ArrayList<String> ttrabs) {
		String sql = "SELECT "
				+ "t.trabajo, " // 0
				+ "t.tipotrabajo, " // 1
				+ "tt.nombre as tipotrabajo, " // 2
				+ "to_char(t.fecha, " // 3
				+ "'YYYY/MM/DD HH24:MI'), "
				+ "t. numviaje, " // 4
				+ "m.nombre as municipio, " // 5
				+ "t.direccion, " // 6
				+ "t.barrio, " // 7
				+ "t.telefono, " // 8
				+ "t.descripcion, " // 9
				+ "t.cliente as idcliente, " // 10
				+ "t.solicitante as nomcliente, "// 11
				+ "to_char(t.fechaentrega, " // 12
				+ "'YYYY/MM/DD'), "
				+ "j.nombre as jornada, " // 13
				+ "to_char(t.horaentrega, " // 14
				+ "'HH24:MI'), "
				+ "t.estadotrabajo " // 15
				+ "FROM  public.\"TRecursos\" tr "
				+ "INNER JOIN public.\"Trabajos\" t ON (tr.trabajo = t.trabajo and tr.tipotrabajo = t.tipotrabajo) "
				+ "INNER JOIN public.\"TiposTrabajos\" tt ON (t.tipotrabajo = tt.tipotrabajo) "
				+ "INNER JOIN public.\"Municipios\" m ON (t.municipio = m.municipio) "
				+ "LEFT OUTER JOIN public.\"Clientes\" c ON (t.cliente = c.cliente) "
				+ "LEFT OUTER JOIN public.\"Jornadas\" j ON (t.jornadaentrega = j.idjornada)"
				+ "where "
				+ "(t.estadotrabajo = '00' or t.estadotrabajo = '10' or t.estadotrabajo = '20' or t.estadotrabajo = '60') "
				+ "and viajefinalizado = 'N' and tr.recurso = '" + recurso
				+ "' and tr.contratista = '" + contratista + "' ";
		if (trabs != null && trabs.size() > 0) {
			sql += "and t.trabajo||t.tipotrabajo not in (";
			for (int i = 0; i < trabs.size(); i++)
				sql += "'" + trabs.get(i) + ttrabs.get(i) + "'"
						+ (i == trabs.size() - 1 ? ") " : ", ");
		}
		sql += "order by t.estadotrabajo desc, t.ordenamiento asc ";

		if (online)
			sql += "limit 1";

		if (db.executeQuery(sql))
			return db.getTable();
		else
			return null;
	}

	public boolean isResourceOnlineMode(String user) {
		String sql = "SELECT R.online FROM \"Recursos\" AS R WHERE R.usuariomovil='"
				+ user + "'";
		boolean sw = false;
		if (db.executeQuery(sql)) {
			db.next();
			try {
				String s = db.getString(1);
				if (s.compareTo("S") == 0) {
					sw = true;
				} else {
					sw = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sw;

	}

	public boolean isResourceOnlineMode(String recurso, String contratista) {
		String sql = "SELECT R.online FROM \"Recursos\" AS R WHERE R.recurso='"
				+ recurso + "' and R.contratista='" + contratista + "'";
		boolean sw = false;
		if (db.executeQuery(sql)) {
			if (db.getTable()[0][0].compareTo("S") == 0) {
				sw = true;
			} else {
				sw = false;
			}
		}
		return sw;
	}

	public String[][] getPKsDeRecurso(String user) {
		String sql = "SELECT R.recurso, R.contratista, coalesce(V.idvehiculo, '') as idvehiculo, R.nombre "
				+ "FROM \"Recursos\"  R LEFT OUTER JOIN \"vehiculos\" V "
				+ "ON (R.recurso = V.recurso and R.contratista=V.contratista) "
				+ "WHERE R.usuariomovil='" + user + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getMunicipios() {
		String sql = "select * from \"Municipios\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getTiposTrabajos() {
		String sql = "select tipotrabajo, nombre from \"TiposTrabajos\" where repcampo='S' and estado = 'S'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public boolean rejectTask(String recurso, String contratista,
			String trabajo, String tipotrabajo, String mrechazo,
			String naprobacion, String fecha) throws Exception {

		String sql = "select estadotrabajo from \"Trabajos\" where trabajo='"
				+ trabajo + "' and tipotrabajo ='" + tipotrabajo + "'";
		if (db.executeQuery(sql)) {
			String estado = db.getTable()[0][0];
			if (!estado.equals("30") && !estado.equals("40")) {
				sql = "update \"Trabajos\" set estadotrabajo='"
						+ Util.REJECTED_TASK + "' where trabajo='" + trabajo
						+ "' and tipotrabajo ='" + tipotrabajo + "'";
				if (db.executeUpdate(sql) > 0) {
					sql = "insert into \"TRechazos\"(trabajo, tipotrabajo, recurso, "
							+ "contratista, mrechazo, numaprobacion, fecharechazo ) values ('"
							+ trabajo
							+ "','"
							+ tipotrabajo
							+ "','"
							+ recurso
							+ "','"
							+ contratista
							+ "','"
							+ mrechazo
							+ "','"
							+ naprobacion + "','" + fecha + "')";
					boolean sw = db.executeUpdate(sql) != 0;

					/*
					 * String[] trab = trabajo.split("-"); String codmunip =
					 * trab[0]; String codtrab = trab[1];
					 */

					/*
					 * try { sql =
					 * "select count(*) from trabajos_pendientes where municipio = "
					 * + codmunip + " and cod_concep = '" + tipotrabajo +
					 * "' and cod_otrb = " + codtrab; if
					 * (dbCao.executeQuery(sql)) { if
					 * (dbCao.getTable()[0][0].equals("0")) { sql =
					 * "update \"Trabajos\" set estadotrabajo = '30' where trabajo = '"
					 * + trabajo + "' and tipotrabajo = '" + tipotrabajo + "'";
					 * db.executeUpdate(sql); } } } catch (Exception e) { }
					 */
					return sw;
				} else
					throw new Exception(sql);
			} else
				return true;
		} else
			throw new Exception(sql);
	}

	public boolean postponeTask(String recurso, String contratista,
			String trabajo, String tipotrabajo, String idmotivo, String fecha)
			throws Exception {
		String sql = "select estadotrabajo from \"Trabajos\" where trabajo='"
				+ trabajo + "' and tipotrabajo ='" + tipotrabajo + "'";
		if (db.executeQuery(sql)) {
			String estado = db.getTable()[0][0];
			if (!estado.equals("30") && !estado.equals("40")) {
				sql = "update \"Trabajos\" set estadotrabajo='"
						+ Util.POSTPONED_TASK + "' where trabajo='" + trabajo
						+ "' and tipotrabajo ='" + tipotrabajo + "'";

				if (db.executeUpdate(sql) > 0) {
					sql = "insert into \"TMotivos\"(trabajo, tipotrabajo, recurso, "
							+ "contratista, idmotivo, fecha) values ('"
							+ trabajo
							+ "','"
							+ tipotrabajo
							+ "','"
							+ recurso
							+ "','"
							+ contratista
							+ "','"
							+ idmotivo
							+ "','"
							+ fecha + "')";
					boolean sw = db.executeUpdate(sql) != 0;
					return sw;
				} else
					throw new Exception(sql);
			} else
				return true;
		} else
			throw new Exception(sql);
	}

	public boolean cleanTasks() throws Exception {
		String sql = "update \"Trabajos\" set estadotrabajo='00' ";
		// , fechadescarga=null, fechainicio=null, fechaprecierre=null";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean changeStatusOfAResource(String recurso, String contratista,
			String estado) throws Exception {
		String sql = "update \"Recursos\" set estadomovil='" + estado
				+ "' where recurso='" + recurso + "' and contratista ='"
				+ contratista + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean changeStatusOfATask(String trabajo, String tipotrabajo,
			String estado) throws Exception {
		String sql = "update \"Trabajos\" set estadotrabajo='" + estado
				+ "' where trabajo='" + trabajo + "' and tipotrabajo ='"
				+ tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean downloadTask(String trabajo, String tipotrabajo)
			throws Exception {
		String sql = "update \"Trabajos\" set estadotrabajo='"
				+ Util.DOWNLOADED_TASK
				+ "', fechadescarga = coalesce(fechadescarga, now()) where trabajo='"
				+ trabajo + "' and tipotrabajo ='" + tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean executeATask(String trabajo, String tipotrabajo,
			String fechainicio) throws Exception {
		String sql = "select fechainicio from \"Trabajos\" where trabajo = '"
				+ trabajo + "' and tipotrabajo = '" + tipotrabajo + "'";
		if (db.executeQuery(sql)) {
			db.next();
			String res = db.getString(1);
			boolean sw = false;
			if (res == null || res.equals("null") || res.trim().equals("")) {
				sql = "update \"Trabajos\" set estadotrabajo='"
						+ Util.TASK_IN_EXECUTION
						+ "', fechainicio=coalesce(fechainicio, '"
						+ fechainicio + "') where trabajo='" + trabajo
						+ "' and tipotrabajo ='" + tipotrabajo + "'";
				if (db.executeUpdate(sql) > 0) {
					sw = true;
				} else
					throw new Exception(sql);
			} else {
				sql = "update \"Trabajos\" set estadotrabajo='"
						+ Util.TASK_IN_EXECUTION + "' where trabajo='"
						+ trabajo + "' and tipotrabajo ='" + tipotrabajo + "'";
				if (db.executeUpdate(sql) > 0) {
					sw = true;
				} else
					throw new Exception(sql);
				sw = true;
			}

			// iniciarKAGUA(trabajo, tipotrabajo);
			return sw;
		} else
			throw new Exception(sql);
	}

	public String preCloseATask(String recurso, String contratista,
			String trabajo, String tipotrabajo, String fechaprecierre,
			String realizo) throws Exception {
		String sql = "select * from precerrartrabajo('" + recurso + "', '"
				+ contratista + "', '" + trabajo + "', '" + tipotrabajo
				+ "', '" + fechaprecierre + "', '" + realizo + "');";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1);
		} else {
			throw new Exception(sql);
		}
	}

	public boolean savePreClosingReasons(String idcodprecierre)
			throws Exception {
		String sql = "insert into \"CodigosTPrecierres\" (idprecierre, idcodigopre ) values ( (select last_value from \"TPrecierres_idprecierre_seq\") , '"
				+ idcodprecierre + "' ) ";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean savePreClosingListReason(String idprecierre, String idlist,
			String idcampo) throws Exception {
		String sql = "insert into \"TPrecierresListas\" (idprecierre, id_lista, idcampo ) values ( "
				+ idprecierre + " , '" + idlist + "', '" + idcampo + "' ) ";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean savePreClosingTextReason(String idprecierre, String idcampo,
			String valor) throws Exception {
		String sql = "insert into \"TPrecierresTextos\" (idprecierre, id_campo, valor ) values ( "
				+ idprecierre + " , '" + idcampo + "', '" + valor + "' ) ";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean CreateATaskWithClientFromCampus(String municipio,
			String tipotrabajo, String Cliente, String Descripcion,
			String Direccion) throws Exception {
		String sql = "insert into \"Trabajos\" (trabajo, tipotrabajo,proceso, municipio, cliente, direccion, descripcion, ordenamiento, fecha, prioridad, latitud, longitud) values ( 'TRC-'||nextval('\"Trabajos_trabajo_seq\"'::regclass),'"
				+ tipotrabajo
				+ "', (select distinct proceso from \"Procesos\" where tipostrabajos like '%"
				+ tipotrabajo
				+ "%'  limit 1), '"
				+ municipio
				+ "', '"
				+ Cliente
				+ "', '"
				+ Direccion
				+ "' ,'"
				+ Descripcion
				+ "',0, now(), 'N',";
		sql += "(select latitud from \"EjesViales\" where dirnoform = obtienecabecera(normalizador('"
				+ Direccion
				+ "')) and municipio='"
				+ municipio
				+ "') , (select longitud from \"EjesViales\" where dirnoform = obtienecabecera(normalizador('"
				+ Direccion + "')) and municipio='" + municipio + "'))";

		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean createATaskFromCampus(String municipio, String tipotrabajo,
			String descripcion, String Direccion, String recurso,
			String contratista, String predio) throws Exception {
		// return createTaskKAGUA(municipio, tipotrabajo, recurso, contratista,
		// predio, Direccion, descripcion);
		String cod_ot = String.valueOf((int) (Math.random() * 10000.0d));
		String sql = "insert into \"Trabajos\" (trabajo, tipotrabajo,proceso, municipio, direccion, descripcion, ordenamiento, fecha, prioridad, latitud, longitud) values ( '"
				+ municipio
				+ "-"
				+ cod_ot
				+ "','"
				+ tipotrabajo
				+ "', (select distinct proceso from \"Procesos\" where tipostrabajos like '%"
				+ tipotrabajo
				+ "%' and municipios like '%"
				+ municipio
				+ "%' limit 1), '"
				+ municipio
				+ "', '"
				+ Direccion
				+ "' ,'"
				+ descripcion
				+ "',0, now(), 'N',"
				+ "(select latitud from \"EjesViales\" where dirnoform = obtienecabecera(normalizador('"
				+ Direccion
				+ "')) and municipio='"
				+ municipio
				+ "') , (select longitud from \"EjesViales\" where dirnoform = obtienecabecera(normalizador('"
				+ Direccion + "')) and municipio='" + municipio + "'))";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else
			throw new Exception(sql);
	}

	public String[][] getInfoCliente(String CodCli) {
		String sql = "select cliente, \"Clientes\".nombre, direccion, telefono, \"Municipios\".nombre from \"Clientes\" inner join \"Municipios\" on (\"Clientes\".municipio = \"Municipios\".municipio ) where cliente='"
				+ CodCli + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	@Deprecated
	public String[][] getInfoClienteElec(String CodCli) {
		String sql = "SELECT nisrad, secnis, nic, tservicio, tsuministro, tarifa, tconexion, "
				+ "ttension, potencia, ruta, itinerario, municipio, localidad, urbanizacion, "
				+ "direccion, nombre, apellido, telefono, tcliente "
				+ "FROM \"ClientesEC\" where nic = '" + CodCli + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getInfoClienteByAddress(String address, String municipio) {
		String sql = "select cliente, direccion from \"Clientes\" where UPPER(direccion) like '%"
				+ address.toUpperCase()
				+ "%' and municipio ='"
				+ municipio
				+ "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getDatosDeUsuario(String user) {
		String sql = "SELECT U.NOMBRE, U.CLAVE, U.CARGO, P.PERFIL AS CODPERFIL, P.NOMBRE AS NOMBREPERFIL"
				+ " FROM \"Usuarios\" AS U INNER JOIN \"UsuariosPerfiles\" AS UP ON U.USUARIO = UP.USUARIO INNER JOIN"
				+ " \"Perfiles\" AS P ON UP.PERFIL = P.PERFIL WHERE U.USUARIO='"
				+ user + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getMRechazos() {
		String sql = "SELECT * from \"MRechazos\" order by mrechazo desc";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public String[][] getMAplazos() {
		String sql = "SELECT DISTINCT idmotivo, descripcion, tipo FROM \"Motivos\" WHERE tipo = 'A' ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		}
		return null;
	}

	/*
	 * String[][] getSpecializedMPrecierres(String contratista) { Stringsql=
	 * "select r.tipotrabajo, r.id_campo, r.descripcion, s.id_lista, s.descripcion, r.tipo from (select q.tipotrabajo, q.id_campo, q.descripcion, q.tipo from (SELECT distinct tipotrabajo from \"MTTContratistas\" where contratista='"
	 * +contratista+
	 * "') p inner join \"CamposPrecierres\" q on (p.tipotrabajo= q.tipotrabajo) where estado='A'  order by orden asc) r left JOIN \"ListasPrecierres\" s ON (r.id_campo=s.id_campo)"
	 * ; if (db.executeQuery(sql)) { return db.getTable(); } else return null; }
	 */

	@Deprecated
	public String[][] getSpecializedMPrecierres(String contratista,
			boolean isNoRealizacion, int limit, int offset) {

		/*
		 * String sql =
		 * "select m.tipotrabajo, m.campo, m.descripcion, lp.id_lista, lp.descripcion, m.tipo, m.item, m.descitem from "
		 * +
		 * "(select r.tipotrabajo, cp.id_campo as campo, cp.id_categoria, cp.descripcion, cp.tipo, r.item, r.descripcion as descitem "
		 * +
		 * "from (select q.tipotrabajo, q.item, q.descripcion from (SELECT distinct tipotrabajo from \"MTTContratistas\" "
		 * + "where contratista='" + contratista +
		 * "') p inner join \"ItemsPrecierres\" q on (p.tipotrabajo= q.tipotrabajo) where "
		 * + "estado='A' and esnorealizacion = '" + (isNoRealizacion ? "S" :
		 * "N") + "' order by orden asc) r " +
		 * "inner join \"CamposPrecierres\" cp on (r.item=cp.item) where estado='A' order by orden asc) m left outer join \"ListasPrecierres\" lp ON (m.id_categoria = lp.id_categoria)"
		 * ;
		 */

		String sql = "select x.tipotrabajo, x.campo, x.desc_campo, x.id_lista, x.desc_lista, x.tipo, x.item, x.descitem from (select m.tipotrabajo, m.campo, m.descripcion as desc_campo, lp.id_lista, "
				+ "lp.descripcion as desc_lista, m.tipo, m.item, m.descitem, m.orden_items, m.orden_campos from "
				+ "(select r.tipotrabajo, cp.id_campo as campo, cp.id_categoria, cp.descripcion, cp.tipo, r.item, r.descripcion as descitem, r.orden_items, cp.orden as orden_campos "
				+ "from (select q.tipotrabajo, q.item, q.descripcion, q.orden as orden_items from (SELECT distinct tipotrabajo from \"MTTContratistas\" "
				+ "where contratista='"
				+ contratista
				+ "') p inner join \"ItemsPrecierres\" q on (p.tipotrabajo= q.tipotrabajo) where "
				+ "estado='A' and esnorealizacion = '"
				+ (isNoRealizacion ? "S" : "N")
				+ "') r "
				+ "inner join \"CamposPrecierres\" cp on (r.item=cp.item) where estado='A') m left outer join \"ListasPrecierres\" lp ON (m.id_categoria = lp.id_categoria)) x "
				+ "order by item::integer, orden_campos::integer limit "
				+ limit + " offset " + offset;

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	@Deprecated
	public String[][] getItemsForMPrecierres(String contratista,
			boolean isNoRealizacion, int limit, int offset) {

		String sql = "select q.item, q.descripcion, q.tipotrabajo from "
				+ "(SELECT distinct tipotrabajo from \"MTTContratistas\" where contratista='"
				+ contratista
				+ "') p "
				+ "inner join \"ItemsPrecierres\" q on (p.tipotrabajo= q.tipotrabajo) "
				+ "where estado='A' and esnorealizacion = '"
				+ (isNoRealizacion ? "S" : "N")
				+ "' order by tipotrabajo, orden::integer limit " + limit
				+ " offset " + offset;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else
			return null;
	}

	public boolean closeConnection() {
		return db.closeAll();
	}

	public String getPrecierresV2(String ttrab, boolean isNoRealizacion)
			throws Exception {
		String sql = "select getprecierres('" + ttrab + "', '"
				+ (isNoRealizacion ? "S" : "N") + "', '~', '@', '�', '/*')";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1);
		} else
			throw new Exception(sql);
	}

	// ----------------------------------- KAGUA
	// --------------------------------------------------

	/*
	 * private boolean iniciarKAGUA(String trabajo, String tipotrabajo) throws
	 * Exception { String sql = "select * from iniciar_ot(" +
	 * trabajo.split("-")[1] + ", '" //String sql = "select * from iniciar_ot("
	 * + trabajo + ", '" + tipotrabajo + "'," + getMunicipioDeTrabajo(trabajo) +
	 * ", '1')"; if (dbCao.executeQuery(sql)) { dbCao.next();
	 * if(dbCao.getString(1).equals("0") || dbCao.getString(1).equals("3"))
	 * return true; else throw new Exception(dbCao.getString(2)); } else throw
	 * new Exception(sql); }
	 */

	/*
	 * public String[] savePreClosingKAGUA(String trabajo, String tipotrabajo,
	 * boolean realizo, String idresp, String idcampo, String resumen) throws
	 * Exception {
	 * 
	 * resumen = resumen.replaceAll("--", "' || CHR(13) || CHR(13) || '");
	 * resumen = resumen.replaceAll("-", "' || CHR(13) || '");
	 * 
	 * 
	 * String sql =
	 * "select out_put, descripcion, id, to_char(f_ini, 'YYYY/MM/DD HH24:MI'), "
	 * + "to_char(f_fin, 'YYYY/MM/DD HH24:MI') from finalizar_ot(" +
	 * trabajo.split("-")[1] + ", '" //String sql =
	 * "select * from finalizar_ot(" + trabajo + ", '" + tipotrabajo + "', " +
	 * getMunicipioDeTrabajo(trabajo) + ", '1', " + (realizo ? "1" : "0") +
	 * ", '" + getCodigoPrecierre(idresp, idcampo) + "', '" + resumen + "')"; if
	 * (dbCao.executeQuery(sql)) { String[] res = dbCao.getTable()[0];
	 * if(res[0].equals("0")){ return new String[]{res[3], res[4]}; } else
	 * //throw new Exception(dbCao.getString(2)); return new String[]{"N/A",
	 * "N/A"}; } else throw new Exception(sql); }
	 */

	public String getCodigoPrecierre(String idcodpre, String idcampo)
			throws Exception {
		String sql = "select codigo from \"ListasPrecierres\" lp, \"CamposPrecierres\" cp "
				+ "where lp.id_categoria = cp.id_categoria and lp.id_lista = '"
				+ idcodpre + "' and cp.id_campo = '" + idcampo + "'";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1);
		} else
			throw new Exception(sql);
	}

	/*
	 * public String createTaskKAGUA(String munip, String ttrab, String recurso,
	 * String contratista, String predio, String dir, String descripcion) throws
	 * Exception { //generar_ot_movil(vmunicipio integer, //vempresa character
	 * varying, //vconcept character varying, //vcontra numeric, //vemple
	 * numeric, //vpredio numeric, //vdirecc character varying, //vobservacion
	 * character varying) String sql = "select * from generar_ot_movil(" + munip
	 * + ", '1', '" + ttrab + "', " + contratista + ", " + recurso + ", " +
	 * predio + ", '" + dir + "'::character varying, '" + descripcion + "')"; if
	 * (dbCao.executeQuery(sql)) { dbCao.next();
	 * if(dbCao.getString(1).equals("0")) return dbCao.getString(3); else
	 * //throw new Exception(dbCao.getString(2) + " -- " + sql); throw new
	 * Exception(dbCao.getString(2)); } else throw new Exception(sql); }
	 */

	/*
	 * public String[][] getInfoPredioKAGUA(String predio, String municipio)
	 * throws Exception { String sql = "select * from infopredio(" + predio +
	 * ", " + municipio + ", '1')"; if (dbCao.executeQuery(sql)) return
	 * dbCao.getTable(); else throw new Exception(sql); }
	 */

	/*
	 * public String[][] getInfoDirsKAGUA(String dir, String munip) throws
	 * Exception{ String sql = "select * from buscarxdireccion('" + dir.trim() +
	 * "', " + munip + ", '1')"; if (dbCao.executeQuery(sql)) return
	 * dbCao.getTable(); else throw new Exception(sql); }
	 */

	public boolean saveGPSStream(String evento, String idveh, String date,
			String lat, String lon, String vel) throws Exception {
		String[] dir = getDireccion(lat, lon);
		String sql = "INSERT INTO EVENTOS VALUES('" + idveh + "','" + evento
				+ "',localtimestamp, " + "'" + vel + "','" + dir[0] + "','"
				+ lat + "','" + lon + "','0','0','" + dir[1] + "', '" + dir[2]
				+ "')";
		if (db.executeUpdate(sql) > 0) {
			sql = "UPDATE VEHICULOS SET LATITUD = '" + lat + "', LONGITUD = '"
					+ lon + "' , DIRECCION = '" + dir[0] + "', "
					+ "MUNICIPIO = '" + dir[1] + "', DEPARTAMENTO = '" + dir[2]
					+ "', ULTIMAACTUALIZACION = LOCALTIMESTAMP, "
					+ "VELOCIDAD = '" + vel + "', ULTIMOEVENTO = '" + evento
					+ "' WHERE IDVEHICULO = '" + idveh + "'";
			if (db.executeUpdate(sql) > 0)
				return true;
			else
				throw new Exception(sql);
		} else
			throw new Exception(sql);
	}

	private String[] getDireccion(String lat, String lon) throws Exception {
		String l = lat.replace(",", ".");
		String lo = lon.replace(",", ".");
		String sql = "select * from getdireccion(" + l + ", " + lo + ")";
		if (db.executeQuery(sql))
			return db.getTable()[0];
		else
			throw new Exception(sql);
	}

	public String[] getInfoFinalizacion(String trabajo, String tipotrabajo)
			throws Exception {
		String sql = "select to_char(fechainicio, 'YYYY/MM/DD HH24:MI'), "
				+ "to_char(fechaprecierre, 'YYYY/MM/DD HH24:MI') from \"Trabajos\" "
				+ "where trabajo = '" + trabajo + "' and tipotrabajo = '"
				+ tipotrabajo + "'";
		if (db.executeQuery(sql))
			return db.getTable()[0];
		else
			throw new Exception(sql);

	}

	/*
	 * public String[][] getInfoVecinosKAGUA(String predio, String municipio,
	 * int dist) throws Exception { String sql =
	 * "select cod_pred, rutareparto, nombre, direccion from prediosvecinos (" +
	 * predio + ", " + municipio + ", '1', " + dist + ")";
	 * if(dbCao.executeQuery(sql)) return dbCao.getTable(); else throw new
	 * Exception(sql); }
	 */

	// public String saveAttachedInfoKAGUA(String trabajo, String tipotrabajo,
	// ArrayList<String> codpres) throws Exception {
	// String sql = "";
	// if(tipotrabajo.equals("SUP") || tipotrabajo.equals("RCX"))
	/*
	 * sql = "select \"xcontrol_actOtAdicional\"(" + trabajo.split("-")[1] +
	 * ", '" + tipotrabajo + "', " + trabajo.split("-")[0] + ", '1', " +
	 * codpres.get(2) + ", '" + codpres.get(1) + "')";
	 */
	/*
	 * else if(tipotrabajo.equals("RCX")) sql =
	 * "select xcontrol_actOtAdicional(" + trabajo.split("-")[1] + ", '" +
	 * tipotrabajo + "', " + trabajo.split("-")[0] + ", '1', " + codpres.get(2)
	 * + ", '" + codpres.get(1) + "')";
	 */
	/*
	 * if (dbCao.executeQuery(sql)) return dbCao.getTable()[0][0]; else throw
	 * new Exception(sql); }
	 */

	public String[] getInfoMail(String trabajo) throws Exception {
		String[][] resul = null;
		String sql = "select tr.recurso, "
				+ "c.nombre, "
				+ "t.trabajo, "
				+ "t.cliente||' - '||t.solicitante as cliente, "
				+ "cp.opcionprecierre|| ' - ' || cp.codigoprecierre as resultado, "
				+ "t.direccion, "
				+ "(select direccion from getdireccion(tp.latitud::double precision, tp.longitud::double precision) limit 1) as direccionregistro "
				+ "from \"Trabajos\" t "
				+ "inner join \"TRecursos\" tr using(trabajo) "
				+ "inner join \"TPrecierres\" tp using(trabajo) "
				+ "inner join \"Conductores\" c on(c.codigo = t.codconductor) "
				+ "inner join \"vwCodigosPrecierres\" cp on(cp.idprecierre = tp.idprecierre) "
				+ "where t.trabajo = '" + trabajo + "'";
		if (db.executeQuery(sql)) {
			resul = db.getTable();
			if (resul != null && resul.length > 0)
				return resul[0];
			else
				return null;
		}

		else
			throw new Exception(sql);
	}

	public Vector<String> getMails() {
		String sql = "select email from \"ContactosAlarmas\" "
				+ "where enviaremail and estado = 'A'";
		Vector<String> res = new Vector<String>();
		if (db.executeQuery(sql)) {
			String[][] resul = db.getTable();
			for (String[] str : resul) {
				res.add(str[0]);
			}
		}
		return res;
	}

	public Vector<String> getCels() {
		String sql = "select 'MOVISTAR', celular from \"ContactosAlarmas\" "
				+ "where enviarsms and estado = 'A'";
		Vector<String> res = new Vector<String>();
		if (db.executeQuery(sql)) {
			String[][] resul = db.getTable();
			for (String[] str : resul) {
				res.add(str[0]);
			}
		}
		return res;
	}
	
}
