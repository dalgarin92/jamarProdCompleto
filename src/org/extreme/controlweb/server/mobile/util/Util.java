package org.extreme.controlweb.server.mobile.util;

public class Util {

	public static final String DATE_FORMAT = "yyyy/MM/dd hh12:mi:ss AM";

	public static final String DOWNLOADED_TASK = "10";
	public static final String TASK_IN_EXECUTION = "20";
	public static final String PRECLOSED_TASK = "30";
	public static final String POSTPONED_TASK = "60";
	public static final String REJECTED_TASK = "50";
}
