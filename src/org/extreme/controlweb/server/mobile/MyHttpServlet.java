package org.extreme.controlweb.server.mobile;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.extreme.controlweb.server.mobile.db.Queries;
import org.extreme.controlweb.server.util.ServerUtils;

@SuppressWarnings("serial")
public class MyHttpServlet extends HttpServlet {
	private Queries db;
	private String message;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			ServerUtils.initialize(ServerUtils.CURRENT_ENVIRONMENT);
			db = new Queries();
		} catch (Exception e) {
			message = e.getMessage();
		}
	}
	
	protected Queries getQueries(){
		try {
			db.checkConnection();
		} catch (Exception e) { }
		return db;
	}
	
	protected String getMessage() {
		return message;
	}
}
