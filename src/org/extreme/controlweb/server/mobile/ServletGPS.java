package org.extreme.controlweb.server.mobile;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.extreme.controlweb.server.mobile.db.Queries;

@SuppressWarnings("serial")
public class ServletGPS extends MyHttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		try {
			String trama = req.getParameter("trama");
			resp.setHeader("cache-control", "no-store");
			resp.setHeader("Pragma", "no-cache");
			if (trama != null) {
				String[] splitted = trama.split("@");
				// evento@codempl-codcontratista@fecha en
				// long@latitud@longitud@velocidad
				String evento = splitted[0];
				String idveh = splitted[1];
				String date = splitted[2];
				String lat = splitted[3] != "" ? splitted[3] : "0";
				String lon = splitted[4] != "" ? splitted[4] : "0";
				String vel = splitted[5] != "" ? splitted[5] : "0";

				Double.parseDouble(lat);
				Double.parseDouble(lon);
				
				Queries q = getQueries();
				if(q.saveGPSStream(evento, idveh, date, lat, lon, vel))
					out.print("OK");
				else
					out.print("ERROR");
				
			}
		} catch (NumberFormatException e) {

		} catch (Exception e) {
			out.print(e.getMessage());
		}
	}

}
