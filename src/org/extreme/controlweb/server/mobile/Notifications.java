package org.extreme.controlweb.server.mobile;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.extreme.controlweb.server.io.XSenderTcpClient;
import org.extreme.controlweb.server.mobile.db.Queries;

@SuppressWarnings("serial")
public class Notifications extends MyHttpServlet {

	/*
	 * public static void main(String[] args){ try { Queries q = new Queries();
	 * String[] res = q.savePreClosingKAGUA("3-70702", "114", false, "11", "20",
	 * "PRUEBA"); System.out.print(res.toString()); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		String action = request.getParameter("action");
		String recurso = request.getParameter("recurso");
		String contratista = request.getParameter("contratista");
		String trabajo = request.getParameter("trabajo");
		String tipotrabajo = request.getParameter("tipotrabajo");
		String offset = request.getParameter("offset");
		long l = 0;
		if (offset != null)
			l = new Date().getTime() - new Long(offset).longValue();

		String fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.format(new Date(l));
		Queries q;
		PrintWriter writer = response.getWriter();

		try {
			q = getQueries();
			if (action.equals("Postpone")) {
				String maplazo = request.getParameter("maplazo");
				if (q.postponeTask(recurso, contratista, trabajo, tipotrabajo,
						maplazo, fecha)) {
					writer.print("Postpone@OK");
				} else {
					writer.print("Postpone@FALSE");
				}
			} else {
				if (action.equals("Reject")) {
					String mrechazo = request.getParameter("mrechazo");
					String naprobacion = request.getParameter("naprobacion");
					/*
					 * if (myQuery.rejectTask(recurso, contratista, trabajo,
					 * tipotrabajo, mrechazo, fecha)) writer.print("Reject@OK");
					 * else writer.print("Reject@FALSE");
					 */
					q.rejectTask(recurso, contratista, trabajo, tipotrabajo,
							mrechazo, naprobacion, fecha);
					writer.print("Reject@OK");
				} else {
					if (action.compareTo("Execute") == 0) {
						try {
							if (q.executeATask(trabajo, tipotrabajo, fecha))
								writer.print("Execute@OK");
							else
								writer.print("Execute@FALSE");
						} catch (Exception e) {
							writer.print("Execute@" + e.getMessage());
						}
					} else {
						if (action.compareTo("PreClosing") == 0) {
							try {
								// int num = request.getParameterMap().size() -
								// 6;
								String realizo = request
										.getParameter("realizo");
								// String resumen =
								// request.getParameter("resumen");

								ArrayList<String> codpres = new ArrayList<String>();

								String idprecierre = q.preCloseATask(recurso,
										contratista, trabajo, tipotrabajo,
										fecha, realizo);
								String[] res = null;
								if (idprecierre != null) {
									if (!idprecierre.equals("DUPLICATED")) {
										boolean sw = true;
										int i = 1;
										// for (int i = 1; i <= num && sw ==
										// true; i++) {
										while (sw == true
												&& request
														.getParameter("CodPre"
																+ i) != null) {
											// key = "CodPre" + i;
											String valor = request
													.getParameter("CodPre" + i);
											String[] v = valor.split(":");
											String tipo = v[0];
											String value = "";
											if (v.length > 1)
												value = v[1];
											// este value, si es lista, hay que
											// buscarlo en la bd
											// con
											if (tipo.equals("list"))
												codpres.add(q
														.getCodigoPrecierre(
																value, v[2]));
											else
												codpres.add(value);

											if (tipo.compareTo("list") == 0) {
												q.savePreClosingListReason(
														idprecierre, value,
														v[2]);
												/*
												 * if (i == 1 && sw2) res =
												 * myQuery
												 * .savePreClosingKAGUA(trabajo,
												 * tipotrabajo, realizo
												 * .equals("S"), value, v[2],
												 * resumen);
												 */
											} else
												q.savePreClosingTextReason(
														idprecierre, tipo,
														value);

											i++;
										}
										if (sw) {
											res = q.getInfoFinalizacion(
													trabajo, tipotrabajo);
											writer.print("PreClosing@OK@"
													+ res[0] + "@" + res[1]);
											try {
												String[] info = q
														.getInfoMail(trabajo);
												String mensaje = "\nResultado de visita: "
														+ "\nFecha y Hora: "
														+ new Date()
														+ "\nPlaca: "
														+ info[0]
														+ "\nConductor: "
														+ info[1]
														+ "\nEntrega: "
														+ info[2]
														+ "\nCliente: "
														+ info[3]
														+ "\nResultado: "
														+ info[4]
														+ "\nDireccion Cliente: "
														+ info[5]
														+ "\nDireccion Registro: "
														+ info[6];
												Vector<String> mail = q
														.getMails();
												// Vector<String> sms=
												// q.getCels();
												// mail.add("jarroyo@extreme.com.co");
												XSenderTcpClient.sendMessage(
														"JAMAR",
														"Resultado de visita "
																+ trabajo,
														mail,
														new Vector<String[]>(),
														new Date(), mensaje);
											} catch (Exception e) {
												e.printStackTrace();
											}
										} else
											writer.print("PreClosing@FALSE");
									} else {
										q.changeStatusOfATask(trabajo,
												tipotrabajo, "30");
										res = q.getInfoFinalizacion(trabajo,
												tipotrabajo);
										writer.print("PreClosing@OK@" + res[0]
												+ "@" + res[1]);
									}
								} else
									writer.print("PreClosing@FALSE");
							} catch (Exception e) {
								writer.print("PreClosing@" + e.getMessage());
							}
						} else {
							if (action.compareTo("clean") == 0) {
								if (q.cleanTasks())
									writer.print("Borre");
								else
									writer.print("No Borre");
							} else {
								try {
									if (action.compareTo("NewTask") == 0) {
										String municipio = request
												.getParameter("mun");
										String tipoTrabajo = request
												.getParameter("tt");
										// String cliente =
										// request.getParameter("cli");
										String descripcion = request
												.getParameter("desc");
										String direccion = request
												.getParameter("dir").trim();

										recurso = request
												.getParameter("recurso");
										contratista = request
												.getParameter("contratista");
										String predio = request
												.getParameter("predio");
										if (predio != null
												&& predio.equals("-"))
											predio = null;

										municipio = municipio.replace('+', ' ');
										tipoTrabajo = tipoTrabajo.replace('+',
												' ');
										// cliente = cliente.replace('+', ' ');
										descripcion = descripcion.replace('+',
												' ');
										direccion = direccion.replace('+', ' ');
										// TODO: verificar ultimo creado desde
										// campo

										// String cod =
										boolean cod = q.createATaskFromCampus(
												municipio, tipoTrabajo,
												descripcion, direccion,
												recurso, contratista, predio);

										// if (Cliente.compareTo("-") == 0) {
										// if ()
										writer.print("NewTask@OK@" + cod);
										// else
										// writer.print("NewTask@FALSE");
										/*
										 * } else { if
										 * (myQuery.CreateATaskWithClientFromCampus
										 * ( municipio, TipoTrabajo, Cliente,
										 * Descripcion, Direccion))
										 * writer.print("NewTask@OK"); else
										 * writer.print("NewTask@FALSE"); }
										 */

									} else {
										if (action.compareTo("LogOut") == 0) {
											String estado = request
													.getParameter("Estado");
											q.changeStatusOfAResource(recurso,
													contratista, estado);
										}
									}
								} catch (Exception e) {
									writer.print("NewTask@" + e.getMessage());
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace(writer);
		}
	}
}
