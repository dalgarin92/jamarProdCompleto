package org.extreme.controlweb.server.mobile;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.extreme.controlweb.server.mobile.db.Queries;

@SuppressWarnings("serial")
public class Consultas extends MyHttpServlet {
	
	/*private String handleNull(String str){
		return str != null ? str : "";
	}*/

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		response.setHeader("cache-control", "no-store");
		response.setHeader("Pragma", "no-cache");
		String type = request.getParameter("type");
		Queries myQuery;
		PrintWriter writer = response.getWriter();
		try {
			//myQuery = new Queries();
			myQuery = getQueries();
			/*if(type.compareTo("infoClienteKAGUA") == 0){
				try {
					String predio = request.getParameter("predio").trim();
					String municipio = request.getParameter("munip");
					String[][] table = myQuery.getInfoPredioKAGUA(predio, municipio);
					String trama = "";
					if(table.length > 0) {
					    trama += "infoClienteKAGUA@OK�";
						for (int i = 0; i < table.length; i++) {
							trama += (i > 0 ? "�" : "") 
									+ "Matricula No.:" + "�" + handleNull(table[i][0])+"�" + "2" +"@"
									+ "Municipio:" + "�" + handleNull(table[i][1])+"�" + "2" +"@"
									+ "Suscriptor:" + "�" + handleNull(table[i][2])+"�" + "2" +"@"
									+ "Ciclo:" + "�" + handleNull(table[i][3])+"�" + "2" +"@"
									+ "Direccion:" + "�" + handleNull(table[i][4])+"�" + "2" +"@"
									+ "Uso:" + "�" + handleNull(table[i][5])+"�" + "2" +"@"
									+ "Estrato:" + "�" + handleNull(table[i][6])+"�" + "2" +"@"
									+ "No. Medidor:" + "�" + handleNull(table[i][7])+"�" + "2" +"@"
									+ "Marca Medidor:" + "�" + handleNull(table[i][8])+"�" + "2" +"@"
									+ "Serv. Acueducto:" + "�" + handleNull(table[i][9])+"�" + "2" +"@"
									+ "Serv. Alcantarillado:" + "�" + handleNull(table[i][10])+"�" + "2" +"@"
									+ "Serv. Aseo:" + "�" + handleNull(table[i][11])+"�" + "2" +"@"
									+ "Recoleccion:" + "�" + handleNull(table[i][12])+"�" + "2" +"@"
									+ "Barrido:" + "�" + handleNull(table[i][13])+"�" + "2" +"@"
									+ "Saldo Actual:" + "�" + handleNull(table[i][14])+"�" + "2" +"@"
									+ "Saldo Financiado:" + "�" + handleNull(table[i][15])+"�" + "2" +"@"
									+ "Periodos Vencidos:" + "�" + handleNull(table[i][16])+"�" + "2" +"@"
									+ "Orden de Suspension:" + "�" + handleNull(table[i][17])+"�" + "2" +"@"
									+ "Orden de Reconexion:" + "�" + handleNull(table[i][18])+"�" + "2";
						}
						response.getWriter().print(trama);
					} else
						writer.print("infoClienteKAGUA@ERROR");
				} catch (Exception e) {
					writer.print("infoClienteKAGUA@ERROR");
				} finally {
					myQuery.closeConnection();
				}
			} else if(type.compareTo("infoDir") == 0){
				try {
					String direccion = request.getParameter("dir").trim();
					String municipio = request.getParameter("munip");
					String[][] table = myQuery.getInfoDirsKAGUA(direccion, municipio);
					String trama = "";
					if(table.length>0) {
					    trama += "InfoDir@OK�";
						for (int i = 0; i < table.length; i++) {
							trama += (i > 0 ? "�" : "") 
									+ table[i][0] + "@"
									+ municipio + "@"
									+ table[i][1];
						}
						response.getWriter().print(trama);
					} else
						writer.print("infoDir@ERROR");
				} catch (Exception e) {
					writer.print("infoDir@ERROR");
				} finally {
					myQuery.closeConnection();
				}
			} else if(type.compareTo("infoVecinos") == 0){
				try {
					final int dist = 5;
					String predio = request.getParameter("predio").trim();
					String municipio = request.getParameter("munip");
					if (!predio.equals("") && !municipio.equals("")) {
						String[][] table = myQuery.getInfoVecinosKAGUA(predio,
								municipio, dist);
						String trama = "";
						if (table.length > 0) {
							trama += "infoVecinos@OK�";
							for (int i = 0; i < table.length; i++) {
								if (i != dist)
									trama += (i == dist - 1 ? "�" : i > 0 ? "�": "") 
										+ "Predio:" + "�"+ handleNull(table[i][0]) + "�" + "2" + "@" 
										+ "Ruta:" + "�"	+ handleNull(table[i][1]) + "�" + "2" + "@" 
										+ "Nombre:" + "�" + handleNull(table[i][2]) + "�" + "2" + "@" 
										+ "Direccion:" + "�" + handleNull(table[i][3]) + "�" + "3";
							}
							response.getWriter().print(trama);
						} else
							response.getWriter().print("infoVecinos@ERROR");
					}else
						response.getWriter().print("infoVecinos@ERROR");
				} catch (Exception e) {
					response.getWriter().print("infoVecinos@ERROR");
				} finally {
					//myQuery.closeConnection();
				}				
			} else*/ if(type.compareTo("clean") == 0){
				myQuery.cleanTasks();
			}
		} catch (Exception ex) {
			writer.print("ERROR");
		}
	}
}
