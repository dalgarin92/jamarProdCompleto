package org.extreme.controlweb.server.geoservice;

import javax.ws.rs.core.MediaType;

import org.extreme.controlweb.server.util.ServerUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;


public class GeoServiceClient {

	/**
	 * @param args
	 */		

	
    private WebResource  webTarget;
    private Client client;
    private static final String BASE_URI = ServerUtils.GEOSERVICE_URL;

    public GeoServiceClient() {
        client =Client.create();
   
    }

    public String getRoute(String points,String index,String currentPosition, boolean manualSort) throws Exception {
       
        Form form = new Form();
        form.add("points",points);
        form.add("startpoint", index);
        form.add("currentposition", currentPosition);
        
        webTarget = client.resource(BASE_URI).path("route");
        
        if (manualSort) {
        	webTarget = client.resource(BASE_URI).path("route/manualsort");
		}
        
        ClientResponse response = webTarget.accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
        
        String result = response.getEntity(String.class);
        
        //String result = webTarget.type(MediaType.APPLICATION_JSON_TYPE)
        //		.post(String.class, form);
    
        return result;
   
}

    public void close() {
       // client.close();
    }
	

}
