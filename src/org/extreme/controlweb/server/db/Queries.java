package org.extreme.controlweb.server.db;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.AddressVO;
import org.extreme.controlweb.client.util.ClientUtils;
import org.extreme.controlweb.client.util.Improved;
import org.extreme.controlweb.server.util.ServerUtils;

import com.extreme.commons.vo.http.MyHttpResponse;
import com.extreme.commons.vo.utils.CommonsUtils;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Queries {

	private DBConnection db;
	private DBConnection db2;

	//private DBConnection dbCao;
	private static String ERRORMESSAGE_01x004="01x004 - Error al consultar en la base de datos";

	private String toDate(String date) {
		return "cast(to_timestamp('" + date
				+ "', 'YYYY/MM/DD HH24:MI') as timestamp without time zone)";
	}


	public Queries() throws SQLException, ClassNotFoundException {
		ServerUtils.initialize(ServerUtils.CURRENT_ENVIRONMENT);
		db = new DBConnection(ServerUtils.CONNECTION_STRING,
				ServerUtils.XCONTROL_DB, ServerUtils.DB_USER,
				ServerUtils.DB_PASSWD);
	}


	public Queries(boolean sw) throws SQLException, ClassNotFoundException {
		db = new DBConnection(ServerUtils.DB_DRIVER_ORACLE,
				ServerUtils.CONNECTION_STRING_ORACLE, ServerUtils.ORACLE_DB,
				ServerUtils.ORACLE_USER,
				ServerUtils.ORACLE_PASSWORD);
	}

	public String[][] getIconos() {
		String sql = "select path, descripcion, tipo, idicono from iconos order by descripcion";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getDatosDeUsuario(String user) {
		String sql = "SELECT U.NOMBRE, U.CLAVE, U.CARGO, P.PERFIL AS CODPERFIL, P.NOMBRE AS NOMBREPERFIL"
				+ " FROM \"Usuarios\" AS U INNER JOIN \"UsuariosPerfiles\" AS UP ON U.USUARIO = UP.USUARIO INNER JOIN"
				+ " \"Perfiles\" AS P ON UP.PERFIL = P.PERFIL WHERE U.USUARIO='"
				+ user + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getProcesos(String usuario) {
		String sql = "SELECT p.proceso, nombre, tipostrabajos, "
				+ "tiposrecursos, municipios, contratistas, "
				+ "manejaturnos, usacontratistas "
				+ "FROM \"Procesos\" p, \"UsuariosProcesos\" "
				+ "up where p.proceso = up.proceso and " + "up.usuario='"
				+ usuario + "' order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getProceso(String proceso) {
		String sql = "SELECT proceso, nombre, tipostrabajos, "
				+ "tiposrecursos, municipios, contratistas, "
				+ "manejaturnos, usacontratistas, solocentral,prioridades FROM \"Procesos\" "
				+ "WHERE proceso='" + proceso + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}


	public String[][] getProceso2(String proceso) {
		String sql = "SELECT proceso, nombre, tipostrabajos, "
				+ "tiposrecursos, municipios, contratistas, "
				+ "manejaturnos, usacontratistas, solocentral, "
				+ "manejalbs FROM \"Procesos\" "
				+ "WHERE proceso='" + proceso + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public boolean manejaTurnos(String proceso) throws SQLException {
		String sql = "select manejaturnos from \"Procesos\" where proceso='"
				+ proceso + "'";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1).equals("S");
		} else {
			return false;
		}
	}

	public boolean usaContratistas(String proceso) throws SQLException {
		String sql = "select usacontratistas from \"Procesos\" where proceso='"
				+ proceso + "'";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1).equals("S");
		} else {
			return false;
		}
	}

	public String[][] getContratistas(String proceso) throws Exception {
		String sql = "SELECT contratistas FROM \"Procesos\" "
				+ "WHERE proceso='" + proceso + "'";
		try{
			if (db.executeQuery(sql)) {
				db.next();
				String conts = db.getString(1);
				if(conts!=null&&!conts.equals(""))
				{
					sql = "SELECT contratista, nombre from \"Contratistas\" where contratista in "
							+ conts + " order by nombre";
					if (db.executeQuery(sql)) {
						return db.getTable();
					} else {
						return null;
					}
				}
				else{
					return new String[][]{};
				}
			} else {
				return new String[][]{};
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage(),e.getCause());
		}
	}



	public String[][] getMunicipiosDeProceso(String proceso)
			throws Exception {
		String sql = "SELECT municipios FROM \"Procesos\" " + "WHERE proceso='"
				+ proceso + "'";
		if (db.executeQuery(sql)) {
			db.next();
			String munips = db.getString(1);
			if (munips != null && !munips.equals("")) {
				sql = "SELECT municipio, nombre from \"Municipios\" where municipio in "
						+ munips + " order by nombre";
				if (db.executeQuery(sql)) {
					return db.getTable();
				} else {
					throw new Exception(sql);
				}
			}
			else{
				return new String[][]{};
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getRecursos(String cont, String estadoProg) {
		return getRecursos(cont, estadoProg, null);
	}

	public String[] getRecurso(String idrec, String cont) {
		ArrayList<String> recs = new ArrayList<String>();
		recs.add(idrec);
		String[][] res =  getRecursos(cont, null, recs);
		if(res != null) {
			return res[0];
		} else {
			return null;
		}
	}
	
	public String[] getRecursoUpdate(String idrec, String cont) {
		ArrayList<String> recs = new ArrayList<String>();
		recs.add(idrec);
		String[][] res =  getRecursosOld(cont, null, recs);
		if(res != null) {
			return res[0];
		} else {
			return null;
		}
	}
	
	public String[][] getRecursos(String cont, String estadoProg, ArrayList<String> idrecs) {

		// FALTA CONTROLAR LOS TURNOS, AQUI SIEMPRE TODOS ESTAN EN TURNO
		String sql = "select r.recurso, r.contratista, r.nomcont as nomcont, r.tiporecurso, tipr.nombre AS nombretiporecurso, r.nombre, "
				+ "r.estadomovil, r.latitud, r.longitud, to_char(r.fechagps,'" + ServerUtils.DATE_FORMAT
				+ "'), r.estadovehiculo, '0' as numtrabajos, cast('1' as varchar) as estadoprog, r.online, "
				+ "r.placa, r.marca, r.color, r.tipo,  r.nompropietario, r.direccion, r.municipio, r.departamento, r.idvehiculo, r.estadogps, r.tipo_modem, coalesce(horasatraso(r.fechagps, localtimestamp), '1000') as atraso "
				+ "from \"vwVehiculosAll\" r, \"TiposRecursos\" tipr where contratista= '" + cont
				+ "' and tipr.tiporecurso = r.tiporecurso  and ( r.estado='A' )";
		if (estadoProg != null) {
			sql += "and 1 = '" + estadoProg + "' ";
		}

		if (idrecs != null && idrecs.size() > 0) {
			sql += "and recurso in (";
			for (int i = 0; i < idrecs.size(); i++) {
				if (i != idrecs.size() - 1) {
					sql += "'" + idrecs.get(i) + "', ";
				} else {
					sql += "'" + idrecs.get(i) + "'";
				}
			}
			sql += ")";
		}
		sql += " order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			System.out.println(sql);
			return null;
		}
	}

	public String[][] getRecursosOld(String cont, String estadoProg, ArrayList<String> idrecs) {

		// FALTA CONTROLAR LOS TURNOS, AQUI SIEMPRE TODOS ESTAN EN TURNO
		String sql = "select r.recurso, r.contratista, r.nomcont as nomcont, r.tiporecurso, tipr.nombre AS nombretiporecurso, r.nombre, "
				+ "r.estadomovil, r.latitud, r.longitud, to_char(r.fechagps,'"
				+ ServerUtils.DATE_FORMAT
				+ "'), r.estadovehiculo, '0' as numtrabajos, cast('1' as varchar) as estadoprog, r.online, "
				+ "r.placa, r.marca, r.color, r.tipo,  r.nompropietario, r.direccion, r.municipio, r.departamento, r.idvehiculo, r.estadogps, r.tipo_modem, coalesce(horasatraso(r.fechagps, localtimestamp), '1000') as atraso "
				+ "from \"vwVehiculosAll\" r, \"TiposRecursos\" tipr where contratista= '"+ cont + "' and tipr.tiporecurso = r.tiporecurso ";
		if (estadoProg != null){
			sql += "and 1 = '" + estadoProg + "' ";
		}

		if (idrecs != null && idrecs.size() > 0) {
			sql += "and recurso in (";
			for (int i = 0; i < idrecs.size(); i++) {
				if(i != idrecs.size() - 1) {
					sql += "'" + idrecs.get(i) + "', ";
				} else {
					sql += "'" + idrecs.get(i) + "'";
				}
			}
			sql += ")";
		}
		sql += " order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else{
			System.out.println(sql);
			return null;
		}
	}

	public String[][] getRecursos(String cont, String estadoProg,
			ArrayList<String> idrecs,boolean manejaSup) {

		// FALTA CONTROLAR LOS TURNOS, AQUI SIEMPRE TODOS ESTAN EN TURNO
		String sql = "select r.recurso, r.contratista, r.nomcont as nomcont, r.tiporecurso, tipr.nombre AS nombretiporecurso, r.nombre, "
				+ "r.estadomovil, r.latitud, r.longitud, to_char(r.fechagps,'"
				+ ServerUtils.DATE_FORMAT
				+ "'), r.estadovehiculo, '0' as numtrabajos, cast('1' as varchar) as estadoprog, r.online, "
				+ "r.placa, r.marca, r.color, r.tipo,  r.nompropietario, r.direccion, r.municipio, r.departamento, r.idvehiculo, r.estadogps, r.tipo_modem, coalesce(horasatraso(r.fechagps, localtimestamp), '1000') as atraso ";
		if (manejaSup) {
			sql+=	" , r.supervisor, s.nombre, s.cedula";
		}
		sql+= " from \"vwVehiculos\" r, \"TiposRecursos\" tipr" ;
		if (manejaSup) {
			sql+=", \"Supervisores\" s where s.supervisor = r.supervisor and r.supervisor = '"+ cont + "' ";
		} else {
			sql+=	" where contratista= '"+ cont + "' and tipr.tiporecurso = r.tiporecurso ";
		}
		if (estadoProg != null) {
			// sql += "and estadoprog = '" + estadoProg + "' ";
			sql += "and 1 = '" + estadoProg + "' ";
		}
		if (idrecs != null && idrecs.size() > 0) {
			sql += "and recurso in (";
			for (int i = 0; i < idrecs.size(); i++) {
				if (i != idrecs.size() - 1) {
					sql += "'" + idrecs.get(i) + "', ";
				} else {
					sql += "'" + idrecs.get(i) + "'";
				}
			}
			sql += ")";
		}
		sql += " order by r.nombre, recurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getTrabajosNoUbicMaqTime(String proceso, String fecha) throws Exception{
		return getTrabajos("noUbic", proceso, null, null, fecha);
	}

	public String[][] getTrabajosNoAsigMaqTime(String proceso, String fecha) throws Exception{
		return getTrabajos("noAsig", proceso, null, null, fecha);
	}

	public String[][] getContratistasConEstado(String proceso) throws Exception {
		String sql = "SELECT contratistas FROM \"Procesos\" "
				+ "WHERE proceso='" + proceso + "'";
		try{
			if (db.executeQuery(sql)) {
				db.next();
				String conts = db.getString(1);
				if(conts!=null&&!conts.equals(""))
				{
					sql = "SELECT contratista, nombre from \"Contratistas\" where contratista in "
							+ conts + " and estado = 'A' order by nombre";
					if (db.executeQuery(sql)) {
						return db.getTable();
					} else {
						return null;
					}
				}
				else{
					return new String[][]{};
				}
			} else {
				return new String[][]{};
			}
		}
		catch (Exception e) {
			throw new Exception(e.getMessage(),e.getCause());
		}
	}

	public String[][] getTrabajos(String tipo, String proceso, String cont,
			String recurso, String dateProg) throws Exception {

		String tabla = "\"vwTrabajos\"";
		String today = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		if(!dateProg.equals(today)) {
			tabla = "\"get_vwTrabajos\"('" + dateProg + "'::date)";
		}

		String sql = "select c.trabajo, c.tipotrabajo, c.nomtipotrab, tr.recurso, tr.contratista, "
				+ "c.municipio, c.nommunip, c.direccion, c.ordenamiento, c.prioridad, "
				+ "c.estadotrabajo, c.inicioruta, c.latitud, c.longitud, c.proceso, c.atraso, to_char(c.fecha, 'yyyy/MM/dd hh24:mi') as fecha, "
				+ "to_char(c.fechainicio, 'yyyy/MM/dd hh24:mi') as fechainicio,"
				+ "to_char(c.fechadescarga, 'yyyy/MM/dd hh24:mi') as fechadescarga, c.codcli, "
				+ "c.nomcli, c.descripcion, c.horasatraso, c.icono, c.significado, c.icontree, "
				+ "c.numviaje, c.barrio, c.telefono, to_char(c.fechaentrega, 'yyyy/MM/dd') as fechaentrega,"
				+ "c.codjornada, c.nomjornada, c.horaentrega, "
				+ "case when c.fechaprecierre::date <= c.fechaentrega and  "
				+ "		c.fechaprecierre::time < c.horafinal "
				+ "		then 'S' else 'N' end as cerradoenjornada, "
				+ "case when (c.fechaentrega + c.horaentrega::time) > localtimestamp "
				+ "		then 'S' else 'N' end as pendienteatiempo, "
				+ "coalesce((select realizo from \"TPrecierres\" tpre "
				+ "where tpre.trabajo = c.trabajo and tpre.tipotrabajo = c.tipotrabajo order by fechaprecierre desc limit 1), 'N') as realizo,c.prioritario "
				+ ",getatraso(c.fecha, '"+proceso+"') as estadoatraso "
				+ "from "+ tabla +"  c "
				+ "left outer join \"TRecursos\" tr "
				+ "on (c.trabajo = tr.trabajo and c.tipotrabajo = tr.tipotrabajo) where proceso='"
				+ proceso + "' ";
		if (tipo != null && tipo.equals("noUbic")) {
			sql += "and (not validaNumero(latitud) or not validaNumero(longitud)  or"
					+ " latitud = '0' or longitud = '0') ";
		}
		if (tipo != null && tipo.equals("noAsig")) {
			sql += " and recurso is null and validaNumero(latitud) and validaNumero(longitud)"
					+ " and latitud <> '0'"
					+ " and longitud <> '0' ";
		}
		if (recurso != null) {
			sql += " and recurso = '" + recurso + "' and contratista ='"+cont+"'";
		}

		//sql += " order by prioritario, ordenamiento limit 5";
		sql += " order by  ordenamiento ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	@Improved
	public String [][] getTrabajosDeRecurso(String recurso, String contratista) {
		String sql = "select * from (select t.trabajo, t.tipotrabajo, t.latitud, t.longitud, t.estadotrabajo, t.inicioruta, t.prioridad, "
				+ "coalesce(to_char(t.fechaentrega, '"+ServerUtils.DATE_FORMAT+"'), 'X') as fe, coalesce(t.horaentrega, 'X') as he, t.fecha, "
				+ "t.fechaprecierre, ( "
				+ "select case "
				+ "when t.estadotrabajo = '50' then fecharechazo "
				+ "else null "
				+ "end from \"TRechazos\" trh "
				+ "where t.trabajo=trh.trabajo and t.tipotrabajo=trh.tipotrabajo "
				+ "order by fecharechazo desc limit 1) as fecharechazo, t.ordenamiento, t.fechainicio "
				+ "from \"vwTrabajos\" t , \"TRecursos\" tr "
				+ "where t.trabajo=tr.trabajo and t.tipotrabajo=tr.tipotrabajo "
				+ "and tr.recurso = '"+recurso+"' and tr.contratista = '"+contratista+"') x "
				+ "order by " +
				"coalesce(fechaprecierre, fecharechazo), " 
				+ 
				"fechainicio, " +
				//"estadotrabajo desc, " +
				//"inicioruta desc, prioridad desc, " +
				"ordenamiento";
				//+", fe, he, fecha ";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getPosiblesContDeTrabajo(String trabajo, String tipotrabajo)
			throws Exception {
		String sql = "select cont.contratista, cont.nombre "
				+ "from \"MTTContratistas\" c, \"Trabajos\" t, \"Contratistas\" cont "
				+ "where c.tipotrabajo = t.tipotrabajo and "
				+ "c.municipio = t.municipio and t.trabajo='" + trabajo
				+ "' and t.tipotrabajo='" + tipotrabajo + "' and cont.contratista = c.contratista";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception (sql);
		}
	}

	public boolean asignarTrabajoARecurso(String trabajo, String tipotrabajo,
			String recurso, String contratista) throws Exception {
		boolean sw;
		String sql = "INSERT INTO  \"TRecursos\"( "
				+ "trabajo, tipotrabajo, recurso, contratista, fechaasigna) "
				+ "VALUES ('" + trabajo + "', '" + tipotrabajo + "', '"
				+ recurso + "', '" + contratista + "', localtimestamp)";
		if (db.executeUpdate(sql) != 0) {
			sw = true;
		} else {
			sql = "UPDATE \"TRecursos\" SET recurso = '" + recurso
					+ "', contratista = '" + contratista
					+ "', fechaasigna = localtimestamp " + "WHERE trabajo='"
					+ trabajo + "' and tipotrabajo='" + tipotrabajo + "'";
			sw = db.executeUpdate(sql) != 0;
		}

		sql = "UPDATE \"Trabajos\" SET estadotrabajo = '00' WHERE trabajo='"
				+ trabajo + "' and tipotrabajo='" + tipotrabajo + "'";

		sw = sw && db.executeUpdate(sql) != 0;

		//sw = sw && asignarOTKAGUA(trabajo, tipotrabajo, recurso, contratista);

		return sw;
	}

	public boolean addNovedad(String trabajo, String tipotrabajo,
			String usuario, String tiponovedad, String recurso, String contratista) {
		if(recurso == null) {
			recurso = "null";
		} else {
			recurso = "'" + recurso + "'";
		}
		if(contratista == null) {
			contratista = "null";
		} else {
			contratista = "'" + contratista + "'";
		}

		String sql = "INSERT INTO \"TNovedades\"( "
				+ "fechanovedad, trabajo, tipotrabajo, usuario, tiponovedad, recurso, contratista) "
				+ "VALUES (localtimestamp, '" + trabajo + "', '" + tipotrabajo
				+ "', '" + usuario + "', '" + tiponovedad + "', "+recurso+", "+contratista+")";
		return db.executeUpdate(sql) != 0;
	}

	public boolean resetTrabajo(String trabajo, String tipotrabajo) {
		String sql = "UPDATE \"Trabajos\" set estadotrabajo = '00' where trabajo='"
				+ trabajo + "' and tipotrabajo='" + tipotrabajo + "'";
		return db.executeUpdate(sql) != 0;
	}

	public boolean changePrioridad(String trabajo, String tipotrabajo,
			String prioridad) {
		String sql = "UPDATE \"Trabajos\" set prioridad = '" + prioridad
				+ "' where trabajo='" + trabajo + "' and tipotrabajo='"
				+ tipotrabajo + "'";
		return db.executeUpdate(sql) != 0;
	}

	public String[][] getCoordsDeDireccion(String dir, String munip) {
		String sql = "SELECT latitud, longitud FROM \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + dir + "')) and municipio='" + munip + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String getOrigen(String direccion, String municipio)
			throws SQLException {
		String sql = "SELECT origen FROM \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + direccion
				+ "')) and municipio='" + municipio + "'";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				return db.getString(1);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public boolean updatePosicionTrabajos(String direccion, String municipio,
			String lat, String lon) {
		String sql = "update \"Trabajos\" set latitud = '" + lat
				+ "', longitud = '" + lon + "' where direccion = '" + direccion
				+ "' and municipio = '" + municipio
				+ "' and estadotrabajo <> '30' and estadotrabajo <> '40'";
		return db.executeUpdate(sql) != 0;
	}

	public boolean addDireccion(String direccion, String municipio, String lat,
			String lon) {
		String sql = "INSERT INTO \"EjesViales\"(direccion, municipio, "
				+ "latitud, longitud, xpos, ypos, origen, dirnoform) VALUES (obtienecabecera(normalizador('"
				+ direccion + "')), '" + municipio + "', '" + lat + "', '" + lon
				+ "', CAST(0 AS NUMERIC), CAST(0 AS NUMERIC), 'MANUAL', obtienecabecera(normalizador('"
				+ direccion + "')))";
		boolean sw = db.executeUpdate(sql) != 0;

		if (sw) {
			updatePosicionTrabajos(direccion, municipio, lat, lon);
		}

		return sw;
	}

	public boolean updateCoordsManuales(String direccion, String municipio,
			String lat, String lon) {
		String sql = "UPDATE \"EjesViales\" SET latitud = '"
				+ lat + "', longitud = '" + lon
				+ "', xpos = CAST(0 AS NUMERIC), ypos = CAST(0 AS NUMERIC) where "
				+ "dirnoform=obtienecabecera(normalizador('" + direccion
				+ "')) and municipio='" + municipio + "'";
		boolean sw = db.executeUpdate(sql) != 0;

		if (sw) {
			updatePosicionTrabajos(direccion, municipio, lat, lon);
		}

		return sw;
	}

	public boolean updateCoordsDeTrabajo(String trabajo, String tipotrabajo,
			String lat, String lon) {
		String sql = "UPDATE \"Trabajos\" SET latitud = '" + lat + "', longitud = '"
				+ lon + "', xpos = CAST(0 AS NUMERIC), ypos = CAST(0 AS NUMERIC) where trabajo='" + trabajo
				+ "' and tipotrabajo='" + tipotrabajo + "'";
		return db.executeUpdate(sql) != 0;
	}

	public boolean addClienteUbicacion(String cliente, String direccion, String lat,
			String lon) {
		String sql = "update \"ClienteUbicacion\" set latitud='" + lat + "', longitud='" + lon + "' where "
				+ "idcliente='" + cliente + "' and direccion='" + direccion + "' ";
		if (!(db.executeUpdate(sql) > 0)){
			String sql2="INSERT INTO \"ClienteUbicacion\" (idcliente, direccion, latitud, longitud) "
					+ "values ('" + cliente + "','" + direccion + "','" + lat + "','" + lon + "') ";
			return db.executeUpdate(sql2) > 0;
		}else{
			return true;
		}
	}

	public String[][] getAllTrabajoDirNoUbic(String dir, String munip)
			throws Exception {
		String sql = "select trabajo, tipotrabajo from \"Trabajos\" where estadotrabajo = '00' and direccion = '"
				+ dir + "' and municipio = '" + munip + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getTiposTrabajoDeProceso(String proceso)
			throws Exception {
		String sql = "SELECT tipostrabajos FROM \"Procesos\" "
				+ "WHERE proceso='" + proceso + "'";
		try{
			if (db.executeQuery(sql)) {
				db.next();
				String tpostrab = db.getString(1);
				if (tpostrab!=null&&!tpostrab.equals("")) {
					sql = "SELECT tipotrabajo, nombre from \"TiposTrabajos\" where tipotrabajo in "
							+ tpostrab + " order by nombre";
					if (db.executeQuery(sql)) {
						return db.getTable();
					} else {
						throw new Exception(sql);
					}
				}
				else{
					return new String[][]{};
				}
			} else{
				throw new Exception(sql);
			}
		}catch(Exception e){
			throw new Exception(sql);
		}
	}

	public String[][] getInfoClientes() throws Exception {
		String sql = "SELECT cliente, nombre, direccion, municipio, telefono from \"Clientes\" order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}

	}
	public String[][] getInfoDrivers() throws Exception {
		String sql = "SELECT codigo, nombre, cedula, celular  from \"Conductores\" where estado='A' order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}

	}
	public String getTiposRecursoAsRaw(String proceso) throws Exception {
		String sql = "SELECT tiposrecursos FROM \"Procesos\" WHERE proceso='"
				+ proceso + "'";
		try {
			if (db.executeQuery(sql)) {
				db.next();
				return db.getString(1);
			} else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			throw new Exception(sql);
		}
	}

	public String[][] getTiposRecurso(String proceso) throws Exception {
		String sql = "";
		try {
			String tposrec = getTiposRecursoAsRaw(proceso);
			if (tposrec != null && !tposrec.equals("")) {
				sql = "SELECT tiporecurso, nombre from \"TiposRecursos\" where tiporecurso in "
						+ tposrec + " order by nombre";
				if (db.executeQuery(sql)) {
					return db.getTable();
				} else {
					throw new Exception(sql);
				}
			} else {
				return new String[][] {};
			}
		} catch (Exception e) {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoRecursos(String usuario) throws Exception {
		String sql = "select tiposrecursos, contratistas "
				+ "from \"Procesos\" p, \"UsuariosProcesos\" up "
				+ "where p.proceso = up.proceso and up.usuario='"+usuario+"'";
		/*String sql = "SELECT contratistas, tiposrecursos FROM \"Procesos\" "
			+ "WHERE proceso='" + proceso + "'";*/

		if (db.executeQuery(sql)) {
			String[][] infoProc = db.getTable();

			String tr = concat(infoProc, 0);
			String conts = concat(infoProc, 1);

			sql = "SELECT r.recurso, r.contratista, c.nombre as nomcont, "
					+ "r.tiporecurso, tr.nombre as nomtporec, r.nombre, r.usuariomovil, r.clavemovil "
					+ "FROM \"Recursos\" r, \"Contratistas\" c, \"TiposRecursos\" tr "
					+ "WHERE r.contratista = c.contratista and r.tiporecurso = tr.tiporecurso "
					+ "and r.estado = 'A' and r.contratista in " + conts + " and r.tiporecurso in " + tr;
			if (db.executeQuery(sql)) {
				return db.getTable();
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	private String concat(String[][] strings, int col) {
		String res = ServerUtils.decodeSymbols(strings[0][col].substring(0,
				strings[0][col].length() - 1));
		for (int i = 1; i < strings.length; i++) {
			res += ServerUtils.decodeSymbols(strings[i][col]).replaceFirst("\\(", ",")
					.substring(0, ServerUtils.decodeSymbols(strings[i][col]).length() - 1);
		}
		return res + ")";
	}

	public String[][] getInfoTrabajos(String proceso) {
		String sql = "SELECT t.trabajo, t.tipotrabajo as codtpotrab, tt.nombre as nomtpotrab, "
				+ "t.cliente as codcliente, c.nombre as nomcliente, t.direccion, "
				+ "t.municipio as codmunip, m.nombre as nommunip, to_char(t.fecha, '" + ServerUtils.DATE_FORMAT + "') "
				+ "as fecha, t.prioridad, t.descripcion, t.inicioruta, calculaatraso(t.fecha,  localtimestamp) as atraso "
				+ "from \"Trabajos\" t, \"TiposTrabajos\" tt, \"Municipios\" m left outer join \"Clientes\" c on (t.cliente = c.cliente) "
				+ "where t.estadotrabajo <> '30' and t.estadotrabajo <> '40' "
				+ "and t.tipotrabajo = tt.tipotrabajo "
				+ "and t.municipio = m.municipio and t.proceso='" + proceso + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[] getLatitudLontudFromDireccion(String dir, String municipio) throws Exception{

		String sql = "select latitud, longitud from \"EjesViales\" where dirnoform = obtienecabecera(normalizador('"+dir+"')) and municipio='"+municipio+"'";
		if (db.executeQuery(sql)) {
			String[][] table = db.getTable();
			if (table.length > 0) {
				return table[0];
			}else{
				return null;
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String[] getEjeVialFromGeoCoding(String[] latlon, String dir, String municipio, String codMunicipio){

		if (latlon == null || latlon.length == 0) {

			Client client = Client.create();
			WebResource webTarget = client.resource(ServerUtils.GEOSERVICE_URL).path("roadaxis/getlatlon").queryParam("dir", dir).queryParam("mun", municipio).queryParam("codmun", codMunicipio);

			ClientResponse response = webTarget.accept("application/json").get(ClientResponse.class);

			String output = response.getEntity(String.class);

			Type type = new TypeToken<MyHttpResponse<AddressVO>>() {
			}.getType();

			MyHttpResponse<AddressVO> responseGson = new GsonBuilder().create().fromJson(output, type);

			if (responseGson.getEst() == 200) {
				if (responseGson.getDatos().size() > 0) {
					AddressVO address = responseGson.getDatos().get(0);
					latlon = new String[2];
					latlon[0] = String.valueOf(address.getLocation()[0]);
					latlon[1] = String.valueOf(address.getLocation()[1]);
					if (addDireccion(dir, codMunicipio, latlon[0], latlon[1])) {
						return latlon;
					}
				}else{
					latlon = new String[2];
					latlon[0] = "";
					latlon[1] = "";
				}
			}else{
				latlon = new String[2];
				latlon[0] = "";
				latlon[1] = "";
			}

			System.out.println(output);
		}
		return latlon;
	}

	public String getNombreMunicipio(String codMunicipio) throws Exception{

		String sql = "select nombre from \"Municipios\" where municipio = '"+codMunicipio+"'";
		if (db.executeQuery(sql)){
			String[][] table=db.getTable();
			if(table!=null && table.length>0){

				return table[0][0];
			}else{
				return null;
			}

		} else {
			throw new Exception(sql);
		}
	}

	public String addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String conductor, String placa, 
			String numentregassap) throws Exception {
		String lat, lon;
		String trabajo=codtrab+" ("+numviaje+")";

		String codMunicipio=String.format("%05d", Integer.parseInt(munip));
		placa=placa.replaceAll("-", "").trim();

		String[] latlon = getLatitudLontudFromDireccion(dir, codMunicipio);

		if (latlon == null || latlon.length == 0) {
			String municipio = getNombreMunicipio(codMunicipio);
			if(municipio!=null){
				latlon = getEjeVialFromGeoCoding(latlon, dir, municipio, codMunicipio);
			}

		}

		if(latlon == null || latlon.length == 0){
			lat="null";
			lon="null";
		}else{
			lat="'"+latlon[0]+"'";
			lon="'"+latlon[1]+"'";
		}

		String cel;
		if(telefono!=null){
			cel="'"+telefono+"'";
		}else{
			cel="null";
		}


		String insertsql = "INSERT INTO \"Trabajos\"(trabajo, tipotrabajo, proceso, "
				+ "municipio, cliente, usuario, direccion, descripcion, "
				//+ "ordenamiento, fecha, prioridad, latitud, longitud, "
				+ "fecha, prioridad, latitud, longitud, "
				+ "numviaje, solicitante, barrio, telefono, fechaentrega, "
				+ "jornadaentrega, horaentrega,codconductor,placa, celular, numerotrabajo, numentregasap) VALUES ('"
				+ trabajo
				+ "','"
				+ codttrab
				+ "','"
				+ proceso
				+ "','"
				+ codMunicipio
				+ "','"
				+ codcli
				+ "','"
				+ usuario
				+ "','"
				+ dir
				+ "','"
				+ desc
				//+ "', 0,"
				+ "', "
				+ toDate(fecha)
				+ "::date, '"
				+ prioridad
				+ "', "
				+ ""+lat+", "+lon+", '"
				+ numviaje
				+ "', '"
				+ nomcli
				+ "', '"
				+ barrio
				+ "', '"
				+ telefono
				+ "', "
				+ "localtimestamp"/*toDate(fechaentrega)*/
				+ "::date, "
				+ Integer.parseInt(jornada)
				+ "," + /*toDate(hora)*/"'00:00:00'" + "::time,'"+conductor+ "','"+placa+ "', "+cel+", '"+codtrab+"', " +
						"'"+numentregassap+"')";

		String updatesql = "Update \"Trabajos\" set municipio='" + codMunicipio
				+ "',cliente='" + codcli + "',usuario='" + usuario + "',direccion='" + dir + "',descripcion='"
				+ desc + "', "
				+ "latitud="+lat+",longitud="+lon+",numviaje='"
				+ numviaje + "',solicitante='" + nomcli + "',barrio='"
				+ barrio + "',telefono='" + telefono + "',codconductor="+conductor+
				",placa='"+placa+ "', celular="+cel+", numerotrabajo='"+codtrab+"', numentregasap='"+numentregassap+"'  " +
						"where trabajo='"+trabajo+"'";

		String selectsql = "select * from \"Trabajos\" where trabajo ='"+trabajo+"'";

		return executeValidatedInsert(selectsql, updatesql, insertsql);
	}

	public boolean closeConnection(){
		return db.closeAll();
	}

	public boolean updateTrabajo(String codtrab, String codttrab,
			String numviaje, String codcli, String nomcli, String fecha,
			String dir, String munip, String barrio, String telefono,
			String prioridad, String desc, String fechaentrega, String jornada,
			String hora, String usuario) throws Exception {
		String sql = "update \"Trabajos\" set cliente = '" + codcli + "', direccion='" + dir
				+ "', municipio = '" + munip + "', fecha = "
				+ toDate(fecha) + ", prioridad='" + prioridad + "', descripcion='"
				+ desc + "', usuario='" + usuario + "', latitud = (select latitud from \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + dir
				+ "')) and municipio='" + munip + "'), longitud = (select longitud from \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + dir
				+ "')) and municipio='" + munip + "'), " + "numviaje='"
				+ numviaje + "', solicitante='" + nomcli + "', barrio='"
				+ barrio + "', telefono='" + telefono + "', fechaentrega="
				+ toDate(fechaentrega) + ", jornadaentrega='" + jornada + "', horaentrega='"
				+ hora + "'  " + "where trabajo='" + codtrab
				+ "' and tipotrabajo='" + codttrab + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteTrabajo(String id, String tipo) throws Exception {
		//Asegurarse de colocar el on delete restrict en esta tabla,
		//para que solo borre los trabajos que esten recien hechos
		String sql = "delete from \"Trabajos\" where trabajo='" + id
				+ "' and tipotrabajo='" + tipo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteRecurso(String id, String cont) throws Exception {
		//Asegurarse de colocar el on delete restrict en esta tabla,
		//para que solo borre los recursos que esten recien hechos
		String sql = "delete from \"Recursos\" where recurso='" + id
				+ "' and contratista='" + cont + "'";
		if (db.executeUpdate(sql) == 0) {
			sql = "update \"Recursos\" set estado = 'I' where recurso='" + id
					+ "' and contratista='" + cont + "'";
			if (db.executeUpdate(sql) > 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		} else {
			return true;
		}
	}

	public boolean setOrdenamiento(String trabajo, String tipotrabajo, int numOrdenamiento) throws Exception {
		String sql = "update \"Trabajos\" set ordenamiento=" + numOrdenamiento
				+ " where trabajo='" + trabajo + "' and tipotrabajo='"
				+ tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[] getCoordsDeRecurso(String recurso, String cont) {
		String sql = "select latitud, longitud from \"vwVehiculos\" where recurso = '"
				+ recurso + "' and contratista = '" + cont + "'";
		if (db.executeQuery(sql)) {
			db.next();
			try {
				return new String[] { db.getString(1), db.getString(2) };
			} catch (SQLException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	public boolean clearPathStart(String recurso, String cont) throws Exception {
		String sql = "update \"Trabajos\" t set inicioruta='N' where exists "
				+ "(select 'X' from \"TRecursos\" tr where t.trabajo = tr.trabajo "
				+ "and t.tipotrabajo = tr.tipotrabajo and tr.recurso='"
				+ recurso + "' and tr.contratista = '" + cont + "')";
		if (db.executeUpdate(sql) > 0){
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean setPathStart(String trabajo, String tipotrabajo)
			throws Exception {
		String sql = "update \"Trabajos\" set inicioruta = 'S' where trabajo = '"
				+ trabajo + "' and tipotrabajo = '" + tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getDatosDeEmpresa() throws Exception {
		String sql = "select codigo, nombre, adminlocal, licencia, nombrebd, " +
				"usuariobd, passwdbd from \"Empresas\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getDatosDeEmpresa(String codempresa) throws Exception {
		String sql =  "select codigo, nombre, adminlocal, licencia, nombrebd, " +
				"usuariobd, passwdbd from \"Empresas\" where codigo = '"+codempresa+"'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public int getRecursosCount() throws Exception {
		String sql = "select count(*) from \"Recursos\"";
		if (db.executeQuery(sql)) {
			db.next();
			return Integer.parseInt(db.getString(1));
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoMunicipios() throws Exception {
		String sql = "select municipio, nombre from \"Municipios\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addMunicipio(String id, String nombre) throws Exception {
		String sql = "insert into \"Municipios\"(municipio, nombre) VALUES ('"
				+ id + "', '" + nombre + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateMunicipio(String id, String nombre) throws Exception {
		String sql = "update \"Municipios\" set nombre = '" + nombre
				+ "' where municipio = '" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteMunicipio(String id) throws Exception {
		String sql = "delete from \"Municipios\" where municipio='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoTiposRec() throws Exception {
		String sql = "select tiporecurso, nombre, capacidad from \"TiposRecursos\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addTipoRec(String id, String nombre, String capacidad) throws Exception {
		String sql = "INSERT INTO \"TiposRecursos\"(tiporecurso, nombre, capacidad) VALUES ('"
				+ id + "', '" + nombre + "', '" + capacidad + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateTipoRec(String id, String nombre, String capacidad) throws Exception {
		String sql = "UPDATE \"TiposRecursos\" SET nombre='" + nombre
				+ "', capacidad='" + capacidad + "' WHERE tiporecurso='" + id
				+ "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteTipoRec(String id) throws Exception {
		String sql = "delete from \"TiposRecursos\" where tiporecurso='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoTiposTrab() throws Exception {
		String sql = "select tipotrabajo, nombre, repcampo, maxduracion, duracionpromedio, duracionestimada,idmensaje "
				+ " from \"TiposTrabajos\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addTipoTrab(String id, String nombre, boolean repcampo) throws Exception {
		String sql = "insert into \"TiposTrabajos\"(tipotrabajo, nombre, repcampo) VALUES ('"
				+ id + "', '" + nombre + "', '" + (repcampo ? "S" : "N") + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateTipoTrab(String id, String nombre, boolean repcampo) throws Exception {
		String sql = "update \"TiposTrabajos\" set nombre = '" + nombre
				+ "', repcampo='" + (repcampo ? "S" : "N") + "' where tipotrabajo = '" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteTipoTrab(String id) throws Exception {
		String sql = "delete from \"TiposTrabajos\" where tipotrabajo='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoContratistas() throws Exception {
		String sql = "select contratista, nombre from \"Contratistas\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addContratista(String id, String nombre) throws Exception {
		String sql = "insert into \"Contratistas\"(contratista, nombre) VALUES ('"
				+ id + "', '" + nombre + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateContratista(String id, String nombre) throws Exception {
		String sql = "update \"Contratistas\" set nombre = '" + nombre
				+ "' where contratista = '" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteContratista(String id) throws Exception {
		String sql = "delete from \"Contratistas\" where contratista='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addCliente(String id, String nombre, String direccion,
			String municipio, String telefono) throws Exception {
		String sql = "INSERT INTO \"Clientes\"(cliente, municipio, "
				+ "nombre, direccion, telefono) VALUES ('" + id + "', '"
				+ municipio + "', '" + nombre + "', '" + direccion + "', '"
				+ telefono + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public boolean addDriver(String id, String nombre, String cedula, String celular) throws Exception {
		String sql = "INSERT INTO \"Conductores\"(codigo, nombre,cedula, celular, estado) "
				+ " VALUES ('" + id + "', '" + nombre + "', '" + cedula + "', '"+celular+"', 'A')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateCliente(String id, String nombre, String direccion,
			String municipio, String telefono) throws Exception {
		String sql = "UPDATE \"Clientes\" SET municipio='" + municipio
				+ "', nombre='" + nombre + "', direccion='" + direccion
				+ "', telefono='" + telefono + "' WHERE cliente='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}


	public boolean updateDriver(String id, String nombre, String cedula, String celular) throws Exception {
		String sql = "UPDATE \"Conductores\" SET codigo='" + id
				+ "', nombre='" + nombre + "', cedula='" + cedula+ "', celular='"+celular+"' WHERE codigo='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateGeocercasJamar(String idgeocerca,String idingreso, String  idegreso, String descripcion, String idvehiculo, String numgeocerca, String latitud, String longitud, String radio, String estado)throws Exception {

		boolean resp2=false;
		String select="select descripcion from geocercas where idgeocerca="+idgeocerca;
		String insert="insert into geocercas values ("+idgeocerca+", '"+idingreso+"', '"+idegreso+"', '"+descripcion+"','"+idvehiculo+"',"+numgeocerca+",'"+latitud+"','"+longitud+"',"+radio+",'"+estado+"')";
		String update = "update geocercas set idingreso='"+idingreso+"', idegreso='"+idegreso+"', descripcion='"+descripcion+"', idvehiculo='"+idvehiculo+"', numgeocerca="+numgeocerca+", latitud='"+latitud+"', longitud='"+longitud+"', radio="+radio+", estado='"+estado+"' where idgeocerca="+idgeocerca;
		String resp=executeValidatedInsert(select, update, insert);
		if(resp.equalsIgnoreCase("t")){
			resp2=true;
		}
		return resp2;

	}


	public boolean deleteCliente(String id) throws Exception {
		String sql = "delete from \"Clientes\" where cliente='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public boolean deleteDriver(String id) throws Exception {
		String sql = "update \"Conductores\" set estado='I' where codigo='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getBitacoraDeTrabajo(String trabajo, String tipotrabajo) throws Exception {
		String sql = "select fecha, usuario, texto, recurso, nomcont from \"vwNovedades\" "
				+ "where trabajo = '" + trabajo	+ "' and tipotrabajo='"
				+ tipotrabajo + "' order by fn desc";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoMRechazo() throws Exception {
		String sql = "select mrechazo, nombre from \"MRechazos\"";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoProgramaciondeFlota() throws Exception {
		String sql = "select fecha, placa as placa, idvehiculo as codplaca,  nombremuelle as muelle, mu.muelle as codmuelle, c.nombre as conductor, codigo as codconductor,  m.nombre as destino, m.iddestino as coddestino, horaentrada::time, horasalida::time, horaretorno::time, observacion  " +
				"from \"ProgramaciondeFlotas\" pr " +
				"inner join vehiculos v using (idvehiculo) " +
				"inner join \"Muelle\" mu using (muelle) " +
				"inner join \"Destinos\" m on (m.iddestino=destino) " +
				"inner join \"Conductores\" c on  (codigo=conductor)";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public String[][] getInfoMuelles() throws Exception {
		String sql = "select muelle, nombremuelle from \"Muelle\" where estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public void EstablacerConexionCentral()throws Exception{
		db2= new DBConnection(ServerUtils.CONNECTION_STRING_GEOC,
				ServerUtils.XCONTROL_DB_GEOC, ServerUtils.DB_USER_GEOC,
				ServerUtils.DB_PASSWD_GEOC);
	}

	public String[][] getGeocercasCentral() throws Exception {
		String sql = "select idgeocerca, idingreso, idegreso, descripcion, idvehiculo, numgeocerca, latitud, longitud, radio, estado from geocercas where estado='A' and idvehiculo like '%JAMAR%'";
		EstablacerConexionCentral();
		if (db2.executeQuery(sql)) {
			return db2.getTable();
		} else {
			throw new Exception(sql);
		}
	}




	public String[][] getInfoDestinos() throws Exception {
		String sql = "select iddestino, nombre from \"Destinos\" where estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}






	public String[][] getInfoProgramaciondeFlotaFiltro(String fecha, String vehiculo, String muelle, String conductor, String destino, String horaentrada, String horasalida, String horaretorno) throws Exception {
		System.out.println("Comenzando ejecucion 3");
		String sql = "select fecha, placa as placa, idvehiculo as codplaca,  nombremuelle as muelle, mu.muelle as codmuelle, c.nombre as conductor, codigo as codconductor,  m.nombre as destino, m.iddestino as coddestino, horaentrada::time, horasalida::time, horaretorno::time, observacion  " +
				"from \"ProgramaciondeFlotas\" pr " +
				"inner join vehiculos v using (idvehiculo) " +
				"inner join \"Muelle\" mu using (muelle) " +
				"inner join \"Destinos\" m on (m.iddestino=destino) " +
				"inner join \"Conductores\" c on  (codigo=conductor) ";
		System.out.println("sql: "+sql);
		if(!fecha.equals("") && fecha!=null || !vehiculo.equals("")&& vehiculo!=null ||  !muelle.equals("")&& muelle!=null || !conductor.equals("")&& conductor!=null ||  !destino.equals("")&& destino!=null ||   !horaentrada.equals("")&& horaentrada!=null ||  !horasalida.equals("")&& horasalida!=null ||  !horaretorno.equals("")&& horaretorno!=null ){
			sql+="where ";
		}

		System.out.println("sql: "+sql);
		int c=0;


		if(!fecha.equals("") && fecha!=null){

			sql+="fecha='"+fecha+"'";
			c++;
		}

		System.out.println("sql: "+sql);
		if(!vehiculo.equals("")&& vehiculo!=null){
			if(c==0){
				sql+="idvehiculo='"+vehiculo+"'";
			}else{
				sql+="and idvehiculo='"+vehiculo+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!muelle.equals("")&& muelle!=null){
			if(c==0){
				sql+="mu.muelle='"+muelle+"'";
			}else{
				sql+="and mu.muelle='"+muelle+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!conductor.equals("")&& conductor!=null){
			if(c==0){
				sql+="codigo='"+conductor+"'";
			}else{
				sql+="and codigo='"+conductor+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!destino.equals("")&& destino!=null){
			if(c==0){
				sql+="m.iddestino='"+destino+"'";
			}else{
				sql+="and m.iddestino='"+destino+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!horaentrada.equals("")&& horaentrada!=null){
			if(c==0){
				sql+="horaentrada='"+horaentrada+"'";
			}else{
				sql+="and horaentrada='"+horaentrada+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!horasalida.equals("")&& horasalida!=null){
			if(c==0){
				sql+="horasalidao='"+horasalida+"'";
			}else{
				sql+="and horasalida='"+horasalida+"'";
			}
			c++;
		}

		System.out.println("sql: "+sql);
		if(!horaretorno.equals("")&& horaretorno!=null){
			if(c==0){
				sql+="horaretorno='"+horaretorno+"'";
			}else{
				sql+="and horaretorno='"+horaretorno+"'";
			}
			c++;
		}


		System.out.println("sql: "+sql);

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public boolean addMuelle(String nombre, String desc)throws Exception{
		String sql = "insert into \"Muelle\"(muelle, nombremuelle, estado ) VALUES ('"
				+nombre + "', '" + desc + "', 'A')";

		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addDestino(String iddest, String nombre)throws Exception{
		String sql = "insert into \"Destinos\"(iddestino, nombre, estado ) VALUES ('"
				+iddest + "', '" + nombre + "', 'A')";

		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateMuelle(String muelle, String nombre) throws Exception {
		String sql = "update \"Muelle\" set nombremuelle = '" + nombre
				+ "' where muelle = '" + muelle + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateDestino(String iddest, String nombre) throws Exception {
		String sql = "update \"Destinos\" set nombre = '" + nombre
				+ "' where iddestino = '" + iddest + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean caractColumnas(String campo, String h, String w) throws Exception{
		String sql = "update \"CaractColumnasPantallaConfig\" set width = '" + w
				+ "', height="+h+"  where idcolumna = '" + campo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean caractPantalla(String tipoletra, String tamletra, String subr,String  negrita,String cursiva ) throws Exception{
		String sql = "update \"CaractetisticasGenerales\" set tamletra= '"+tamletra+"', tipoletra='"+tipoletra+"', subrayado="+subr+", negrita="+negrita+", cursiva="+cursiva+" where idcargeneral='01'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	/*
	 * idcargeneral character varying(10) not null,
tamletra character varying(5) not null,
tipoletra character varying(20) not null,
Subrayado
negrita
cursiva
	 * 
	 * */


	public boolean addMRechazo(String id, String nombre) throws Exception {
		String sql = "insert into \"MRechazos\"(mrechazo, nombre) VALUES ('"
				+ id + "', '" + nombre + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addRegProgramacion(String fecha, String vehiculo, String muelle, String conductor, String destino, String horaentrada, String horasalida, String horaretorno, String observacion) throws Exception {
		String sql = "insert into \"ProgramaciondeFlotas\"(fecha, idvehiculo, muelle, conductor, destino, horaentrada, horasalida, horaretorno, observacion) VALUES ('"
				+ fecha + "', '" + vehiculo + "', '" + muelle + "', '" + conductor + "', '" + destino + "', '" + horaentrada + "', '" + horasalida + "', '" + horaretorno + "', '" + observacion + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public boolean updateMRechazo(String id, String nombre) throws Exception {
		String sql = "update \"MRechazos\" set nombre = '" + nombre
				+ "' where mrechazo = '" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateRegProg(String fecha, String vehiculo, String muelle, String conductor, String destino, String horaentrada, String horasalida, String horaretorno, String observacion) throws Exception
	{
		String sql = "update \"ProgramaciondeFlotas\" set muelle = '" +muelle+ "', conductor='" +conductor+ "', observacion='" +observacion+ "',destino='" +destino+ "', horaentrada='" +horaentrada+ "', horasalida='" +horasalida+ "', horaretorno='"+horaretorno
				+ "' where fecha = '" + fecha + "' and idvehiculo= '"+ vehiculo+ "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] registarMovimiento(String fecha, String vehiculo,
			String hora) throws Exception{
		// TODO Auto-generated method stub

		String sql = "select regmovimientos from regmovimientos('"+fecha+"', '"+vehiculo+"',  '"+hora+"') ";
		if (db.executeQuery(sql) ){
			return  db.getTable();

		}    else{
			throw new Exception(sql);
		}
	}



	public String[][] obtenerCampo(String fecha, String vehiculo) throws Exception{
		// TODO Auto-generated method stub

		String sql = "select obtenercampo from obtenercampo('"+fecha+"', '"+vehiculo+"') ";
		if (db.executeQuery(sql) ){
			return  db.getTable();

		}    else{
			throw new Exception(sql);
		}
	}

	//upper(v.direccion||', '||v.municipio||', '||v.departamento)
	public String[][] controldeFlotas(String fecha) throws Exception{
		// TODO Auto-generated method stub

		String sql = "select upper(m.muelle), upper(placa), upper(c.nombre),  upper(c.celular),  upper(mu.nombre) as destino,  horaentrada::time ,  horaentradareal::time, " +
				"horasalida::time ,  horasalidareal::time,  horaretorno::time,  horaretornoreal::time,  upper(ev.descripcion) as estadoactual,  prg.ubicacion as ubicacion, " +
				"(select count(fechaentrega)"+
				"from \"Trabajos\" t "+
				"INNER JOIN \"TRecursos\" tr ON (t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo) "+
				"where t.fechaentrega::date='"+fecha+"' and tr.recurso=v.placa)as programadas, "+
				"(select  count(fechaprecierre) "+
				"from \"Trabajos\" t "+
				"INNER JOIN \"TRecursos\" tr ON (t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo) "+
				"where t.fechaentrega::date='"+fecha+"' and tr.recurso=v.placa) as realizadas, "+
				"(select upper(t.trabajo||', '||t.direccion) " +
				" from \"Trabajos\" t inner join ( " +
				"select * from \"TPrecierres\" where recurso=v.placa  and contratista = '10' and fechaprecierre::date = '"+fecha+"'::date order by idprecierre desc limit 1) tp " +
				" on t.trabajo = tp.trabajo and t.tipotrabajo = tp.tipotrabajo) as ultimaentrega,  "+
				"(select upper(trabajo||', '||direccion) " +
				" from \"Trabajos\" where (trabajo, tipotrabajo) = (select t.trabajo, t.tipotrabajo" +
				" from \"Trabajos\" t inner join \"TRecursos\" tr on t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo" +
				" where  estadotrabajo in ('00', '10', '20') and fechaentrega='"+fecha+"' and recurso = v.placa and contratista = '10'" +
				" order by ordenamiento limit 1)) as proxentrega, "+
				" upper(prg.observacion) "+
				"from \"ProgramaciondeFlotas\" prg " +
				"inner join \"Muelle\" m on (m.muelle=prg.muelle) " +
				"inner join vehiculos v on (v.idvehiculo=prg.idvehiculo) " +
				"inner join \"Conductores\" c on (c.codigo= prg.conductor) " +
				"inner join \"Destinos\" mu on (mu.iddestino=prg.destino) " +
				"left join eventosdesc ev on (ev.evento=v.ultimoevento) " +
				"where fecha='"+fecha+"' "+
				"order by upper(m.muelle), upper(placa) ";
		if (db.executeQuery(sql) ){
			return  db.getTable();

		}    else{
			throw new Exception(sql);
		}
	}

	public String[][] widthCampo(String campo) throws Exception{
		String sql="select width, height from \"CaractColumnasPantallaConfig\" where idcolumna='"+campo+"'";
		if (db.executeQuery(sql) ){
			return  db.getTable();

		}    else{
			throw new Exception(sql);
		}

	}

	public String[][] CaracteristicasPant() throws Exception{
		String sql="select tamletra, tipoletra, subrayado, negrita, cursiva from \"CaractetisticasGenerales\" where idcargeneral='01'";
		if (db.executeQuery(sql) ){
			return  db.getTable();

		}    else{
			throw new Exception(sql);
		}
	}






	public boolean registarMovimientos(String fecha, String vehiculo,
			String hora) throws Exception{
		// TODO Auto-generated method stub

		String sql = "select regmovimientos from regmovimientos('"+fecha+"', '"+vehiculo+"',  '"+hora+"') ";
		if (db.executeQuery(sql) ){
			return  true;

		}    else{
			throw new Exception(sql);
		}
	}

	public boolean deleteMRechazo(String id) throws Exception {
		String sql = "delete from \"MRechazos\" where mrechazo='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteMuelles(String muelle) throws Exception {
		String sql = "update \"Muelle\" set estado='I' where muelle='" + muelle + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteDestinos(String iddest) throws Exception {
		String sql = "update \"Destinos\"set estado='I' where iddestino='" + iddest + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}




	public boolean deleteRegProg(String fecha, String vehiculo) throws Exception {
		System.out.println("Realizando eliminacion de registro.. 3");
		String sql = "delete from \"ProgramaciondeFlotas\" " +
				" where fecha = '" + fecha + "' and idvehiculo= '"+ vehiculo+ "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getUserProfiles() throws Exception {
		String sql = "select perfil, nombre from \"Perfiles\" where perfil <> 'superadmin'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoUsuarios() throws Exception {
		String sql = "select u.usuario, u.nombre, u.clave, u.cargo, p.perfil, p.nombre from \"Usuarios\" u, \"Perfiles\" p, \"UsuariosPerfiles\" up where p.perfil = up.perfil and u.usuario = up.usuario";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addUsuario(String id, String nombre, String pass,
			String cargo, String idperfil) throws Exception {
		String sql = "INSERT INTO \"Usuarios\"(usuario, nombre, clave, cargo) VALUES ('"
				+ id + "', '" + nombre + "', '" + pass + "', '" + cargo + "')";
		if (db.executeUpdate(sql) > 0) {
			sql = "insert into \"UsuariosPerfiles\"(usuario, perfil) values ('"
					+ id + "','" + idperfil + "')";
			if (db.executeUpdate(sql) > 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateUsuario(String id, String nombre, String pass,
			String cargo, String idperfil) throws Exception {
		String sql = "update \"Usuarios\" set nombre = '" + nombre
				+ "', clave='" + pass + "', cargo='" + cargo
				+ "' where usuario = '" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			sql = "update \"UsuariosPerfiles\" set perfil='" + idperfil
					+ "' where usuario='" + id + "'";
			if (db.executeUpdate(sql) > 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteUsuario(String id) throws Exception {
		String sql = "delete from \"Usuarios\" where usuario='" + id + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoPermisos(String user) throws Exception {
		String sql = "select up.usuario, p.proceso, p.nombre from \"UsuariosProcesos\" up, "
				+ "\"Procesos\" p where p.proceso = up.proceso and  up.usuario='"
				+ user + "' order by p.nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deletePermiso(String user, String proceso) throws Exception {
		String sql = "delete from \"UsuariosProcesos\" where usuario='" + user
				+ "' and proceso='" + proceso + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addPermiso(String proceso, String user) throws Exception {
		String sql = "insert into \"UsuariosProcesos\"(usuario, proceso) values ('"
				+ user + "', '" + proceso + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else{
			System.out.println(sql);
			throw new Exception(sql);
		}
	}

	public String[][] getAllTTrab() throws Exception {
		String sql = "select tipotrabajo, nombre from \"TiposTrabajos\" order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAllCont() throws Exception {
		String sql = "select contratista, nombre from \"Contratistas\" order by nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoMunipTTrab(String munip) throws Exception {
		String sql = "select mtt.municipio, mtt.tipotrabajo, tt.nombre from "
				+ "\"MTTrabajos\" mtt, \"TiposTrabajos\" tt where mtt.tipotrabajo = tt.tipotrabajo and mtt.municipio='"
				+ munip + "' order by tt.nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addMunTTrab(String munip, String ttrab) throws Exception {
		String sql = "insert into \"MTTrabajos\"(municipio, tipotrabajo) values ('"
				+ munip + "', '" + ttrab + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteMunTTrab(String munip, String ttrab) throws Exception {
		String sql = "delete from \"MTTrabajos\" where municipio = '" + munip
				+ "' and tipotrabajo='" + ttrab + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception("Error al eliminar la relaci\u00f3n con el tipo de trabajo y el municipio");
		}
	}

	public String[][] getInfoMunipTTrabCont(String munip, String ttrab)
			throws Exception {
		String sql = "select mtt.municipio, mtt.tipotrabajo, mtt.contratista, c.nombre from "
				+ "\"MTTContratistas\" mtt, \"Contratistas\" c where mtt.contratista = c.contratista and mtt.municipio='"
				+ munip
				+ "' and mtt.tipotrabajo='"
				+ ttrab
				+ "' order by c.nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addMunTTrabCont(String munip, String ttrab, String cont)
			throws Exception {
		String sql = "insert into \"MTTContratistas\"(municipio, tipotrabajo, contratista) values ('"
				+ munip + "', '" + ttrab + "', '" + cont + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteMunTTrabCont(String munip, String ttrab, String cont) throws Exception {
		String sql = "delete from \"MTTContratistas\" where municipio = '"
				+ munip + "' and tipotrabajo='" + ttrab
				+ "' and contratista = '" + cont + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception("Error al borrar el contratista del tipo de trabajo en este municipio");
		}
	}

	public String[][] getInfoProcesos() throws Exception {
		String sql = "select p.proceso, nombre, usacontratistas, manejaturnos, " +
				"solocentral, (select valorfinal from \"TematicosProcesos\" tp " +
				"where p.proceso=tp.proceso and icono='ATRASO1') as bflagf, " +
				"(select valorfinal from \"TematicosProcesos\" tp where " +
				"p.proceso=tp.proceso and icono='ATRASO2') as oflagf  " +
				"from \"Procesos\" p";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoOpcPrecierre() throws Exception {
		String sql = "select o.opcion, o.tipotrabajo, tt.nombre, o.descripcion from "
				+ "\"OpcionesPrecierres\" o, \"TiposTrabajos\" tt "
				+ "where o.tipotrabajo = tt.tipotrabajo";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addOpcPre(String opcion, String codttrab, String nombre) throws Exception {
		String sql = "insert into \"OpcionesPrecierres\"(opcion, tipotrabajo, descripcion) values ('"
				+ opcion + "', '" + codttrab + "', '" + nombre + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateOpcPre(String opcion, String codttrab, String nombre) throws Exception {
		String sql = "update \"OpcionesPrecierres\" set descripcion='"
				+ nombre + "' where tipotrabajo = '" + codttrab + "' and opcion = '" + opcion + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean reporderOpcionesCategoria(String asString, String asString2) throws Exception {
		String sql = "update \"ListasPrecierres\" set orden='"
				+ asString2+ "' where id_lista = '" + asString + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteOpcPre(String opcion, String ttrab) throws Exception {
		String sql = "delete from \"OpcionesPrecierres\" where "
				+ "tipotrabajo = '" + ttrab + "' and opcion = '" + opcion + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoCodPrecierre(String opcpre, String tipotrabajo) throws Exception {
		String sql = "select idcodigopre, opcion, tipotrabajo, descripcion, codigo "
				+ "from \"CodigosPrecierres\" where opcion=" + opcpre
				+ " and tipotrabajo='" + tipotrabajo + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addCodPre(String opcion, String codttrab, String codigo,
			String nombre) throws Exception {
		String sql = "insert into \"CodigosPrecierres\"(opcion, tipotrabajo, codigo, descripcion) "
				+ "values (" + opcion + ", '" + codttrab + "', '" + codigo + "', '" + nombre + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteCodPre(String idcodpre) throws Exception {
		String sql = "delete from \"CodigosPrecierres\" where idcodigopre = " + idcodpre;
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean closeWork(String trabajo, String tipotrabajo) throws Exception {
		String sql = "update \"Trabajos\" set estadotrabajo = '30',  fechaprecierre = LocalTimeStamp where trabajo='"
				+ trabajo + "' and tipotrabajo='" + tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			sql = "insert into \"TPrecierres\"(trabajo, tipotrabajo, fechaprecierre, modo) values ('"
					+ trabajo + "', '" + tipotrabajo + "', localtimestamp, 'DESDE XCONTROLWEB')";
			if (db.executeUpdate(sql) > 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public boolean setResourceMode(String recurso, String contratista,
			String online) throws Exception {
		String sql = "update \"Recursos\" set online='" + online
				+ "' where recurso='" + recurso + "' and contratista='"
				+ contratista + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAllTRec() throws Exception {
		String sql = "select tiporecurso, nombre, capacidad from \"TiposRecursos\"";
		System.out.println(sql);
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAllMunip() throws Exception {
		String sql = "select municipio, nombre from \"Municipios\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getCommandList(String recurso, String contratista) throws Exception{
		String sql = "select operacion, idota from comandosota where idota in "+
				"(select idota from comando_tipo where sintaxis in "+
				"(select sintaxis from modem_tipo where tipo_modem in ( select tipo_modem from vehiculos where recurso='"+recurso+"' and contratista='"+contratista+"'))) order by idota";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoTrackRecurso(String recurso, String contratista,
			String desde, String hasta, ArrayList<String> eventos)
					throws Exception {
		String e = "";
		if (eventos.size() != 0) {
			String eventsString = "";
			for (String string : eventos) {
				if (!eventsString.equals("")) {
					eventsString += ",";
				}

				eventsString += "'" + string + "'";
			}
			e = " and evento in (" + eventsString + ")";
		} else {
			e = " and latitud is not null and longitud is not null and latitud <> 'null' and longitud <> 'null' and latitud not like '%0.0' and longitud not like '%0.0'";
		}
		String infoVeh[] = null;
		try {
			infoVeh = getVehiculoDeRecurso(recurso, contratista);
		} catch (Exception e1) {
			throw new Exception("El recurso no tiene vehiculo asignado.");
		}
		String sql = "select  latitud, longitud, fecha, velocidad || ' Km/h', direccion,  municipio,departamento, descripcion, '"
				+ infoVeh[1]
						+ "' from \"vwEventos\" "
						+ "where fecha between '"
						+ desde
						+ "' and  '"
						+ hasta
						+ "' and idvehiculo = '" + infoVeh[0] + "' " + e;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[] getVehiculoDeRecurso(String recurso, String contratista) throws Exception{
		String sql = "select idvehiculo, placa from vehiculos where recurso='"
				+ recurso + "' and contratista='" + contratista + "'";
		if (db.executeQuery(sql)) {
			return db.getTable()[0];
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoGeocercasRecurso(String idveh, String desde, String hasta) throws Exception{


		/*
		 * String sql =
		 * "select * from \"vwEventosGeocercas\" v where v.idvehiculo = '" +
		 * idveh + "' and fecha between '" + desde + "' and '" + hasta + "'";
		 */

		String sql = "select *  from \"vwEventosGeocercas\"  v where v.placa = (SELECT placa from vehiculos veh where veh.idvehiculo='"
				+ idveh + "') and fecha between '" + desde + "' and '" + hasta + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoOdometroRecurso(String idveh, String desde, String hasta) throws Exception{

		String sql = "select * from calculaodom('" + idveh + "', '" + desde
				+ "', '" + hasta + "')";
		if (db.executeQuery(sql)) {
			String table[] = db.getColumn(1);
			String res[][] = new String[table.length][2];
			for (int i = 0; i < table.length; i++) {
				String split[] = table[i].split("@");
				res[i][0] = split[0];
				res[i][1] = split[1];
			}
			return res;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getEventos() throws Exception{

		String sql = "select evento, descripcion from eventosdesc order by cast (evento as int)";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String getContratistasProceso(String proceso) throws Exception{

		String sql = "select contratistas from \"Procesos\" where proceso='"+proceso+"'";
		if (db.executeQuery(sql)){
			db.next();
			return db.getString(1);
		} else {
			throw new Exception(sql);
		}
	}

	
	public String[][] getInfoVehiculoProceso(String proceso) throws Exception {
		String sql = "select placa, " + // 0
				"latitud, " + // 1
				"longitud, " + // 2
				"fechagps, " + // 3
				"velocidad, " + // 4
				"direccion, " + // 5
				"municipio, " + // 6
				"departamento, " + // 7
				"nomevento "
				+ "from \"vwVehiculos\" v  where (v.placa is not NULL AND v.placa <> 'NA') AND contratista in "
				+ getContratistasProceso(proceso) + "";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	
	public String[][] getInfoVehiculoProcesoOld(String proceso) throws Exception{
		String sql = "select placa, " +//0
				"latitud, " +//1
				"longitud, " +//2
				"fechagps, " +//3
				"velocidad, " +//4
				"direccion, " +//5
				"municipio, " +//6
				"departamento, " +//7
				"nomevento "+
				"from \"vwVehiculos\" where contratista in "+getContratistasProceso(proceso)+"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoRecursosProceso(String proceso) throws Exception{
		String sql = "select recurso, " +//0
				"r.contratista, " +//1
				"c.nombre, " +//2
				"r.tiporecurso, " +//3
				"tr.nombre, " +//4
				"r.nombre, " +//5
				"usuariomovil, " +//6
				"clavemovil, "//7
				+ "codigosap, "+  //8
				" h.nombre, h.codigo "+ //9
				" from \"Recursos\" r inner join \"Contratistas\" c using (contratista) inner join \"TiposRecursos\" tr using (tiporecurso) "
				+ "left join \"Horarios\" h on (h.codigo=r.horario) "
				+" where r.contratista in "+getContratistasProceso(proceso)+"";

		System.out.println(sql);
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoRecursosProcesoConCarro(String proceso) throws Exception{
		String sql = "select recurso, " +//0
				"r.contratista, " +//1
				"c.nombre, " +//2
				"r.tiporecurso, " +//3
				"tr.nombre, " +//4
				"r.nombre, " +//5
				"usuariomovil, " +//6
				"clavemovil "+  //7
				" from \"Recursos\" r inner join \"Contratistas\" c using (contratista) inner join \"TiposRecursos\" tr using (tiporecurso) inner join vehiculos v using (recurso) where r.contratista in "+getContratistasProceso(proceso)+"";
		System.out.println(sql);
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAllTModems() throws Exception {
		String sql = "select tipo_modem, tipo_modem from modem_tipo order by tipo_modem" ;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAllPropietarios() throws Exception {
		String sql = "select p.idpropietario, u.nombre from propietarios p, "
				+ "\"Usuarios\" u where p.idusuario = u.usuario order by u.nombre" ;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoVehs() throws Exception {
		String sql = "select idvehiculo, placa, modelo, marca, estado, tipo, "
				+ "imei, min, puk, idpropietario, nompropietario, tipo_modem, "
				+ "recurso, nombre, contratista, nomcont, color from \"vwVehiculos\" "
				+ "where idvehiculo is not null order by idvehiculo";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoVehsProceso(String proceso) throws Exception {
		String sql = "select idvehiculo, placa, modelo, marca, estado, tipo, imei, min, puk, idpropietario, nompropietario, tipo_modem," +
				"recurso, v.nombre, contratista, nomcont, color from \"vwVehiculos\" v, \"Procesos\" " +
				"where instr(contratistas, ''''||v.contratista||'''')>0 and proceso='"+proceso+"' and idvehiculo is not null";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addVeh(String idcont, String idrec, String idveh,
			String placa, String tipo, String model, String marca,
			String codprop, String tipomodem, String imei, String min,
			String puk, String activo) throws Exception {
		String sql = "INSERT INTO vehiculos( "
				+ "placa, idvehiculo, nromovil, modelo, marca, fechaingreso, "
				+ "estado, tipo, "
				//"idpropietario, " +
				+"tipo_modem, modem_imei, "
				+ "modem_min, modem_puk, modem_estado, recurso, contratista) "
				+ "VALUES ('" + placa + "', '" + idveh + "', '-1', '" + model + "', '"
				+ marca + "', localtimestamp, '" + (activo.equals("true") ? "Y" : "N")
				+ "', '" + tipo + "', "
				//+"'" + codprop + "', "
				+"'" + tipomodem + "', '"
				+ imei + "', '" + min + "', '" + puk + "', 'Y', '" + idrec + "', '"
				+ idcont + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean updateVeh(String idcont, String idrec, String idveh,
			String placa, String tipo, String model, String marca,
			String codprop, String tipomodem, String imei, String min,
			String puk, String activo) throws Exception {
		String sql = "UPDATE vehiculos SET " + "placa = '" + placa
				+ "', modelo = '" + model + "', marca = '" + marca + "', "
				+ "estado = '" + (activo.equals("true") ? "Y" : "N")
				+ "', tipo = '" + tipo + "'"
				//+", idpropietario = '" + codprop+ "',"
				+ ", tipo_modem = '" + tipomodem + "', modem_imei = '" + imei
				+ "', " + "modem_min = '" + min + "', modem_puk = '" + puk
				+ "', ip = " + (activo.equals("true") ? "ip" : "null")
				+ " where idvehiculo = '" + idveh + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public boolean deleteVeh(String idVeh) throws Exception {
		String sql = "delete from vehiculos where idvehiculo = '" + idVeh + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	private boolean addProcRelation(String proceso, String id, String field) throws Exception {
		String sql = "select " + field + " from \"Procesos\" where proceso='"
				+ proceso + "'";
		String res = "";
		if (db.executeQuery(sql)) {
			res = ServerUtils.decodeSymbols(db.getTable()[0][0]);
		} else {
			throw new Exception(sql);
		}

		System.out.println("res: "+res);
		if(res.equals("")){
			res="('"+id+"')";
		}
		else{
			res = res.replace(")", "");
			res += ",'" + id + "')";
		}
		res = res.replaceAll("'", "''");

		sql = "update \"Procesos\" set " + field + " = '" + res
				+ "' where proceso = '" + proceso + "'";

		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	private boolean deleteProcRelation(String proceso, String id, String field) throws Exception {
		String sql = "select " + field + " from \"Procesos\" where proceso='"
				+ proceso + "'";
		String res = "";
		if (db.executeQuery(sql)) {
			res = ServerUtils.decodeSymbols(db.getTable()[0][0]);
		} else {
			throw new Exception(sql);
		}

		if(res.equals("('"+id+"')")){
			res="";
		}else
		{
			int index = res.indexOf("'" + id + "'");

			if (res.substring(index + ("'" + id + "'").length(),
					index + ("'" + id + "'").length() + 1).compareTo(",") == 0) {
				res = res.replace("'" + id + "',", "");
			} else {
				res = res.replace(",'" + id + "'", "");
			}

			res = res.replaceAll("'", "''");
		}




		sql = "update \"Procesos\" set " + field + " = '" + res
				+ "' where proceso = '" + proceso + "'";

		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}

	}

	public boolean addProcTTrab(String proceso, String id) throws Exception {
		return addProcRelation(proceso, id, "tipostrabajos");
	}

	public boolean addProcTRec(String proceso, String id) throws Exception {
		return addProcRelation(proceso, id, "tiposrecursos");
	}

	public boolean addProcMunip(String proceso, String id) throws Exception {
		return addProcRelation(proceso, id, "municipios");
	}

	public boolean addProcCont(String proceso, String id) throws Exception {
		return addProcRelation(proceso, id, "contratistas");
	}

	public boolean deleteProcTTrab(String proceso, String id) throws Exception {
		return deleteProcRelation(proceso, id, "tipostrabajos");
	}

	public boolean deleteProcTRec(String proceso, String id) throws Exception {
		return deleteProcRelation(proceso, id, "tiposrecursos");
	}

	public boolean deleteProcMunip(String proceso, String id) throws Exception {
		return deleteProcRelation(proceso, id, "municipios");
	}

	public boolean deleteProcCont(String proceso, String id) throws Exception {
		return deleteProcRelation(proceso, id, "contratistas");
	}

	public boolean updateProcess(String proceso, String name, String isturn, String iscont,
			String iscentral, int bflag, int oflag, int rflag) throws Exception {
		String sql = "update \"Procesos\" set nombre = '" + name
				+ "', manejaturnos='" + isturn + "', usacontratistas='"
				+ iscont + "', solocentral='" + iscentral + "' where proceso='"
				+ proceso + "'";
		if (db.executeUpdate(sql) > 0) {
			sql = "insert into \"TematicosProcesos\"(icono, proceso, valorinicial, "
					+ "valorfinal, significado) values ('ATRASO1', '" + proceso
					+ "', 0, " + bflag + ", 'De 0 a " + bflag + " horas'), "
					+ "('ATRASO2', '" + proceso + "', " + (bflag + 1) +", "
					+ oflag + ", 'De "+(bflag + 1)+" a " + oflag + " horas'), "
					+ "('ATRASO3', '" + proceso + "', " + (oflag + 1) +", "
					+ rflag + ", 'De "+(oflag + 1)+" a " + rflag + " horas')";
			if (db.executeUpdate(sql) > 0) {
				return true;
			}
			else {
				sql = "update \"TematicosProcesos\" set valorinicial = 0, valorfinal ="
						+ bflag	+ ", significado = 'De 0 a " + bflag + " horas' "
						+ "where proceso='" + proceso + "' and icono='ATRASO1';";

				sql += "update \"TematicosProcesos\" set valorinicial = "+(bflag + 1)+", valorfinal ="
						+ oflag	+ ", significado = 'De "+(bflag + 1)+" a " + oflag + " horas' "
						+ "where proceso='" + proceso + "' and icono='ATRASO2';";

				sql += "update \"TematicosProcesos\" set valorinicial = "+(oflag + 1)+", valorfinal ="
						+ rflag	+ ", significado = 'De "+(oflag + 1)+" a " + rflag + " horas' "
						+ "where proceso='" + proceso + "' and icono='ATRASO3';";

				if (db.executeUpdate(sql) > 0) {
					return true;
				} else {
					throw new Exception(sql);
				}
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoTrabajosPrecerradosFechas(String proceso,
			String desde, String hasta, String ttrab, String recurso, String contratista,
			String estadoB, boolean fCreacion)
					throws Exception {
		String sql = "select trabajo, tipotrabajo, nombre, cliente, solicitante, direccion, "
				+ "municipio, nommunip, fecha, atraso, prioridad, inicioruta, descripcion, "
				+ "fechapre, estadotrabajo, fechaent, horaent, telefono, barrio, numviaje, "
				+ "jornada, (tur).actual, (tur).asignado, (tur).tiempoest , fechaini, fechadescarga, tejecucion "
				+ "from (select t.trabajo, t.tipotrabajo, tt.nombre, t.cliente, t.solicitante, "
				+ "t.direccion, t.municipio, m.nombre as nommunip, to_char(t.fecha, '"
				+ ServerUtils.DATE_FORMAT+ "') as fecha, calculaatraso(fecha, fechaprecierre) as atraso, "
				+ "t.prioridad, t.inicioruta, t.descripcion, to_char(fechaprecierre, '"
				+ ServerUtils.DATE_FORMAT+ "') as fechapre, t.estadotrabajo, to_char(t.fechaentrega, '"
				+ ServerUtils.DATE_FORMAT.split(" ")[0]+ "') as fechaent, to_char(t.horaentrega, '"
				+ ServerUtils.DATE_FORMAT.split(" ")[1]+ "') as horaent, telefono, barrio, numviaje, "
				+ "j.nombre || ' [' || j.horainicial || ' - ' || j.horafinal || ']' as jornada, "
				+ "getturnotrabajo(t.trabajo, t.tipotrabajo) as tur  ,to_char(t.fechainicio,'yyyy/MM/dd hh12:mi:ss PM') as fechaini "
				+ ", to_char(t.fechadescarga,'yyyy/MM/dd hh12:mi:ss PM') as fechadescarga, "
				//+ "extract(minute from (fechaprecierre - t.fechainicio))::int as tejecucion "
				+ "age(fechaprecierre , t.fechainicio) as tejecucion "
				//+ "replace (replace (age(fechaprecierre , t.fechainicio), 'mons', 'meses' ), 'days', 'dias') as tejecucion "
				+ "from \"Trabajos\" t inner join \"TiposTrabajos\" tt using (tipotrabajo) "
				+ "inner join \"Municipios\" m on (m.municipio=t.municipio) "
				+ "inner join \"Jornadas\" j on (j.idjornada = t.jornadaentrega) where ";

		if(recurso != null && contratista != null) {
			sql +="exists (select * from \"TPrecierres\" tp "
					+ "where t.trabajo=tp.trabajo and t.tipotrabajo=tp.tipotrabajo and "
					+ "tp.fechaprecierre between '" + desde + "' and '" + hasta + "'"
					+" and recurso = '"+recurso+"' and contratista='"+contratista+"' )";
		} else if (estadoB.equals("0")){
			sql += "t.estadotrabajo <> '30' and t.estadotrabajo <> '40' and ";
			if(fCreacion) {
				sql += "t.fecha between '" + desde + "' and '" + hasta + "'";
			} else {
				sql += "t.fechaentrega between '" + desde + "' and '" + hasta + "'";
			}
		}else if (estadoB.equals("1")){
			sql += "exists (select * from \"TPrecierres\" tp "
					+ "where t.trabajo=tp.trabajo and t.tipotrabajo=tp.tipotrabajo ";
			if(fCreacion) {
				sql += ") and t.fecha between '" + desde + "' and '" + hasta + "'";
			} else {
				sql += "and tp.fechaprecierre between '" + desde + "' and '" + hasta + "' )";
			}
		}else if (estadoB.equals("2")) {
			sql += "t.fecha between '" + desde + "' and '" + hasta + "'";
		} else if (estadoB.equals("3")) {
			sql += "t.estadotrabajo = '50'";
		}

		sql += " and t.proceso='"+proceso+"' and t.tipotrabajo = '" + ttrab + "' order by fechaprecierre desc) a";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoTrabajosPrecerrados(String trabajo,String tipotrabajo) throws Exception {
		String sql = "select cp.idprecierre, tp.trabajo, tp.tipotrabajo, to_char(tp.fechaprecierre, '"
				+ ServerUtils.DATE_FORMAT
				+ "'), cp.opcionprecierre, cp.codigoprecierre, ip.descripcion "
				+ "from \"TPrecierres\" tp, \"vwCodigosPrecierres\" cp, \"ItemsPrecierres\" ip "
				+ "where tp.idprecierre = cp.idprecierre and cp.item = ip.item and ip.tipotrabajo=tp.tipotrabajo "
				+ "and tp.tipotrabajo='" + tipotrabajo + "' and tp.trabajo='"
				+ trabajo + "' order by cp.idprecierre desc, ip.orden, cp.orden";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getTrabajo(String idTrabajo, String idTipoTrabajo)
			throws Exception {
		String sql = "select t.trabajo, t.tipotrabajo, t.direccion, t.municipio, t.descripcion, "
				+ "t.estadotrabajo, t.nomrecurso, t.nomcontratista, "
				+ "t.inicioruta, t.prioridad, t.atraso, to_char(t.fecha, '"
				+ ServerUtils.DATE_FORMAT
				+ "'), "
				+ "to_char(t.fechainicio, '"
				+ ServerUtils.DATE_FORMAT
				+ "'), to_char(t.fechadescarga, '"
				+ ServerUtils.DATE_FORMAT
				+ "'), nomcli || ' (' || codcli || ')', t.telefono, "
				+ "t.barrio, t.numviaje, to_char(t.fechaentrega, 'yyyy/MM/dd'), t.nomjornada, t.horaentrega "
				+ ", (extract (epoch from ((to_char(t.fechaprecierre, 'yyyy/MM/dd HH24:MI:SS')::timestamp) - (to_char(t.fechainicio, 'yyyy/MM/dd HH24:MI:SS')::timestamp)))/60)::integer , COALESCE(x.entregofactura,'N') as entregofactura, COALESCE(x.motivonoentrega,'') as motivonoentrega  "
				+ "from \"vwTrabajosAll\" t, \"Trabajos\" x  where t.trabajo = '"
				+ idTrabajo
				+ "' and t.tipotrabajo = '" + idTipoTrabajo + "'"
				+" AND t.trabajo = x.trabajo ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public String[] getComandoOta(String recurso, String contratista, String idota) throws Exception {
		String sql = "select (select idvehiculo from vehiculos where recurso='"
				+ recurso + "' and contratista='" + contratista
				+ "') as idvehiculo, comando from comando_tipo where sintaxis = "
				+ "(select sintaxis from vehiculos v, modem_tipo mt "
				+ "where v.tipo_modem =  mt.tipo_modem and recurso='" + recurso
				+ "' and contratista = '" + contratista + "') and idota='"
				+ idota + "'";
		if (db.executeQuery(sql)) {
			return db.getTable()[0];
		} else {
			throw new Exception(sql);
		}
	}

	/*public String[][] getGeocercas(String idvehiculo) throws Exception {
		String sql = "select numgeocerca, descripcion, cast(radio as character varying(10)) || ' m.' ,"+
						"coalesce(latitud, '0') as latitud, coalesce(longitud, '0') as longitud"+
						" from geocercas where idvehiculo = '"+idvehiculo+"' order by numgeocerca";
		if (db.executeQuery(sql))
			 return db.getTable();
		else
			throw new Exception(sql);
	}*/

	public String[][] getGeocercas(String recurso, String contratista) throws SQLException {
		String sql = "select idgeocerca, idingreso, idegreso, descripcion, idvehiculo, numgeocerca, "
				+ "latitud, longitud, radio, estado from active_geofences((select idvehiculo from vehiculos where recurso='"
				+ recurso + "' and contratista='" + contratista + "')) where estado = 'A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		}
		return null;
	}


	public boolean insertGeo(int numgeocerca, String descripcion, String idvehiculo, double latitud, double longitud, int radio) throws Exception {
		int idingreso = numgeocerca * 2 + 23;
		int idegreso = idingreso + 1;
		String sql = "insert into geocercas (idingreso, idegreso, descripcion, idvehiculo, numgeocerca, latitud, longitud, radio ) values" +
				"('"+idingreso +"', '"+idegreso+"','"+descripcion+"','"+idvehiculo+"','"+numgeocerca+"','"+latitud+"'," +
				"'"+longitud+"','"+radio+"')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public int getGeoCount(String idveh) throws Exception {
		String sql = "select count(*) from geocercas where idvehiculo='"+idveh+"'";
		if (db.executeQuery(sql)) {
			return Integer.parseInt(db.getTable()[0][0]);
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getGeocercasRec(String idveh) throws Exception {
		String sql = "select idgeocerca, descripcion from geocercas where idvehiculo = '"
				+ idveh + "' order by descripcion";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getMinInfoRecs(String proc, String id, String codTurno) throws Exception {

		String trecs = getTiposRecursoAsRaw(proc);

		String sql = "select * from (select r.recurso, nombre, estadomovil, CASE "
				+ "WHEN (localtimestamp - v.ultimaactualizacion) < '00:05:00'::interval THEN 'OK'::character varying(20) "
				+ "ELSE 'NOTOK'::character varying(20) " + "END AS estadogps, '1'::text as turno, r.contratista "
				+ "from \"Recursos\" r left outer join vehiculos v on (r.recurso = v.recurso and r.contratista = v.contratista) "
				+ "where r.contratista = '" + id + "' and (r.estado='A') and (v.idvehiculo is null or v.estado = 'Y') and tiporecurso in "
				+ trecs
				/*
				 * + " and exists (" + "		select 'X' " +
				 * "		from \"Trabajos\" t inner join \"TRecursos\" tr on "
				 * +
				 * "			(t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo) "
				 * + "		where t.viajefinalizado = 'N' " +
				 * "			and tr.recurso = r.recurso and tr.contratista = r.contratista"
				 * + " 	) "
				 */
				+ ") c where turno = '" + codTurno + "' order by nombre, recurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			System.out.println(sql);
		}
		return null;
	}
	public String[][] getMinInfoRecsOld(String proc, String id, String codTurno)
			throws Exception {

		String trecs = getTiposRecursoAsRaw(proc);

		String sql = "select * from (select r.recurso, nombre, estadomovil, CASE "
				+ "WHEN (localtimestamp - v.ultimaactualizacion) < '00:05:00'::interval THEN 'OK'::character varying(20) "
				+ "ELSE 'NOTOK'::character varying(20) "
				+ "END AS estadogps, '1'::text as turno, r.contratista "
				+ "from \"Recursos\" r left outer join vehiculos v on (r.recurso = v.recurso and r.contratista = v.contratista) "
				+ "where r.contratista = '"
				+ id
				+ "' and (v.idvehiculo is null or v.estado = 'Y') and tiporecurso in "
				+ trecs
				/*+ " and exists ("
				+ "		select 'X' "
				+ "		from \"Trabajos\" t inner join \"TRecursos\" tr on "
				+ "			(t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo) "
				+ "		where t.viajefinalizado = 'N' "
				+ "			and tr.recurso = r.recurso and tr.contratista = r.contratista"
				+ " 	) " */
				+") c where turno = '" + codTurno + "' order by nombre, recurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			System.out.println(sql);
		}
		return null;
	}

	public String[][] getMinInfoRecs(String proc, String id, String codTurno,boolean manejaSuperv)
			throws Exception {

		String trecs = getTiposRecursoAsRaw(proc);

		String sql = "select * from (select r.recurso, r.nombre, estadomovil, CASE "
				+ "WHEN (localtimestamp - v.ultimaactualizacion) < '00:05:00'::interval THEN 'OK'::character varying(20) "
				+ "ELSE 'NOTOK'::character varying(20) "
				+ "END AS estadogps, '1'::text as turno, r.contratista" ;
		if (manejaSuperv) {
			sql+=", r.supervisor, s.nombre as nomsuper, s.cedula ";
		}
		sql+= " from \"Recursos\" r" ;
		if (manejaSuperv) {
			sql+=" inner join \"Supervisores\" s on (s.supervisor = r.supervisor) ";
		}

		sql+= " left outer join vehiculos v on (r.recurso = v.recurso and r.contratista = v.contratista) where" ;
		if (manejaSuperv) {
			sql+=" r.supervisor = '";
		} else {
			sql+=" r.contratista = '";
		}
		sql+= id;
		sql+="' and (v.idvehiculo is null or v.estado = 'Y') and r.estado = 'A' and tiporecurso in "
				+ trecs + " ) c where   turno = '" + codTurno + "' order by nombre, recurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getRecursosSinVehiculo(String proceso) throws Exception {
		String sql = "select recurso, contratista,tiporecurso,nombre,estado,usuariomovil,clavemovil, estadomovil, latitud, longitud, fechagps, estadovehiculo,online, direccion, estadogps from \"Recursos\" r where not exists (select * from vehiculos where recurso=r.recurso and contratista=r.contratista) and contratista in "
				+ getContratistasProceso(proceso) + "";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public String getNextGeofenceValue(String idvehiculo) throws Exception {
		String sql="select min(numgeocerca) from active_geofences('"+idvehiculo+"') where estado = 'I'";
		if (db.executeQuery(sql)) {
			if(db.next()) {
				return db.getString(1);
			} else {
				throw new Exception("06x001 - No hay geocercas disponibles");
			}
		} else {
			throw new Exception(ERRORMESSAGE_01x004);
		}
	}

	public boolean deactivateGeofence(String idvehiculo, String numgeocerca)
			throws Exception {
		String sql = "update geocercas set estado = 'I' where numgeocerca="
				+ numgeocerca + " and idvehiculo='" + idvehiculo + "' AND estado='A'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception("01x003 - Error al insertar en la base de datos");
		}
	}

	public String[][] getPtoInt(String idusuario, String idproceso)
			throws Exception {
		String sql = "select pi.descripcion,direccion, municipio, departamento, i.path, " +
				"case when idusuario is null then 'P' else 'U' end as tipo ,latitud, longitud, pi.idpunto, i.idicono " +
				"from puntosdeinteres pi inner join iconos i on (i.idicono = pi.icono) left join usuario_ptoint up on (pi.idpunto=up.idpunto) " +
				"left join proceso_ptoint pp on (pi.idpunto=pp.idpunto) " +
				"where (idusuario is not null or idproceso is not null) and (idusuario='"
				+ idusuario + "' or idproceso='" + idproceso + "') and estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(ERRORMESSAGE_01x004);
		}
	}

	public String[][] getPtoIntProceso(String idproceso) throws Exception {
		String sql = "select pi.descripcion,direccion, municipio, departamento, i.path, " +
				"'P' as tipo ,latitud, longitud, pi.idpunto, i.idicono from puntosdeinteres " +
				"pi inner join iconos i on (i.idicono = pi.icono) left join proceso_ptoint pp on (pi.idpunto=pp.idpunto) where (idproceso is not null) and (idproceso='"
				+ idproceso + "') and estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(ERRORMESSAGE_01x004);
		}
	}

	public String[][] getPtoIntUsuario(String idusuario) throws Exception {
		String sql = "select pi.descripcion,direccion, municipio, departamento, i.path," +
				"'U' as tipo ,latitud, longitud, pi.idpunto, i.idicono from puntosdeinteres " +
				"pi inner join iconos i on (i.idicono = pi.icono) left join usuario_ptoint up on (pi.idpunto=up.idpunto) where (idusuario is not null) and (idusuario='"
				+ idusuario + "') and estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(ERRORMESSAGE_01x004);
		}
	}

	public String[][] getUsersInfo(String idusuario) throws Exception {
		String sql = "select U.usuario,U.NOMBRE, U.CLAVE, U.CARGO, P.PERFIL AS CODPERFIL, P.NOMBRE AS NOMBREPERFIL FROM \"Usuarios\" AS U INNER JOIN \"UsuariosPerfiles\" AS UP ON U.USUARIO = UP.USUARIO INNER JOIN"
				+ " \"Perfiles\" AS P ON UP.PERFIL = P.PERFIL where exists (select usuario from \"UsuariosProcesos\" p2 " +
				"where exists (select proceso from \"UsuariosProcesos\" where usuario='"+idusuario+"' " +
				"and proceso =p2.proceso) and usuario=u.usuario) or " +
				"not exists (select usuario from \"UsuariosProcesos\" p3 where usuario=u.usuario)";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(ERRORMESSAGE_01x004);
		}
	}

	private String handleNulls(String value) {
		if (value != null) {
			value = "'" + value + "'";
		} else {
			value = "NULL";
		}
		return value;
	}

	public boolean addPtoInt(String id, String tipo, String latitud,
			String longitud, String desc, String direccion, String municipio,
			String departamento, String icono) throws Exception {
		if (!tipo.equals(ClientUtils.USER_POI) && !tipo.equals(ClientUtils.PROCESS_POI)) {
			throw new Exception("06x202 - Tipo de punto de interes no admitido");
		}
		direccion = handleNulls(direccion);
		departamento = handleNulls(departamento);
		municipio = handleNulls(municipio);

		String sql = "insert into puntosdeinteres (latitud, longitud, descripcion,direccion, municipio, departamento, icono) values('"
				+ latitud
				+ "','"
				+ longitud
				+ "','"
				+ desc
				+ "',"
				+ direccion
				+ ","
				+ municipio
				+ ","
				+ departamento
				+ ",'"
				+ icono
				+ "') returning idpunto";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				String pto = db.getString(1);
				if (tipo.equals(ClientUtils.USER_POI)) {
					sql = "insert into usuario_ptoint (idusuario, idpunto) values ('"
							+ id + "','" + pto + "')";
				} else if (tipo.equals(ClientUtils.PROCESS_POI)) {
					sql = "insert into proceso_ptoint (idproceso, idpunto) values ('"
							+ id + "','" + pto + "')";
				}
				if (db.executeUpdate(sql) < 1) {
					throw new Exception(
							"06x204 - No se logro asociar el punto de interes con la entidad indicada");
				}
			} else {
				throw new Exception(
						"06x203 - La insercion del punto de interes no retorno valor alguno");
			}
		} else {
			throw new Exception(
					"06x201 - Error al insertar el punto de interes");
		}
		return true;
	}

	public boolean deletePtoInt(String idPtoInt) {
		String sql = "update puntosdeinteres set estado='I' where idpunto = " + idPtoInt;
		return db.executeUpdate(sql) > 0;
	}

	public boolean addProceso(String proceso, String nombre) {
		String sql="insert into \"Procesos\"(proceso, nombre) values ('"+proceso+"','"+nombre+"')";
		return db.executeUpdate(sql) > 0;
	}

	public String[][] getCategoriasListasPrecierres() throws Exception {

		String sql = "SELECT id_categoria, descripcion, mlista "
				+ "FROM \"CategoriasListasPrecierres\" where  estado = 'A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception();
		}
	}

	public String[][] getListaPrecierreDeCategoria(String idcat) throws Exception {

		String sql = "SELECT lp.id_lista, lp.codigo, lp.descripcion, lp.orden, clp.descripcion "
				+ "FROM \"ListasPrecierres\" lp, \"CategoriasListasPrecierres\" clp "
				+ "where clp.id_categoria=lp.id_categoria and clp.estado = 'A' and lp.\"Estado\" = 'A' "
				+ "and lp.id_categoria = '"+idcat+"' order by lp.orden";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception();
		}
	}

	public Boolean addOpcListaToCategoria(String cod, String desc, String idcat) throws Exception{
		String sql = "INSERT INTO \"ListasPrecierres\"( "
				+ "codigo, descripcion, id_categoria) " + "VALUES ('" + cod
				+ "', '" + desc + "', '" + idcat + "')";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean addCategoriasListasPrecierre(String nombre, String tipo) throws Exception {
		String sql = "INSERT INTO \"CategoriasListasPrecierres\"(descripcion,mlista) "
				+ "VALUES ('" + nombre + "','"+tipo+"')";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception();
		}
	}

	public Boolean deleteCategoriasListasPrecierre(String idcat) throws Exception {
		String sql = "delete from \"CategoriasListasPrecierres\" where id_categoria = '"
				+ idcat + "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			sql = "update \"CategoriasListasPrecierres\" set estado = 'I' where id_categoria = '"
					+ idcat + "'";
			if (db.executeUpdate(sql) != 0) {
				return true;
			} else {
				throw new Exception();
			}
		}
	}

	public Boolean updateCategoriasListasPrecierre(String idcat, String descripcion, String tipo) throws Exception {
		String sql = "update \"CategoriasListasPrecierres\" set descripcion = '"
				+ descripcion + "', mlista = '"+tipo+"' where id_categoria='" + idcat + "'";
		String tipolargo = "";
		if (tipo.equals("S")) {
			tipolargo = "multilista";
		} else {
			tipolargo = "lista";
		}
		String sqlu = "update \"CamposPrecierresV2\" set tipo = '"+tipolargo+"' where id_categoria = '"+idcat+"'";
		if (db.executeUpdate(sql) != 0 || db.executeUpdate(sqlu) != 0) {
			return true;
		} else {
			return false;
		}
	}

	public Boolean deleteOpcLista(String idlista) throws Exception {
		String sql = "delete from \"ListasPrecierres\" where id_lista='"
				+ idlista + "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			sql = "update \"ListasPrecierres\" set \"Estado\" = 'I' "
					+ "where id_lista = '" + idlista + "'";
			if (db.executeUpdate(sql) != 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		}
	}

	public Boolean addItemPrecierre(String ttrab, String descripcion, boolean noRealizacion,int orden)
			throws Exception {
		String sql = "INSERT INTO \"ItemsPrecierres\"( "
				+ "tipotrabajo, orden, descripcion, esnorealizacion) " + "VALUES ('" + ttrab
				+ "', " + orden + ", '" + descripcion + "','"+(noRealizacion?"S":"N")+"')";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean deleteItemPrecierre(String item) throws Exception {
		String sql = "delete from \"ItemsPrecierres\" where item = '" + item
				+ "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			sql = "update \"ItemsPrecierres\" set estado = 'I' where item = '"
					+ item + "'";
			if (db.executeUpdate(sql) != 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		}
	}

	public Boolean updateItemPrecierre(String item, String descripcion,boolean  noRealizacion,
			int orden) throws Exception {
		String sql = "update \"ItemsPrecierres\" set orden = " + orden
				+ ", descripcion = '" + descripcion + "', esnorealizacion='"+(noRealizacion?"S":"N")+"' where item='" + item
				+ "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getItemsPrecierres(String ttrab) throws Exception {
		/*String sql = "SELECT item, descripcion, orden, esnorealizacion "
				+ "FROM \"ItemsPrecierres\" where tipotrabajo = '" + ttrab
				+ "' and estado='A' order by orden";*/
		String sql = "SELECT item, descripcion, orden, esnorealizacion, repeticion "
				+ "FROM \"vwItemsPrecierres\" where tipotrabajo = '"+ttrab+"' and estado = 'A' order by orden";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean addCampoPrecierreTexto(String desc, int orden, String item) throws Exception{
		String sql = "INSERT INTO \"CamposPrecierres\"( "
				+ "descripcion, tipo, orden, item) " + "VALUES ('" + desc
				+ "', 'texto', " + orden + ", '" + item + "')";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean addCampoPrecierreLista(String desc, int orden, String item,
			String idcat, String tipo) throws Exception {
		String sql = "INSERT INTO \"CamposPrecierres\"( "
				+ "descripcion, tipo, orden, item, id_categoria) " + "VALUES ('" + desc
				+ "', '"+tipo+"', " + orden + ", '" + item + "', '"+idcat+"')";
		if (db.executeUpdate(sql) != 0) {

			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean updateCampoPrecierre(String idcampo, String desc,
			int orden) throws Exception {
		String sql = "update \"CamposPrecierres\" set descripcion='" + desc
				+ "', orden = " + orden + " where id_campo='" + idcampo + "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean deleteCampoPrecierre(String idcampo) throws Exception {
		String sql = "delete from \"CamposPrecierres\" where id_campo='"+idcampo+"'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			sql = "update \"CamposPrecierres\" set estado = 'I' "
					+ "where id_campo = '" + idcampo + "'";
			if (db.executeUpdate(sql) != 0) {
				return true;
			} else {
				throw new Exception(sql);
			}
		}
	}

	public String[][] getCamposPrecierres(String item) throws Exception {

		String sql = "SELECT id_campo, cp.descripcion, coalesce(tipo,'Ninguno'), orden, c.id_categoria, "
				+ "c.descripcion as catdesc, cp.codigo FROM \"vwCamposPrecierres\" cp "
				+ "left outer join \"CategoriasListasPrecierres\" c "
				+ "on (cp.id_categoria = c.id_categoria) where cp.estado = 'A' and item = '"+item+"'::bigint order by orden";
		/*String sql = "SELECT id_campo, cp.descripcion, tipo, orden, c.id_categoria, c.descripcion as catdesc "+
				"FROM \"CamposPrecierres\" cp left outer join \"CategoriasListasPrecierres\" c "+
>>>>>>> .r124
				"on (cp.id_categoria = c.id_categoria) where cp.estado = 'A' and item = '"+item+"' order by orden";*/
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean updateOpcionesCategoria(String id, String nombre) throws Exception {
		String sql = "update \"ListasPrecierres\" set descripcion='" + nombre
				+ "' " + "where id_lista = '" + id + "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	//----------------------------KAGUA-----------------------------

	/*private String getMunicipioDeTrabajo(String trabajo)
			throws Exception {
		String sql = "select municipio from \"Trabajos\" where trabajo = '"
				+ trabajo + "' and tipotrabajo = '" + tipotrabajo + "'";
		if (db.executeQuery(sql)) {
			db.next();
			return db.getString(1);
		} else
			throw new Exception(sql);
		return trabajo.split("-")[0];
	}*/

	public String[][] getInfoKagua(String idTrabajo, String idTipoTrabajo) throws Exception {
		String sql = "select solicitante, telefono, propietario, barrio, ruta, nomedidor, ultlectura, prioridad_ot, registra_ot, idpqr, cptopqr, descripcionpqr, fechapqr, registrapqr" +
				" from \"Trabajos\" where trabajo='"+idTrabajo+"' and tipotrabajo='"+idTipoTrabajo+"'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception("Hubo un problema de red, intente nuevamente...");
		}
	}

	/*private boolean asignarOTKAGUA(String trabajo, String tipotrabajo,
			String recurso, String contratista) throws Exception {
		String sql = "select * from asignar_ot_(" + trabajo.split("-")[1] + ", '"
				+ tipotrabajo + "', "
				+ getMunicipioDeTrabajo(trabajo) + ", '1', "
				+ contratista + ", " + recurso + ")";
		if (dbCao.executeQuery(sql)) {
			dbCao.next();
			if(dbCao.getString(1).equals("0"))
				return true;
			else
				throw new Exception(dbCao.getString(2));
		} else
			throw new Exception(sql);
	}*/

	public boolean checkConnection() throws Exception {
		db.checkConnection();
		//dbCao.checkConnection();
		return true;
	}

	@Improved
	public String[][] getTrabajosConsulta(String codmunip, String criterio,
			String busqueda, String proceso) throws Exception {
		/*- Codigo OT
		- Concepto OT
		- Matricula
		- Direccion
		- Municipio
		- Descripcion
		- Estado
		- Asignado a*/
		String sql = "select tr.recurso, tr.contratista, t.trabajo, t.nomtipotrab || ' (' || t.tipotrabajo || ')' as concepto, "
				+ "t.nomcli || ' (' || t.codcli || ')' as cliente, "
				+ "t.direccion, t.nommunip || ' (' || t.municipio || ')' as municipio, t.descripcion, t.estadotrabajo, "
				+ "case when tr.recurso is null then "
				+ "case when not validaNumero(t.latitud) or not validaNumero(t.longitud)  or t.latitud = '0' or t.longitud = '0' then "
				+ "'No Ubicado' "
				+ "else "
				+ "'No Asignado' "
				+ "end else "
				+ "(select r.nombre || ' (' || r.recurso || ' - ' || r.contratista || ')' "
				+ "from \"Recursos\" r "
				+ "where r.recurso = tr.recurso and r.contratista = tr.contratista) "
				+ "end as estado "
				+ "from \"vwTrabajos\" t "
				+ "left outer join \"TRecursos\" tr on (t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo) where t.proceso = '"
				+ proceso + "' and t.municipio = '" + codmunip + "' ";

		if(criterio.equals("1")) {
			sql += "and t.trabajo = '" + busqueda
					+ "' order by t.trabajo";
		} else if(criterio.equals("2")) {
			sql += "and ',' || t.codcli ',' || t.nomcli ',' "
					+ "like '%," + busqueda + ",%' order by t.nomcli, t.codcli";
		} else if(criterio.equals("3")) {
			sql += "and t.direccion like '" + busqueda + "' order by t.direccion";
		}

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}

	}

	public String[][] getInfoAdicionalTrabajo(String idTrabajo,
			String idTipoTrabajo) throws Exception {
		String sql = "select nomcli || ' (' || codcli || ')', t.numviaje, "
				+ "t.barrio, t.telefono, to_char(t.fechaentrega, 'yyyy/MM/dd'), nomjornada, horaentrega "
				+ "from \"vwTrabajos\" t " + "where t.trabajo = '" + idTrabajo
				+ "' and t.tipotrabajo = '" + idTipoTrabajo + "'";
		if(db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getJornadas() throws Exception {
		String sql = "select idjornada, nombre from \"Jornadas\"";
		if(db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public boolean endCurrentTrip(String idrec, String idcont) throws Exception {
		String sql = "update \"Trabajos\" t set viajefinalizado = 'S' " +
				"where viajefinalizado = 'N' and exists ("
				+ "select 'X' from \"TRecursos\" tr "
				+ "where t.trabajo = tr.trabajo and t.tipotrabajo = tr.tipotrabajo "
				+ "		and recurso = '"
				+ idrec + "' and contratista = '" + idcont + "')";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getWorksFromDates(String recurso, String contratista,
			String fromDate, String toDate) throws Exception {
		String sql = "select t.trabajo, tt.nombre, cliente, t.municipio, direccion, inicioruta, prioridad, "
				+ "fecha, estadotrabajo, latitud, longitud, m.nombre, departamento, t.solicitante, "
				+ "t.numviaje, calculaatraso(t.fecha, t.fechaprecierre) AS atraso, "
				+ "to_char(t.fechaprecierre, '"+ServerUtils.DATE_FORMAT+"') "
				+ "from \"Trabajos\" t inner join (select trabajo from \"TRecursos\" "
				+ "where recurso='" + recurso + "' and contratista ='"
				+ contratista + "') tr on (t.trabajo = tr.trabajo) "
				+ "inner join (select tipotrabajo, nombre from \"TiposTrabajos\") tt on (tt.tipotrabajo = t.tipotrabajo) "
				+ "inner join (select * from \"Municipios\") m on (m.municipio = t.municipio) "
				+ "where ((t.estadotrabajo != '30' or t.fechaprecierre::date >= '"
				+ fromDate + "'::date" + ") and t.fecha::date <= '"	+ fromDate + "'::date) "
				+ "or (t.fecha::date between '" + fromDate + "'::date and '"
				+ toDate + "'::date) or (t.fechaprecierre between '" + fromDate
				+ "'::date and '" + toDate + "'::date)";

		/*
		 * String sql =
		 * "select t.trabajo, tt.nombre, cliente, t.municipio, direccion, inicioruta, prioridad, fecha, estadotrabajo, latitud, longitud, m.nombre, departamento "
		 * +
		 * "from \"Trabajos\" t inner join (select trabajo from \"TRecursos\" where recurso='"
		 * + recurso + "' and contratista ='" + contratista +
		 * "') tr on (t.trabajo = tr.trabajo) " +
		 * "inner join \"TiposTrabajos\" tt on (t.tipotrabajo = tt.tipotrabajo) "
		 * + "inner join \"Municipios\" m on(t.municipio = m.municipio) " +
		 * "where t.fecha between '" +
		 * fromDate+"'::timestamp without time zone and '" +
		 * toDate+"'::timestamp without time zone";
		 */
		System.out.println(sql);
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	//---------------------------  FILE UPLOADER ----------------------------------------

	public Boolean addTemplate(String archivo, String nombre, String formato,
			String proceso) {
		String sql = "insert into \"ConfiguracionArchivo\"(idarchivo, nombre, idformato, activo, proceso) values ('"
				+ archivo
				+ "','"
				+ nombre
				+ "','"
				+ formato
				+ "','t','"
				+ proceso + "');";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean updateTemplate(String id, String nombre, String formato,
			String proceso) {
		String sql = "update \"ConfiguracionArchivo\" set nombre='" + nombre
				+ "', idformato='" + formato + "' where idconfiguracion='" + id
				+ "' and proceso='" + proceso + "';";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean delTemplate(String id) {
		String sql = "update \"ConfiguracionArchivo\" set activo='f' where idconfiguracion='"
				+ id + "'";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean addCampoTemplate(String template, String columna,
			String campo) {
		String sql = "insert into \"DetalleConfiguracionArchivo\" (idconfiguracion, columna, idcampo)values('"
				+ template + "','" + columna + "','" + campo + "' );";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean updateCampoTemplate(String id, String columna) {
		String sql = "update \"DetalleConfiguracionArchivo\" set columna='"
				+ columna + "' where idconfiguracion='" + id + "';";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean delCampoTemplate(String id) {
		String sql = "delete from \"DetalleConfiguracionArchivo\" where iddetalleconfiguracion='"
				+ id + "';";
		return db.executeUpdate(sql) != 0;
	}

	public String[][] getCamposArchivo(String id) throws Exception {
		String sql = "select idcampo, nombre from \"Campos\" "
				+ "where idarchivo='" + id + "' order by idcampo;";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getArchivos() throws Exception {
		String sql = "select idarchivo, nombre from \"Archivo\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getDestinos() throws Exception {
		String sql = "select iddestino, nombre from \"Destinos\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getVehiculos() throws Exception {
		String sql = "select idvehiculo, placa from vehiculos";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getGeocercas(String idvehiculo) throws Exception {
		String sql = "select idgeocerca, descripcion from geocercas where idvehiculo='"+idvehiculo+"'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public String[][] getMuelles() throws Exception {
		String sql = "select muelle, nombremuelle from \"Muelle\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getHoraEventos(String fecha, String campo, String idvehiculo)throws Exception{
		String sql="";
		System.out.println("Horas eventos campo: "+campo);
		if(campo.equalsIgnoreCase("Hora Entrada")){
			sql = "select e.fecha::time, e.fecha::time from eventos e inner join vehiculos v on (v.idvehiculo=e.idvehiculo) where evento='5' and fecha::date='"+fecha+"' and v.placa='"+idvehiculo+"'";
		}else{
			if(campo.equalsIgnoreCase("Hora Salida")){
				sql = "select e.fecha::time, e.fecha::time  from eventos e inner join vehiculos v on (v.idvehiculo=e.idvehiculo) where evento=(select g.idegreso from geocercas g inner join vehiculos v on (v.idvehiculo=g.idvehiculo) where g.estado='A' and v.placa='"+idvehiculo+"') and fecha::date='"+fecha+"' and v.placa='"+idvehiculo+"'";
			}else{
				if(campo.equalsIgnoreCase("Hora Retorno")){
					sql = "select e.fecha::time from eventos e inner join vehiculos v on (v.idvehiculo=e.idvehiculo) where evento=(select g.idingreso from geocercas g inner join vehiculos v on (v.idvehiculo=g.idvehiculo) where g.estado='A' and v.placa='"+idvehiculo+"') and fecha::date='"+fecha+"' and v.placa='"+idvehiculo+"'";
				}
			}
		}

		System.out.println("Horas eventos: "+sql);

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getConductores() throws Exception {
		String sql = "select codigo, nombre from \"Conductores\" where estado='A'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getTiemposAct() throws Exception {
		String sql = "select miliseg, tiempodesc from \"TiemposAct\" ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getCamposTemplatesAsignados(String template)
			throws Exception {
		String sql = "select iddetalleconfiguracion ,columna,c.nombre, c.idcampo,\"default\", funcion, pk from \"Archivo\" inner join \"Campos\" c using (idarchivo) "
				+ "inner join \"ConfiguracionArchivo\" using (idarchivo) inner join \"DetalleConfiguracionArchivo\" "
				+ "using (idconfiguracion, idcampo) where idconfiguracion ='"
				+ template + "' or idconfiguracion is null order by columna";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getFormatos() throws Exception {
		String sql = "select idformato, nombre from \"Formatos\"";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getTemplates(String id) throws Exception {
		String sql = "select idconfiguracion, idarchivo, c.nombre as nombreTemplete, c.idformato, activo, "
				+ "proceso,f.nombre as nombreFormato, servlet, separacion, f.extensiones, "
				+ "a.nombre as nombreArchivo from \"ConfiguracionArchivo\" c inner join "
				+ "\"Formatos\" f using (idformato) inner join \"Archivo\" a using (idarchivo)  "
				+ "where proceso='" + id + "' and activo";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(
					"Hubo un problema de red, intente nuevamente...");
		}
	}

	public String[][] getCamposTemplates(String id) throws Exception {
		String sql = "select columna,campotabla, \"default\", funcion, pk, iddetalleconfiguracion, tipo, buscarconv from \"Archivo\" inner join \"Campos\" using (idarchivo) "
				+ "inner join \"ConfiguracionArchivo\" using (idarchivo) left join \"DetalleConfiguracionArchivo\" "
				+ "using (idconfiguracion, idcampo) where idconfiguracion ='"
				+ id + "' or idconfiguracion is null  order by columna";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[] getTablaTemplates(String template) throws Exception {
		String sql = "select tabla, mensaje from \"ConfiguracionArchivo\" inner join \"Archivo\" using (idarchivo) where idconfiguracion='"
				+ template + "'";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				return new String[] { db.getString(1), db.getString(2) };
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String executeValidatedInsert(String select, String update,
			String insert) throws Exception {
		select = select.replace("'", "''");
		update = update.replace("'", "''");
		insert = insert.replace("'", "''");
		String sql = "select validar_insercion('" + select + "','" + update
				+ "','" + insert + "')";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				return db.getString(1);
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String getMunicipio(String nombre) throws Exception {
		String sql = "select getMunicipio('" + nombre + "')";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				return db.getString(1);
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getValoresCampo(String id) throws Exception {
		String sql = "select idvalor, idcampo, valor, estatico, consulta, nombre from \"ValoresCampos\" where idcampo='"
				+ id + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] executeQuery(String sql) throws Exception {
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getValoresCampoTemplete(String id) throws Exception {
		String sql = "select iddetallevalor, iddetalleconfiguracion, invalue, outvalue, label from \"DetalleValores\" where iddetalleconfiguracion='"
				+ id + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean addValorCampoTemplate(String iddetalle, String invalue,
			String outvalue, String label) {
		String sql = "insert into \"DetalleValores\" (iddetalleconfiguracion, invalue, outvalue, label)"
				+ "values('"
				+ iddetalle
				+ "','"
				+ invalue
				+ "','"
				+ outvalue
				+ "','" + label + "' );";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean delValorCampoTemplate(String id) {
		String sql = "delete from \"DetalleValores\" where iddetallevalor='"
				+ id + "'";
		return db.executeUpdate(sql) != 0;
	}

	public String getValorDetalleCampo(String id, String invalue)
			throws Exception {
		String sql = "select outvalue from \"DetalleValores\" where iddetalleconfiguracion='"
				+ id + "' and trim(invalue)=trim('" + invalue + "')";
		if (db.executeQuery(sql)) {
			if (db.next()) {
				return db.getString(1);
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String getJornada(String value) throws Exception {
		/*String sql = "select idjornada from \"Jornadas\" where horainicial <= '"
				+ value + "'::time and horafinal >= '" + value + "'::time";*/

		String sql = "select idjornada from \"Jornadas\" where " +
				"case " +
				"when horainicial < horafinal then horainicial <= '"+value+"'::time and horafinal >= '"+value+"'::time " +
				"when horafinal < horainicial then " +
				"(horainicial <= '"+value+"'::time and '23:59:59'::time >= '"+value+"'::time " +
				"or " +
				"'00:00:00'::time <= '"+value+"'::time and horafinal >= '"+value+"'::time) " +
				"end " +
				"limit 1";
		if (db.executeQuery(sql))
		{
			String[][] resultado = db.getTable();
			if(resultado.length > 0) {
				return resultado[0][0];
			} else {
				return "";
			}
		} else {
			throw new Exception(sql);
		}
	}

	public String[] getGeo(String value,String city) throws Exception {
		String sql = "select latitud, longitud from \"EjesViales\" where dirnoform=obtienecabecera(normalizador('"
				+ value + "')) and municipio=" + city + "";
		if (db.executeQuery(sql)){
			String s[][] =db.getTable();
			if(s.length>0) {
				return s[0];
			} else {
				return new String[]{null,null};
			}
		} else {
			throw new Exception(sql);
		}
	}

	public boolean assignProg(String fechaent,
			String horaent, String trab, String ttrab) throws Exception {
		String idjornada = getJornada(horaent);
		String sqljornada = "";
		if(!idjornada.equals("")) {
			sqljornada = ", jornadaentrega = " + idjornada + " ";
		}
		String sql = "update \"Trabajos\" set fechaentrega = to_date('"
				+ fechaent + "', 'YYYY/MM/DD')" + sqljornada
				+ ", horaentrega = to_timestamp('" + horaent
				+ "', 'HH24:MI')::time where trabajo = '" + trab
				+ "' and tipotrabajo = '" + ttrab + "'";
		if(db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getInfoGPSPrecierre(String oldIdPrecierre) throws Exception {
		String sql = "select validagps, coalesce(to_char(fechagps, '"
				+ ServerUtils.DATE_FORMAT
				+ "'), 'No Disponible'), coalesce(distancia::character varying || ' m.', 'No Disponible') "
				+ "from \"TPrecierres\" where idprecierre = " + oldIdPrecierre;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][][] getPrecierresExport(String proceso, String ttrab, String fechadesde,
			String fechahasta) throws Exception {
		String sql_cat = "select c.descripcion "
				+ "from \"ItemsPrecierres\" i inner join \"CamposPrecierres\" c on (i.item = c.item) "
				+ "where c.estado = 'A' and i.estado = 'A' and tipotrabajo = '"
				+ ttrab + "' " + "order by i.orden, c.orden";

		String sql_src = "select ARRAY[t.trabajo::text, t.tipotrabajo, numviaje::text, to_char(t.fecha, 'YYYY-MM-dd HH24:MI'), t.descripcion, "
				+ "t.cliente, t.solicitante, t.direccion, to_char(t.fechaentrega, 'YYYY-MM-dd HH24:MI'), "
				+ "j.nombre, to_char(t.horaentrega, 'HH24:MI'), p.recurso, r.nombre, "
				+ "to_char(t.fechainicio, 'YYYY-MM-dd HH24:MI'), to_char(t.fechaprecierre, 'YYYY-MM-dd HH24:MI'), "
				+ "coalesce(p.validagps, ''), coalesce(to_char(p.fechagps, 'YYYY-MM-dd HH24:MI'), ''), coalesce(p.distancia::text, ''),(select calculaatraso(t.fecha, t.fechaprecierre)) ] , "
				+ "opcionprecierre, codigoprecierre "
				+ "from \"vwCodigosPrecierres\" cp inner join \"TPrecierres\" p on (cp.idprecierre = p.idprecierre) "
				+ "inner join \"Trabajos\" t on (p.trabajo = t.trabajo and p.tipotrabajo = t.tipotrabajo) "
				+ "inner join \"Recursos\" r on (r.recurso = p.recurso and r.contratista = p.contratista) "
				+ "inner join \"Jornadas\" j on (j.idjornada = t.jornadaentrega) "
				+ "where t.proceso = '" + proceso + "' and t.tipotrabajo = '" + ttrab
				+ "' and t.fechaprecierre between "	+ toDate(fechadesde)
				+ " and " + toDate(fechahasta) + " order by cp.idprecierre";

		return crosstabN(sql_src, sql_cat);
	}

	public String[][] getParkedVehs(String proceso) throws Exception {
		String sql = "select * from getestacionados('" + proceso + "')";
		if(db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	//---------------------------  STATISTICS -------------------------------------------

	
	public String[][] getTTrabajoRecurso(String fromDate, String toDate, String[] rec, String[] cont, String munip)
			throws Exception {
		String sql = "select t.tipotrabajo, '[' || t.tipotrabajo || '] ' || tt.nombre "
				+ "from \"Trabajos\" t inner join \"TiposTrabajos\" tt on (t.tipotrabajo = tt.tipotrabajo) "
				+ "inner join \"TPrecierres\" tp on (t.trabajo = tp.trabajo and t.tipotrabajo = tp.tipotrabajo) "
				+ "where estadotrabajo = '30' and tp.fechaprecierre between '" + fromDate
				+ " 00:00'::timestamp without time zone and '" + toDate + " 23:59'::timestamp without time zone ";
		// + "and contratista = '" + cont + "' ";
		String sql2 = "";
		if (rec != null) {
			String[] reccont = new String[rec.length];
			for (int i = 0; i < rec.length; i++) {
				reccont[i] = rec[i] + cont[i];
			}
			sql2 += "and recurso|| tp.contratista in " + ServerUtils.arrayToString(reccont);
		} else {
			sql2 += "and tp.contratista in " + ServerUtils.arrayToString(cont);
		}

		sql += sql2;

		if (!munip.equals("all")) {
			sql += "and municipio = '" + munip + "' ";
		}
		sql += "group by t.tipotrabajo, tt.nombre " + "order by tt.nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getTTrabajoRecursoOld(String fromDate, String toDate,
			String[] rec, String[] cont, String munip) throws Exception {
		String sql = "select t.tipotrabajo, '[' || t.tipotrabajo || '] ' || tt.nombre "
				+ "from \"Trabajos\" t inner join \"TiposTrabajos\" tt on (t.tipotrabajo = tt.tipotrabajo) "
				+ "inner join \"TPrecierres\" tp on (t.trabajo = tp.trabajo and t.tipotrabajo = tp.tipotrabajo) "
				+ "where estadotrabajo = '30' and tp.fechaprecierre between '"
				+ fromDate + " 00:00'::timestamp without time zone and '"
				+ toDate + " 23:59'::timestamp without time zone ";
		//+ "and contratista = '"	+ cont + "' ";
		String sql2 = "";
		if(rec != null) {
			String[] reccont = new String[rec.length];
			for (int i = 0; i < rec.length; i++) {
				reccont[i] = rec[i] + cont[i];
			}
			sql2 += "and recurso||contratista in " + ServerUtils.arrayToString(reccont);
		} else {
			sql2 += "and contratista in " + ServerUtils.arrayToString(cont);
		}

		sql += sql2;

		if (!munip.equals("all")) {
			sql += "and municipio = '" + munip + "' ";
		}
		sql += "group by t.tipotrabajo, tt.nombre "
				+ "order by tt.nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	private String getDato(String dato) {
		if (dato.equals("numtrabs")) {
			return "sum(" + dato + ")";
		} else if (dato.equals("tiempoprom")) {
			return "avg(" + dato + ")";
		} else if (dato.equals("tiempomax")) {
			return "max(" + dato + ")";
		} else if (dato.equals("tiempomin")) {
			return "min(" + dato + ")";
		} else {
			throw new IllegalArgumentException("Dato desconocido: " + dato);
		}
	}

	public String[][][] getComparativeCrosstab(String[] rec, String cont[],
			String munip, String fromDate, String toDate, String dato,
			String[] ttrabs, boolean includeOthers) throws Exception {

		String ttarray = ServerUtils.arrayToString(ttrabs);

		String cat_sql = "select '[' || tipotrabajo || '] ' || nombre as tt from \"TiposTrabajos\" "
				+ "where tipotrabajo in " + ttarray;
		if(includeOthers) {
			cat_sql += " union select 'OTROS' as tt order by tt desc";
		}

		String d = getDato(dato);

		String src_sql = "select * from (";

		String recCont = "";
		if(rec != null) {
			recCont += "select '[' || re.recurso || '-' || re.contratista || '] ' || r.nombre as recurso";
		} else {
			recCont += "select '[' || re.contratista || '] ' || c.nombre as contratista";
		}

		src_sql += recCont;

		src_sql	+= ", '[' || re.tipotrabajo || '] ' || tt.nombre as tipotrabajo, " + d
				+ "from \"vwResumenEstadisticas\" re inner join \"Recursos\" r on (re.recurso = r.recurso and re.contratista=r.contratista) "
				+ "inner join \"TiposTrabajos\" tt on (re.tipotrabajo = tt.tipotrabajo) ";

		if(rec == null) {
			src_sql += "inner join \"Contratistas\" c on (re.contratista = c.contratista) ";
		}

		src_sql += "where re.tipotrabajo in " + ttarray
				+ " and fecha between '"+fromDate+"'::timestamp without time zone and '"+toDate+"'::timestamp without time zone ";
		if (!munip.equals("all")) {
			src_sql += "and municipio = '" + munip + "' ";
		}

		String sql = "";
		if(rec != null) {
			String[] reccont = new String[rec.length];
			for (int i = 0; i < rec.length; i++) {
				reccont[i] = rec[i] + cont[i];
			}
			sql += "and re.recurso||re.contratista in " + ServerUtils.arrayToString(reccont);
		} else {
			sql += "and re.contratista in " + ServerUtils.arrayToString(cont);
		}

		src_sql += sql;

		src_sql += " group by 1, 2 ";
		if(includeOthers){
			src_sql	+= "union " + recCont
					+ ", 'OTROS' as tipotrabajo, " + d
					+ "from \"vwResumenEstadisticas\" re inner join \"Recursos\" r on (re.recurso = r.recurso and re.contratista=r.contratista) "
					+ "inner join \"TiposTrabajos\" tt on (re.tipotrabajo = tt.tipotrabajo) ";
			if(rec == null) {
				src_sql += "inner join \"Contratistas\" c on (re.contratista = c.contratista) ";
			}

			src_sql	+= "where re.tipotrabajo not in " +ttarray
					+ " and fecha between '"+fromDate+"'::timestamp without time zone and '"+toDate+"'::timestamp without time zone ";
			if (!munip.equals("all")) {
				src_sql += "and municipio = '" + munip + "' ";
			}
			src_sql += sql	+ " group by 1";
		}
		src_sql +="	) a order by 1,2 desc";

		return crosstabN(src_sql, cat_sql);
	}

	public String[][][] getIndividualCrosstab(String rec, String cont,
			String munip, String fromDate, String toDate, String dato,
			String[] ttrabs) throws Exception {
		String cat_sql = "select ('"
				+ fromDate
				+ "'::timestamp without time zone + i * '1 day'::interval)::date as day from generate_series(0, "
				+ "((date_part('epoch', '" + toDate
				+ "'::timestamp without time zone) - " + "date_part('epoch', '"
				+ fromDate
				+ "'::timestamp without time zone))/3600/24)::integer) i";

		String d = getDato(dato);


		String src_sql = "select '[' || re.tipotrabajo || '] ' || tt.nombre as tipotrabajo, fecha::date, "
				+ d	+ " from \"vwResumenEstadisticas\" re inner join \"TiposTrabajos\" tt using (tipotrabajo) "
				+ "where fecha between '"
				+ fromDate
				+ "'::timestamp without time zone and '"
				+ toDate
				+ "'::timestamp without time zone "
				+ "and tipotrabajo in "
				+ ServerUtils.arrayToString(ttrabs) + " and contratista = '" + cont + "' ";
		if (rec != null) {
			src_sql += "and recurso = '" + rec + "' ";
		}
		if (!munip.equals("all")) {
			src_sql += "and municipio = '" + munip + "' ";
		}
		src_sql += "group by 1, 2 order by 1, 2";

		return crosstabN(src_sql, cat_sql);

	}

	public String[][] getManagerReport(String munip, String fromDate,
			String toDate, String dato) throws Exception {
		String sql = "select '[' || re.tipotrabajo || '] ' || tt.nombre as tipotrabajo, "
				+ getDato(dato)
				+ " from \"vwResumenEstadisticas\" re inner join \"TiposTrabajos\" tt "
				+ "on (re.tipotrabajo = tt.tipotrabajo) "
				+ "where fecha between '" + fromDate
				+ "'::timestamp without time zone and '"
				+ toDate + "'::timestamp without time zone ";

		if (!munip.equals("all")) {
			sql += "and municipio = '" + munip + "' ";
		}

		sql += "group by 1 order by 2 desc";
		if(db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	private String[][][] crosstabN(String sqlsource, String sqlcategory) throws Exception {
		String sqls = sqlsource.replaceAll("'", "''");
		String sqlc = sqlcategory.replaceAll("'", "''");
		String selcols = "*";
		String seltype = "item_name character varying";

		if(db.executeQuery(sqlcategory)){
			String[][] cats = db.getTable();

			if(sqls.contains("ARRAY")){
				selcols = "";
				String cols = sqls.split("\\[")[1].split("\\]")[0];
				while(cols.contains("(")) {
					cols = cols.replaceAll("\\([^()]*\\)", "");
				}
				int arrayCols = cols.split(",").length;

				for (int i = 0; i < arrayCols; i++) {
					selcols += (i > 0 ? ", " : "") + "item_name[" + (i + 1) + "]";
				}

				for (int i = 0; i < cats.length; i++) {
					selcols += ", bucket" + (i + 1);
				}

				seltype = "item_name text[]";
			}

			String sql = "SELECT " + selcols + " FROM crosstab('" + sqls + "','"
					+ sqlc + "') AS report(" + seltype;

			for (int i = 0; i < cats.length; i++) {
				sql += ", bucket" + (i + 1) + " character varying";
			}


			sql += ")";

			if(db.executeQuery(sql)) {
				return new String[][][]{db.getTable(), cats};
			} else {
				throw new Exception(sql);
			}
		} else {
			throw new Exception("Category SQL: " + sqlcategory);
		}
	}

	// ------------------------------ CENTRAL STATS ------------------------------------------

	public String[][] getAllOdometerReportCentral(String[] rec, String[] cont,
			String fromDate, String toDate) throws Exception {
		String sql = "select r.nombre || ' [' || c.nombre || ']' as nombre, v.valor "
				+ "from ("
				+ "select v.recurso, v.contratista, sum(valor) as valor "
				+ "from odometroscalc oc inner join vehiculos v on (oc.idvehiculo=v.idvehiculo) "
				+ "where fecha between '"
				+ fromDate
				+ "'::timestamp without time zone and '"
				+ toDate
				+ "'::timestamp without time zone "
				+ "and v.recurso || v.contratista in (";
		for (int i = 0; i < rec.length; i++) {
			sql += "'" + rec[i] + cont[i] + "'";
			if (i < rec.length - 1) {
				sql += ",";
			}
		}
		sql += ") "
				+ "group by 1,2) v "
				+ "inner join \"Recursos\" r on (r.recurso = v.recurso and r.contratista = v.contratista)"
				+ "inner join \"Contratistas\" c on (c.contratista = r.contratista) "
				+ "order by 1";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][][] getComparativeCrosstabItem(String[] rec, String cont[],
			String fromDate, String toDate, String dato) throws Exception {

		String cat_sql = "select ('"
				+ fromDate
				+ "'::timestamp without time zone + i * '1 day'::interval)::date as day from generate_series(0, "
				+ "((date_part('epoch', '" + toDate
				+ "'::timestamp without time zone) - " + "date_part('epoch', '"
				+ fromDate
				+ "'::timestamp without time zone))/3600/24)::integer) i";

		String src_sql = "";

		if (dato.equals("km")) {
			src_sql = "select (select nombre from \"Recursos\" r where v.recurso = r.recurso and v.contratista = r.contratista), fecha as day, valor "
					+ "from  odometroscalc oc inner join vehiculos v on (oc.idvehiculo=v.idvehiculo) "
					+ "where fecha between '"
					+ fromDate
					+ "'::timestamp without time zone and '"
					+ toDate
					+ "'::timestamp without time zone "
					+ "and recurso || contratista in (";
			for (int i = 0; i < rec.length; i++) {
				src_sql += "'" + rec[i] + cont[i] + "'";
				if (i < rec.length - 1) {
					src_sql += ",";
				}
			}
			src_sql += ") order by 1,2";

		} else {
			src_sql = "select (select nombre from \"Recursos\" r where v.recurso = r.recurso and v.contratista = r.contratista),"
					+ "fecha::date, "
					+ (dato.equals("vp") ? "avg(ev.velocidad::double precision)"
							: "max(ev.velocidad::double precision)")
							+ " from eventos ev inner join vehiculos v on (ev.idvehiculo=v.idvehiculo) "
							+ "where fecha between '"
							+ fromDate
							+ " 00:00'::timestamp without time zone and '"
							+ toDate
							+ " 23:59'::timestamp without time zone and ev.velocidad::double precision>0 "
							+ "and recurso || contratista in (";
			for (int i = 0; i < rec.length; i++) {
				src_sql += "'" + rec[i] + cont[i] + "'";
				if (i < rec.length - 1) {
					src_sql += ",";
				}
			}
			src_sql += ")" + "group by 1,2 order by 1,2";
		}

		return crosstabN(src_sql, cat_sql);
	}

	public String[][] getManagerReportEvents(String[] eve, String fromDate,
			String toDate, String proc) throws Exception {

		String[][] vehiculos = getInfoVehsProceso(proc);

		String sql =

				"select ed.descripcion, count(e.idevento) "
						+ "from eventos e inner join eventosdesc ed on (e.evento = ed.evento) "
						+ "where fecha between '" + fromDate
						+ " 00:00'::timestamp without time zone and '" + toDate
						+ " 23:59'::timestamp without time zone " + "and e.idvehiculo in (";
		for (int i = 0; i < vehiculos.length; i++) {
			sql += "'" + vehiculos[i][0] + "'";
			if (i < vehiculos.length - 1) {
				sql += ",";
			}
		}
		sql += ") and e.evento in (";
		for (int i = 0; i < eve.length; i++) {
			sql += "'" + eve[i] + "'";
			if (i < eve.length - 1) {
				sql += ",";
			}
		}
		sql += ") group by 1 " + "order by 1";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getEvents() throws Exception {

		String sql = "select evento, descripcion from eventosdesc order by evento::interval";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getManagerReportOdom(String fromDate, String toDate,
			String proc) throws Exception {

		String[][] vehiculos = getInfoVehsProceso(proc);

		String sql = "select (select nombre from \"Recursos\" r where v.recurso = r.recurso and v.contratista = r.contratista), sum(valor) "
				+ "from  odometroscalc oc inner join vehiculos v on (oc.idvehiculo=v.idvehiculo) "
				+ "where fecha between '"
				+ fromDate
				+ "'::timestamp without time zone and '"
				+ toDate
				+ "'::timestamp without time zone " + "and v.idvehiculo in (";
		for (int i = 0; i < vehiculos.length; i++) {
			sql += "'" + vehiculos[i][0] + "'";
			if (i < vehiculos.length - 1) {
				sql += ",";
			}
		}
		sql += ") group by 1 order by 1";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getEvents(String[] rec, String[] cont, String fromDate,
			String toDate) throws Exception {
		String sql =

				"select distinct ed.evento, ed.descripcion "
						+ "from eventos e inner join eventosdesc ed on (e.evento = ed.evento) "
						+ "inner join vehiculos v on (e.idvehiculo = v.idvehiculo) "
						+ "where fecha between '" + fromDate
						+ " 00:00'::timestamp without time zone and '" + toDate
						+ " 23:59'::timestamp without time zone "
						+ "and e.evento = ed.evento and recurso || contratista in (";
		for (int i = 0; i < rec.length; i++) {
			sql += "'" + rec[i] + cont[i] + "'";
			if (i < rec.length - 1) {
				sql += ",";
			}
		}
		sql += ") ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][][] getComparativeCrosstab(String[] rec, String cont[],
			String fromDate, String toDate, String[] eventos) throws Exception {

		String cat_sql = "select descripcion from eventosdesc"
				+ " where evento in (";
		for (int i = 0; i < eventos.length; i++) {
			cat_sql += "'" + eventos[i] + "'";
			if (i < eventos.length - 1) {
				cat_sql += ",";
			}
		}
		cat_sql += ") order by 1";

		String src_sql = "select (select nombre from \"Recursos\" r where v.recurso = r.recurso and v.contratista = r.contratista), "
				+ "ed.descripcion, count(e.idevento) "
				+ "from eventos e inner join eventosdesc ed on (e.evento = ed.evento) "
				+ "inner join vehiculos v on (e.idvehiculo = v.idvehiculo) "
				+ "where fecha between '"
				+ fromDate
				+ " 00:00'::timestamp without time zone and '"
				+ toDate
				+ " 23:59'::timestamp without time zone "
				+ "and e.evento = ed.evento and recurso || contratista in (";
		for (int i = 0; i < rec.length; i++) {
			src_sql += "'" + rec[i] + cont[i] + "'";
			if (i < rec.length - 1) {
				src_sql += ",";
			}
		}
		src_sql += ") and e.evento in (";
		for (int i = 0; i < eventos.length; i++) {
			src_sql += "'" + eventos[i] + "'";
			if (i < eventos.length - 1) {
				src_sql += ",";
			}
		}
		src_sql += ") group by 1,2 order by 1,2";

		return crosstabN(src_sql, cat_sql);
	}

	public String[][] getIndReportCentral(String idrec, String idcont,
			String fromDate, String toDate, String dato) throws Exception {
		String sql;
		if (dato.equals("km")) {
			sql = "select fecha, valor"
					+ " from  odometroscalc oc inner join vehiculos v on (oc.idvehiculo=v.idvehiculo) "
					+ "where fecha between '"
					+ fromDate
					+ " 00:00'::timestamp without time zone and '"
					+ toDate
					+ " 23:59'::timestamp without time zone and valor>0 and v.recurso ='"
					+ idrec + "' and v.contratista ='" + idcont + "' "
					+ "order by 1";

		} else {
			sql = "select fecha::date, "
					+ (dato.equals("vp") ? "avg(ev.velocidad::double precision)"
							: "max(ev.velocidad::double precision)")
							+ " from eventos ev inner join vehiculos v on (ev.idvehiculo=v.idvehiculo) "
							+ "where fecha between '"
							+ fromDate
							+ " 00:00'::timestamp without time zone and '"
							+ toDate
							+ " 23:59'::timestamp without time zone and ev.velocidad::double precision>0 and v.recurso ='"
							+ idrec + "' and v.contratista ='" + idcont + "' "
							+ "group by 1 order by 1";
		}

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][][] getIndividualEvents(String rec, String cont,
			String fromDate, String toDate, String[] eve) throws Exception {

		String cat_sql = "select ('"
				+ fromDate
				+ "'::timestamp without time zone + i * '1 day'::interval)::date as day from generate_series(0, "
				+ "((date_part('epoch', '" + toDate
				+ "'::timestamp without time zone) - " + "date_part('epoch', '"
				+ fromDate
				+ "'::timestamp without time zone))/3600/24)::integer) i";

		String src_sql = " select ed.descripcion, e.fecha::date, count(e.idevento) "
				+ "from eventos e inner join eventosdesc ed on (e.evento = ed.evento) "
				+ "inner join vehiculos v on (e.idvehiculo = v.idvehiculo) "
				+ "where fecha between '"
				+ fromDate
				+ " 00:00'::timestamp without time zone and '"
				+ toDate
				+ " 23:59'::timestamp without time zone "
				+ "and e.evento = ed.evento and recurso || contratista in ('"
				+ rec + cont + "') " + "and e.evento in (";
		for (int i = 0; i < eve.length; i++) {
			src_sql += "'" + eve[i] + "'";
			if (i < eve.length - 1) {
				src_sql += ",";
			}
		}
		src_sql += ") group by 1,2 " + "order by 1,2";

		return crosstabN(src_sql, cat_sql);
	}

	public String[][][] getComparativeCrosstabItemForDays(String[] rec,
			String cont[], String munip, String fromDate, String toDate,
			String dato, String[] ttrabs, boolean includeOthers)
					throws Exception {
		String ttarray = ServerUtils.arrayToString(ttrabs);

		String sqlsource = "select nombre, fecha::date as day, "
				+ getDato(dato)
				+ " from (select * "
				+ "from \"vwResumenEstadisticas\" where ";

		if (rec != null) {
			sqlsource += "recurso||'-'||contratista in ('" + rec[0] + "-"
					+ cont[0] + "'";
			for (int i = 1; i < rec.length; i++) {
				sqlsource += ",'" + rec[i] + "-" + cont[i] + "'";
			}
			sqlsource += ") ";
		} else {
			sqlsource += "contratista in ('" + cont[0] + "'";
			for (int i = 1; i < cont.length; i++) {
				sqlsource += ",'" + cont[i] + "'";
			}
			sqlsource += ") ";
		}


		if (!munip.equals("all")) {
			sqlsource += "and municipio = '" + munip + "' ";
		}

		sqlsource += " and tipotrabajo in "
				+ ttarray
				+ ") vwr "
				+ (rec != null ? "inner join \"Recursos\" r on (vwr.recurso||'-'||vwr.contratista = r.recurso||'-'||r.contratista) "
						: "inner join \"Contratistas\" c on (vwr.contratista = c.contratista) ")
						+ "where fecha between '" + fromDate
						+ " 00:00'::timestamp without time zone " + "and '" + toDate
						+ " 23:59'::timestamp without time zone "
						+ "group by 1,2 order by 1,2";

		String sqlcategory = "select ('"
				+ fromDate
				+ "'::timestamp without time zone + i * '1 day'::interval)::date as day from generate_series(0, "
				+ "((date_part('epoch', '" + toDate
				+ "'::timestamp without time zone) - " + "date_part('epoch', '"
				+ fromDate
				+ "'::timestamp without time zone))/3600/24)::integer) i";
		return crosstabN(sqlsource, sqlcategory);
	}

	public Boolean addVehiculo(String idvehiculo, String placa, String recurso,
			String contratista) throws Exception {
		String sql = "INSERT INTO vehiculos( "
				+ "placa, idvehiculo, recurso, contratista) values (" +
				"'"+placa+"','"+idvehiculo+"','"+recurso+"','"+contratista+"')";
		if (db.executeUpdate(sql) != 0) {

			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean addVehiculo (String idvehiculo, String geocerca, String placa, String recurso,
			String contratista) throws Exception {
		String sql = "INSERT INTO vehiculos( "
				+ "placa, geocerca, idvehiculo, recurso, contratista) values (" +
				"'"+placa+"','"+geocerca+"','"+idvehiculo+"','"+recurso+"','"+contratista+"')";
		if (db.executeUpdate(sql) != 0) {

			return true;
		} else {
			throw new Exception(sql);
		}

	}
	public Boolean delContactoVehiculo(String idvehiculo, String idcontacto)throws Exception {
		String sql = "delete from contactos_vehiculos where idvehiculo='"+idvehiculo+"' and idcontacto='"+idcontacto+"'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public Boolean updateInsurance(String idvehiculo, String placa, String tipo, String marca,
			String ciudad, String colorCabina, String modelo,
			String tipoCarroceria, String colorCarroceria, boolean asegurado,
			String aseguradora, String tel_emergencia, String fechaPoliza,
			String vigenciaPoliza, String poliza, boolean estado,
			String tipo_modem, String min, String puk, String imei,String propietario,
			String modemUser, String modemPasswd, String geocerca)throws Exception {
		String sql = "update vehiculos set placa='" + placa + "', tipo='"
				+ tipo + "', marca='" + marca + "', color='" + colorCabina
				+ "', " + "modelo='" + modelo + "', asegurado='"
				+ (asegurado ? "S" : "N") + "', aseguradora='" + aseguradora
				+ "', tel_emergencia='" + tel_emergencia + "', fecha_poliza='"
				+ fechaPoliza + "', " + "vigencia_poliza='" + vigenciaPoliza
				+ "', poliza='" + poliza + "', tipo_carroceria='"
				+ tipoCarroceria + "', color_carroceria='" + colorCarroceria
				+ "'," + " tipo_modem='" + tipo_modem + "', modem_min='" + min
				+ "', modem_puk='" + puk + "', modem_imei='" + imei
				+ "', idpropietario=" + propietario + ", modem_user = '"
				+ modemUser + "', modem_passwd = '" + modemPasswd
				+ "', geocerca='"+geocerca+"'  where idvehiculo='" + idvehiculo + "'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public Boolean addAlarma(String idvehiculo, String idevento, String alarma, String gpio) throws Exception{
		String sql = "insert into gpio_vehiculo (idvehiculo, evento, alarma, idgpio) values ('"+idvehiculo+"', '"+idevento+"', '"+alarma+"', '"+gpio+"');";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean deleteAlarma(String idvehiculo, String idevento)throws Exception {
		String sql = "delete from gpio_vehiculo where idvehiculo='"+idvehiculo+"' and evento='"+idevento+"';";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}
	public Boolean addContactoVehiculo(String idvehiculo, String idContacto) throws Exception {
		String sql = "insert into contactos_vehiculos (idvehiculo, idcontacto) values ('"+idvehiculo+"','"+idContacto+"')";
		if (db.executeUpdate(sql) != 0) {

			return true;
		} else {
			throw new Exception(sql);
		}
	}


	public String[] getMoreInfoVehiculo(String idvehiculo) throws Exception {
		String sql = "select asegurado, aseguradora, tel_emergencia, " +
				"fecha_poliza, vigencia_poliza, poliza, idpropietario, " +
				"tipo_carroceria, color_carroceria,estado, tipo_modem, " +
				"modem_min, modem_puk,modem_imei, fechaingreso, modem_user, modem_passwd  " +
				"from vehiculos where idvehiculo='"+idvehiculo+"'";
		if (db.executeQuery(sql)){
			return db.getTable()[0];
		} else {
			throw new Exception(sql);
		}

	}

	public String[][] getAllContactos() throws Exception{
		String sql = "select idcontacto, nombre, nombre, telp, tels, celp, cels, email, parent as parentesco from contactos order by nombre ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getContactosVehiculo(String id) throws Exception {
		String sql = "select idcontacto, nombre, nombre, telp, tels, celp, cels, email, parent as parentesco from contactos_vehiculos inner join contactos using (idcontacto)where idvehiculo='"
				+ id + "'";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getEventosVehiculo(String idvehiculo) throws Exception{

		String sql="select * from (select idingreso as idevento,'ENTRADA '||descripcion as descripcion,  'N' as estatico from geocercas where idvehiculo='"+idvehiculo+"' and estado='A'"+
				" union "+
				"select idegreso as idevento,'SALIDA '||descripcion as descripcion,  'N' as estatico from geocercas where idvehiculo='"+idvehiculo+"' and estado='A'"+
				" union "+
				"select evento as idevento, descripcion, estatico from eventosdesc) as x order by idevento::integer";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String[][] getAlarmas(String id) throws Exception {
		String sql = "select evento, idvehiculo, alarma, idgpio, descripcion "+
				"from gpio_vehiculo inner join "+
				"(select idingreso as evento, 'ENTRADA '||descripcion as descripcion from geocercas where idvehiculo='"+id+"' "+
				"union "+
				"select idegreso as evento, 'SALIDA '||descripcion as descripcion from geocercas where idvehiculo='"+id+"' "+
				"union "+
				"select evento, descripcion from eventosdesc "+
				") as x using(evento) "+
				"where idvehiculo='"+id+"' order by evento::interval";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getContactosProceso(String idProceso) throws Exception {
		String sql = "select idcontacto,nombre, telp,tels, celp,cels, email, parent as parentesco from contactos where proceso='"+idProceso+"'";
		if (db.executeQuery(sql)){
			return db.getTable();} else {
				throw new Exception(sql);
			}
	}
	public Boolean addContactoProceso(String idproceso, String nombre,
			String fijo,String fijo2, String celular, String celular2, String email,String parent) throws Exception {
		String sql = "insert into contactos (nombre, telp, tels, celp, cels, email, parent, proceso) values (" +
				" '"+nombre+"'," +
				" '"+fijo+"'," +
				" '"+fijo2+"'," +
				" '"+celular+"'," +
				" '"+celular2+"'," +
				" '"+email+"'," +
				" '"+parent+"'," +
				" '"+idproceso+"')";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean updateContacto(String idContacto, String nombre,
			String fijo,String fijo2, String celular, String celular2, String email,String parent) throws Exception {
		String sql = "update contactos set" +
				" nombre='"+nombre+"'," +
				" telp='"+fijo+"'," +
				" tels='"+fijo2+"'," +
				" celp='"+celular+"'," +
				" cels='"+celular2+"'," +
				" email='"+email+"'," +
				" parent='"+parent+"'"+
				" where idcontacto='"+idContacto+"'";
		if (db.executeUpdate(sql) != 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String getIdVehiculo(String value) throws Exception {
		String sql = "select idvehiculo from vehiculos where placa = '" + value + "'";
		if (db.executeQuery(sql)){
			return db.getTable()[0][0];
		} else {
			throw new Exception(sql);
		}
	}

	public String getServerDate() throws Exception {
		String sql = "select to_char(localtimestamp, 'yyyy-MM-dd HH24:mi:ss')";
		if (db.executeQuery(sql)) {
			return db.getTable()[0][0];
		} else {
			throw new Exception(sql);
		}
	}

	public boolean quitJob(String trabajo, String tipotrabajo) throws Exception {
		String sql = "delete from \"TRecursos\" "
				+ "WHERE trabajo='" + trabajo + "' and tipotrabajo='" + tipotrabajo + "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String [][] getTrabajosDeRecurso(String recurso, String contratista, String dateProg) {
		/*	String sql = "select * from (select t.trabajo, t.tipotrabajo, t.latitud, t.longitud, t.estadotrabajo, t.inicioruta, t.prioridad, "
				+ "coalesce(to_char(t.fechaentrega, 'yyyy/MM/dd'), 'X') as fe, coalesce(t.horaentrega, 'X') as he, t.fecha, "
				+ "t.fechaprecierre, ( "
					+ "select case "
						+ "when t.estadotrabajo = '50' then fecharechazo "
						+ "else null "
						+ "end from \"TRechazos\" trh "
					+ "where t.trabajo=trh.trabajo and t.tipotrabajo=trh.tipotrabajo  "
					+ "order by fecharechazo desc limit 1) as fecharechazo, '' as ubicacion, t.direccion "
				+ "from \"get_vwTrabajos2\"('" + dateProg + "'::date) t , \"TRecursos\" tr "
				+ "where t.trabajo=tr.trabajo and t.tipotrabajo=tr.tipotrabajo "
				+ "and tr.recurso = '"+recurso+"' and tr.contratista = '"+contratista+"' and t.estadotrabajo <>'30' ) x "
				+ "order by coalesce(fechaprecierre, fecharechazo), estadotrabajo desc, inicioruta desc, prioridad desc, fe, he, fecha ";*/

		/*String sql = "select * from ( select t.trabajo, t.tipotrabajo,  t.estadotrabajo,  coalesce(to_char(t.fechaentrega, 'yyyy-MM-dd'), 'X') as fe,  t.fechaprecierre,tt.nombre, ("
				+ " select case when t.estadotrabajo = '50' then fecharechazo else null end  from \"TRechazos\" trh where t.trabajo=trh.trabajo and t.tipotrabajo=trh.tipotrabajo "
				+ " order by fecharechazo desc limit 1) as fecharechazo, '' as ubicacion, t.direccion  from \"get_vwTrabajos2\"('"
				+ dateProg
				+ "'::date) t , \"TRecursos\" tr,  \"TiposTrabajos\" tt "
				+ " where t.idviaje is not null and t.trabajo=tr.trabajo and t.tipotrabajo=tr.tipotrabajo and tr.recurso = '"
				+ recurso
				+ "' and tr.contratista = '"
				+ contratista
				+ "' and t.estadotrabajo <>'30' and tt.tipotrabajo=t.tipotrabajo ) x"
				+ " order by coalesce(fechaprecierre, fecharechazo), estadotrabajo desc,  fe";*/

		String sql = "select t.trabajo, t.tipotrabajo, t.estadotrabajo, coalesce(to_char(t.fechaentrega, 'yyyy-MM-dd'), 'X') as fe, "
				+"t.fechaprecierre, tt.nombre, ( select case when t.estadotrabajo = '50' then fecharechazo else null end "
				+"from \"TRechazos\" trh where t.trabajo=trh.trabajo and t.tipotrabajo=trh.tipotrabajo  order by fecharechazo desc limit 1) as fecharechazo, '' as ubicacion, t.direccion "
				+"from \"vwTrabajos\" t, \"TRecursos\" tr,  \"TiposTrabajos\" tt "
				+"where t.trabajo=tr.trabajo "
				+"and t.tipotrabajo=tr.tipotrabajo "
				+"and tr.recurso = '"+recurso+"' "
				+"and tr.contratista = '"+contratista+"' "
				+"and t.estadotrabajo <> '30' and "
				+"tt.tipotrabajo=t.tipotrabajo "
				//+"and t.fechaentrega = '"+dateProg+"'::date "
				+"order by coalesce(fechaprecierre), estadotrabajo desc,  fe ";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String getDateUpdate(String title) {
		String sql = "select fecha from interfazdetalles where nombre = '"
				+ title + "'";
		if (db.executeQuery(sql)) {
			String[][] tabla = db.getTable();
			if (tabla.length > 0) {
				return tabla[0][0];
			} else {
				return "";
			}
		} else {
			return null;
		}
	}

	public String[][] getConductoresOracle() throws Exception {
		String sql = "select DISTINCT cod_conductor, nombre_conductor from viaje_extreme"; //where estado='A'"; and rownum<3";

		try {
			if (db.executeQuery(sql)) {
				return db.getTable();
			} else {
				throw new Exception(sql);
			}

		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	public String addConductor(String trim, String trim2) throws Exception {
		String insertsql = "insert into \"Conductores\"(codigo, nombre,estado) VALUES ('"
				+ trim + "', '" + trim2 + "','A')";
		String updatesql = "update \"Conductores\" set codigo='"+trim+"',nombre='"+trim2+"', estado='A'" +
				" where codigo='"+trim+"'";
		String selectsql = "select * from \"Conductores\" where codigo='"+trim+"'";

		return executeValidatedInsert(selectsql, updatesql, insertsql);

	}



	public String[][] getEntregasOracle() throws Exception {

		String sql = "select numerotrabajo,tipotrabajo,municipio,cliente,direccion,"
				+ "observaciones,nombre_cliente,telefono," +
				" barrio,viaje,1,fechaprogramacion,cod_conductor,estado,placa, numentregasap "
				+ "from viaje_extreme "
				+ "where to_date(fechaprogramacion, 'YYYY-MM-DD')=to_date(sysdate, 'YYYY-MM-DD') and placa IS NOT NULL" +
				" and municipio IS NOT NULL" +
				" and numerotrabajo IS NOT NULL ";
		//+ "where tipotrabajo='OP' "
		//+ "and bodega_despacho = '72' "
		//+ "and to_date(fechaprogramacion, 'YYYY-MM-DD')=to_date(sysdate, 'YYYY-MM-DD') ";
		//+ "and trim(placa)='HGM958' "; //v_viajes_extreme ";//where rownum<3";
		//vehiculo
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public boolean addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario) throws Exception {

		String sql = "INSERT INTO \"Trabajos\"(trabajo, tipotrabajo, proceso, "
				+ "municipio, cliente, usuario, direccion, descripcion, "
				+ "ordenamiento, fecha, prioridad, latitud, longitud, "
				+ "numviaje, solicitante, barrio, telefono, fechaentrega, "
				+ "jornadaentrega, horaentrega) VALUES ('"
				+ codtrab + "','" + codttrab + "','" + proceso + "','" + munip
				+ "','" + codcli + "','" + usuario + "','" + dir + "','"
				+ desc + "', 0," + toDate(fecha) + "::date, '" + prioridad + "', "
				+ "(select latitud from \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + dir
				+ "')) and municipio='" + munip + "'), (select longitud "
				+ "from \"EjesViales\" where dirnoform = "
				+ "obtienecabecera(normalizador('" + dir
				+ "')) and municipio='" + munip + "'), '"
				+ numviaje + "', '" + nomcli + "', '"
				+ barrio + "', '" + telefono + "', "
				+ "localtimestamp"/*toDate(fechaentrega)*/ + "::date, " + Integer.parseInt(jornada) + "," + "'00:00:00'"/*toDate(hora)*/ + "::time)";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}


	public boolean addTripNovelty(String rec, String novedad) {
		SimpleDateFormat formato=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		String sql = "INSERT INTO \"NovedadesViaje\" (fecha,recurso,observacion,hora) VALUES ('"
				+ formato.format(new Date())
				+ "'::date,'"
				+ rec
				+ "','"
				+ novedad + "','" + formato.format(new Date()) + "'::time)";
		return db.executeUpdate(sql) > 0;
	}

	public DBConnection getDB(){
		return db;
	}
	public Boolean setAutoCommit(boolean value, DBConnection dbTemp) throws Exception {
		//Asegurarse de colocar el on delete restrict en esta tabla,
		//para que solo borre los trabajos que esten recien hechos

		dbTemp.autoCommit(value);
		String sql = "insert into \"LogCommit\" (valor,fecha) values ('"+value+"',localtimestamp)";
		if (db.executeUpdate(sql) >=0) {
			return true;
		} else {
			throw new Exception(sql);
		}

	}

	public String[][] getInfoTrabajosXCJamar(String ordenes) throws Exception {
		String sql = "SELECT trabajo, fecha from \"Trabajos\" where trabajo in "+ordenes;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}

	}


	public String[][] getInfoConducXCJamar(String conduc) throws Exception {
		String sql = "SELECT codigo, nombre from \"Conductores\" where codigo in "+conduc;
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}


	public String[][] getBitacoraNovedades(String recurso) throws Exception {

		String sql = "Select fecha,hora,recurso,observacion from \"NovedadesViaje\" where recurso ilike '"
				+ recurso + "' order by fecha,hora DESC limit 100";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String getTrabsSamePriority(String prioridad, String viaje) throws Exception {
		String sql = "select  trabajo from \"Trabajos\"  where prioritario="+prioridad+" and numviaje = '"+viaje+"'";
		if (db.executeQuery(sql)){

			if (db.next()) {
				return db.getString(1);
			}
			return null;
		} else {
			throw new Exception(sql);
		}
	}


	public boolean updateTrabajoPriority(String trabajo,String prioridad,String time)  throws Exception {


		String sql = "update \"Trabajos\"  set prioritario= " + prioridad+ ",horaentrega ='"+time+"'::time without time zone  where trabajo = '" + trabajo+ "' ";
		if (db.executeUpdate(sql) >= 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String [][] getWorksAddress(String recurso, String contratista, String dateProg) {

		String sql = "select distinct t.direccion "
				+"from \"vwTrabajos\" t, \"TRecursos\" tr,  \"TiposTrabajos\" tt "
				+"where t.trabajo=tr.trabajo "
				+"and t.tipotrabajo=tr.tipotrabajo "
				+"and tr.recurso = '"+recurso+"' "
				+"and tr.contratista = '"+contratista+"' "
				+"and t.estadotrabajo <> '30' and "
				+"tt.tipotrabajo=t.tipotrabajo ";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}


	public String [][] getWorksPriority(String recurso, String contratista, String dateProg) {

		String sql = "select prioritario, t.trabajo, t.tipotrabajo,coalesce(to_char(t.fechaentrega, 'yyyy-MM-dd'), 'X') as fe, " +
				"t.horaentrega, t.direccion,t.estadotrabajo,t.ordenamiento "
				+"from \"vwTrabajos\" t, \"TRecursos\" tr,  \"TiposTrabajos\" tt "
				+"where t.trabajo=tr.trabajo "
				+"and t.tipotrabajo=tr.tipotrabajo "
				+"and tr.recurso = '"+recurso+"' "
				+"and tr.contratista = '"+contratista+"' "
				+"and t.estadotrabajo <> '30' and "
				+"tt.tipotrabajo=t.tipotrabajo "
				//+"and t.fecha::date = '"+dateProg+"'::date "
				+"order by  prioritario asc,ordenamiento";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public boolean deleteTrabajoAnulado(String id) throws Exception {
		//Asegurarse de colocar el on delete restrict en esta tabla,
		//para que solo borre los trabajos que esten recien hechos
		String sql = "delete from \"Trabajos\" where trabajo='" + id+"'";
		if (db.executeUpdate(sql) >= 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public String getReporteTrafico(String viaje){
		String sql ="";

		return sql;

	}

	public DBConnection getDBConnection() {
		return db;
	}

	public String getRepTraficoQuery(String fecha) {
		// TODO Auto-generated method stub
		String sql="Select DISTINCT  pf.conductor,pf.muelle,'' as pack, pf.destino, "+
				"calculatetotaloperations('"+fecha+"'::date,v.placa,pf.muelle),c.nombre, "+
				"v.placa,c.celular, "+
				"'11:00:00'::time as hora1,calculatefinishedoperations('"+fecha+"'::date,'7:00:00'::time,'10:59:59'::time,v.placa,pf.muelle) as entregadas1, "+
				"calculatependantsoperations('"+fecha+"'::date,'7:00:00'::time,'10:59:59'::time,v.placa,pf.muelle) as pendientes1, "+
				"'15:00:00'::time as hora2,calculatefinishedoperations('"+fecha+"'::date,'11:00:00'::time,'14:59:59'::time,v.placa,pf.muelle) as entregadas2, "+
				"calculatependantsoperations('"+fecha+"'::date,'11:00:00'::time,'14:59:59'::time,v.placa,pf.muelle) as pendientes2, "+
				"'20:00:00'::time as hora3,calculatefinishedoperations('"+fecha+"'::date,'15:00:00'::time,'23:59:59'::time,v.placa,pf.muelle) as entregadas3, "+
				"calculatependantsoperations('"+fecha+"'::date,'15:00:00'::time,'23:59:59'::time,v.placa,pf.muelle) as pendientes3 "+
				"from \"ProgramaciondeFlotas\" pf "+
				"INNER JOIN vehiculos v ON pf.idvehiculo=v.idvehiculo "+
				"inner join \"Recursos\" r on v.recurso=r.recurso "+
				"left join \"TRecursos\" tr on (tr.recurso=r.recurso) "+
				"left join \"Trabajos\" t on (t.trabajo=tr.trabajo) "+
				"left JOIN \"Conductores\" c ON pf.conductor=c.codigo "+
				"where pf.fecha::date= '"+fecha+"'::date ";

		return sql;

	}

	public String isInterno(String recurso) throws Exception{

		recurso=recurso.replaceAll("-", "").trim();
		String sql= "select isinterno('"+recurso+"')";
		if (db.executeQuery(sql))
		{
			String[][] resultado = db.getTable();
			if(resultado.length > 0) {
				return resultado[0][0];
			} else {
				return "";
			}
		} else {
			throw new Exception(sql);
		}
	}
	public String[][] getInfoTrabMultiSelect(ArrayList<String[]> selectedKeys, String recurso, String contratista)
			throws Exception {
		String sql2 = "select trabajo,tipotrabajo, direccion, coalesce(codcli, ''), nomrecurso, "
				+ " nommunip,atraso,atraso, t.barrio, t.nomcli, t.nombresus, t.cor, t.sector, "
				+ "t.tipoaviso, t.departamento " + "from \"vwTrabajosAll\" t   where (trabajo||tipotrabajo in (";
		String sql="SELECT trabajo, tipotrabajo, direccion, COALESCE (codcli, ''),nomrecurso,nommunip,atraso,atraso,T .barrio,T .nomcli, '' AS nombresus,'' AS cor,'' AS sector,'' AS tipoaviso,'' AS departamento FROM \"vwTrabajosAll\" T WHERE ( trabajo || tipotrabajo IN (";
		for (int i = 0; i < selectedKeys.size(); i++) {
			sql += "'" + selectedKeys.get(i)[0] + selectedKeys.get(i)[1] + "'"
					+ (i != selectedKeys.size() - 1 ? ", " : ")");
		}
		sql += "  ) order by tipotrabajo, nomrecurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}
	public String[][] getInfoTrabMultiSelectOld(ArrayList<String[]> selectedKeys,
			String recurso, String contratista) throws Exception {
		String sql = "select trabajo,tipotrabajo, direccion, coalesce(codcli, ''), nomrecurso, "
				+ " nommunip,atraso,atraso, t.barrio, t.nomcli, t.nombresus, t.cor, t.sector, "
				+ "t.tipoaviso, t.departamento "
				+ "from \"vwTrabajosAll\" t   where (trabajo||tipotrabajo in (";
		for (int i = 0; i < selectedKeys.size(); i++) {
			sql += "'" + selectedKeys.get(i)[0] + selectedKeys.get(i)[1] + "'"
					+ (i != selectedKeys.size() - 1 ? ", " : ")");
		}
		sql += "  ) order by tipotrabajo, nomrecurso";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getInfoRecursosProcesoConductores(String proceso)
			throws Exception {
		String sql = "select recurso, r.contratista, c.nombre, r.tiporecurso, tr.nombre, "
				+ "r.nombre, usuariomovil, clavemovil, "
				+ "coalesce(capacidad::double precision, 0) as capacidad, "
				+ "idconductor from \"Recursos\" r inner join \"Contratistas\" c using (contratista) "
				+ "inner join \"TiposRecursos\" tr using (tiporecurso) left join vehiculos v using (recurso) "
				+ "where r.estado = 'A' and r.contratista in "
				+ getContratistasProceso(proceso) + "";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getRecursosConEstado(String proceso) throws Exception {
		String sql = "SELECT recurso, contratista,tiporecurso, UPPER(nombre) AS nombre, estado, usuariomovil, "
				+ "clavemovil, estadomovil, latitud, longitud, fechagps, estadovehiculo, online, "
				+ "direccion, estadogps FROM \"Recursos\" r WHERE r.estado = 'A' ";
		if (!proceso.equals("*")) {
			sql += "and contratista in " + getContratistasProceso(proceso);
		}
		sql+=" ORDER BY nombre";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}


	public String[][] getResourceHours(String resource,String contractor) throws Exception {
		String sql = "select to_char(j.horainicial, 'hh24:mi')  ,to_char(j.horafinal, 'hh24:mi') ," +
				"r.recurso,h.nombre,j.nombre,TRUNC(EXTRACT(EPOCH FROM ( horafinal::interval -  horainicial::interval))/60)   as duracion "
				+"FROM \"Recursos\" r "
				+"JOIN \"Horarios\" h ON h.codigo= r.horario "
				+"JOIN \"HorariosJornadas\" hj ON hj.horario=h.codigo "
				+"JOIN \"Jornadas\" j ON j.idjornada=hj.idjornada  "
				+"WHERE recurso = '"+resource+"' and contratista = '"+contractor+"' "
				+"ORDER BY to_char(j.horainicial, 'hh24:mi')::interval";


		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getHorariosAll()  throws Exception {
		String sql = "select codigo, nombre from  \"Horarios\" where estado= 'A' ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String  getResourceHour(String recurso,String contractor) throws Exception {
		String sql = "SELECT h.codigo as index "
				+"FROM \"Horarios\" h "
				+"JOIN \"Recursos\" r ON h.codigo= r.horario "
				+"WHERE r.recurso = '"+recurso+"' and contratista = '"+contractor+"'";
		try {
			if (db.executeQuery(sql)) {
				db.next();
				return db.getString(1);
			} else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			//throw new Exception(sql);
			return "";
		}
	}

	public boolean updateResourceHour(String code, String resource,String contractor) throws Exception {
		String sql = "UPDATE \"Recursos\" SET horario = '"+code+"' WHERE  recurso='"+resource+"' and contratista = '"+contractor+"' ";

		return db.executeUpdate(sql) != 0;
	}

	@SuppressWarnings("deprecation")
	public Boolean deleteAssigns(String idrec, String idcont,String date) throws Exception {
		String today = new SimpleDateFormat("yyyy/MM/dd").format(new Date(date));
		String sql = "delete from \"TRecursos\" where (recurso = '"+idrec+"' and contratista = '"+idcont+"' "
				+ "and (trabajo, tipotrabajo) in (select trabajo, tipotrabajo "
				+ "from \"Trabajos\" where estadotrabajo = '"+CommonsUtils.TASK_INIT+"')) ";
		return db.executeUpdate(sql) != 0;
	}

	@SuppressWarnings("deprecation")
	public boolean updateJob(String routeStart,int ordering,int duration,int index, String date,String time, String workCode,String WorkType)throws Exception{
		String today = new SimpleDateFormat("yyyy/MM/dd").format(new Date(date));
		String sql = "SELECT ('"+time+"'::time without time zone)::interval - (me.tiempo_tolerancia||' minutes')::interval, ('"+time+"'::time without time zone)::interval + (me.tiempo_tolerancia||' minutes')::interval "
				+" FROM \"Trabajos\" c "
				+"	JOIN \"TiposTrabajos\" tt using (tipotrabajo) "
				+" LEFT JOIN \"Mensajes\" me ON (me.idmensaje =tt.idmensaje) WHERE c.trabajo = '"+workCode+"' AND c.tipotrabajo = '"+WorkType+"' ";
		String[] tablaFechas;
		if (db.executeQuery(sql)) {
			tablaFechas = db.getTable()[0];
		} else {
			tablaFechas = null;
		}

		String insert = "UPDATE \"Trabajos\" SET "
				+"inicioruta='"+routeStart+"', ordenamiento = "+ordering+"::numeric, duracion = "+duration+"::integer, indexjornada = "+index+"::integer, "
				+"fechamodif=localtimestamp,  "
				+"fecha='"+today+" "+time+"'::timestamp without time zone, "
				+"fechaentrega='"+today+"'::timestamp without time zone,"
				+"horaentrega='"+time+"'::time without time zone "
				+", hora_minima = to_timestamp('"+tablaFechas[0]+"', 'HH:MI:SS')::time without time zone "
				+", hora_maxima = to_timestamp('"+tablaFechas[1]+"', 'HH:MI:SS')::time without time zone "
				+"WHERE trabajo = '"+workCode+"' AND tipotrabajo = '"+WorkType+"'";
		return db.executeUpdate(insert) != 0;
	}

	public boolean AsignJob(String workCode,String WorkType,String resource,String contractor)throws Exception{
		String select  = "SELECT count(*) as index FROM \"TRecursos\"  WHERE  recurso='"+resource+"' and contratista = '"+contractor+"' and trabajo='"+workCode+"' and tipotrabajo='"+WorkType+"'";

		try {
			if (db.executeQuery(select)) {
				db.next();

				int index = 0;
				if ((index=Integer.parseInt(db.getString(1)))> 0) {

					return true;
				}else{
					String insert = "INSERT INTO \"TRecursos\" (trabajo,tipotrabajo,recurso,contratista,fechaasigna) " +
							"VALUES ('"+workCode+"', '"+WorkType+"', '"+resource+"', '"+contractor+"', localtimestamp)";
					if(index==0) {
						return db.executeUpdate(insert) != 0;
					}
					return false;
				}
			} else {
				throw new Exception(select);
			}
		} catch (Exception e) {
			throw new Exception(select);
		}
	}

	public String[][] getJobsToSchedule(String tipo, String proceso,String orderby) throws Exception {

		String sql = "SELECT to_char(c.fecha, 'yyyy/MM/dd hh24:mi') as fecha, "
				+ "to_char(c.fecha, 'hh24:mi') as hora,"
				+ " c.trabajo, c.tipotrabajo, tt.nombre as nomtipotrab, "
				+ "c.cliente as codcli, c.solicitante as nomcli,c.municipio, m.nombre as nommunip,"
				+ "c.direccion, c.estadotrabajo, c.inicioruta, c.prioridad, c.latitud, c.longitud,"
				+ "r.nombre,coalesce(c.barrio,'Ninguno') as barrio,r.recurso,r.contratista,"
				+ "to_char( (tt.duracionpromedio::text||' minutes')::interval , 'hh24:mi'),"
				+ "tt.duracionpromedio,0 as duracionacum,0 as indexjornada, "
				+ "c.celular, me.asunto, replace(me.destinatario,'{NOMBRE_CLIENTE}',c.solicitante)"
				+ "||chr(13)||chr(10)||replace(me.detalletrabajo,'{DETALLE_TRABAJO}',c.descripcion)"
				+ "||chr(13)||chr(10)||me.remitente||chr(13)||chr(10)||replace(me.fechaentrega,'{YYYY-MM-DD HH:MI}',"
				//+"to_char(c.fecha,'yyyy/mm/dd HH24:MI')) as mensaje,c.proceso "
				+"to_char(c.fecha,'yyyy/mm/dd')) "
				+"||chr(13)||chr(10)||'Hora de entrega : ' || to_char(c.fecha::timestamp, 'HH24:MI:SS')::interval - (me.tiempo_tolerancia||' minutes')::interval || ' - ' || to_char(c.fecha::timestamp, 'HH24:MI:SS')::interval + (me.tiempo_tolerancia||' minutes')::interval "
				+" as mensaje"
				+",c.proceso,c.ordenamiento "
				+ "FROM \"Trabajos\" c "
				+ "JOIN \"TiposTrabajos\" tt using (tipotrabajo) "
				+ "LEFT JOIN \"Mensajes\" me ON (me.idmensaje =tt.idmensaje) "
				+ "LEFT JOIN \"Municipios\" m ON (m.municipio =c.municipio) "
				+ "LEFT OUTER JOIN \"TRecursos\" tr "
				+ "ON (c.trabajo = tr.trabajo and c.tipotrabajo = tr.tipotrabajo) "
				+ "LEFT OUTER JOIN \"Recursos\" r "
				+ "ON (r.recurso = tr.recurso and r.contratista = tr.contratista) "
				+ "WHERE c.proceso IN (SELECT proceso FROM \"Procesos\" where proceso= '"+proceso+"') AND c.estadotrabajo NOT IN ('30','20') ";
		if (tipo != null){
			if (tipo.equals("noAsig")) {
				sql += " and tr.recurso is null   ";
			}else if (tipo.equals("asig")) {
				sql += " and tr.recurso is not null ";
			}
		}

		if(orderby!=null
				&&!orderby.equals("")
				&&!orderby.equalsIgnoreCase("Georreferenciado")){
			sql += " order by "+ orderby;
		}else{
			sql += " order by c.latitud desc,c.longitud desc ,c.prioridad,c.ordenamiento";

		}
		//sql += " limit 10";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getJobsToSchedule( String proceso,String cont, String recurso, String dateProg) throws Exception {

		String sql = "SELECT "
				+ "to_char(c.fecha, 'yyyy/MM/dd hh24:mi') as fecha, "
				+ "to_char(c.fecha, 'hh24:mi') as hora,"
				+ " c.trabajo, c.tipotrabajo, tt.nombre as nomtipotrab, "
				+ "c.cliente as codcli, c.solicitante as nomcli,c.municipio, m.nombre as nommunip,"
				+ "c.direccion, c.estadotrabajo, c.inicioruta, c.prioridad, c.latitud, c.longitud,r.nombre,"
				+ "COALESCE(c.barrio,'Ninguno'), r.recurso,r.contratista,"
				+ "to_char( (tt.duracionpromedio::text||' minutes')::interval, 'hh24:mi'),"
				+ "tt.duracionpromedio,c.duracion,c.indexjornada,  "
				+ "c.celular, me.asunto, replace(me.destinatario,'{NOMBRE_CLIENTE}',c.solicitante)"
				+ "||chr(13)||chr(10)||replace(me.detalletrabajo,'{DETALLE_TRABAJO}',c.descripcion)"
				+ "||chr(13)||chr(10)||me.remitente||chr(13)||chr(10)||replace(me.fechaentrega,'{YYYY-MM-DD HH:MI}',"
				+"to_char(c.fecha,'yyyy/mm/dd'))"
				+" ||chr(13)||chr(10)||'Horario de entrega : entre ' || to_char(c.fecha::timestamp, 'HH24:MI:SS')::interval - (me.tiempo_tolerancia||' minutes')::interval || ' y ' || to_char(c.fecha::timestamp, 'HH24:MI:SS')::interval + (me.tiempo_tolerancia||' minutes')::interval "
				+" as mensaje"
				+",c.proceso,c.ordenamiento "
				+ "FROM \"Trabajos\" c "
				+ "JOIN \"TiposTrabajos\" tt using (tipotrabajo) "
				+ "LEFT JOIN \"Mensajes\" me ON (me.idmensaje =tt.idmensaje) "
				+ "LEFT JOIN \"Municipios\" m ON (m.municipio =c.municipio) "
				+ "JOIN \"TRecursos\" tr on (c.trabajo = tr.trabajo and c.tipotrabajo = tr.tipotrabajo) "
				+ "JOIN \"Recursos\" r on (r.recurso = tr.recurso and r.contratista = tr.contratista) "
				+ "WHERE c.fecha::date  = '"+dateProg+"'::date and  c.proceso  = '"+proceso+"'  "
				+ "AND tr.recurso = '"+recurso+"' AND tr.contratista ='"+cont+"' AND c.estadotrabajo NOT IN ('30','20')"
				+ "ORDER BY  c.inicioruta desc,c.ordenamiento, c.latitud,c.longitud ";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}

	}

	public String[][] getJornadasHorarios()  throws Exception {
		String sql = " select j.codigo, j.nombre "
				+ "from \"Jornadas\" j where estado='A' ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getJornadasHorarios(String horario)  throws Exception {
		String sql = " select j.codigo, j.nombre "
				+ "from \"HorariosJornadas\" hj "
				+ "inner join \"Jornadas\" j using (idjornada) "
				+ "inner join \"Horarios\" h on (h.codigo=hj.horario) "
				+ "where hj.horario='"+horario+"' ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public boolean deleteHorario(String codigo) throws Exception {
		String select = "select horario from \"Recursos\" where horario = '"+codigo+"'";
		String[][] resp;
		if (db.executeQuery(select)) {
			resp =  db.getTable();
		} else {
			resp = null;
		}

		if (resp != null && resp.length > 0) {
			throw new Exception("No se puede borrar el horario");
		} else {
			String sql = "update \"Horarios\" set estado='I' where codigo = '"+codigo+"' ";
			return db.executeUpdate(sql) != 0;
		}
	}

	public boolean deleteHorarioJornada(String codigo, String idjornada)
			throws Exception {
		String sql = "delete from  \"HorariosJornadas\"  where horario = '"+codigo+"' and idjornada=(select idjornada from \"Jornadas\"where codigo='"+idjornada+"')";
		return db.executeQuery(sql);
	}

	public Boolean addHorarioJornada(String horario, String jornada)
			throws Exception {
		String sql = "insert into \"HorariosJornadas\" (horario,idjornada) values ('"+horario+"', (select idjornada from \"Jornadas\"where codigo='"+jornada+"')) ";

		return db.executeUpdate(sql) != 0;
	}

	public String[][] getJornadasAll()  throws Exception {
		String sql = "select idjornada, codigo, nombre, horainicial, horafinal from  \"Jornadas\" where estado='A' ";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public Boolean addHorario(String codigo, String nombre) throws Exception {
		String sql = "insert into \"Horarios\" (codigo,nombre) values ('"+codigo+"', '"+nombre+"') ";
		return db.executeUpdate(sql) != 0;
	}

	public boolean updateHorario(String codigo, String nombre) throws Exception {
		String sql = "UPDATE \"Horarios\" SET nombre = '"+nombre+"' " + "WHERE  codigo='"+codigo+"' ";
		return db.executeQuery(sql);
	}

	public boolean deleteJornada(String id) throws Exception {
		String select ="select idjornada from \"HorariosJornadas\" where idjornada = '"+id+"'::bigint";
		String [][] resp;
		if (db.executeQuery(select)) {
			resp =  db.getTable();
		} else {
			resp = null;
		}
		if(resp!=null && resp.length>0){
			throw new Exception("No se puede borrar la jornada");
		}else{
			String sql = "update \"Jornadas\" set estado='I' where idjornada = '"+id+"'::bigint ";
			return db.executeUpdate(sql) != 0;
		}

	}

	public Boolean addJornada(String codigo, String nombre, String horaini,
			String horafin) throws Exception {
		String sql = "insert into \"Jornadas\" (codigo,nombre, horainicial, horafinal) values ('"+codigo+"', '"+nombre+"', "
				+ "'"+horaini+"'::time without time zone, '"+horafin+"'::time without time zone) ";
		return db.executeQuery(sql);
	}

	public boolean updateJornada(String idjornada, String codigo,
			String nombre, String horaini, String horafin) throws Exception {
		String sql = "UPDATE \"Jornadas\" SET nombre = '"+nombre+"', horainicial = '"+horaini+"'::time without time zone , "
				+ "horafinal = '"+horafin+"'::time without time zone "
				+ "WHERE idjornada = '"+idjornada+"'::bigint and codigo='"+codigo+"' ";
		return db.executeQuery(sql);
	}

	public boolean addRecurso(String id, String nombre, String tipo,
			String cont, String usuariom, String clavem, String codigosap, String horario)  throws Exception {

		if (horario != null) {
			String sql = "INSERT INTO \"Recursos\"(recurso, nombre, tiporecurso, "
					+ "contratista, estado, usuariomovil, clavemovil, estadomovil, codigosap, horario) "
					+ "VALUES('"+id+"', '"+nombre+"', '"+tipo+"', '"+cont+"', 'A', '"+usuariom+"', "
					+ "'"+clavem+"', 'OUT', '"+codigosap+"','"+horario+"')";
			return db.executeUpdate(sql) != 0;
		} else {
			String sql = "INSERT INTO \"Recursos\"(recurso, nombre, tiporecurso, "
					+ "contratista, estado, usuariomovil, clavemovil, estadomovil, codigosap) "
					+ "VALUES('"+id+"', '"+nombre+"', '"+tipo+"', '"+cont+"', 'A', '"+usuariom+"', '"+clavem+"', "
					+ "'OUT', '"+codigosap+"')";
			return db.executeUpdate(sql) != 0;
		}
	}

	public boolean updateRecurso(String id, String nombre, String tipo,
			String cont, String usuariom, String clavem, String codigosap,
			String horario)
					throws Exception {

		String sql = "update \"Recursos\" set nombre='" + nombre
				+ "',horario= '"+horario+"', codigosap='"+codigosap+"', tiporecurso='" + tipo + "' " +(!usuariom.equals("")?", usuariomovil='" + usuariom+"'":"")
				+ (!clavem.equals("")?", clavemovil='" + clavem+"'":"") + " where recurso='" + id
				+ "' and contratista = '" + cont+ "'";
		if (db.executeUpdate(sql) > 0) {
			return true;
		} else {
			throw new Exception(sql);
		}
	}

	public Boolean deleteItemPrecierre(String item, String ttrab)
			throws Exception {
		String sql = "update \"ItemsTipoTrabajo\" set estado = 'I' "
				+ "where item = '"+item+"'::bigint and tipotrabajo = '"+ttrab+"'";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean deleteCampoPrecierre(String item, String idcampo)
			throws Exception {
		String sql = "update \"ItemsCampos\" set estado = 'I' "
				+ "where item = '"+item+"'::bigint and id_campo = '"+idcampo+"'::bigint";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean updateCampoItemP(String idCampo, String idItemPrecierre,
			int orden) throws Exception {
		String sql = "Update \"ItemsCampos\" set orden = '"+orden+"' "
				+ "where id_campo = '"+idCampo+"'::bigint and item = '"+idItemPrecierre+"'::bigint";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean updateCampoPrecierre(String idcampo, String desc, String cod)
			throws Exception {
		String sql = "update \"CamposPrecierresV2\" set descripcion = '"+desc+"', codigo = '"+cod+"' "
				+ "where id_campo = '"+idcampo+"'::bigint";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean addCampoItemP(String campo, String item, int orden)
			throws Exception {
		String sql = "select estado AS index from \"ItemsCampos\" "
				+ "where item = '"+item+"'::bigint and id_campo = '"+campo+"'::bigint";

		try {
			if (db.executeQuery(sql)) {
				String[][] tabla = db.getTable();
				String v = null;
				if (tabla.length != 0) {
					v = tabla[0][0];
				}
				if (v  != null	&& v.trim().length() > 0) {
					if (v.equals("A")) {
						throw new Exception("El campo escogido se encuentra adicionado");
					}
					sql = "update \"ItemsCampos\" set estado = 'A' "
							+ "where item = '"+item+"'::bigint and id_campo = '"+campo+"'::bigint";
					return db.executeUpdate(sql) != 0;
				}
				sql = "Insert into \"ItemsCampos\" (item, id_campo, orden) "
						+ "values ('"+item+"'::bigint, '"+campo+"'::bigint, "+orden+")";
				return db.executeUpdate(sql) != 0;
			} else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public String addCampoPrecierreLista(String desc, String cod, String idcat,
			String tipo) throws Exception {
		String sql = "INSERT INTO \"CamposPrecierresV2\"(descripcion, tipo, id_categoria, codigo) "
				+ "VALUES ('"+desc+"', '"+tipo+"', '"+idcat+"', '"+cod+"') returning id_campo";
		try {
			if (db.executeQuery(sql)) {
				db.next();
				return db.getString(1);
			}
			return "";
		} catch (Exception e) {
			throw new Exception(sql);
		}
	}

	public String[][] getCamposPrecierres() throws Exception {
		String sql = "SELECT id_campo, cp.descripcion, tipo, c.descripcion as catdesc, "
				+ "cp.codigo, cp.descripcion || '(' || id_campo || ')' "
				+ "FROM \"CamposPrecierresV2\" cp left outer join \"CategoriasListasPrecierres\" c "
				+ "on (cp.id_categoria = c.id_categoria) where cp.estado = 'A' order by 2";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public Boolean addTTrabajoItemP(String ttrab, String item, int orden)
			throws Exception {
		String sql = "select estado AS index from \"ItemsTipoTrabajo\" "
				+ "where tipotrabajo = '"+ttrab+"' and item = '"+item+"'::bigint";
		try {
			String[][] resp;
			if (db.executeQuery(sql)) {
				resp = db.getTable();
				if ( resp  != null	&& resp.length > 0) {
					if (resp[0][0].equals("A")) {
						throw new Exception("El item escogido se encuentra adicionado");
					}
					sql = "update \"ItemsTipoTrabajo\" set estado = 'A', orden = "+orden+" "
							+ "where tipotrabajo = '"+ttrab+"' and item = '"+item+"'::bigint";
					return db.executeUpdate(sql) != 0;
				}
				sql = "Insert into \"ItemsTipoTrabajo\" (tipotrabajo, item, orden) "
						+ "values ('"+ttrab+"', '"+item+"'::bigint, "+orden+")";
				return db.executeUpdate(sql) != 0;
			}else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public String addItemPrecierre(String descripcion, boolean noRealizacion,
			String repeticion) throws Exception {
		String sql = "INSERT INTO \"ItemsPrecierresV2\"(descripcion, esnorealizacion, repeticion) "
				+ "VALUES ('"+descripcion+"', '"+(noRealizacion ? "S" : "N")+"', '"+repeticion+"') returning item";
		try {
			if (db.executeQuery(sql)) {
				db.next();
				return db.getString(1);
			}else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			throw new Exception(sql);
		}
	}

	public Boolean updateTTrabajoItem(String idItem, String ttrab, int orden)
			throws Exception {
		String sql = "Update \"ItemsTipoTrabajo\" set orden = "+orden+" where tipotrabajo = '"+ttrab+"' and item = '"+idItem+"'::bigint";
		return db.executeUpdate(sql) != 0;
	}

	public Boolean updateItemPrecierre(String item, String descripcion,
			boolean noRealizacion, String repeticion) throws Exception {
		String sql = "update \"ItemsPrecierresV2\" set descripcion = '"+descripcion+"', "
				+ "esnorealizacion = '"+(noRealizacion ? "S" : "N")+"', repeticion = '"+repeticion+"' where item = '"+item+"'::bigint";
		return db.executeUpdate(sql) != 0;
	}

	public String[][] getItemsPrecierres() throws Exception {
		String sql = "SELECT item, descripcion, descripcion || '(' || item || ')' "
				+ "FROM \"ItemsPrecierresV2\" where estado = 'A' order by 2";
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public String[][] getMessage(int idMesaage) throws Exception {
		String sql = "SELECT idmensaje, asunto, remitente, destinatario, fechaentrega, detalletrabajo, tiempo_tolerancia " +
				"FROM \"Mensajes\" " +
				"WHERE  idmensaje="+idMesaage+" ";

		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			return null;
		}
	}

	public boolean updateMessage(String subject, String from,String to,
			String dateDelivery,String jobDetails, String  timeTolerance,int idMesaage) throws Exception {
		String sql = "UPDATE \"Mensajes\" SET asunto ='"+subject+"',remitente='"+from+"',"
				+"destinatario='"+to+"', fechaentrega='"+dateDelivery+"',detalletrabajo='"+jobDetails+"' "
				+", tiempo_tolerancia = '"+timeTolerance+"' "
				+"WHERE idmensaje="+idMesaage+" ";

		return db.executeUpdate(sql) != 0;
	}

	public String addMessage(String subject, String from,String to,
			String dateDelivery, String jobDetails, String timeTolerance) throws Exception {
		String sql = "INSERT INTO \"Mensajes\"(asunto,remitente,destinatario, fechaentrega,detalletrabajo, tiempo_tolerancia) " +
				"VALUES('"+subject+"', '"+from+"', '"+to+"', '"+dateDelivery+"', '"+jobDetails+"', '"+timeTolerance+"') returning idmensaje ";
		try {
			if (db.executeQuery(sql)) {
				db.next();
				return db.getString(1);
			}else {
				throw new Exception(sql);
			}
		} catch (Exception e) {
			throw new Exception(sql);
		}

	}

	public boolean updateMessageJobType(String jobType, int idMesaage) throws Exception {
		String sql = "UPDATE \"TiposTrabajos\" SET idmensaje ="+idMesaage+" "
				+"WHERE tipotrabajo='"+jobType+"' ";
		return db.executeUpdate(sql) != 0;
	}

	public boolean addTipoTrab(String id, String nombre, boolean repcampo,
			String rango, String tiempopromedio) throws Exception {
		if (tiempopromedio != null && tiempopromedio.equalsIgnoreCase("")) {
			tiempopromedio = null;
		}
		if (rango != null && rango.trim().length() > 0
				&& !rango.equalsIgnoreCase("null")) {
			String sql = "insert into \"TiposTrabajos\"(tipotrabajo, nombre, repcampo, maxduracion, duracionpromedio) "
					+ "VALUES ('"+id+"', '"+nombre+"', '"+(repcampo ? "S"
							: "N")+"', '"+rango+"'::interval, '"+tiempopromedio+"'::numeric)";
			return db.executeUpdate(sql) != 0;
		}
		if (tiempopromedio == null) {
			String sql = "insert into \"TiposTrabajos\"(tipotrabajo, nombre, repcampo) "
					+ "VALUES ('"+id+"', '"+nombre+"', '"+(repcampo ? "S"
							: "N")+"')";
			return db.executeUpdate(sql) != 0;
		}
		String sql = "insert into \"TiposTrabajos\"(tipotrabajo, nombre, repcampo, duracionpromedio) "
				+ "VALUES ('"+id+"', '"+nombre+"', '"+(repcampo ? "S"
						: "N")+"', '"+ tiempopromedio+"'::numeric)";
		return db.executeUpdate(sql) != 0;
	}

	public boolean updateTipoTrab(String id, String nombre, boolean repcampo,
			String rango, String tiempopromedio) throws Exception {
		if (tiempopromedio != null && tiempopromedio.equalsIgnoreCase("")) {
			tiempopromedio = null;
		}
		if (rango != null && rango.trim().length() > 0
				&& !rango.equalsIgnoreCase("null")) {
			String sql = "update \"TiposTrabajos\" set nombre = '"+nombre+"', repcampo = '"+(repcampo ? "S"
					: "N")+"', maxduracion = '"+rango+"'::interval, duracionpromedio='"+tiempopromedio+"'::numeric "
					+ "where tipotrabajo = '"+id+"'";
			return db.executeUpdate(sql) != 0;
		}
		String sql = "update \"TiposTrabajos\" set nombre = '"+nombre+"', repcampo = '"+(repcampo ? "S" : "N")+"', duracionpromedio='"+tiempopromedio+"'::numeric "
				+ "where tipotrabajo = '"+id+"'";
		return db.executeUpdate(sql) != 0;
	}

	public String[][] getTrabajosInfo(String tipo, String proceso, String cont,
			String recurso) throws Exception {

		String sql = "select c.trabajo, c.tipotrabajo, c.nomtipotrab, tr.recurso, tr.contratista, "
				+ "c.municipio, c.nommunip, c.direccion, c.ordenamiento, c.prioridad, "
				+ "c.estadotrabajo, c.inicioruta, c.latitud, c.longitud, c.proceso, c.atraso, to_char(c.fecha, 'yyyy/MM/dd hh24:mi') as fecha, "
				+ "to_char(c.fechainicio, 'yyyy/MM/dd hh24:mi') as fechainicio,"
				+ "to_char(c.fechadescarga, 'yyyy/MM/dd hh24:mi') as fechadescarga, c.codcli, "
				+ "c.nomcli, c.descripcion, c.horasatraso, c.icono, c.significado, c.icontree, "
				+ "c.numviaje, c.barrio, c.telefono, to_char(c.fechaentrega, 'yyyy/MM/dd') as fechaentrega,"
				+ "c.codjornada, c.nomjornada, c.horaentrega, "
				+ "case when c.fechaprecierre::date <= c.fechaentrega and  "
				+ "		c.fechaprecierre::time < c.horafinal "
				+ "		then 'S' else 'N' end as cerradoenjornada, "
				+ "case when (c.fechaentrega + c.horaentrega::time) > localtimestamp "
				+ "		then 'S' else 'N' end as pendienteatiempo, "
				+ "coalesce((select realizo from \"TPrecierres\" tpre "
				+ "where tpre.trabajo = c.trabajo and tpre.tipotrabajo = c.tipotrabajo order by fechaprecierre desc limit 1), 'N') as realizo,c.prioritario "
				+ ", horasatraso2(c.fecha, 'now'::text::timestamp without time zone) AS horasatraso2 "
				+ "from \"vwTrabajos\"  c "
				+ "left outer join \"TRecursos\" tr "
				+ "on (c.trabajo = tr.trabajo and c.tipotrabajo = tr.tipotrabajo) where proceso='"
				+ proceso + "' ";
		if (tipo != null && tipo.equals("noUbic")) {
			sql += "and (not validaNumero(latitud) or not validaNumero(longitud)  or"
					+ " latitud = '0' or longitud = '0') ";
		}
		if (tipo != null && tipo.equals("noAsig")) {
			sql += " and recurso is null and validaNumero(latitud) and validaNumero(longitud)"
					+ " and latitud <> '0'"
					+ " and longitud <> '0' ";
		}
		if (recurso != null) {
			sql += " and recurso = '" + recurso + "' and contratista ='" + cont
					+ "'";
		}
		sql += " order by prioritario, ordenamiento ";
		if (tipo != null && tipo.equals("noUbic")) {
			sql+= " limit 10";
		}
		if (db.executeQuery(sql)) {
			return db.getTable();
		} else {
			throw new Exception(sql);
		}
	}

	public String getMensajeTrabajo(String trabajo, String tipotrabajo) throws Exception {
		String sql = "SELECT (replace(me.destinatario,'{NOMBRE_CLIENTE}',c.solicitante)||"
				+ "chr(13)||chr(10)||replace(replace(me.detalletrabajo,'null',''),'{DETALLE_TRABAJO}',c.descripcion)"
				+ "||chr(13)||chr(10)||me.remitente||chr(13)||chr(10)||replace(me.fechaentrega,'{YYYY-MM-DD HH:MI}',"
				+ "to_char(c.fecha,'yyyy/mm/dd')) ||chr(13)||chr(10)||'Horario de entrega : entre ' || "
				+ "to_char(c.hora_minima, 'HH24:MI:SS') || ' y ' || "
				+ "to_char(c.hora_maxima, 'HH24:MI:SS') ) "
				+ "FROM \"Trabajos\" c JOIN \"TiposTrabajos\" tt using (tipotrabajo) "
				+ "LEFT JOIN \"Mensajes\" me ON (me.idmensaje =tt.idmensaje) "
				+ "where trabajo = '"+trabajo+"' and tipotrabajo='"+tipotrabajo+"'";
		if (db.executeQuery(sql)){

			if (db.next()) {
				return db.getString(1);
			}
			return null;
		} else {
			throw new Exception(sql);
		}
	}
	
	public Boolean updatePtoInt(String id, String name, String value,
			String tipoPunto, String idPrcUSer, boolean b,String latitude,String longitude) throws Exception {

		String sql = "update puntosdeinteres  SET descripcion = '"+name+"', icono = '"+value+"'" +
				" ,latitud = '"+latitude+"',longitud = '"+longitude+"' where idpunto = '"+id+"'::bigint";
		String sql2 = "";
		String sql3 = "";

		if (tipoPunto.equals("Punto Global") && !b) {
			sql2 = "delete from proceso_ptoint where idpunto = '"+id+"'";
			sql3 = "insert into usuario_ptoint (idusuario,idpunto)  values ('"+idPrcUSer+"', '"+id+"'::bigint)";
			if (db.executeUpdate(sql) > 0) {
				if (db.executeUpdate(sql2) > 0) {
					return db.executeUpdate(sql3) > 0;
				}
			}
			return false;
		} else if (tipoPunto.equals("Punto de Usuario") && b) {
			sql2 = "delete from usuario_ptoint where idpunto = '"+id+"'::bigint";
			sql3 = "insert into proceso_ptoint (idproceso,idpunto)  values('"+idPrcUSer+"', '"+id+"')";
			if (db.executeUpdate(sql) > 0) {
				if (db.executeUpdate(sql2) > 0) {
					return db.executeUpdate(sql3) > 0;
				}
			}
			return false;
		}
		return db.executeUpdate(sql) > 0;
	}
	
	public boolean addJamarResult(String trabajo, String tipotrabajo,String calificacion,String causal,String fuente) throws Exception {
		String sql = "insert into \"ResultadosJamarCendis\"(trabajo, tipotrabajo, calificacion, causal,fuente) values ('"
					+ trabajo + "', '" + tipotrabajo + "', '"+ calificacion + "', "
					+(causal !=null? "'"+causal+"'":"null" )+","
					+(fuente !=null? "'"+fuente+"'":"null" )+")";
			if (db.executeUpdate(sql) > 0) {
				return true;
			} else {
				throw new Exception(sql);
			}

	}

}
