package org.extreme.controlweb.server.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

import org.extreme.controlweb.server.util.ServerUtils;

public class DBConnection {
	private Connection conn;
	private Statement st;
	private ResultSet rs;
	private CallableStatement cs;
	private String dbDriver;
	private String db, user, passwd;
	private String connString;
	private boolean autoCommit;


	/*public DBConnection() throws SQLException, ClassNotFoundException {
		db = ServerUtils.DEFAULT_DB;
		user = ServerUtils.DB_USER;
		passwd =ServerUtils.DB_PASSWD;
		connString = ServerUtils.CONNECTION_STRING;
		init();
	}*/

	private void init() throws SQLException, ClassNotFoundException {
		try{
			Class.forName(ServerUtils.DB_DRIVER);

			if(conn != null && !conn.isClosed()) {
				conn.close();
			}
			if(st != null && !st.isClosed()) {
				st.close();
			}

			conn = DriverManager.getConnection(connString + db,
					user, passwd);
			st = conn.createStatement();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	private void init(String driver) throws SQLException, ClassNotFoundException {
		try{
			Class.forName(driver);

			if(conn != null && !conn.isClosed()) {
				conn.close();
			}
			if(st != null && !st.isClosed()) {
				st.close();
			}

			conn = DriverManager.getConnection(connString /*+ db*/,
					user, passwd);
			st = conn.createStatement();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}


	public void checkConnection() throws SQLException, ClassNotFoundException {
		try {
			if (dbDriver != null && dbDriver.equals(ServerUtils.DB_DRIVER_ORACLE)) {
				st.executeQuery("select 1 from dual ");
			} else {
				st.executeQuery("select 1");
			}
		} catch (SQLException e) {
			init();
		}
	}

	public DBConnection(String db, String user, String passwd) throws SQLException, ClassNotFoundException {
		this.db = db;
		this.user = user;
		this.passwd = passwd;
		connString = ServerUtils.CONNECTION_STRING;
		init();
	}

	public DBConnection(String connString, String db, String user, String passwd) throws SQLException, ClassNotFoundException {
		this.connString = connString;
		this.db = db;
		this.user = user;
		this.passwd = passwd;
		init();
	}


	public DBConnection(String dbDriver, String connString, String db, String user, String passwd) throws SQLException, ClassNotFoundException {
		this.dbDriver = dbDriver;
		this.connString = connString;
		this.db = db;
		this.user = user;
		this.passwd = passwd;
		autoCommit = true;
		init(ServerUtils.DB_DRIVER_ORACLE);
	}
	public boolean executeQuery(String sql) {
		try {
			closeQuery();
			checkConnection();

			rs = st.executeQuery(sql);
			return true;
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public int executeUpdate(String sql) {
		try {
			checkConnection();

			return st.executeUpdate(sql);
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean next() {
		try {
			if (rs != null) {
				return rs.next();
			} else {
				return false;
			}
		} catch (SQLException e) {
			return false;
		}
	}

	public String getString(int index) throws SQLException {
		return rs.getString(index);
	}

	public boolean closeQuery() {
		try {
			rs.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean closeAll() {
		try {
			rs.close();
			st.close();
			conn.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public String[] getColumn(int col) throws SQLException {
		ArrayList<String> array = new ArrayList<String>();
		while (next()) {
			array.add(getString(col));
		}
		String[] data = new String[array.size()];
		int i = 0;
		for (String s : array) {
			data[i++] = s;
		}
		return data;
	}

	public String[][] getTable() {
		try {
			ArrayList<String[]> array = new ArrayList<String[]>();
			int ncols = rs.getMetaData().getColumnCount();
			while (next()) {
				String[] v = new String[ncols];
				for (int i = 0; i < ncols; i++) {
					//v[i] = ServerUtils.encodeSymbols(rs.getString(i + 1));
					v[i] = rs.getString(i + 1);
				}
				array.add(v);
			}
			String[][] data = new String[array.size()][ncols];
			for (int i = 0; i < data.length; i++) {
				data[i] = array.get(i);
			}
			return data;
		} catch (SQLException e) {
			return null;
		}
	}

	@Deprecated
	public String[][] getTableNoEnc() {
		try {
			ArrayList<String[]> array = new ArrayList<String[]>();
			int ncols = rs.getMetaData().getColumnCount();
			while (next()) {
				String[] v = new String[ncols];
				for (int i = 0; i < ncols; i++) {
					v[i] = rs.getString(i + 1);
				}
				array.add(v);
			}
			String[][] data = new String[array.size()][ncols];
			for (int i = 0; i < data.length; i++) {
				data[i] = array.get(i);
			}
			return data;
		} catch (SQLException e) {
			return null;
		}
	}

	public boolean prepareCall(String call) {
		try {
			checkConnection();

			cs = conn.prepareCall(call);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean setInt(int arg, int num){
		try {
			cs.setInt(arg, num);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean setString(int arg, String str){
		try {
			cs.setString(arg, str);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean registerVarcharOutParameter(int arg) {
		try {
			cs.registerOutParameter(arg, Types.VARCHAR);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean registerNumberOutParameter(int arg) {
		try {
			cs.registerOutParameter(arg, Types.NUMERIC);
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean executeProcedure() {
		try {
			cs.executeUpdate();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public String getProcedureString(int arg) throws SQLException {
		return cs.getString(arg);
	}

	public int getProcedureInt(int arg) throws SQLException	{
		return cs.getInt(arg);
	}

	public String[][] addToTable(String[][] d) {
		try {
			ArrayList<String[]> array = new ArrayList<String[]>();
			int ncols = rs.getMetaData().getColumnCount();
			while (next()) {
				String[] v = new String[ncols];
				for (int i = 0; i < ncols; i++) {
					v[i] = rs.getString(i + 1);
				}
				array.add(v);
			}
			String[][] data = new String[array.size() + d.length][ncols];

			for (int i = 0; i < d.length; i++) {
				for (int j = 0; j < d[i].length; j++) {
					data[i][j] = d[i][j];
				}
			}

			for (int i = d.length; i < data.length; i++) {
				data[i] = array.get(i - d.length);
			}
			return data;
		} catch (SQLException e) {
			return null;
		}
	}

	public Connection getConnection() {
		return conn;
	}
	public void autoCommit(boolean autoCommit) throws Exception
	{
		try {
			conn.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
