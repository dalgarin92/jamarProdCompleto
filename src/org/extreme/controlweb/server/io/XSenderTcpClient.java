package org.extreme.controlweb.server.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;


public class XSenderTcpClient {

	private static PrintWriter out;
	private static BufferedReader in;
	private static Socket kkSocket;
	private static String fromServer;
	private static String ip = "192.168.0.20";
	private static int port = 9094;

	private static void openConnection() throws UnknownHostException, IOException{
		kkSocket = new Socket(ip, port);
		out = new PrintWriter(kkSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
		kkSocket.setSoTimeout(30000);
	}

	private static void closeConnection() throws IOException{
		out.close();
		in.close();
		kkSocket.close(); 
	}



	public static synchronized String sendMessage(String proceso,String asunto,Vector<String>emails,Vector<String[]>sms,Date fecha,String mensaje) throws UnknownHostException, IOException{
		openConnection();
		String comando = "";
		SimpleDateFormat sf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		comando="<Entrada></Entrada>";

		/*
		try
		{	SAXBuilder builder = new SAXBuilder(false);
		Reader in = new StringReader(comando);
			 Document doc = builder.build(in);
			 Element root = doc.getRootElement();
			 Element men = new Element("mensaje");
			 men.setAttribute("asunto",asunto);
			 men.setAttribute("aplicacion", "JAMAR");
			 men.setAttribute("proceso", proceso);
			 men.setAttribute("fecha_programacion",sf.format(fecha));
			 Element conten = new Element("contenido");
			 CDATA contenido = new CDATA(mensaje);
			 conten.addContent(contenido);
			 men.addContent(conten);
			 Element contactos = new Element("contactos");
			 for(String[]m_sms:sms){
				 	Element smsele = new Element("sms");
				 	Attribute a = new Attribute("operador", m_sms[0]);
				 	smsele.setAttribute(a);
				 	smsele.setText(m_sms[1]);
				 	contactos.addContent(smsele);
			 }
			 for(String m_em:emails){
					Element emaele = new Element("mail");
				 	emaele.setText(m_em);
				 	contactos.addContent(emaele);					
			 } 
			 men.addContent(contactos);
			 root.addContent(men);
			 //System.out.println(comando);
			 Format format = Format.getRawFormat();
			 format.setEncoding("ISO-8859-1");
			 format.setTextMode(TextMode.TRIM_FULL_WHITE);
			 format.setLineSeparator("");
			 XMLOutputter outXML = new XMLOutputter(format);
			 String salida = outXML.outputString(doc);
			 salida = salida.replaceFirst("\\r\\n","");
			 salida = salida.replace("\n","\\n");
			 System.out.println(salida);
			 out.println(salida);
			 waitForResponse();		
			 if((fromServer.contains("ERROR"))||!fromServer.contains("OK")||!fromServer.contains("ok")){
				 System.out.println("SOP ERROR: "+fromServer);
			 }			 

		} catch (JDOMException e)
		 {
			e.printStackTrace();
			return "Error al construir XML ";
	    }
		catch (Exception e)
		 {
			e.printStackTrace();
	    }*/

		return fromServer;
	}



	public static void waitForResponse() throws IOException {
		String buffer = "";
		fromServer = "";
		while ((buffer = in.readLine()) != null) {
			fromServer += buffer + "\n";
			//break;
		}
	}

	public static void main(String args[]) throws Exception {
		try {
			ip="192.168.0.20";
			port=9094;
			System.out.println("sending");
			Vector<String> mail=new Vector<String>();
			mail.add("jarroyo@extreme.com.co");
			XSenderTcpClient.sendMessage("JAMAR", 
					"Trabajo Precerrado "
					, mail, new Vector<String[]>(), new Date(), "\n Trabajo Precerrado Exitosamente");

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}