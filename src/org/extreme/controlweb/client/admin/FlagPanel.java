package org.extreme.controlweb.client.admin;

import com.google.gwt.user.client.ui.Image;
import com.gwtext.client.core.EventCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Component;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.TextFieldListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FormLayout;

public class FlagPanel extends MultiFieldPanel {

	private NumberField tfNumber;
	private FlagPanel reflectTo;
	private Panel panel;
	private boolean isInfinite;
	
	public int getValue(){
		return Integer.parseInt(getTextField().getValueAsString());
	}
	
	
	public FlagPanel(String url, FlagPanel reflectTo, boolean isInfinite) {
		super();
		this.setBorder(false);
		this.setPaddings(0, 10, 0 , 0);
		this.reflectTo = reflectTo;
		
		this.isInfinite = isInfinite;
		
		Image flagImage = new Image(url);
		this.addToRow(flagImage, 32);
		
		tfNumber = new NumberField();
		tfNumber.setAllowDecimals(false);
		tfNumber.setAllowNegative(false);
		tfNumber.setMinValue(1);
		//tfNumber.setMaxText("El valor de este campo debe ser mayor que el anterior");
		tfNumber.setMinText("El valor de este campo debe ser mayor que el anterior");
	
		tfNumber.setLabel("");
		tfNumber.setLabelSeparator("&nbsp;");
		if(isInfinite)
			tfNumber.setValue(9999999);
		//tfNumber.setHideLabel(true);
		//tfNumber.setWidth(150);
		panel = new Panel();
		panel.setLayout(new FormLayout());
		panel.setBorder(false);
		panel.setPaddings(0, 5, 0, 0);
		
		this.addToRow(panel, new ColumnLayoutData(1));
		reflectToPanel();
	}
	
	private void reflectToPanel(){
		if (reflectTo != null) {
			/*
			 *  case 38: //up arrow  
		     *  case 40: //down arrow
		     *  case 37: //left arrow
		     *  case 39: //right arrow
		     *  case 33: //page up  
		     *  case 34: //page down  
		     *  case 36: //home  
		     *  case 35: //end                  
		     *  case 13: //enter  
		     *  case 9:  //tab  
		     *  case 27: //esc  
		     *  case 16: //shift  
		     *  case 17: //ctrl  
		     *  case 18: //alt  
		     *  case 20: //caps lock
		     *  case 8:  //backspace  
		     *  case 46: //delete 
			 * */
			
			tfNumber.addListener(new TextFieldListenerAdapter() {
				public void onRender(Component component) {
					component.getEl().addListener("keyup", new EventCallback() {
						public void execute(EventObject e) {
							if(tfNumber.getValueAsString() != null && !tfNumber.getValueAsString().equals("")) {
								reflectTo.getTextField().setLabel(
										"De " + (tfNumber.getValue().intValue() + 1) + " a ...");
								reflectTo.setMinValue(Integer.parseInt(tfNumber.getValueAsString()) + 1);
								if(!reflectTo.isInfinite())
									reflectTo.enable();
							} else {
								reflectTo.getTextField().setLabel("");
								if(!reflectTo.isInfinite())
									reflectTo.disable();
									reflectTo.setMinValue(1);
							}
						}
					});
					reflect();
				}
			});
		}
		panel.add(tfNumber, new AnchorLayoutData("85%"));
	}
	
	public TextField getTextField(){
		return tfNumber;
	}
	
	public void setLabel(String label){
		tfNumber.setLabel(label);
	}
	
	public void disable() {
		tfNumber.disable();
	}
	
	public void enable() {
		tfNumber.enable();
	}
	
	public boolean isInfinite(){
		return isInfinite;
	}
	
	public void setMinValue(int minValue){
		tfNumber.setMinValue(minValue);
	}
	
	public void setValue(String value){
		tfNumber.setValue(value);
		//reflect();
	}

	public void reflect() {
		if(tfNumber.getValueAsString() != null && !tfNumber.getValueAsString().equals("")) {
			reflectTo.getTextField().setLabel(
					"De " + (tfNumber.getValue().intValue() + 1) + " a ...");
			reflectTo.setMinValue(Integer.parseInt(tfNumber.getValueAsString()) + 1);
			if(!reflectTo.isInfinite())
				reflectTo.enable();
		} else {
			reflectTo.getTextField().setLabel("");
			if(!reflectTo.isInfinite())
				reflectTo.disable();
				reflectTo.setMinValue(1);
		}
	}
	
}
