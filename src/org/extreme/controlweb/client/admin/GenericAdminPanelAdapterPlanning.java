package org.extreme.controlweb.client.admin;

import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;

@SuppressWarnings("unchecked")
public abstract class GenericAdminPanelAdapterPlanning extends GenericAdminPanelPlanning {

	public GenericAdminPanelAdapterPlanning(String title, String details,
			boolean modify, boolean numerate, boolean delete) {
		super(title, details, modify, numerate, delete);

	}

	public GenericAdminPanelAdapterPlanning(String title, String details,
			boolean modify, boolean numerate, boolean delete, String reorder) {
		super(title, details, modify, numerate, delete, reorder);
	}

	public GenericAdminPanelAdapterPlanning(String title, String details,
			boolean modify, boolean numerate, boolean delete, String reorder, int buttonDistance) {
		super(title, details, modify, numerate, delete, reorder, buttonDistance);
	}

	public GenericAdminPanelAdapterPlanning(String title, String details,
			boolean modify, boolean numerate, boolean delete,
			String comboPercentage, String centerPercentage,
			String gridPercentage) {
		super(title, details, modify, numerate, delete, comboPercentage,
				centerPercentage, gridPercentage);

	}

	public Button getButton(){
		return button;
	}

	public Panel getCenterPanel() {
		return centerPanel;
	}

	@Override
	protected void addFunction(Record[] recs) {		
	}

	@Override
	protected void deleteFunction(Record r) {		
	}

	@Override
	protected void detailFunction(Record r) {
	}	

	@Override
	protected void modifyFunction(Record r) {
	}

	@Override
	protected void reorderFunction(Record r) {
	}

}
