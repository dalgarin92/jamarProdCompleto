package org.extreme.controlweb.client.admin;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class ComboLoaderAdapter implements ComboLoader {

	private boolean forceSelection;
	
	public ComboLoaderAdapter(boolean forceSelection) {
		this.forceSelection = forceSelection;
	}
	
	public boolean isForceSelection() {
		return forceSelection;
	}
	
	@Override
	public void load(AsyncCallback<ArrayList<RowModel>> cb) {
	

	}

}
