package org.extreme.controlweb.client.admin.listeners;

import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StoreTraversalCallback;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.PaddedPanel;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;

public class MunipDetailListener implements MyGridOnCellClickListener {
	
	private MyGridPanel gridTTrabs;
	private MyGridPanel gridCont;
	private Record recordMunip;
	private Record recordTTrab;
	private ComboBox cbConts;
	
	private FieldSet rightFieldSet;
	
	
	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}
	
	public MunipDetailListener() {			
		gridCont = new MyGridPanel();		
	}
	
	Function cargarContratistas=new Function(){
		public void execute() {
			if(recordTTrab!=null){
				getStaticQueriesServiceAsync().getContratistasMunicipio(
						recordMunip.getAsString("id"),
						recordTTrab.getAsString("id"),
						new RPCFillTable(gridCont, rightFieldSet, null, false,
								true, true, new int[] { 100, 100 },
								new boolean[] { true, true }, null, null));
				cbConts.setValue("");				
			}			
		}
	};
	
	private FieldSet getLeftPanel(final Record rec){
		final FieldSet leftFieldSet = new FieldSet("Tipos de Trabajo");
		leftFieldSet.setLayout(new RowLayout());
		leftFieldSet.setHeight(200);
		leftFieldSet.setWidth(380);
		
		MultiFieldPanel formPanel = new MultiFieldPanel();		
		final ComboBox cbTiposTrab = new ComboBox();//ClientUtils.createCombo("Agregar Tipo", tpoTrabStore);
		formPanel.setPaddings(0, 0, 0, 15);
		formPanel.setBorder(false);
		
		gridTTrabs = new MyGridPanel();
		gridTTrabs.setHeight(200);
		gridTTrabs.setWidth(380);
		gridTTrabs.setEnableHdMenu(false);
		//Tipo trabajo
		
		final Function cargarTTrabajos= new Function(){

			public void execute() {
				getStaticQueriesServiceAsync().getTTrabajoMunicipio(
						recordMunip.getAsString("id"),
						new RPCFillTable(gridTTrabs, leftFieldSet, null, false, true,
								true, new int[] { 100, 100 }, new boolean[] { true,
										true }, null, null));
				cbTiposTrab.setValue("");
				
			}
		};
		gridTTrabs.addDeleteListener(new MyGridOnCellClickListener(){
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync()
						.delMunipTipoTrabajo(
								recordMunip.getAsString("id"),
								r.getAsString("id"),
								new MyBooleanAsyncCallback(
										"Tipo de trabajo eliminado del municipio exitosamente",
										cargarTTrabajos));
			}
		});		
		
		
		
		gridTTrabs.addListener(new MyGridOnCellClickListener(){
			public void onCellClick(Record r) {				
				recordTTrab=r;
				cargarContratistas.execute();
			}
		},MyGridPanel.ALL_COLUMNS);
		
		
		
		final TextField tf = new TextField();
		cbTiposTrab.setReadOnly(true);
		Panel p= new Panel();
		p.setLayout(new FitLayout());
		p.setBorder(false);
		getStaticQueriesServiceAsync().getAllTTrabajos(new RPCFillComboBoxHandler(cbTiposTrab, p, null));

		cbTiposTrab.addListener(new ComboBoxListenerAdapter(){			
			public void onSelect(ComboBox comboBox, Record record, int index) {
				tf.setValue(rec.getAsString("id"));					
			}
			
			public void onFocus(Field field) {
				Store store = ((ComboBox)field).getStore();
				store.clearFilter();
				store.commitChanges();
				store.filterBy(new StoreTraversalCallback(){
					public boolean execute(Record record) {
						boolean sw = false;
						if(gridTTrabs.getStore()!=null)
						{
							Record[] s = gridTTrabs.getStore().getRecords();
							int i = 0;
							while (i < s.length && !sw) {
								sw = s[i].getAsString("id").equals(record.getAsString("id"));
								i++;
							}							
						}
						return !sw;
					}
				});
			}
			
			public void onExpand(ComboBox comboBox) {
				onFocus(comboBox);
			}
		});
		formPanel.addToRow(p, 280);
		Button button = new Button("Agregar");
		button.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync().addMunipTipoTrabajo(
						recordMunip.getAsString("id"),
						cbTiposTrab.getValueAsString(),
						new MyBooleanAsyncCallback(
								"Tipo de trabajo adicionado exitosamente",
								cargarTTrabajos));
			}
		});
					
		formPanel.addToRow(button, new ColumnLayoutData(1));		
		leftFieldSet.add(formPanel);		
		cargarTTrabajos.execute();
		return leftFieldSet;
	}
	

	private FieldSet getRightPanel(final Record rec) {
		rightFieldSet = new FieldSet("Contratistas");
		rightFieldSet.setLayout(new RowLayout());
		rightFieldSet.setHeight(200);
		rightFieldSet.setWidth(400);
		
		MultiFieldPanel formPanel = new MultiFieldPanel();
		
		formPanel.setPaddings(0, 0, 0, 15);
		formPanel.setBorder(false);		
				
		gridCont.setHeight(200);
		gridCont.setWidth(380);		
		gridCont.setEnableHdMenu(false);
		
		gridCont.addDeleteListener(new MyGridOnCellClickListener(){
			public void onCellClick(Record r) {
				if(recordTTrab!=null){
					getStaticAdminQueriesServiceAsync()
							.delMunipTTrabajoContratista(
									recordMunip.getAsString("id"),
									recordTTrab.getAsString("id"),
									r.getAsString("id"),
									new MyBooleanAsyncCallback(
											"Contratista eliminado del tipo de trabajo en el municipio",
											cargarContratistas));
				}else{
					ClientUtils.alert("Alerta", "Debe seleccionar primero un tipo de trabajo", ClientUtils.ERROR);
				}
			}
		});
	
		cbConts = new ComboBox();//ClientUtils.createCombo("Agregar Cont.", contStore);		
		final TextField tf = new TextField();
		final TextField tf2 = new TextField();
		cbConts.setReadOnly(true);

		cbConts.addListener(new ComboBoxListenerAdapter(){
			
			public void onSelect(ComboBox comboBox, Record record, int index) {
				tf.setValue(rec.getAsString("id"));					
				tf2.setValue(gridTTrabs.getSelectionModel().getSelected()
						.getAsString("ttrab"));
			}
			
			public void onFocus(Field field) {
				Store store = ((ComboBox)field).getStore();
				store.clearFilter();
				store.commitChanges();
				store.filterBy(new StoreTraversalCallback(){
					public boolean execute(Record record) {
						boolean sw = false;
						if(gridCont.getStore()!=null){
							Record[] s = gridCont.getStore().getRecords();
							int i = 0;							
							while (i < s.length && !sw) {
								sw = s[i].getAsString("id").equals(record.getAsString("id"));
								i++;
							}														
						}
						return !sw;						
					}
				});
			}
			
			public void onExpand(ComboBox comboBox) {
				onFocus(comboBox);
			}
		});
		Panel p=new Panel();
		p.setLayout(new FitLayout());		
		p.setBorder(false);
		getStaticQueriesServiceAsync().getAllContratistas(new RPCFillComboBoxHandler(cbConts, p,0));
		formPanel.addToRow(p, 280);	
		Button buttonCont = new Button("Agregar");
		buttonCont.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync()
						.addMunipTTrabajoContratista(
								recordMunip.getAsString("id"),
								recordTTrab.getAsString("id"),
								cbConts.getValue(),
								new MyBooleanAsyncCallback(
										"Contratista adicionado al municipio en el tipo de trabajo",
										cargarContratistas));
			}
		});
		formPanel.addToRow(buttonCont, new ColumnLayoutData(1));
		rightFieldSet.add(formPanel);
		cargarContratistas.execute();	
		return rightFieldSet;
	}
	
	public void onCellClick(Record r) {
		recordMunip = r;

		Window wnd = new Window("Detalles del Municipio", true, false);
		wnd.setPaddings(5);
		wnd.setWidth(840);
		wnd.setHeight(330);
		wnd.setLayout(new FitLayout());
		wnd.setIconCls("icon-butterfly");

		MultiFieldPanel mfp = new MultiFieldPanel();
		mfp.addToRow(new PaddedPanel(getLeftPanel(recordMunip), 5), 400);
		mfp.addToRow(new PaddedPanel(getRightPanel(recordMunip), 5),new ColumnLayoutData(1));
		wnd.add(mfp);
		wnd.show();

	}
}
