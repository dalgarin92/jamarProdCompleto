package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.ComboLoaderAdapter;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapterPlanning;
import org.extreme.controlweb.client.admin.GenericAdminPanelPlanning;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StoreTraversalCallback;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.RowLayout;

public class ItemPrecierreListenerPlanning implements MyGridOnCellClickListener {

	private Window wnd;
	private String[][] _;
	private ControlWebGUI gui;

	public ItemPrecierreListenerPlanning(Record r, ControlWebGUI gui)
	{
		this.gui = gui;
		onCellClick(r);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onCellClick(Record r) {
		final Record record = r;
		wnd = new Window("Campos precierre", true, true);
		wnd.setLayout(new RowLayout());
		//final ComboBox cib = new ComboBox("Campo");
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(700);
		wnd.setHeight(500);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");
		final GenericAdminPanelPlanning gapItems = new GenericAdminPanelAdapterPlanning(null,
				null, true, false, true, "orden", 500) {
			@Override
			protected void loadGridFunction(
					AsyncCallback<ArrayList<RowModel>> rows) {
				gui.getQueriesServiceProxy().getCamposPrecierre(
						record.getAsString("id"), rows);
			}

			@Override
			protected void deleteFunction(Record r) {
				gui.getStaticAdminQueriesServiceAsync().deleteCampoPrecierres(
						record.getAsString("id"),
						r.getAsString("id"),
						new MyBooleanAsyncCallback("Campo precierre borrado",
								this.reloadFunction));
			}

			@Override
			protected void modifyFunction(Record r) {
				((ComboBox)this.getField(0)).setValue(r.getAsString("nombre"));
				this.getField(1).setValue(r.getAsString("codigo"));
				this.getField(2).setValue(r.getAsString("id_categoria"));
				this.getField(2).setDisabled(true);
				this.getField(3).setValue(r.getAsString("tipo"));
				this.getField(3).setDisabled(true);

			}

			@Override
			protected void reorderFunction(Record r) {
				getEl().mask("Espere un momento por favor...");
				gui.getStaticAdminQueriesServiceAsync().updateCampoPrecierreLista(
						r.getAsString("id"),
						record.getAsString("id"),
						r.getAsString("nombre"),
						r.getAsString("codigo"),
						r.getAsInteger("orden"),
						new MyBooleanAsyncCallback(this.reloadFunction));
			}

			@Override
			protected void addFunction(Record[] recs) {
				boolean esta = false;
				int i = 0;
				String cbValue = ((ComboBox)getField(0)).getEl().getValue();
				if(_.length > 0)
				{
					for (i = 0; i < _.length && !esta; i++) {
						if(_[i][5].equals(cbValue))
							esta = true;
					}
					i--;
				}
				/*boolean esta = false;
				Record[] valores = null;
				int i = 0;
				if (cbCampo.getStore().getRecords().length > 0){
					 valores = cbCampo.getStore().getRecords();
					for (i = 0; i < valores.length && !esta ; i++)
						if(valores[i].getAsString("nombreid").equals(recs[0].getAsString("id")))
							esta = true;
				 *i--;
				}*/
				if (getCurrentRecord() == null && esta){
					gui.getStaticAdminQueriesServiceAsync()
					.addCampoItem(
							_[i][0],
							record.getAsString("id"),
							this.getRowCount(),
							new MyBooleanAsyncCallback(
									"Campo precierre agregado",
									this.reloadFunction));
				}else if (getCurrentRecord() == null && !esta){
					// idcampo,descripcion, tipo, orden, item, id_categoria
					gui.getStaticAdminQueriesServiceAsync()
					.addCampoPrecierreLista(
							record.getAsString("id"),
							getField(1).getValueAsString() == null ? ""
									: getField(1).getValueAsString(),
									cbValue,
									getField(2).getValueAsString().equals(
											"texto") ||
											getField(2).getValueAsString().equals(
													"numerico") ||
													getField(2).getValueAsString().equals(
															"decimal") ||
															getField(2).getValueAsString().equals(
																	"direccion") ? null : getField(3)
																			.getValueAsString(),
																			getField(2).getValueAsString(),
																			this.getRowCount(),
																			new MyBooleanAsyncCallback(
																					"Campo precierre agregado",
																					this.reloadFunction));

				}else{
					gui.getStaticAdminQueriesServiceAsync()
					.updateCampoPrecierreLista(
							getCurrentRecord().getAsString("id"),
							record.getAsString("id"),
							cbValue,
							recs[1].getAsString("id"),
							getCurrentRecord().getAsInteger("orden"),
							new MyBooleanAsyncCallback(
									"Campo precierre actualizado",
									reloadFunction));
				}
				((TextField)getField(1)).disable();
				((ComboBox)getField(2)).disable();
				((MyComboBox)getField(3)).disable();

			}
		};

		final Function siEsta = new Function() {

			@Override
			public void execute() {
				boolean esta = false;
				int i = 0;
				if(_.length > 0)
				{
					String cbValue = ((ComboBox)gapItems.getField(0)).getEl().getValue();
					for (i = 0; i < _.length && !esta; i++) {
						if(_[i][5].equals(cbValue))
							esta = true;
					}
					i--;
				}
				/*boolean esta = false;
				int i =0;
				Record[] valores = cbCampo.getStore().getRecords();
				for (i = 0; i < valores.length && !esta ; i++)
					if(valores[i].getAsString("nombreid").equals(((ComboBox)gapItems.getField(0)).getValue()))
						esta = true;
				i--;*/
				((TextField)gapItems.getField(1)).disable();
				((ComboBox)gapItems.getField(2)).disable();
				((MyComboBox)gapItems.getField(3)).disable();
				((TextField)gapItems.getField(1)).setValue(_[i][4]);
				((ComboBox)gapItems.getField(2)).setValue(_[i][2]);
				((MyComboBox)gapItems.getField(3)).setValue(_[i][3]);
			}
		};

		final Function noEsta = new Function() {

			@Override
			public void execute() {
				((TextField)gapItems.getField(1)).enable();
				((ComboBox)gapItems.getField(2)).enable();
				((MyComboBox)gapItems.getField(3)).disable();
				((TextField)gapItems.getField(1)).setValue("");
				((ComboBox)gapItems.getField(2)).setValue("texto");
				((MyComboBox)gapItems.getField(3)).setValue("");
			}
		};

		ComboBox cbCampo = gapItems.addCombo("Campo",
				new ComboLoaderAdapter(false) { // 0
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				gui.getQueriesServiceProxy().getCamposPrecierre(cb);
			}
		}, null, 350);
		
		cbCampo.setWidth(166);
		cbCampo.setStyle("width: 166px;");

		cbCampo.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				boolean esta = false;
				for (int i = 0; i < _.length && !esta; i++) {
					if(_[i][5].equals(comboBox.getEl().getValue()))
						esta = true;
				}
				/*boolean esta = false;
					Record[] valores = cbCampo.getStore().getRecords();
					for (int i = 0; i < valores.length && !esta ; i++)
						if(valores[i].getAsString("nombreid").equals(record.getAsString("nombreid")))
							esta = true;*/
				if (esta)
					siEsta.execute();
				else
					noEsta.execute();
			}

			@Override
			public void onBlur(Field field) {
				boolean esta = false;
				for (int i = 0; i < _.length  && !esta; i++) {
					if(_[i][5].equals(field.getEl().getValue()))
						esta = true;
				}
				/*boolean esta = false;
					Record[] valores = cbCampo.getStore().getRecords();
					for (int i = 0; i < valores.length && !esta ; i++)
						if(valores[i].getAsString("id").equals(cbCampo.getValue()))
							esta = true;*/
				if (esta)
					siEsta.execute();
				else
					noEsta.execute();
			}
		});

		gapItems.addTextField("codigo"); // 1

		final ComboBox cbtipo = ClientUtils.createCombo("Tipo",
				new SimpleStore(new String[] { "id", "nombre" },
						new String[][] { { "texto", "Texto" },
						{"numerico", "Numerico"},
						{"decimal", "Decimal"},
						{"direccion","Direcci\u00f3n"},
						{ "lista", "Lista" },
						{ "multilista", "Multilista" }
				}));
		gapItems.addComboBox(cbtipo); // 2
		cbtipo.setEditable(false);

		final ComboBox cbcategoria = gapItems.addCombo("Categoria",
				new ComboLoader() { // 3
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				gui.getQueriesServiceProxy().getAllCategorias(cb);
			}
		}, null);
		cbcategoria.setDisabled(true);
		cbcategoria.setEditable(false);
		cbcategoria.addListener(new ComboBoxListenerAdapter() {

			@Override
			public void onFocus(Field field) {
				Store store = ((ComboBox) field).getStore();
				store.clearFilter();
				store.commitChanges();
				store.filterBy(new StoreTraversalCallback() {
					@Override
					public boolean execute(Record record) {
						if (record.getAsString("clase").equals(
								cbtipo.getValue()))
							return true;
						else
							return false;
					}
				});
			}

			@Override
			public void onExpand(ComboBox comboBox) {
				onFocus(comboBox);
			}
		});

		cbtipo.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				cbcategoria.setValue("");
				if (record.getAsString("id").equals("texto") ||
						record.getAsString("id").equals("numerico") ||
						record.getAsString("id").equals("decimal") ||
						record.getAsString("id").equals("direccion")) {
					cbcategoria.setDisabled(true);
				} else {
					cbcategoria.setDisabled(false);
				}
			}
		});
		gapItems.loadAllCombos();
		cbCampo.setReadOnly(false);
		wnd.add(gapItems);
		gui.getQueriesServiceProxy().getCamposPrecierreString(new AsyncCallback<String[][]>(
				) {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(String[][] result) {
				_ = result;
				//id_campo
				//cp.descripcion
				//tipo
				//c.descripcion as catdesc
				//cp.codigo
				//cp.descripcion || '(' || id_campo || ')'
			}
		});
		wnd.show();
		/*getStaticQueriesServiceAsync().getCamposPrecierre(
				new AsyncCallback<String[][]>() {

					@Override
					public void onFailure(Throwable caught) {
						ClientUtils.alert("Error", "Error 205", ClientUtils.ERROR);
					}

					@Override
					public void onSuccess(String[][] result) {

				        	final Store store = 
				        			new SimpleStore(new String[]{"id", "nombre", "tipo", "catdesc", "codigo", "nombreid"}
				        			, ((result.length > 0) ? result :
				        				new String[][]{}));   

							store.load();   

				        //cb.setForceSelection(true);
							cib.setMinChars(1);
				        	cib.setStore(store);
				        	cib.setDisplayField("nombreid");   
					        cib.setMode(ComboBox.LOCAL);   
					        cib.setTriggerAction(ComboBox.ALL);   
					        cib.setEmptyText("Seleccione Campo");   
					        cib.setLoadingText("Buscando...");   
					        cib.setTypeAhead(true);   
					        cib.setSelectOnFocus(true);   
					        cib.setWidth(150);   
					        cib.setHideTrigger(false);

					        cib.addListener(new ComboBoxListenerAdapter() {
								public void onSelect(ComboBox comboBox, Record record, int index) {

									boolean esta = false;
									Record[] valores = cib.getStore().getRecords();
									for (int i = 0; i < valores.length && !esta ; i++)
										if(valores[i].getAsString("nombreid").equals(record.getAsString("nombreid")))
											esta = true;
									if (esta)
										siEsta.execute();
									else
										noEsta.execute();
								}

								@Override
								public void onBlur(Field field) {
									boolean esta = false;
									Record[] valores = cib.getStore().getRecords();
									for (int i = 0; i < valores.length && !esta ; i++)
										if(valores[i].getAsString("nombreid").equals(cib.getValue()))
											esta = true;
									if (esta)
										siEsta.execute();
									else
										noEsta.execute();
								}
							});
					    gapItems.addComboBox(cib); //0

					}

				});	*/


	}
}
