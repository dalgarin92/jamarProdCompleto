package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.RowLayout;

public class ItemPrecierreListener implements MyGridOnCellClickListener {

	private Window wnd;

	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;
	}

	public void onCellClick(Record r) {
		final Record record = r;
		wnd = new Window("Campos precierre", true, true);
		wnd.setLayout(new RowLayout());
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(700);
		wnd.setHeight(500);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");
		final GenericAdminPanel gapItems = new GenericAdminPanelAdapter(
				null, null, true, false, true, "orden") {
			@Override
			protected void loadGridFunction(
					AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getCamposPrecierre(
						record.getAsString("id"), rows);
			}

			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync().deleteCampoPrecierres(
						r.getAsString("id"),
						new MyBooleanAsyncCallback("Campo precierre borrado",
								this.reloadFunction));
			}

			@Override
			protected void modifyFunction(Record r) {
				this.getField(0).setValue(r.getAsString("nombre"));
				this.getField(1).setValue(r.getAsString("id_categoria"));
				this.getField(1).setDisabled(true);
				this.getField(2).setValue(r.getAsString("tipo"));
				this.getField(2).setDisabled(true);
				
			}

			@Override
			protected void reorderFunction(Record r) {
				getEl().mask("Espere un momento por favor...");
				getStaticAdminQueriesServiceAsync().updateCampoPrecierreLista(
						r.getAsString("id"), r.getAsString("nombre"),
						r.getAsInteger("orden"),
						new MyBooleanAsyncCallback(this.reloadFunction));
			}

			@Override
			protected void addFunction(Record[] recs) {
				if (getCurrentRecord() != null) {
					getStaticAdminQueriesServiceAsync()
							.updateCampoPrecierreLista(
									getCurrentRecord().getAsString("id"),
									recs[0].getAsString("id"),
									getCurrentRecord().getAsInteger("orden"),
									new MyBooleanAsyncCallback("Campo precierre actualizado",reloadFunction));
				} else {
					// idcampo,descripcion, tipo, orden, item, id_categoria
					getStaticAdminQueriesServiceAsync().addCampoPrecierreLista(
							record.getAsString("id"),
							getField(0).getValueAsString(),
							getField(1).getValueAsString(),
							getField(2).getValueAsString(),
							this.getRowCount(),
							new MyBooleanAsyncCallback(
									"Campo precierre agregado",
									this.reloadFunction));
				}
			}
		};
		
		ArrayList<RowModel> values = new ArrayList<RowModel>();
		RowModelAdapter rma = new RowModelAdapter();
		rma.addValue("id", "lista");
		rma.addValue("nombre", "Lista");
		values.add(rma);
		rma = new RowModelAdapter();
		rma.addValue("id", "texto");
		rma.addValue("nombre", "Texto");
		values.add(rma);
		gapItems.addTextField("Nombre");
		gapItems.addCombo("Categoria", new ComboLoader() {
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getAllCategorias(cb);
			}
		}, false);
		gapItems.addStaticCombo("Tipo", values);
		gapItems.loadAllCombos();
		wnd.add(gapItems);
		wnd.show();
	}
}
