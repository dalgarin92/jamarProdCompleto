package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoaderAdapter;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapterPlanning;
import org.extreme.controlweb.client.admin.GenericAdminPanelPlanning;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.NameValuePair;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.TabPanelListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.FormPanel;
import com.gwtext.client.widgets.form.Label;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.CheckboxListenerAdapter;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.tree.ColumnNodeUI;
import com.gwtext.client.widgets.tree.ColumnTree;
import com.gwtext.client.widgets.tree.TreeNode;

public class TiposTrabajosDetListenerPlanning implements MyGridOnCellClickListener {

	private Window wnd;
	private ControlWebGUI gui;
	private String[][] result_;
	private Function function;

	public TiposTrabajosDetListenerPlanning(ControlWebGUI gui,Function function) {
		this.gui = gui;
		this.function= function;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onCellClick(Record r) {
		if (gui.getPerfil().equals("superadmin")
				|| gui.getPerfil().equals("admin")) {

			final Record record = r;
			wnd = new Window("Opciones tipos de trabajo...", true, true);
			wnd.setLayout(new RowLayout());
			wnd.setButtonAlign(Position.CENTER);
			wnd.setWidth(500);
			wnd.setHeight(530);
			wnd.setPaddings(5);
			wnd.setIconCls("icon-butterfly");

			TabPanel tab = new TabPanel();

			final GenericAdminPanelPlanning gapItems = new GenericAdminPanelAdapterPlanning(
					null, "Modificar campos precierre", true, false, true,
					"orden") {
				@Override
				protected void loadGridFunction(
						AsyncCallback<ArrayList<RowModel>> rows) {
					gui.getQueriesServiceProxy().getItemsPrecierre(
							record.getAsString("id"), rows);
				}

				@Override
				protected void deleteFunction(Record r) {
					gui.getStaticAdminQueriesServiceAsync()
							.deleteItemsPrecierres(
									r.getAsString("id"),
									record.getAsString("id"),
									new MyBooleanAsyncCallback(
											"Item precierre borrado exitosamente",
											this.reloadFunction));
				}

				@Override
				protected void detailFunction(Record r) {
					new ItemPrecierreListenerPlanning(r, gui);
				}

				@Override
				protected void addFunction(Record[] recs) {
					boolean esta = false;
					int i = 0;
					String cbValue = ((MyComboBox) getField(0)).getEl()
							.getValue();
					if (result_.length > 0) {
						for (i = 0; i < result_.length && !esta; i++) {
							if (result_[i][2].equals(cbValue))
								esta = true;
						}
						i--;
					}
					String num = "1";
					if (((NumberField) getField(3)) != null
							&& !((NumberField) getField(3)).getValueAsString()
									.equals("")
							&& ((Checkbox) getField(2)).getValue())
						num = ((NumberField) getField(3)).getValueAsString();
					if (getCurrentRecord() == null && esta)
						gui.getStaticAdminQueriesServiceAsync()
								.addTTrabajoItem(
										record.getAsString("id"),
										result_[i][0],
										this.getRowCount(),
										new MyBooleanAsyncCallback(
												"Item precierre agregado",
												this.reloadFunction));
					else if (getCurrentRecord() == null && !esta)
						gui.getStaticAdminQueriesServiceAsync()
								.addItemsPrecierre(
										record.getAsString("id"),
										cbValue,
										((Checkbox) getField(1)).getValue(),
										num,
										this.getRowCount(),
										new MyBooleanAsyncCallback(
												"Item precierre agregado",
												this.reloadFunction));
					else
						gui.getStaticAdminQueriesServiceAsync()
								.updateItemPrecierre(
										getCurrentRecord().getAsString("id"),
										cbValue,
										((Checkbox) getField(1)).getValue(),
										num,
										getCurrentRecord()
												.getAsInteger("orden"),
										record.getAsString("id"),
										new MyBooleanAsyncCallback(
												"Item precierre actualizado",
												reloadFunction));
				}

				@Override
				protected void modifyFunction(Record r) {
					getField(0).setValue(r.getAsString("nombre"));
					((Checkbox) getField(1)).setChecked(r.getAsString(
							"No Realizaci\u00f3n").equals("S"));
					boolean repeticion = !r.getAsString("Repetici\u00f3n")
							.equals("1")
							&& !r.getAsString("Repetici\u00f3n").equals("N");
					((Checkbox) getField(2)).setChecked(repeticion);
					if (repeticion) {
						String valor = r.getAsString("Repetici\u00f3n");
						((NumberField) getField(3)).setValue(valor
								.equals("inf") ? "0" : valor);
					} else
						((NumberField) getField(3)).setValue(r.getAsString(""));
				}

				@Override
				protected void reorderFunction(Record r) {
					getEl().mask("Espere un momento por favor...");
					gui.getStaticAdminQueriesServiceAsync()
							.updateItemPrecierreOrden(
									r.getAsString("id"),
									r.getAsInteger("orden"),
									record.getAsString("id"),
									new MyBooleanAsyncCallback(
											this.reloadFunction));
				}
			};
			gapItems.setTitle("Precierres");

			final Button b = new Button("Ver Arbol",
					new ButtonListenerAdapter() {
						@Override
						public void onClick(Button button, EventObject e) {
							final Window w = new Window("", false, true);
							w.setLayout(new FitLayout());
							w.setWidth(520);
							w.setHeight(500);

							final Panel panel = new Panel();
							panel.setLayout(new FitLayout());

							final ColumnTree coltree = new ColumnTree();

							coltree.setWidth(500);
							coltree.setHeight(150);
							coltree.setRootVisible(true);
							coltree.setAutoScroll(true);
							coltree.setTitle("Items Precierre");

							ColumnTree.Column cols[] = new ColumnTree.Column[2];
							cols[0] = coltree.new Column("Nombre", 300,
									"nombret");
							cols[1] = coltree.new Column("Tipo", 100, "tipot");
							// cols[2] = coltree.new Column("Orden", 100,
							// "ordent");
							coltree.setColumns(cols);

							NameValuePair nodeData[] = new NameValuePair[2];
							nodeData[0] = new NameValuePair("nombret", record
									.getAsString("nombre"));
							nodeData[1] = new NameValuePair("tipot", "");
							// nodeData[2] = new NameValuePair("ordent","");

							TreeNode tnode = ColumnNodeUI
									.getNewTreeNode(nodeData);
							tnode.setCls("master-task");
							tnode.setIconCls("task-folder");
							coltree.setRootNode(tnode);

							gui.getQueriesServiceProxy().getItemsPrecierre(
									record.getAsString("id"),
									new AsyncCallback<ArrayList<RowModel>>() {
										@Override
										public void onSuccess(
												ArrayList<RowModel> result) {
											if (result.size() > 0) {
												for (RowModel rm : result) {
													NameValuePair nodeData[] = new NameValuePair[2];

													nodeData[0] = new NameValuePair(
															"nombret",
															rm.getRowValues()[1] != null ? ((String) rm
																	.getRowValues()[1])
																	: "Ninguno");

													if (((String) rm
															.getRowValues()[3])
															.equals("N") == true) {
														nodeData[1] = new NameValuePair(
																"tipot",
																"Realizaci\u00F3n");
													} else {
														nodeData[1] = new NameValuePair(
																"tipot",
																"No Realizaci\u00F3n");
													}
													// nodeData[2] = new
													// NameValuePair("ordent",((String)rm.getRowValues()[2]));

													final TreeNode tnode = ColumnNodeUI
															.getNewTreeNode(nodeData);
													tnode.setIconCls("task");
													tnode.setCls("master-task");
													coltree.getRootNode()
															.appendChild(tnode);

													gui.getQueriesServiceProxy()
															.getCamposPrecierre(
																	((String) rm
																			.getRowValues()[0]),
																	new AsyncCallback<ArrayList<RowModel>>() {
																		@Override
																		public void onSuccess(
																				ArrayList<RowModel> result) {

																			for (RowModel rm : result) {
																				NameValuePair nodeData[] = new NameValuePair[2];

																				nodeData[0] = new NameValuePair(
																						"nombret",
																						((String) rm
																								.getRowValues()[1]));
																				// System.out.println("0."+((String)rm.getRowValues()[2]));

																				nodeData[1] = new NameValuePair(
																						"tipot",
																						(rm.getRowValues()[3] != null
																								&& !rm.getRowValues()[3]
																										.equals("null") ? ((String) rm
																								.getRowValues()[3])
																								: "Ninguno"));
																				// nodeData[2]
																				// =
																				// new
																				// NameValuePair("ordent","..."+((String)rm.getRowValues()[3]));

																				final TreeNode tnode1 = ColumnNodeUI
																						.getNewTreeNode(nodeData);
																				tnode1.setIconCls("task");
																				tnode1.setCls("master-task");
																				tnode.appendChild(tnode1);

																				gui.getQueriesServiceProxy()
																						.getOpcionesCategoria(
																								((String) rm
																										.getRowValues()[4]),
																								new AsyncCallback<ArrayList<RowModel>>() {
																									@Override
																									public void onSuccess(
																											ArrayList<RowModel> result) {
																										for (RowModel rm : result) {
																											NameValuePair nodeData[] = new NameValuePair[2];

																											nodeData[0] = new NameValuePair(
																													"nombret",
																													((String) rm
																															.getRowValues()[2]));
																											System.out
																													.println("1."
																															+ ((String) rm
																																	.getRowValues()[4]));
																											nodeData[1] = new NameValuePair(
																													"tipot",
																													rm.getRowValues()[4] != null ? ((String) rm
																															.getRowValues()[4])
																															: "Ninguno");
																											// nodeData[2]
																											// =
																											// new
																											// NameValuePair("ordent",((String)rm.getRowValues()[3]));

																											TreeNode tnode2 = ColumnNodeUI
																													.getNewTreeNode(nodeData);
																											tnode2.setIconCls("task");
																											tnode2.setCls("master-task");
																											tnode2.setLeaf(true);
																											tnode1.appendChild(tnode2);
																										}
																										w.getEl()
																												.unmask();
																									}

																									@Override
																									public void onFailure(
																											Throwable caught) {
																										w.getEl()
																												.unmask();
																										ClientUtils
																												.alert("Error",
																														"Error 206",
																														ClientUtils.ERROR);
																									}
																								});
																			}
																		}

																		@Override
																		public void onFailure(
																				Throwable caught) {
																			w.getEl()
																					.unmask();
																			ClientUtils
																					.alert("Error",
																							"Error 201",
																							ClientUtils.ERROR);
																		}
																	});

												}
											} else {
												w.getEl().unmask();
											}
											// coltree.getRootNode().expand(true,
											// true);

										}

										@Override
										public void onFailure(Throwable caught) {
											w.getEl().unmask();
											ClientUtils.alert("Error",
													"Error 200",
													ClientUtils.ERROR);
										}
									});

							panel.add(coltree);
							w.add(panel);
							w.show();

							w.getEl().mask("Cargando el arbol. Espere...");

						}
					});
			wnd.addButton(b);
			wnd.setButtonAlign(Position.RIGHT);

			final MyComboBox cbItem = gapItems.addCombo("Item",
					new ComboLoaderAdapter(false) { // 0
						@Override
						public void load(AsyncCallback<ArrayList<RowModel>> cb) {
							gui.getQueriesServiceProxy().getItemsPrecierre(cb);
						}
					}, null, 175);

			cbItem.addListener(new ComboBoxListenerAdapter() {
				@Override
				public void onSelect(ComboBox comboBox, Record record, int index) {

					boolean esta = false;
					for (int i = 0; i < result_.length && !esta; i++) {
						if (result_[i][0].equals(comboBox.getValueAsString()))
							esta = true;
					}
					if (esta) {
						gapItems.getField(1).setDisabled(true);
						gapItems.getField(2).setDisabled(true);
						gapItems.getField(3).setVisible(false);
					} else {
						gapItems.getField(1).setDisabled(false);
						gapItems.getField(2).setDisabled(false);
					}
				}

				@Override
				public void onBlur(Field field) {
					boolean esta = false;
					for (int i = 0; i < result_.length && !esta; i++) {
						if (result_[i][2].equals(field.getEl().getValue()))
							esta = true;
					}

					if (esta) {
						gapItems.getField(1).setDisabled(true);
						gapItems.getField(2).setDisabled(true);
						gapItems.getField(3).setVisible(false);
					} else {
						gapItems.getField(1).setDisabled(false);
						gapItems.getField(2).setDisabled(false);
					}
				}
			});

			gui.getQueriesServiceProxy().getItemsPrecierreString(
					new AsyncCallback<String[][]>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}

						@Override
						public void onSuccess(String[][] result) {
							result_ = result;
						}
					});

			gapItems.addCheckBox("Item de no realizaci\u00f3n"); // 1
			Checkbox chb = gapItems.addCheckBox("Item de Repetici\u00f3n"); // 2
			final NumberField nf = gapItems.addNumberField(
					"Numero de veces (0 = inf)", 0); // 3
			chb.addListener(new CheckboxListenerAdapter() {

				@Override
				public void onCheck(Checkbox field, boolean checked) {
					if (!field.isDisabled())
						if (checked)
							nf.setVisible(true);
						else
							nf.setVisible(false);
				}
			});
			gapItems.loadAllCombos();
			cbItem.setReadOnly(false);
			nf.setVisible(false);

			tab.add(gapItems);
			wnd.add(tab);

			FormPanel formPanel = new FormPanel();
			formPanel.setTitle("Conf. Mensaje");
			formPanel.setPaddings(15);
			// strips all Ext styling for the component
			formPanel.setBaseCls("x-plain");
			formPanel.setLabelWidth(55);

			formPanel.setWidth(500);
			formPanel.setHeight(300);

			// anchor width by percentage
			final TextField txtsubject = new TextField("Asunto", "subject");
			formPanel.add(txtsubject, new AnchorLayoutData("100%"));

			final TextField txtfrom = new TextField("Firma", "from");
			formPanel.add(txtfrom, new AnchorLayoutData("100%"));
			
			final NumberField txtTiempoTolerancia = new NumberField("Tolerancia (min)");
			txtTiempoTolerancia.setValue(0);
			formPanel.add(txtTiempoTolerancia, new AnchorLayoutData("100%"));

			final Label label = new Label("1 mensaje");

			final TextArea textArea = new TextArea("Subject", "subject");
			textArea.setDisabled(true);
			final Checkbox ckName = new Checkbox("Agregar nombre del cliente");
			formPanel.add(ckName);
			
			final Checkbox ckDesc = new Checkbox("Detalle del trabajo");


			MultiFieldPanel optionsPanel = new MultiFieldPanel();
			optionsPanel.setBaseCls("x-plain");
			optionsPanel.addToRow(ckDesc, 220);
			formPanel.add(optionsPanel, new AnchorLayoutData("100%"));

			textArea.setHideLabel(true);
			formPanel.add(textArea, new AnchorLayoutData("100% - 53"));
			formPanel.add(label);
			textArea.setHeight(250);
			textArea.setGrow(true);
			textArea.setGrowMin(250);
			textArea.setGrowMax(350);
			
			if(r.getAsInteger("idmensaje")>0){
				
				gui.getQueriesServiceProxy().getMessage(r.getAsInteger("idmensaje"),
						new AsyncCallback<ArrayList<RowModel>>() {

					@Override
					public void onFailure(Throwable caught) {						
						System.out.println(caught.getMessage());						
					}

					@Override
					public void onSuccess(ArrayList<RowModel> result) {
						
						if(result!=null&&!result.isEmpty()){
							Object[] res = result.get(0).getRowValues();
														
							txtsubject.setValue(res[1].toString());
							txtfrom.setValue(res[2].toString());
							ckName.setValue(res[3].toString()!=null);
							ckDesc.setValue(res[4].toString()!=null);
							txtTiempoTolerancia.setValue(Float.parseFloat(res[6].toString()));
						}
					}
				});
				
			}
			
			
			
			
			final Button button = new Button("Guardar",
					new ButtonListenerAdapter() {
						public void onClick(Button button, EventObject e) {
							
							if(record.getAsString("idmensaje")!=null&&!record.getAsString("idmensaje").isEmpty()){
								
								gui.getStaticAdminQueriesServiceAsync().updateMessage(			
										txtsubject.getValueAsString(), 
										txtfrom.getValueAsString(), 
										ckName.getValue()?"Sr(a): {NOMBRE_CLIENTE}":null,
												"Fecha de entrega : {YYYY-MM-DD HH:MI}", 
												ckDesc.getValue()?"Detalle: {DETALLE_TRABAJO}":null, 
														txtTiempoTolerancia.getValueAsString(),
														record.getAsInteger("idmensaje"), 
														new MyBooleanAsyncCallback("Mensaje actualizado",function));
								
							}else{
								

								gui.getStaticAdminQueriesServiceAsync().addMessage(
										txtsubject.getValueAsString(), 
										txtfrom.getValueAsString(), 
										ckName.getValue()?"Sr(a): {NOMBRE_CLIENTE}":null,
										"Fecha de entrega : {YYYY-MM-DD HH:MI}", 
										ckDesc.getValue()?"Detalle: {DETALLE_TRABAJO}":null,
										txtTiempoTolerancia.getValueAsString(),		
										 new AsyncCallback<String>() {

											@Override
											public void onFailure(Throwable caught) {
												// TODO Auto-generated method stub
												
											}

											@Override
											public void onSuccess(String result) {
											
												gui.getStaticAdminQueriesServiceAsync().
												updateMessageJobsType(record.getAsString("id"), Integer.parseInt(result), 
														new MyBooleanAsyncCallback("Mensaje creado exitosamente!",function));
												
											}
										});
						
							}
						}
					});
			
			
			
			
			final Button button2 = new Button("Ver mensaje",
					new ButtonListenerAdapter() {
						public void onClick(Button button, EventObject e) {
							String txt= "";
							textArea.setValue("");
							if(!txtsubject.getValueAsString().isEmpty())
								txt+=txtsubject.getValueAsString()+"\n";
							if (ckName.getValue())
								txt+="Sr(a): {NOMBRE_CLIENTE}\n";
							if (ckDesc.getValue())
								txt+="Detalle: {DETALLE_TRABAJO}\n";
							
							if(!txtfrom.getValueAsString().isEmpty())
								txt+=txtfrom.getValueAsString()+"\n";
							
							txt+="Fecha de entrega : {YYYY-MM-DD HH:MI}\n";	
							txt+="Horario de entrega: entre {HH:MI y HH:MI}\n";
							textArea.setValue(txt);
							int cont=0;
							int sum= 1;
							for (int i = 0; i < txt.length(); i++) {
								if(cont==150){
									cont=0;
									sum++;
								}
								cont++;
							}
							label.setText(sum+" mensaje(s) - "+ txt.length()+" caracteres" );
							

						}
					});
			optionsPanel.addToRow(button2, new ColumnLayoutData(1));

			button.setVisible(false);
			tab.addListener(new TabPanelListenerAdapter() {
				@Override
				public void onTabChange(TabPanel source, Panel tab) {
					if (tab.getTitle().equals("Precierres")) {
						button.setVisible(false);
						b.setVisible(true);
					} else {
						button.setVisible(true);
						b.setVisible(false);

					}

				}

			});
			tab.add(formPanel);
			wnd.addButton(button);
			wnd.show();
		} else {

		}
	}
}
