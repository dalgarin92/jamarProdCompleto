package org.extreme.controlweb.client.admin.listeners;

import org.extreme.controlweb.client.admin.FileUploadManagerWindow;
import org.extreme.controlweb.client.admin.MyPanelFactory;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.data.Node;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.event.TreeNodeListenerAdapter;

public class AdminTreeNodeClickListener extends TreeNodeListenerAdapter {

	TabPanel tabPanel;
	ControlWebGUI gui;
	public static final String JOBS_LABEL="Trabajos";
	public static final String RECS_LABEL="Recursos";
	public static final String UPLOAD_FILES_LABEL="Subir archivos";
	public static final String MUN_LABEL="Municipios";
	public static final String RECS_TYPES_LABEL="Tipos de Recursos";
	public static final String JOBS_TYPES_LABEL="Tipos de Trabajos";
	public static final String CONT_LABEL="Contratistas";
	public static final String CLIENTS_LABEL = "Clientes";
	public static final String REJECT_SUBJECT_LABEL = "Motivos de Rechazo";
	public static final String USERS_LABEL = "Usuarios";
	public static final String PROCESS_LABEL = "Procesos";
	public static final String LIST_CATEGORY_LABEL = "Categorias Listas";
	public static final String VEHICLES_LABEL = "Veh\u00edculos";
	public static final String TEMPLATES_FILES_LABEL = "Plantilla de archivos";
	public static final String CONTACTS_LABEL = "Contactos";
	public static final String DRIVERS_LABEL = "Conductores";
	public static final String PROG_VEHIC="Programaci\u00f3n de Vehiculos";
	public static final String MUELLES="Muelles";
	public static final String DESTINOS="Destinos";
	
	public static final String JORNADAS_LABEL = "Jornadas";
	public static final String HORARIOS_LABEL = "Horarios";
	
	
	
	public AdminTreeNodeClickListener(TabPanel tabPanel, ControlWebGUI gui) {
		this.tabPanel = tabPanel;
		this.gui = gui;
	}
	
	public void onClick(Node node, EventObject e) {
		String option = ((TreeNode) node).getText();
		if ((!option.equals(JOBS_LABEL) && !option.equals(RECS_LABEL))
				|| (gui.getProceso() != null 
					&& !gui.getProceso().getId().equals(""))) {
			if (tabPanel.findByID(((TreeNode) node).getText()) == null) {
				if (option.equals(UPLOAD_FILES_LABEL)) {
					if (gui.getProceso() != null) {
						FileUploadManagerWindow wnd = new FileUploadManagerWindow(gui, UPLOAD_FILES_LABEL);
						wnd.show();
					}else{
						 ClientUtils.alert("Error","Debe seleccionar primero el proceso a administrar.", ClientUtils.ERROR);
					}
				} else {
					Panel panel = MyPanelFactory.newPanel(option, gui);
					panel.setClosable(true);
					panel.setId(((TreeNode) node).getText());
					panel.setIconCls(((TreeNode) node).getIconCls());
					tabPanel.add(panel);
					tabPanel.setActiveTab(panel.getId());
				}
			} else
				tabPanel.setActiveTab(((TreeNode) node).getText());
		} else ClientUtils.alert("Error","Debe seleccionar primero el proceso a administrar.", ClientUtils.ERROR);
	}
}
