package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.layout.RowLayout;

public class TiposTrabajosDetListener implements MyGridOnCellClickListener {

private Window wnd;

private ControlWebGUI gui;
	
	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;
	
	public TiposTrabajosDetListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;
	}
	
	public void onCellClick(Record r) {
		if (gui.getPerfil().equals("superadmin")
				|| (!r.getAsString("id").equals("RCX")
						&& !r.getAsString("id").equals("CMD")
						&& !r.getAsString("id").equals("VIA") && !r
						.getAsString("id").equals("SUP"))) {
			final Record record = r;
			wnd = new Window("Items precierre", true, true);
			wnd.setLayout(new RowLayout());
			wnd.setButtonAlign(Position.CENTER);
			wnd.setWidth(450);
			wnd.setHeight(500);
			wnd.setPaddings(5);
			wnd.setIconCls("icon-butterfly");
			final GenericAdminPanel gapItems = new GenericAdminPanelAdapter(null, "Modificar campos precierre", true, false, true,"orden") {		
				@Override
				protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
					getStaticQueriesServiceAsync().getItemsPrecierre(
							record.getAsString("id"), rows);
				} 
			
				@Override
				protected void deleteFunction(Record r) {
					getStaticAdminQueriesServiceAsync().deleteItemsPrecierres(
							r.getAsString("id"),
							new MyBooleanAsyncCallback(
									"Item precierre borrado exitosamente",
									this.reloadFunction));
				}
				
				@Override
				protected void detailFunction(Record r) {
					new ItemPrecierreListener().onCellClick(r);
				}
				
				@Override
				protected void addFunction(Record[] recs) {
					if(getCurrentRecord()==null){
						getStaticAdminQueriesServiceAsync().addItemsPrecierre(
								record.getAsString("id"),
								recs[0].getAsString("id"),
								((Checkbox)getField(1)).getValue(),
								this.getRowCount(),
								new MyBooleanAsyncCallback("Item precierre agregado",
										this.reloadFunction));					
					}
					else
					{
						getStaticAdminQueriesServiceAsync().updateItemPrecierre(
								getCurrentRecord().getAsString("id"),
								recs[0].getAsString("id"),
								((Checkbox)getField(1)).getValue(),
								getCurrentRecord().getAsInteger("orden"),
								new MyBooleanAsyncCallback("Item precierre actualizado",
										this.reloadFunction));
					}
				}
				
				@Override
				protected void modifyFunction(Record r) {
					getField(0).setValue(r.getAsString("nombre"));
					((Checkbox)getField(1)).setChecked(r.getAsString("No Realizaci\u00f3n").equals("S"));
				}
				
				@Override
				protected void reorderFunction(Record r) {
					getEl().mask("Espere un momento por favor...");
					getStaticAdminQueriesServiceAsync().updateItemPrecierre(
							r.getAsString("id"), r.getAsString("nombre"),((Checkbox)getField(1)).getValue(),
							r.getAsInteger("orden"),new MyBooleanAsyncCallback(this.reloadFunction));
				}
			};		
			gapItems.addTextField("Nombre");
			gapItems.addCheckBox("Item de no realizaci\u00f3n");
			wnd.add(gapItems);		
			wnd.show();
		} else {
			
		}
	}
}
