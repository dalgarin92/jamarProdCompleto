package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.FlagPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.CheckboxListenerAdapter;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;

public class ModifyProcessListener extends GridCellListenerAdapter implements MyGridOnCellClickListener {	
	
	private Window wnd;
	private Function reloadFunction;
	
	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}

	public ModifyProcessListener(Function reloadFunction) {		
		this.reloadFunction=reloadFunction;
	}

	public void onCellClick(Record r) {
		final Record record = r;
		wnd = new Window("Modificar Proceso", true, true);
		wnd.setLayout(new RowLayout());
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(450);
		wnd.setHeight(595);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");

		FieldSet basicData = new FieldSet("Datos B\u00e1sicos");
		basicData.setLayout(new FormLayout());
		basicData.setLabelWidth(50);

		final TextField tfName = new TextField("Nombre");
		final Checkbox ckTurnos = new Checkbox("Maneja Turnos?");
		final Checkbox ckCont = new Checkbox("Usa Contratistas?");
		final Checkbox ckCentral = new Checkbox("S\u00f3lo Central?");
		

		tfName.setValue(record.getAsString("nombre"));
		ckTurnos.setValue(record.getAsString("Maneja turnos").equals("S"));
		ckCont.setValue(record.getAsString("Usa contratistas").equals("S"));
		ckCentral.setValue(record.getAsString("Solo central").equals("S"));

		basicData.add(tfName, new AnchorLayoutData("100%"));
		basicData.add(ckTurnos);
		basicData.add(ckCont);
		basicData.add(ckCentral);

		wnd.add(basicData);

		TabPanel tabPanel = new TabPanel();
		tabPanel.setHeight(250);
		GenericAdminPanel gapTiposTrabajo=new GenericAdminPanelAdapter("Tipos de Trabajo", null, false, false, true){
		
			@Override
			protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getTiposTrabajoAsRowModel(
						record.getAsString("id"), rows);
			}
			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync().delProcesoTTrabajo(
						record.getAsString("id"),
						r.getAsString("id"),
						new MyBooleanAsyncCallback(
								"Tipo de trabajo eliminado del proceso",
								this.reloadFunction));
			}
			
			@Override
			protected void addFunction(Record[] recs) {
				getStaticAdminQueriesServiceAsync().addTTrabajoProceso(
						record.getAsString("id"),
						recs[0].getAsString("id"),
						new MyBooleanAsyncCallback(
								"Tipo de trabajo asociado al proceso",
								this.reloadFunction));
			}
		};
		gapTiposTrabajo.addCombo("Tipo Trabajo", new ComboLoader() {
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getAllTTrabajosAsRowModel(cb);
			}
		}, true);
		gapTiposTrabajo.loadAllCombos();		
		tabPanel.add(gapTiposTrabajo);		
		GenericAdminPanel gapTRecursos=new GenericAdminPanelAdapter("Tipos de recursos",null, false, false, true) {
		
			@Override
			protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getProcesoTRecursos(
						record.getAsString("id"), rows);		
			}
			
			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync()
						.delProcesoTRecurso(
								record.getAsString("id"),
								r.getAsString("id"),
								new MyBooleanAsyncCallback(
										"Tipo de recurso borrado exitosamente del proceso",
										this.reloadFunction));
			}
			
			@Override
			protected void addFunction(Record[] recs) {
				getStaticAdminQueriesServiceAsync()
				.addProcesoTRecurso(
						record.getAsString("id"),
						recs[0].getAsString("id"),
						new MyBooleanAsyncCallback(
								"Tipo de recurso asociado exitosamente del proceso",
								this.reloadFunction));
			}
		
			
		};
		gapTRecursos.addCombo("Tipos de recursos", new ComboLoader(){
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getAllTiposRecurso(cb);
			}
		}, true);
		gapTRecursos.loadAllCombos();
		tabPanel.add(gapTRecursos);
		GenericAdminPanel gapMunicipios=new GenericAdminPanelAdapter("Municipios",null, false, false, true){
			
			@Override
			protected void loadGridFunction(
					AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getProcesoMunicipios(
						record.getAsString("id"), rows);
			}
			
			@Override
			protected void addFunction(Record[] recs) {
				getStaticAdminQueriesServiceAsync().addProcesoMunicipio(
						record.getAsString("id"),
						recs[0].getAsString("id"),
						new MyBooleanAsyncCallback(
								"Municipio creado al proceso exitosamente",
								this.reloadFunction));
			}
			
			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync().delProcesoMunicipio(
						record.getAsString("id"),
						r.getAsString("id"),
						new MyBooleanAsyncCallback(
								"Municipio eliminado exitosamente del proceso",
								this.reloadFunction));
			}
		};
		gapMunicipios.addCombo("Municipio", new ComboLoader(){
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getMunicipiosRowModel(cb);
			}
		}, true);		
		gapMunicipios.loadAllCombos();
		tabPanel.add(gapMunicipios);
		
		GenericAdminPanel gapContratistas=new GenericAdminPanelAdapter("Contratistas", null, false, false, true){
			
			@Override
			protected void loadGridFunction(
					AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getContratistasAsRowModel(
						record.getAsString("id"), rows);
			}
			
			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync()
						.delProcesoContratista(
								record.getAsString("id"),
								r.getAsString("id"),
								new MyBooleanAsyncCallback(
										"Contratista eliminado correctamente del proceso",
										this.reloadFunction));
			}
			
			@Override
			protected void addFunction(Record[] value) {
				getStaticAdminQueriesServiceAsync().addProcesoContratista(
						record.getAsString("id"),
						value[0].getAsString("id"),
						new MyBooleanAsyncCallback(
								"Contratista agregado exitosamente al proceso",
								this.reloadFunction));
			}
		};
		gapContratistas.addCombo("Contratista", new ComboLoader(){
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getContratistasAsRowModel(cb);		
			}
		}, true);
		gapContratistas.loadAllCombos();
		tabPanel.add(gapContratistas);
		
		wnd.add(tabPanel);

		final FieldSet tematicData = new FieldSet(
				"Configuraci\u00f3n de Tem\u00e1tico (Rangos en horas de atraso)");
		ckCentral.addListener(new CheckboxListenerAdapter(){
			@Override
			public void onCheck(Checkbox field, boolean checked) {
				tematicData.setVisible(!checked);
			}			
		});
		tematicData.setLabelWidth(10);
		tematicData.setLayout(new FormLayout());

		final FlagPanel redFlag = new FlagPanel("images/utiles/icons/redflag.png",
				null, true);
		final FlagPanel orangeFlag = new FlagPanel(
				"images/utiles/icons/orangeflag.png", redFlag, false);
		final FlagPanel blueFlag = new FlagPanel("images/utiles/icons/blueflag.png",
				orangeFlag, false);
		blueFlag.setLabel("De 0 a ...");

		blueFlag.setValue(record.getAsString("bflagf")!=null?record.getAsString("bflagf"):"24");
		orangeFlag.setValue(record.getAsString("oflagf")!=null?record.getAsString("oflagf"):"48");

		orangeFlag.disable();
		redFlag.disable();

		tematicData.add(blueFlag);
		tematicData.add(orangeFlag);
		tematicData.add(redFlag);

		wnd.add(tematicData);

		

		final Function function = new Function() {
			public void execute() {
				reloadFunction.execute();
				wnd.close();				
			}
		};

		Button btOK = new Button("Aceptar");
		
		btOK.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync().updateProceso(
						record.getAsString("id"), tfName.getValueAsString(),
						ckTurnos.getValue(), ckCont.getValue(),
						ckCentral.getValue(),blueFlag.getValue(),orangeFlag.getValue(), redFlag.getValue(),
						new MyBooleanAsyncCallback("Proceso actualizado",function));
			}
		});
		
		Button btCancel = new Button("Cancelar", new ButtonListenerAdapter() {
			public void onClick(Button button, EventObject e) {
				wnd.close();
			}
		});

		wnd.addButton(btOK);
		wnd.addButton(btCancel);
		wnd.show();
	}
}
