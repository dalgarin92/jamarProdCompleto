package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.StoreTraversalCallback;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.FitLayout;

public class TemplateClickListener implements MyGridOnCellClickListener {

	protected static QueriesServiceAsync serviceProxy = null;

	GenericAdminPanelAdapter gapa;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;
	}

	public void onCellClick(final Record r) {
		gapa = new GenericAdminPanelAdapter(null, "Modificar valores", false,
				false, true) {
			@Override
			protected void loadGridFunction(
					AsyncCallback<ArrayList<RowModel>> rows2) {
				getStaticQueriesServiceAsync().getCamposTemplate(
						r.getAsString("id"), rows2);
			}

			@Override
			protected void addFunction(Record[] recs) {
				if (getCurrentRecord() == null) {
					getStaticAdminQueriesServiceAsync().addCampoTemplate(
							r.getAsString("id"),
							getField(1).getValueAsString(),
							getField(0).getValueAsString(),
							new MyBooleanAsyncCallback("Campo adicionado",
									this.reloadFunction));
				} else {
					getStaticAdminQueriesServiceAsync().updateCampoTemplate(
							getCurrentRecord().getAsString("id"),
							getField(1).getValueAsString(),
							new MyBooleanAsyncCallback("Campo actualizado",
									this.reloadFunction));
				}
			}

			@Override
			protected void detailFunction(final Record r) {
				detalleValores(r);
			}

			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync().delCampoTemplate(
						r.getAsString("id"),
						new MyBooleanAsyncCallback("Campo eliminado",
								this.reloadFunction));
			}

			@Override
			protected void modifyFunction(Record r) {
				getField(0).setValue(r.getAsString("campo"));
				getField(1).setValue(r.getAsString("columna"));
			}
		};
		gapa.addCombo("Campo", new ComboLoader() {
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getCamposArchivo(
						r.getAsString("archivo"), cb);
			}
		}, new StoreTraversalCallback() {
			public boolean execute(Record record) {
				Record[] gridRecords = gapa.getGridRecords();
				for (Record r : gridRecords) {
					if (r.getAsString("idcampo").equals(
							record.getAsString("id"))) {
						return false;
					}
				}
				return true;
			}
		});
		gapa.addNumberField("Columna", 0);
		gapa.loadAllCombos();
		Window wnd = new Window("Modificar plantilla");
		wnd.setLayout(new FitLayout());
		wnd.setHeight(400);
		wnd.setWidth(450);
		wnd.setResizable(false);
		wnd.add(gapa);
		wnd.show();
	}

	private void detalleValores(final Record r) {
		final Window wnd2 = new Window("Valor");
		GenericAdminPanel valPanel = new GenericAdminPanelAdapter(null, null,
				true, true, true) {
			@Override
			protected void loadGridFunction(
					final AsyncCallback<ArrayList<RowModel>> rows3) {
				getStaticQueriesServiceAsync().getValoresCampoTemplate(
						r.getAsString("id"), rows3);
			}

			@Override
			protected void deleteFunction(Record r2) {
				getStaticAdminQueriesServiceAsync().delValorCampoTemplate(
						r2.getAsString("id"),
						new MyBooleanAsyncCallback("Valor eliminado",
								reloadFunction));
			}

			@Override
			protected void addFunction(Record[] recs) {
				getStaticAdminQueriesServiceAsync().addValorCampoTemplate(
						r.getAsString("id"),
						getField(0).getValueAsString(),
						getField(1).getValueAsString(),
						((MyComboBox) getField(1)).getSelectedRecord()
								.getAsString("nombre"),
						new MyBooleanAsyncCallback("Valor creado",
								reloadFunction));
			}
		};
		valPanel.addTextField("Valor de entrada");
		valPanel.addCombo("Valores", new ComboLoader() {
			@Override
			public void load(final AsyncCallback<ArrayList<RowModel>> cb) {
				gapa.getEl().mask("Cargando valores");
				getStaticQueriesServiceAsync().getValoresCampo(
						r.getAsString("idcampo"),
						new AsyncCallback<ArrayList<RowModel>>() {
							@Override
							public void onFailure(Throwable caught) {
								cb.onFailure(caught);
							}

							@Override
							public void onSuccess(ArrayList<RowModel> result) {
								if (result.size() == 0) {
									ClientUtils
											.alert(
													"Informaci\u00f3n",
													"El campo no tiene valores predefinidos",
													ClientUtils.INFO);
									wnd2.close();
								} else {
									cb.onSuccess(result);
									wnd2.getEl().unmask();
								}
								gapa.getEl().unmask();
							}
						});
			}
		}, true).setFiltered();
		valPanel.loadAllCombos();
		valPanel.setFormHeight(100);
		wnd2.setLayout(new FitLayout());
		wnd2.setHeight(400);
		wnd2.setWidth(450);
		wnd2.add(valPanel);
		wnd2.show();
		wnd2.getEl().mask("Cargando valores");
	}

}
