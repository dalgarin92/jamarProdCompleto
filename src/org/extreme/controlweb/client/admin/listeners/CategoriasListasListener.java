package org.extreme.controlweb.client.admin.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.RowLayout;

public class CategoriasListasListener implements MyGridOnCellClickListener {

	private Window wnd;

	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;
	}

	@Override
	public void onCellClick(Record r) {
		final Record record=r;
		wnd = new Window("Modificar Categoria", true, true);
		wnd.setLayout(new RowLayout());
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(450);
		wnd.setHeight(500);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");

		final TextField opcionText = new TextField("Opcion");
		opcionText.setBlankText("El campo no puede ser vacio");

		final TextField nombreText = new TextField("Nombre");
		nombreText.setBlankText("El campo no puede ser vacio");

		GenericAdminPanel gapOpciones= new GenericAdminPanelAdapter("Opciones", null, true, true, true,"orden") {
			@Override
			protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
				getStaticQueriesServiceAsync().getOpcionesCategoria(record.getAsString("id"), rows);
			}

			@Override
			protected void modifyFunction(Record r) {
				getField(0).setValue(r.getAsString("id"));
				getField(0).setDisabled(true);
				getField(1).setValue(r.getAsString("nombre"));
			}

			@Override
			protected void deleteFunction(Record r) {
				getStaticAdminQueriesServiceAsync().deleteOpcionCategoria(
						r.getAsString("id_lista"),
						new MyBooleanAsyncCallback("Opcion borrada",
								reloadFunction));
			}

			@Override
			protected void addFunction(Record[] recs) {
				if (!opcionText.getText().trim().equals("") && !nombreText.getText().trim().equals("")) {
					if(getCurrentRecord()!=null){
						getStaticAdminQueriesServiceAsync()
						.updateOpcionesCategoria(
								getCurrentRecord().getAsString("id_lista"),
								recs[1].getAsString("id"),
								new MyBooleanAsyncCallback(
										"Opcion modificada",
										reloadFunction));
					}else{
						getStaticAdminQueriesServiceAsync().addOpcionesCategoria(
								record.getAsString("id"),
								recs[0].getAsString("id"),
								recs[1].getAsString("id"),
								new MyBooleanAsyncCallback("Opcion agregada",
										reloadFunction));
					}
				}else{
					ClientUtils.showError("No puden ser existir campos vacios");
				}
			}

			@Override
			protected void reorderFunction(Record r) {
				getStaticAdminQueriesServiceAsync()
				.reorderOpcionesCategoria(r.getAsString("id_lista"),
						r.getAsString("orden"),
						new MyBooleanAsyncCallback(
								"Opcion modificada",
								reloadFunction));
			}
		};

		gapOpciones.addTextField(opcionText);
		gapOpciones.addTextField(nombreText);
		wnd.add(gapOpciones);

		wnd.show();
	}

}
