package org.extreme.controlweb.client.admin;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FileUpload;
import com.gwtext.client.core.Connection;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.data.XmlReader;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.Form;
import com.gwtext.client.widgets.form.FormPanel;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.form.event.FormListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;

public class FileUploadManagerWindow extends Window {

	FileUpload file = null;
	TextField spacer = null;
	NumberField offset = null;
	MyComboBox template = null;

	protected static QueriesServiceAsync serviceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	public FileUploadManagerWindow(final ControlWebGUI gui, String label) {
		super(label, true, true);
		setHeight(250);
		setWidth(300);
		setLayout(new FitLayout());
		
		Panel p = new Panel();
		p.setBorder(false);
		p.setPaddings(5);
		p.setLayout(new FormLayout());
		
		
		final FormPanel form = new FormPanel();
		form.addFormListener(new FormListenerAdapter() {
			@Override
			public void onActionComplete(Form form, int httpStatus,
					String responseText) {
				super.onActionComplete(form, httpStatus, responseText);
				FileUploadManagerWindow.this.getEl().unmask();
				
				if (responseText.contains("Fila") && responseText.contains("problema")) {
					Window w = new Window("Log", true, true);
					w.setPaddings(5);
					w.setHeight(400);
					w.setWidth(600);
					w.setLayout(new FitLayout());
					TextArea taLog = new TextArea();
					taLog.setReadOnly(true);
					taLog.setHideLabel(true);
					w.add(taLog);
					w.show();
					taLog.setValue(responseText.replaceAll("@@", "\n"));
				} else		
					ClientUtils.alert("Info", responseText.replaceAll("@@", "<br>"), ClientUtils.INFO);
			}

			@Override
			public void onActionFailed(Form form, int httpStatus,
					String responseText) {
				super.onActionFailed(form, httpStatus, responseText);
				ClientUtils.alert("Info", responseText, ClientUtils.ERROR);
			}

			@Override
			public boolean doBeforeAction(Form form) {
				FileUploadManagerWindow.this.getEl().mask(
						"Enviando archivo: <br/>" + file.getFilename()
								+ "<br/> Espere un momento por favor... ");
				return true;
			}
		});
		form.setFrame(true);
		form.setFileUpload(true);
		RecordDef recordDef = new RecordDef(new FieldDef[] {
				new StringFieldDef("template"), new StringFieldDef("spacer"),
				new StringFieldDef("offset") });
		final XmlReader reader = new XmlReader("contact", recordDef);

		reader.setSuccess("@success");

		RecordDef errorRecordDef = new RecordDef(new FieldDef[] {
				new StringFieldDef("id"), new StringFieldDef("msg") });

		XmlReader errorReader = new XmlReader("field", errorRecordDef);
		errorReader.setSuccess("@success");

		form.setReader(reader);
		form.setErrorReader(errorReader);

		setLayout(new FormLayout());
		setPaddings(0);
		template = new MyComboBox("Plantilla");
		//template.setResizable(true);
		template.setPosition(-10,0);
		template.setWidth(180);
		template.setName("template");
		getStaticQueriesServiceAsync().getTemplates(gui.getProceso().getId(),
				new RPCFillComplexComboHandler(template, p, 0));
		template.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				spacer
						.setDisabled(record.getAsString("separacion").equals(
								"f"));
			}
		});
		FieldSet fs = new FieldSet("Datos del template");
		fs.setLayout(new FormLayout());
		file = new FileUpload();
		file.setName("file");
		// file.getElement().setAttribute("accept", "text/plain");
		fs.add(file);
		spacer = new TextField("Caracter de separaci\u00f3n", "spacer");
		spacer.setDisabled(true);
		spacer.setValue(";");
		spacer.setMaxLength(1);
		fs.add(spacer);
		// form.add(spacer);
		offset = new NumberField("Offset", "offset");
		offset.setDecimalPrecision(0);
		offset.setAllowDecimals(false);
		offset.setAllowBlank(false);
		offset.setValue(1);
		fs.add(offset);
		// form.add(offset);
		form.add(fs);

		Button enviar = new Button("Enviar");
		enviar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!file.getFilename().equals("")) {
					String servlet = template.getSelectedRecord() == null ? null
							: template.getSelectedRecord().getAsString(
									"servlet");
					if (servlet == null
							&& template.getStore().getRecords().length > 0) {
						servlet = template.getStore().getAt(0).getAsString(
								"servlet");
					}

					String action = ClientUtils.SERVICES_URL + servlet;
					action = action + "?";
					action = action + "template=" + template.getValue() + "&";
					action = action + "offset=" + offset.getValueAsString()
							+ "&";
					action = action + "proceso=" + gui.getProceso().getId()
							+ "&";
					action = action + "usuario=" + gui.getUsuario().getId()
							+ "&";
					// String SSpacer=spacer.getValueAsString();
					if (template.getSelectedRecord().getAsString("separacion")
							.equals("t")) {
						action = action + "spacer=" + spacer.getValueAsString();
					}
					String message = "Enviando archivo: <br/>"
							+ file.getFilename()
							+ "<br/> Espere un momento por favor... ";
					/*
					 * UrlParam pTemplate=new
					 * UrlParam("template",template.getValue()); UrlParam
					 * pOffset=new UrlParam("offset",offset.getValueAsString());
					 * UrlParam pProceso=new
					 * UrlParam("proceso",gui.getProceso().getId()); UrlParam
					 * pUsuario=new
					 * UrlParam("usuario",gui.getUsuario().getId()); UrlParam
					 * pSpacer=new UrlParam("spacer",SSpacer); UrlParam[]
					 * params={pTemplate,pOffset,pProceso,pUsuario,pSpacer};
					 * //action=GWT.getModuleBaseURL()+action;
					 */form.getForm().submit(action, null, Connection.POST,
							message, false);
				}
			}
		});
		p.add(form);
		add(p);
		setButtonAlign(Position.CENTER);
		addButton(enviar);
	}
}
