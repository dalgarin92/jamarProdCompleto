package org.extreme.controlweb.client.admin;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StoreTraversalCallback;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;

public abstract class GenericAdminPanel extends Panel implements
		AsyncCallback<ArrayList<RowModel>> {

	public static final int PERMISOS_USUARIO = 1;

	protected MyGridPanel gridRecords;

	private Panel padded;

	private String details;

	private boolean modify;

	private boolean delete;

	private boolean numerate;
	
	private Panel comboPanel;
	
	private Record currentRecord;

	private ArrayList<Field> fieldsArray;
	private ArrayList<ComboLoader> loadersArray;
	
	protected Record getCurrentRecord() {
		return currentRecord;
	}
	
	protected abstract void deleteFunction(Record r);

	protected abstract void modifyFunction(Record r);

	protected abstract void detailFunction(Record r);
	
	protected abstract void reorderFunction(Record r);

	protected abstract void loadGridFunction(
			AsyncCallback<ArrayList<RowModel>> rows);


	protected final Function reloadFunction = new Function() {
		public void execute() {
			getEl().unmask();
			currentRecord=null;
			reload();
		}
	};

	private String reorder;

	protected Button button;

	private String msgNoResults;

	private MultiFieldPanel formPanel;

	
	public void reload() {
		loadGridFunction(this);
		cleanFields();
	}
	
	public void setFormHeight(int i) {		
		formPanel.setHeight(i);
	}
	
	protected abstract void addFunction(Record[] recs);

	public void setMsgNoResults(String msgNoResults) {
		this.msgNoResults = msgNoResults;
	}
	
	public void onSuccess(ArrayList<RowModel> result) {
		boolean sw = false;
		GWT.log(result.toString(), null);
		if (result != null) {
			if (!gridRecords.isConfigured() && result.size() > 0) {
				if(result.get(0) instanceof RowModelAdapter)
					gridRecords.configure(result.get(0).getColumnNames(),
							details, modify, delete, numerate,
							((RowModelAdapter) result.get(0)).getSizes(),
							((RowModelAdapter) result.get(0)).getVisibility(),
							reorder);
				else
					gridRecords.configure(result.get(0).getColumnNames(), details,
							modify, delete, numerate, null, null, reorder);
				sw = true;
			} else if(result.size() == 0){
				if(msgNoResults != null)
					ClientUtils.alert("Info", msgNoResults, ClientUtils.INFO);
			}
			
			gridRecords.loadRows(result);
			if (sw) {
				padded.add(gridRecords);
				padded.doLayout();
			}
		}
	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
	}
	
	public GenericAdminPanel(String title, String details,
			boolean modify, boolean numerate, boolean delete, String reorder) {
		super();
		initialize(title, details, modify, numerate, delete, reorder);
	}

	public GenericAdminPanel(String title, String details,
			boolean modify, boolean numerate, boolean delete) {
		super();
		initialize(title, details, modify, numerate, delete, null);
	}
	
	public void addNumberField(String tittle, int decimalPrecision){
		NumberField tf= new NumberField(tittle);		
		if(decimalPrecision>=0){
			if(decimalPrecision==0){
				tf.setAllowDecimals(false);
			}
			tf.setDecimalPrecision(decimalPrecision);
		}
		fieldsArray.add(tf);		
		comboPanel.add(tf);		
		loadersArray.add(null);
	}
	
	public MyComboBox addCombo(String title, ComboLoader cl, boolean useDefaultFilter){
		return addCombo(title, cl, new StoreTraversalCallback() {
			public boolean execute(Record record) {
				boolean sw = false;
				Record[] s = getGridRecords();
				int i = 0;
				while (i < s.length && !sw) {
					sw = s[i].getAsString("id").equals(
							record.getAsString("id"));
					i++;
				}
				return !sw;
			}
		});
	}
	
	protected Field getField(int i) {
		return fieldsArray.get(i);
	}
	
	public void addTextField(String title){
		TextField tf= new TextField(title);
		addTextField(tf);
	}
	
	public void addTextField(TextField tf){		
		fieldsArray.add(tf);		
		comboPanel.add(tf);		
		loadersArray.add(null);
	}
	
	public void addTextField(String title, int index){
		TextField tf = new TextField(title);
		fieldsArray.add(tf);		
		comboPanel.insert(index, tf);		
		loadersArray.add(null);
	}
	
	public void addCheckBox(String tittle){
		Checkbox tf= new Checkbox(tittle);
		fieldsArray.add(tf);		
		comboPanel.add(tf);		
		loadersArray.add(null);
	}
	
	/*public void addStaticCombo(String tittle, ArrayList<RowModel> values){
		MyComboBox tf= new MyComboBox(tittle);
		fieldsArray.add(tf);
		new RPCFillComplexComboHandler(tf,comboPanel, new RowLayoutData()).onSuccess(values);
		//comboPanel.add(tf);
		loadersArray.add(null);
	}*/
	
	public MyComboBox addStaticCombo(String title,
			final ArrayList<RowModel> values) {
		final MyComboBox tf = new MyComboBox(title);
		fieldsArray.add(tf);
		loadersArray.add(new ComboLoader() {
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				new RPCFillComplexComboHandler(tf, comboPanel,
						new RowLayoutData()).onSuccess(values);
			}
		});
		return tf;
	}
	
	public MyComboBox addCombo(String title, ComboLoader cl, final StoreTraversalCallback filter){
		MyComboBox cbAddRecord = new MyComboBox(title);
		cbAddRecord.setReadOnly(true);
			
		cbAddRecord.addListener(new ComboBoxListenerAdapter() {
			public void onFocus(Field field) {
				if (gridRecords.isConfigured()) {
					Store store = ((ComboBox) field).getStore();
					store.clearFilter();
					store.commitChanges();
					if(filter != null)
						store.filterBy(filter);
				}				
			}

			public void onExpand(ComboBox comboBox) {
				onFocus(comboBox);
			}
		});
		
		fieldsArray.add(cbAddRecord);
		loadersArray.add(cl);
		return cbAddRecord;
	}

	private void initialize(String title,  String details,
			boolean modify, boolean numerate, boolean delete, String reorder) {
				
		if (title != null)
			this.setTitle(title);
		this.setBorder(false);
		this.setLayout(new RowLayout());

		fieldsArray = new ArrayList<Field>();
		loadersArray = new ArrayList<ComboLoader>();
		
		formPanel = new MultiFieldPanel();
		formPanel.setHeight(200);
		formPanel.setPaddings(5);
		formPanel.setBorder(false);

		gridRecords = new MyGridPanel();

		this.numerate = numerate;
		this.details = details;
		this.modify = modify;
		this.delete=delete;
		this.reorder=reorder;

		gridRecords.addDeleteListener(new MyGridOnCellClickListener() {
			public void onCellClick(Record r) {
				deleteFunction(r);
				reload();
			}
		});		
		gridRecords.addModListener(new MyGridOnCellClickListener() {			

			public void onCellClick(Record r) {
				currentRecord=r;
				modifyFunction(r);
			}
		});
		gridRecords.addDetListener(new MyGridOnCellClickListener() {
			public void onCellClick(Record r) {
				detailFunction(r);
			}
		});
		
		gridRecords.addReorderListener(new MyGridOnCellClickListener(){
			public void onCellClick(Record r) {
				reorderFunction(r);
			}
		});
		
		comboPanel = new Panel();
		comboPanel.setLayout(new FormLayout());
		comboPanel.setPaddings(0);
		comboPanel.setBorder(false);

		
		formPanel.addToRow(comboPanel, 280);

		button = new Button("Agregar");
		button.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				addFunction(getSelectedRecords());
			}
		});
		formPanel.addToRow(button, new ColumnLayoutData(1));

		this.add(formPanel, new RowLayoutData("30%"));
		padded = new Panel();
		padded.setBorder(false);
		padded.setPaddings(5, 5, 5, 5);
		padded.setLayout(new FitLayout());
		this.add(padded, new RowLayoutData("70%"));
		loadGridFunction(this);	
	}
	
	public Record[] getGridRecords(){
		return gridRecords.getStore().getRecords();
	}

	public void cleanFields() {
		for (int i = 0; i < fieldsArray.size(); i++) {
			if(fieldsArray.get(i) instanceof MyComboBox){				
				((MyComboBox)fieldsArray.get(i)).setValue("");				
			}
			else
			if(fieldsArray.get(i) instanceof TextField)
			{
				((TextField)fieldsArray.get(i)).setValue("");
			}
			else
			if(fieldsArray.get(i) instanceof Checkbox)
			{
				((Checkbox)fieldsArray.get(i)).setChecked(false);
			}
			fieldsArray.get(i).setDisabled(false);
		}
	}
	
	public Record[] getSelectedRecords(){
		Record[] r = new Record[fieldsArray.size()];
		for (int i = 0; i < fieldsArray.size(); i++)
		{
			if(fieldsArray.get(i) instanceof MyComboBox){
				r[i] = ((MyComboBox)fieldsArray.get(i)).getSelectedRecord();				
			}else
			{
				RecordDef recordef=new RecordDef(new FieldDef[]{new StringFieldDef("id")});
				r[i]=recordef.createRecord(new String[]{fieldsArray.get(i).getValueAsString()});
			}
		}			
		return r;
	}	
	
	public void loadAllCombos(){
		for (int i = 0; i < fieldsArray.size(); i++){
			if(fieldsArray.get(i) instanceof MyComboBox){
				if(loadersArray.get(i)!=null){
					loadersArray.get(i).load(
							new RPCFillComplexComboHandler(((MyComboBox)fieldsArray.get(i)),
									comboPanel, i));					
				}				
			}			
		}	
	}

	public int getRowCount() {		 
		return gridRecords.isRendered()?gridRecords.getStore().getRecords().length:0;
	}
}
