package org.extreme.controlweb.client.admin;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.admin.listeners.CategoriasListasListener;
import org.extreme.controlweb.client.admin.listeners.ModifyProcessListener;
import org.extreme.controlweb.client.admin.listeners.MunipDetailListener;
import org.extreme.controlweb.client.admin.listeners.TemplateClickListener;
import org.extreme.controlweb.client.admin.listeners.TiposTrabajosDetListenerPlanning;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MoreInfoWindowVehiculo;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.gui.WizardWindow;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Margins;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.FormPanel;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.TimeField;
import com.gwtext.client.widgets.form.event.FieldListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;

@SuppressWarnings("unused")
public class MyPanelFactory {

	/*
	 * private static boolean[] visTrabajo = new boolean[] { true, true, true,
	 * true, true, true, true, true, true, true, true, true, true, false, false,
	 * false, false }; private static int[] sizesTrabajo = new int[] { 100, 100,
	 * 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100
	 * };
	 */

	private static boolean[] visVehiculos = new boolean[] { true, true, true, true, true, true, true, true, true, true,
			false, false, false, false, false, false };
	private static int[] sizesVehiculos = new int[] { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
			100, 100, 100 };

	protected static QueriesServiceAsync serviceProxy = null;

	private static AdminQueriesServiceAsync adminServiceProxy = null;
	private static Record rTemplate;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}

	public static Panel newPanel(String title, ControlWebGUI gui) {

		Panel newPanel = new Panel(title);
		newPanel.setLayout(new BorderLayout());
		FormPanel formPanel = new FormPanel();
		formPanel.setLayout(new FormLayout());
		formPanel.setLabelAlign(Position.TOP);
		formPanel.setPaddings(15);
		formPanel.setBodyStyle("background-color:#f2f2f2");
		 //formPanel.setBodyStyle("background-color:#DEE3E7");
		//formPanel.setBodyStyle("background-color:#DFE8F6");
		// formPanel.setBodyStyle("background-color:#D6DFF7");
		formPanel.setBorder(false);
		formPanel.setWidth(200);
		formPanel.add(ClientUtils.getSeparator());
		Panel gridContainerPanel = new Panel();
		gridContainerPanel.setLayout(new RowLayout());
		gridContainerPanel.setBorder(false);

		gridContainerPanel.setPaddings(15);
		gridContainerPanel.add(ClientUtils.getSeparator());

		BorderLayoutData westLayoutData = new BorderLayoutData(RegionPosition.WEST);
		westLayoutData.setMargins(new Margins(5, 5, 0, 5));
		westLayoutData.setCMargins(new Margins(5, 5, 5, 5));
		westLayoutData.setSplit(false);

		// if (!title.equals("Procesos"))
		newPanel.add(formPanel, westLayoutData);

		newPanel.add(gridContainerPanel, ClientUtils.CENTER_LAYOUT_DATA);

		if (title.equals("Trabajos")) {
			toTrabajosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Recursos")) {
			toRecursosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Municipios")) {
			toMunicipiosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Jornadas")) {
			toJornadasPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Horarios")) {
			toHorariosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Tipos de Recursos")) {
			toTiposRecursosPanel(formPanel, gridContainerPanel, gui);

		} else if (title.equals("Tipos de Trabajos")) {
			toTiposTrabajosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Contratistas")) {
			toContratistasPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Clientes")) {
			toClientesPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Motivos de Rechazo")) {
			toMRechazosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Usuarios")) {
			toUsuariosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Procesos")) {
			toProcesosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Categorias Listas")) {
			toCategoriasListasPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Veh\u00edculos")) {
			toVehiculosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Plantilla de archivos")) {
			toTemplatesPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Programaci\u00f3n de Vehiculos")) {
			toProgramacionPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Contactos")) {
			toContactosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Destinos")) {
			toDestinosPanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Muelles")) {
			toMuellePanel(formPanel, gridContainerPanel, gui);
		} else if (title.equals("Conductores")) {
			toConductoresPanel(formPanel, gridContainerPanel, gui);
		}
		return newPanel;
	}

	private static void toHorariosPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos");
		fs.setLabelWidth(40);

		final TextField txtCod = new TextField("Codigo");
		fs.add(txtCod);
		final TextField txtNombre = new TextField("Nombre");
		fs.add(txtNombre);

		final MyGridPanel grid = new MyGridPanel();
		final Function recargarJornadas = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getHorariosAll(new RPCFillTable(grid, gridContainerPanel,
						"Asociar jornadas", true, true, false, null, null, new RowLayoutData(), null));
				txtCod.setDisabled(false);
				txtCod.setValue("");
				txtNombre.setValue("");

			}
		};

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				txtCod.setDisabled(true);
				txtCod.setValue(r.getAsString("Codigo"));
				txtNombre.setValue(r.getAsString("Nombre"));

			}
		});

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().deleteHorario(r.getAsString("Codigo"),
						new MyBooleanAsyncCallback("Horario borrado con exito", recargarJornadas));
			}
		});

		grid.addDetListener(new MyGridOnCellClickListener() {

			@Override
			public void onCellClick(final Record r) {
				// TODO Auto-generated method stub
				Window wnd = new Window("Jornadas de Horarios", true, false);
				wnd.setWidth(450);
				wnd.setHeight(200);
				wnd.setIconCls("icon-butterfly");
				wnd.setLayout(new FitLayout());

				@SuppressWarnings("unchecked")
				GenericAdminPanel adminJornadasHorarios = new GenericAdminPanelAdapter(null, null, false, false, true) {

					@Override
					protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
						getStaticQueriesServiceAsync().getJornadasAsRowModel(r.getAsString("Codigo"), rows);
					}

					@Override
					protected void deleteFunction(Record r2) {
						getStaticAdminQueriesServiceAsync().delJornadasHorarios(r.getAsString("Codigo"),
								r2.getAsString("id"), new MyBooleanAsyncCallback("Jornada borrada", reloadFunction));
					}

					@Override
					protected void addFunction(Record[] recs) {
						getStaticAdminQueriesServiceAsync().addJornadasHorarios(recs[0].getAsString("id"),
								r.getAsString("Codigo"),
								new MyBooleanAsyncCallback("Jornada agregada", reloadFunction));
					}
				};

				adminJornadasHorarios.addCombo("Jornadas", new ComboLoader() {
					@Override
					public void load(AsyncCallback<ArrayList<RowModel>> cb) {
						getStaticQueriesServiceAsync().getJornadasAsRowModel(cb);
					}
				}, true);
				adminJornadasHorarios.loadAllCombos();
				wnd.add(adminJornadasHorarios);
				wnd.show();
			}
		});

		Button guardar = new Button("Guardar");
		fs.setButtonAlign(Position.CENTER);
		guardar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (txtCod.getValueAsString() != null && !txtCod.getValueAsString().equalsIgnoreCase("")) {

					if (txtNombre.getValueAsString() != null && !txtNombre.getValueAsString().equalsIgnoreCase("")) {

						if (!txtCod.isDisabled()) {
							getStaticAdminQueriesServiceAsync().addHorario(txtCod.getValueAsString(),
									txtNombre.getValueAsString(),

									new MyBooleanAsyncCallback("Horario creado", recargarJornadas));
						} else {
							getStaticAdminQueriesServiceAsync().updateHorario(txtCod.getValueAsString(),
									txtNombre.getValueAsString(),
									new MyBooleanAsyncCallback("Horario actualizado", recargarJornadas));
							txtCod.setDisabled(false);
						}

					} else {
						ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
					}

				} else {
					ClientUtils.alert("Info.", "El codigo es requerido.", ClientUtils.ERROR);
				}

			}
		});

		Button nuevo = new Button("Nuevo");

		nuevo.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				txtCod.setDisabled(false);
				txtCod.setValue("");
				txtNombre.setValue("");
			}
		});

		fs.addButton(guardar);
		fs.addButton(nuevo);
		formPanel.add(fs);
		recargarJornadas.execute();
	}

	public static String idjornada = "";

	private static void toJornadasPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos");
		fs.setLabelWidth(40);
		final TextField txtCod = new TextField("Codigo");
		fs.add(txtCod);
		final TextField txtNombre = new TextField("Nombre");
		fs.add(txtNombre);
		final TimeField tmHoraInicio = new TimeField("Hora inicio");
		tmHoraInicio.setFormat("H:i");
		tmHoraInicio.setIncrement(15);
		tmHoraInicio.setWidth(95);
		fs.add(tmHoraInicio);
		final TimeField tmHoraFin = new TimeField("Hora fin");
		tmHoraFin.setFormat("H:i");
		tmHoraFin.setIncrement(15);
		tmHoraFin.setWidth(95);
		fs.add(tmHoraFin);

		final MyGridPanel grid = new MyGridPanel();
		final Function recargarJornadas = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getJornadasAll(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, false, null, null, new RowLayoutData(), null));
				txtCod.setDisabled(false);
				txtCod.setValue("");
				txtNombre.setValue("");
				tmHoraInicio.setValue("");
				tmHoraFin.setValue("");
				idjornada = "";

			}
		};

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				idjornada = r.getAsString("idjornada");
				txtCod.setDisabled(true);
				txtCod.setValue(r.getAsString("Codigo"));
				txtNombre.setValue(r.getAsString("Nombre"));
				tmHoraInicio.setValue(r.getAsString("Hora inicio").substring(0, 5));
				tmHoraFin.setValue(r.getAsString("Hora fin").substring(0, 5));

			}
		});

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				idjornada = r.getAsString("idjornada");
				getStaticAdminQueriesServiceAsync().deleteJornada(idjornada, new MyBooleanAsyncCallback(
						"No se puede eliminar la jornada", "Jornada borrada con exito", recargarJornadas));
			}
		});

		Button guardar = new Button("Guardar");
		fs.setButtonAlign(Position.CENTER);
		guardar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (txtCod.getValueAsString() != null && !txtCod.getValueAsString().equalsIgnoreCase("")) {

					if (txtNombre.getValueAsString() != null && !txtNombre.getValueAsString().equalsIgnoreCase("")) {

						if (tmHoraInicio.getValueAsString() != null
								&& !tmHoraInicio.getValueAsString().equalsIgnoreCase("")) {

							if (tmHoraFin.getValueAsString() != null
									&& !tmHoraFin.getValueAsString().equalsIgnoreCase("")) {

								if (!txtCod.isDisabled()) {
									getStaticAdminQueriesServiceAsync().addJornada(txtCod.getValueAsString(),
											txtNombre.getValueAsString(), tmHoraInicio.getValueAsString(),
											tmHoraFin.getValueAsString(),
											new MyBooleanAsyncCallback("Jornada creada", recargarJornadas));
								} else {
									getStaticAdminQueriesServiceAsync().updateJornada(idjornada,
											txtCod.getValueAsString(), txtNombre.getValueAsString(),
											tmHoraInicio.getValueAsString(), tmHoraFin.getValueAsString(),
											new MyBooleanAsyncCallback("Jornada actualizada", recargarJornadas));
									txtCod.setDisabled(false);
								}

							} else {
								ClientUtils.alert("Info.", "La hora de finalizacion es requerida.", ClientUtils.ERROR);
							}

						} else {
							ClientUtils.alert("Info.", "La hora de inicio es requerida.", ClientUtils.ERROR);
						}

					} else {
						ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
					}

				} else {
					ClientUtils.alert("Info.", "El codigo es requerido.", ClientUtils.ERROR);
				}

			}
		});

		Button nuevo = new Button("Nuevo");

		nuevo.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				txtCod.setDisabled(false);
				txtCod.setValue("");
				txtNombre.setValue("");
				tmHoraInicio.setValue("");
				tmHoraFin.setValue("");
			}
		});

		fs.addButton(guardar);
		fs.addButton(nuevo);
		formPanel.add(fs);
		recargarJornadas.execute();
	}

	private static void toDestinosPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Destinos");
		fs.setLabelWidth(40);

		final TextField tfIdDest = new TextField("Id Destino");
		fs.add(tfIdDest);

		final TextField tfnombre = new TextField("Nombre");
		fs.add(tfnombre);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarDestinos = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllDestinos(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, true, null, null, new RowLayoutData(), null));
				tfIdDest.setValue("");
				tfnombre.setValue("");

			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				String fecha = r.getAsString("nombre");
				getStaticAdminQueriesServiceAsync().deleteDestino(r.getAsString("Id Destino"),
						new MyBooleanAsyncCallback("Registro borrado exitosamente", cargarDestinos));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdDest.setDisabled(true);
				tfIdDest.setValue(r.getAsString("Id Destino"));
				tfnombre.setValue(r.getAsString("Nombre"));

			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdDest.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addDestino(tfIdDest.getValueAsString(),
							tfnombre.getValueAsString(),
							new MyBooleanAsyncCallback("Error en la creaci\u00f3n del registro",
									"Destino Creado Exitosamente", cargarDestinos));
				} else {
					getStaticAdminQueriesServiceAsync().updateDestino(tfIdDest.getValueAsString(),
							tfnombre.getValueAsString(),
							new MyBooleanAsyncCallback("Error en la actualizaci\u00f3n del registro",
									"Destino Actualizado Exitosamente", cargarDestinos));

					tfIdDest.setDisabled(false);
				}
			}
		});

		fs.addButton(btnSave);
		formPanel.add(fs);
		cargarDestinos.execute();

	}

	private static void toMuellePanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Muelles");
		fs.setLabelWidth(40);

		final TextField tfMuelle = new TextField("Muelle");
		fs.add(tfMuelle);

		final TextField tfDesc = new TextField("Nombre");
		fs.add(tfDesc);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarMuelles = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllMuelles(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, true, null, null, new RowLayoutData(), null));
				tfMuelle.setValue("");
				tfDesc.setValue("");

			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				String muelle = r.getAsString("nombre");
				System.out.println("muelle: " + muelle);
				getStaticAdminQueriesServiceAsync().deleteMuelle(muelle,
						new MyBooleanAsyncCallback("Registro borrado exitosamente", cargarMuelles));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfMuelle.setDisabled(true);
				tfMuelle.setValue(r.getAsString("nombre"));
				tfDesc.setValue(r.getAsString("descripcion"));

			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfMuelle.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addMuelle(tfMuelle.getValueAsString(),
							tfDesc.getValueAsString(),
							new MyBooleanAsyncCallback("Error en la creaci\u00f3n del registro",
									"Muelle Creado Exitosamente", cargarMuelles));
				} else {
					getStaticAdminQueriesServiceAsync().updateMuelle(tfMuelle.getValueAsString(),
							tfDesc.getValueAsString(),
							new MyBooleanAsyncCallback("Error en la creaci\u00f3n del registro",
									"Muelle Actualizado Exitosamente", cargarMuelles));

					tfMuelle.setDisabled(false);
				}
			}
		});

		fs.addButton(btnSave);
		formPanel.add(fs);
		cargarMuelles.execute();

	}

	private static void toProgramacionPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos de la Programaci\u00f3n");
		fs.setLabelWidth(40);

		final DateField fecha = new DateField("Fecha", "Y-m-d");
		fecha.setValue(new Date());
		fecha.setWidth(180);
		fs.add(fecha, new AnchorLayoutData("100%"));

		final MyComboBox placaVeh = new MyComboBox("Placa Vehiculo");
		placaVeh.setWidth(180);
		getStaticQueriesServiceAsync().getVehiculos(new RPCFillComplexComboHandler(placaVeh, fs, 1));

		final MyComboBox muelleCarga = new MyComboBox("Muelle Carga");
		muelleCarga.setWidth(180);
		getStaticQueriesServiceAsync().getMuelleCarga(new RPCFillComplexComboHandler(muelleCarga, fs, 2));

		final MyComboBox conductor = new MyComboBox("Conductor");
		conductor.setWidth(180);
		getStaticQueriesServiceAsync().getConductores(new RPCFillComplexComboHandler(conductor, fs, 3));

		final MyComboBox destino = new MyComboBox("Destino");
		destino.setWidth(180);
		getStaticQueriesServiceAsync().getDestinos(new RPCFillComplexComboHandler(destino, fs, 4));

		final TimeField horaEntrada = new TimeField("Hora Entrada Prog.");
		horaEntrada.setFormat("H:i");
		horaEntrada.setIncrement(15);
		horaEntrada.setWidth(180);
		fs.add(horaEntrada, new AnchorLayoutData("100%"));

		final TimeField horaSalida = new TimeField("Hora Salida Prog.");
		horaSalida.setFormat("H:i");
		horaSalida.setIncrement(15);
		horaSalida.setWidth(180);
		fs.add(horaSalida, new AnchorLayoutData("100%"));

		final TimeField horaRetorno = new TimeField("Hora Retorno Prog.");
		horaRetorno.setFormat("H:i");
		horaRetorno.setIncrement(15);
		horaRetorno.setWidth(180);
		fs.add(horaRetorno, new AnchorLayoutData("100%"));

		final TextArea observacion = new TextArea("Observaci\u00f3n");
		observacion.setWidth(180);
		fs.add(observacion);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarAllFlotaFiltro = new Function() {
			@Override
			public void execute() {
				String fecha2 = "";
				String horaentrada = "";
				String horasalida = "";
				String horaretorno = "";
				if (!fecha.getValueAsString().equals("") && fecha.getValueAsString() != null) {
					fecha2 = DateTimeFormat.getFormat("yyyy-MM-dd").format(fecha.getValue());
					if (!horaEntrada.getValueAsString().equals("") && horaEntrada.getValueAsString() != null) {
						horaentrada = fecha2 + " " + horaEntrada.getValueAsString();
					}
					if (!horaSalida.getValueAsString().equals("") && horaSalida.getValueAsString() != null) {
						horasalida = fecha2 + " " + horaSalida.getValueAsString();
					}
					if (!horaRetorno.getValueAsString().equals("") && horaRetorno.getValueAsString() != null) {
						horaretorno = fecha2 + " " + horaRetorno.getValueAsString();
					}
				}
				getStaticQueriesServiceAsync().getAllProgramaciondeFlotaFiltro(fecha2, placaVeh.getValueAsString(),
						muelleCarga.getValueAsString(), conductor.getValueAsString(), destino.getValueAsString(),
						horaentrada, horasalida, horaretorno, new RPCFillTable(grid, gridContainerPanel, null, true,
								true, true, null, null, new RowLayoutData(), null));
				fecha.setDisabled(false);
				placaVeh.setDisabled(false);
				fecha.setValue("");
				placaVeh.setValue("");
				muelleCarga.setValue("");
				conductor.setValue("");
				destino.setValue("");
				horaEntrada.setValue("");
				horaSalida.setValue("");
				horaRetorno.setValue("");
				observacion.setValue("");
			}
		};

		final Function cargarAllFlota = new Function() {
			@Override
			public void execute() {

				getStaticQueriesServiceAsync().getAllProgramaciondeFlota(new RPCFillTable(grid, gridContainerPanel,
						null, true, true, true, null, null, new RowLayoutData(), null));
				fecha.setDisabled(false);
				placaVeh.setDisabled(false);
				fecha.setValue("");
				placaVeh.setValue("");
				muelleCarga.setValue("");
				conductor.setValue("");
				destino.setValue("");
				horaEntrada.setValue("");
				horaSalida.setValue("");
				horaRetorno.setValue("");
				observacion.setValue("");
			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				System.out.println("Realizando eliminacion de registro..");
				String fecha = r.getAsString("fecha");
				String placa = r.getAsString("codplaca");
				System.out.println("fecha: " + fecha + " placa: " + placa);
				getStaticAdminQueriesServiceAsync().deleteRegProg(r.getAsString("fecha"), r.getAsString("codplaca"),
						new MyBooleanAsyncCallback(
								"Registro de programaci\u00f3n de flota de vehiculo borrado exitosamente",
								cargarAllFlota));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				fecha.setDisabled(true);
				placaVeh.setDisabled(true);
				fecha.setValue(r.getAsString("fecha"));
				placaVeh.setValue(r.getAsString("codplaca"));
				muelleCarga.setValue(r.getAsString("codmuelle"));
				conductor.setValue(r.getAsString("codconductor"));
				destino.setValue(r.getAsString("coddestino"));
				int a = r.getAsString("horaentrada").length() - 3;
				int b = r.getAsString("horasalida").length() - 3;
				int c = r.getAsString("horaretorno").length() - 3;
				horaEntrada.setValue(r.getAsString("horaentrada").substring(0, a));
				horaSalida.setValue(r.getAsString("horasalida").substring(0, b));
				horaRetorno.setValue(r.getAsString("horaretorno").substring(0, c));
				observacion.setValue(r.getAsString("observacion"));

			}
		});

		Button btnSave = new Button("Guardar");
		Button btnFiltrar = new Button("Filtrar");
		// btnSave.set
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				Date fechaprog = fecha.getValue();
				String fecha2 = "";
				Date fechaA = new Date();
				Date fechaB = new Date();
				Date fechaC = new Date();
				String horaentrada = "";
				String horasalida = "";
				String horaretorno = "";
				if (!fecha.getValueAsString().equals("") && fecha.getValueAsString() != null) {
					fecha2 = DateTimeFormat.getFormat("yyyy-MM-dd").format(fecha.getValue());
					if (!horaEntrada.getValueAsString().equals("") && horaEntrada.getValueAsString() != null) {
						horaentrada = fecha2 + " " + horaEntrada.getValueAsString();
						fechaA = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm").parse(horaentrada);
					}
					if (!horaSalida.getValueAsString().equals("") && horaSalida.getValueAsString() != null) {
						horasalida = fecha2 + " " + horaSalida.getValueAsString();
						fechaB = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm").parse(horasalida);
					}
					if (!horaRetorno.getValueAsString().equals("") && horaRetorno.getValueAsString() != null) {
						horaretorno = fecha2 + " " + horaRetorno.getValueAsString();
						fechaC = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm").parse(horaretorno);
					}

				}

				if (fechaB.getTime() > fechaA.getTime() && fechaC.getTime() > fechaB.getTime()) {
					if (!fecha.isDisabled() && !placaVeh.isDisabled()) {
						if (fechaprog.getTime() >= new Date().getTime()) {
							getStaticAdminQueriesServiceAsync().addRegFlota(fecha2, placaVeh.getValueAsString(),
									muelleCarga.getValueAsString(), conductor.getValueAsString(),
									destino.getValueAsString(), horaentrada, horasalida, horaretorno,
									observacion.getValueAsString(),
									new MyBooleanAsyncCallback(
											"Error en el ingreso del registro. Verificar datos del regristo.",
											"Registro agregado exitosamente", cargarAllFlota));
						} else {
							ClientUtils.alert("Info", "La fecha de programaci\u00f3n debe ser mayor a la fecha actual.",
									ClientUtils.INFO);
						}
					} else {

						getStaticAdminQueriesServiceAsync().updateRegFlota(fecha2, placaVeh.getValueAsString(),
								muelleCarga.getValueAsString(), conductor.getValueAsString(),
								destino.getValueAsString(), horaentrada, horasalida, horaretorno,
								observacion.getValueAsString(),
								new MyBooleanAsyncCallback(
										"Error en el ingreso del registro. Verificar datos del regristo.",
										"Registro actualizado exitosamente", cargarAllFlota));
					}
				} else {
					ClientUtils.alert("Info",
							"Error en la elecci\u00f3n de las horas para esta programaci\u00f3n, los campos Hora entrada, Hora Salida y Hora Retorno deben cumplir la siguiente condici\u00f3n: Hora Entrada < Hora Salida < Hora Retorno ",
							ClientUtils.INFO);
				}
			}
		});

		btnFiltrar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				System.out.println("Comenzando ejecucion");
				cargarAllFlotaFiltro.execute();
			}

		});

		fs.addButton(btnSave);
		fs.addButton(btnFiltrar);
		formPanel.add(fs);
		cargarAllFlota.execute();
	}

	private static void toTemplatesPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos");
		final MyComboBox cbFiles = new MyComboBox("Destino");
		getStaticQueriesServiceAsync().getArchivos(new RPCFillComplexComboHandler(cbFiles, fs, 0));
		final TextField tfNombre = new TextField("Nombre");
		fs.add(tfNombre);
		final MyComboBox cbFormatos = new MyComboBox("Formato");
		getStaticQueriesServiceAsync().getFormatos(new RPCFillComplexComboHandler(cbFormatos, fs, 2));
		final Button enviar = new Button("Enviar");
		rTemplate = null;
		final MyGridPanel grid = new MyGridPanel();
		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				rTemplate = r;
				tfNombre.setValue(rTemplate.getAsString("nombre"));
				cbFiles.setValue(rTemplate.getAsString("archivo"));
				cbFormatos.setValue(rTemplate.getAsString("formato"));
			}
		});
		grid.addDetListener(new TemplateClickListener());
		final Function recargarTemplates = new Function() {
			@Override
			public void execute() {
				tfNombre.setValue("");
				cbFiles.setValue("");
				cbFormatos.setValue("");
				getStaticQueriesServiceAsync().getTemplates(gui.getProceso().getId(),
						new RPCFillTable(grid, gridContainerPanel, "Modificar template", true, true, false,
								new int[] { 50, 1, 100, 1, 100, 1, 1, 100 },
								new boolean[] { true, false, true, false, true, false, false, true, false },
								new RowLayoutData()));
				rTemplate = null;
				enviar.enable();
			}
		};
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delTemplate(r.getAsString("id"),
						new MyBooleanAsyncCallback("Borrado exitosamente", recargarTemplates));
			}
		});
		enviar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (rTemplate == null) {
					getStaticAdminQueriesServiceAsync().addTemplate(cbFiles.getValue(), tfNombre.getValueAsString(),
							cbFormatos.getValue(), gui.getProceso().getId(),
							new MyBooleanAsyncCallback("Plantilla actualizada", recargarTemplates));
				} else {
					getStaticAdminQueriesServiceAsync().updateTemplate(rTemplate.getAsString("id"),
							tfNombre.getValueAsString(), cbFormatos.getValue(), gui.getProceso().getId(),
							new MyBooleanAsyncCallback("Plantilla modificada", recargarTemplates));
				}
				button.disable();
			}
		});
		fs.setButtonAlign(Position.CENTER);
		fs.addButton(enviar);
		formPanel.add(fs);
		recargarTemplates.execute();
	}

	private static void toVehiculosPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {

		final FieldSet fs = new FieldSet("Datos de Veh\u00edculo");
		fs.setLabelWidth(40);

		final TextField tfIdVeh = new TextField("Id Veh\u00edculo");
		fs.add(tfIdVeh, new AnchorLayoutData("100%"));

		final TextField tfPlaca = new TextField("Placa");
		fs.add(tfPlaca, new AnchorLayoutData("100%"));

		final ComboBox cbRec = new ComboBox("Recurso");

		getStaticQueriesServiceAsync().getRecursosSinVehiculo(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbRec, fs, 12));

		final ComboBox cbCont = new ComboBox("Contratista");

		getStaticQueriesServiceAsync().getContratistas(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbCont, fs, 11));

		final MyGridPanel grid = new MyGridPanel();
		grid.addDetListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				new MoreInfoWindowVehiculo(r, gui);
			}
		});

		final Function recargarVehiculos = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getVehiculos(gui.getProceso().getId(),
						new RPCFillTable(grid, gridContainerPanel, "Modificar datos adicionales", true, false, false,
								null, null, new RowLayoutData()));
				tfIdVeh.setDisabled(false);
			}
		};

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (cbRec.getValueAsString() != null) {
					getStaticAdminQueriesServiceAsync().addVehiculo(tfIdVeh.getValueAsString(),
							tfPlaca.getValueAsString(), cbRec.getValueAsString(), cbCont.getValueAsString(),
							new MyBooleanAsyncCallback("Creado Exitosamente", recargarVehiculos));
				} else {
					ClientUtils.alert("Error", "Debe seleccionar un recurso", ClientUtils.ERROR);
				}
			}
		});
		fs.setButtonAlign(Position.CENTER);
		fs.addButton(btnSave);
		formPanel.add(fs);
		recargarVehiculos.execute();
	}

	private static void toCategoriasListasPanel(FormPanel formPanel, final Panel gridContainerPanel,
			ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Categorias de Listas");
		fs.setLabelWidth(40);

		final TextField tfIdCategoria = new TextField("Id");
		fs.add(tfIdCategoria, new AnchorLayoutData("100%"));

		final TextField tfNomCategoria = new TextField("Nombre");
		fs.add(tfNomCategoria, new AnchorLayoutData("100%"));

		final ComboBox cbMlista = ClientUtils.createCombo("Tipo", new SimpleStore(new String[] { "id", "nombre" },
				new String[][] { { "S", "Multilista" }, { "N", "Lista" } }));
		cbMlista.setEditable(false);
		fs.add(cbMlista, new AnchorLayoutData("70%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarCategorias = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllCategorias(new RPCFillTable(grid, gridContainerPanel,
						"Ver Detalles", true, true, true, new int[] { 100, 100, 100, 100 },
						new boolean[] { true, true, true, false }, new RowLayoutData(), new Function() {
							@Override
							public void execute() {
								tfIdCategoria.setVisible(false);
								tfIdCategoria.setValue("");
								tfNomCategoria.setValue("");
							}
						}));
			}
		};
		grid.addDetListener(new CategoriasListasListener());
		fs.setButtonAlign(Position.CENTER);

		grid.setHeight(480);
		grid.setWidth(500);
		grid.setEnableHdMenu(false);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delCategoriaPrecierre(r.getAsString("id"),
						new MyBooleanAsyncCallback("Categoria borrada", cargarCategorias));
			}
		});
		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdCategoria.setValue(r.getAsString("id"));
				tfIdCategoria.setFieldLabel("Id");
				tfIdCategoria.setVisible(true);
				tfNomCategoria.setValue(r.getAsString("nombre"));
				cbMlista.setValue(r.getAsString("Tipo").equals("Multilista") ? "S" : "N");
			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (tfNomCategoria.getValueAsString() != null
						&& !tfNomCategoria.getValueAsString().equalsIgnoreCase("")) {
					if (cbMlista.getValueAsString() != null && !cbMlista.getValueAsString().equalsIgnoreCase("")) {
						if (tfIdCategoria.isVisible()) {
							tfIdCategoria.setVisible(false);
							getStaticAdminQueriesServiceAsync().updateCategoriaPrecierre(
									tfIdCategoria.getValueAsString(), tfNomCategoria.getValueAsString(),
									cbMlista.getValue(),
									new MyBooleanAsyncCallback("Categoria actualizada", cargarCategorias));
						} else {
							tfIdCategoria.setVisible(false);
							getStaticAdminQueriesServiceAsync().addCategoriaLista(tfNomCategoria.getValueAsString(),
									cbMlista.getValue(),
									new MyBooleanAsyncCallback("Categoria creada con exito", cargarCategorias));
						}
						tfNomCategoria.setValue("");
					} else {
						ClientUtils.alert("Info.", "El tipo de lista es requerido.", ClientUtils.ERROR);
					}
				} else {
					ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
				}

			}
		});

		fs.addButton(btnSave);
		cargarCategorias.execute();

		/*
		 * FieldSet fs = new FieldSet("Categorias de Listas");
		 * fs.setLabelWidth(40);
		 * 
		 * final TextField tfIdCategoria = new TextField("Id");
		 * fs.add(tfIdCategoria, new AnchorLayoutData("100%"));
		 * 
		 * final TextField tfNomCategoria = new TextField("Nombre");
		 * fs.add(tfNomCategoria, new AnchorLayoutData("100%"));
		 * 
		 * formPanel.add(fs);
		 * 
		 * final MyGridPanel grid = new MyGridPanel();
		 * 
		 * final Function cargarCategorias = new Function() {
		 * 
		 * @Override public void execute() {
		 * getStaticQueriesServiceAsync().getAllCategorias( new
		 * RPCFillTable(grid, gridContainerPanel, "Ver Detalles", true, true,
		 * true, new int[] { 100, 100 }, new boolean[] { true, true }, new
		 * RowLayoutData(), null)); tfIdCategoria.setValue("");
		 * tfIdCategoria.setDisabled(false); tfNomCategoria.setValue(""); } };
		 * grid.addDetListener(new CategoriasListasListener());
		 * fs.setButtonAlign(Position.CENTER);
		 * 
		 * grid.setHeight(480); grid.setWidth(500); grid.setEnableHdMenu(false);
		 * 
		 * grid.addDeleteListener(new MyGridOnCellClickListener() {
		 * 
		 * @Override public void onCellClick(Record r) {
		 * getStaticAdminQueriesServiceAsync().delCategoriaPrecierre(
		 * r.getAsString("id"), new MyBooleanAsyncCallback("Categoria borrada",
		 * cargarCategorias)); } }); grid.addModListener(new
		 * MyGridOnCellClickListener() {
		 * 
		 * @Override public void onCellClick(Record r) {
		 * tfIdCategoria.setValue(r.getAsString("id"));
		 * tfIdCategoria.setDisabled(true);
		 * tfNomCategoria.setValue(r.getAsString("nombre")); } });
		 * 
		 * Button btnSave = new Button("Guardar"); btnSave.addListener(new
		 * ButtonListenerAdapter() {
		 * 
		 * @Override public void onClick(Button button, EventObject e) { if
		 * (tfIdCategoria.isDisabled()) { getStaticAdminQueriesServiceAsync()
		 * .updateCategoriaPrecierre( tfIdCategoria.getValueAsString(),
		 * tfNomCategoria.getValueAsString(), new MyBooleanAsyncCallback(
		 * "Categoria actualizada", cargarCategorias)); } else {
		 * getStaticAdminQueriesServiceAsync().addCategoriaLista(
		 * tfNomCategoria.getValueAsString(), new MyBooleanAsyncCallback(
		 * "Categoria creada con exito", cargarCategorias)); } } });
		 * fs.addButton(btnSave); cargarCategorias.execute();
		 */
	}

	private static void toProcesosPanel(Panel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos Proceso");
		fs.setLabelWidth(40);

		final TextField tfIdProceso = new TextField("Id del proceso");
		fs.add(tfIdProceso, new AnchorLayoutData("100%"));

		final TextField tfNomProceso = new TextField("Nombre del proceoso");
		fs.add(tfNomProceso, new AnchorLayoutData("100%"));

		formPanel.add(fs);
		fs.setButtonAlign(Position.CENTER);

		final MyGridPanel grid = new MyGridPanel();

		grid.setHeight(480);
		grid.setWidth(500);

		grid.setEnableHdMenu(false);

		final Function cargarProcesos = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getProcesosAsRowModel(new RPCFillTable(grid, gridContainerPanel,
						"Ver detalles", false, false, true, new int[] { 100, 100, 100, 100, 100, 100, 100 },
						new boolean[] { true, true, true, true, true, false, false }, new RowLayoutData(), null));
				tfIdProceso.setValue("");
				tfNomProceso.setValue("");
			}
		};
		fs.setButtonAlign(Position.CENTER);
		Button btnSave = new Button("Crear");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync().addProceso(tfIdProceso.getValueAsString(),
						tfNomProceso.getValueAsString(),
						new MyBooleanAsyncCallback("Proceso creado exitosamente", cargarProcesos));
			}
		});
		fs.addButton(btnSave);
		grid.addDetListener(new ModifyProcessListener(cargarProcesos));
		cargarProcesos.execute();
	}

	private static void toUsuariosPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos Usuario");
		fs.setLabelWidth(40);

		final TextField tfIdUser = new TextField("Id de Usuario", "iduser");
		fs.add(tfIdUser, new AnchorLayoutData("100%"));

		final TextField tfNomUser = new TextField("Descripci\u00f3n", "nombreuser");
		fs.add(tfNomUser, new AnchorLayoutData("100%"));

		final TextField tfPasswd = new TextField("Contrase\u00f1a", "passwduser");
		tfPasswd.setPassword(true);
		fs.add(tfPasswd, new AnchorLayoutData("100%"));

		final TextField tfCargo = new TextField("Cargo", "cargouser");
		fs.add(tfCargo, new AnchorLayoutData("100%"));

		final ComboBox cbProfiles = new ComboBox("Perfil");

		getStaticQueriesServiceAsync().getPerfiles(new RPCFillComboBoxHandler(cbProfiles, fs, 4));

		formPanel.add(fs);
		fs.setButtonAlign(Position.CENTER);

		final MyGridPanel grid = new MyGridPanel();

		grid.addDetListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(final Record r) {
				Window wnd = new Window("Permisos de Usuario", true, false);
				wnd.setWidth(400);
				wnd.setHeight(200);
				wnd.setIconCls("icon-butterfly");
				wnd.setLayout(new FitLayout());

				GenericAdminPanel adminProcsPanel = new GenericAdminPanelAdapter(null, "Proceso", false, false, true) {

					@Override
					protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
						getStaticQueriesServiceAsync().getProcesosAsRowModel(r.getAsString("Id de Usuario"), rows);
					}

					@Override
					protected void deleteFunction(Record r2) {
						getStaticAdminQueriesServiceAsync().delPermiso(r.getAsString("Id de Usuario"),
								r2.getAsString("id"), new MyBooleanAsyncCallback("Permiso borrado", reloadFunction));
					}

					@Override
					protected void addFunction(Record[] recs) {
						getStaticAdminQueriesServiceAsync().addPermiso(recs[0].getAsString("id"),
								r.getAsString("Id de Usuario"),
								new MyBooleanAsyncCallback("Permiso agregado", reloadFunction));
					}
				};

				adminProcsPanel.addCombo("Procesos", new ComboLoader() {
					@Override
					public void load(AsyncCallback<ArrayList<RowModel>> cb) {
						getStaticQueriesServiceAsync().getProcesosAsRowModel(cb);
					}
				}, true);
				adminProcsPanel.loadAllCombos();
				wnd.add(adminProcsPanel);
				wnd.show();
			}
		});

		final boolean[] visibility = new boolean[] { true, true, true, true, true };
		final int[] sizes = new int[] { 100, 100, 100, 100, 100 };
		final Function recargarUsuarios = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getUsersInfo(gui.getUsuario().getId(),
						new RPCFillTable(grid, gridContainerPanel, "Permisos de usuario", true, true, true, sizes,
								visibility, new RowLayoutData()));
				tfIdUser.setValue("");
				tfIdUser.setDisabled(false);
				tfNomUser.setValue("");
				tfPasswd.setValue("");
				tfCargo.setValue("");
				cbProfiles.setValue("");
			}

		};

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				/*
				 * if (com.google.gwt.user.client.Window
				 * .confirm("Seguro que desea borrar al usuario " +
				 * r.getAsString("id"))) {
				 */
				getStaticAdminQueriesServiceAsync().delUser(r.getAsString("Id de Usuario"),
						new MyBooleanAsyncCallback("Usuario borrado", recargarUsuarios));
				// }
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {

			@Override
			public void onCellClick(Record r) {
				tfIdUser.setValue(r.getAsString("Id de Usuario"));
				tfIdUser.setDisabled(true);
				tfNomUser.setValue(r.getAsString("Descripci\u00f3n"));
				tfPasswd.setValue(r.getAsString("Contrase\u00f1a"));
				tfCargo.setValue(r.getAsString("Cargo"));
				cbProfiles.setValue(r.getAsString("Perfil"));
			}

		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdUser.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addUser(tfIdUser.getValueAsString(),
							tfNomUser.getValueAsString(), tfPasswd.getValueAsString(), tfCargo.getValueAsString(),
							cbProfiles.getValueAsString(),
							new MyBooleanAsyncCallback("Nuevo usuario ingresado", recargarUsuarios));
				} else {
					getStaticAdminQueriesServiceAsync().updateUser(tfIdUser.getValueAsString(),
							tfNomUser.getValueAsString(), tfPasswd.getValueAsString(), tfCargo.getValueAsString(),
							cbProfiles.getValueAsString(),
							new MyBooleanAsyncCallback("Usuario actualizado", recargarUsuarios));
				}
			}
		});
		fs.addButton(btnSave);

		recargarUsuarios.execute();
	}

	private static void toMRechazosPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos de Motivo de Rechazo");
		fs.setLabelWidth(40);

		final TextField tfIdRechazo = new TextField("C\u00f3digo", "idcont");
		fs.add(tfIdRechazo, new AnchorLayoutData("100%"));

		final TextField tfNomRechazo = new TextField("Nombre", "nombrecont");
		fs.add(tfNomRechazo, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarMrechazo = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllMRechazo(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, true, new int[] { 100, 100 }, new boolean[] { true, true }, new RowLayoutData(), null));
				tfIdRechazo.setDisabled(false);
				tfIdRechazo.setValue("");
				tfNomRechazo.setValue("");
			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().deleteMRechazo(r.getAsString("id"),
						new MyBooleanAsyncCallback("Motivo de rechazo borrado exitosamente", cargarMrechazo));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdRechazo.setDisabled(true);
				tfIdRechazo.setValue(r.getAsString("id"));
				tfNomRechazo.setValue(r.getAsString("nombre"));
			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdRechazo.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addMRechazo(tfIdRechazo.getValueAsString(),
							tfNomRechazo.getValueAsString(),
							new MyBooleanAsyncCallback("Motivo de rechazo agregado exitosamente", cargarMrechazo));
				} else {
					getStaticAdminQueriesServiceAsync().updateMRechazo(tfIdRechazo.getValueAsString(),
							tfNomRechazo.getValueAsString(),
							new MyBooleanAsyncCallback("Motivo de rechazo agregado exitosamente", cargarMrechazo));
				}
			}
		});
		fs.addButton(btnSave);
		cargarMrechazo.execute();
	}

	private static void toClientesPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos de Contratista");
		fs.setLabelWidth(40);

		final TextField tfIdCliente = new TextField("C\u00f3digo", "idcli");
		fs.add(tfIdCliente, new AnchorLayoutData("100%"));

		final TextField tfNomCliente = new TextField("Nombre", "nombrecli");
		fs.add(tfNomCliente, new AnchorLayoutData("100%"));

		final TextField tfDirCliente = new TextField("Direcci\u00f3n", "dircli");
		fs.add(tfDirCliente, new AnchorLayoutData("100%"));

		final ComboBox cbMunicipios = new ComboBox("Municipio");

		getStaticQueriesServiceAsync().getMunicipios(new RPCFillComboBoxHandler(cbMunicipios, fs, 3));

		final TextField tfTelCliente = new TextField("Tel\u00e9fono", "telcli");
		fs.add(tfTelCliente, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function recargarClientes = new Function() {

			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getInfoClientes(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, true, new int[] { 100, 100, 100, 100, 100 },
						new boolean[] { true, true, true, true, true }, new RowLayoutData(), null));
				tfIdCliente.setValue("");
				tfNomCliente.setValue("");
				tfDirCliente.setValue("");
				cbMunicipios.setValue("");
				tfTelCliente.setValue("");
			}

		};

		fs.setButtonAlign(Position.CENTER);
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				// if(com.google.gwt.user.client.Window.confirm(
				// "Esta seguro que desea borrar este cliente: "
				// +r.getAsString("id"))){
				getStaticAdminQueriesServiceAsync().deleteClient(r.getAsString("id"),
						new MyBooleanAsyncCallback("Cliente Borrado", recargarClientes));
				// }
			}
		});
		recargarClientes.execute();

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync().addClient(tfIdCliente.getValueAsString(),
						tfNomCliente.getValueAsString(), tfDirCliente.getValueAsString(),
						cbMunicipios.getValueAsString(), tfTelCliente.getValueAsString(),
						new MyBooleanAsyncCallback("Cliente creado exitosamente", recargarClientes));
			}
		});
		fs.addButton(btnSave);
	}

	private static void toConductoresPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos de Conductores");
		fs.setLabelWidth(40);

		final TextField tfIdCond = new TextField("C\u00f3digo", "idcli");
		fs.add(tfIdCond, new AnchorLayoutData("100%"));

		final TextField tfNomCond = new TextField("Nombre", "nombrecli");
		fs.add(tfNomCond, new AnchorLayoutData("100%"));

		final NumberField tfCedulaCond = new NumberField("C\u00e9dula", "ced");
		tfCedulaCond.setAllowDecimals(false);
		tfCedulaCond.setDecimalPrecision(0);
		fs.add(tfCedulaCond, new AnchorLayoutData("100%"));

		final TextField tfCelCond = new TextField("Celular", "celular");
		fs.add(tfCelCond, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function recargarClientes = new Function() {

			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getInfoDrivers(new RPCFillTable(grid, gridContainerPanel, null, true,
						true, true, null, null, new RowLayoutData(), null));
				tfIdCond.setValue("");
				tfNomCond.setValue("");
				tfCedulaCond.setValue("");
				tfCelCond.setValue("");
				tfIdCond.setDisabled(false);
			}

		};

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdCond.setDisabled(true);
				tfIdCond.setValue(r.getAsString("C\u00f3digo"));
				tfNomCond.setValue(r.getAsString("Nombre"));
				tfCedulaCond.setValue(r.getAsString("C\u00e9dula"));
				tfCelCond.setValue(r.getAsString("celular"));
			}
		});

		fs.setButtonAlign(Position.CENTER);
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				// if(com.google.gwt.user.client.Window.confirm(
				// "Esta seguro que desea borrar este cliente: "
				// +r.getAsString("id"))){
				getStaticAdminQueriesServiceAsync().deleteDriver(r.getAsString("C\u00f3digo"),
						new MyBooleanAsyncCallback("Conductor Borrado", recargarClientes));
				// }
			}
		});
		recargarClientes.execute();

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdCond.getText().equals("") && !tfNomCond.getText().equals("")) {
					if (tfIdCond.isDisabled()) {
						getStaticAdminQueriesServiceAsync().updateDriver(tfIdCond.getValueAsString(),
								tfNomCond.getValueAsString(), tfCedulaCond.getValueAsString(),
								tfCelCond.getValueAsString(),
								new MyBooleanAsyncCallback("Conductor actualizado exitosamente", recargarClientes));
					} else {
						getStaticAdminQueriesServiceAsync().addDriver(tfIdCond.getValueAsString(),
								tfNomCond.getValueAsString(), tfCedulaCond.getValueAsString(),
								tfCelCond.getValueAsString(),
								new MyBooleanAsyncCallback("Conductor creado exitosamente", recargarClientes));
					}
				} else {

					ClientUtils.alert("Error", "Por favor llene todos los campos", ClientUtils.ERROR);

				}

			}
		});
		fs.addButton(btnSave);
	}

	private static void toContratistasPanel(FormPanel formPanel, final Panel gridContainerPanel,
			final ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos de Contratista");
		fs.setLabelWidth(40);

		final TextField tfIdCont = new TextField("C\u00f3digo", "idcont");
		fs.add(tfIdCont, new AnchorLayoutData("100%"));

		final TextField tfNomCont = new TextField("Nombre", "nombrecont");
		fs.add(tfNomCont, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function recargarContratistas = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync()
						.getContratistasAsRowModel(new RPCFillTable(grid, gridContainerPanel, null, true, true, true,
								new int[] { 100, 100 }, new boolean[] { true, true }, new RowLayoutData(), null));
				tfIdCont.setValue("");
				tfIdCont.setDisabled(false);
				tfNomCont.setValue("");
			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delContratista(r.getAsString("id"),
						new MyBooleanAsyncCallback("Contratista borrado exitosamente", recargarContratistas));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdCont.setDisabled(true);
				tfIdCont.setValue(r.getAsString("id"));
				tfNomCont.setValue(r.getAsString("nombre"));
			}
		});
		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (tfIdCont.isDisabled()) {
					getStaticAdminQueriesServiceAsync().updateContratista(tfIdCont.getValueAsString(),
							tfNomCont.getValueAsString(),
							new MyBooleanAsyncCallback("Contratista actualizado con exito", recargarContratistas));
				} else {
					getStaticAdminQueriesServiceAsync().addContratista(tfIdCont.getValueAsString(),
							tfNomCont.getValueAsString(),
							new MyBooleanAsyncCallback("Contratista agregado con exito", recargarContratistas));
				}
			}
		});
		fs.addButton(btnSave);
		recargarContratistas.execute();
	}

	private static void toTiposTrabajosPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos de Tipo de Trabajo");
		fs.setLabelWidth(40);

		final TextField tfIdTpoTrab = new TextField("C\u00f3digo", "idtpotrab");
		fs.add(tfIdTpoTrab, new AnchorLayoutData("100%"));

		final TextField tfNombreTpoTrab = new TextField("Nombre", "nombretpotrab");
		fs.add(tfNombreTpoTrab, new AnchorLayoutData("100%"));

		final Checkbox chkRepCampo = new Checkbox("Reportable desde campo?");
		fs.add(chkRepCampo, new AnchorLayoutData("100%"));

		// Agregado modulo admin de mensajes
		FieldSet fs1 = new FieldSet("Control de tiempos");
		fs1.setLabelWidth(30);

		final NumberField nfHoras = new NumberField("Hora");
		nfHoras.setAllowDecimals(false);
		nfHoras.setValue("00");
		fs1.add(nfHoras, new AnchorLayoutData("100%"));

		nfHoras.addListener(new FieldListenerAdapter() {
			@Override
			public void onBlur(Field field) {
				if (nfHoras.getValueAsString() == null || nfHoras.getValueAsString().equals("")) {
					nfHoras.setValue("00");
				}
			}
		});

		final NumberField nfMinutos = new NumberField("Minutos");
		nfMinutos.setAllowDecimals(false);
		nfMinutos.setMaxLength(2);
		nfMinutos.setValue("00");
		fs1.add(nfMinutos, new AnchorLayoutData("100%"));

		nfMinutos.addListener(new FieldListenerAdapter() {
			@Override
			public void onBlur(Field field) {
				if (nfMinutos.getValueAsString() != null && !nfMinutos.getValueAsString().equals("")) {
					if (nfMinutos.getValue().intValue() > 59) {
						nfMinutos.setValue(59);
					} else if (nfMinutos.getValue().intValue() > 0 && nfMinutos.getValue().intValue() < 10) {
						nfMinutos.setValue("0" + nfMinutos.getValueAsString());
					} else if (nfMinutos.getValue().intValue() == 0) {
						nfMinutos.setValue("00");
					}
				} else {
					nfMinutos.setValue("00");
				}
			}
		});

		final NumberField nfSegundos = new NumberField("Segundos");
		nfSegundos.setAllowDecimals(false);
		nfSegundos.setMaxLength(2);
		nfSegundos.setValue("00");
		fs1.add(nfSegundos, new AnchorLayoutData("100%"));

		nfSegundos.addListener(new FieldListenerAdapter() {
			@Override
			public void onBlur(Field field) {
				if (nfSegundos.getValueAsString() != null && !nfSegundos.getValueAsString().equals("")) {
					if (nfSegundos.getValue().intValue() > 59) {
						nfSegundos.setValue(59);
					} else if (nfSegundos.getValue().intValue() > 0 && nfSegundos.getValue().intValue() < 10) {
						nfSegundos.setValue("0" + nfSegundos.getValueAsString());
					} else if (nfSegundos.getValue().intValue() == 0) {
						nfSegundos.setValue("00");
					}
				} else {
					nfSegundos.setValue("00");
				}
			}
		});

		fs.add(fs1);

		final NumberField nfDuracionProm = new NumberField("Duracion prom. (Min)");
		nfDuracionProm.setAllowDecimals(false);
		nfDuracionProm.setValue("00");
		fs.add(nfDuracionProm, new AnchorLayoutData("100%"));
		//

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();
		final Function cargarTiposTrabajo = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllTTrabajosAsRowModel(new RPCFillTable(grid, gridContainerPanel,
						"Modificar Items de precierre", true, true, true,
						new int[] { 100, 100, 100, 100, 100, 100, 100 },
						new boolean[] { true, true, true, true, true, true, false }, new RowLayoutData(), null));
				tfIdTpoTrab.setDisabled(false);
				tfIdTpoTrab.setValue("");
				tfNombreTpoTrab.setValue("");
				chkRepCampo.setValue(false);
				nfHoras.setValue("00");
				nfMinutos.setValue("00");
				nfSegundos.setValue("00");
				nfDuracionProm.setValue("");
			}
		};

		fs.setButtonAlign(Position.CENTER);
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delTTrabajo(r.getAsString("id"),
						new MyBooleanAsyncCallback("No se puede eliminar el tipo trabajo",
								"Tipo de trabajo borrado exitosamente", cargarTiposTrabajo));
			}
		});

		grid.addDetListener(new TiposTrabajosDetListenerPlanning(gui, cargarTiposTrabajo));
		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdTpoTrab.setValue(r.getAsString("id"));
				tfIdTpoTrab.setDisabled(true);
				tfNombreTpoTrab.setValue(r.getAsString("nombre"));
				chkRepCampo.setValue(r.getAsString("Reporta desde campo").equals("S"));

				if (r.getAsString("maxduracion") == null) {
					nfHoras.setValue("00");
					nfMinutos.setValue("00");
					nfSegundos.setValue("00");
				} else {
					nfHoras.setValue(r.getAsString("maxduracion").split(":")[0]);
					nfMinutos.setValue(r.getAsString("maxduracion").split(":")[1]);
					nfSegundos.setValue(r.getAsString("maxduracion").split(":")[2]);
				}

				nfDuracionProm.setValue(r.getAsString("Tiempo promedio"));
			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {

				if (tfIdTpoTrab.getValueAsString() != null && !tfIdTpoTrab.getValueAsString().equalsIgnoreCase("")) {
					if (tfNombreTpoTrab.getValueAsString() != null
							&& !tfNombreTpoTrab.getValueAsString().equalsIgnoreCase("")) {
						String rango = "";
						if (!nfHoras.getValueAsString().equals("") || !nfMinutos.getValueAsString().equals("")
								|| !nfSegundos.getValueAsString().equals("")) {
							rango = nfHoras.getEl().getValue() + ":" + nfMinutos.getEl().getValue() + ":"
									+ nfSegundos.getEl().getValue();
							if (rango.replace("0", "").replace(":", "").equals("")) {
								rango = null;
							}
						} else {
							rango = null;
						}

						if (!tfIdTpoTrab.isDisabled()) {
							getStaticAdminQueriesServiceAsync().addTipoTrabajo(tfIdTpoTrab.getValueAsString(),
									tfNombreTpoTrab.getValueAsString(), chkRepCampo.getValue(), rango,
									nfDuracionProm.getValueAsString(),
									new MyBooleanAsyncCallback("Error al crear el tipo trabajo",
											"Tipo de trabajo creado exitosamente", cargarTiposTrabajo));
						} else {
							getStaticAdminQueriesServiceAsync().updateTipoTrabajo(tfIdTpoTrab.getValueAsString(),
									tfNombreTpoTrab.getValueAsString(), chkRepCampo.getValue(), rango,
									nfDuracionProm.getValueAsString(),
									new MyBooleanAsyncCallback("Error al actualizar el tipo trabajo",
											"Tipo de trabajo actualizado exitosamente", cargarTiposTrabajo));
						}

					} else {
						ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
					}

				} else {
					ClientUtils.alert("Info.", "El codigo es requerido.", ClientUtils.ERROR);
				}

				/*
				 * if (!tfIdTpoTrab.isDisabled()) {
				 * getStaticAdminQueriesServiceAsync().addTipoTrabajo(
				 * tfIdTpoTrab.getValueAsString(),
				 * tfNombreTpoTrab.getValueAsString(), chkRepCampo.getValue(),
				 * new MyBooleanAsyncCallback(
				 * "Tipo de trabajo creado exitosamente", cargarTiposTrabajo));
				 * } else {
				 * getStaticAdminQueriesServiceAsync().updateTipoTrabajo(
				 * tfIdTpoTrab.getValueAsString(),
				 * tfNombreTpoTrab.getValueAsString(), chkRepCampo.getValue(),
				 * new MyBooleanAsyncCallback(
				 * "Tipo de trabajo actualizado exitosamente",
				 * cargarTiposTrabajo)); }
				 */
			}
		});
		cargarTiposTrabajo.execute();
		fs.addButton(btnSave);
	}

	private static void toTiposRecursosPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos del Tipo de Recurso");
		fs.setLabelWidth(40);

		final TextField tfIdTipoRec = new TextField("C\u00f3digo", "idtporec");
		fs.add(tfIdTipoRec, new AnchorLayoutData("50%"));

		final TextField tfNombreTipoRec = new TextField("Nombre", "nombretporec");
		fs.add(tfNombreTipoRec, new AnchorLayoutData("100%"));

		final TextField tfCapacidad = new TextField("Capacidad", "capacidad");
		fs.add(tfCapacidad, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		fs.setButtonAlign(Position.CENTER);

		grid.setHeight(480);
		grid.setWidth(500);

		grid.setEnableHdMenu(false);

		final Function cargarTipoRecurso = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getAllTiposRecurso(
						new RPCFillTable(grid, gridContainerPanel, null, true, true, true, new int[] { 100, 100, 100 },
								new boolean[] { true, true, true }, new RowLayoutData(), null));
				tfIdTipoRec.setDisabled(false);
				tfIdTipoRec.setValue("");
				tfNombreTipoRec.setValue("");
				tfCapacidad.setValue("");
			}
		};

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delTRecurso(r.getAsString("id"),
						new MyBooleanAsyncCallback("Tipo de recurso borrado con exito", cargarTipoRecurso));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdTipoRec.setValue(r.getAsString("id"));
				tfIdTipoRec.setDisabled(true);
				tfNombreTipoRec.setValue(r.getAsString("nombre"));
				tfCapacidad.setValue(r.getAsString("capacidad"));
			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdTipoRec.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addTRecurso(tfIdTipoRec.getValueAsString(),
							tfNombreTipoRec.getValueAsString(), tfCapacidad.getValueAsString(),
							new MyBooleanAsyncCallback("Tipo de recurso agregado exitosamente", cargarTipoRecurso));
				} else {
					getStaticAdminQueriesServiceAsync().updateTRecurso(tfIdTipoRec.getValueAsString(),
							tfNombreTipoRec.getValueAsString(), tfCapacidad.getValueAsString(),
							new MyBooleanAsyncCallback("Tipo de recurso actualizado exitosamente", cargarTipoRecurso));
				}
			}
		});
		fs.addButton(btnSave);
		cargarTipoRecurso.execute();
	}

	private static void toMunicipiosPanel(FormPanel formPanel, final Panel gridContainerPanel, ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos de Municipio");
		fs.setLabelWidth(40);

		final TextField tfIdMunip = new TextField("C\u00f3digo", "idmunip");
		fs.add(tfIdMunip, new AnchorLayoutData("50%"));

		final TextField tfNombreMunip = new TextField("Nombre", "nombremunip");
		fs.add(tfNombreMunip, new AnchorLayoutData("100%"));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		fs.setButtonAlign(Position.CENTER);

		final int[] municipiosSizes = new int[] { 100, 100 };
		final boolean[] municipiosVisibility = new boolean[] { true, true };

		final Function cargarMunicipios = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync()
						.getMunicipiosRowModel(new RPCFillTable(grid, gridContainerPanel, "Ver detalles", true, true,
								true, municipiosSizes, municipiosVisibility, new RowLayoutData(), null));
				tfIdMunip.setValue("");
				tfIdMunip.setDisabled(false);
				tfNombreMunip.setValue("");
			}
		};

		grid.addDetListener(new MunipDetailListener());

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delMunicipio(r.getAsString("id"),
						new MyBooleanAsyncCallback("Municipio borrado", cargarMunicipios));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdMunip.setValue(r.getAsString("id"));
				tfIdMunip.setDisabled(true);
				tfNombreMunip.setValue(r.getAsString("nombre"));
			}
		});

		Button btnSave = new Button("Guardar");
		btnSave.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (!tfIdMunip.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addMunicipio(tfIdMunip.getValueAsString(),
							tfNombreMunip.getValueAsString(),
							new MyBooleanAsyncCallback("Municipio creado", cargarMunicipios));
				} else {
					getStaticAdminQueriesServiceAsync().updateMunicipio(tfIdMunip.getValueAsString(),
							tfNombreMunip.getValueAsString(),
							new MyBooleanAsyncCallback("Municipio modificado", cargarMunicipios));
				}
			}
		});
		fs.addButton(btnSave);
		cargarMunicipios.execute();
	}

	// @SuppressWarnings("deprecation")
	private static void showTrabajosWizard(final ControlWebGUI gui, String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir, String munip, String barrio, String telefono,
			String prioridad, String desc, String fechaentrega, String jornada, String hora,
			final Function recargarTrabajos, final MyGridPanel grid) {

		WizardWindow wnd = new WizardWindow("Trabajo", 350, 250, false, true);

		Panel p1 = wnd.addPage(null, new FormLayout());

		final TextField tfIdTrabajo = new TextField("C\u00f3digo");
		wnd.addItem(tfIdTrabajo, new AnchorLayoutData("70%"));

		final ComboBox cbTiposTrabajo = new ComboBox("Tipo Trabajo");

		getStaticQueriesServiceAsync().getTiposTrabajo(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbTiposTrabajo, p1, 1));

		wnd.addItemToArray(cbTiposTrabajo, 1, 0);

		final TextField tfNumViaje = new TextField("Num. Viaje");
		wnd.addItem(tfNumViaje, new AnchorLayoutData("100%"));

		final TextField tfCodCliente = new TextField("Codigo Cliente");
		wnd.addItem(tfCodCliente, new AnchorLayoutData("100%"));

		final TextField tfNomCliente = new TextField("Nombre Cliente");
		wnd.addItem(tfNomCliente, new AnchorLayoutData("100%"));

		Panel p2 = wnd.addPage(null, new FormLayout());

		final DateField dfFechaTrabajo = new DateField("Fecha", "Y/m/d H:i");
		dfFechaTrabajo.setFieldMsgTarget("under");
		dfFechaTrabajo.setInvalidText("Formato de fecha incorrecto");

		wnd.addItem(dfFechaTrabajo, new AnchorLayoutData("100%"));

		final TextField tfDireccion = new TextField("Direcci\u00f3n");
		wnd.addItem(tfDireccion, new AnchorLayoutData("100%"));

		final ComboBox cbMunicipios = new ComboBox("Municipio");

		getStaticQueriesServiceAsync().getMunicipios(new RPCFillComboBoxHandler(cbMunicipios, p2, 2));

		wnd.addItemToArray(cbMunicipios, 2, 1);

		final TextField tfBarrio = new TextField("Barrio");
		wnd.addItem(tfBarrio, new AnchorLayoutData("100%"));

		final TextField tfTelefono = new TextField("Telefono");
		wnd.addItem(tfTelefono, new AnchorLayoutData("100%"));

		final ComboBox cbPrioridad = ClientUtils.createCombo("Tiene Prioridad?",
				new SimpleStore(new String[] { "id", "nombre" }, new String[][] { { "S", "SI" }, { "N", "NO" } }));
		final TextField taDesc = new TextField("Descripci\u00f3n", "tadesc");

		final DateField dfFechaEntrega = new DateField("Fecha Entrega", "Y/m/d");

		/*
		 * final ComboBox cbJornada = ClientUtils.createCombo("Jornada", new
		 * SimpleStore(new String[] { "id", "nombre" }, new String[][] { { "1",
		 * "Manana" }, { "2", "Tarde" }, { "3", "Noche" } }));
		 */

		final MyComboBox cbJornada = new MyComboBox("Jornada");

		final TimeField tmHoraEntrega = new TimeField("Hora");

		Panel p = wnd.addPage(null, new FormLayout(), new Function() {
			@Override
			public void execute() {
				if (!tfIdTrabajo.isDisabled()) {
					getStaticAdminQueriesServiceAsync().addTrabajo(tfIdTrabajo.getValueAsString(),
							cbTiposTrabajo.getValue(), tfNumViaje.getValueAsString(), tfCodCliente.getValueAsString(),
							tfNomCliente.getValueAsString(), dfFechaTrabajo.getEl().getValue(),
							tfDireccion.getValueAsString(), cbMunicipios.getValue(), tfBarrio.getValueAsString(),
							tfTelefono.getValueAsString(), cbPrioridad.getValue(), taDesc.getValueAsString(),
							dfFechaEntrega.getEl().getValue(), cbJornada.getValue(), tmHoraEntrega.getValue(),
							gui.getProceso().getId(), gui.getUsuario().getId(), "",
							new MyBooleanAsyncCallback("Creado exitosamente", recargarTrabajos));
				} else {
					getStaticAdminQueriesServiceAsync().updateTrabajo(tfIdTrabajo.getValueAsString(),
							cbTiposTrabajo.getValue(), tfNumViaje.getValueAsString(), tfCodCliente.getValueAsString(),
							tfNomCliente.getValueAsString(), dfFechaTrabajo.getEl().getValue(),
							tfDireccion.getValueAsString(), cbMunicipios.getValue(), tfBarrio.getValueAsString(),
							tfTelefono.getValueAsString(), cbPrioridad.getValue(), taDesc.getValueAsString(),
							dfFechaEntrega.getEl().getValue(), cbJornada.getValue(), tmHoraEntrega.getValue(),
							gui.getUsuario().getId(),
							new MyBooleanAsyncCallback("Actualizado exitosamente", recargarTrabajos));
				}

			}
		});

		wnd.addItem(cbPrioridad, new AnchorLayoutData("70%"));
		wnd.addItemToArray(cbPrioridad, 0, 2);

		wnd.addItem(taDesc, new AnchorLayoutData("100%"));

		dfFechaEntrega.setFieldMsgTarget("under");
		dfFechaEntrega.setInvalidText("Formato de fecha incorrecto");
		wnd.addItem(dfFechaEntrega, new AnchorLayoutData("100%"));

		tmHoraEntrega.setIncrement(5);
		tmHoraEntrega.setFormat("H:i");

		gui.getQueriesServiceProxy().getJornadas(new RPCFillComplexComboHandler(cbJornada, p, 3, new Function() {
			@Override
			public void execute() {
				cbJornada.getStore().sort("id");
			}
		}));
		// wnd.addItem(cbJornada, new AnchorLayoutData("70%"));

		wnd.addItemToArray(cbJornada, 3, 2);

		wnd.addItem(tmHoraEntrega, new AnchorLayoutData("70%"));

		if (codtrab != null) {
			tfIdTrabajo.setValue(codtrab);
			tfIdTrabajo.setDisabled(true);
		}

		if (codttrab != null) {
			cbTiposTrabajo.setValue(codttrab);
			cbTiposTrabajo.setDisabled(true);
		}

		if (numviaje != null) {
			tfNumViaje.setValue(numviaje);
		}

		if (codcli != null) {
			tfCodCliente.setValue(codcli);
		}
		if (nomcli != null) {
			tfNomCliente.setValue(nomcli);
		}

		if (fecha != null) {
			dfFechaTrabajo.setValue(fecha);
		}

		if (dir != null) {
			tfDireccion.setValue(dir);
		}

		if (munip != null) {
			cbMunicipios.setValue(munip);
		}

		if (barrio != null) {
			tfBarrio.setValue(barrio);
		}

		if (telefono != null) {
			tfTelefono.setValue(telefono);
		}

		if (prioridad != null) {
			cbPrioridad.setValue(prioridad);
		}

		if (desc != null) {
			taDesc.setValue(desc);
		}

		if (fechaentrega != null) {
			dfFechaEntrega.setValue(fechaentrega);
		}

		if (jornada != null) {
			cbJornada.setValue(jornada);
		}

		if (hora != null) {
			tmHoraEntrega.setValue(hora);
		}

		wnd.show();

	}

	private static void toTrabajosPanel(Panel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos de Trabajo");
		fs.setLabelWidth(40);

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		fs.setButtonAlign(Position.CENTER);

		Button btnNewTrabajo = new Button("Crear Nuevo trabajo");

		fs.addButton(btnNewTrabajo);

		final Function recargarTrabajos = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getTrabajos(gui.getProceso().getId(), new RPCFillTable(grid,
						gridContainerPanel, null, true, true, true, null, null, new RowLayoutData()));
			}
		};

		btnNewTrabajo.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				showTrabajosWizard(gui, null, null, null, null, null, null, null, null, null, null, null, null, null,
						null, null, recargarTrabajos, grid);

			}
		});

		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().delTrabajo(r.getAsString("ID Trabajo"),
						r.getAsString("_tipotrabajo_"),
						new MyBooleanAsyncCallback("Registro Borrado", recargarTrabajos));
			}
		});

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				showTrabajosWizard(gui, r.getAsString("ID Trabajo"), r.getAsString("_tipotrabajo_"),
						r.getAsString("Num. Viaje"), r.getAsString("_cliente_"), r.getAsString("Cliente"),
						r.getAsString("Fecha"), r.getAsString("Direcci\u00f3n"), r.getAsString("_municipio_"),
						r.getAsString("Barrio"), r.getAsString("Telefono"),
						r.getAsString("Prioritario").substring(0, 1), r.getAsString("Descripci\u00f3n"),
						r.getAsString("Fecha Entrega"), r.getAsString("_jornada_"), r.getAsString("Hora Entrega"),
						recargarTrabajos, grid);
			}
		});

		recargarTrabajos.execute();
	}

	private static void toRecursosPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos de Recurso");
		fs.setLabelWidth(40);

		final TextField tfIdRec = new TextField("C\u00f3digo", "idrecurso");
		fs.add(tfIdRec, new AnchorLayoutData("70%"));

		final ComboBox cbContRec = new ComboBox("Contratista");

		getStaticQueriesServiceAsync().getContratistas(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbContRec, fs, 1));

		final ComboBox cbTiposRec = new ComboBox("Tipo");

		getStaticQueriesServiceAsync().getTiposRecurso(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbTiposRec, fs, 2));

		final TextField tfNombreRec = new TextField("Nombre", "nombrecurso");
		fs.add(tfNombreRec, new AnchorLayoutData("100%"));

		final TextField tfUsuarioMovil = new TextField("Usuario M\u00f3vil", "usuariom");
		fs.add(tfUsuarioMovil, new AnchorLayoutData("100%"));

		final TextField tfClaveMovil = new TextField("Clave M\u00f3vil", "clavem");
		fs.add(tfClaveMovil, new AnchorLayoutData("100%"));

		final MyComboBox cbHorarios = new MyComboBox("Horario");
		//final TextField tfHorario = new TextField("Horario");
		//final TextField tfCodHorario = new TextField("Codigo Horario");
		//fs.add(tfHorario, new AnchorLayoutData("100%"));
		//fs.add(tfCodHorario, new AnchorLayoutData("100%"));
		final TextField tfCodigoSAP = new TextField("Codigo SAP", "clavem");
		fs.add(tfCodigoSAP, new AnchorLayoutData("100%"));

		getStaticQueriesServiceAsync().getHorariosCombo(new RPCFillComplexComboHandler(cbHorarios, fs, 7));

		formPanel.add(fs);
		

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarRecursos = new Function() {
			@Override
			public void execute() {
				int[] sizes = new int[] { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };
				boolean[] visibility = new boolean[] { true, true, true, false, true, false, true, true, true, true,
						true };
				if (gui.getProceso() != null) {
					getStaticQueriesServiceAsync().getAllRecursosProceso(gui.getProceso().getId(),
							new RPCFillTable(grid, gridContainerPanel, null, true, true, true, sizes, visibility,
									new RowLayoutData(), null));

				}
				tfIdRec.setDisabled(false);
				tfIdRec.setValue("");
				tfClaveMovil.setValue("");
				tfNombreRec.setValue("");
				tfUsuarioMovil.setValue("");
				tfCodigoSAP.setValue("");
				cbHorarios.setValue("");
				//tfCodHorario.setValue("");
				//tfHorario.setValue("");

				if (cbContRec.isRendered()) {
					cbContRec.setValue("");
				}
				if (cbTiposRec.isRendered()) {
					cbTiposRec.setValue("");
				}
			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.setHeight(480);
		grid.setWidth(500);

		grid.setEnableHdMenu(false);
		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				
				tfIdRec.setDisabled(true);
				tfIdRec.setValue(r.getAsString("id"));
				tfClaveMovil.setValue(r.getAsString("clave"));
				tfNombreRec.setValue(r.getAsString("nombre"));
				tfUsuarioMovil.setValue(r.getAsString("usuario"));
				cbContRec.setValue(r.getAsString("id_contratista"));
				cbTiposRec.setValue(r.getAsString("id_tiporecurso"));
				tfCodigoSAP.setValue(r.getAsString("Codigo SAP"));
				cbHorarios.setValue(r.getAsString("horario"));
				//tfHorario.setValue(r.getAsString("horario"));
				//tfCodHorario.setValue(r.getAsString("id_horario"));
			}
		});
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().deleteRecurso(r.getAsString("id"), r.getAsString("id_contratista"),
						new MyBooleanAsyncCallback("Recurso eliminado correctamente", cargarRecursos));
			}
		});

		Button btnSave = new Button("Guardar");

		btnSave.addListener(new ButtonListenerAdapter() {

			@Override
			public void onClick(Button button, EventObject e) {

				if (tfIdRec.getValueAsString() != null && !tfIdRec.getValueAsString().equalsIgnoreCase("")) {

					if (cbContRec.getValueAsString() != null && !cbContRec.getValueAsString().equalsIgnoreCase("")) {

						if (cbTiposRec.getValueAsString() != null
								&& !cbTiposRec.getValueAsString().equalsIgnoreCase("")) {

							if (tfNombreRec.getValueAsString() != null
									&& !tfNombreRec.getValueAsString().equalsIgnoreCase("")) {

								if (tfUsuarioMovil.getValueAsString() != null
										&& !tfUsuarioMovil.getValueAsString().equalsIgnoreCase("")) {

									if (tfClaveMovil.getValueAsString() != null
											&& !tfClaveMovil.getValueAsString().equalsIgnoreCase("")) {

										if (cbHorarios.getValueAsString() != null
												&& !cbHorarios.getValueAsString().equalsIgnoreCase("")) {

											if (!tfIdRec.isDisabled()) {
												getStaticAdminQueriesServiceAsync().addRecurso(
														tfIdRec.getValueAsString(), cbContRec.getValueAsString(),
														cbTiposRec.getValueAsString(), tfNombreRec.getValueAsString(),
														tfUsuarioMovil.getValueAsString(),
														tfClaveMovil.getValueAsString(), tfCodigoSAP.getValueAsString(),
														cbHorarios.getValue(), new MyBooleanAsyncCallback(
																"Recurso creado exitosamente", cargarRecursos));
											} else {
												getStaticAdminQueriesServiceAsync().updateRecurso(
														tfIdRec.getValueAsString(), cbContRec.getValueAsString(),
														cbTiposRec.getValueAsString(), tfNombreRec.getValueAsString(),
														tfUsuarioMovil.getValueAsString(),
														tfClaveMovil.getValueAsString(), tfCodigoSAP.getValueAsString(),
														cbHorarios.getValue(), new MyBooleanAsyncCallback(
																"Recurso actualizado exitosamente", cargarRecursos));
											}

										} else {
											ClientUtils.alert("Info.", "Debe seleccionar un horario.",
													ClientUtils.ERROR);
										}
									} else {
										ClientUtils.alert("Info.", "La clave es requerida.", ClientUtils.ERROR);
									}

								} else {
									ClientUtils.alert("Info.", "El usuario es requerido.", ClientUtils.ERROR);
								}

							} else {
								ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
							}

						} else {
							ClientUtils.alert("Info.", "El tipo de recurso es requerido.", ClientUtils.ERROR);
						}

					} else {
						ClientUtils.alert("Info.", "El contratista es requerido.", ClientUtils.ERROR);
					}

				} else {
					ClientUtils.alert("Info.", "El codigo es requerido.", ClientUtils.ERROR);
				}

			}
		});
		fs.addButton(btnSave);
		cargarRecursos.execute();
	}

	private static void toRecursosPanelOld(FormPanel formPanel, final Panel gridContainerPanel,
			final ControlWebGUI gui) {

		FieldSet fs = new FieldSet("Datos de Recurso");
		fs.setLabelWidth(40);

		final TextField tfIdRec = new TextField("C\u00f3digo", "idrecurso");
		fs.add(tfIdRec, new AnchorLayoutData("70%"));

		final ComboBox cbContRec = new ComboBox("Contratista");

		getStaticQueriesServiceAsync().getContratistas(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbContRec, fs, 1));

		final ComboBox cbTiposRec = new ComboBox("Tipo");

		getStaticQueriesServiceAsync().getTiposRecurso(gui.getProceso().getId(),
				new RPCFillComboBoxHandler(cbTiposRec, fs, 2));

		final TextField tfNombreRec = new TextField("Nombre", "nombrecurso");
		fs.add(tfNombreRec, new AnchorLayoutData("100%"));

		final TextField tfUsuarioMovil = new TextField("Usuario M\u00f3vil", "usuariom");
		fs.add(tfUsuarioMovil, new AnchorLayoutData("100%"));

		final TextField tfClaveMovil = new TextField("Clave M\u00f3vil", "clavem");
		fs.add(tfClaveMovil, new AnchorLayoutData("100%"));

		final MyComboBox cbHorarios = new MyComboBox("Horario");

		final TextField tfCodigoSAP = new TextField("Codigo SAP", "clavem");
		fs.add(tfCodigoSAP, new AnchorLayoutData("100%"));

		getStaticQueriesServiceAsync().getHorariosCombo(new RPCFillComplexComboHandler(cbHorarios, fs, 7));

		formPanel.add(fs);

		final MyGridPanel grid = new MyGridPanel();

		final Function cargarRecursos = new Function() {
			@Override
			public void execute() {
				int[] sizes = new int[] { 100, 100, 100, 100, 100, 100, 100, 100, 100 };
				boolean[] visibility = new boolean[] { true, true, true, false, true, false, true, true, true };
				if (gui.getProceso() != null) {
					getStaticQueriesServiceAsync().getAllRecursosProceso(gui.getProceso().getId(),
							new RPCFillTable(grid, gridContainerPanel, null, true, true, true, sizes, visibility,
									new RowLayoutData(), null));

				}
				tfIdRec.setDisabled(false);
				tfIdRec.setValue("");
				tfClaveMovil.setValue("");
				tfNombreRec.setValue("");
				tfUsuarioMovil.setValue("");
				tfCodigoSAP.setValue("");
				cbHorarios.setValue("");

				if (cbContRec.isRendered()) {
					cbContRec.setValue("");
				}
				if (cbTiposRec.isRendered()) {
					cbTiposRec.setValue("");
				}
			}
		};

		fs.setButtonAlign(Position.CENTER);

		grid.setHeight(480);
		grid.setWidth(500);

		grid.setEnableHdMenu(false);
		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				tfIdRec.setDisabled(true);
				tfIdRec.setValue(r.getAsString("id"));
				tfClaveMovil.setValue(r.getAsString("clave"));
				tfNombreRec.setValue(r.getAsString("nombre"));
				tfUsuarioMovil.setValue(r.getAsString("usuario"));
				cbContRec.setValue(r.getAsString("id_contratista"));
				cbTiposRec.setValue(r.getAsString("id_tiporecurso"));
				tfCodigoSAP.setValue(r.getAsString("Codigo SAP"));
				cbHorarios.setValue(r.getAsString("codigohorario"));
			}
		});
		grid.addDeleteListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				getStaticAdminQueriesServiceAsync().deleteRecurso(r.getAsString("id"), r.getAsString("id_contratista"),
						new MyBooleanAsyncCallback("Recurso eliminado correctamente", cargarRecursos));
			}
		});

		Button btnSave = new Button("Guardar");

		btnSave.addListener(new ButtonListenerAdapter() {

			@Override
			public void onClick(Button button, EventObject e) {

				if (tfIdRec.getValueAsString() != null && !tfIdRec.getValueAsString().equalsIgnoreCase("")) {

					if (cbContRec.getValueAsString() != null && !cbContRec.getValueAsString().equalsIgnoreCase("")) {

						if (cbTiposRec.getValueAsString() != null
								&& !cbTiposRec.getValueAsString().equalsIgnoreCase("")) {

							if (tfNombreRec.getValueAsString() != null
									&& !tfNombreRec.getValueAsString().equalsIgnoreCase("")) {

								if (tfUsuarioMovil.getValueAsString() != null
										&& !tfUsuarioMovil.getValueAsString().equalsIgnoreCase("")) {

									if (tfClaveMovil.getValueAsString() != null
											&& !tfClaveMovil.getValueAsString().equalsIgnoreCase("")) {

										if (cbHorarios.getValueAsString() != null
												&& !cbHorarios.getValueAsString().equalsIgnoreCase("")) {

											if (!tfIdRec.isDisabled()) {
												getStaticAdminQueriesServiceAsync().addRecurso(
														tfIdRec.getValueAsString(), cbContRec.getValueAsString(),
														cbTiposRec.getValueAsString(), tfNombreRec.getValueAsString(),
														tfUsuarioMovil.getValueAsString(),
														tfClaveMovil.getValueAsString(), tfCodigoSAP.getValueAsString(),
														cbHorarios.getValue(), new MyBooleanAsyncCallback(
																"Recurso creado exitosamente", cargarRecursos));
											} else {
												getStaticAdminQueriesServiceAsync().updateRecurso(
														tfIdRec.getValueAsString(), cbContRec.getValueAsString(),
														cbTiposRec.getValueAsString(), tfNombreRec.getValueAsString(),
														tfUsuarioMovil.getValueAsString(),
														tfClaveMovil.getValueAsString(), tfCodigoSAP.getValueAsString(),
														cbHorarios.getValue(), new MyBooleanAsyncCallback(
																"Recurso actualizado exitosamente", cargarRecursos));
											}

										} else {
											ClientUtils.alert("Info.", "Debe seleccionar un horario.",
													ClientUtils.ERROR);
										}
									} else {
										ClientUtils.alert("Info.", "La clave es requerida.", ClientUtils.ERROR);
									}

								} else {
									ClientUtils.alert("Info.", "El usuario es requerido.", ClientUtils.ERROR);
								}

							} else {
								ClientUtils.alert("Info.", "El nombre es requerido.", ClientUtils.ERROR);
							}

						} else {
							ClientUtils.alert("Info.", "El tipo de recurso es requerido.", ClientUtils.ERROR);
						}

					} else {
						ClientUtils.alert("Info.", "El contratista es requerido.", ClientUtils.ERROR);
					}

				} else {
					ClientUtils.alert("Info.", "El codigo es requerido.", ClientUtils.ERROR);
				}

			}
		});
		fs.addButton(btnSave);
		cargarRecursos.execute();
	}

	private static void toContactosPanel(FormPanel formPanel, final Panel gridContainerPanel, final ControlWebGUI gui) {
		FieldSet fs = new FieldSet("Datos");
		fs.setLabelWidth(40);

		final TextField txtId = new TextField("ID");
		txtId.setDisabled(true);
		txtId.setValue("");
		fs.add(txtId);
		final TextField txtNombre = new TextField("Nombre");
		fs.add(txtNombre);
		final TextField txtFijo = new TextField("Tel Fijo");
		fs.add(txtFijo);
		final TextField txtFijo2 = new TextField("Tel Fijo2");
		fs.add(txtFijo2);
		final TextField txtCelular = new TextField("Celular");
		fs.add(txtCelular);
		final TextField txtCelular2 = new TextField("Celular2");
		fs.add(txtCelular2);
		final TextField txtEmail = new TextField("Email");
		fs.add(txtEmail);
		final TextField txtParent = new TextField("Parentesco");
		fs.add(txtParent);
		final MyGridPanel grid = new MyGridPanel();
		final Function recargarContactos = new Function() {
			@Override
			public void execute() {
				getStaticQueriesServiceAsync().getContactosProceso(gui.getProceso().getId(), new RPCFillTable(grid,
						gridContainerPanel, null, true, false, false, null, null, new RowLayoutData(), null));
				txtId.setValue("");
				txtNombre.setValue("");
				txtFijo.setValue("");
				txtFijo2.setValue("");
				txtCelular.setValue("");
				txtCelular2.setValue("");
				txtEmail.setValue("");
				txtParent.setValue("");

			}
		};

		grid.addModListener(new MyGridOnCellClickListener() {
			@Override
			public void onCellClick(Record r) {
				txtId.setValue(r.getAsString("Id Contacto "));
				txtNombre.setValue(r.getAsString("Nombre"));
				txtFijo.setValue(r.getAsString("Telefono 1"));
				txtFijo2.setValue(r.getAsString("Telefono 2"));
				txtCelular.setValue(r.getAsString("Celular 1"));
				txtCelular2.setValue(r.getAsString("Celular 2"));
				txtEmail.setValue(r.getAsString("Correo "));
				txtParent.setValue(r.getAsString("Parentesco"));

			}
		});
		Button guardar = new Button("Guardar");
		fs.setButtonAlign(Position.CENTER);
		guardar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (txtId.getValueAsString().equals("")) {
					getStaticAdminQueriesServiceAsync().addContacto(gui.getProceso().getId(),
							txtNombre.getValueAsString(), txtFijo.getValueAsString(), txtFijo2.getValueAsString(),
							txtCelular.getValueAsString(), txtCelular2.getValueAsString(), txtEmail.getValueAsString(),
							txtParent.getValueAsString(),
							new MyBooleanAsyncCallback("Contacto creado", recargarContactos));
				} else {
					getStaticAdminQueriesServiceAsync().updateContacto(txtId.getValueAsString(),
							txtNombre.getValueAsString(), txtFijo.getValueAsString(), txtFijo2.getValueAsString(),
							txtCelular.getValueAsString(), txtCelular2.getValueAsString(), txtEmail.getValueAsString(),
							txtParent.getValueAsString(),
							new MyBooleanAsyncCallback("Contacto actualizado", recargarContactos));
				}
			}
		});
		fs.addButton(guardar);
		formPanel.add(fs);
		recargarContactos.execute();
	}
}
