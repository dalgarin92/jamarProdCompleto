package org.extreme.controlweb.client.admin;

import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;

public abstract class GenericAdminPanelAdapter extends GenericAdminPanel {

	
	
	public GenericAdminPanelAdapter(String title, String details,
			boolean modify, boolean numerate, boolean delete) {
		super(title, details, modify, numerate, delete);
		
	}
	
	public GenericAdminPanelAdapter(String title, String details,
			boolean modify, boolean numerate, boolean delete, String reorder) {
		super(title, details, modify, numerate, delete, reorder);
	}
	
	public Button getButton(){
		return button;
	}
	
	@Override
	protected void addFunction(Record[] recs) {		
	}

	@Override
	protected void deleteFunction(Record r) {		
	}

	@Override
	protected void detailFunction(Record r) {
	}	

	@Override
	protected void modifyFunction(Record r) {
	}
	
	@Override
	protected void reorderFunction(Record r) {
	}

}
