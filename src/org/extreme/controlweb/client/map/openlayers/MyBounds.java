package org.extreme.controlweb.client.map.openlayers;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.util.JSObject;

public class MyBounds extends Bounds {

	public MyBounds(double lowerLeftX, double lowerLeftY, double upperRightX,
			double upperRightY) {
		super(lowerLeftX, lowerLeftY, upperRightX, upperRightY);
	}
	
	private native void extend(JSObject ll, JSObject bounds)/*-{
		bounds.extend(ll);
	}-*/;

	public void extend(LonLat ll){
		extend(ll.getJSObject(), this.getJSObject());
	}
	
	private native boolean containsLonLat(JSObject ll, JSObject bounds, boolean inclusive)/*-{
		return bounds.containsLonLat(ll, inclusive);
	}-*/;
	
	public boolean containsLonLat(LonLat ll){
		return containsLonLat(ll.getJSObject(), getJSObject(), true);
	}
}
