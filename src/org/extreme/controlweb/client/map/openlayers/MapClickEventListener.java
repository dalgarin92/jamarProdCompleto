package org.extreme.controlweb.client.map.openlayers;

import org.extreme.controlweb.client.core.ProcesoLight;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.listeners.NodeTreeClickListener;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.menu.CheckItem;
import com.gwtext.client.widgets.tree.TreeNode;


public class MapClickEventListener implements MapClickListener, AsyncCallback<Boolean> {

	private CheckItem button;
	private OpenLayersMap map;
	private TreeNode selectedNode;
	private ControlWebGUI gui;

	public MapClickEventListener (CheckItem ubicButton, ControlWebGUI gui){
		this.gui = gui;
		button = ubicButton;
		map = gui.getMap();
	}


	@Override
	public void onClick(MapClickEvent event) {
		if (button.isChecked()) {
			try {
				Trabajo trab = null;
				selectedNode = map.getSelectedTrabajoNode();
				if (selectedNode != null
						&& selectedNode.getAttributeAsObject("entity") instanceof Trabajo) {
					trab = (Trabajo)selectedNode.getAttributeAsObject("entity");
				}
				gui.showMessage("Por favor espere...",
						"Actualizando Ubicaci\u00f3n...", Position.CENTER);

				gui.getQueriesServiceProxy().locateJobAddress(
						trab.getDireccion(), trab.getMunicipio().getId(),
						Double.toString(event.getLonLat().lat()),
						Double.toString(event.getLonLat().lon()), trab.getId(),
						trab.getTpoTrab().getId(), this);
			} catch (Exception e) {
				gui.hideMessage();
			} finally {
				button.setChecked(!button.isChecked());
				//gui.getMap().setCursorDefault();
			}
		}

	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
		gui.hideMessage();
	}

	@Override
	public void onSuccess(Boolean result) {
		gui.hideMessage();
		final NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
		final TreeNode parent = (TreeNode) selectedNode.getParentNode();

		try {
			Marker m = (Marker)selectedNode.getAttributeAsObject("marker");
			if(m != null) {
				map.getMkTrab().removeMarker(m);
			}
		} catch (Exception e) {}

		if (parent.getAttribute("tiponodo") != null
				&& (parent.getAttribute("tiponodo").equals("noUbic") || parent
						.getAttribute("tiponodo").equals("noAsig"))) {
			gui.getTree().getEl().mask(
					"Actualizando trabajos no ubicados y no asignados.");
			QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
			serviceProxy.getNoUbicNoAsigTrabs(gui.getProceso().getId(), gui.getDfDateProg().getEl().getValue(),
					new AsyncCallback<ProcesoLight>() {

				@Override
				public void onFailure(Throwable caught) {
					ClientUtils.alert("Error", caught.getMessage(),
							ClientUtils.ERROR);
					gui.getTree().getEl().unmask();
					GWT.log("Error RPC", caught);
				}

				@Override
				public void onSuccess(ProcesoLight result) {
					gui.clearNoUbicNoAsigNodes();
					gui.addTrabTreeNodes(result.getTrabsNoUbic(), gui
							.getNoUbicTreeNode(), false, false, gui);
					gui.addTrabTreeNodes(result.getTrabsNoAsig(), gui
							.getNoAsigTreeNode(), true, false, gui);
					gui.getTree().getEl().unmask();
					parent.expand();
				}

			});
		} else {
			l.updateRecurso(parent);
		}

		/*TreeNode nextSibling = ((TreeNode) parent.getNextSibling());
		if (nextSibling != null
				&& nextSibling.getText().indexOf("Trabajos") == -1) {
			Timer t = new Timer() {
				public void run() {
					l.updateRecurso(parent.getNextSibling());
				}
			};
			t.schedule(ClientUtils.DELAY2);
		}*/


	}
}