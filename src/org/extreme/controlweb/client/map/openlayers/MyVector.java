package org.extreme.controlweb.client.map.openlayers;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.layer.VectorOptions;

public class MyVector extends Vector {

	private ArrayList<VectorFeature> cmps;

	public MyVector(String name, VectorOptions options) {
		super(name, options);
		cmps = new ArrayList<VectorFeature>();
	}

	public MyVector(String name) {
		super(name);
		cmps = new ArrayList<VectorFeature>();
	}

	@Override
	public void addFeature(VectorFeature f) {
		super.addFeature(f);
		cmps.add(f);
	}

	@Override
	public void addFeatures(VectorFeature[] features) {
		for (int i = 0; i < features.length; i++) {
			addFeature(features[i]);
		}
	}


	public void addFeatures(ArrayList<VectorFeature> features) {
		for (int i = 0; i < features.size(); i++) {
			addFeature(features.get(i));
		}
	}

	@Override
	public void removeFeature(VectorFeature feature) {
		super.removeFeature(feature);
		cmps.remove(feature);
	}

	public void removeFeatures(ArrayList<VectorFeature> vf){
		for (VectorFeature vectorFeature : vf) {
			removeFeature(vectorFeature);
		}

	}

	public void removeAllFeatures(){
		while(cmps.size() > 0) {
			removeFeature(cmps.get(0));
		}
	}

	public ArrayList<VectorFeature> getAllFeatures() {
		return cmps;
	}
}
