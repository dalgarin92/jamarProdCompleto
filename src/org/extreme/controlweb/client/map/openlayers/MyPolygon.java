package org.extreme.controlweb.client.map.openlayers;

import org.gwtopenmaps.openlayers.client.geometry.LinearRing;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;
import org.gwtopenmaps.openlayers.client.util.JSObject;

public class MyPolygon extends Polygon {

	protected MyPolygon(JSObject element) {
		super(element);
	}

	public MyPolygon(LinearRing[] rings) {
		super(rings);
	}
	
	private static native JSObject createRegularPolygonImpl(Point p, float radius,
			int sides, float rotation) /*-{
		var point = p.@org.gwtopenmaps.openlayers.client.geometry.Point::getJSObject();
		return $wnd.OpenLayers.Geometry.Polygon.createRegularPolygon(point, radius, sides, rotation);		
	}-*/;
	
	public static Polygon createRegularPolygon(Point p, float radius,
			int sides, float rotation){
		return Polygon.narrowToPolygon(createRegularPolygonImpl(p, radius,
				sides, rotation));
	}
	
}
