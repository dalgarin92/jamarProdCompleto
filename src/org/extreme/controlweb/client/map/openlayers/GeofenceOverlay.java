package org.extreme.controlweb.client.map.openlayers;

import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.LinearRing;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;

public abstract class GeofenceOverlay extends VectorFeature {

	private static final int COMPLEXITY = 1;
	private int radious;
	private VectorFeature circle;
	private MyBounds bounds;

	private boolean isUbic;
	private OpenLayersMap map;

	private static Style getStyles(){
		Style s = new Style();
		s.setExternalGraphic(OpenLayersMap.GEOFENCE);
		s.setGraphicSize(32, 32);
		s.setFillOpacity(1);
		return s;
	}


	private static Style getCircleStyle(){
		Style s = new Style();
		s.setFillOpacity(0.25);
		s.setFillColor("#000000");
		s.setStrokeColor("#000000");
		s.setStrokeWidth(2);
		return s;
	}

	public GeofenceOverlay(double lat, double lon, int radious, OpenLayersMap map2) {
		super(new Point(lon, lat), getStyles());
		this.radious = radious;
		map = map2;

		map.setCompleteDragListener(new CompleteDragFeatureListener() {			
			@Override
			public void onComplete(VectorFeature vf, Pixel p) {
				drawGeofence();
			}
		});

		map.activateDragControl();

	}

	public void setRadious(int radious) {
		this.radious = radious;
	}

	/*public void setCenter(LonLat center) {
		moveGeometry(getGeometry(), center.getJSObject());
	}*/

	/*private native void moveGeometry(JSObject jso, JSObject lonlat)-{
		jso.move(lonlat.lon(), lonlat.lat());
	}-;*/

	public void removeAll() {
		map.getVecAuxDraggable().removeFeature(this);
		if(circle != null) {
			map.getVecRecorrido().removeFeature(circle);
		}
	}

	public MyBounds getBounds() {
		return bounds;
	}

	public void drawGeofence() {  
		doBeforeDraw();
		if (circle != null) {
			map.getVecRecorrido().removeFeature(circle);
		}
		Point[] circlePoints = new Point[360 / COMPLEXITY + 1];
		double d = radious/6378800.0d;	// radians
		double lat1 = Math.PI/180* getCenterLonLat().lat(); // radians
		double lng1 = Math.PI/180* getCenterLonLat().lon(); // radians

		for (int i = 0 ; i <= 360 ; i += COMPLEXITY) {
			double tc = Math.PI / 180 * i;
			double y = Math.asin(Math.sin(lat1) * Math.cos(d) + Math.cos(lat1)
					* Math.sin(d) * Math.cos(tc));
			double dlng = Math.atan2(Math.sin(tc) * Math.sin(d)
					* Math.cos(lat1), Math.cos(d) - Math.sin(lat1)
					* Math.sin(y));
			double x = (lng1 - dlng + Math.PI) % (2 * Math.PI) - Math.PI; // MOD function
			Point point = new Point(x * (180 / Math.PI), y * (180 / Math.PI));
			circlePoints[i] = point;

			//if(i == 0 || i == 90 || i == 180 || i == 270)
			//bounds.extend(point);
		}
		//if (d < 1.5678565720686044) {
		//circle = new Polygon(ClientUtils.getLatLngPoints(circlePoints), "#000000", 2, 1, "#000000", 0.25);
		circle = new VectorFeature(new Polygon(
				new LinearRing[] { new LinearRing(circlePoints) }),
				getCircleStyle());
		/*}
		else {
			//circle = new Polygon(ClientUtils.getLatLngPoints(circlePoints), "#000000", 2, 1, "#FFFFFF", 0);	
		}*/

		map.getVecRecorrido().addFeature(circle);
		bounds = new MyBounds(circlePoints[0].getX(), circlePoints[0]
				.getY(), circlePoints[90 / COMPLEXITY].getX(), 
				circlePoints[90 / COMPLEXITY].getY());
		bounds.extend(new LonLat(circlePoints[180 / COMPLEXITY].getX(),
				circlePoints[180 / COMPLEXITY].getY()));
		bounds.extend(new LonLat(circlePoints[270 / COMPLEXITY].getX(),
				circlePoints[270 / COMPLEXITY].getY()));

		/*map.addOverlay(new GPolyline(new GLatLng[] {
				(GLatLng) circlePoints.get(0),
				(GLatLng) circlePoints.get(90 / COMPLEXITY),
				(GLatLng)circlePoints.get(180 / COMPLEXITY),
				(GLatLng)circlePoints.get(270 / COMPLEXITY),
				(GLatLng) circlePoints.get(0) }));*/
		//map.setCenter(this.getPoint(), map.getBoundsZoomLevel(bounds));
	}

	protected boolean getIsUbic() {
		return isUbic;
	}

	public void setIsUbic(boolean isUbic) {
		this.isUbic = isUbic;
	}

	public abstract void doBeforeDraw();	
}
