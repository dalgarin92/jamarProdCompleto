package org.extreme.controlweb.client.map.openlayers.control;

import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.control.Control;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;

public class DragFeature extends Control {
	
	public interface StartDragFeatureListener{
		public void onStart(VectorFeature vf, Pixel p);
	}
	
	public interface DragFeatureListener{
		public void onDrag(VectorFeature vf, Pixel p);
	}
	
	public interface CompleteDragFeatureListener{
		public void onComplete(VectorFeature vf, Pixel p);
	}
	
	
	public DragFeature(Vector layer) {
		this(DragFeatureImpl.create(layer.getJSObject()));
	}

	public DragFeature(Vector layer, DragFeatureOptions options) {
		this(DragFeatureImpl.create(layer.getJSObject(), options.getJSObject()));
	}

	public DragFeature(JSObject element) {
		super(element);
	}
	
	public void onStart(StartDragFeatureListener listener){
		JSObject callback = DragFeatureImpl.createStartDragFeatureCallback(listener);
		getJSObject().setProperty("onStart", callback);
	}
	
	public void onDrag(DragFeatureListener listener){
		JSObject callback = DragFeatureImpl.createDragFeatureCallback(listener);
		getJSObject().setProperty("onDrag", callback);
	}
	
	public void onComplete(CompleteDragFeatureListener listener){
		JSObject callback = DragFeatureImpl.createCompleteDragFeatureCallback(listener);
		getJSObject().setProperty("onComplete", callback);
	}	
}
