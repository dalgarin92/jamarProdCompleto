package org.extreme.controlweb.client.map.openlayers.control;

import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature.DragFeatureListener;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature.StartDragFeatureListener;
import org.gwtopenmaps.openlayers.client.util.JSObject;

public class DragFeatureImpl {
	public static native JSObject create(JSObject layer)/*-{
		return new $wnd.OpenLayers.Control.DragFeature(layer);
	}-*/;

	public static native JSObject create(JSObject layer, JSObject options)/*-{
		return new $wnd.OpenLayers.Control.DragFeature(layer, options);
	}-*/;

	public static native JSObject createStartDragFeatureCallback(
			StartDragFeatureListener listener)/*-{
		var callback = function(feature, pixel){
			var vectorFeatureObj = @org.gwtopenmaps.openlayers.client.feature.VectorFeature::narrowToVectorFeature(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(feature);
			var pixelObj = @org.gwtopenmaps.openlayers.client.Pixel::narrowToPixel(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(pixel);
			listener.@org.extreme.controlweb.client.map.openlayers.control.DragFeature.StartDragFeatureListener::onStart(Lorg/gwtopenmaps/openlayers/client/feature/VectorFeature;Lorg/gwtopenmaps/openlayers/client/Pixel;)(vectorFeatureObj, pixelObj);
		}
		return callback;
	}-*/;

	public static native JSObject createDragFeatureCallback(
			DragFeatureListener listener)/*-{
		var callback = function(feature, pixel){
			var vectorFeatureObj = @org.gwtopenmaps.openlayers.client.feature.VectorFeature::narrowToVectorFeature(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(feature);
			var pixelObj = @org.gwtopenmaps.openlayers.client.Pixel::narrowToPixel(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(pixel);
			listener.@org.extreme.controlweb.client.map.openlayers.control.DragFeature.DragFeatureListener::onDrag(Lorg/gwtopenmaps/openlayers/client/feature/VectorFeature;Lorg/gwtopenmaps/openlayers/client/Pixel;)(vectorFeatureObj, pixelObj);
		}
		return callback;
	}-*/;

	public static native JSObject createCompleteDragFeatureCallback(
			CompleteDragFeatureListener listener) /*-{
		var callback = function(feature, pixel){
			var vectorFeatureObj = @org.gwtopenmaps.openlayers.client.feature.VectorFeature::narrowToVectorFeature(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(feature);
			var pixelObj = @org.gwtopenmaps.openlayers.client.Pixel::narrowToPixel(Lorg/gwtopenmaps/openlayers/client/util/JSObject;)(pixel);
			listener.@org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener::onComplete(Lorg/gwtopenmaps/openlayers/client/feature/VectorFeature;Lorg/gwtopenmaps/openlayers/client/Pixel;)(vectorFeatureObj, pixelObj);
		}
		return callback;
	}-*/;

}