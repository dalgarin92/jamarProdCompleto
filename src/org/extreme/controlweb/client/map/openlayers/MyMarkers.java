package org.extreme.controlweb.client.map.openlayers;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.layer.Markers;

public class MyMarkers extends Markers {

	private ArrayList<Marker> markers;

	public MyMarkers(String name) {
		super(name);
		markers = new ArrayList<Marker>();
	}

	@Override
	public void addMarker(Marker marker) {
		super.addMarker(marker);
		markers.add(marker);
	}

	public void addMarkers(ArrayList<Marker> markers) {
		for (Marker marker : markers) {
			addMarker(marker);
		}
	}

	public void addMarkersAsObject(ArrayList<Object> markers) {
		for (Object marker : markers) {
			addMarker((Marker)marker);
		}
	}

	public ArrayList<Marker> getMyMarkers() {
		return markers;
	}

	@Override
	public void removeMarker(Marker marker) {
		if(markers.contains(marker)){
			super.removeMarker(marker);
			markers.remove(marker);
		}
	}

	public void removeMarkers(ArrayList<Marker> markers) {
		for (int i = 0; i < markers.size(); i++) {
			removeMarker(markers.get(i));
		}
	}

	public void removeAll(){
		ArrayList<Marker> markers2 = getMyMarkers();
		while(markers2.size() > 0) {
			removeMarker(markers2.get(0));
		}
	}

	@Override
	public void destroy() {
		markers.clear();
		super.destroy();
	}
}