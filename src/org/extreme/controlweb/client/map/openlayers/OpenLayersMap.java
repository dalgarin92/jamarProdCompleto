//package org.extreme.controlweb.client.map.openlayers;
//
//import java.util.ArrayList;
//
//import org.extreme.controlweb.client.core.Recurso;
//import org.extreme.controlweb.client.core.Trabajo;
//import org.extreme.controlweb.client.gui.ControlWebGUI;
//import org.extreme.controlweb.client.map.openlayers.control.DragFeature;
//import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
//import org.gwtopenmaps.openlayers.client.Icon;
//import org.gwtopenmaps.openlayers.client.LonLat;
//import org.gwtopenmaps.openlayers.client.Map;
//import org.gwtopenmaps.openlayers.client.MapOptions;
//import org.gwtopenmaps.openlayers.client.MapWidget;
//import org.gwtopenmaps.openlayers.client.Marker;
//import org.gwtopenmaps.openlayers.client.Pixel;
//import org.gwtopenmaps.openlayers.client.Size;
//import org.gwtopenmaps.openlayers.client.Style;
//import org.gwtopenmaps.openlayers.client.control.LayerSwitcher;
//import org.gwtopenmaps.openlayers.client.control.Navigation;
//import org.gwtopenmaps.openlayers.client.control.NavigationOptions;
//import org.gwtopenmaps.openlayers.client.control.PanZoomBar;
//import org.gwtopenmaps.openlayers.client.control.ZoomBox;
//import org.gwtopenmaps.openlayers.client.event.EventHandler;
//import org.gwtopenmaps.openlayers.client.event.EventObject;
//import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
//import org.gwtopenmaps.openlayers.client.geometry.LineString;
//import org.gwtopenmaps.openlayers.client.geometry.Point;
//import org.gwtopenmaps.openlayers.client.layer.GMapType;
//import org.gwtopenmaps.openlayers.client.layer.Google;
//import org.gwtopenmaps.openlayers.client.layer.GoogleOptions;
//import org.gwtopenmaps.openlayers.client.layer.Layer;
//import org.gwtopenmaps.openlayers.client.layer.VectorOptions;
//import org.gwtopenmaps.openlayers.client.layer.WMS;
//import org.gwtopenmaps.openlayers.client.layer.WMSOptions;
//import org.gwtopenmaps.openlayers.client.layer.WMSParams;
//import org.gwtopenmaps.openlayers.client.popup.FramedCloud;
//import org.gwtopenmaps.openlayers.client.popup.Popup;
//import org.gwtopenmaps.openlayers.client.util.JObjectArray;
//import org.gwtopenmaps.openlayers.client.util.JSObject;
//
//import com.google.gwt.core.client.GWT;
//import com.gwtext.client.core.Function;
//import com.gwtext.client.data.Node;
//import com.gwtext.client.widgets.tree.TreeNode;
//import com.gwtext.client.widgets.tree.TreePanel;
//
//public class OpenLayersMap extends MapWidget {
//
//	private Map map;
//
//	private WMS wmsLayer;
//	private Popup popup;
//
//	private MyMarkers mkVeh;
//	private MyMarkers mkTrab;
//	private MyMarkers mkPtosGlobal;
//	private MyMarkers mkPtosUsuario;
//	private MyMarkers mkPtosRecorrido;
//	private MyMarkers mkAux;
//	private MyVector vecRecorrido;
//	private MyVector vecAuxDraggable;
//
//	private TreePanel panel;
//	private TreeNode selectedNode;
//	private Function fnClearOverlays;
//
//	private ControlWebGUI gui;
//
//	private DragFeature dragControl;
//
//	private static final Size SIZE_CAR = new Size(32, 32);
//	private static final Size SIZE_REC = new Size(16, 16);
//	//private static final Size SIZE_REC2 = new Size(24, 24);
//	private static final Size SIZE_MOTO = new Size(40, 40);
//
//	public static final String CARRITO_ON = "images/utiles/icons/greencar.png";
//	public static final String CARRITO_OFF = "images/utiles/icons/redcar.png";
//	public static final String CARRITO_NOGPS = "images/utiles/icons/yellowcar.png";
//
//	public static final String MOTO_ON = "images/utiles/icons/greenbike.png";
//	public static final String MOTO_OFF = "images/utiles/icons/redbike.png";
//	public static final String MOTO_NOGPS = "images/utiles/icons/yellowbike.png";
//
//	public static final String ATRASO_1 = "images/utiles/icons/blueflag.png";
//	public static final String ATRASO_2 = "images/utiles/icons/orangeflag.png";
//	public static final String ATRASO_3 = "images/utiles/icons/redflag.png";
//	public static final String ATRASO_4 = "images/utiles/icons/greenflag.png";
//	public static final String ATRASO_5 = "images/utiles/icons/greyflag.png";
//
//	public static final String GEOFENCE = "images/utiles/icons/icon6.png";
//	public static final String RECORRIDO = "images/utiles/recorrido.png";
//	public static final String EVENT = "images/utiles/icons/event.png";
//
//	private static final double DEGREES_PER_RADIAN = 180.0d / Math.PI;
//
//	@Deprecated
//	public OpenLayersMap(String width, String height) {
//		super(width, height);
//	}
//
//	private static MapOptions getOptions() {
//		MapOptions mapOptions = new MapOptions();
//		mapOptions.setControls(new JObjectArray(new JSObject[] {}));
//		mapOptions.setNumZoomLevels(19);
//		mapOptions.setProjection("EPSG:4326");
//		return mapOptions;
//	}
//
//	private native void setLang(String lang)/*-{
//		$wnd.OpenLayers.Lang.setCode(lang);
//	}-*/;
//
//	private native String getWMSUrl()/*-{
//		return $wnd.WMS_URL;
//	}-*/;
//
//	public OpenLayersMap(ControlWebGUI gui) {
//		super("100%", "100%", getOptions());
//
//		this.gui = gui;
//
//		setLang("es");
//
//		MyBounds bounds = new MyBounds(-81.7358169555665, -4.23687362670895,
//				-66.8704528808595, 12.5947074890135);
//
//		map = getMap();
//
//		//map.setMaxExtent(bounds);
//
//		GoogleOptions go1 = new GoogleOptions();
//		//go1.setSphericalMercator(true);
//		go1.setType(GMapType.G_SATELLITE_MAP);
//		go1.setSmoothDragPan(true);
//
//		GoogleOptions go2 = new GoogleOptions();
//		go2.setType(GMapType.G_NORMAL_MAP);
//		//go2.setSphericalMercator(true);
//		go2.setSmoothDragPan(true);
//
//		GoogleOptions go3 = new GoogleOptions();
//		go3.setType(GMapType.G_HYBRID_MAP);
//		//go3.setSphericalMercator(true);
//		go3.setSmoothDragPan(true);
//
//		Google g1 = new Google("Google Satelital", go1);
//		Google g2 = new Google("Google Hibrido", go3);
//		Google g3 = new Google("Google Callejero", go2);
//
//		mkVeh = new MyMarkers("Vehiculos");
//		mkTrab = new MyMarkers("Trabajos");
//
//		mkPtosGlobal = new MyMarkers("Ptos. de Int. Globales");
//		//mkPtosGlobal.setIsVisible(false);
//
//		mkPtosUsuario = new MyMarkers("Ptos. de Int. de Usuario");
//		//mkPtosUsuario.setIsVisible(false);
//
//		mkPtosRecorrido = new MyMarkers("Recorrido");
//		mkPtosRecorrido.setDisplayInLayerSwitcher(false);
//
//		mkAux = new MyMarkers("Auxiliar");
//		mkAux.setDisplayInLayerSwitcher(false);
//
//		VectorOptions options = new VectorOptions();
//		options.setDisplayInLayerSwitcher(false);
//		options.setIsBaseLayer(false);
//		options.setProjection("EPSG:4326");
//
//		vecRecorrido = new MyVector("Recorrido", options);
//		vecAuxDraggable = new MyVector("Aux", options);
//
//		dragControl = new DragFeature(vecAuxDraggable);
//		map.addControl(dragControl);
//
//
//		WMSParams wmsParams = new WMSParams();
//		wmsParams.setFormat("image/png");
//		//wmsParams.setLayers("lbs_colombia");
//		//wmsParams.setLayers("basic");
//		wmsParams.setLayers("wms_xtream");
//		wmsParams.setStyles("");
//		// wmsParams.setIsTransparent(true);
//
//		WMSOptions wmsOpts = new WMSOptions();
//		wmsOpts.setIsBaseLayer(true);
//		wmsOpts.setRatio(1);
//		// wmsOpts.setLayerOpacity(0.50);
//
//		wmsLayer = new WMS("Extreme Maps", getWMSUrl(), wmsParams, wmsOpts);
//		//wmsLayer = new WMS("OpenLayers WMS",  "http://labs.metacarta.com/wms/vmap0", wmsParams);
//		map.addLayers(new Layer[] {wmsLayer, g3, g1, g2, vecAuxDraggable, vecRecorrido, 
//				mkPtosRecorrido, mkVeh, mkTrab,	mkPtosGlobal, mkPtosUsuario });
//
//		map.addControl(new PanZoomBar());
//		// map.addControl(new MousePosition());
//		// map.addControl(new Scale());
//		//map.addControl(new NavToolBar());
//		NavigationOptions no = new NavigationOptions();
//		no.setZoomWheelEnabled(true);
//		Navigation nav = new Navigation(no);
//
//		map.addControl(nav);
//		nav.activate();
//
//		map.addControl(new ZoomBox());
//		map.addControl(new LayerSwitcher());
//
//		getElement().getStyle().setProperty("border", "double");
//		getElement().getStyle().setProperty("borderColor", "#AEAEAE");
//
//		map.setCenter(map.getCenter(), map.getZoomForExtent(bounds, false) + 3);
//
//		setDefaultCursor();
//
//		/*map.getEvents().register("movestart", map, new EventHandler() {
//
//			@Override
//			public void onHandle(EventObject eventObject) {
//				setClosedHandCursor();
//				System.out.println("movestart");
//			}
//		});
//
//		map.getEvents().register("moveend", map, new EventHandler() {
//
//			@Override
//			public void onHandle(EventObject eventObject) {
//				setDefaultCursor();
//				System.out.println("moveend");
//			}
//		});*/
//
//	}
//
//	public void setDefaultCursor(){
//		getElement().getStyle().setProperty("cursor", "url(images/utiles/icons/openhand.cur),default");
//	}
//
//	public void setCrosshairCursor(){
//		getElement().getStyle().setProperty("cursor", "crosshair");
//	}
//
//	public void setClosedHandCursor(){
//		getElement().getStyle().setProperty("cursor", "url(images/utiles/icons/closedhand.cur),auto");
//	}
//
//	public void activateDragControl(){
//		dragControl.activate();
//	}
//
//	public void deactivateDragControl(){
//		dragControl.deactivate();
//	}
//
//	public void setCompleteDragListener(CompleteDragFeatureListener dl){
//		dragControl.onComplete(dl);
//	}
//
//	public MyVector getVecAuxDraggable() {
//		return vecAuxDraggable;
//	}
//
//	public void clearOverlays(){
//		getVecRecorrido().removeAllFeatures();
//		getMkPtosRecorrido().removeAll();
//		getMkVeh().removeAll();
//		getMkTrab().removeAll();
//		getMkAux().removeAll();
//
//		if(fnClearOverlays != null) {
//			fnClearOverlays.execute();
//		}
//	}
//
//	public void setTreePanel(TreePanel panel){
//		this.panel = panel;
//	}
//
//	public void removeClearHandler(){
//		fnClearOverlays = null;
//	}
//
//	public void addClearHandler(Function coh){
//		fnClearOverlays = coh;
//	}
//
//	private void searchSelectedNode(TreeNode root) {
//		Node[] childs = root.getChildNodes();
//		for (int i = 0; i < childs.length; i++)	{
//			searchSelectedNode((TreeNode)childs[i]);
//		}
//		if(root.isSelected()) {
//			selectedNode = root;
//		}
//	}
//
//	public TreeNode getSelectedTrabajoNode() {
//		selectedNode = null;
//		searchSelectedNode(panel.getRootNode());
//		if(selectedNode.getAttributeAsObject("entity") instanceof Trabajo) {
//			return selectedNode;
//		} else {
//			return null;
//		}
//	}
//
//	public TreeNode getSelectedRecursoNode() {
//		selectedNode = null;
//		searchSelectedNode(panel.getRootNode());
//		if(selectedNode.getAttributeAsObject("entity") instanceof Recurso) {
//			return selectedNode;
//		} else {
//			return null;
//		}
//	}
//
//	private Marker createMarker(double lat, double lon, String tipo,
//			final String info) {
//
//		String icon = "";
//		Size size = SIZE_CAR;
//
//		if (tipo.equals("CAR_ON")) {
//			icon = CARRITO_ON;
//		} else if (tipo.equals("CAR_OFF")) {
//			icon = CARRITO_OFF;
//		} else if (tipo.equals("CAR_NOGPS")) {
//			icon = CARRITO_NOGPS;
//		} else if (tipo.equals("PATH")) {
//			icon = RECORRIDO;
//		} else if (tipo.equals("MOTO_ON")) {
//			icon = MOTO_ON;
//			size = SIZE_MOTO;
//		} else if (tipo.equals("MOTO_OFF")) {
//			icon = MOTO_OFF;
//			size = SIZE_MOTO;
//		} else if (tipo.equals("MOTO_NOGPS")) {
//			icon = MOTO_NOGPS;
//			size = SIZE_MOTO;
//		} else if (tipo.equals("ATRASO1")) {
//			icon = ATRASO_1;
//		} else if (tipo.equals("ATRASO2")) {
//			icon = ATRASO_2;
//		} else if (tipo.equals("ATRASO3")) {
//			icon = ATRASO_3;
//		} else if (tipo.equals("ATRASO4")) {
//			icon = ATRASO_4;
//		} else if (tipo.equals("ATRASO5")) {
//			icon = ATRASO_5;
//		} else {
//			icon = "images/puntos_int/" + tipo + ".png";
//		}
//
//		Marker marker = new Marker(new LonLat(lon, lat), new Icon(icon, size));
//
//		//mkVeh.addMarker(marker);
//		// map.setCenter(center, 5);
//		if(info != null) {
//			registerPopup(marker, info, false);
//		}
//
//		return marker;
//	}
//
//	public Marker createTrabMarker(double lat, double lon, String tipo,
//			final String info) {
//		Marker m = createMarker(lat, lon, tipo, info);
//		mkTrab.addMarker(m);
//		return m;
//	}
//
//	public Marker createVehicleMarker(double lat, double lon, String tipo,
//			final String info) {
//		Marker m = createMarker(lat, lon, tipo, info);
//		mkVeh.addMarker(m);
//		return m;
//	}
//
//	public Marker createAuxMarker(double lat, double lon, String tipo,
//			final String info) {
//		Marker m = createMarker(lat, lon, tipo, info);
//		mkAux.addMarker(m);
//		return m;
//	}
//
//	public VectorFeature createDraggableMarker(double lat, double lon, String tipo) {
//		VectorFeature m;
//
//		Style s = new Style();
//		s.setExternalGraphic(tipo);
//		s.setGraphicSize(32, 32);
//		s.setFillOpacity(1);
//
//		vecAuxDraggable.addFeature(m = new VectorFeature(new Point(lon, lat), s));
//
//
//		return m;
//	}
//
//	public Marker createPtoUsuarioMarker(double lat, double lon, String tipo,
//			final String info) {
//		Marker m = createMarker(lat, lon, tipo, info);
//		mkPtosUsuario.addMarker(m);
//		return m;
//	}
//
//	public Marker createPtoGlobalMarker(double lat, double lon, String tipo,
//			final String info) {
//		Marker m = createMarker(lat, lon, tipo, info);
//		mkPtosGlobal.addMarker(m);
//		return m;
//	}
//
//	public void setPopup(Marker marker, final String info) {
//		try {
//			if (popup != null) {
//				map.removePopup(popup);
//			}
//		} catch (Exception e) {
//		}
//
//		popup = new FramedCloud("marker-info", marker.getLonLat(), new Size(
//				170, 120), info,
//				new Icon("", new Size(10, 10), new Pixel(0, 0)), true);
//		map.addPopup(popup);
//	}
//
//	private void registerPopup(Marker marker, final String info,
//			boolean isMouseOver) {
//		/*
//		 * map.getEvents().register("changebaselayer", map, new EventHandler() {
//		 * 
//		 * @Override public void onHandle(EventObject eventObject) {
//		 * Window.alert("hola"); } });
//		 */
//		marker.getEvents().register(isMouseOver ? "mouseover" : "click",
//				marker, new EventHandler() {
//
//			@Override
//			public void onHandle(EventObject eventObject) {
//				Marker marker = Marker.narrowToMarker(eventObject
//						.getSourceJSObject());
//				setPopup(marker, info);
//			}
//		});
//
//		if (isMouseOver) {
//			marker.getEvents().register("mouseout", marker, new EventHandler() {
//				@Override
//				public void onHandle(EventObject eventObject) {
//					if (popup != null) {
//						map.removePopup(popup);
//					}
//				}
//			});
//		}
//	}
//
//	public void extend() {
//		ArrayList<ArrayList<Marker>> m = getAllMarkers();
//		ArrayList<Marker> all = new ArrayList<Marker>();
//		all.addAll(m.get(0));
//		all.addAll(m.get(1));
//		extend(all);	
//	}
//
//	public void extend(ArrayList<Marker> v) {
//		try {
//			MyBounds bounds = new MyBounds(v.get(0).getLonLat().lon(), v.get(0)
//					.getLonLat().lat(), v.get(0).getLonLat().lon(), v.get(0)
//					.getLonLat().lat());
//			for (int i = 0; i < v.size(); i++) {
//				// map.addOverlay(v.get(i));
//				if (!bounds.containsLonLat(v.get(i).getLonLat())) {
//					bounds.extend(v.get(i).getLonLat());
//				}
//			}
//			// este codigo centra el mapa en el maximo zoom en el que
//			// es visible el extend (bounds)
//			map.setCenter(bounds.getCenterLonLat(), 
//					map.getZoomForExtent(bounds, true) - 1);
//		} catch (Exception e) {
//			GWT.log(e.getMessage(), null);
//		}
//
//	}
//
//	public ArrayList<VectorFeature> traceRoute(ArrayList<Marker> v) {
//		return traceRoute(v, 0);
//	}
//
//	public ArrayList<VectorFeature> traceRoute(ArrayList<Marker> v, int first) {
//		return traceRoute(v, first, "#00FF00", "#FF0000");
//	}
//
//	public ArrayList<VectorFeature> traceRoute(ArrayList<Marker> v, String color) {
//		return traceRoute(v, 0, color, color);
//	}
//
//	public ArrayList<VectorFeature> traceRoute(ArrayList<Marker> v, int first, String color1, String color2) {
//		ArrayList<VectorFeature> vfs = new ArrayList<VectorFeature>();
//
//		Style s1 = new Style();
//		s1.setStrokeWidth(3);
//		s1.setStrokeColor(color1);
//		s1.setFillColor(color1);
//		s1.setFillOpacity(0.7);
//
//		Style s2 = new Style();
//		s2.setStrokeWidth(3);
//		s2.setStrokeColor(color2);
//		s2.setFillColor(color2);
//		s2.setFillOpacity(0.7);
//
//		for (int i = first; i < v.size() - 1; i++) {
//			Point[] ini = new Point[2];
//			ini[0] = new Point(v.get(i).getLonLat().lon(), 
//					v.get(i).getLonLat().lat());
//			ini[1] = new Point(v.get(i + 1).getLonLat().lon(), 
//					v.get(i + 1).getLonLat().lat());
//
//			VectorFeature vf = new VectorFeature(new LineString(ini),
//					i == first ? s1 : s2);
//			vfs.add(vf);
//			vecRecorrido.addFeature(vf);
//		}
//		return vfs; 
//	}
//
//	public ArrayList<Marker> createTrackMarkers(ArrayList<LonLat> points,
//			final ArrayList<String> infos) {
//		return createTrackMarkers(points, infos, true);
//	}
//
//	public ArrayList<Marker> createTrackMarkers(ArrayList<LonLat> points,
//			final ArrayList<String> infos, boolean isRecorrido) {
//		ArrayList<Marker> marks = new ArrayList<Marker>();
//		for (int i = 0; i < points.size(); i++) {
//			final int ind = i;
//			Icon opt = null;
//			if(isRecorrido){
//				if (i == points.size() - 1) {
//					opt = new Icon(RECORRIDO, SIZE_REC);
//				} else {
//					opt = getMidArrow(points.get(i), points.get(i + 1));
//				}
//			} else {
//				opt = new Icon(EVENT, SIZE_CAR);
//			}
//
//			Marker mark = new Marker(points.get(i), opt);
//			registerPopup(mark, infos.get(ind), true);
//			mkPtosRecorrido.addMarker(mark);
//			marks.add(mark);
//		}
//		return marks;
//	}
//
//	public Icon getMidArrow(LonLat from, LonLat to) {
//		int dir = getBearing(from, to);
//		// == round it to a multiple of 3 and cast out 120s
//		dir = Math.round(dir / 3) * 3;
//		while (dir >= 120) {
//			dir -= 120;
//		}
//		// == use the corresponding triangle marker
//		return new Icon("http://www.google.com/intl/en_ALL/mapfiles/dir_" + dir
//				+ ".png", SIZE_REC);
//	}
//
//	private int getBearing(LonLat from, LonLat to) {
//		// See T. Vincenty, Survey Review, 23, No 176, p 88-93,1975.
//		// Convert to radians.
//		double lat1 = from.lat();
//		double lon1 = from.lon();
//		double lat2 = to.lat();
//		double lon2 = to.lon();
//
//		// Compute the angle.
//		double angle = -Math.atan2(Math.sin(lon1 - lon2) * Math.cos(lat2), Math
//				.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2)
//				* Math.cos(lon1 - lon2));
//		if (angle < 0.0) {
//			angle += Math.PI * 2.0;
//		}
//
//		// And convert result to degrees.
//		angle = angle * DEGREES_PER_RADIAN;
//		// angle = angle.toFixed(1);
//
//		return (int) angle;
//
//	}
//
//	public ArrayList<ArrayList<Marker>> getAllMarkers() {
//		ArrayList<ArrayList<Marker>> res = new ArrayList<ArrayList<Marker>>();
//		ArrayList<Marker> resVeh = new ArrayList<Marker>();
//		ArrayList<Marker> resTrab = new ArrayList<Marker>();
//		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
//		gui.getTreeNodes(Recurso.class.getName(), nodes);
//
//		for (TreeNode treeNode : nodes) {
//			Marker m = (Marker)treeNode.getAttributeAsObject("marker");
//			if(m != null) {
//				resVeh.add(m);
//			}
//			Node[] childs = treeNode.getChildNodes();
//			for (Node node : childs) {
//				m = (Marker)node.getAttributeAsObject("marker");
//				if(m != null) {
//					resTrab.add(m);
//				}
//			}
//		}
//
//		if(gui.getNoAsigTreeNode() != null) {
//			Node[] noAsigNodes = gui.getNoAsigTreeNode().getChildNodes();
//			for (Node node : noAsigNodes){ 
//				Marker m = (Marker)node.getAttributeAsObject("marker");
//				if(m != null) {
//					resTrab.add(m);
//				}
//			}
//		}
//
//		/*for (TreeNode treeNode : nodes) {
//			ArrayList<Marker> markers = new ArrayList<Marker>(); 
//			markers = getAllMarkers(treeNode);
//			if(!treeNode.equals(gui.getNoAsigTreeNode()))
//				traceRoute(markers, treeNode.getAttributeAsObject("marker") != null ? 1 : 0);
//		}*/
//		res.add(resVeh);
//		res.add(resTrab);
//		return res;
//	}
//
//	public void showAll(){
//		clearOverlays();
//
//		ArrayList<ArrayList<Marker>> m = getAllMarkers();
//		getMkVeh().addMarkers(m.get(0));
//		getMkTrab().addMarkers(m.get(1));
//
//		getMkVeh().setIsVisible(true);
//		getMkTrab().setIsVisible(true);
//
//		extend();
//	}
//
//	public MyMarkers getMkPtosGlobal() {
//		return mkPtosGlobal;
//	}
//
//	public MyMarkers getMkPtosRecorrido() {
//		return mkPtosRecorrido;
//	}
//
//	public MyMarkers getMkPtosUsuario() {
//		return mkPtosUsuario;
//	}
//
//	public MyMarkers getMkTrab() {
//		return mkTrab;
//	}
//
//	public MyMarkers getMkVeh() {
//		return mkVeh;
//	}
//
//	public MyMarkers getMkAux() {
//		return mkAux;
//	}
//
//	public MyVector getVecRecorrido() {
//		return vecRecorrido;
//	}
//
//	public void removeMarker(Marker m){
//		mkVeh.removeMarker(m);
//		mkTrab.removeMarker(m);
//	}
//
//	public void removeMarkers(ArrayList<Marker> m){
//		for (Marker marker : m) {
//			removeMarker(marker);
//		}
//	}
//}
//
package org.extreme.controlweb.client.map.openlayers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.MapWidget;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.Size;
import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.control.DrawFeature;
import org.gwtopenmaps.openlayers.client.control.DrawFeature.FeatureAddedListener;
import org.gwtopenmaps.openlayers.client.control.DrawFeatureOptions;
import org.gwtopenmaps.openlayers.client.control.LayerSwitcher;
import org.gwtopenmaps.openlayers.client.control.ModifyFeature.OnModificationEndListener;
import org.gwtopenmaps.openlayers.client.control.ModifyFeature.OnModificationListener;
import org.gwtopenmaps.openlayers.client.control.ModifyFeatureOptions;
import org.gwtopenmaps.openlayers.client.control.Navigation;
import org.gwtopenmaps.openlayers.client.control.NavigationOptions;
import org.gwtopenmaps.openlayers.client.control.PanZoomBar;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.control.SelectFeature;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.ClickFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.SelectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeature.UnselectFeatureListener;
import org.gwtopenmaps.openlayers.client.control.SelectFeatureOptions;
import org.gwtopenmaps.openlayers.client.control.ZoomBox;
import org.gwtopenmaps.openlayers.client.event.EventHandler;
import org.gwtopenmaps.openlayers.client.event.EventObject;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.format.WKT;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.handler.PathHandler;
import org.gwtopenmaps.openlayers.client.handler.PolygonHandler;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3MapType;
import org.gwtopenmaps.openlayers.client.layer.GoogleV3Options;
import org.gwtopenmaps.openlayers.client.layer.Layer;
import org.gwtopenmaps.openlayers.client.layer.VectorOptions;
import org.gwtopenmaps.openlayers.client.layer.WMS;
import org.gwtopenmaps.openlayers.client.layer.WMSOptions;
import org.gwtopenmaps.openlayers.client.layer.WMSParams;
import org.gwtopenmaps.openlayers.client.popup.FramedCloud;
import org.gwtopenmaps.openlayers.client.popup.Popup;
import org.gwtopenmaps.openlayers.client.util.Attributes;
import org.gwtopenmaps.openlayers.client.util.JObjectArray;
import org.gwtopenmaps.openlayers.client.util.JSObject;
import org.gwtopenmaps.openlayers.client.util.JSObjectWrapper;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Node;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class OpenLayersMap extends MapWidget {

	private Map map;

	private Popup popup;

	private MyMarkers mkVeh;
	private MyMarkers mkTrab;
	private MyMarkers mkPtosGlobal;
	private MyMarkers mkPtosUsuario;
	private MyMarkers mkPtosRecorrido;
	private MyMarkers mkAux;
	private MyVector vecRecorrido;
	private MyVector vecAuxDraggable;
	private MyVector vecTrab;
	private MyVector vectorVeh;
	private TreePanel panel;
	private TreeNode selectedNode;
	private Function fnClearOverlays;

	private ControlWebGUI gui;
	private VectorOptions options;
	private ArrayList<VectorFeature> selectedFeatures;
	private SelectFeature sf;
	private DragFeature dragControl;
	private SelectFeatureOptions sfo;
	private boolean active=false;
	private boolean firstTimeGeomarketing;
	private WMS wmsGeomarketingLayer;
	private boolean geomarketingEnabled;

	private static final Size SIZE_CAR = new Size(32, 32);
	private static final Size SIZE_REC = new Size(16, 16);
	//private static final Size SIZE_REC2 = new Size(24, 24);
	private static final Size SIZE_MOTO = new Size(40, 40);

	public static final String CARRITO_ON = "images/utiles/icons/greencar.png";
	public static final String CARRITO_OFF = "images/utiles/icons/redcar.png";
	public static final String CARRITO_NOGPS = "images/utiles/icons/yellowcar.png";

	public static final String MOTO_ON = "images/utiles/icons/greenbike.png";
	public static final String MOTO_OFF = "images/utiles/icons/redbike.png";
	public static final String MOTO_NOGPS = "images/utiles/icons/yellowbike.png";

	public static final String ATRASO_1 = "images/utiles/icons/blueflag.png";
	public static final String ATRASO_2 = "images/utiles/icons/orangeflag.png";
	public static final String ATRASO_3 = "images/utiles/icons/redflag.png";
	public static final String ATRASO_4 = "images/utiles/icons/greenflag.png";
	public static final String ATRASO_5 = "images/utiles/icons/greyflag.png";
	public static final String ATRASO_1I = "images/utiles/icons/blueflag2.png";
	public static final String ATRASO_2I = "images/utiles/icons/orangeflag2.png";
	public static final String ATRASO_3I = "images/utiles/icons/redflag2.png";
	public static final String ATRASO_4I = "images/utiles/icons/greenflag2.png";
	public static final String ATRASO_5I = "images/utiles/icons/greyflag2.png";

	public static final String GEOFENCE = "images/utiles/icons/icon6.png";
	public static final String RECORRIDO = "images/utiles/recorrido.png";
	public static final String EVENT = "images/utiles/icons/event.png";
	public static final String FLECHADIR = "images/utiles/icons/uparrow.png";
	private static final double DEGREES_PER_RADIAN = 180.0d / Math.PI;

	private boolean isActiveDL = false;
	private boolean isActiveMF = false;
	private boolean isActiveDP = false;
	private boolean featureModified = false;
	private DrawFeature drawLineControl;

	private DrawFeature drawPolygonControl;

	public interface GeoFunction {
		public void execute(VectorFeature vf);
	}

	@Deprecated
	public OpenLayersMap(String width, String height) {
		super(width, height);
	}

	private static MapOptions getOptions() {
		MapOptions mapOptions = new MapOptions();
		mapOptions.setControls(new JObjectArray(new JSObject[] {}));
		mapOptions.setNumZoomLevels(23);
		//mapOptions.setDisplayProjection(new Projection("EPSG:900913"));
		return mapOptions;
	}

	private native void unSelectAll(JSObject obj)/*-{
		obj.unselectAll();
	}-*/;

	public void unSelectAll(){
		unSelectAll(sf.getJSObject());
	}

	private native void setLang(String lang)/*-{
		$wnd.OpenLayers.Lang.setCode(lang);
	}-*/;

	private native String getWMSUrl()/*-{
		return $wnd.WMS_URL;
	}-*/;

	public OpenLayersMap(ControlWebGUI gui) {
		super("100%", "100%", getOptions());
		this.gui = gui;
		setLang("es");

		selectedFeatures = new ArrayList<VectorFeature>();

		MyBounds bounds = new MyBounds(-81.7358169555665, -4.23687362670895,
				-66.8704528808595, 12.5947074890135);

		//LonLat ll = Clients;
		//ll.transform("EPSG:4326", "EPSG:900913");

		map = getMap();

		//map.setMaxExtent(bounds);

		GoogleV3Options go1 = new GoogleV3Options();
		//go1.setSphericalMercator(true);
		go1.setType(GoogleV3MapType.G_SATELLITE_MAP);
		go1.setSmoothDragPan(true);
		go1.setProjection("EPSG:900913");

		GoogleV3Options go2 = new GoogleV3Options();
		go2.setType(GoogleV3MapType.G_NORMAL_MAP);
		//go2.setSphericalMercator(true);
		go2.setSmoothDragPan(true);
		go2.setProjection("EPSG:900913");

		GoogleV3Options go3 = new GoogleV3Options();
		go3.setType(GoogleV3MapType.G_HYBRID_MAP);
		go3.setSmoothDragPan(true);
		go3.setProjection("EPSG:900913");

		GoogleV3 g1 = new GoogleV3("Google Satelital", go1);
		GoogleV3 g2 = new GoogleV3("Google Hibrido", go3);
		GoogleV3 g3 = new GoogleV3("Google Callejero", go2);

		mkVeh = new MyMarkers("Vehiculos");
		mkTrab = new MyMarkers("Trabajos");

		mkPtosGlobal = new MyMarkers("Ptos. de Int. Globales");
		//mkPtosGlobal.setIsVisible(false);

		mkPtosUsuario = new MyMarkers("Ptos. de Int. de Usuario");
		//mkPtosUsuario.setIsVisible(false);

		mkPtosRecorrido = new MyMarkers("Recorrido");
		mkPtosRecorrido.setDisplayInLayerSwitcher(false);
		mkAux = new MyMarkers("Auxiliar");
		mkAux.setDisplayInLayerSwitcher(false);

		options = new VectorOptions();
		options.setDisplayInLayerSwitcher(false);
		options.setIsBaseLayer(false);
		options.setProjection("EPSG:900913");

		VectorOptions optionsVeh = new VectorOptions();
		optionsVeh.setDisplayInLayerSwitcher(true);
		optionsVeh.setIsBaseLayer(false);
		optionsVeh.setProjection("EPSG:900913");

		vecRecorrido = new MyVector("Recorrido", options);
		vecAuxDraggable = new MyVector("Aux", options);
		vectorVeh = new MyVector("Vehiculos", optionsVeh);
		dragControl = new DragFeature(vecAuxDraggable);
		map.addControl(dragControl);

		/*Scale s = new Scale();
		map.addControl(s);*/

		ScaleLine sl = new ScaleLine();
		map.addControl(sl);

		WMSParams wmsParams = new WMSParams();
		wmsParams.setFormat("image/png");
		//wmsParams.setLayers("lbs_colombia");
		//wmsParams.setLayers("basic");
		wmsParams.setLayers("wms_xtream");
		wmsParams.setStyles("");
		// wmsParams.setIsTransparent(true);

		WMSOptions wmsOpts = new WMSOptions();
		wmsOpts.setIsBaseLayer(true);
		wmsOpts.setRatio(1);
		// wmsOpts.setLayerOpacity(0.50);

		map.addLayers(new Layer[] {g3, g1, g2, vecAuxDraggable,vecRecorrido, mkPtosRecorrido,
				mkVeh, mkTrab, mkPtosGlobal, mkPtosUsuario });
		sfo = new SelectFeatureOptions();
		sfo.setMultiple();
		sfo.getJSObject().setProperty("clickout", true);
		sfo.setBox(true);

		sfo.onSelect(new SelectFeatureListener() {
			@Override
			public void onFeatureSelected(VectorFeature vf) {
				selectedFeatures.add(vf);

			}
		});

		sfo.onUnSelect(new UnselectFeatureListener() {
			@Override
			public void onFeatureUnselected(VectorFeature vf) {
				if (selectedFeatures.size() > 0) {
					selectedFeatures.clear();

				}
			}
		});

		sfo.clickFeature(new ClickFeatureListener() {

			@Override
			public void onFeatureClicked(VectorFeature vectorFeature) {
				String info = vectorFeature.getJSObject().getPropertyAsString("htmlinfo");
				if(info != null) {
					setPopup(vectorFeature.getCenterLonLat(), info);
				}
			}
		});
		map.addControl(new PanZoomBar());

		NavigationOptions no = new NavigationOptions();
		no.setZoomWheelEnabled(true);
		Navigation nav = new Navigation(no);

		map.addControl(nav);
		nav.activate();

		map.addControl(new ZoomBox());
		map.addControl(new LayerSwitcher());

		getElement().getStyle().setProperty("border", "double");
		getElement().getStyle().setProperty("borderColor", "#AEAEAE");

		recreateVecTrab();
		map.setCenter(map.getCenter(), map.getZoomForExtent(
				bounds.transform(new Projection("EPSG:4326"),
						new Projection("EPSG:900913")), false) + 3);

		setDefaultCursor();
	}



	private native void mergeSLDParam(JSObject wmsLayer, String layer, String urlSLD)/*-{
		wmsLayer.mergeNewParams({sld : urlSLD,
							 layers: layer});
	}-*/;

	public void activateSelection() {
		try {
			mkVeh.removeAll();
			//clearRecorrido();
			map.removeLayer(vecAuxDraggable);
			map.removeLayer(vecRecorrido);

			map.removeLayer(mkPtosRecorrido);
			//map.removeLayer(mkVeh);
			map.removeLayer(mkTrab);
			map.removeLayer(mkPtosGlobal);
			map.removeLayer(mkPtosUsuario);

			// map.removeLayer(vecConfirmados);
		} catch (Exception e) {
		}
		recreateVecTrab();
		map.addLayer(vecTrab);
		map.addLayer(vecAuxDraggable);
		//map.addLayer(vectorVeh);
		getVecTrab().addFeatures( getAllMarkers2());
		map.addControl(sf);
		sf.activate();
		active = true;
	}

	public void deactivateSelection() {
		mkVeh.removeAll();
		active = false;
		unSelectAll();
		selectedFeatures.clear();
		sf.deactivate();
		map.removeControl(sf);
		getVecTrab().removeAllFeatures();
		map.removeLayer(vecTrab);

		map.addLayer(vecAuxDraggable);
		map.addLayer(vecRecorrido);

		map.addLayer(mkPtosRecorrido);
		map.removeLayer(mkVeh);
		map.addLayer(mkVeh);
		map.addLayer(mkTrab);
		map.addLayer(mkPtosGlobal);
		map.addLayer(mkPtosUsuario);
		// showTrabs();
	}


	public void activateDrawPolygonControl(final GeoFunction onFeatureAdded){
		PolygonHandler ph = new PolygonHandler();
		ph.getJSObject().setProperty("stopDown", false);
		ph.getJSObject().setProperty("stopUp", false);

		DrawFeatureOptions dfo = new DrawFeatureOptions();
		dfo.onFeatureAdded(new FeatureAddedListener() {
			@Override
			public void onFeatureAdded(VectorFeature vectorFeature) {
				onFeatureAdded.execute(vectorFeature);
			}
		});

		drawPolygonControl = new DrawFeature(vecRecorrido, ph, dfo);
		map.addControl(drawPolygonControl);
		drawPolygonControl.activate();
		setCrosshairCursor();
		isActiveDP = true;
	}

	public void deactivateDrawPolygonControl(){
		drawPolygonControl.deactivate();
		map.removeControl(drawPolygonControl);
		setDefaultCursor();
		isActiveDP = false;
	}

	public boolean isActiveDrawPolygonControl(){
		return isActiveDP;
	}

	public void activateDrawLineControl(final GeoFunction onFeatureAdded){
		PathHandler ph = new PathHandler();
		ph.getJSObject().setProperty("stopDown", false);
		ph.getJSObject().setProperty("stopUp", false);

		DrawFeatureOptions dfo = new DrawFeatureOptions();
		dfo.onFeatureAdded(new FeatureAddedListener() {
			@Override
			public void onFeatureAdded(VectorFeature vectorFeature) {
				onFeatureAdded.execute(vectorFeature);
			}
		});

		drawLineControl = new DrawFeature(vecRecorrido, ph, dfo);
		map.addControl(drawLineControl);
		drawLineControl.activate();
		setCrosshairCursor();
		isActiveDL = true;
	}

	public void deactivateDrawLineControl(){
		drawLineControl.deactivate();
		map.removeControl(drawLineControl);
		setDefaultCursor();
		isActiveDL = false;
	}

	public boolean isActiveDrawLineControl(){
		return isActiveDL;
	}

	public void activateModifyFeatureControl(VectorFeature vf, final GeoFunction onModificationEnd){
		activateModifyFeatureControl(vf, null, onModificationEnd);
	}

	public void activateModifyFeatureControl(VectorFeature vf, Integer mode, final GeoFunction onModificationEnd){
		ModifyFeatureOptions mfo = new ModifyFeatureOptions();
		mfo.onModificationEnd(new OnModificationEndListener() {
			@Override
			public void onModificationEnd(VectorFeature vectorFeature) {
				if(featureModified) {
					onModificationEnd.execute(vectorFeature);
				} else {
					deactivateModifyFeatureControl();
				}
			}
		});

		mfo.onModification(new OnModificationListener() {
			@Override
			public void onModification(VectorFeature vectorFeature) {
				featureModified = true;
			}
		});

		if(mode != null) {
			mfo.setMode(mode.intValue());
		}

		featureModified = false;
		/*modifyFeatureControl = new MyModifyFeature(vecRecorrido, mfo);
		map.addControl(modifyFeatureControl);
		modifyFeatureControl.activate();

		if(vf != null) {
			modifyFeatureControl.selectFeature(vf);
		}*/

		setCrosshairCursor();
		isActiveMF = true;
	}

	public void deactivateModifyFeatureControl(){
		/*modifyFeatureControl.deactivate();
		map.removeControl(modifyFeatureControl);*/
		setDefaultCursor();
		isActiveMF = false;
	}

	public boolean isActiveModifyFeatureControl(){
		return isActiveMF;
	}

	public void setDefaultCursor(){
		getElement().getStyle().setProperty("cursor", "url(images/utiles/icons/openhand.cur),default");
	}

	public void setCrosshairCursor(){
		getElement().getStyle().setProperty("cursor", "crosshair");
	}

	public void setClosedHandCursor(){
		getElement().getStyle().setProperty("cursor", "url(images/utiles/icons/closedhand.cur),auto");
	}

	public void activateDragControl(){
		dragControl.activate();
	}

	public void deactivateDragControl(){
		dragControl.deactivate();
	}

	public void setCompleteDragListener(CompleteDragFeatureListener dl){
		dragControl.onComplete(dl);
	}

	public MyVector getVecAuxDraggable() {
		return vecAuxDraggable;
	}
	public MyVector getVecTrab() {
		return vecTrab;
	}

	public void clearPoligonos(){
		//ecaLayerPolAfect.

	}

	public void clearOverlays(){
		getVecRecorrido().removeAllFeatures();
		getMkPtosRecorrido().removeAll();
		getMkVeh().removeAll();
		getMkTrab().removeAll();
		getMkAux().removeAll();

		if(fnClearOverlays != null) {
			fnClearOverlays.execute();
		}
	}
	public void clearOverlaysGps(){
		getVecRecorrido().removeAllFeatures();
		getMkPtosRecorrido().removeAll();
		getMkVeh().removeAll();
		getMkAux().removeAll();

		if(fnClearOverlays != null) {
			fnClearOverlays.execute();
		}
	}
	public void setTreePanel(TreePanel panel){
		this.panel = panel;
	}

	public void removeClearHandler(){
		fnClearOverlays = null;
	}

	public void addClearHandler(Function coh){
		fnClearOverlays = coh;
	}

	private void searchSelectedNode(TreeNode root) {
		Node[] childs = root.getChildNodes();
		for (int i = 0; i < childs.length; i++)	{
			searchSelectedNode((TreeNode)childs[i]);
		}
		if(root.isSelected()) {
			selectedNode = root;
		}
	}

	public TreeNode getSelectedTrabajoNode() {
		selectedNode = null;
		searchSelectedNode(panel.getRootNode());
		if(selectedNode.getAttributeAsObject("entity") instanceof Trabajo) {
			return selectedNode;
		} else {
			return null;
		}
	}

	public TreeNode getSelectedRecursoNode() {
		selectedNode = null;
		searchSelectedNode(panel.getRootNode());
		if(selectedNode.getAttributeAsObject("entity") instanceof Recurso) {
			return selectedNode;
		} else {
			return null;
		}
	}

	public TreeNode searchAnySelectedNode(TreeNode root) {
		searchSelectedNode(root);
		return selectedNode;
	}

	private String getIcon(String tipo){
		String icon = "";
		if (tipo.equals("CAR_ON")) {
			icon = CARRITO_ON;
		} else if (tipo.equals("CAR_OFF")) {
			icon = CARRITO_OFF;
		} else if (tipo.equals("CAR_NOGPS")) {
			icon = CARRITO_NOGPS;
		} else if (tipo.equals("PATH")) {
			icon = RECORRIDO;
		} else if (tipo.equals("MOTO_ON")) {
			icon = MOTO_ON;
		} else if (tipo.equals("MOTO_OFF")) {
			icon = MOTO_OFF;
		} else if (tipo.equals("MOTO_NOGPS")) {
			icon = MOTO_NOGPS;
		} else if (tipo.equals("ATRASO1")) {
			icon = ATRASO_1;
		} else if (tipo.equals("ATRASO1INICIO")) {
			icon = ATRASO_1I;
		}else if (tipo.equals("ATRASO2")) {
			icon = ATRASO_2;
		}else if (tipo.equals("ATRASO2INICIO")) {
			icon = ATRASO_2I;
		} else if (tipo.equals("ATRASO3")) {
			icon = ATRASO_3;
		}else if (tipo.equals("ATRASO3INICIO")) {
			icon = ATRASO_3I;
		} else if (tipo.equals("ATRASO4")) {
			icon = ATRASO_4;
		} else if (tipo.equals("ATRASO4INICIO")) {
			icon = ATRASO_4I;
		}else if (tipo.equals("ATRASO5")) {
			icon = ATRASO_5;
		}else if (tipo.equals("ATRASO5INICIO")) {
			icon = ATRASO_5I;
		} else {
			icon = "images/puntos_int/" + tipo + ".png";
		}
		return icon;
	}

	private Marker createMarker(double lat, double lon, String tipo,
			final String info) {

		String icon = getIcon(tipo);
		Size size = tipo.contains("MOTO") ? SIZE_MOTO : SIZE_CAR;

		LonLat ll = new LonLat(lon, lat);
		ll.transform("EPSG:4326", "EPSG:900913");

		Marker marker = new Marker(ll, new Icon(icon, size));

		if(info != null) {
			registerPopup(marker, info, false);
		}

		return marker;
	}

	public Marker createTrabMarker(double lat, double lon, String tipo,
			final String info) {
		Marker m = createMarker(lat, lon, tipo, info);
		mkTrab.addMarker(m);
		return m;
	}
	
	public JSObjectWrapper[] createTrabMarker(double lat, double lon, String tipo,
			final String info, String key1, String key2) {
		Marker m = createMarker(lat, lon, tipo, info);

		mkTrab.addMarker(m);

		String icono = getIcon(tipo);
			
		LonLat ll = new LonLat(lon, lat);
		ll.transform("EPSG:4326", "EPSG:900913");
		
		VectorFeature m2 = new VectorFeature(new Point(ll.lon(), ll.lat()));
		
		m2.getJSObject().setProperty("htmlinfo", info);
		m2.getJSObject().setProperty("key1", key1);
		m2.getJSObject().setProperty("key2", key2);

		Attributes attr = new Attributes();
		attr.setAttribute("myicon", icono);
		m2.setAttributes(attr);
		vecTrab.addFeature(m2);
		return new JSObjectWrapper[]{m, m2};
	}
	public JSObjectWrapper[] createTrabMarkerOld(double lat, double lon, String tipo,
			final String info, String key1, String key2) {
		Marker m = createMarker(lat, lon, tipo, info);

		mkTrab.addMarker(m);



		String icono = getIcon(tipo);
		VectorFeature m2 = new VectorFeature(new Point(lon, lat));
		m2.getJSObject().setProperty("htmlinfo", info);
		m2.getJSObject().setProperty("key1", key1);
		m2.getJSObject().setProperty("key2", key2);

		Attributes attr = new Attributes();
		attr.setAttribute("myicon", icono);
		m2.setAttributes(attr);
		vecTrab.addFeature(m2);
		return new JSObjectWrapper[]{m, m2};
	}

	public Marker createVehicleMarker(double lat, double lon, String tipo,
			final String info) {
		Marker m = createMarker(lat, lon, tipo, info);

		mkVeh.addMarker(m);

		return m;
	}


	public VectorFeature createVehicleMarker(double lat, double lon, String tipo,
			final String info, String placa, String numinterno) {
		VectorFeature m;
		int size = 32;
		String color = "";
		if (tipo.equals("CAR_ON")) {
			tipo = CARRITO_ON;
			color = "green";
		} else if (tipo.equals("CAR_OFF")) {
			tipo = CARRITO_OFF;
			color = "red";
		} else if (tipo.equals("CAR_NOGPS")) {
			tipo = CARRITO_NOGPS;
			color = "yellow";
		} else if (tipo.equals("MOTO_ON")) {
			tipo = MOTO_ON;
			size = 40;
			color = "green";
		} else if (tipo.equals("MOTO_OFF")) {
			tipo = MOTO_OFF;
			size = 40;
			color = "red";
		} else if (tipo.equals("MOTO_NOGPS")) {
			tipo = MOTO_NOGPS;
			size = 40;
			color = "yellow";
		}

		Style s = new Style();
		s.setExternalGraphic(tipo);
		s.setGraphicSize(size, size);
		s.setFillOpacity(1);

		if(numinterno == null || numinterno.equals("")) {
			s.setLabel(placa);
		} else {
			s.setLabel(numinterno);
		}

		s.setLabelAlign("ct");
		s.setFontColor(color);
		s.setFontSize("14px");
		s.setFontFamily("Courier New, monospace");
		s.setFontWeight("bold");
		s.getJSObject().setProperty("labelYOffset", "-15");

		LonLat ll = new LonLat(lon, lat);
		ll.transform("EPSG:4326", "EPSG:900913");

		m = new VectorFeature(new Point(ll.lon(), ll.lat()), s);
		m.getJSObject().setProperty("htmlinfo", info);


		/*m.getAttributes().setAttribute("placa", placa);
		m.getAttributes().setAttribute("numinterno", numinterno);
		m.getAttributes().setAttribute("favColor", color);*/
		vectorVeh.addFeature(m);
		return m;
	}

	public Marker createAuxMarker(double lat, double lon, String tipo,
			final String info) {
		Marker m = createMarker(lat, lon, tipo, info);
		mkAux.addMarker(m);
		return m;
	}

	public VectorFeature createDraggableMarker(double lat, double lon, String tipo) {
		VectorFeature m;

		Style s = new Style();
		s.setExternalGraphic(tipo);
		s.setGraphicSize(32, 32);
		s.setFillOpacity(1);

				
		vecAuxDraggable.addFeature(m = new VectorFeature(new Point(lon, lat), s));

		return m;
	}

	public Marker createPtoUsuarioMarker(double lat, double lon, String tipo,
			final String info) {
		Marker m = createMarker(lat, lon, tipo, info);
		mkPtosUsuario.addMarker(m);
		return m;
	}

	public Marker createPtoGlobalMarker(double lat, double lon, String tipo,
			final String info) {
		Marker m = createMarker(lat, lon, tipo, info);
		mkPtosGlobal.addMarker(m);
		return m;
	}

	public LonLat midpoint(LonLat from, LonLat to) {

		Double lat2 = from.lat() * (Math.PI / 180);
		Double lat1 = to.lat() * (Math.PI / 180);
		Double lon1 = to.lon() * (Math.PI / 180);
		Double dLon = (from.lon() - to.lon()) * (Math.PI / 180);
		Double Bx = Math.cos(lat2) * Math.cos(dLon);
		Double By = Math.cos(lat2) * Math.sin(dLon);
		Double lat3 = Math.atan2(
				Math.sin(lat1) + Math.sin(lat2),
				Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By
						* By));
		Double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
		Double lat4 = lat3 * (180 / Math.PI);
		Double lon4 = lon3 * (180 / Math.PI);

		LonLat ll = new LonLat(lon4, lat4);
		ll.transform("EPSG:4326", "EPSG:900913");
		return ll;
	}

	public void setPopup(LonLat lonlat, final String info) {
		try {
			if (popup != null) {
				map.removePopup(popup);
			}
		} catch (Exception e) {
		}

		popup = new FramedCloud("marker-info", lonlat, new Size(
				170, 120), info,
				new Icon("", new Size(10, 10), new Pixel(0, 0)), true);
		map.addPopup(popup);
	}

	private void registerPopup(Marker marker, final String info,
			boolean isMouseOver) {
		/*
		 * map.getEvents().register("changebaselayer", map, new EventHandler() {
		 * 
		 * @Override public void onHandle(EventObject eventObject) {
		 * Window.alert("hola"); } });
		 */
		marker.getEvents().register(isMouseOver ? "mouseover" : "click",
				marker, new EventHandler() {

			@Override
			public void onHandle(EventObject eventObject) {
				Marker marker = Marker.narrowToMarker(eventObject
						.getSourceJSObject());
				setPopup(marker.getLonLat(), info);
			}
		});

		if (isMouseOver) {
			marker.getEvents().register("mouseout", marker, new EventHandler() {
				@Override
				public void onHandle(EventObject eventObject) {
					if (popup != null) {
						map.removePopup(popup);
					}
				}
			});
		}
	}


	public void extend() {
		ArrayList<ArrayList<Object>> m = getAllMarkers();
		ArrayList<Object> all = new ArrayList<Object>();
		all.addAll(m.get(0));
		all.addAll(m.get(1));
		extend(all);
	}

	public void extend(ArrayList<Object> v) {
		try {
			LonLat l;
			if (v.get(0) instanceof Marker) {
				l = ((Marker) v.get(0)).getLonLat();
			} else {
				l = ((VectorFeature) v.get(0)).getCenterLonLat();
			}
			MyBounds bounds = new MyBounds(l.lon(), l.lat(), l.lon(), l.lat());
			for (int i = 0; i < v.size(); i++) {
				// map.addOverlay(v.get(i));
				if (v.get(i) instanceof Marker) {
					l = ((Marker) v.get(i)).getLonLat();
				} else {
					l = ((VectorFeature) v.get(i)).getCenterLonLat();
				}
				if (!bounds.containsLonLat(l)) {
					bounds.extend(l);
				}
			}
			// este codigo centra el mapa en el maximo zoom en el que
			// es visible el extend (bounds)
			map.setCenter(bounds.getCenterLonLat(),
					map.getZoomForExtent(bounds, true) - 1);
		} catch (Exception e) {
			GWT.log(e.getMessage(), null);
		}

	}

	public void extendM(ArrayList<Marker> v) {
		try {
			MyBounds bounds = new MyBounds(v.get(0).getLonLat().lon(), v.get(0)
					.getLonLat().lat(), v.get(0).getLonLat().lon(), v.get(0)
					.getLonLat().lat());
			for (int i = 0; i < v.size(); i++) {
				// map.addOverlay(v.get(i));
				if (!bounds.containsLonLat(v.get(i).getLonLat())) {
					bounds.extend(v.get(i).getLonLat());
				}
			}
			// este codigo centra el mapa en el maximo zoom en el que
			// es visible el extend (bounds)
			map.setCenter(bounds.getCenterLonLat(),
					map.getZoomForExtent(bounds, true) - 1);
		} catch (Exception e) {
			GWT.log(e.getMessage(), null);
		}

	}

	public ArrayList<VectorFeature> traceRoute(ArrayList<Marker> v, int first,
			String color1, String color2, boolean show) {
		ArrayList<VectorFeature> vfs = new ArrayList<VectorFeature>();

		Style s1 = new Style();
		s1.setStrokeWidth(3);
		s1.setStrokeColor(color1);
		s1.setFillColor(color1);
		s1.setFillOpacity(0.7);

		Style s2 = new Style();
		s2.setStrokeWidth(3);
		s2.setStrokeColor(color2);
		s2.setFillColor(color2);
		s2.setFillOpacity(0.7);

		for (int i = first; i < v.size() - 1; i++) {
			Point[] ini = new Point[2];
			ini[0] = new Point(v.get(i).getLonLat().lon(),
					v.get(i).getLonLat().lat());
			ini[1] = new Point(v.get(i + 1).getLonLat().lon(),
					v.get(i + 1).getLonLat().lat());

			VectorFeature vf = new VectorFeature(new LineString(ini),
					i == first ? s1 : s2);
			vfs.add(vf);

			if (show) {
				vecRecorrido.addFeature(vf);
			}
		}
		return vfs;
	}

	public ArrayList<VectorFeature> traceRoute(ArrayList<Object> v) {
		return traceRoute(v, 0);
	}

	public ArrayList<VectorFeature> traceRoute(ArrayList<Object> v, int first) {
		return traceRoute(v, first, "#FF0000", "#FF0000"/*"#1212E7"*/);
	}

	public ArrayList<VectorFeature> traceRoute(ArrayList<Object> v, String color) {
		return traceRoute(v, 0, color, color);
	}

	public ArrayList<VectorFeature> traceRoute(ArrayList<Object> v, int first,
			String color1, String color2) {
		ArrayList<VectorFeature> vfs = new ArrayList<VectorFeature>();

		Style s1 = new Style();
		s1.setStrokeWidth(3);
		s1.setStrokeColor(color1);
		s1.setFillColor(color1);
		s1.setFillOpacity(0.7);

		Style s2 = new Style();
		s2.setStrokeWidth(3);
		s2.setStrokeColor(color2);
		s2.setFillColor(color2);
		s2.setFillOpacity(0.7);

		for (int i = first; i < v.size() - 1; i++) {
			Point[] ini = new Point[2];
			/*
			 * ini[0] = new Point(((VectorFeature)v.get(i)).getCenterLonLat(),
			 * ((VectorFeature)v.get(i)) .getLonLat().lat()); ini[1] = new
			 * Point(((VectorFeature)v.get(i + 1)).getCenterLonLat(),
			 * ((VectorFeature)v.get(i + 1)) .getLonLat().lat()); VectorFeature
			 * vf = new VectorFeature(new LineString(ini), i == first ? s1 :
			 * s2); vfs.add(vf);
			 */
			LonLat ll1 = null;
			if (v.get(i) instanceof Marker) {
				ll1 = ((Marker) v.get(i)).getLonLat();
			} else if (v.get(i) instanceof VectorFeature) {
				ll1 = ((VectorFeature) v.get(i)).getCenterLonLat();
			}

			LonLat ll2 = null;
			if (v.get(i + 1) instanceof Marker) {
				ll2 = ((Marker) v.get(i + 1)).getLonLat();
			} else if (v.get(i + 1) instanceof VectorFeature) {
				ll2 = ((VectorFeature) v.get(i + 1)).getCenterLonLat();
			}

			ini[0] = new Point(ll1.lon(), ll1.lat());
			ini[1] = new Point(ll2.lon(), ll2.lat());

			VectorFeature vf = new VectorFeature(new LineString(ini),
					i == first ? s1 : s2);
			vfs.add(vf);
			vecRecorrido.addFeature(vf);

		}
		return vfs;
	}
	public ArrayList<Object> createTrackMarkers(ArrayList<LonLat> points,
			final ArrayList<String> infos) {
		return createTrackMarkers(points, infos, true);
	}

	public ArrayList<Object> createTrackMarkers(ArrayList<LonLat> points,
			final ArrayList<String> infos, boolean isRecorrido) {
		ArrayList<Object> marks = new ArrayList<Object>();
		for (int i = 0; i < points.size() - 1; i++) {

			Style opt = null;
			if (isRecorrido) {
				if (i == points.size() - 1) {
					opt = getIcon(points.get(i), points.get(i + 1), RECORRIDO);
				} else {
					opt = getIcon(points.get(i), points.get(i + 1), FLECHADIR);
				}

			}
			VectorFeature mark = null;
			LonLat mid = midpoint(points.get(i), points.get(i + 1));

			mark = new VectorFeature(new Point(mid.lon(), mid.lat()), opt);

			// registerPopup(mark, infos.get(ind), true);
			vecRecorrido.addFeature(mark);
			marks.add(mark);

			/*
			 * final int ind = i; Icon opt = null; if(isRecorrido){ if (i ==
			 * points.size() - 1) opt = new Icon(RECORRIDO, SIZE_REC); else opt
			 * = getMidArrow(points.get(i), points.get(i + 1)); } else opt = new
			 * Icon(EVENT, SIZE_CAR);
			 * 
			 * Marker mark = new Marker(points.get(i), opt); registerPopup(mark,
			 * infos.get(ind), true); mkPtosRecorrido.addMarker(mark);
			 * marks.add(mark);
			 */

		}
		return marks;
	}

	public Icon getMidArrow(LonLat from, LonLat to) {
		int dir = getBearing(from, to);
		// == round it to a multiple of 3 and cast out 120s
		dir = Math.round(dir / 3) * 3;
		while (dir >= 120) {
			dir -= 120;
		}
		// == use the corresponding triangle marker
		return new Icon("http://www.google.com/intl/en_ALL/mapfiles/dir_" + dir
				+ ".png", SIZE_REC);
	}

	public Style getIcon(LonLat from, LonLat to, String imagen) {
		Style s = new Style();
		s.setFillOpacity(1);
		s.setExternalGraphic(imagen);
		if (imagen.contains("uparrow")) {
			s.setGraphicSize(24, 24);
			s.setRotation(String.valueOf(getBearing(from, to)));
		} else {
			s.setGraphicSize(32, 32);
		}

		return s;

	}

	private int getBearing(LonLat from, LonLat to) {
		// See T. Vincenty, Survey Review, 23, No 176, p 88-93,1975.
		// Convert to radians.
		double lat1 = from.lat();
		double lon1 = from.lon();
		double lat2 = to.lat();
		double lon2 = to.lon();

		// Compute the angle.
		double angle = -Math.atan2(Math.sin(lon1 - lon2) * Math.cos(lat2), Math
				.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2)
				* Math.cos(lon1 - lon2));
		if (angle < 0.0) {
			angle += Math.PI * 2.0;
		}

		// And convert result to degrees.
		angle = angle * DEGREES_PER_RADIAN;
		// angle = angle.toFixed(1);

		return (int) angle;

	}

	public ArrayList<ArrayList<Object>> getAllMarkers() {
		ArrayList<ArrayList<Object>> res = new ArrayList<ArrayList<Object>>();
		ArrayList<Object> resVeh = new ArrayList<Object>();
		ArrayList<Object> resTrab = new ArrayList<Object>();
		ArrayList<Object> resTrab2 = new ArrayList<Object>();
		// ArrayList<JSObjectWrapper> resTrabConf= new
		// ArrayList<JSObjectWrapper>();
		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		gui.getTreeNodes(Recurso.class.getName(), nodes);

		for (TreeNode treeNode : nodes) {
			Object m = treeNode.getAttributeAsObject("marker");
			if (m != null) {
				resVeh.add(m);
			}
			Node[] childs = treeNode.getChildNodes();
			for (Node node : childs) {
				m = node.getAttributeAsObject("marker");
				VectorFeature m2 = (VectorFeature) node
						.getAttributeAsObject("marker2");

				if (m != null) {
					resTrab.add(m);
				}

				if (m2 != null) {
					resTrab2.add(m2);
				}

				Node[] childs2 = node.getChildNodes();
				for (Node node2 : childs2) {
					m = node2.getAttributeAsObject("marker");
					VectorFeature m23 = (VectorFeature) node2
							.getAttributeAsObject("marker2");

					if (m != null) {
						resTrab.add(m);
					}

					if (m23 != null) {
						resTrab2.add(m23);
					}

				}
			}
		}

		if (gui.getNoAsigTreeNode() != null) {
			Node[] noAsigNodes = gui.getNoAsigTreeNode().getChildNodes();
			for (Node node : noAsigNodes) {

				Marker m = (Marker) node.getAttributeAsObject("marker");
				VectorFeature m23 = (VectorFeature) node
						.getAttributeAsObject("marker2");

				if (m != null) {
					resTrab.add(m);
				}

				if (m23 != null) {
					resTrab2.add(m23);
				}

			}
		}

		res.add(resVeh);
		res.add(resTrab);
		res.add(resTrab2);
		return res;
	}
	
	
	public ArrayList<VectorFeature>  getAllMarkers2() {
		ArrayList<VectorFeature> resTrab2 = new ArrayList<VectorFeature>();


		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		gui.getTreeNodes(Recurso.class.getName(), nodes);

		for (TreeNode treeNode : nodes) {
		Marker m = (Marker)treeNode.getAttributeAsObject("marker");
		VectorFeature m2 = (VectorFeature)treeNode.getAttributeAsObject("marker2");
		if(m != null) {
		if(m2 != null) {
		resTrab2.add(m2);
		}
		}
		Node[] childs = treeNode.getChildNodes();
		for (Node node : childs) {
		m = (Marker)node.getAttributeAsObject("marker");
		m2 = (VectorFeature)node.getAttributeAsObject("marker2");
		if(m != null) {
		if(m2 != null) {
		resTrab2.add(m2);
		}
		}
		}
		}

		if(gui.getNoAsigTreeNode() != null) {
		Node[] noAsigNodes = gui.getNoAsigTreeNode().getChildNodes();
		for (Node node : noAsigNodes){
		Marker m = (Marker)node.getAttributeAsObject("marker");
		VectorFeature m2 = (VectorFeature)node.getAttributeAsObject("marker2");
		if(m != null) {
		if(m2 != null) {
		resTrab2.add(m2);
		}
		}
		}
		}


		/*if(gui.getPendTreeNode() != null) {
		Node[] pendientesNode = gui.getPendTreeNode().getChildNodes();
		for (Node node : pendientesNode){
		Marker m = (Marker)node.getAttributeAsObject("marker");
		VectorFeature m2 = (VectorFeature)node.getAttributeAsObject("marker2");
		if(m != null) {
		if(m2 != null) {
		resTrab2.add(m2);
		}
		}
		}
		}*/


		/*
		 * for (TreeNode treeNode : nodes) { ArrayList<Marker> markers = new
		 * ArrayList<Marker>(); markers = getAllMarkers(treeNode);
		 * if(!treeNode.equals(gui.getNoAsigTreeNode())) traceRoute(markers,
		 * treeNode.getAttributeAsObject("marker") != null ? 1 : 0); }
		 */

		return resTrab2;
		}


	public ArrayList<VectorFeature>  getAllMarkers3() {
		ArrayList<VectorFeature> resTrab2 = new ArrayList<VectorFeature>();


		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		gui.getTreeNodes(Recurso.class.getName(), nodes);

		/*for (TreeNode treeNode : nodes) {
			Marker m = (Marker)treeNode.getAttributeAsObject("marker");
			VectorFeature m2 = (VectorFeature)treeNode.getAttributeAsObject("marker2");
			if(m != null) {
				if(m2 != null) {
					resTrab2.add(m2);
				}
			}
			Node[] childs = treeNode.getChildNodes();
			for (Node node : childs) {
				m = (Marker)node.getAttributeAsObject("marker");
				m2 = (VectorFeature)node.getAttributeAsObject("marker2");
				if(m != null) {
					if(m2 != null) {
						resTrab2.add(m2);
					}
				}
			}
		}*/

		if(gui.getNoAsigTreeNode() != null) {
			Node[] noAsigNodes = gui.getNoAsigTreeNode().getChildNodes();
			for (Node node : noAsigNodes){
				Node[] ns = ((TreeNode)node).getChildNodes();
				for (Node n : ns) {
					Marker m = (Marker)n.getAttributeAsObject("marker");
					VectorFeature m2 = (VectorFeature)n.getAttributeAsObject("marker2");
					if(m != null) {
						if(m2 != null) {
							resTrab2.add(m2);
						}
					}
				}
			}
		}

		/*for (TreeNode treeNode : nodes) {
			ArrayList<Marker> markers = new ArrayList<Marker>();
			markers = getAllMarkers(treeNode);
			if(!treeNode.equals(gui.getNoAsigTreeNode()))
				traceRoute(markers, treeNode.getAttributeAsObject("marker") != null ? 1 : 0);
		}*/

		return resTrab2;
	}

	public ArrayList<VectorFeature>  getAllMarkers3(TreeNode tn) {
		ArrayList<VectorFeature> resTrab2 = new ArrayList<VectorFeature>();

		Node[] childs = tn.getChildNodes();
		if (tn.getAttributeAsObject("entity") instanceof String
				&& tn.getAttributeAsObject("entity").toString()
				.equals("noAsig")) {
			for (Node node : childs) {
				Node[] ns = ((TreeNode)node).getChildNodes();
				for (Node n : ns) {
					Marker m = (Marker) n.getAttributeAsObject("marker");
					VectorFeature m2 = (VectorFeature) n
							.getAttributeAsObject("marker2");
					if (m != null && m2 != null) {
						resTrab2.add(m2);
					}
				}
			}
		} else {
			for (Node node : childs) {
				Marker m = (Marker) node.getAttributeAsObject("marker");
				VectorFeature m2 = (VectorFeature) node
						.getAttributeAsObject("marker2");
				if (m != null && m2 != null) {
					resTrab2.add(m2);
				}
			}
		}
		return resTrab2;
	}
	public void showAll() {
		clearOverlays();

		getVecTrab().removeAllFeatures();
		//gui.getMultiSelectToggleButton().setDisabled(false);
		ArrayList<ArrayList<Object>> m = getAllMarkers();
		getMkVeh().addMarkersAsObject(m.get(0));
		getMkTrab().addMarkersAsObject(m.get(1));
		// getVecTrab().addFeatures(m.get(2));
		getMkVeh().setIsVisible(true);
		getMkTrab().setIsVisible(true);

		extend();
	}

	public void showAllNoError() {

		clearOverlays();

		getVecTrab().removeAllFeatures();
		ArrayList<ArrayList<Object>> m = getAllMarkers();
		getMkVeh().addMarkersAsObject(m.get(0));
		getMkTrab().addMarkersAsObject(m.get(1));

		getMkVeh().setIsVisible(true);
		getMkTrab().setIsVisible(true);
		extend();
	}

	public MyMarkers getMkPtosGlobal() {
		return mkPtosGlobal;
	}

	public MyMarkers getMkPtosRecorrido() {
		return mkPtosRecorrido;
	}

	public MyMarkers getMkPtosUsuario() {
		return mkPtosUsuario;
	}

	public MyMarkers getMkTrab() {
		return mkTrab;
	}

	public MyMarkers getMkVeh() {
		return mkVeh;
	}

	public MyMarkers getMkAux() {
		return mkAux;
	}

	public MyVector getVecRecorrido() {
		return vecRecorrido;
	}

	public void removeMarker(Marker m){
		mkVeh.removeMarker(m);
		mkTrab.removeMarker(m);
	}

	public void removeMarkers(ArrayList<Object> m){
		for (Object marker : m) {
			removeMarker((Marker)marker);
		}
	}

	public ArrayList<String[]> getSelectedKeys() {
		ArrayList<String[]> res = new ArrayList<String[]>();
		for (VectorFeature vf : selectedFeatures) {
			res.add(new String[] {
					vf.getJSObject().getPropertyAsString("key1"),
					vf.getJSObject().getPropertyAsString("key2") });
		}
		return res;
	}

	private native void setStyleMap(JSObject obj)/*-{
		var s1 = new $wnd.OpenLayers.Style({
		        'graphicWidth': 32,
		        'graphicHeight': 32,
		        'externalGraphic': '${myicon}'
		  });
		var s2 = new $wnd.OpenLayers.Style({
		        'graphicWidth': 32,
		        'graphicHeight': 32,
		        'externalGraphic': 'images/utiles/icons/selectedflag.png'
		  });

		obj['styleMap'] = new $wnd.OpenLayers.StyleMap({
			'default': s1,
            'select':  s2
        	});
	}-*/;

	public void recreateVecTrab() {
		VectorOptions options = new VectorOptions();
		options.setDisplayInLayerSwitcher(false);
		options.setIsBaseLayer(false);
		options.setProjection("EPSG:900913");

		setStyleMap(options.getJSObject());

		vecTrab = new MyVector("Trabajos", options);
		sf = new SelectFeature(vecTrab, sfo);
	}

	public VectorFeature searchJob(String trab, String ttrab){
		ArrayList<VectorFeature> vec = vecTrab.getAllFeatures();
		int i = 0;
		boolean sw = false;
		VectorFeature res = null;
		while (i < vec.size() && !sw) {
			JSObject obj = vec.get(i).getJSObject();
			if (obj.getPropertyAsString("key1").equals(trab)
					&& obj.getPropertyAsString("key2").equals(ttrab)) {
				res = vec.get(i);
				sw = true;
			}
			i++;
		}
		return res;
	}

	public void centerOnJob(String trab, String ttrab, boolean doZoom) {
		if (vecTrab != null && vecTrab.isVisible()) {
			VectorFeature res = searchJob(trab, ttrab);
			if(res != null){
				getMap().setCenter(res.getCenterLonLat());
				if(doZoom) {
					getMap().zoomTo(16);
				}
				//unSelectAll();
				//selectFeature(res);
				String info = res.getJSObject().getPropertyAsString("htmlinfo");
				if(info != null) {
					setPopup(res.getCenterLonLat(), info);
				}
			}
		}
	}

	private native void selectFeature(JSObject sf, JSObject vf)/*-{
		sf.select(vf);
	}-*/;

	private native void unselectFeature(JSObject sf, JSObject vf)/*-{
		sf.unselect(vf);
	}-*/;

	public void selectFeature(VectorFeature vf){
		selectFeature(sf.getJSObject(), vf.getJSObject());
	}

	public void unselectFeature(VectorFeature vf){
		unselectFeature(sf.getJSObject(), vf.getJSObject());
	}

	public boolean isActive() {
		return active;
	}

	public static String writeWKT(VectorFeature vf) {
		return new WKT().write(vf);
	}

	public static VectorFeature readWKT(String wkt) {
		return new WKT().read(wkt)[0];
	}

	public void setPopup(Object marker, final String info) {
		try {
			if (popup != null) {
				map.removePopup(popup);
			}
		} catch (Exception e) {
		}

		LonLat ll = null;

		if (marker instanceof Marker) {
			ll = ((Marker) marker).getLonLat();
		} else if (marker instanceof VectorFeature) {
			ll = ((VectorFeature) marker).getCenterLonLat();
		}

		popup = new FramedCloud("marker-info", ll, new Size(170, 120), info,
				new Icon("", new Size(10, 10), new Pixel(0, 0)), true);
		map.addPopup(popup);
	}
}

