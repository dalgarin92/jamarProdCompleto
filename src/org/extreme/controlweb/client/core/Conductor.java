package org.extreme.controlweb.client.core;

public class Conductor  implements SimpleEntity, RowModel {

	String id;
	String nombre;
	String cedula;
	String celular;
	
	
	
	
	public Conductor(String id, String nombre, String cedula, String celular) {
		
		this.id = id;	
		this.nombre = nombre;
		this.cedula = cedula;
		this.celular=celular;
	}

	public String[] getColumnNames() {		
		return new String[]{"C\u00f3digo", "Nombre","C\u00e9dula","Celular"};
	}

	public Object[] getRowValues() {		
		return new String[]{id, nombre,cedula, celular};
	}
	
	@SuppressWarnings("unused")
	private Conductor(){
		
	}
	
	public Conductor(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
		
	}
	
	
	public String getCelular(){
		return celular;
	}
	
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	public String getCedula() {
		return cedula;
	}
	
}
