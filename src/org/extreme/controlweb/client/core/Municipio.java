package org.extreme.controlweb.client.core;

public class Municipio implements SimpleEntity, RowModel {
	
	private String id;
	private String nombre;
	private String departamento;
	
	@SuppressWarnings("unused")
	private Municipio(){
		
	}
	
	public Municipio(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	public Municipio(String id, String nombre, String departamento) {
		this.id = id;
		this.nombre = nombre;
		this.departamento = departamento;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public String[] getColumnNames() {
		String[] r= {"id","nombre"};
		return r;
	}

	public String[] getRowValues() {
		String[] r={this.id, this.nombre};
		return r;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDepartamento() {
		return departamento;
	}
}
