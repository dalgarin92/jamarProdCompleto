package org.extreme.controlweb.client.core;

public class TipoTrabajo implements SimpleEntity {

	private String id;
	private String nombre;
	private String icono;
	
	@SuppressWarnings("unused")
	private TipoTrabajo(){
		
	}
	
	public TipoTrabajo(String id, String nombre, String icono) {
		this.id = id;
		this.nombre = nombre;
		this.icono = icono;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getIcono() {
		return icono;
	}
	
}
