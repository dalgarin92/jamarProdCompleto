package org.extreme.controlweb.client.core;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface RowModel extends IsSerializable {
	public Object[] getRowValues();
	public String[] getColumnNames();
	
}
