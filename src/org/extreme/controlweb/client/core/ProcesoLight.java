package org.extreme.controlweb.client.core;

import java.util.ArrayList;

public class ProcesoLight implements ProcInterface {

	private ArrayList<Trabajo> trabsNoUbic;
	private ArrayList<Trabajo> trabsNoAsig;
	
	private ArrayList<Contratista> conts;
	private String id;
	
	@SuppressWarnings("unused")
	private ProcesoLight(){
		
	}
	
	public ProcesoLight(String id) {
		
		this.id = id;
		
		trabsNoAsig = new ArrayList<Trabajo>();
		trabsNoUbic = new ArrayList<Trabajo>();
				
		conts = new ArrayList<Contratista>();
	}
	
	public String getId() {
		return id;
	}
	
	public ArrayList<Trabajo> getTrabsNoAsig() {
		return trabsNoAsig;
	}
	
	public ArrayList<Trabajo> getTrabsNoUbic() {
		return trabsNoUbic;
	}
	
	public void addTrabNoUbic(Trabajo e){
		trabsNoUbic.add(e);
	}
	
	public void addTrabNoAsig(Trabajo e){
		trabsNoAsig.add(e);
	}
	
	public void removeAllNoUbic(){
		trabsNoUbic.clear();
	}
	
	public void removeAllNoAsig(){
		trabsNoAsig.clear();
	}
	
	public void addContratista(Contratista c){
		conts.add(c);
	}

	public ArrayList<Contratista> getContratistas() {
		return conts;
	}

	public ArrayList<Recurso> getRecursos(Contratista cont, Turno turno) {
		ArrayList<Turno> ts = conts.get(conts.indexOf(cont)).getTurnos();
		return ts.get(ts.indexOf(turno)).getRecs();
	}
}
