package org.extreme.controlweb.client.core;

public interface DrawableSimpleEntity extends SimpleEntity {

	public String getIcon();
}
