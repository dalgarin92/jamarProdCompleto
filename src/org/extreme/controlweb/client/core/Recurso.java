package org.extreme.controlweb.client.core;

import java.util.ArrayList;

import org.extreme.controlweb.client.util.ClientUtils;

public class Recurso implements SimpleEntity, RowModel{
	
	private String id;
	private Contratista cont;
	private TipoRecurso tporec;
	private ProcInterface proceso;
	private String nombre;
	private String estadogps;
	private String estadomovil;
	private boolean online;
	
	private boolean checked;
	
	private String idmovil;
	private String nombremovil;
	
	private ArrayList<Trabajo> trabs;
	public ArrayList<Trabajo> getTrabs() {
		return trabs;
	}

	public void setTrabs(ArrayList<Trabajo> trabs) {
		this.trabs = trabs;
	}
	
	private Vehiculo veh;
	
	private ArrayList<Horario> horarios;
	private final static String[] columnNames = { "ID Recurso", 	
		  "Contratista",
		  "Tipo Recurso",
		  "Proceso",
		  "Nombre", 
		  "Estado GPS", 
		  "Estado Movil",
		  "Online",
		  "id",
		  "id1",
		  "nombre"
		  };
	
	public String[] getColumnNames() {		
		return columnNames;
	}

	public Object[] getRowValues() {		
		String[] res = { this.id,
				this.cont!=null?this.cont.getNombre():null,
				this.tporec!=null?this.tporec.getId():null,
				this.proceso!=null?this.proceso.getId():null,
				this.nombre,
				this.estadogps,
				this.estadomovil,
				this.online?"Online":"Offline",
				this.id,
				this.cont!=null?this.cont.getId():null,
				this.nombre
				};
		return res;
	}
	
	@SuppressWarnings("unused")
	private Recurso(){
		
	}
	
	public Recurso(String id,  String nombre) {
		this.id=id;
		this.nombre = nombre;
	}
	
	public Recurso(String id, Contratista cont, ProcInterface proceso,
			TipoRecurso tporec, String nombre, String estadogps,
			String estadomovil, boolean online) {
		this.cont = cont;
		this.estadogps = estadogps;
		this.estadomovil = estadomovil;
		this.id=id;
		this.nombre = nombre;
		this.online = online;
		this.proceso = proceso;
		this.tporec = tporec;
		
		trabs = new ArrayList<Trabajo>();
	}
	
	public Recurso(String id, Contratista cont, String nombre,
			String estadomovil, String estadogps) {
		this.id=id;
		this.cont = cont;
		this.nombre = nombre;
		this.estadogps = estadogps;
		this.estadomovil = estadomovil;
	}
	
	public Recurso(String id, 
			TipoRecurso tporec, String nombre, String estadogps,
			String estadomovil, boolean online) {
		this.estadogps = estadogps;
		this.estadomovil = estadomovil;
		this.id=id;
		this.nombre = nombre;
		this.online = online;
		this.tporec = tporec;
		
		trabs = new ArrayList<Trabajo>();
	}
	
	public String getHTML(){
		if(getVeh() != null)
			return ClientUtils.INFOWINDOW_FONT + "<b>Id:</b> " + getId()
			+ "<br/><b>Nombre:</b> " + getNombre().toUpperCase()
			+ "<br/><b>Placa:</b> " + getVeh().getPlaca()
			+ "<br/><b>Marca:</b> " + getVeh().getMarca()
			//+ "<br/><b>Color:</b> " + color
			+ "<br/><b>Tipo Veh\u00edculo:</b> " + getVeh().getTipoVeh()
			//+ "<br/><b>Propietario:</b> " + nompropietario
			+ "<br/><b>Direcci\u00f3n:</b> " + getVeh().getDireccion()
			+ "<br/><b>Municipio:</b> " + getVeh().getMunicipio()
			+ "<br/><b>Departamento:</b> " + getVeh().getDepartamento()
	        + "<br/><b>Tipo Recurso:</b> " + getTporec().getNombre()
			+ "<br/><b>Estado:</b> " + (getVeh().getEstadoVeh() != null ? (getVeh().getEstadoVeh().equals("E")? "ENCENDIDO" : "APAGADO") : "N/A")
			+ "<br/><b>Empresa:</b> " + getCont().getNombre()
			+ "<b><br/>\u00daltimo reporte:</b> " + (getVeh().getFechaUltRep() != null ? getVeh().getFechaUltRep() : "N/A") + "</font>";
		else
			return null;
	}
		
	public String getId() {		
		return id;
	}

	public Contratista getCont() {
		return cont;
	}

	public TipoRecurso getTporec() {
		return tporec;
	}

	public ProcInterface getProceso() {
		return proceso;
	}

	public String getNombre() {
		return nombre;
	}

	public String getEstadoGps() {
		return estadogps;
	}

	public String getEstadoMovil() {
		return estadomovil;
	}

	public boolean isOnline() {
		return online;
	}
	
	public void addTrabajo(Trabajo e) {
		trabs.add(e);
	}
	
	public ArrayList<Trabajo> getTrabajos() {
		return trabs;
	}
	
	public Vehiculo getVeh() {
		return veh;
	}
	
	public void setVeh(Vehiculo veh) {
		this.veh = veh;
	}
	
	public String getMapIcon() { 
		String res =  (getVeh().getEstadoVeh().equals("E")) ? "CAR_ON" : "CAR_OFF";
		//if(getVeh().getHorasAtraso() > 0)
		//	res = "CAR_NOGPS";
		
		return res;
	}

	public String getTitle() {
		String res =  "";
		if(getNombreMovil() != null && !getNombreMovil().equals(""))
			res += "<span style='color:#0000FF;'>&lt;" + getNombreMovil() + "&gt; </span>";
		
		res += getNombre().toUpperCase() + " [" + getId() + "]";
		if(getTrabajos() != null && getTrabajos().size() > 0)
			res += " -- " + getTrabajos().size();
		return res;
	}
	
	public void setOnline(boolean online) {
		this.online = online;
	}

	public String getIconTree() {
		String iconTree = "icon-";

		if (getEstadoMovil() != null) {
			if (getEstadoMovil().equals("OUT"))
				iconTree += "0";
			else if (getEstadoMovil().equals("IN"))
				iconTree += "1";
			else if (getEstadoMovil().equals("PAUSED"))
				iconTree += "2";
			if (getEstadoGps().equals("NOTOK"))
				iconTree += "0";
			else if (getEstadoGps().equals("OK"))
				iconTree += "1";
			else if (getEstadoGps().equals("UNKNOWN"))
				iconTree += "2";
		} else
			return null;
		return iconTree;
	}
	
	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
	}
	
	public void setCont(Contratista cont) {
		this.cont = cont;
	}
	
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public String getIdMovil() {
		return idmovil;
	}
	
	public void setIdMovil(String idmovil) {
		this.idmovil = idmovil;
	}
	
	public String getNombreMovil() {
		return nombremovil;
	}
	
	public void setNombreMovil(String nombremovil) {
		this.nombremovil = nombremovil;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Recurso)
			return ((Recurso) obj).getId().equals(getId())
				&& ((Recurso) obj).getCont().equals(getCont());
		else
			return false;
	}

	public void clearTrabajos() {
		if(trabs != null)
			trabs.clear();
	}	
	
	public ArrayList<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(ArrayList<Horario> horarios) {
		this.horarios = horarios;
	}
	
}
