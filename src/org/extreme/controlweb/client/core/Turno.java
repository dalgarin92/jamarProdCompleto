package org.extreme.controlweb.client.core;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Turno implements IsSerializable {

	String nombre;
	String icontree;
	
	ArrayList<Recurso> recs;
	
	@SuppressWarnings("unused")
	private Turno(){
		
	}
	
	public Turno(String icontree, String nombre) {
		this.icontree = icontree;
		this.nombre = nombre;
		
		recs = new ArrayList<Recurso>();
	}
	
	public String getNombre() {
		return nombre;
	}
	public String getIconTree() {
		return icontree;
	}
	
	public void setIcontree(String icontree) {
		this.icontree = icontree;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public ArrayList<Recurso> getRecs() {
		return recs;
	}
	
	public void addRec(Recurso rec) {
		if(!recs.contains(rec))
			recs.add(rec);
		else
			recs.set(recs.indexOf(rec), rec);
	}
	
	public void addRecIfExists(Recurso rec) {
		if(recs.contains(rec))
			recs.set(recs.indexOf(rec), rec);
	}

	public String getTitle() {
		return getNombre() + " (" + getCheckedRecursos().size()+ "/" + getRecs().size() + ")";
	}

	public ArrayList<Recurso> getCheckedRecursos() {
		ArrayList<Recurso> res = new ArrayList<Recurso>();
		for (int i = 0; i < recs.size(); i++) 
			if(recs.get(i).isChecked())
				res.add(recs.get(i));
		return res;
	}
	
	public void removeRecurso(Recurso rec){
		recs.remove(recs.indexOf(rec));
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((Turno)obj).getNombre().equals(this.getNombre());
	}
}
