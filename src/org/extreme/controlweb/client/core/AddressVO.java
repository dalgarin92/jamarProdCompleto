/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.extreme.controlweb.client.core;

import java.io.Serializable;


/**
 *
 * @author Extreme
 */
public class AddressVO implements Serializable {

    private String address;
    private String city;
    private String state;
    private Bound bound;
    private double[] location;

    public AddressVO() {
        this.location = new double[]{0d, 0d};
        this.bound = new Bound(new double[]{0d, 0d}, new double[]{0d, 0d});
    }

    public AddressVO(String address, String city, String state) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.location = new double[]{0d, 0d};
        this.bound = new Bound(new double[]{0d, 0d}, new double[]{0d, 0d});
    }

    public AddressVO(String address, String city, String state, double[] location, Bound bound) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.location = location;
        this.bound = bound;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    public Bound getBound() {
        return bound;
    }

    public void setBound(Bound bound) {
        this.bound = bound;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AddressVO)) {
            return false;
        }
        AddressVO objectToCompare = (AddressVO) obj;
        boolean isEqual = objectToCompare.getAddress().equalsIgnoreCase(this.address)
                && objectToCompare.getCity().equalsIgnoreCase(this.city)
                && objectToCompare.getState().equalsIgnoreCase(this.state);
        return isEqual;
    }   

    public class Bound {

        private double[] pointUpRight;
        private double[] pointLowLeft;

        public Bound(double[] pointUpRight, double[] pointLowLeft) {
            this.pointUpRight = pointUpRight;
            this.pointLowLeft = pointLowLeft;
        }

        public double[] getPointUpRight() {
            return pointUpRight;
        }

        public void setPointUpRight(double[] pointUpRight) {
            this.pointUpRight = pointUpRight;
        }

        public double[] getPointLowLeft() {
            return pointLowLeft;
        }

        public void setPointLowLeft(double[] pointLowLeft) {
            this.pointLowLeft = pointLowLeft;
        }

    }

}
