package org.extreme.controlweb.client.core;

public class Usuario implements SimpleEntity, RowModel {

	private String id;
	private String nombre;
	private String clave;
	private String cargo;
	private String tipo;
	
	private final static String[] columnNames = { "Descripci\u00f3n","Id de Usuario",
		  "Contrase\u00f1a",
		  "Cargo",
		  "Perfil"};
	
	
	
	public String getClave() {
		return clave;
	}

	public String getCargo() {
		return cargo;
	}

	public String getTipo() {
		return tipo;
	}

	@SuppressWarnings("unused")
	private Usuario() {
	}
	
	public Usuario(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public Usuario(String id, String nombre, String cargo, String clave,
			String tipo) {
		this.cargo = cargo;
		this.clave = clave;
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}
	
	public String[] getColumnNames() {
		return columnNames;
	}
	
	public Object[] getRowValues() {
		String[] res = { this.nombre, this.id,this.clave, this.cargo, this.tipo};
		return res;
	}

}
