package org.extreme.controlweb.client.core;


public class TipoModem implements SimpleEntity {

	String id;
	String nombre;
	
	@SuppressWarnings("unused")
	private TipoModem() {
		
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public TipoModem(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}


}
