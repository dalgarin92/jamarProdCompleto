package org.extreme.controlweb.client.core;

import org.extreme.controlweb.client.util.ClientUtils;

public class PuntoInteres extends LocalizableObject {

	private String id;
	private String descripcion;
	private String tipo;
	private String idicono;
	private final static String[] columnnames = {"id", "Descripci\u00f3n", "Latitud",
			"Longitud", "Tipo", "Direcci\u00f3n", "Municipio", "Departamento", "ID Icono" };
		 
	
	@SuppressWarnings("unused")
	private PuntoInteres() {
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public PuntoInteres(String id, String departamento, String direccion, String icono,
			String latitud, String longitud, String municipio,
			String descripcion, String tipo,String idicono) {
		super(departamento, direccion, icono, latitud, longitud, municipio);
		this.descripcion = descripcion;
		this.tipo=tipo;
		this.id = id;
		this.idicono=idicono;
	}

	@Override
	public String[] getColumnNames() {		
		return columnnames;
	}
	
	public String getTipo() {
		String t = null;
		if(tipo.equals(ClientUtils.PROCESS_POI))
			t = "Punto Global";
		else if(tipo.equals(ClientUtils.USER_POI))
			t = "Punto de Usuario";
		return t;
	}
	
	public String getId() {
		return id;
	}

	@Override
	public Object[] getRowValues() {
		String[] r={getId(), getDescripcion(),getLatitud(),getLongitud(),getTipo()
				,getDireccion(),getMunicipio(),getDepartamento(), idicono};
		return r;
	}
	
	
	
	public String getHTML(){
			
		return ClientUtils.INFOWINDOW_FONT + "<b>Descripci\u00f3n:</b> " + getDescripcion()
			+ "<br/><b>Latitud:</b> " + getLatitud()
			+ "<br/><b>Longitud:</b> " + getLongitud()
			+ "<br/><b>Tipo:</b> " + getTipo()
			+ "<br/><b>Direcci\u00f3n:</b> " + (getDireccion() != null ? getDireccion() : "NA")
			+ "<br/><b>Municipio:</b> " + (getMunicipio() != null ? getMunicipio() : "NA")
			//+ "<br/><b>Departamento:</b> " + (getDepartamento() != null ? getDepartamento() : "NA")
			+ "</font>";
}


}
