package org.extreme.controlweb.client.core;

public class Empresa implements SimpleEntity {
	
	private String id;
	private String nombre;
	
	
	@SuppressWarnings("unused")
	private Empresa() {
		
	}
	
	public Empresa(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

}
