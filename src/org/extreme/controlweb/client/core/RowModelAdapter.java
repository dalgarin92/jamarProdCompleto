package org.extreme.controlweb.client.core;

import java.util.ArrayList;
import java.util.Iterator;

public class RowModelAdapter implements RowModel {
	
	private ArrayList<String> variables;
	private ArrayList<String> valores;
	private ArrayList<Boolean> visibility;
	private ArrayList<Integer> sizes;
	
	public void addInvisibleValue(String key, String value){
		variables.add(key);
		valores.add(value);
		visibility.add(new Boolean(false));
		sizes.add(150);
	}
	
	public ArrayList<String> getVisibleVariables(){
		ArrayList<String> res = new ArrayList<String>();
		for (int i = 0; i < variables.size(); i++) {
			if(visibility.get(i))
				res.add(variables.get(i));
		}
		return res;
	}
	
	public void addValue(String key, String value){
		variables.add(key);
		valores.add(value);
		visibility.add(new Boolean(true));
		sizes.add(150);
	}
	
	public void addInvisibleVariable(String key){
		variables.add(key);
		visibility.add(new Boolean(false));
		sizes.add(150);
	}
	
	public void addVariable(String key){
		variables.add(key);
		//valores.add(value);
		visibility.add(new Boolean(true));
		sizes.add(150);
	}
	
	public RowModelAdapter(){
		variables = new ArrayList<String>();
		valores=new ArrayList<String>();
		visibility= new ArrayList<Boolean>();
		sizes = new ArrayList<Integer>();
	}
	
	public RowModelAdapter(String key, String value){
		this();
		addValue("id", key);
		addValue("nombre", value);
	}
	
	public boolean[] getVisibility(){
		boolean[] result= new boolean[visibility.size()];
		int i=0;
		for (Iterator<Boolean> iterator = visibility.iterator(); iterator.hasNext();) {
			result[i]= iterator.next().booleanValue();
			i++;
		}
		return result;
	}
	
	public int[] getSizes(){
		int[] result = new int[sizes.size()];
		for (int i = 0; i < sizes.size(); i++) {
			result[i] = sizes.get(i);
		}
		return result;
	}

	public String[] getColumnNames() {
		String[] result= new String[variables.size()];
		int i=0;
		for (Iterator<String> iterator = variables.iterator(); iterator.hasNext();) {
			result[i]= iterator.next();
			i++;
		}
		return result;
	}

	public String[] getRowValues() {
		String[] result= new String[valores.size()];
		int i=0;
		for (Iterator<String> iterator = valores.iterator(); iterator.hasNext();) {
			result[i]= iterator.next();
			i++;
		}
		return result;		
	}
	
	public void setVisibilityToAll(boolean vis){
		for (int i = 0; i < visibility.size(); i++) 
			visibility.set(i, vis);
	}
	
	public void setVisibility(int i, boolean vis){
		visibility.set(i, vis);
	}
	
	public void setSize(int i, int size){
		sizes.set(i, size);
	}
	
	public void setValue(int i, String value){
		valores.set(i, value);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof RowModelAdapter))
			return false;
		else {
			String[] objRV = ((RowModelAdapter) obj).getRowValues();
			String[] rv = getRowValues();
			if(rv.length != objRV.length)
				return false;
			else {
				boolean sw = true;
				int i = 0;
				while(i < rv.length && !sw){
					if(!rv[i].equals(objRV[i]))
						sw = false;
					i++;
				}
				return sw;
			}
		}
	}
}
