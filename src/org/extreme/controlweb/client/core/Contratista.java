package org.extreme.controlweb.client.core;

import java.util.ArrayList;

public class Contratista implements SimpleEntity {
	
	private String id;
	private String nombre;
	private String icon;
	//private ArrayList<Recurso> recs;
	private ArrayList<Turno> turnos;
	
	@SuppressWarnings("unused")
	private Contratista(){
		
	}
	
	public Contratista(String id) {
		this.id = id;
	}
	
	public Contratista(String id, String nombre, String icon) {
		this.id = id;
		this.nombre = nombre;
		this.icon = icon;
		
		turnos = new ArrayList<Turno>();
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getIcon() {
		return icon;
	}
	
	/*public void addRecurso(Recurso rec){
		if(recs == null)
			recs = new ArrayList<Recurso>();
		recs.add(rec);
	}*/
	
	public void addTurno(Turno turno) {
		if(turnos.size() < 3) { 
			turnos.add(turno);
		} else
			throw new ArrayIndexOutOfBoundsException();
	}
	
	public ArrayList<Turno> getTurnos() {
		return turnos;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Contratista)
			return ((Contratista)obj).getId().equals(getId());
		else
			return false;
	}
	
	public ArrayList<Recurso> getRecursos(){
		ArrayList<Recurso> res = new ArrayList<Recurso>();
		for (int i = 0; i < turnos.size(); i++) 
			res.addAll(turnos.get(i).getRecs());
		return res;
	}
}
