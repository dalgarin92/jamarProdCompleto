package org.extreme.controlweb.client.core;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface SimpleEntity extends IsSerializable{

	public String getId();
	public String getNombre();	
}
