package org.extreme.controlweb.client.core;

public class TipoRecurso implements SimpleEntity {
	
	private String id;
	private String nombre;
	
	@SuppressWarnings("unused")
	private TipoRecurso(){
		
	}
	
	public TipoRecurso(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
}
