package org.extreme.controlweb.client.core;

import com.google.gwt.user.client.rpc.IsSerializable;

public class WayPoint implements IsSerializable{
		
		private String latitude;
	    private String longitude;
	    private String address;
	    private String municipality;

	    public WayPoint() {
	    }

	    
	    public WayPoint(String latitude, String longitude) {
	        this.latitude = latitude;
	        this.longitude = longitude;
	    }

	    public WayPoint(String latitude, String longitude,String address,String municipality) {
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.address=address;
	        this.municipality=municipality;
	    }

	    

	    public String getLatitude() {
	        return latitude;
	    }

	    public void setLatitude(String latitude) {
	        this.latitude = latitude;
	    }

	    public String getLongitude() {
	        return longitude;
	    }

	    public void setLongitude(String longitude) {
	        this.longitude = longitude;
	    }

	    public String getAddress() {
	        return address;
	    }

	    public void setAddress(String address) {
	        this.address = address;
	    }

	    public void setMunicipality(String municipality) {
	        this.municipality = municipality;
	    }

	    public String getMunicipality() {
	        return municipality;
	    }

	    @Override
	    public String toString() {
	        return latitude!=null&&longitude!=null?latitude+","+longitude:address!=null?
	                address+(municipality!=null?", "+municipality:""):"";
	    }
	    
}
