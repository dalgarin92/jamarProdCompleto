package org.extreme.controlweb.client.core;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.util.ClientUtils;

public class Trabajo implements RowModel, Comparable<Trabajo> {
	private String atraso;
	private String desc;
	private String estado;
	private String fecha;
	private String fechaDescarga;
	private String fechaInicio;
	private String id;
	private boolean inicioRuta;
	private double lat;
	private double lon;

	private Date fechaPrecierreAsDate;

	@SuppressWarnings("unused")
	private String mapicon;

	private boolean prioritario;
	private String tematico;
	private TipoTrabajo tpotrab;
	private Cliente cli;
	private Municipio munip;
	private String direccion;
	private ArrayList<Contratista> posiblesCont;
	private String fechaEntrega;
	private String horaEntrega;
	private String numviaje;
	private String barrio;
	private String telefono;
	private String codjornada;
	private String nomjornada;
	private boolean pendienteATiempo;
	private boolean cerradoEnJornada;
	private boolean isRealizado;
	private String fechaprecierre;
	private String prioridad;
	private String estadoatraso;

	private final static String[] columnNames = {
		/*1*/	  "ID Trabajo",
		"Tipo Trabajo",
		"Descripci\u00f3n",
		"Cliente",
		/*5*/	  "Municipio",
		"Direcci\u00f3n",
		"Atraso",
		"Estado",
		"Fecha",
		/*10*/	  "Fecha Descarga",
		"Fecha Inicio",
		"Inicio Ruta",
		"Prioritario",
		"_tipotrabajo_",
		/*15*/	  "_cliente_",
		"_municipio_",
		/*17*/	  "_prioridad_"
	};

	@Override
	public Object[] getRowValues() {
		String[] res = { id,
				tpotrab.getNombre(),
				desc,
				cli.getNombre(),
				munip.getNombre(),
				direccion,
				atraso,
				getNombreEstado(),
				fecha,
				fechaDescarga,
				fechaInicio,
				inicioRuta ? "Si" : "No",
						prioritario ? "Si" : "No",
								getTpoTrab().getId(),
								cli.getId(),
								munip.getId(),
								prioritario + "" };
		return res;
	}

	public String getNombreEstado() {
		return getEstado(estado);
	}


	public static String getEstado(String estado) {
		if (estado.equals("00")) {
			return "Recien Creado";
		} else if (estado.equals("10")) {
			return "Descargado";
		} else if (estado.equals("20")) {
			return "En Ejecuci\u00f3n";
		} else if(estado.equals("30")) {
			return "Ejecutado";
		} else if (estado.equals("50")) {
			return "Rechazado";
		} else if (estado.equals("60")) {
			return "En Pausa";
		} else {
			return "N/A";
		}
	}


	@Override
	public String[] getColumnNames() {
		return columnNames;
	}

	@SuppressWarnings("unused")
	private Trabajo(){

	}

	public Trabajo(String id, String tpotrab) {
		this.id = id;
		this.tpotrab = new TipoTrabajo(tpotrab, null, null);
	}

	public Trabajo(String id, TipoTrabajo tpotrab, Cliente cli,
			Municipio munip, String direccion, double lat, double lon,
			String desc, String atraso, String estado, String fecha,
			String fechaDescarga, String fechaInicio, boolean inicioRuta,
			boolean prioritario, String mapicon, String tematico,String prioridad) {

		this.atraso = atraso;
		this.desc = desc;
		this.estado = estado;
		this.fecha = fecha;
		this.fechaDescarga = fechaDescarga;
		this.fechaInicio = fechaInicio;
		this.id = id;
		this.inicioRuta = inicioRuta;
		this.lat = lat;
		this.lon = lon;
		this.mapicon = mapicon;
		this.prioritario = prioritario;
		this.tematico = tematico;
		this.tpotrab = tpotrab;
		this.cli = cli;
		this.munip = munip;
		this.direccion = direccion;
		this.prioridad = prioridad;
		posiblesCont = new ArrayList<Contratista>();
	}


	public Trabajo(String id, String nomtpotrab, String idcli, String nomcli,
			Municipio munip, String direccion, double lat, double lon,
			String estado, String fecha, boolean inicioRuta,
			boolean prioritario, String atraso, String numviaje, String fechaprecierre) {
		tpotrab = new TipoTrabajo(null, nomtpotrab, null);
		cli = new Cliente(idcli, nomcli);
		this.munip = munip;
		this.estado = estado;
		this.fecha = fecha;
		this.id = id;
		this.inicioRuta = inicioRuta;
		this.lat = lat;
		this.lon = lon;
		mapicon = estado.equals("30") ? "ATRASO4" : "ATRASO3";
		this.prioritario = prioritario;
		this.direccion = direccion;
		this.atraso = atraso;
		this.numviaje = numviaje;
		this.fechaprecierre = fechaprecierre;
	}

	public Trabajo(String id, String nomtpotrab, String cli,
			Municipio munip, String direccion, double lat, double lon,
			String estado, String fecha, boolean inicioRuta,
			boolean prioritario) {
		this.cli = new Cliente(null, cli);
		tpotrab = new TipoTrabajo(null, nomtpotrab, null);
		this.munip = munip;
		this.estado = estado;
		this.fecha = fecha;
		this.id = id;
		this.inicioRuta = inicioRuta;
		this.lat = lat;
		this.lon = lon;
		mapicon = estado.equals("30") ? "ATRASO4" : "ATRASO3";
		this.prioritario = prioritario;
		this.direccion = direccion;
	}

	public String getHTML(){
		String res = ClientUtils.INFOWINDOW_FONT
				+ "<b>C\u00f3digo OT:</b> " + getId()
				+ "<br/><b>Concepto OT:</b> " + getTpoTrab().getNombre()
				+ "<br/><b>Num. Viaje:</b> " + getNumViaje()
				+ "<br/><b>Cliente:</b> " + getCliente().getNombre() + " (" + getCliente().getId() + ")"
				+ "<br/><b>Direcci\u00f3n:</b> " + getDireccion()
				+ "<br/><b>Municipio:</b> " + getMunicipio().getNombre();
		if(fechaprecierre != null) {
			res += "<br/><b>Tiempo Ejecuci\u00f3n:</b> " + getAtraso();
		} else {
			res += "<br/><b>Atraso:</b> " + getAtraso();
		}

		res += "<br/><b>Inicio de Ruta:</b> " + (isInicioRuta() ? "SI" : "NO")
				+ "<br/><b>Prioritario:</b> "+ (isPrioritario() ? "SI" : "NO");
		if(fechaprecierre != null) {
			res += "<br/><b>Fecha Precierre:</b> " + getFechaPrecierre();
		}
		res += "</font>";

		return res;
	}

	public String getHTML2(){
		return ClientUtils.INFOWINDOW_FONT
				//+ "<b>ID Trabajo:</b> " + getId()
				+ "<b>C\u00f3digo Trabajo:</b> " + getId()
				+ "<br/><b>Concepto Trabajo:</b> " + getTpoTrab().getNombre()
				+ "<br/><b>Cliente:</b> " + getCliente().getNombre()
				+ "<br/><b>Direcci\u00f3n:</b> " + getDireccion()
				+ "<br/><b>Municipio:</b> " + getMunicipio().getNombre()
				+ "<br/><b>Departamento:</b> " + getMunicipio().getDepartamento()
				+ "<br/><b>Fecha de creaci\u00f3n:</b> " + getFecha()
				+ "<br/><b>Inicio de Ruta:</b> "
				+ (isInicioRuta() ? "SI" : "NO")
				+ "<br/><b>Prioritario:</b> "
				+ (isPrioritario() ? "SI" : "NO")
				+ "</font>";
	}

	public String getAtraso() {
		return atraso;
	}

	public String getFechaPrecierre() {
		return fechaprecierre;
	}

	public String getDesc() {
		return desc;
	}

	public String getEstado() {
		return estado;
	}

	public String getFecha() {
		return fecha;
	}

	public String getFechaDescarga() {
		return fechaDescarga;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public String getId() {
		return id;
	}

	public boolean isInicioRuta() {
		return inicioRuta;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	public String getMapIcon()
	  {
	    String icon = "ATRASO1";
	    if (getEstado().equals("00")) {
	      icon = "ATRASO1";
	    } else if ((getEstado().equals("10")) || (getEstado().equals("20"))) {
	      icon = "ATRASO2";
	    } else if ((getEstado().equals("30")) || 
	      (getEstado().equals("40")) || 
	      (getEstado().equals("50"))) {
	      icon = "ATRASO4";
	    }
	    return icon;
	  }

	public boolean isPrioritario() {
		return prioritario;
	}

	public String getTematico() {
		return tematico;
	}

	public Cliente getCliente() {
		return cli;
	}

	public String getDireccion() {
		return direccion;
	}

	public Municipio getMunicipio() {
		return munip;
	}

	public TipoTrabajo getTpoTrab() {
		return tpotrab;
	}

	public void addPosibleCont(Contratista cont) {
		posiblesCont.add(cont);
	}

	public ArrayList<Contratista> getPosiblesCont() {
		return posiblesCont;
	}

	public void setPrioritario(boolean prioritario) {
		this.prioritario = prioritario;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setInicioRuta(boolean inicioRuta) {
		this.inicioRuta = inicioRuta;
	}

	public void resetEstado() {
		estado = "00";
	}

	public void setNumViaje(String numviaje) {
		this.numviaje = numviaje;
	}

	public String getNumViaje() {
		return numviaje;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setCodJornada(String codjornada) {
		this.codjornada = codjornada;
	}

	public String getCodJornada() {
		return codjornada;
	}

	public void setNomJornada(String nomjornada) {
		this.nomjornada = nomjornada;
	}

	public String getNomJornada() {
		return nomjornada;
	}

	public String getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(String fechaEntrega) {
		if(fechaEntrega != null && !fechaEntrega.equals("X")) {
			this.fechaEntrega = fechaEntrega;
		} else {
			this.fechaEntrega = null;
		}
	}

	public String getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(String horaEntrega) {
		if(horaEntrega != null && !horaEntrega.equals("X")) {
			this.horaEntrega = horaEntrega;
		} else {
			this.horaEntrega = null;
		}
	}

	public void setCerradoEnJornada(boolean cerradoEnJornada) {
		this.cerradoEnJornada = cerradoEnJornada;
	}

	public void setRealizado(boolean isRealizado) {
		this.isRealizado = isRealizado;
	}

	public void setPendienteATiempo(boolean pendienteATiempo) {
		this.pendienteATiempo = pendienteATiempo;
	}

	public boolean isPendienteATiempo() {
		return pendienteATiempo;
	}

	public boolean isCerradoEnJornada() {
		return cerradoEnJornada;
	}

	public boolean isRealizado() {
		return isRealizado;
	}

	public Date getFechaPrecierreAsDate() {
		return fechaPrecierreAsDate;
	}

	public void setFechaPrecierreAsDate(Date fechaPrecierreAsDate) {
		this.fechaPrecierreAsDate = fechaPrecierreAsDate;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Trabajo) {
			return getId().equals(((Trabajo) obj).getId())
					&& getTpoTrab().getId().equals(
							((Trabajo) obj).getTpoTrab().getId());
		} else {
			return false;
		}
	}

	public String getIcono() {
		String icon = "icon-";
			if(getEstado().equals("00")) {
				icon +="blue";
			}else if(getEstado().equals("10")||getEstado().equals("20")) {
				icon +="orange";
			}else if(getEstado().equals("30")
					|| getEstado().equals("40")
					|| getEstado().equals("50")) {
				icon +="green";
			}else{
				icon +="blue";
			}
			icon+= "-diamond";
	
	
		
		return icon;
	}


	@Override
	public int compareTo(Trabajo o) {
		if(o == null) {
			throw new NullPointerException();
		} else if(getFechaPrecierreAsDate() == null || o.getFechaPrecierreAsDate() == null) {
			return 1;
		} else if (equals(o)) {
			return 0;
		} else if(getFechaPrecierreAsDate().before(o.getFechaPrecierreAsDate())) {
			return -1;
		} else {
			return 1;
		}
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getEstadoatraso() {
		return estadoatraso;
	}

	public void setEstadoatraso(String estadoatraso) {
		this.estadoatraso = estadoatraso;
	}

}
