package org.extreme.controlweb.client.core;

public class SimpleEntityAdapter implements SimpleEntity {

	private String id;
	private String nombre;	
	
	@SuppressWarnings("unused")
	private SimpleEntityAdapter() {}
	
	public SimpleEntityAdapter(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {		
		return id;
	}

	public String getNombre() {		
		return nombre;
	}

}
