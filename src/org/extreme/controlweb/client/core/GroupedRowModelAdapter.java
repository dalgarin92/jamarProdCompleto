package org.extreme.controlweb.client.core;

public class GroupedRowModelAdapter extends RowModelAdapter {
	String group=null;
	public String getGroup() {
		return group;
	}
	public void addProperty(String group, String name, String value){
		addValue(name, value);
		this.group=group;
	}
	
	public GroupedRowModelAdapter() {
		super();
	}
	
}
