package org.extreme.controlweb.client.core;


public class Vehiculo extends LocalizableObject {

	private String idveh;
	private String placa;
	private String color;	
	private String marca;
	private String nompropietario;
	private String tipoveh;
	private String fechaultrep;
	private String tipomodem;
	private String estadoVeh;
	private String modelo;
	private int horasAtraso;
	private String imei;
	private String min;
	private String puk;
	private Recurso r;
	private String idprop;
	
	public String getImei() {
		return imei;
	}

	public String getMin() {
		return min;
	}

	public String getPuk() {
		return puk;
	}

	public Recurso getR() {
		return r;
	}
	
	

	public Vehiculo(String color, String estadoVeh, String fechaultrep,
			int horasAtraso, String idveh, String imei, String marca,
			String min, String nompropietario, String placa, String puk,
			Recurso r, String tipomodem, String tipoveh, String modelo, String idprop) {
		this.color = color;
		this.estadoVeh = estadoVeh;
		this.fechaultrep = fechaultrep;
		this.horasAtraso = horasAtraso;
		this.idveh = idveh;
		this.imei = imei;
		this.marca = marca;
		this.min = min;
		this.nompropietario = nompropietario;
		this.placa = placa;
		this.puk = puk;
		this.r = r;
		this.tipomodem = tipomodem;
		this.tipoveh = tipoveh;
		this.modelo=modelo;
		this.idprop=idprop;
	}



	private final static String[] columnNames = { 
		 /*1*/"ID Vehiculo", 	
		  "Placa",
		  "Color",
		  "Marca",
		 /*5*/ "Modelo",
		  "Propietario", 
		  "Tipo vehiculo", 
		  "Fecha ultimo reporte",
		  "Tipo Modem", 
		  /*10*/"Estado Vehiculo",
		  "IMEI",
		  "MIN",
		  "PUK",
		  "IdProp",
		  /*15*/"Recurso",
		  "Contratista"
		  };
	
	public Object[] getRowValues() {
		String[] res = { 
				/*1*/this.idveh, 
				this.placa,
				this.color, 
				this.marca,
				/*5*/this.modelo,
				this.nompropietario, 
				this.tipoveh, 
				this.fechaultrep,
				this.tipomodem, 
				/*10*/this.estadoVeh,
				this.imei, 
				this.min, 
				this.puk,
				this.idprop,
				/*15*/this.r!=null?this.r.getId():null, 
				this.r!=null?this.r.getCont()!=null?this.r.getCont().getId():null:null};
		return res;
	}

	public String[] getColumnNames() {
		return columnNames;
	}
	
	@SuppressWarnings("unused")
	private Vehiculo(){
		
	}
	
	
	
	public Vehiculo(String idveh, String placa, String color, String marca,
			String tipoveh, String nompropietario, String lat, String lon,
			String departamento, String municipio, String direccion,
			String fechaultrep, String tipomodem, String estadoVeh, int horasAtraso) {
		super(departamento, direccion, lat, lon, municipio);
		this.idveh = idveh;
		this.placa = placa;
		this.color = color;
		this.marca = marca;
		this.tipoveh = tipoveh;
		this.nompropietario = nompropietario;
		this.fechaultrep = fechaultrep;
		this.tipomodem = tipomodem;
		this.estadoVeh = estadoVeh;
		this.horasAtraso = horasAtraso;
	}

	public String getIdVeh() {
		return idveh;
	}

	public String getPlaca() {
		return placa;
	}

	public String getColor() {
		return color;
	}

	public String getMarca() {
		return marca;
	}

	public String getNompropietario() {
		return nompropietario;
	}

	public String getTipoVeh() {
		return tipoveh;
	}

	public String getFechaUltRep() {
		return fechaultrep;
	}
	
	public String getTipoModem() {
		return tipomodem;
	}
	
	public String getEstadoVeh() {
		return estadoVeh;
	}
	
	public int getHorasAtraso() {
		return horasAtraso;
	}
	
}
