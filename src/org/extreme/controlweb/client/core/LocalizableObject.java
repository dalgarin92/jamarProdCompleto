package org.extreme.controlweb.client.core;

import com.google.gwt.user.client.rpc.IsSerializable;


public class LocalizableObject implements IsSerializable, RowModel {
	private String latitud;
	private String longitud;
	private String direccion;
	private String icono;
	private String municipio;
	private String departamento;
	
	
	public String getMunicipio() {
		return municipio;
	}

	public String getDepartamento() {
		return departamento;
	}

	public LocalizableObject() {
	}

	public LocalizableObject(String departamento, String direccion,
			String icono, String latitud, String longitud, String municipio) {
		this.departamento = departamento;
		this.direccion = direccion;
		this.icono = icono;
		this.latitud = latitud;
		this.longitud = longitud;
		this.municipio = municipio;
	}
	
	

	public LocalizableObject(String departamento, String direccion,
			String latitud, String longitud, String municipio) {
		this.departamento = departamento;
		this.direccion = direccion;
		this.latitud = latitud;
		this.longitud = longitud;
		this.municipio = municipio;
	}	

	public LocalizableObject(String latitud, String longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public String getLatitud() {
		return latitud;
	}
	
	public Double getLatitudAsDouble() {
		try {
			return Double.parseDouble(latitud);
		} catch (Exception e) {
			return 0.0d;
		}
	}

	public String getLongitud() {
		return longitud;
	}
	
	public Double getLongitudAsDouble() {
		try {
			return Double.parseDouble(longitud);
		} catch (Exception e) {
			return 0.0d;
		}
	}

	public String getDireccion() {
		return direccion;
	}

	public String getIcono() {
		return icono;
	}

	public String[] getColumnNames() {
		return new String[] { "Latitud", "Longitud", "Direccion", "Municipio",
				"Departamento" };
	}

	public Object[] getRowValues() {
		return new Object[] { latitud, longitud, direccion, municipio,
				departamento };
	}
}

