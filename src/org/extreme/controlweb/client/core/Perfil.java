package org.extreme.controlweb.client.core;


public class Perfil implements SimpleEntity {
	
	String id;
	String nombre;

	@SuppressWarnings("unused")
	private Perfil() {
	}

	public Perfil(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}


}
