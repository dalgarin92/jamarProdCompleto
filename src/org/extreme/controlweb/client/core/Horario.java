package org.extreme.controlweb.client.core;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Horario implements IsSerializable{

	private String initialTime;
	private String endTime;
	private String name;
	private String dayName;
	private int duration;

	
	public Horario() {
	}
	
	public Horario( String initialTime, String endTime, String name, String dayName,int duration) {
		this.initialTime=initialTime;
		this.endTime=endTime;
		this.name=name;
		this.dayName=dayName;
		this.duration=duration;
	}


	public String getInitialTime() {
		return initialTime;
	}


	public void setInitialTime(String initialTime) {
		this.initialTime = initialTime;
	}


	public String getendTime() {
		return endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getDayName() {
		return dayName;
	}

	public void setDayName(String dayName) {
		this.dayName = dayName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	
}
