package org.extreme.controlweb.client.core;

public class Cliente implements SimpleEntity, RowModel {

	String id;
	String nombre;
	String direccion;
	String municipio;
	String telefono;
	
	
	
	public Cliente(String id, String nombre, String municipio,
			String direccion, String telefono) {
		this.direccion = direccion;
		this.id = id;
		this.municipio = municipio;
		this.nombre = nombre;
		this.telefono = telefono;
	}

	public String[] getColumnNames() {		
		return new String[]{"id", "Nombre","Municipio","Direcci\u00f3n" ,"Telefono"};
	}

	public Object[] getRowValues() {		
		return new String[]{id, nombre,municipio, direccion, telefono};
	}
	
	@SuppressWarnings("unused")
	private Cliente(){
		
	}
	
	public Cliente(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}

	
}
