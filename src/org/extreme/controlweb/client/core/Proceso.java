package org.extreme.controlweb.client.core;

import java.util.ArrayList;

public class Proceso implements ProcInterface {

	private String id;
	private String nombre;
	private boolean conTurnos;
	private boolean conContratistas;
	private boolean solocentral;
	private String iconTree;

	private ArrayList<Trabajo> trabsNoUbic;
	private ArrayList<Trabajo> trabsNoAsig;

	private ArrayList<TipoTrabajo> tposTrab;
	private ArrayList<TipoRecurso> tposRec;
	private ArrayList<Contratista> conts;
	private ArrayList<Municipio> munips;
	private int prioridades;



	@SuppressWarnings("unused")
	private Proceso(){

	}

	public Proceso(String id, String nombre, boolean manejaturnos,
			boolean usacontratistas, boolean solocentral, String iconTree,
			boolean manejalbs) {
		this.id = id;
		this.nombre = nombre;
		conTurnos = manejaturnos;
		conContratistas = usacontratistas;
		this.solocentral = solocentral;
		this.iconTree = iconTree;

		trabsNoAsig = new ArrayList<Trabajo>();
		trabsNoUbic = new ArrayList<Trabajo>();

		tposRec = new ArrayList<TipoRecurso>();
		tposTrab = new ArrayList<TipoTrabajo>();
		conts = new ArrayList<Contratista>();
		munips = new ArrayList<Municipio>();
	}

	public Proceso(String id, String nombre, boolean manejaturnos,
			boolean usacontratistas, boolean solocentral, String iconTree,int prioridades) {
		this.id = id;
		this.nombre = nombre;
		conTurnos = manejaturnos;
		conContratistas = usacontratistas;
		this.solocentral = solocentral;
		this.iconTree = iconTree;
		this.prioridades= prioridades;
		trabsNoAsig = new ArrayList<Trabajo>();
		trabsNoUbic = new ArrayList<Trabajo>();

		tposRec = new ArrayList<TipoRecurso>();
		tposTrab = new ArrayList<TipoTrabajo>();
		conts = new ArrayList<Contratista>();
		munips = new ArrayList<Municipio>();
	}





	@Override
	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public boolean isConContratistas() {
		return conContratistas;
	}

	public boolean isConTurnos() {
		return conTurnos;
	}

	public boolean isSoloCentral() {
		return solocentral;
	}

	@Override
	public ArrayList<Trabajo> getTrabsNoAsig() {
		return trabsNoAsig;
	}

	public void setTrabsNoAsig(ArrayList<Trabajo> trabsNoAsig) {
		this.trabsNoAsig = trabsNoAsig;
	}

	@Override
	public ArrayList<Trabajo> getTrabsNoUbic() {
		return trabsNoUbic;
	}

	public void setTrabsNoUbic(ArrayList<Trabajo> trabsNoUbic) {
		this.trabsNoUbic = trabsNoUbic;
	}

	@Override
	public void addTrabNoUbic(Trabajo e){
		trabsNoUbic.add(e);
	}

	@Override
	public void addTrabNoAsig(Trabajo e){
		trabsNoAsig.add(e);
	}

	@Override
	public void removeAllNoUbic(){
		trabsNoUbic.clear();
	}

	@Override
	public void removeAllNoAsig(){
		trabsNoAsig.clear();
	}

	public void addTipoTrabajo(TipoTrabajo tt){
		tposTrab.add(tt);
	}

	public void addTipoRecurso(TipoRecurso tr){
		tposRec.add(tr);
	}

	public void addMunicipio(Municipio m) {
		munips.add(m);
	}

	@Override
	public void addContratista(Contratista c){
		conts.add(c);
	}

	public ArrayList<TipoTrabajo> getTiposTrab() {
		return tposTrab;
	}

	public ArrayList<TipoRecurso> getTiposRec() {
		return tposRec;
	}

	@Override
	public ArrayList<Contratista> getContratistas() {
		return conts;
	}

	public void setContratistas(ArrayList<Contratista> conts) {
		this.conts = conts;
	}

	public ArrayList<Municipio> getMunicipios() {
		return munips;
	}

	public String getIconTree() {
		return iconTree;
	}

	public ArrayList<Recurso> getRecursos(){
		ArrayList<Recurso> res = new ArrayList<Recurso>();
		for (int i = 0; i < conts.size(); i++) {
			res.addAll(conts.get(i).getRecursos());
		}
		return res;
	}

	private ArrayList<Recurso> minus(ArrayList<Recurso> checked, ArrayList<Recurso> newRecs){ //v2 = newRecs
		ArrayList<Recurso> v = new ArrayList<Recurso>();
		for (int i = 0; i < checked.size(); i++) {
			if(!newRecs.contains(checked.get(i))) {
				v.add(checked.get(i));
			}

		}
		return v;
	}

	public ArrayList<Turno> getAllTurnos(){
		ArrayList<Turno> res = new ArrayList<Turno>();
		for (Contratista cont : conts) {
			for (Turno turno : cont.getTurnos()) {
				res.add(turno);
			}
		}

		return res;
	}

	public void updateProceso(ProcesoLight newProc){
		setTrabsNoAsig(newProc.getTrabsNoAsig());
		setTrabsNoUbic(newProc.getTrabsNoUbic());
		for (Contratista cont : conts) {
			ArrayList<Turno> turnos = cont.getTurnos();
			for (Turno turno : turnos) {

				ArrayList<Recurso> checked = turno.getCheckedRecursos();
				ArrayList<Recurso> newRecs = newProc.getRecursos(cont, turno);

				for (Recurso recurso : newRecs) {
					turno.addRec(recurso);

				}
				ArrayList<Recurso> dif  = minus(checked, newRecs);
				for (Recurso recurso : dif) {
					turno.removeRecurso(recurso);

				}


			}

		}
	}

	public int getPrioridades() {
		return prioridades;
	}

	public void setPrioridades(int prioridades) {
		this.prioridades = prioridades;
	}


}
