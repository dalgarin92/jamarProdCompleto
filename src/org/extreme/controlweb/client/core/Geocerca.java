package org.extreme.controlweb.client.core;

public class Geocerca extends LocalizableObject implements RowModel {

	private String idgeocerca;
	private String ingreso;
	private String egreso;
	private String descripcion;
	private String idvehiculo;
	private String numgeocerca;
	private String radio;
	private String estado;

	private final static String[] columnNames = { "ID Geocerca", 	
												  "ID Vehiculo",
												  "N\u00famero Geocerca",
												  "Descripci\u00f3n",
												  "C\u00f3digo de ingreso", 
												  "C\u00f3digo de egreso", 
												  "Latitud",
												  "Longitud", 
												  "Radio", 
												  "Estado" };
	
	public Geocerca(String idgeocerca, String idvehiculo, String numgeocerca,
			String descripcion, String ingreso, String egreso, String latitud,
			String longitud, String radio, String estado) {
		super(latitud, longitud);
		this.idgeocerca = idgeocerca;
		this.idvehiculo = idvehiculo;
		this.numgeocerca = numgeocerca;
		this.descripcion = descripcion;
		this.ingreso = ingreso;
		this.egreso = egreso;
		this.radio = radio;
		this.estado = estado;
	}

	public Geocerca(String numgeocerca, String descripcion, String latitud,
			String longitud, String radio) {
		super(latitud, longitud);
		this.descripcion = descripcion;
		this.numgeocerca = numgeocerca;
		this.radio = radio;
	}

	public String getIdgeocerca() {
		return idgeocerca;
	}

	public void setIdgeocerca(String idgeocerca) {
		this.idgeocerca = idgeocerca;
	}

	public String getIngreso() {
		return ingreso;
	}
	
	public void setIngreso(String ingreso) {
		this.ingreso = ingreso;
	}

	public String getEgreso() {
		return egreso;
	}

	public void setEgreso(String egreso) {
		this.egreso = egreso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdvehiculo() {
		return idvehiculo;
	}

	public void setIdvehiculo(String idvehiculo) {
		this.idvehiculo = idvehiculo;
	}

	public String getNumgeocerca() {
		return numgeocerca;
	}

	public void setNumgeocerca(String numgeocerca) {
		this.numgeocerca = numgeocerca;
	}

	public String getRadio() {
		return radio;
	}

	public void setRadio(String radio) {
		this.radio = radio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@SuppressWarnings("unused")
	private Geocerca() {
	}

	public Object[] getRowValues() {
		String[] res = { this.idgeocerca, this.idvehiculo, this.numgeocerca,
				this.descripcion, this.ingreso, this.egreso, getLatitud(),
				getLongitud(), this.radio, this.estado };
		return res;
	}

	public String[] getColumnNames() {
		return columnNames;
	}

}
