package org.extreme.controlweb.client.stats.listeners;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.stats.gui.WizardWindow;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ComparativeReportItemListener  extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
	private DateField fromDate;
	private DateField toDate;
	private MyComboBox cbMunip;
	private MyComboBox cbDato;
	private CheckBoxGridPanel cbgpRecCont;
	private CheckBoxGridPanel cbgpTTrabs;
	private WizardWindow wnd;
	private String reportSubject;
	private Integer tipo;
	
	private Function finalFunction;
	private Function addTTrabsFunction;
	private Function addRecContFunction;
	
	private String[] rec;
	private String[] cont;
	private Checkbox chb;
	
	public static int RECURSO = 1;
	public static int CONTRATISTA = 2;
	
	public ComparativeReportItemListener(ControlWebGUI g, int t) {
		this.gui = g;
		this.tipo = t;
		
		if(tipo == RECURSO)
			reportSubject = "Recurso";
		else if(tipo == CONTRATISTA)
			reportSubject = "Contratista";
		
		this.addRecContFunction = new Function(){
			@Override
			public void execute() {
				cbgpRecCont = new CheckBoxGridPanel(true);
				if(tipo == RECURSO){
					cbgpRecCont.addExcludedCol(5);
					cbgpRecCont.addExcludedCol(6);
					cbgpRecCont.addExcludedCol(7);
				}
				
				
				Function f = new Function() {
					@Override
					public void execute() {
						cbgpRecCont.getStore().sort("contratista", SortDir.ASC);
						wnd.addPage(cbgpRecCont, addTTrabsFunction, 1);
					}
				};
				if (tipo == RECURSO)
					gui.getQueriesServiceProxy().getAllRecursosProcesoConCarro(
							gui.getProceso().getId(),
							new RPCFillCheckboxGridPanel(cbgpRecCont, f));
				else if (tipo == CONTRATISTA)
					gui.getQueriesServiceProxy().getContratistasAsRowModel(
							gui.getProceso().getId(),
							new RPCFillCheckboxGridPanel(cbgpRecCont, f));
			}
		};
		
		this.addTTrabsFunction = new Function(){
			@Override
			public void execute() {
				cbgpTTrabs = new CheckBoxGridPanel(true);
				Function f = new Function() {
					@Override
					public void execute() {
						wnd.addPage(cbgpTTrabs, finalFunction, 2);
					}
				};
				
				
				if (tipo == RECURSO) {
					String[][] selected = cbgpRecCont.getSelectedIDs(new String[]{"id", "id_contratista"});
					rec = ClientUtils.extractColumn(selected, 0);
					cont = ClientUtils.extractColumn(selected, 1);
				} else if (tipo == CONTRATISTA)
					cont = cbgpRecCont.getSelectedIDs();
								
				gui.getQueriesServiceProxy().getTTrabajoRecurso(
						fromDate.getEl().getValue(), toDate.getEl().getValue(),
						rec, cont, cbMunip.getValue(),
						new RPCFillCheckboxGridPanel(cbgpTTrabs, f));

			}
		};
				
		this.finalFunction = new Function(){
			@Override
			public void execute() {

				Panel[] panels = gui.addStatsTab("Reporte Estad\u00edstico Comparativo");
				String[] selected = cbgpTTrabs.getSelectedIDs();
				boolean includeOthers = selected.length < cbgpTTrabs.getStore().getRecords().length;
				
				gui.getQueriesServiceProxy().getComparativeCrosstab(rec, cont,
						cbMunip.getValue(), fromDate.getEl().getValue(),
						toDate.getEl().getValue(), cbDato.getValue(), selected,
						panels[0].getWidth(), panels[0].getHeight(),
						reportSubject, includeOthers, chb.getValue(),
						new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		wnd = new WizardWindow("Par\u00e1metros de "
				+ "Reporte Estad\u00edstico", 500, 500, false, true);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		Panel p = wnd.addPage("General", new FormLayout(), addRecContFunction);
		
		cbMunip = new MyComboBox("Municipio");
		gui.getQueriesServiceProxy().getMunicipiosDeProceso(gui.getProceso().getId(),
				new RPCFillComplexComboHandler(cbMunip, p, 0, new Function(){
					@Override
					public void execute() {
						cbMunip.getStore().add(cbMunip.getRecordDef().createRecord(
									new String[] { "all", "Todos" }));
						cbMunip.getStore().commitChanges();
						cbMunip.setValue("all");
					}
				}));
		wnd.addItemToArray(cbMunip, 0, 0);
		
		wnd.addItem(fromDate);
		wnd.addItem(toDate);
		
		cbDato = new MyComboBox("Dato");
		ArrayList<RowModel> rma = new ArrayList<RowModel>();
		
		RowModelAdapter rm1 = new RowModelAdapter();
		rm1.addValue("id", "numtrabs");
		rm1.addValue("nombre", "N\u00famero OTs");
		
		RowModelAdapter rm2 = new RowModelAdapter();
		rm2.addValue("id", "tiempoprom");
		rm2.addValue("nombre", "Promedio Tiempo OTs");
				
		RowModelAdapter rm3 = new RowModelAdapter();
		rm3.addValue("id", "tiempomax");
		rm3.addValue("nombre", "M\u00e1ximo Tiempo OTs");
		
		RowModelAdapter rm4 = new RowModelAdapter();
		rm4.addValue("id", "tiempomin");
		rm4.addValue("nombre", "M\u00ednimo Tiempo OTs");
		
		rma.add(rm1);
		rma.add(rm2);
		rma.add(rm3);
		rma.add(rm4);
		
		wnd.addItemToArray(cbDato, 3, 0);
		
		Panel p2 = wnd.addPage(tipo == RECURSO ? "Recursos"
				: (tipo == CONTRATISTA ? "Contratistas" : ""), new RowLayout());
		p2.setPaddings(0);
		
		Panel p3 = wnd.addPage("Tipos de Trabajo", new RowLayout());
		p3.setPaddings(0);
		
		wnd.show();
		new RPCFillComplexComboHandler(cbDato, p, 3, new Function(){
			@Override
			public void execute() {
				cbDato.setValue("numtrabs");
			}
		}).onSuccess(rma);
		
		chb = new Checkbox();
		chb.setBoxLabel("Por dias");
		chb.setChecked(false);
		p.add(chb);
	}
	
}
