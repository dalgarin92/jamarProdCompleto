package org.extreme.controlweb.client.stats.listeners;

import java.util.Date;

import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.HorizontalLayout;
import com.gwtext.client.widgets.layout.VerticalLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class CentralComparativeReportItemListener  extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
	private DateField fromDate;
	private DateField toDate;
	private CheckBoxGridPanel cbgpRecCont;
	private ComboBox cbDato;
	
	private Function finalFunction;

	
	public CentralComparativeReportItemListener(ControlWebGUI g) {
		this.gui = g;
		
		this.finalFunction = new Function(){
			@Override
			public void execute() {
				
				String[][] selected = cbgpRecCont.getSelectedIDs(new String[]{"id", "id_contratista"});
				Panel[] panels = gui.addStatsTab("An\u00e1lisis Comparativo");
				
				String[] rec = ClientUtils.extractColumn(selected, 0);
				String[] cont = ClientUtils.extractColumn(selected, 1);
							
				gui.getQueriesServiceProxy().getComparativeAll( rec, cont, fromDate.getEl().getValue(),
						toDate.getEl().getValue(),
						panels[0].getWidth(), panels[0].getHeight(),
						cbDato.getValue().equals("km")? "Comparativo de Kilometros":
						cbDato.getValue().equals("vp")?"Comparativo de Velocidad Promedio":
						"Comparativo de Velocidad M\u00e1xima"
						,cbDato.getValue(), new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		
		final Panel panel = new Panel();
        panel.setBorder(false);
        panel.setPaddings(7);
        panel.setLayout(new VerticalLayout(15));  
  
        Panel horizontalPanel = new Panel();
        horizontalPanel.setLayout(new HorizontalLayout(15));
  
        Panel pFromDate = new Panel();
        pFromDate.setSize(220, 25);
        pFromDate.setPaddings(0, 15, 0, 0);
        pFromDate.setLayout(new FormLayout());
        pFromDate.setBorder(false);
        horizontalPanel.add(pFromDate);
        
        
        Panel pToDate= new Panel();
        pToDate.setSize(210, 25);
        pToDate.setPaddings(0, 10, 0, 0);
        pToDate.setLayout(new FormLayout());
        pToDate.setBorder(false);
        horizontalPanel.add(pToDate);
  
        panel.add(horizontalPanel);  
        
        Panel pDato = new Panel(); 
        pDato.setSize(300,20);
        pDato.setPaddings(0, 15, 0, 0);
        pDato.setLayout(new FormLayout());
        pDato.setBorder(false);
        panel.add(pDato);
        
        final Panel pVehi = new Panel(); 
        pVehi.setSize(480, 400);
        pVehi.setLayout(new FitLayout());
        pVehi.setBorder(false);
        panel.add(pVehi);
        /*panel.add(new Panel("Item 3", 150, 100));  
        panel.add(new Panel("Item 4", 150, 150));*/
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		cbDato = ClientUtils.createCombo(
				"Dato", new SimpleStore(new String[] { "id",
				"nombre" }, new String[][] { { "km", "Kilometros" },
				{ "vp", "Velocidad Promedio" }, { "vm","Velocidad M\u00e1xima"} }));
		cbDato.setEditable(false);
		cbDato.setValue("km");
		
		cbgpRecCont = new CheckBoxGridPanel(true);
		
		pFromDate.add(fromDate);
		pToDate.add(toDate);
		pDato.add(cbDato);
		
		gui.getQueriesServiceProxy().getAllRecursosProcesoConCarro(
				gui.getProceso().getId(),
				new RPCFillCheckboxGridPanel(cbgpRecCont, 
						new Function() {
					@Override
					public void execute() {
						pVehi.add(cbgpRecCont);
						
						final Window wd = new Window("General", false, false);
						wd.setSize(508, 552);
						wd.add(panel);
						wd.setButtonAlign(Position.CENTER);
						wd.addButton(new Button("Generar", new ButtonListenerAdapter(){
							public void onClick(Button button, EventObject e) {
										if (!fromDate.getEl().getValue().equals("")
												&& !toDate.getEl().getValue().equals("")) {
											if (fromDate.getValue().before(toDate.getValue())
													|| (fromDate.getValue().equals(toDate.getValue()) 
															&& fromDate.getValue().compareTo(toDate.getValue())<= 0)) {
												
												finalFunction.execute();
												wd.close();	
												
											} else
												ClientUtils.alert("Error",
														"Rango de fechas inv\u00e1lido.",
														ClientUtils.ERROR);
										} else
											ClientUtils.alert("Error",
													"Todas las opciones son requeridas.",
													ClientUtils.ERROR);
							};
						}));
						cbgpRecCont.getStore().sort("nombre", SortDir.ASC);
						wd.show();
					}
				}
				));
	}
	
}
