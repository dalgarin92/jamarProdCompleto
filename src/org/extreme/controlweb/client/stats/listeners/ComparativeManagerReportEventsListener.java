package org.extreme.controlweb.client.stats.listeners;

import java.util.Date;

import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.HorizontalLayout;
import com.gwtext.client.widgets.layout.VerticalLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ComparativeManagerReportEventsListener  extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
	private DateField fromDate;
	private DateField toDate;
	private CheckBoxGridPanel cbgpRecCont;
	
	private Function finalFunction;
	
	public static int RECURSO = 1;
	public static int CONTRATISTA = 2;
	
	public ComparativeManagerReportEventsListener(ControlWebGUI g) {
		this.gui = g;
		
		this.finalFunction = new Function(){
			@Override
			public void execute() {
				
				String[][] selected = cbgpRecCont.getSelectedIDs(new String[]{"id", "nombre"});
				Panel[] panels = gui.addStatsTab("An\u00e1lisis Gerencial");
				
				String[] events = ClientUtils.extractColumn(selected, 0);
								
				gui.getQueriesServiceProxy().getManagerReportEvents(events, gui.getProceso().getId(), fromDate.getEl().getValue(),
						toDate.getEl().getValue(),
						panels[0].getWidth(), panels[0].getHeight(),
						"An\u00e1lisis Gerencial de Eventos", new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		
		final Panel panel = new Panel();
        panel.setBorder(false);
        panel.setPaddings(7);
        panel.setLayout(new VerticalLayout(15));  
  
        Panel horizontalPanel = new Panel();
        horizontalPanel.setLayout(new HorizontalLayout(15));
  
        Panel pFromDate = new Panel();
        pFromDate.setSize(220, 25);
        pFromDate.setPaddings(0, 15, 0, 0);
        pFromDate.setLayout(new FormLayout());
        pFromDate.setBorder(false);
        horizontalPanel.add(pFromDate);
        
        
        Panel pToDate= new Panel();
        pToDate.setSize(210, 25);
        pToDate.setPaddings(0, 10, 0, 0);
        pToDate.setLayout(new FormLayout());
        pToDate.setBorder(false);
        horizontalPanel.add(pToDate);
  
        panel.add(horizontalPanel);  
        
        final Panel pEvents = new Panel(); 
        pEvents.setSize(480, 400);
        pEvents.setLayout(new FitLayout());
        pEvents.setBorder(false);
        panel.add(pEvents);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		

		pFromDate.add(fromDate);
		pToDate.add(toDate);
		
		cbgpRecCont = new CheckBoxGridPanel(true);
		
		gui.getQueriesServiceProxy().getAllEventos(
				new RPCFillCheckboxGridPanel(cbgpRecCont, 
						new Function() {
					@Override
					public void execute() {
						pEvents.add(cbgpRecCont);
						final Window wd = new Window("General", false, false);
						wd.setSize(508, 515);
						wd.add(panel);
						wd.setButtonAlign(Position.CENTER);
						wd.addButton(new Button("Generar", new ButtonListenerAdapter(){
							public void onClick(Button button, EventObject e) {
								if(gui.verificarRangoFecha(fromDate, toDate))
								{
									finalFunction.execute();
									wd.close();
								}else
									ClientUtils.alert("Error",
											"Rango de fechas inv\u00e1lido.",
											ClientUtils.ERROR);
							};
						}));
						cbgpRecCont.getStore().sort("nombre", SortDir.ASC);
						wd.show();
					}
				}
				));

	}
	
}
