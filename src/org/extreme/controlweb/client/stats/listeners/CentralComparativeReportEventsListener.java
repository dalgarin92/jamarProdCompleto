package org.extreme.controlweb.client.stats.listeners;

import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.stats.gui.WizardWindow;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class CentralComparativeReportEventsListener  extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
/*	private DateField fromDate;
	private DateField toDate;*/
	private CheckBoxGridPanel cbgpRecCont;
	private CheckBoxGridPanel cbgpEvento;
	private WizardWindow ww;
	private String reportSubject;
	
	private Function finalFunction;
	private Function addTTrabsFunction;
	private Function addRecContFunction;
	
	private String[] rec;
	private String[] cont;
	private DateWindow dw;
	private String toDate;
	private String fromDate;
	
	public CentralComparativeReportEventsListener(ControlWebGUI g) {
		this.gui = g;
		
			reportSubject = "Comparativo de eventos";

		
		this.addRecContFunction = new Function(){
			@Override
			public void execute() {
				ww = new WizardWindow("Par\u00e1metros de "
						+ "Reporte Estad\u00edstico", 500, 500, false, true);
				Panel p2 = ww.addPage("Veh\u00edculos", new RowLayout());
				p2.setPaddings(0);
				
				Panel p3 = ww.addPage("Eventos", new RowLayout());
				p3.setPaddings(0);
				
				cbgpRecCont = new CheckBoxGridPanel(true);
				
				Function f = new Function() {
					@Override
					public void execute() {
						ww.show();
						gui.hideMessage();
						cbgpRecCont.getStore().sort("nombre", SortDir.ASC);
						ww.addPage(cbgpRecCont, addTTrabsFunction, 0);	
					}
				};
				
				gui.getQueriesServiceProxy().getAllRecursosProcesoConCarro(
					gui.getProceso().getId(),
					new RPCFillCheckboxGridPanel(cbgpRecCont, f));
			}
		};
		
		this.addTTrabsFunction = new Function(){
			@Override
			public void execute() {
				cbgpEvento = new CheckBoxGridPanel(true);
				Function f = new Function() {
					@Override
					public void execute() {
						ww.getEl().unmask();
						cbgpEvento.getStore().sort("evento", SortDir.ASC);
						ww.addPage(cbgpEvento, finalFunction, 1);		
					}
				};
				
				String[][] selected = cbgpRecCont.getSelectedIDs(new String[]{"id", "id_contratista"});
				rec = ClientUtils.extractColumn(selected, 0);
				cont = ClientUtils.extractColumn(selected, 1);
				ww.getEl().mask("Por favor espere...<br>Buscando Eventos...");
				gui.getQueriesServiceProxy().getEvents( rec, cont,
						fromDate, toDate,
						new RPCFillCheckboxGridPanel(cbgpEvento, f));

			}
		};
				
		this.finalFunction = new Function(){
			@Override
			public void execute() {

				Panel[] panels = gui.addStatsTab("An\u00e1lisis Comparativo");
				String[] selected = cbgpEvento.getSelectedIDs();
				
				//boolean includeOthers = selected.length < cbgpEvento.getStore().getRecords().length;
				
				gui.getQueriesServiceProxy().getCentralComparativeCrosstab(rec, cont,
						fromDate,
						toDate,selected,
						panels[0].getWidth(), panels[0].getHeight(),
						reportSubject,false,
						new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		dw = new DateWindow("Rango de fechas",false,false);
		dw.setHeight(dw.getHeight() + 30);
		
		dw.addOKListener(new ButtonListenerAdapter() {
			public void onClick(Button button, EventObject e) {
				if (!dw.getFromDate().equals("")
						&& !dw.getToDate().equals("")) {
					if (dw.getFromDateAsDate().before(dw.getToDateAsDate())
							|| (dw.getFromDateAsDate().equals(
									dw.getToDateAsDate()) && dw.getFromTime()
									.compareTo(dw.getToTime()) <= 0)) {
						toDate=dw.getToDate();
						fromDate=dw.getFromDate();
						addRecContFunction.execute();
						
					} else
						ClientUtils.alert("Error",
								"Rango de fechas inv\u00e1lido.",
								ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error",
							"Todas las opciones son requeridas.",
							ClientUtils.ERROR);
				dw.close();
				gui.showMessage("Por favor espere...",
						"Buscando veh\u00edculos...", Position.CENTER);
			}
		});

		dw.show();
	}
	
}