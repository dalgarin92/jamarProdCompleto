package org.extreme.controlweb.client.stats.listeners;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.Radio;
import com.gwtext.client.widgets.form.event.CheckboxListenerAdapter;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ManagerReportItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private String reportSubject;
	private Window wnd;
	private DateField fromDate;
	private DateField toDate;
	private MyComboBox cbMunip;
	private MyComboBox cbDato;
	private Function finalFunction;
	private Radio rdPie;
	private Radio rdBars;
	private Panel p;

	public ManagerReportItemListener(ControlWebGUI g, String rs) {
		this.gui = g;
		this.reportSubject = rs;
		
		this.finalFunction = new Function() {
			@Override
			public void execute() {
				Panel[] panels = gui.addStatsTab("Reporte Gerencial");
				gui.getQueriesServiceProxy().getManagerReport(
						cbMunip.getValue(), fromDate.getEl().getValue(),
						toDate.getEl().getValue(), cbDato.getValue(),
						panels[0].getWidth(), panels[0].getHeight(),
						reportSubject, rdPie.getValue(),
						new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		wnd = new Window("Par\u00e1metros de "
				+ "Reporte Estad\u00edstico",  350, 250, false, true);
		
		wnd.setPaddings(0);
		wnd.setLayout(new FitLayout());
		
		p = new Panel();
		p.setBorder(false);
		p.setPaddings(5);
		p.setLayout(new FormLayout());
		wnd.add(p);
		
		wnd.setButtonAlign(Position.CENTER);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		
		p.add(fromDate);
		p.add(toDate);
		
		cbMunip = new MyComboBox("Municipio");
		gui.getQueriesServiceProxy().getMunicipiosDeProceso(gui.getProceso().getId(),
				new RPCFillComplexComboHandler(cbMunip, p, 0, new Function(){
					@Override
					public void execute() {
						cbMunip.getStore().add(cbMunip.getRecordDef().createRecord(
									new String[] { "all", "Todos" }));
						cbMunip.getStore().commitChanges();
						cbMunip.setValue("all");
					}
				}));
		
		cbDato = new MyComboBox("Dato");
		
		cbDato.addListener(new ComboBoxListenerAdapter(){
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				if(!record.getAsString("id").equals("numtrabs"))
					rdBars.setValue(true);
			}
		});
		ArrayList<RowModel> rma = new ArrayList<RowModel>();
		
		RowModelAdapter rm1 = new RowModelAdapter();
		rm1.addValue("id", "numtrabs");
		rm1.addValue("nombre", "N\u00famero OTs");
		
		RowModelAdapter rm2 = new RowModelAdapter();
		rm2.addValue("id", "tiempoprom");
		rm2.addValue("nombre", "Promedio Tiempo OTs");
				
		RowModelAdapter rm3 = new RowModelAdapter();
		rm3.addValue("id", "tiempomax");
		rm3.addValue("nombre", "M\u00e1ximo Tiempo OTs");
		
		RowModelAdapter rm4 = new RowModelAdapter();
		rm4.addValue("id", "tiempomin");
		rm4.addValue("nombre", "M\u00ednimo Tiempo OTs");
		
		rma.add(rm1);
		rma.add(rm2);
		rma.add(rm3);
		rma.add(rm4);
				
		rdPie = new Radio("Pie", new CheckboxListenerAdapter(){
			
			@Override
			public void onCheck(Checkbox field, boolean checked) {
				if(checked)
					cbDato.setValue("numtrabs");
			}
		});
		rdBars = new Radio("Barras");
		
		rdPie.setName("groupChartType");
		rdBars.setName("groupChartType");
		
		wnd.addButton(new Button("Generar", new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				finalFunction.execute();
				wnd.close();
			}
		}));
		
		wnd.show();
		new RPCFillComplexComboHandler(cbDato, p, 3, new Function(){
			@Override
			public void execute() {
				rdPie.setValue(true);
				p.add(rdPie);
				p.add(rdBars);
				cbDato.setValue("numtrabs");				
			}
		}).onSuccess(rma);
	}
}
