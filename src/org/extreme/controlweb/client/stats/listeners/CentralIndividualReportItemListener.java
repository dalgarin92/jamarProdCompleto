package org.extreme.controlweb.client.stats.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class CentralIndividualReportItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private DateWindow wnd;
	private String idrec;
	private String idcont;
	private String reportSubject;


	public CentralIndividualReportItemListener(ControlWebGUI g,
			String idr, String idc, String rs) {
		this.gui = g;
		this.idrec = idr;
		this.idcont = idc;
		this.reportSubject = rs;
		
	}
		
	@Override
	public void onClick(BaseItem item, EventObject e) {
		
		
		wnd = new DateWindow("General", false, false);
		wnd.setHeight(wnd.getHeight() + 30);
		
		
		final ComboBox cbDato = ClientUtils.createCombo(
				"Dato", new SimpleStore(new String[] { "id",
				"nombre" }, new String[][] { { "km", "Kilometros" },
				{ "vp", "Velocidad Promedio" }, { "vm","Velocidad M\u00e1xima"} }));
		cbDato.setValue("km");
		cbDato.setEditable(false);
		
		wnd.getAdditionalPanel().add(cbDato);
		
		wnd.addOKListener(new ButtonListenerAdapter() {
			public void onClick(Button button, EventObject e) {
				if (!wnd.getFromDate().equals("")
						&& !wnd.getToDate().equals("")) {
					if (wnd.getFromDateAsDate().before(wnd.getToDateAsDate())
							|| (wnd.getFromDateAsDate().equals(wnd.getToDateAsDate()) 
									&& wnd.getFromDateAsDate().compareTo(wnd.getToDateAsDate())<= 0)) {
						
						Panel[] panels = gui
						.addStatsTab("An\u00e1lisis Individual");
						
						gui.getStaticQueriesServiceAsync().getIndReportCentral(
								idrec,
								idcont,
								wnd.getFromDate(),wnd.getToDate(), cbDato.getValue(),panels[0].getWidth(),
								panels[0].getHeight(), reportSubject, 
								new StatHandlerVisualizer(gui, panels[0], panels[1]));
						
						
					} else
						ClientUtils.alert("Error",
								"Rango de fechas inv\u00e1lido.",
								ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error",
							"Todas las opciones son requeridas.",
							ClientUtils.ERROR);
				wnd.close();
				gui.showMessage("Por favor espere...",
						"Generando el reporte...", Position.CENTER);
			}
		});

		wnd.show();
		
	}
}
