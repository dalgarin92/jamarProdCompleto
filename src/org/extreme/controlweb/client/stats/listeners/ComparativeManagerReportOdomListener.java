package org.extreme.controlweb.client.stats.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ComparativeManagerReportOdomListener  extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
	private String fromDate;
	private String toDate;
	private DateWindow dw;
	
	private Function finalFunction;
	
	public ComparativeManagerReportOdomListener(ControlWebGUI g) {
		this.gui = g;
		
		this.finalFunction = new Function(){
			@Override
			public void execute() {
				Panel[] panels = gui.addStatsTab("An\u00e1lisis Gerencial");
								
				gui.getQueriesServiceProxy().getManagerReportOdom(gui.getProceso().getId(), fromDate,
						toDate,
						panels[0].getWidth(), panels[0].getHeight(),
						"An\u00e1lisis Gerencial de Od\u00f3metros", new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		
		dw = new DateWindow("Rango de fechas",false,false);
		dw.setHeight(dw.getHeight() + 30);
		
		dw.addOKListener(new ButtonListenerAdapter() {
			public void onClick(Button button, EventObject e) {
				if (!dw.getFromDate().equals("")
						&& !dw.getToDate().equals("")) {
					if (dw.getFromDateAsDate().before(dw.getToDateAsDate())
							|| (dw.getFromDateAsDate().equals(
									dw.getToDateAsDate()) && dw.getFromTime()
									.compareTo(dw.getToTime()) <= 0)) {
						toDate=dw.getToDate();
						fromDate=dw.getFromDate();
						finalFunction.execute();
						
					} else
						ClientUtils.alert("Error",
								"Rango de fechas inv\u00e1lido.",
								ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error",
							"Todas las opciones son requeridas.",
							ClientUtils.ERROR);
				dw.close();
			}
		});

		dw.show();

	}
	
}
