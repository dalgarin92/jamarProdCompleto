package org.extreme.controlweb.client.stats.listeners;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.stats.gui.WizardWindow;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.StatHandlerVisualizer;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class IndividualReportItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private DateField fromDate;
	private DateField toDate;
	private MyComboBox cbMunip;
	private MyComboBox cbDato;
	private CheckBoxGridPanel cbgp;
	private WizardWindow wnd;
	private Function finalFunction;
	private String idrec;
	private String idcont;
	private String reportSubject;
	private Function addTTrabsFunction;

	public IndividualReportItemListener(ControlWebGUI g,
			String idr, String idc, String rs) {
		this.gui = g;
		this.idrec = idr;
		this.idcont = idc;
		this.reportSubject = rs;
		
		this.addTTrabsFunction = new Function(){
			@Override
			public void execute() {
				cbgp = new CheckBoxGridPanel(true);
				gui.getQueriesServiceProxy().getTTrabajoRecurso(
						fromDate.getEl().getValue(), toDate.getEl().getValue(),
						idrec != null ? new String[] { idrec } : null,
						new String[] { idcont }, cbMunip.getValue(),
						new RPCFillCheckboxGridPanel(cbgp, new Function() {
							@Override
							public void execute() {
								wnd.addPage(cbgp, finalFunction, 1);
							}
						}));
			}
		};
		
		this.finalFunction = new Function() {
			@Override
			public void execute() {
				Panel[] panels = gui
						.addStatsTab("Reporte Estad\u00edstico Individual");

				gui.getQueriesServiceProxy().getIndividualCrosstab(idrec,
						idcont, cbMunip.getValue(),
						fromDate.getEl().getValue(), toDate.getEl().getValue(),
						cbDato.getValue(), cbgp.getSelectedIDs(),
						panels[0].getWidth(), panels[0].getHeight(),
						reportSubject,
						new StatHandlerVisualizer(panels[0], panels[1]));
			}
		};
	}
		
	@Override
	public void onClick(BaseItem item, EventObject e) {
		wnd = new WizardWindow("Par\u00e1metros de "
				+ "Reporte Estad\u00edstico", 500, 500, false, true);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		Panel p = wnd.addPage("General", new FormLayout(), addTTrabsFunction);
		
		cbMunip = new MyComboBox("Municipio");
		gui.getQueriesServiceProxy().getMunicipiosDeProceso(gui.getProceso().getId(),
				new RPCFillComplexComboHandler(cbMunip, p, 0, new Function(){
					@Override
					public void execute() {
						cbMunip.getStore().add(cbMunip.getRecordDef().createRecord(
									new String[] { "all", "Todos" }));
						cbMunip.getStore().commitChanges();
						cbMunip.setValue("all");
					}
				}));
		wnd.addItemToArray(cbMunip, 0, 0);
		
		wnd.addItem(fromDate);
		wnd.addItem(toDate);
		
		cbDato = new MyComboBox("Dato");
		ArrayList<RowModel> rma = new ArrayList<RowModel>();
		
		RowModelAdapter rm1 = new RowModelAdapter();
		rm1.addValue("id", "numtrabs");
		rm1.addValue("nombre", "N\u00famero OTs");
		
		RowModelAdapter rm2 = new RowModelAdapter();
		rm2.addValue("id", "tiempoprom");
		rm2.addValue("nombre", "Promedio Tiempo OTs");
				
		RowModelAdapter rm3 = new RowModelAdapter();
		rm3.addValue("id", "tiempomax");
		rm3.addValue("nombre", "M\u00e1ximo Tiempo OTs");
		
		RowModelAdapter rm4 = new RowModelAdapter();
		rm4.addValue("id", "tiempomin");
		rm4.addValue("nombre", "M\u00ednimo Tiempo OTs");
		
		rma.add(rm1);
		rma.add(rm2);
		rma.add(rm3);
		rma.add(rm4);
		
		wnd.addItemToArray(cbDato, 3, 0);
		
		Panel p2 = wnd.addPage("Tipos de Trabajo", new RowLayout());
		p2.setPaddings(0);
		
		wnd.show();
		new RPCFillComplexComboHandler(cbDato, p, 3, new Function(){
			@Override
			public void execute() {
				cbDato.setValue("numtrabs");
			}
		}).onSuccess(rma);
	}
}
