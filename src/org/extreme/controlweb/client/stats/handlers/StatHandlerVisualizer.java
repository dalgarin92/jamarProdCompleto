package org.extreme.controlweb.client.stats.handlers;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.layout.RowLayoutData;

public class StatHandlerVisualizer implements
		AsyncCallback<ArrayList<ArrayList<RowModel>>> {
 
	private Panel chartPanel;
	private Panel gridPanel;
	private ControlWebGUI gui;

	public StatHandlerVisualizer(Panel chartPanel, Panel gridPanel) {
		this.chartPanel = chartPanel;
		this.gridPanel = gridPanel;
	}

	public StatHandlerVisualizer(ControlWebGUI gui, Panel chartPanel, Panel gridPanel) {
		this.chartPanel = chartPanel;
		this.gridPanel = gridPanel;
		this.gui = gui;
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", this.getClass().toString() + ": "
				+ caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(ArrayList<ArrayList<RowModel>> result) {
		chartPanel.add(new HTML("<img src = '" + ClientUtils.SESSION_CHART_URL
				+ "?" + new Date().getTime() + "' usemap = '#mymap'/>"
				+ result.get(1).get(0).getRowValues()[0]));
	
		if(result.get(0).size() > 0){
			MyGridPanel grid = new MyGridPanel();
			
			new RPCFillTable(grid, gridPanel, null, false, false, true, null, null,
					new RowLayoutData()).onSuccess(result.get(0));
		}
		
		chartPanel.doLayout(true);
		if(gui != null)
			gui.hideMessage();
	}
}
