package org.extreme.controlweb.client.stats.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.CheckBoxGridPanelPlaning;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;

public class RPCFillCheckboxGridPanelPlaning implements
		AsyncCallback<ArrayList<RowModel>> {

	private CheckBoxGridPanelPlaning cbgp;
	private Function f;
	private String message;
	private boolean showEmptyMessage;
	private boolean forceFunction;
	
	public RPCFillCheckboxGridPanelPlaning(CheckBoxGridPanelPlaning cbgp, Function function) {
		this.cbgp = cbgp;
		this.f = function;
		showEmptyMessage=true;
		forceFunction=false;
	}

	
	public RPCFillCheckboxGridPanelPlaning(CheckBoxGridPanelPlaning cbgp, Function function,boolean showEmptyMessage,
			boolean forceFunction) {
		this.cbgp = cbgp;
		this.f = function;
		this.showEmptyMessage=showEmptyMessage;
		this.forceFunction=forceFunction;
	}
	
	public RPCFillCheckboxGridPanelPlaning(CheckBoxGridPanelPlaning cbgp, Function function,boolean showEmptyMessage) {
		this.cbgp = cbgp;
		this.f = function;
		this.showEmptyMessage=showEmptyMessage;
		forceFunction=false;
	}

	
	public RPCFillCheckboxGridPanelPlaning( String message, CheckBoxGridPanelPlaning cbgp, Function function) {
		this.cbgp = cbgp;
		this.f = function;
		this.message = message;
		showEmptyMessage=true;
		forceFunction=false;
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", this.getClass().toString() + ": "+ caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(ArrayList<RowModel> result) {
		 if(result.size()>0){	
			if (!cbgp.isConfigured()) {
				cbgp.configure(result.get(0).getColumnNames(),
						((RowModelAdapter) result.get(0)).getSizes(), 
						((RowModelAdapter) result.get(0)).getVisibility());
			}

			cbgp.addAll(result);
		
	
		 }else{
		
			 if(showEmptyMessage&&!this.message.isEmpty()){
				ClientUtils.alert("Info",this.message, ClientUtils.INFO);	
			}				
		}
		 
		if(forceFunction||result.size() > 0){
			if (f != null) {
				f.execute();
			}
		}

	}

}
