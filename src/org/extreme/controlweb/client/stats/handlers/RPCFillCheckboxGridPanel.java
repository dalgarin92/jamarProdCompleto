package org.extreme.controlweb.client.stats.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;

public class RPCFillCheckboxGridPanel implements
		AsyncCallback<ArrayList<RowModel>> {

	private CheckBoxGridPanel cbgp;
	private Function f;

	public RPCFillCheckboxGridPanel(CheckBoxGridPanel cbgp, Function function) {
		this.cbgp = cbgp;
		this.f = function;
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", this.getClass().toString() + ": "
				+ caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(ArrayList<RowModel> result) {
		if (result.size() > 0) {
			if (!cbgp.isConfigured())
				cbgp.configure(result.get(0).getColumnNames(),
						null, ((RowModelAdapter) result.get(0)).getVisibility());

			cbgp.addAll(result);
			if (f != null)
				f.execute();
		} else
			ClientUtils.alert("Error", "No hay datos.", ClientUtils.ERROR);
		
	}

}
