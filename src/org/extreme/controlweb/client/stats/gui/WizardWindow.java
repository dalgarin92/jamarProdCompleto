package org.extreme.controlweb.client.stats.gui;

import java.util.ArrayList;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.BoxComponent;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.CardLayout;
import com.gwtext.client.widgets.layout.ContainerLayout;
import com.gwtext.client.widgets.layout.LayoutData;
import com.gwtext.client.widgets.layout.RowLayoutData;

public class WizardWindow extends Window {

	private int numPage;
	private ArrayList<Panel> panels;
	private CardLayout cardLayout;
	private ArrayList<Function> funcs;
	private ArrayList<ArrayList<BoxComponent>> items;
	private ToolbarButton backButton;
	private ToolbarButton nextButton;
	
	public WizardWindow(String title, int width, int height, boolean modal, boolean resizable) {
		super(title, width, height, modal, resizable);
		cardLayout = new CardLayout(true);
		panels = new ArrayList<Panel>();
		funcs = new ArrayList<Function>();
		items = new ArrayList<ArrayList<BoxComponent>>();

		numPage = 0;
		this.setLayout(cardLayout);
		this.setActiveItem(numPage);  
        this.setPaddings(5);
         
        Toolbar toolbar = new Toolbar();  
  
        nextButton = new ToolbarButton("<b>Siguiente</b>");
        
        backButton = new ToolbarButton("<b>Atr\u00e1s</b>");
        backButton.setDisabled(true);
        
        backButton.addListener(new ButtonListenerAdapter(){
        	@Override
        	public void onClick(Button button, EventObject e) {
        		cardLayout.setActiveItem(--numPage);
        		nextButton.setText("<b>Siguiente</b>");
        		if(numPage == 0)
        			backButton.setDisabled(true);
        	}
        });
        
        toolbar.addButton(backButton);  
        toolbar.addFill();  
       
        nextButton.addListener(new ButtonListenerAdapter(){
        	@Override
        	public void onClick(Button button, EventObject e) {
        		nextButton();
        	}
        });
        toolbar.addButton(nextButton);
        this.setBottomToolbar(toolbar);       
	}
	
		
	public void nextButton(){
   		backButton.setDisabled(false);
		boolean sw = false;
		if(numPage == panels.size() - 1){
			sw = true;
			numPage++;
		} else {
			if (numPage < panels.size()) 
				cardLayout.setActiveItem(++numPage);
			if (numPage == panels.size() - 1) 
				nextButton.setText("<b>Finalizar</b>");				
		} 
		if(funcs.get(numPage - 1) !=  null)
			funcs.get(numPage - 1).execute();
		if (sw)
			WizardWindow.this.close();
	}
	
	public Panel addPage(String title){
		return addPage(title, null, null);
	}
	
	public Panel addPage(String title, ContainerLayout cl){
		return addPage(title, cl, null);
	}
	public Panel addPage(String title, Function f){
		return addPage(title, null, f);
	}
	
	public void addPage(Panel p, Function f, int i){
		Panel oldp = panels.get(i); 
		//panels.remove(i);
		//panels.add(i, p);
		
		
		funcs.remove(i);
		funcs.add(i, f);
		
		/*this.remove(panels.get(i), true);
		this.insert(i, p);
		this.doLayout();*/
		//p.setTitle(null);
		//oldp.setLayout(new RowLayout());
		oldp.removeAll(true);
		oldp.add(p, new RowLayoutData());
		oldp.doLayout();
	}
	
	public Panel addPage(String title, ContainerLayout cl, Function f){
		Panel p = new Panel(title);
		p.setPaddings(5);
		if(cl != null)
			p.setLayout(cl);
		panels.add(p);
		funcs.add(f);
		this.add(p);
		items.add(new ArrayList<BoxComponent>());
		return p;
	}
	
	public void addItem(BoxComponent item){
		addItemAtPage(item, null, panels.size() - 1);
	}
	
	public void addItem(BoxComponent item, LayoutData ld){
		addItemAtPage(item, ld, panels.size() - 1);
	}
	
	public void addItemAtPage(BoxComponent item, LayoutData ld, int i){ 
		if (i < panels.size()) {
			if (ld != null)
				panels.get(i).add(item, ld);
			else
				panels.get(i).add(item);

			items.get(i).add(item);
		} else
			throw new IllegalArgumentException();
	}
	
	public void addItemAtPageInPosition(BoxComponent item, int position, int page){
		if(page < panels.size()){
			panels.get(page).insert(position, item);
			items.get(page).add(position, item);
		} else
			throw new IllegalArgumentException();
	}
	
	public void addItemToArray(BoxComponent item, int position, int page){
		items.get(page).add(position, item);
	}
	
	public void addItemToArray(BoxComponent item, int page){
		items.get(page).add(item);
	}
	
	public ArrayList<BoxComponent> getItemsAtPage(int i){
		return items.get(i);
	}
	
	public String[] getValues(){
		int size = 0;
		for (ArrayList<BoxComponent> page : items) 
			size += page.size();
		String[] res = new String[size];
		
		int i = 0;
		for (ArrayList<BoxComponent> page : items) {
			for (BoxComponent bc : page) {
				if(bc instanceof ComboBox)
					res[i] = ((ComboBox)bc).getValue();
				else if(bc instanceof NumberField)
					res[i] = ((NumberField)bc).getValueAsString();
				else if(bc instanceof TextField)
					res[i] = ((TextField)bc).getValueAsString();
				else if(bc instanceof Checkbox)
					res[i] = ((Checkbox)bc).getValue() ? "S" : "N";
				if(res[i] != null)
					i++;
			}
		}
		return res;
	}
	
	@Override
	public void show() {
		super.show();
		if (panels.size() == 1) {
			numPage--;
			nextButton();
			disableBack();
		}
//		if(panels.size() == 1){
//			nextButton.setDisabled(true);
//		}
	}
	
	public void disableBack(){
		backButton.setDisabled(true);
	}
}
