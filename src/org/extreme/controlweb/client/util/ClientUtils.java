package org.extreme.controlweb.client.util;

import org.extreme.controlweb.client.core.RowModel;
import org.gwtopenmaps.openlayers.client.LonLat;

import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.Component;
import com.gwtext.client.widgets.MessageBox;
import com.gwtext.client.widgets.MessageBoxConfig;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.grid.CellMetadata;
import com.gwtext.client.widgets.grid.Renderer;
import com.gwtext.client.widgets.layout.BorderLayoutData;

public class ClientUtils {

	public static final int HOSTED_MODE = 1;
	public static final int DEPLOYED_BQ = 2;
	public static final int DEPLOYED_BTA = 3;
	public static final int DEPLOYED_BTA_LAN = 4;

	public static final String APP_NAME = "Extreme Control Jamar";
	public static final int DEFAULT_REFRESH_RATE = 180000;
	public static final int DELAY2 = 3000;
	public static final int MINI_DELAY = 1000;
	public static final int CENTRAL_REFRESH_RATE = 60000;

	public static String URL_PATH;
	public static String REPORT_EXPORTER_SERVLET;
	public static String CONTEXT;
	public static String SESSION_CHART_URL;
	public static String SERVICES_URL;

	public static final String P_MANEJA_SUPERVISORES = "MSV";

	//public static final int CURRENT_ENVIRONMENT = HOSTED_MODE;
	public static final int CURRENT_ENVIRONMENT = DEPLOYED_BQ;
	//public static final int CURRENT_ENVIRONMENT = DEPLOYED_BTA;
	//public static final int CURRENT_ENVIRONMENT = DEPLOYED_BTA_LAN;
	public static void initialize(int env) {
		String contexto = "xcontroljamar/";
		switch (env) {
			/*case HOSTED_MODE:
				CONTEXT = "";
				URL_PATH = "http://192.168.64.88:8888/"+CONTEXT;
				break;*/
			case HOSTED_MODE:
				CONTEXT = "";
				URL_PATH = "http://127.0.0.1:8888/" + CONTEXT;
				break;
			case DEPLOYED_BQ:
				CONTEXT = contexto;
				URL_PATH = "http://localdev.extreme.com.co/" + CONTEXT;
				break;
			case DEPLOYED_BTA:
				CONTEXT = contexto;
				URL_PATH = "http://190.144.145.150:8083/" + CONTEXT;
				break;
			case DEPLOYED_BTA_LAN:
				CONTEXT = "xcontroljamar2/";
				URL_PATH = "http://192.168.70.15:8083/" + CONTEXT;
				break;
		}
		SERVICES_URL = URL_PATH + "services";
		REPORT_EXPORTER_SERVLET = URL_PATH + "servlet/ReportExporter";
		SESSION_CHART_URL = URL_PATH + "servlet/SessionChart";
		setLinkHref("theme", "themes/silverCherry/css/xtheme-silverCherry.css");
	}

	public static final String ERROR = MessageBox.ERROR;
	public static final String WARNING = MessageBox.WARNING;
	public static final String QUESTION = MessageBox.QUESTION;
	public static final String INFO = MessageBox.INFO;

	public static final String USER_POI = "U";
	public static final String PROCESS_POI = "P";

	public static final RecordDef RECORD_DEF_COMBOS = new RecordDef(
			new FieldDef[] { new StringFieldDef("id"),
					new StringFieldDef("nombre") });

	public static final RecordDef RECORD_DEF_STYLED_COMBOS = new RecordDef(
			new FieldDef[] { new StringFieldDef("id"),
					new StringFieldDef("nombre"),
					new StringFieldDef("icono")});

	public static RecordDef createRecordDef(RowModel rma) {
		FieldDef[] fds = new FieldDef[rma.getColumnNames().length];
		for (int i = 0; i < rma.getColumnNames().length; i++) {
			fds[i] = new StringFieldDef(rma.getColumnNames()[i]);
		}
		RecordDef res = new RecordDef(fds);
		return res;
	}

	private static final String HTML_MODIFY = "<img class=\"mod\" title=\"Modificar registro\" src=\"images/utiles/icons/edit.png\"/>";
	private static final String HTML_DELETE = "<img class=\"elim\" title=\"Eliminar registro\" src=\"images/utiles/icons/delete.png\"/>";
	//public static final GLatLng CITY_CENTER = new GLatLng(10.968831228257736, -74.79629516601562);
	//public static final int DEFAULT_ZOOM = 12;
	public static final LonLat DEFAULT_CENTER = new LonLat(-72.4658203125, 4.017699464336852);
	public static final int DEFAULT_ZOOM = 6;

	public static final String INFOWINDOW_FONT = "<font face='Verdana, Arial, Helvetica, sans-serif' size='-4'>";
	public static final BorderLayoutData CENTER_LAYOUT_DATA = new BorderLayoutData(
			RegionPosition.CENTER);

	public static final Renderer MODIFY_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			return HTML_MODIFY;
		}

	};

	public static final Renderer DELETE_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			return HTML_DELETE;
		}

	};

	public static final Renderer getDetailsRenderer(String string){
		final String s = string;
		return new Renderer() {
			@Override
			public String render(Object value, CellMetadata cellMetadata,
					Record record, int rowIndex, int colNum, Store store) {
				return "<img class=\"attach\" title=\"" + s + "\" src=\"images/utiles/icons/open_16.png\"/>";
			}
		};
	}


	public static final double DEGREES_PER_RADIAN = 180.0d / Math.PI;
	public static final String EVENTOSDESC_XML_TAG = "evento";
	public static final String[] EVENTOSDESC_XML_FIELDS = new String[]{"checked", "id", "nombre"};

	public static final RecordDef RECORD_DEF_EVENTOS = new RecordDef(
			new FieldDef[] { new StringFieldDef("placa"), new StringFieldDef("lat"),
					new StringFieldDef("lon"), new StringFieldDef("fecha"),
					new StringFieldDef("vel"), new StringFieldDef("dir"),
					new StringFieldDef("munip"), new StringFieldDef("depto"),
					new StringFieldDef("nomevento") });
	public static final String[] EVENTOS_XML_FIELDS = new String[] { "placa", "lat",
		"lon", "fecha", "vel", "dir", "munip", "depto", "nomevento" };
	public static final String EVENTOS_XML_TAG = "point";
	public static final String POSACTUAL_XML_TAG = "posactual";
	public static final String GEOCERCA_XML_TAG = "geocerca";
	public static final RecordDef RECORD_DEF_ODOMETER = new RecordDef(
			new FieldDef[] { new StringFieldDef("fecha"),
					new StringFieldDef("valor") });
	public static final String[] ODOMETER_XML_FIELDS = new String[] { "fecha", "valor"};
	public static final String ODOMETER_XML_TAG = "odometro";
	public static final RecordDef RECORD_DEF_RECS_NO_VEH = new RecordDef(
			new FieldDef[] { new StringFieldDef("idrec"),
					new StringFieldDef("idcont"), new StringFieldDef("nombre") });


	public static final RecordDef RECORD_DEF_GEOCERCAS = new RecordDef(
			new FieldDef[] {
					new StringFieldDef("numgeo"), new StringFieldDef("desc"),
					new StringFieldDef("rad"), new StringFieldDef("lat"),
					new StringFieldDef("lon")
			});
	public static final String[] ADM_GEOCERCA_XML_FIELDS = new String[]{
		"num", "desc", "rad", "lat", "lon"
	};
	public static final int TREE_LOAD_TIMEOUT = /*90000*/90000000;

	public static Renderer YES_NO_RENDERER = new Renderer() {
		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			return ((String) value).equals("S")  || ((String) value).equals("Y") ? "SI" : "NO";
		}
	};

	public static void alert(String title, String message, String iconCls) {
		MessageBoxConfig conf = new MessageBoxConfig();
		conf.setIconCls(iconCls);
		conf.setButtons(MessageBox.OK);
		conf.setTitle(title);
		conf.setMsg(message);
		//MessageBox.getDialog().setIconCls("icon-butterfly");
		MessageBox.show(conf);
	}

	public static Component getSeparator() {
		TextField blank = new TextField();
		blank.setVisible(false);
		blank.setHideLabel(true);
		return blank;
	}

	public static ComboBox createCombo(String label, Store store){
		ComboBox combo = new ComboBox(label);
		combo.setStore(store);
		combo.setValueField("id");
		combo.setDisplayField("nombre");
		combo.setForceSelection(true);
		combo.setMinChars(1);
		combo.setTypeAhead(true);
		combo.setSelectOnFocus(true);
		combo.setMode(ComboBox.LOCAL);
		combo.setTriggerAction(ComboBox.ALL);
		return combo;
	}

	public static Store readList(NodeList nodes){
		Store res = new Store(ClientUtils.RECORD_DEF_COMBOS);
		for (int i = 0; i < nodes.getLength(); i++) {
			Node item = nodes.item(i);
			if(item.getNodeType() == Node.ELEMENT_NODE) {
				String id = item.getAttributes().getNamedItem("id").getNodeValue();
				String nombre = item.getAttributes().getNamedItem("nombre").getNodeValue();
				res.add(ClientUtils.RECORD_DEF_COMBOS.createRecord(new String[] {
						id, nombre }));
			}
		}
		res.commitChanges();
		return res;
	}
	/**
	 * Metodo nativo JavaScript que se utiliza para cambiar el link de un
	 * elemento obteniendolo por medio del id
	 * 
	 * @param linkElementId
	 *            id del elemento de la pagina
	 * @param url
	 *            direccion de donde se encuentra el elemento
	 * 
	 * @see ClientUtils#initialize()
	 * 
	 * @author ealtamar
	 **/
	public static native void setLinkHref(String linkElementId, String url) /*-{

		var link = $doc.getElementById(linkElementId);

		if (link != null && link != undefined) {
			link.href = url;
		}
	}-*/;

	public static double trunc(double n) {
		String num = Double.toString(n);
		return Double.parseDouble(num.substring(0, num.indexOf(".") + 1)
				+ num.substring(num.indexOf(".") + 1).substring(0, 10));
	}

	public static String[] extractColumn(String[][] data, int colIndex){
		String[] res = new String[data.length];
		for (int i = 0; i < data.length; i++) {
			res[i] = data[i][colIndex];
		}
		return res;
	}

	public static String lpad(String valueToPad, String filler, int size) {
		char[] array = new char[size];

		int len = size;

		if(valueToPad.length()>size){
			len = size - valueToPad.substring(0,6).length();
		}else{
			len = size - valueToPad.length();
		}

		for (int i = 0; i < (len>0?len:size); i++) {
			array[i] = filler.charAt(0);
		}
		if(len>0) {
			valueToPad.getChars(0, valueToPad.length(), array, len);
		}
		return String.valueOf(array);
	}

	public static void showError(String message){
		alert("Error",  message, ERROR);
	}
	public static void showInfo(String message){
		alert("Info",  message, INFO);
	}

	public static double distance(double lat1, double lon1, double lat2,
			double lon2) {
		double a = 6378137, b = 6356752.3142, f = 1 / 298.257223563; // WGS-84
		// ellipsoid
		double L = toRad(lon2 - lon1);
		double U1 = Math.atan((1 - f) * Math.tan(toRad(lat1)));
		double U2 = Math.atan((1 - f) * Math.tan(toRad(lat2)));
		double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

		double lambda = L, lambdaP = 2 * Math.PI;
		double iterLimit = 20;
		double sinLambda, sinSigma = 0, cosLambda, cosSigma = 0, sigma = 0, sinAlpha, cosSqAlpha = 0, cos2SigmaM = 0, C;
		while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0) {
			sinLambda = Math.sin(lambda);
			cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt(cosU2 * sinLambda * (cosU2 * sinLambda)
					+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
					* (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
			if (sinSigma == 0) {
				return 0; // co-incident points
			}
			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
			if (cos2SigmaM == Double.NaN) {
				cos2SigmaM = 0; // equatorial line: cosSqAlpha = 0
			}
			C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = L
					+ (1 - C)
					* f
					* sinAlpha
					* (sigma + C
							* sinSigma
							* (cos2SigmaM + C * cosSigma
									* (-1 + 2 * cos2SigmaM * cos2SigmaM)));
		}
		if (iterLimit == 0) {
			return Double.NaN; // formula failed to converge
		}

		double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSq / 16384
				* (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		double deltaSigma = B
				* sinSigma
				* (cos2SigmaM + B
						/ 4
						* (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B
								/ 6 * cos2SigmaM
								* (-3 + 4 * sinSigma * sinSigma)
								* (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		double s = b * A * (sigma - deltaSigma);

		return s;
	}

	private static double toRad(double deg) { // convert degrees to radians
		return deg * Math.PI / 180;
	}

}