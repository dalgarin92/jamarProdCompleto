package org.extreme.controlweb.client.services;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AdminQueriesServiceAsync {

	void addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String conductor, String placa,
			AsyncCallback<Boolean> response);

	public void delTrabajo(String trabajo, String tipotrabajo,
			AsyncCallback<Boolean> response);

	public void updateTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String usuario, AsyncCallback<Boolean> response);

	public void delVehiculo(String id, AsyncCallback<Boolean> response);

	public void addVehiculo(String idcont, String idrec, String idveh,
			String placa, String tipo, String model, String marca,
			String codprop, String tipomodem, String imei, String min,
			String puk, String activo, AsyncCallback<Boolean> response);
	public void addContacto(String idproceso, String nombre,
			String fijo, String fijo2, String celular,
			String celular2, String email,String parent,
			AsyncCallback<Boolean> result);

	public void updateContacto(String idContacto, String nombre,
			String fijo,String fijo2, String celular,
			String celular2, String email, String parent,
			AsyncCallback<Boolean> result);

	public void addVehiculo(String idvehiculo, String placa,
			String recurso, String contratista,
			AsyncCallback<Boolean> result);

	public void addVehiculo(String idvehiculo, String geocerca, String placa,
			String recurso, String contratista,
			AsyncCallback<Boolean> result);

	public void addContactoVehiculo(String idvehiculo, String valueAsString,
			AsyncCallback<Boolean> result);
	public void delPermiso(String idusuario, String idproceso,
			AsyncCallback<Boolean> result);

	public void addPermiso(String idusuario, String idproceso,
			AsyncCallback<Boolean> result);

	public void addUser(String id, String nombre, String pass, String cargo,
			String idperfil, AsyncCallback<Boolean> result);

	public void updateUser(String id, String nombre, String pass, String cargo,
			String idperfil, AsyncCallback<Boolean> result);

	public void updateDriver(String id, String nombre, String cedula, String celular
			, AsyncCallback<Boolean> result);

	public void delUser(String id, AsyncCallback<Boolean> result);

	public void deleteClient(String id, AsyncCallback<Boolean> result);

	public void deleteDriver(String id, AsyncCallback<Boolean> result);

	public void addClient(String id, String nombre, String direccion,
			String municipio, String telefono, AsyncCallback<Boolean> result);
	public void  addDriver(String id, String nombre, String cedula, String celular, AsyncCallback<Boolean> result) ;
	public void delContratista(String asString, AsyncCallback<Boolean> result);

	public void addContratista(String idContratista, String nombreContratista,
			AsyncCallback<Boolean> result);

	public void updateContratista(String id, String nombre,
			AsyncCallback<Boolean> result);

	public void delMunicipio(String id, AsyncCallback<Boolean> result);

	public void addMunicipio(String id, String nombre,
			AsyncCallback<Boolean> result);

	public void updateMunicipio(String valueAsString, String valueAsString2,
			AsyncCallback<Boolean> result);

	public void addMunipTipoTrabajo(String munip, String ttrab,
			AsyncCallback<Boolean> result);

	public void delMunipTipoTrabajo(String munip, String ttrab,
			AsyncCallback<Boolean> result);

	public void delMunipTTrabajoContratista(String munip, String ttrab,
			String cont, AsyncCallback<Boolean> result);

	public void addMunipTTrabajoContratista(String munip, String ttrab,
			String cont, AsyncCallback<Boolean> result);

	public void delTTrabajo(String asString, AsyncCallback<Boolean> result);

	public void addTipoTrabajo(String id, String nombre, boolean reporta,
			AsyncCallback<Boolean> result);

	public void updateTipoTrabajo(String id, String nombre, boolean reporta,
			AsyncCallback<Boolean> result);

	public void deleteMRechazo(String id, AsyncCallback<Boolean> result);

	public void deleteRegProg(String asString, String asString2,
			AsyncCallback<Boolean> result);

	public void deleteMuelle(String asString,
			AsyncCallback<Boolean> result);

	public void deleteDestino(String asString,
			AsyncCallback<Boolean> result);


	public void addRegFlota(String valueAsString, String valueAsString2,
			String valueAsString3, String valueAsString4,
			String valueAsString5, String valueAsString6,
			String valueAsString7, String valueAsString8, String observacion,
			AsyncCallback<Boolean> result);

	public void addMuelle(String nombre,  String desc,
			AsyncCallback<Boolean> result);

	public void addDestino(String nombre,  String desc,
			AsyncCallback<Boolean> result);


	public void addMRechazo(String id, String nombre,
			AsyncCallback<Boolean> result);


	public void updateMuelle(String muelle, String desc,
			AsyncCallback<Boolean> result);

	public void updateDestino(String muelle, String desc,
			AsyncCallback<Boolean> result);

	public void updateCaractColumnas(String campo, String h, String w,
			AsyncCallback<Boolean> result);

	public void updateCaractPantalla(String tipoletra, String tamletra, String sub, String negrita, String cursiva,
			AsyncCallback<Boolean> result);


	public void updateMRechazo(String id, String nombre,
			AsyncCallback<Boolean> result);

	public void addTRecurso(String id, String nombre, String capacidad,
			AsyncCallback<Boolean> result);

	public void updateTRecurso(String id, String nombre, String capacidad,
			AsyncCallback<Boolean> result);

	public void delTRecurso(String id, AsyncCallback<Boolean> result);

	public void deleteRecurso(String id, String cont,
			AsyncCallback<Boolean> result);

	public void addTTrabajoProceso(String idproceso, String idttrab,
			AsyncCallback<Boolean> result);

	public void delProcesoTTrabajo(String proceso, String ttrab,
			AsyncCallback<Boolean> result);

	public void delProcesoTRecurso(String proceso, String trecurso,
			AsyncCallback<Boolean> result);

	public void addProcesoTRecurso(String proceso, String trecurso,
			AsyncCallback<Boolean> result);

	public void addProcesoMunicipio(String asString, String value,
			AsyncCallback<Boolean> result);

	public void delProcesoMunicipio(String proceso, String municipio,
			AsyncCallback<Boolean> result);

	public void delProcesoContratista(String proceso, String contratista,
			AsyncCallback<Boolean> result);

	public void addProcesoContratista(String proceso, String contratista,
			AsyncCallback<Boolean> result);

	public void updateProceso(String proceso, String nombre, boolean turnos,
			boolean contratistas, boolean central, int blueFlag,
			int orangeFlag, int redFlag, AsyncCallback<Boolean> result);

	public void addProceso(String proceso, String nombre,
			AsyncCallback<Boolean> result);

	public void addOpcionesCategoria(String categoria, String idOpcion,
			String nombreOpcion, AsyncCallback<Boolean> result);

	public void addCategoriaLista(String nomcategoria, String tipo,
			AsyncCallback<Boolean> result);

	public void deleteOpcionCategoria(String opcion,
			AsyncCallback<Boolean> result);

	public void deleteItemsPrecierres(String item, AsyncCallback<Boolean> result);

	public void addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, int orden, AsyncCallback<Boolean> result);

	public void deleteCampoPrecierres(String idcampoprecierre,
			AsyncCallback<Boolean> result);

	public void addCampoPrecierreLista(String idItemPrecierre,
			String nombreCampoPrecierre, String idcat, String tipo, int orden,
			AsyncCallback<Boolean> result);

	public void updateCampoPrecierreLista(String idCampo, String nombre,
			int orden, AsyncCallback<Boolean> result);

	public void updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, int orden, AsyncCallback<Boolean> result);

	public void delCategoriaPrecierre(String idCategoria,
			AsyncCallback<Boolean> result);

	public void updateCategoriaPrecierre(String idCategoria,
			String nomCategoria, String tipo, AsyncCallback<Boolean> result);

	public void updateOpcionesCategoria(String id, String nombre,
			AsyncCallback<Boolean> result);

	public void reorderOpcionesCategoria(String asString, String asString2,
			AsyncCallback<Boolean> result);

	public void addCampoTemplate(String template, String columna,
			String campo, AsyncCallback<Boolean> result);

	public void updateCampoTemplate(String id, String columna, AsyncCallback<Boolean> result);

	public void delCampoTemplate(String id, AsyncCallback<Boolean> result);

	public void addTemplate(String archivo, String nombre, String formato,
			String proceso, AsyncCallback<Boolean> result);

	public void updateTemplate(String asString, String valueAsString,
			String value, String id,
			AsyncCallback<Boolean> result);

	public void delTemplate(String asString, AsyncCallback<Boolean> result);

	public void addValorCampoTemplate(String iddetalle, String invalue,
			String outvalue, String label, AsyncCallback<Boolean> result);

	public void delValorCampoTemplate(String asString,AsyncCallback<Boolean> result);

	public void updateInsurance(String idvehiculo, String placa, String tipo,
			String marca, String ciudad, String colorCarroceria, String modelo,
			String tipoCarroceria, String colorCabina, boolean asegurado,
			String aseguradora, String tel_emergencia, String fechaPoliza,
			String vigenciaPoliza, String poliza, boolean estado,
			String tipo_modem, String min, String puk, String imei,
			String propietario, String modemUser, String modemPasswd, String geocerca,
			AsyncCallback<Boolean> myBooleanAsyncCallback);


	public void delContactoVehiculo(String idvehiculo, String idcontacto,
			AsyncCallback<Boolean> result);

	public void addAlarma(String idvehiculo, String evento, String alarma, String gpio,
			AsyncCallback<Boolean> myBooleanAsyncCallback);

	public void delAlarma(String idvehiculo, String asString,
			AsyncCallback<Boolean> myBooleanAsyncCallback);

	public void updateRegFlota(String fecha2, String valueAsString,
			String valueAsString2, String valueAsString3,
			String valueAsString4, String valueAsString5,
			String valueAsString6, String valueAsString7, String observacion,
			AsyncCallback<Boolean> myBooleanAsyncCallback);

	public void ActDatosGeocercas(AsyncCallback<Boolean> result);

	public void addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String placa,
			AsyncCallback<Boolean> response);

	public void updateResourceHour(String hourCode, String resource,
			String contractor, AsyncCallback<Boolean> callback);

	public void addResourceJob(ArrayList<RowModel> rm, String date, String resource,
			String contractor, String user, boolean sendMessage,
			AsyncCallback<Boolean> callback);

	public void deleteHorario(String codigo, AsyncCallback<Boolean> callback);

	public void delJornadasHorarios(String horario, String idjornada,
			AsyncCallback<Boolean> callback);

	public void addJornadasHorarios(String idjornada, String horario,
			AsyncCallback<Boolean> callback);

	public void addHorario(String codigo, String nombre,
			AsyncCallback<Boolean> callback);

	public void updateHorario(String codigo, String nombre,
			AsyncCallback<Boolean> callback);

	public void deleteJornada(String idjornada, AsyncCallback<Boolean> callback);

	public void addJornada(String codigo, String nombre, String horainicio,
			String horafin, AsyncCallback<Boolean> callback);

	public void updateJornada(String idjornada, String codigo, String nombre,
			String horainicio, String horafin, AsyncCallback<Boolean> callback);


	public void addRecurso(String id, String cont, String trec, String nombre,
			String usuario, String clave, String codigosap, String horario,
			AsyncCallback<Boolean> result);

	public void updateRecurso(String id, String cont, String trec, String nombre,
			String usuario, String clave, String codiosap, String horario,
			AsyncCallback<Boolean> callback);

	public void deleteItemsPrecierres(String item, String ttrab,
			AsyncCallback<Boolean> callback);

	public void deleteCampoPrecierres(String item, String idcampoprecierre,
			AsyncCallback<Boolean> callback);

	public void updateCampoPrecierreLista(String idCampo, String idItemPrecierre,
			String nombre, String codigo, int orden,
			AsyncCallback<Boolean> callback);

	public void addCampoItem(String campo, String item, int orden,
			AsyncCallback<Boolean> callback);

	public void addCampoPrecierreLista(String idItemPrecierre, String codigo,
			String nombreCampoPrecierre, String idcat, String tipo, int orden,
			AsyncCallback<Boolean> callback);

	public void addTTrabajoItem(String tipotrabajo, String item, int orden,
			AsyncCallback<Boolean> callback);

	public void addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, String repeticion, int orden,
			AsyncCallback<Boolean> callback);

	public void updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, String repeticion, int orden, String ttrab,
			AsyncCallback<Boolean> callback);

	public void updateItemPrecierreOrden(String idItem, int orden, String ttrab,
			AsyncCallback<Boolean> callback);

	public void updateMessage(String subject, String from, String to,
			String dateDelivery, String jobDetails, String timeTolerance, int idMesaage,
			AsyncCallback<Boolean> callback);

	public void addMessage(String subject, String from, String to,
			String dateDelivery, String jobDetails, String timeTolerance,
			AsyncCallback<String> callback);

	public void updateMessageJobsType(String jobType, int idMesaage,
			AsyncCallback<Boolean> callback);

	public void addTipoTrabajo(String id, String nombre, boolean reporta,
			String maxduracion, String tiempopromedio,
			AsyncCallback<Boolean> callback);

	public void updateTipoTrabajo(String id, String nombre, boolean reporta,
			String maxduracion, String tiempopromedio,
			AsyncCallback<Boolean> callback);

	public void closeWorkAndAddJamarResult(String trabajo, String tipotrabajo, String calificacion, String causal,
			String fuente, String usuario, AsyncCallback<Boolean> callback);



}
