package org.extreme.controlweb.client.services;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.GroupedRowModelAdapter;
import org.extreme.controlweb.client.core.Horario;
import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.ProcesoLight;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.SimpleEntity;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface QueriesServiceAsync {
	@Deprecated
	public void setSessionVars(String[] vars, AsyncCallback<String> cb);

	@Deprecated
	public void getEmpresas(AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void login(String user, String pass, String codemp,
			AsyncCallback<ArrayList<String>> cb);

	public void getProcesos(String idusuario,
			AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getProcesosAsRowModel(String idusuario,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getProcesosAsRowModel(AsyncCallback<ArrayList<RowModel>> cb);

	public void getProcesosAsSimpleEntity(
			AsyncCallback<ArrayList<SimpleEntity>> cb);


	public void getProcesoMaqTime(String proceso, boolean b, String value,
			AsyncCallback<Proceso> cb);

	public void getInfoRecursosMaqTime(ArrayList<String[]> checkedRecs,
			String id, boolean soloCentral, boolean conTurnos, String value,
			AsyncCallback<ProcesoLight> cb);



	public void updateRecurso(String id, String idCont, String idProc,
			boolean soloCentral, String fecha, AsyncCallback<Recurso> cb);

	public void getTiposTrabajo(String idproceso,
			AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getTiposTrabajoAsRowModel(String idproceso,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getMunicipios(AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getMunicipiosRowModel(AsyncCallback<ArrayList<RowModel>> cb);

	public void getTiposRecurso(String idproceso,
			AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getContratistas(String idproceso,
			AsyncCallback<ArrayList<SimpleEntity>> cb);


	public void getContratistasAsRowModel(
			AsyncCallback<ArrayList<RowModel>> gridFillHandler);

	public void getPropietarios(AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getTiposModem(AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getPerfiles(AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void getRecursosSinVehiculo(String idproceso,
			AsyncCallback<ArrayList<SimpleEntity>> cb);

	public void assignJob(String trabajo, String tipotrabajo, String recurso,
			String contratista, String usuario, AsyncCallback<Boolean> cb);

	public void closeJob(String trabajo, String tipotrabajo, String idusuario,
			AsyncCallback<Boolean> cb);

	public void locateJobAddress(String dir, String munip, String lat,
			String lon, String trabajo, String tipotrabajo,
			AsyncCallback<Boolean> cb);

	public void setPathStart(String trabajo, String tipotrabajo,
			String recurso, String contratista, boolean noPath,
			AsyncCallback<Boolean> cb);

	public void resetJob(String trabajo, String tipotrabajo, String idusuario,
			AsyncCallback<Boolean> cb);

	public void getAddressCoords(String direccion, String munip,
			AsyncCallback<LocalizableObject> cb);

	public void setJobPriority(String trabajo, String tipotrabajo,
			String prioridad, AsyncCallback<Boolean> cb);

	public void setResourceMode(String recurso, String contratista,
			String online, AsyncCallback<Boolean> cb);

	public void getGeocercas(String recurso, String contratista,
			AsyncCallback<ArrayList<RowModel>> cb);



	public void createGeofence(String idvehiculo, int radio, double latitud,
			double longitud, String nombre, AsyncCallback<Boolean> cb);

	public void deactivateGeofence(String idvehiculo, String numgeocerca,
			AsyncCallback<Boolean> cb);

	public void createPtoInt(String id, String tipo, String latitud,
			String longitud, String desc, String direccion, String municipio,
			String departamento, String icono, AsyncCallback<Boolean> result);

	public void getPtoInt(String idusuario, String idproceso,
			AsyncCallback<ArrayList<RowModel>> puntosinteres);

	public void getPtoIntUsuario(String idusuario,
			AsyncCallback<ArrayList<LocalizableObject>> puntosinteres);

	public void getPtoIntProceso(String idproceso,
			AsyncCallback<ArrayList<LocalizableObject>> puntosinteres);

	public void getIconos(AsyncCallback<ArrayList<SimpleEntity>> iconos);

	public void getDireccion(String direccion, String municipio,
			AsyncCallback<LocalizableObject> d);

	public void deletePtoInt(String idPtoInt,
			AsyncCallback<Boolean> asyncCallback);

	public void getTrabajos(String idproceso,
			AsyncCallback<ArrayList<RowModel>> trabajos);

	public void getVehiculos(String proceso,
			AsyncCallback<ArrayList<RowModel>> vehiculos);

	public void getInfoTrabajosPrecerrados(String proceso, String fechaDesde,
			String fechaHasta, String ttrab, String estadoB, boolean fCreacion,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getInfoTrabajosPrecerrados(String proceso, String fechaDesde,
			String fechaHasta, String recurso, String contratista,
			String ttrab, AsyncCallback<ArrayList<RowModel>> cb);

	public void getInfoTrabajoPrecierre(String trabajo, String tipoTrabajo,
			AsyncCallback<ArrayList<GroupedRowModelAdapter>> result);

	public void getInfoBitacora(String trabajo, String tipoTrabajo,
			AsyncCallback<ArrayList<GroupedRowModelAdapter>> cb);

	public void getUsersInfo(String admin,
			AsyncCallback<ArrayList<RowModel>> result);

	public void sendOtaPosActual(String recurso, String contratista,
			AsyncCallback<String> respuesta);

	public void getInfoClientes(AsyncCallback<ArrayList<RowModel>> result);

	public void getInfoDrivers(AsyncCallback<ArrayList<RowModel>> result);

	public void getTTrabajoMunicipio(String idmunip,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getAllTTrabajos(AsyncCallback<ArrayList<SimpleEntity>> result);

	public void getAllTTrabajosAsRowModel(
			AsyncCallback<ArrayList<RowModel>> result);

	public void getAllContratistas(AsyncCallback<ArrayList<SimpleEntity>> result);

	public void getContratistasMunicipio(String idmunip, String ttrab,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getAllMRechazo(AsyncCallback<ArrayList<RowModel>> result);

	public void getAllProgramaciondeFlota(AsyncCallback<ArrayList<RowModel>> result);
	public void getAllMuelles(AsyncCallback<ArrayList<RowModel>> result);

	public void getAllDestinos(AsyncCallback<ArrayList<RowModel>> result);

	public void getAllProgramaciondeFlotaFiltro(String fecha2,
			String valueAsString, String valueAsString2, String valueAsString3,
			String valueAsString4, String valueAsString5,
			String valueAsString6, String valueAsString7,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getControldeFlotasGeneral(String fecha2,
			AsyncCallback<ArrayList<RowModel>> result);

	public void registrarMovimiento(String valueAsString,
			String valueAsString2, String valueAsString3,
			AsyncCallback<ArrayList<RowModel>> result);


	public void regMovi(String valueAsString,
			String valueAsString2, String valueAsString3,
			AsyncCallback<String> respuesta);

	public void  obtenerCampo(String valueAsString,
			String valueAsString2,
			AsyncCallback<String> respuesta);
	public void obtenerValores(AsyncCallback<String[]> respuest);


	public void getAllTiposRecurso(AsyncCallback<ArrayList<RowModel>> result);

	public void getAllRecursosProceso(String idproceso,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getProcesoTRecursos(String asString,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getProcesoMunicipios(String asString,
			AsyncCallback<ArrayList<RowModel>> result);

	public void getAllTiposRecursoAsSimpleEntity(
			AsyncCallback<ArrayList<SimpleEntity>> rows);

	public void getContratistasAsRowModel(String proceso,
			AsyncCallback<ArrayList<RowModel>> rows);

	public void getAllCategorias(AsyncCallback<ArrayList<RowModel>> result);

	public void getOpcionesCategoria(String categoria,
			AsyncCallback<ArrayList<RowModel>> rows);

	public void getItemsPrecierre(String asString,
			AsyncCallback<ArrayList<RowModel>> rows);

	public void getCamposPrecierre(String asString,
			AsyncCallback<ArrayList<RowModel>> rows);

	public void getOdometro(String recurso, String contratista, String desde,
			String hasta, AsyncCallback<ArrayList<RowModel>> cb);

	public void getEventosDesc(AsyncCallback<ArrayList<RowModel>> cb);

	public void getAllRecursosProcesoConCarro(String idproceso, AsyncCallback<ArrayList<RowModel>> cb);

	public void GeocercasDeRecurso(String rec, String cont,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getInfoGeocercas(String rec, String cont, String fromDate,
			String toDate, ArrayList<String> geos,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getPosActualVehiculos(String proceso, AsyncCallback<ArrayList<RowModel>> cb);

	public void getInfoEventos(String idrec, String idcont, String fromDate,
			String toDate, ArrayList<String> events,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getOTACommands(String recurso, String contratista,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void sendOTAPredef(String recurso, String contratista, String idota,
			AsyncCallback<String> otaCallback);

	public void sendOTACustom(String recurso, String contratista, String atCommand,
			AsyncCallback<String> otaCallback);


	public void getInfoJAMAR(String idTrabajo, String idTipoTrabajo,
			AsyncCallback<ArrayList<GroupedRowModelAdapter>> asyncCallback);

	public void getMunicipiosDeProceso(String proceso, AsyncCallback<ArrayList<RowModel>> cb);

	public void getTrabajos(String codmunip, String criterio, String busqueda,
			String proceso, AsyncCallback<ArrayList<RowModel>> cb);

	public void getJornadas(AsyncCallback<ArrayList<RowModel>> cb);

	public void assignProg(String fechaent, String idjornada, String horaent,
			String trab, String ttrab, AsyncCallback<Boolean> cb);


	//--------------------- FILE UPLOAD ---------------------------------------------------

	public void getArchivos(AsyncCallback<ArrayList<RowModel>> result);

	public void getDestinos(AsyncCallback<ArrayList<RowModel>> result);


	public void getGeocerca(String idvehiculo,
			AsyncCallback<ArrayList<RowModel>> result);


	public void getVehiculos(AsyncCallback<ArrayList<RowModel>> result);
	public void getConductores(AsyncCallback<ArrayList<RowModel>> result);
	public void getTiemposActualizacion(AsyncCallback<ArrayList<RowModel>> result);
	public void getMuelleCarga(AsyncCallback<ArrayList<RowModel>> result);

	public void getFormatos(AsyncCallback<ArrayList<RowModel>> result);

	public void getCamposTemplate(String asString,
			AsyncCallback<ArrayList<RowModel>> rows);

	public void getCamposArchivo(String id,
			AsyncCallback<ArrayList<RowModel>> cb);

	public void getTemplates(String id, AsyncCallback<ArrayList<RowModel>> result);

	public void getValoresCampo(String asString, AsyncCallback<ArrayList<RowModel>> rows);

	public void getValoresCampoTemplate(String id, AsyncCallback<ArrayList<RowModel>> rows);

	//--------------------- STATS ---------------------------------------------------------
	public void getTTrabajoRecurso(String fromDate, String toDate, String[] rec,
			String[] cont, String munip, AsyncCallback<ArrayList<RowModel>> cb);

	public void getIndividualCrosstab(String rec, String cont, String munip,
			String fromDate, String toDate, String dato, String[] ttrabs,
			int graphWidth, int graphHeight, String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> cb);

	public void getComparativeCrosstab(String[] rec, String[] cont,
			String munip, String fromDate, String toDate, String dato,
			String[] ttrabs, int graphWidth, int graphHeight,
			String reportSubject, boolean includeOthers, boolean forDays,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> cb);


	public void getManagerReport(String munip, String fromDate, String toDate,
			String dato, int graphWidth, int graphHeight, String reportSubject,
			boolean isPie, AsyncCallback<ArrayList<ArrayList<RowModel>>> cb);

	public void endCurrentTrip(String idrec, String idcont, AsyncCallback<Boolean> cb);

	public void registrarMov(String valueAsString, String valueAsString2,
			String valueAsString3, AsyncCallback<Boolean>  myBooleanAsyncCallback);


	public void getComparativeOdometer(String[] rec, String[] cont, String fromDate,
			String toDate, int graphWidth, int graphHeight,
			String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getAllRecursosProceso(String idproceso, boolean allInfo,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getComparativeAll(String[] rec, String[] cont, String fromDate,
			String toDate, int graphWidth, int graphHeight,
			String reportSubject, String dato,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getManagerReportEvents(String[] eve, String proc, String fromDate,
			String toDate, int graphWidth, int graphHeight,
			String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getAllEventos(AsyncCallback<ArrayList<RowModel>> callback);

	public void getManagerReportOdom(String proc, String fromDate, String toDate,
			int graphWidth, int graphHeight, String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getEvents(String[] rec, String[] cont, String fromDate, String toDate,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getCentralComparativeCrosstab(String[] rec, String[] cont,
			String fromDate, String toDate, String[] eventos, int graphWidth,
			int graphHeight, String reportSubject, boolean includeOthers,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getIndReportCentral(String idrec, String idcont, String fromDate,
			String toDate, String dato, int graphWidth, int graphHeight,
			String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getIndividualEvents(String rec, String cont, String[] eve,
			String fromDate, String toDate, int graphWidth, int graphHeight,
			String reportSubject,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getInfoTrabajo(String trabajo, String tipotrabajo,
			AsyncCallback<ArrayList<GroupedRowModelAdapter>> cb);

	public void searchWorks(String recurso, String contratista,
			String fromDate, String toDate,
			AsyncCallback<ArrayList<RowModel>> asyncCallback);
	public void getContactosProceso(String idProceso, AsyncCallback<ArrayList<RowModel>> cb);
	public void getParkedVehs(String id, AsyncCallback<ArrayList<RowModel>> cb);
	public void getAllContactos(AsyncCallback<ArrayList<RowModel>> rows);
	public void getContactosVehiculo(String idvehiculo,	AsyncCallback<ArrayList<RowModel>> rows);
	public void getAlarmas(String asString,
			AsyncCallback<ArrayList<RowModel>> rows);
	public void getAllEventosVehiculo(String idvehiculo,
			AsyncCallback<ArrayList<RowModel>> cb);
	public void getMoreInfoVehiculo(String idvehiculo,
			AsyncCallback<RowModel> asyncCallback);

	public void getHorasEventos(String fecha, String campo, String idvehiculo,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getServerDate(AsyncCallback<Date> cb);

	public void getNoUbicNoAsigTrabs(String idproc, String dateProg,
			AsyncCallback<ProcesoLight> callback);

	public void quitJob(String trabajo, String tipotrabajo, String idusuario,
			AsyncCallback<Boolean> callback);

	public void closeJobArray(ArrayList<String[]> trabajos, String usuario,
			AsyncCallback<Boolean> callback);

	void multiCloseJob(String recurso, String contratista, String dateProg,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getInterfaceState(AsyncCallback<String> callback);
	public void getInterfaceError(String tipo,AsyncCallback<String> callback);


	public void getDateUpdate(String title, AsyncCallback<String> callback);

	public 	void getConductores(boolean checkedDelete,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void getEntregas(boolean checkedDelete,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void loadTrabajosOracle(boolean checkedDelete,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void loadConductoresOracle(boolean checkedDelete,
			AsyncCallback<ArrayList<ArrayList<RowModel>>> callback);

	public void asignarPrioridad(String trabajo, String prioridad, String viaje,
			AsyncCallback<Boolean> callback);

	public void getWorksPriority(String recurso, String contratista, String dateProg,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void asignarPrioridad(ArrayList<String[]> trabajos,
			AsyncCallback<Boolean> callback);

	public void addTripNovelty(String rec, String novedad,
			AsyncCallback<Boolean> asyncCallback);

	public void getBitacoraNovedades(String recurso, AsyncCallback<ArrayList<RowModel>> callback);

	public void getWorksAddress(String recurso, String contratista, String dateProg,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void generateReporteTrafico(Date fecha, String tipo,
			AsyncCallback<Boolean> callback);

	public void getInfoTrabMultiSelect(ArrayList<String[]> selectedKeys,
			String recurso, String contratista,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void multiAssignJobs(ArrayList<String[]> trabs, String idrec,
			String idcont, String usuario, String idtrabInicioRuta,
			String ttrabInicioRuta, AsyncCallback<Boolean> callback);

	public void getAllRecursosProcesoConductores(String idproceso,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getResourceHours(String resource, String contractor,
			AsyncCallback<ArrayList<Horario>> callback);

	public void getResourcesToSchedule(String idproceso,
			AsyncCallback<ArrayList<Recurso>> callback);

	public void getHorariosCombo(AsyncCallback<ArrayList<RowModel>> callback);

	public void getResourceHour(String resource, String contractor,
			AsyncCallback<String> callback);

	public void getJobsToSchedule(String process, String type, String contractor,
			String resource, String date, String orderBy,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getPointsOfInterest(String process,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getGoogleDistanceTime(ArrayList<RowModel> rm, String startLat,
			String startLon, String resource, String contractor, boolean manualSort,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getHorariosAll(AsyncCallback<ArrayList<RowModel>> callback);

	public void getJornadasAsRowModel(String codigo,
			AsyncCallback<ArrayList<RowModel>> callback);

	public void getJornadasAsRowModel(AsyncCallback<ArrayList<RowModel>> callback);

	public void getJornadasAll(AsyncCallback<ArrayList<RowModel>> callback);

	public void getCamposPrecierre(AsyncCallback<ArrayList<RowModel>> callback);

	public void getCamposPrecierreString(AsyncCallback<String[][]> callback);

	public void getItemsPrecierre(AsyncCallback<ArrayList<RowModel>> callback);

	public void getItemsPrecierreString(AsyncCallback<String[][]> callback);

	public void getMessage(int idMesaage, AsyncCallback<ArrayList<RowModel>> callback);

	public void updatePtoInt(String id, String name, String value, String tipoPunto,
			String idPrcUser, boolean b, String latitude, String longitude,
			AsyncCallback<Boolean> callback);


}