package org.extreme.controlweb.client.services;



import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("services/AdminQueriesService")
public interface AdminQueriesService extends RemoteService {

	public Boolean addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String conductor) throws Exception;

	public Boolean delTrabajo(String trabajo, String tipotrabajo)
			throws Exception;

	public Boolean updateTrabajo(String codtrab, String codttrab,
			String numviaje, String codcli, String nomcli, String fecha,
			String dir, String munip, String barrio, String telefono,
			String prioridad, String desc, String fechaentrega, String jornada,
			String hora, String usuario) throws Exception;

	public Boolean addVehiculo(String idcont, String idrec, String idveh,
			String placa, String tipo, String model, String marca,
			String codprop, String tipomodem, String imei, String min,
			String puk, String activo) throws Exception;


	public Boolean delVehiculo(String id) throws Exception;
	public Boolean addContactoVehiculo(String idvehiculo, String idContacto) throws Exception;
	public Boolean delContactoVehiculo(String idvehiculo, String idcontacto) throws Exception;
	public Boolean delPermiso(String idusuario, String idproceso)
			throws Exception;
	public Boolean addContacto(String idproceso, String nombre,
			String fijo,String fijo2, String celular,
			String celular2, String txtemail, String parent) throws Exception;

	public Boolean updateContacto(String idContacto, String nombre,
			String fijo,String fijo2, String celular,
			String celular2, String email,String parent) throws Exception;
	public Boolean addPermiso(String idusuario, String idproceso)
			throws Exception;

	public Boolean addUser(String id, String nombre, String pass, String cargo,
			String idperfil) throws Exception;

	public Boolean updateUser(String id, String nombre, String pass,
			String cargo, String idperfil) throws Exception;

	public Boolean updateDriver(String id, String nombre,  String cedula, String celular) throws Exception;

	public Boolean delUser(String id) throws Exception;

	public Boolean deleteClient(String id) throws Exception;

	public Boolean deleteDriver(String id) throws Exception;

	public Boolean addClient(String id, String nombre, String direccion,
			String municipio, String telefono) throws Exception;
	public Boolean addDriver(String id, String nombre, String cedula, String celular) throws Exception;

	public Boolean delContratista(String asString) throws Exception;

	public Boolean addContratista(String idContratista, String nombreContratista)
			throws Exception;

	public Boolean updateContratista(String id, String nombre) throws Exception;

	public Boolean delMunicipio(String id) throws Exception;

	public Boolean addMunicipio(String id, String nombre) throws Exception;

	public Boolean updateMunicipio(String id, String nombre) throws Exception;

	public Boolean addMunipTipoTrabajo(String munip, String ttrab)
			throws Exception;

	public Boolean delMunipTipoTrabajo(String munip, String ttrab)
			throws Exception;

	public Boolean delMunipTTrabajoContratista(String munip, String ttrab,
			String cont) throws Exception;

	public Boolean addMunipTTrabajoContratista(String munip, String ttrab,
			String cont) throws Exception;

	public Boolean delTTrabajo(String asString) throws Exception;

	public Boolean addTipoTrabajo(String id, String nombre, boolean reporta)
			throws Exception;

	public Boolean updateTipoTrabajo(String id, String nombre, boolean reporta)
			throws Exception;

	public Boolean deleteMRechazo(String id) throws Exception;

	public Boolean addMRechazo(String id, String nombre) throws Exception;

	public Boolean updateMRechazo(String id, String nombre) throws Exception;

	public Boolean addTRecurso(String id, String nombre, String capacidad)
			throws Exception;

	public Boolean updateTRecurso(String id, String nombre, String capacidad)
			throws Exception;

	public Boolean delTRecurso(String id) throws Exception;


	public Boolean deleteRecurso(String id, String cont) throws Exception;

	public Boolean addTTrabajoProceso(String idproceso, String idttrab)
			throws Exception;

	public Boolean delProcesoTTrabajo(String proceso, String ttrab)
			throws Exception;

	public Boolean delProcesoTRecurso(String proceso, String trecurso)
			throws Exception;

	public Boolean addProcesoTRecurso(String proceso, String trecurso)
			throws Exception;

	public Boolean addProcesoMunicipio(String proceso, String municipio)
			throws Exception;

	public Boolean delProcesoMunicipio(String proceso, String municipio)
			throws Exception;

	public Boolean delProcesoContratista(String proceso, String contratista)
			throws Exception;

	public Boolean addProcesoContratista(String proceso, String contratista)
			throws Exception;

	public Boolean updateProceso(String proceso, String nombre, boolean turnos,
			boolean contratistas, boolean central, int blueFlag,
			int orangeFlag, int redFlag) throws Exception;

	public Boolean addProceso(String proceso, String nombre) throws Exception;

	public Boolean addOpcionesCategoria(String categoria, String idOpcion,
			String nombreOpcion) throws Exception;

	public Boolean addCategoriaLista(String nomcategoria, String tipo) throws Exception;

	public Boolean deleteOpcionCategoria(String opcion) throws Exception;

	public Boolean deleteItemsPrecierres(String item) throws Exception;

	public Boolean addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, int orden) throws Exception;

	public Boolean deleteCampoPrecierres(String idcampoprecierre)
			throws Exception;

	public Boolean addCampoPrecierreLista(String idItemPrecierre,
			String nombreCampoPrecierre, String idcat, String tipo, int orden)
					throws Exception;

	public Boolean updateCampoPrecierreLista(String idCampo, String nombre,
			int orden) throws Exception;

	public Boolean updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, int orden) throws Exception;

	public Boolean delCategoriaPrecierre(String idCategoria) throws Exception;

	public Boolean updateCategoriaPrecierre(String idCategoria,
			String nomCategoria, String tipo) throws Exception;

	public Boolean updateOpcionesCategoria(String id, String nombre) throws Exception;

	public Boolean reorderOpcionesCategoria(String asString, String asString2) throws Exception;

	public Boolean addTemplate(String archivo, String nombre, String formato,String proceso) throws Exception;

	public Boolean updateTemplate(String asString, String valueAsString,String value, String id) throws Exception;

	public Boolean delTemplate(String id) throws Exception;

	public Boolean addCampoTemplate(String template, String columna,String campo) throws Exception;

	public Boolean updateCampoTemplate(String id, String columna) throws Exception;

	public Boolean delCampoTemplate(String id)throws Exception;

	public Boolean addValorCampoTemplate(String iddetalle, String invalue,
			String outvalue, String label) throws Exception;

	public Boolean delValorCampoTemplate(String id) throws Exception;
	public Boolean addAlarma(String idvehiculo, String idevento, String alarma, String gpio) throws Exception;

	public Boolean delAlarma(String idvehiculo, String idevento) throws Exception;

	public Boolean updateInsurance(String idvehiculo, String placa, String tipo,
			String marca, String ciudad,
			String colorCarroceria, String modelo,
			String tipoCarroceria, String colorCabina, boolean asegurado,
			String aseguradora, String tel_emergencia,
			String fechaPoliza, String vigenciaPoliza,
			String poliza,boolean estado,
			String tipo_modem, String min, String puk, String imei,String propietario,
			String modemUser, String modemPasswd, String geocerca) throws Exception;

	public boolean addRegFlota(String valueAsString, String valueAsString2,
			String valueAsString3, String valueAsString4,
			String valueAsString5, String valueAsString6,
			String valueAsString7, String valueAsString8, String observacion)throws Exception;

	public boolean updateRegFlota(String fecha2, String valueAsString,
			String valueAsString2, String valueAsString3,
			String valueAsString4, String valueAsString5,
			String valueAsString6, String valueAsString7, String observacion)throws Exception;

	public boolean deleteRegProg(String fecha, String placa)throws Exception;

	public Boolean addVehiculo(String idvehiculo, String geocerca, String placa,
			String recurso, String contratista)throws Exception;

	public Boolean addVehiculo(String idvehiculo, String placa, String recurso,
			String contratista)throws Exception;

	public boolean deleteMuelle(String asString)throws Exception;

	public boolean addMuelle(String nombre, String desc)throws Exception;

	public boolean updateMuelle(String muelle, String desc)throws Exception;

	public boolean deleteDestino(String asString)throws Exception;

	public boolean addDestino(String iddest, String nombre)throws Exception;

	boolean updateDestino(String iddest, String nombre)throws Exception;

	public boolean ActDatosGeocercas()throws Exception;

	public boolean updateCaractColumnas(String campo, String h, String w)throws Exception;

	public boolean updateCaractPantalla(String tipoletra, String tamletra, String sub,
			String negrita, String cursiva)throws Exception;

	public Boolean addTrabajo(String codtrab, String codttrab, String numviaje,
			String codcli, String nomcli, String fecha, String dir,
			String munip, String barrio, String telefono, String prioridad,
			String desc, String fechaentrega, String jornada, String hora,
			String proceso, String usuario, String conductor, String placa)
					throws Exception;

	public boolean updateResourceHour(String hourCode, String resource,
			String contractor) throws Exception;

	public boolean addResourceJob(ArrayList<RowModel> rm, String date,
			String resource, String contractor, String user, boolean sendMessage)
					throws Exception;

	public boolean deleteHorario(String codigo) throws Exception;

	public boolean delJornadasHorarios(String horario, String idjornada)
			throws Exception;

	public boolean addJornadasHorarios(String idjornada, String horario)
			throws Exception;

	public boolean addHorario(String codigo, String nombre) throws Exception;

	public boolean updateHorario(String codigo, String nombre) throws Exception;

	public boolean deleteJornada(String idjornada) throws Exception;

	public boolean addJornada(String codigo, String nombre, String horainicio,
			String horafin) throws Exception;

	public boolean updateJornada(String idjornada, String codigo, String nombre,
			String horainicio, String horafin) throws Exception;

	public boolean addRecurso(String id, String cont, String trec, String nombre,
			String usuario, String clave, String codigosap, String horario) throws Exception;

	public boolean updateRecurso(String id, String cont, String trec, String nombre,
			String usuario, String clave, String codigosap, String horario) throws Exception;

	public Boolean deleteItemsPrecierres(String item, String ttrab) throws Exception;

	public Boolean deleteCampoPrecierres(String item, String idcampoprecierre)
			throws Exception;

	public Boolean updateCampoPrecierreLista(String idCampo, String idItemPrecierre,
			String nombre, String codigo, int orden) throws Exception;

	public Boolean addCampoItem(String campo, String item, int orden) throws Exception;

	public Boolean addCampoPrecierreLista(String idItemPrecierre, String codigo,
			String nombreCampoPrecierre, String idcat, String tipo, int orden)
					throws Exception;

	public Boolean addTTrabajoItem(String tipotrabajo, String item, int orden)
			throws Exception;

	public Boolean addItemsPrecierre(String tipotrabajo, String nomItem,
			boolean noRealizacion, String repeticion, int orden)
					throws Exception;

	public Boolean updateItemPrecierre(String idItem, String nombre,
			boolean noRealizacion, String repeticion, int orden, String ttrab)
					throws Exception;

	public Boolean updateItemPrecierreOrden(String idItem, int orden, String ttrab)
			throws Exception;

	public boolean updateMessage(String subject, String from, String to,
			String dateDelivery, String jobDetails, String timeTolerance, int idMesaage)
					throws Exception;
	
	public String addMessage(String subject, String from, String to,
			String dateDelivery, String jobDetails, String timeTolerance) throws Exception;

	public boolean updateMessageJobsType(String jobType, int idMesaage)
			throws Exception;

	public Boolean addTipoTrabajo(String id, String nombre, boolean reporta,
			String maxduracion, String tiempopromedio) throws Exception;

	public Boolean updateTipoTrabajo(String id, String nombre, boolean reporta,
			String maxduracion, String tiempopromedio) throws Exception;


	public boolean closeWorkAndAddJamarResult(String trabajo, String tipotrabajo, String calificacion, String causal,
			String fuente, String usuario) throws Exception;





}
