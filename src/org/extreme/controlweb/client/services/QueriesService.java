package org.extreme.controlweb.client.services;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.GroupedRowModelAdapter;
import org.extreme.controlweb.client.core.Horario;
import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.ProcesoLight;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.SimpleEntity;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("services/QueriesService")
public interface QueriesService extends RemoteService {

	@Deprecated
	public String setSessionVars(String[] vars);

	@Deprecated
	public ArrayList<SimpleEntity> getEmpresas() throws Exception;

	public ArrayList<String> login(String user, String pass, String codemp)
			throws Exception;

	public ArrayList<SimpleEntity> getProcesos(String idusuario);

	public Recurso updateRecurso(String id, String idCont, String idProc,
			boolean soloCentral, String fecha);

	public ArrayList<SimpleEntity> getTiposTrabajo(String idproceso);

	public ArrayList<SimpleEntity> getMunicipios();

	public ArrayList<SimpleEntity> getTiposRecurso(String idproceso);

	public ArrayList<SimpleEntity> getContratistas(String idproceso);

	public ArrayList<SimpleEntity> getPropietarios();

	public ArrayList<SimpleEntity> getTiposModem();

	public ArrayList<SimpleEntity> getPerfiles();

	public ArrayList<SimpleEntity> getRecursosSinVehiculo(String idproceso);

	public Boolean assignJob(String trabajo, String tipotrabajo,
			String recurso, String contratista, String usuario)
					throws Exception;

	public Boolean closeJob(String trabajo, String tipotrabajo, String idusuario)
			throws Exception;

	public ArrayList<RowModel> getContactosProceso(String idProceso) throws Exception;
	public ArrayList<RowModel> getAllContactos() throws Exception;
	public ArrayList<RowModel> getContactosVehiculo(String idvehiculo) throws Exception;
	public ArrayList<RowModel> getAlarmas(String asString) throws Exception;
	public ArrayList<RowModel> getAllEventosVehiculo(String idvehiculo) throws Exception;
	public RowModel getMoreInfoVehiculo(String idvehiculo) throws Exception;

	public Boolean locateJobAddress(String dir, String munip, String lat,
			String lon, String trabajo, String tipotrabajo) throws Exception;

	public Boolean setPathStart(String trabajo, String tipotrabajo,
			String recurso, String contratista, boolean noPath)
					throws Exception;

	public Boolean resetJob(String trabajo, String tipotrabajo, String idusuario)
			throws Exception;

	public LocalizableObject getAddressCoords(String direccion, String munip)
			throws Exception;

	public Boolean setJobPriority(String trabajo, String tipotrabajo,
			String prioridad) throws Exception;

	public Boolean setResourceMode(String recurso, String contratista,
			String online) throws Exception;

	public ArrayList<RowModel> getGeocercas(String recurso, String contratista);

	public Boolean createGeofence(String idvehiculo, int radio, double latitud,
			double longitud, String nombre) throws Exception;

	public Boolean deactivateGeofence(String idvehiculo, String numgeocerca)
			throws Exception;

	public Boolean createPtoInt(String id, String tipo, String latitud,
			String longitud, String desc, String direccion, String municipio,
			String departamento, String icono) throws Exception;

	public ArrayList<RowModel> getPtoInt(String idusuario, String idproceso)
			throws Exception;

	public ArrayList<LocalizableObject> getPtoIntUsuario(String idusuario)
			throws Exception;

	public ArrayList<LocalizableObject> getPtoIntProceso(String idproceso)
			throws Exception;

	public ArrayList<SimpleEntity> getIconos() throws Exception;

	public LocalizableObject getDireccion(String direccion, String municipio)
			throws Exception;

	public Boolean deletePtoInt(String idPtoInt) throws Exception;

	public ArrayList<RowModel> getTrabajos(String idproceso) throws Exception;

	public ArrayList<RowModel> getVehiculos(String proceso) throws Exception;

	public ArrayList<RowModel> getInfoTrabajosPrecerrados(String proceso,
			String fechaDesde, String fechaHasta, String ttrab, String estadoB,
			boolean fCreacion) throws Exception;

	public ArrayList<RowModel> getInfoTrabajosPrecerrados(String proceso,
			String fechaDesde, String fechaHasta, String recurso,
			String contratista, String ttrab) throws Exception;

	public ArrayList<GroupedRowModelAdapter> getInfoTrabajoPrecierre(
			String trabajo, String tipoTrabajo) throws Exception;

	public ArrayList<GroupedRowModelAdapter> getInfoBitacora(String trabajo,
			String tipoTrabajo) throws Exception;

	public ArrayList<RowModel> getUsersInfo(String admin) throws Exception;

	public String sendOtaPosActual(String recurso, String contratista)
			throws Exception;

	public ArrayList<RowModel> getMunicipiosRowModel() throws Exception;

	public ArrayList<RowModel> getProcesosAsRowModel(String idusuario)
			throws Exception;

	public ArrayList<RowModel> getProcesosAsRowModel() throws Exception;

	public ArrayList<SimpleEntity> getProcesosAsSimpleEntity() throws Exception;

	public ArrayList<RowModel> getInfoClientes() throws Exception;

	public ArrayList<RowModel> getInfoDrivers() throws Exception;

	public ArrayList<RowModel> getContratistasAsRowModel() throws Exception;

	public ArrayList<RowModel> getTTrabajoMunicipio(String idmunip)
			throws Exception;

	public ArrayList<SimpleEntity> getAllTTrabajos() throws Exception;

	public ArrayList<RowModel> getAllTTrabajosAsRowModel() throws Exception;

	public ArrayList<RowModel> getContratistasMunicipio(String idmunip,
			String ttrab) throws Exception;

	public ArrayList<SimpleEntity> getAllContratistas() throws Exception;

	public ArrayList<RowModel> getAllMRechazo() throws Exception;

	public ArrayList<RowModel> getAllTiposRecurso() throws Exception;

	public ArrayList<RowModel> getAllRecursosProceso(String idproceso)
			throws Exception;

	public ArrayList<RowModel> getTiposTrabajoAsRowModel(String idproceso)
			throws Exception;

	public ArrayList<RowModel> getProcesoTRecursos(String idproceso)
			throws Exception;

	public ArrayList<RowModel> getProcesoMunicipios(String proceso)
			throws Exception;

	public ArrayList<SimpleEntity> getAllTiposRecursoAsSimpleEntity()
			throws Exception;

	public ArrayList<RowModel> getContratistasAsRowModel(String proceso)
			throws Exception;

	public ArrayList<RowModel> getAllCategorias() throws Exception;

	public ArrayList<RowModel> getOpcionesCategoria(String categoria)
			throws Exception;

	public ArrayList<RowModel> getItemsPrecierre(String asString)
			throws Exception;

	public ArrayList<RowModel> getCamposPrecierre(String item) throws Exception;

	public ArrayList<RowModel> getOdometro(String recurso, String contratista,
			String desde, String hasta) throws Exception;

	public ArrayList<RowModel> getEventosDesc() throws Exception;

	public ArrayList<RowModel> getAllRecursosProcesoConCarro(String idproceso)
			throws Exception;

	public ArrayList<RowModel> GeocercasDeRecurso(String rec, String cont)
			throws Exception;

	public ArrayList<RowModel> getInfoGeocercas(String rec, String cont,
			String fromDate, String toDate, ArrayList<String> geos)
					throws Exception;

	public ArrayList<RowModel> getPosActualVehiculos(String proceso)
			throws Exception;

	public ArrayList<RowModel> getInfoEventos(String idrec, String idcont,
			String fromDate, String toDate, ArrayList<String> events)
					throws Exception;

	public ArrayList<RowModel> getOTACommands(String recurso, String contratista)
			throws Exception;

	public String sendOTAPredef(String recurso, String contratista, String idota)
			throws Exception;

	public String sendOTACustom(String recurso, String contratista,
			String atCommand) throws Exception;

	public ArrayList<GroupedRowModelAdapter> getInfoJAMAR(String idTrabajo,
			String idTipoTrabajo) throws Exception;

	public ArrayList<RowModel> getMunicipiosDeProceso(String proceso)
			throws Exception;

	public ArrayList<RowModel> getTrabajos(String codmunip, String criterio,
			String busqueda, String proceso) throws Exception;

	public ArrayList<RowModel> getJornadas() throws Exception;

	public Boolean assignProg(String fechaent, String idjornada,
			String horaent, String trab, String ttrab) throws Exception;

	boolean endCurrentTrip(String idrec, String idcont) throws Exception;

	// --------------------------- FILE UPLOAD
	// ---------------------------------------

	public ArrayList<RowModel> getArchivos() throws Exception;
	public ArrayList<RowModel> getDestinos()throws Exception;

	public ArrayList<RowModel> getFormatos() throws Exception;

	public ArrayList<RowModel> getCamposTemplate(String template)
			throws Exception;

	public ArrayList<RowModel> getCamposArchivo(String id) throws Exception;

	public ArrayList<RowModel> getTemplates(String id) throws Exception;

	public ArrayList<RowModel> getValoresCampo(String asString)
			throws Exception;

	public ArrayList<RowModel> getValoresCampoTemplate(String id)
			throws Exception;

	// --------------------------- STATS
	// ---------------------------------------------

	public ArrayList<RowModel> getTTrabajoRecurso(String fromDate,
			String toDate, String[] rec, String[] cont, String munip)
					throws Exception;

	public ArrayList<ArrayList<RowModel>> getIndividualCrosstab(String rec,
			String cont, String munip, String fromDate, String toDate,
			String dato, String[] ttrabs, int graphWidth, int graphHeight,
			String reportSubject) throws Exception;

	public ArrayList<ArrayList<RowModel>> getComparativeCrosstab(String[] rec,
			String[] cont, String munip, String fromDate, String toDate,
			String dato, String[] ttrabs, int graphWidth, int graphHeight,
			String reportSubject, boolean includeOthers, boolean forDays)
					throws Exception;

	public ArrayList<ArrayList<RowModel>> getManagerReport(String munip,
			String fromDate, String toDate, String dato, int graphWidth,
			int graphHeight, String reportSubject, boolean isPie)
					throws Exception;

	public ArrayList<ArrayList<RowModel>> getComparativeOdometer(String[] rec,
			String[] cont, String fromDate, String toDate, int graphWidth,
			int graphHeight, String reportSubject) throws Exception;

	public ArrayList<RowModel> getAllRecursosProceso(String idproceso,
			boolean allInfo) throws Exception;

	public ArrayList<ArrayList<RowModel>> getComparativeAll(String[] rec,
			String[] cont, String fromDate, String toDate, int graphWidth,
			int graphHeight, String reportSubject, String dato)
					throws Exception;

	public ArrayList<ArrayList<RowModel>> getManagerReportEvents(String[] eve,
			String proc, String fromDate, String toDate, int graphWidth,
			int graphHeight, String reportSubject) throws Exception;

	public ArrayList<RowModel> getAllEventos() throws Exception;

	public ArrayList<ArrayList<RowModel>> getManagerReportOdom(String proc,
			String fromDate, String toDate, int graphWidth, int graphHeight,
			String reportSubject) throws Exception;

	public ArrayList<RowModel> getEvents(String[] rec, String[] cont,
			String fromDate, String toDate) throws Exception;

	public ArrayList<ArrayList<RowModel>> getCentralComparativeCrosstab(
			String[] rec, String[] cont, String fromDate, String toDate,
			String[] eventos, int graphWidth, int graphHeight,
			String reportSubject, boolean includeOthers) throws Exception;

	public ArrayList<ArrayList<RowModel>> getIndReportCentral(String idrec,
			String idcont, String fromDate, String toDate, String dato,
			int graphWidth, int graphHeight, String reportSubject)
					throws Exception;

	public ArrayList<ArrayList<RowModel>> getIndividualEvents(String rec,
			String cont, String[] eve, String fromDate, String toDate,
			int graphWidth, int graphHeight, String reportSubject)
					throws Exception;

	public ArrayList<GroupedRowModelAdapter> getInfoTrabajo(String trabajo,
			String tipotrabajo) throws Exception;

	public ArrayList<RowModel> searchWorks(String recurso, String contratista,
			String fromDate, String toDate) throws Exception;

	public ArrayList<RowModel> getParkedVehs(String proceso) throws Exception;

	public ArrayList<RowModel> getVehiculos()throws Exception;

	public ArrayList<RowModel> getConductores()throws Exception;

	public ArrayList<RowModel> getMuelleCarga()throws Exception;

	public ArrayList<RowModel> getAllProgramaciondeFlota()throws Exception;

	public ArrayList<RowModel> getAllProgramaciondeFlotaFiltro(String fecha2,
			String valueAsString, String valueAsString2, String valueAsString3,
			String valueAsString4, String valueAsString5,
			String valueAsString6, String valueAsString7)throws Exception;

	public boolean registrarMov(String valueAsString, String valueAsString2,
			String valueAsString3)throws Exception;

	public ArrayList<RowModel> registrarMovimiento(String valueAsString, String valueAsString2,
			String valueAsString3)throws Exception;

	public String regMovi(String valueAsString, String valueAsString2,
			String valueAsString3)throws Exception;

	public ArrayList<RowModel> getControldeFlotasGeneral(String fecha2)throws Exception;

	public ArrayList<RowModel> getTiemposActualizacion()throws Exception;

	public String obtenerCampo(String valueAsString, String valueAsString2) throws Exception;

	public Proceso getProcesoMaqTime(String proceso, boolean b, String value);

	public ProcesoLight getInfoRecursosMaqTime(ArrayList<String[]> checkedRecs,
			String id, boolean soloCentral, boolean conTurnos, String value);

	public ArrayList<RowModel> getGeocerca(String idvehiculo)throws Exception;

	public ArrayList<RowModel> getAllMuelles()throws Exception;

	public ArrayList<RowModel> getAllDestinos()throws Exception;

	public String[] obtenerValores() throws Exception;

	public ArrayList<RowModel> getHorasEventos(String fecha, String campo, String idvehiculo) throws Exception;

	public Date getServerDate()throws Exception;

	public ProcesoLight getNoUbicNoAsigTrabs(String idproc, String dateProg);

	public Boolean quitJob(String trabajo, String tipotrabajo, String idusuario)
			throws Exception;

	public Boolean closeJobArray(ArrayList<String[]> trabajos, String usuario)
			throws Exception;

	public ArrayList<RowModel> multiCloseJob(String recurso, String contratista,
			String dateProg)throws Exception;

	public String getInterfaceState() throws Exception;

	public String getDateUpdate(String title)throws Exception;

	public  ArrayList<ArrayList<RowModel>>   getConductores(boolean checkedDelete) throws Exception;

	public  ArrayList<ArrayList<RowModel>>  getEntregas(boolean checkedDelete) throws Exception;

	public ArrayList<ArrayList<RowModel>> loadTrabajosOracle(boolean checkedDelete)
			throws Exception;

	public ArrayList<ArrayList<RowModel>> loadConductoresOracle(boolean checkedDelete)
			throws Exception;

	public Boolean asignarPrioridad(String trabajo, String prioridad, String viaje)
			throws Exception;

	public ArrayList<RowModel> getWorksPriority(String recurso, String contratista,
			String dateProg);

	public Boolean asignarPrioridad(ArrayList<String[]> trabajos)
			throws Exception;

	public Boolean addTripNovelty(String rec, String novedad) throws Exception;

	public ArrayList<RowModel> getBitacoraNovedades(String recurso) throws Exception;

	public ArrayList<RowModel> getWorksAddress(String recurso, String contratista,
			String dateProg)throws Exception;

	public Boolean generateReporteTrafico(Date fecha, String tipo) throws Exception;

	public String getInterfaceError(String tipo)throws Exception;

	public ArrayList<RowModel> getInfoTrabMultiSelect(
			ArrayList<String[]> selectedKeys, String recurso, String contratista)
					throws Exception;

	public Boolean multiAssignJobs(ArrayList<String[]> trabs, String idrec,
			String idcont, String usuario, String idtrabInicioRuta,
			String ttrabInicioRuta) throws Exception;

	public ArrayList<RowModel> getAllRecursosProcesoConductores(String idproceso)
			throws Exception;

	public ArrayList<Horario> getResourceHours(String resource, String contractor);

	public ArrayList<Recurso> getResourcesToSchedule(String idproceso);

	public ArrayList<RowModel> getHorariosCombo() throws Exception;

	public String getResourceHour(String resource, String contractor) throws Exception;

	public ArrayList<RowModel> getJobsToSchedule(String process, String type,
			String contractor, String resource, String date, String orderBy)
					throws Exception;

	public ArrayList<RowModel> getPointsOfInterest(String process) throws Exception;

	public ArrayList<RowModel> getGoogleDistanceTime(ArrayList<RowModel> rm,
			String startLat, String startLon, String resource, String contractor, boolean manualSort)
					throws Exception;

	public ArrayList<RowModel> getHorariosAll() throws Exception;

	public ArrayList<RowModel> getJornadasAsRowModel(String codigo) throws Exception;

	public ArrayList<RowModel> getJornadasAsRowModel() throws Exception;

	public ArrayList<RowModel> getJornadasAll() throws Exception;

	public ArrayList<RowModel> getCamposPrecierre() throws Exception;

	public String[][] getCamposPrecierreString() throws Exception;

	public ArrayList<RowModel> getItemsPrecierre() throws Exception;

	public String[][] getItemsPrecierreString() throws Exception;

	public ArrayList<RowModel> getMessage(int idMesaage) throws Exception;

	public Boolean updatePtoInt(String id, String name, String value,
			String tipoPunto, String idPrcUser, boolean b, String latitude,
			String longitude) throws Exception;


}

