package org.extreme.controlweb.client.gui;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.TextAlign;
import com.gwtext.client.data.BooleanFieldDef;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.grid.BaseColumnConfig;
import com.gwtext.client.widgets.grid.CellMetadata;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.ColumnConfig;
import com.gwtext.client.widgets.grid.ColumnModel;
import com.gwtext.client.widgets.grid.EditorGridPanel;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.Renderer;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;

public class CheckBoxGridPanel extends EditorGridPanel {

	private ArrayList<Integer> colsExclusionList;
	private RecordDef recordDef;
	private boolean configured;
	private boolean tools;

	public CheckBoxGridPanel(boolean tools){
		super();
		colsExclusionList = new ArrayList<Integer>();
		this.tools = tools;
	}

	public CheckBoxGridPanel() {
		super();
		colsExclusionList = new ArrayList<Integer>();
		this.tools = false;
	}



	public void addExcludedCol(int colIndex){
		colsExclusionList.add(colIndex);
	}

	public void configure(String[] columnNames, int[] columnSizes,
			boolean[] visible) {

		ColumnConfig ccChecked = new ColumnConfig("", "checked", 55);
		ccChecked.setRenderer(new Renderer() {
			@Override
			public String render(Object value, CellMetadata cellMetadata,
					Record record, int rowIndex, int colNum, Store store) {
				boolean checked = ((Boolean) value).booleanValue();
				return "<img class=\"checkbox\" src=\"js/ext/resources/images/default/menu/" + (checked ? "checked.gif" : "unchecked.gif") + "\"/>";
			}
		});
		ccChecked.setAlign(TextAlign.CENTER);

		ArrayList<FieldDef> fd = new ArrayList<FieldDef>();
		fd.add(new BooleanFieldDef("checked"));

		ArrayList<BaseColumnConfig> bcc = new ArrayList<BaseColumnConfig>();
		bcc.add(ccChecked);

		for (int j = 0; j < columnNames.length; j++) {
			fd.add(new StringFieldDef(columnNames[j]));
			if (visible != null) {
				if (visible[j] && !colsExclusionList.contains(j)) {
					int size = 200;
					if (columnSizes != null) {
						size = columnSizes[j];
					}
					bcc.add(new ColumnConfig(columnNames[j], columnNames[j],
							size, true));
				}
			} else if (!colsExclusionList.contains(j)) {
				bcc.add(new ColumnConfig(columnNames[j], columnNames[j], 200,
						true));
			}
		}

		ColumnModel cm = new ColumnModel(toBaseColumnConfigArray(bcc));

		cm.setDefaultSortable(true);
		recordDef = new RecordDef(toFieldDefArray(fd));
		Store s = new Store(recordDef);
		this.setStore(s);
		this.setColumnModel(cm);
		this.setEnableHdMenu(false);


		this.addGridCellListener(new GridCellListenerAdapter() {
			@Override
			public void onCellClick(GridPanel grid, int rowIndex, int colIndex,
					EventObject e) {
				if (grid.getColumnModel().getDataIndex(colIndex).equals("checked") && e.getTarget(".checkbox", 1) != null) {
					Record record = grid.getStore().getAt(rowIndex);
					record.set("checked", !record.getAsBoolean("checked"));
				}
			}
		});
		this.setStore(new Store(recordDef));
		this.setColumnModel(new ColumnModel(toBaseColumnConfigArray(bcc)));
		this.setEnableHdMenu(false);
		this.setHeight(150);
		this.setWidth(100);

		if(tools){
			Toolbar tools = new Toolbar();
			ToolbarButton expandButton = new ToolbarButton();
			expandButton.setCls("x-btn-icon expand-all-btn");
			expandButton.setTooltip("Seleccionar Todas");
			expandButton.addListener(new ButtonListenerAdapter() {
				@Override
				public void onClick(Button button, EventObject e) {
					CheckBoxGridPanel.this.checkAll();
				}
			});
			tools.addButton(expandButton);
			ToolbarButton collapseButton = new ToolbarButton();
			collapseButton.setCls("x-btn-icon collapse-all-btn");
			collapseButton.setTooltip("Quitar Selecci\u00f3n");
			collapseButton.addListener(new ButtonListenerAdapter() {
				@Override
				public void onClick(Button button, EventObject e) {
					CheckBoxGridPanel.this.uncheckAll();
				}
			});
			tools.addButton(collapseButton);
			this.setTopToolbar(tools);
		}

		configured = true;
	}

	/*public void addRow(String id, String nombre) {
		String[] data = new String[] { "false", id, nombre };
		Record newRecord = recordDef.createRecord(data);
		this.getStore().add(newRecord);
		this.getStore().commitChanges();
	}*/

	public Record[] getSelected() {
		Record[] all = this.getStore().getRecords();
		ArrayList<Record> selected = new ArrayList<Record>();
		for (int i = 0; i < all.length; i++)
			if(all[i].getAsBoolean("checked"))
				selected.add(all[i]);

		Record[] res = new Record[selected.size()];
		for (int i = 0; i < selected.size(); i++) 
			res[i] = selected.get(i);

		return res;
	}

	public String[] getSelectedIDs(){
		Record[] sel = getSelected();
		String[] res = new String[sel.length];
		for (int i = 0; i < res.length; i++) 
			res[i] = sel[i].getAsString("id");
		return res;
	}

	public String[][] getSelectedIDs(String[] cols){
		Record[] sel = getSelected();
		String[][] res = new String[sel.length][cols.length];
		for (int i = 0; i < res.length; i++) 
			for (int j = 0; j < cols.length; j++) 
				res[i][j] = sel[i].getAsString(cols[j]);
		return res;
	}

	public RecordDef getRecordDef(){
		return recordDef;
	}

	public void checkAll() {
		Record[] all = this.getStore().getRecords();
		for (int i = 0; i < all.length; i++) {
			all[i].set("checked", true);
		}
		this.getStore().commitChanges();
	}

	public void uncheckAll() {
		Record[] all = this.getStore().getRecords();
		for (int i = 0; i < all.length; i++) {
			all[i].set("checked", false);
		}
		this.getStore().commitChanges();
	}

	public void addAll(ArrayList<RowModel> result) {
		getStore().removeAll();
		getStore().commitChanges();
		for (RowModel rm : result) {
			Object[] rv = rm.getRowValues();
			Object[] row = new Object[rv.length + 1];
			row[0] = false;
			for (int i = 1; i < row.length; i++) 
				row[i] = rv[i - 1]; 
			this.getStore().add(getRecordDef().createRecord(row));
		}
		getStore().commitChanges();
	}

	private BaseColumnConfig[] toBaseColumnConfigArray(
			ArrayList<BaseColumnConfig> bcc) {
		BaseColumnConfig[] a = new BaseColumnConfig[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			a[i] = bcc.get(i);
		}
		return a;
	}

	private FieldDef[] toFieldDefArray(ArrayList<FieldDef> bcc) {
		FieldDef[] a = new FieldDef[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			a[i] = bcc.get(i);
		}
		return a;
	}

	public boolean isConfigured() {
		return configured;
	}
	
}
