package org.extreme.controlweb.client.gui;





import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.layout.FitLayout;


public class ErrorWindow extends Window {

	

	
	public ErrorWindow(String title, int width, int height,String textLog) {
		super(title, width, height);
		
		setLayout(new FitLayout());		
		setWidth(850);
		setHeight(500);
	    setIconCls("icon-butterfly");	
	    TextArea log= new TextArea();
	    log.setReadOnly(true);	
	    log.setHideLabel(true);
	    log.setValue( textLog);
	    add(log);
		show();
           
	}
	
	
	
	
	@Override
	public void show() {
		super.show();
	
	}
	
}
