package org.extreme.controlweb.client.gui;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.GroupedRowModelAdapter;

import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.GroupingStore;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.SortState;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.grid.CellMetadata;
import com.gwtext.client.widgets.grid.ColumnConfig;
import com.gwtext.client.widgets.grid.ColumnModel;
import com.gwtext.client.widgets.grid.EditorGridPanel;
import com.gwtext.client.widgets.grid.GridEditor;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.GroupingView;
import com.gwtext.client.widgets.grid.Renderer;
import com.gwtext.client.widgets.grid.event.EditorGridListenerAdapter;

public class GroupedGridPanel extends EditorGridPanel{

	private GroupingStore store;
	private RecordDef recordDefProperties;

	public GroupedGridPanel() {
		super();
		recordDefProperties = new RecordDef(new FieldDef[] {
				new StringFieldDef("property"), 
				new StringFieldDef("value"),
				new StringFieldDef("group")
		 });
		
		store = new GroupingStore(recordDefProperties);
		store.setSortInfo(new SortState("property", SortDir.ASC));
		store.setGroupField("group");
		store.groupBy("group", true);
		
		ColumnConfig c1 = new ColumnConfig("&nbsp;", "property", 250, false, new Renderer(){

			public String render(Object value,
					CellMetadata cellMetadata, Record record,
					int rowIndex, int colNum, Store store) {
				
				return "<b>"+ value +"</b>";
			}
 			
 		 });
		
		
		
		ColumnConfig c2 = new ColumnConfig("&nbsp;", "value", 250, false);
		
		ColumnConfig c3 = new ColumnConfig("&nbsp;", "group", 10, false);
		
		ColumnModel columnModel = new ColumnModel(new ColumnConfig[]{ c1, c2, c3 });
		columnModel.setEditable(1, true);
		
		
		TextArea ta = new TextArea();
		ta.setReadOnly(true);
		columnModel.setEditor(1, new GridEditor(ta));
		this.setStore(store);
		this.setColumnModel(columnModel);
		this.setTrackMouseOver(true);
		//this.setStripeRows(true);
		this.setEnableHdMenu(false);
		this.setEnableColumnMove(false);
		this.setClicksToEdit(1);
		this.hideColumnHeader();
		
		GroupingView view = new GroupingView();
		view.setEnableGrouping(true);
		view.setHideGroupedColumn(true);
		view.setShowGroupName(false);
		view.setGroupTextTpl("{text}");
		this.setView(view);
		
		this.addEditorGridListener(new EditorGridListenerAdapter(){
			@Override
			public boolean doBeforeEdit(GridPanel grid, Record record,
					String field, Object value, int rowIndex, int colIndex) {
				// TODO Auto-generated method stub
				if (value.toString().contains("</div>")
						|| value.toString().contains("</span>")
						|| value.toString().contains("</b>"))
					return false;
				else
					return true;
			}
		});
	}
	
	public void addProperty(GroupedRowModelAdapter grma){
		String p=grma.getColumnNames()[0];		
		String v=grma.getRowValues()[0];
		store.add(recordDefProperties.createRecord(new String[]{p,v,grma.getGroup()}));
	}
	
	public void addProperties(ArrayList<GroupedRowModelAdapter> agrma){
		for (GroupedRowModelAdapter groupedRowModelAdapter : agrma) {
			addProperty(groupedRowModelAdapter);
		}
	}
	
	public void addProperty(String property, String value, String group) {
		store.add(recordDefProperties.createRecord(new String[] { property,
				value, group }));

	}
}
