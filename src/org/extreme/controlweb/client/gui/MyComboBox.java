package org.extreme.controlweb.client.gui;

import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;

public class MyComboBox extends ComboBox {

	private Record rec;
	private RecordDef rd;

	public MyComboBox(String title) {
		super(title);
		rec = null;
		this.addListener(new ComboBoxListenerAdapter(){
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				rec = record; 
			}
		});
	}
	
	public MyComboBox() {
		super();
		initialize();	
	}
    
	private void initialize(){
		rec = null;
		this.addListener(new ComboBoxListenerAdapter(){
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				rec = record; 
			}
		});
	}
	
	public void setFiltered() {
		setForceSelection(true);
		setMinChars(1);
		setMode(ComboBox.LOCAL);
		setTriggerAction(ComboBox.ALL);
		setTypeAhead(true);
		setSelectOnFocus(true);

		setHideTrigger(false);
		setReadOnly(false);
	}
	
	public Record getSelectedRecord(){
		return rec;
	}
	
	public void cleanSelectedRecord(){
		rec = null;
	}

	public void setRecordDef(RecordDef rd) {
		this.rd = rd;
	}
	
	public RecordDef getRecordDef() {
		return rd;
	}

	public void setSelected(int i) {
		rec = getStore().getAt(i);	
	}
}
