package org.extreme.controlweb.client.gui;

import com.gwtext.client.data.Record;

/**
 * Interfaz que maneja el evento del clic sobre un MyGridPanel.
 *
 * @author Miguel Fruto
 */

public interface MyGridOnCellClickListener {
	/**
	 * Metodo que se ejecuta al clic en la celda, se recibe un com.gwtext.client.data.Record por parametro
	 *
	 * @author Miguel Fruto
	 */
	public void onCellClick(Record r);
}
