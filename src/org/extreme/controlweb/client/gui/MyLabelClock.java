package org.extreme.controlweb.client.gui;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.gwtext.client.widgets.form.TextField;

public class MyLabelClock extends TextField {
	private Timer timer;
	private Date date;
	private static final int INTERVAL = 100;
		
	public MyLabelClock(String title) {
		super(title);
		setReadOnly(true);
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public void startClock() {
		timer = new Timer() {
			public void run() {
				String hora = "";
				date = new Date(date.getTime() + INTERVAL);
				hora = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss").format(date);
				setValue(hora);

			}

		};
		timer.scheduleRepeating(INTERVAL);
	}

	public void StopClock() {
		timer.cancel();
	}

}
