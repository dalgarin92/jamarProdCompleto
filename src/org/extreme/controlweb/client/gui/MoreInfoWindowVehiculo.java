package org.extreme.controlweb.client.gui;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.GenericAdminPanel;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.core.SimpleEntity;
import org.extreme.controlweb.client.core.SimpleEntityAdapter;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.HorizontalLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;

public class MoreInfoWindowVehiculo extends Window {
	private String idvehiculo;
	private TabPanel tpVehiculos;

	private Panel pGeneral;
	private FieldSet fsGeneralIzquieda;
	private FieldSet fsGeneralDerecha;
	private TextField txtPlaca, txtMarca, txtTipo, txtModelo, txtCiudad,
			txtColorCarroceria, txtColorCabina, txtFechaIngreso;
	private MyComboBox cmbTipoCarroceria;
	private ArrayList<SimpleEntity> alCarroceria;
	private Checkbox ckActivo;

	private Panel pSeguro;
	private FieldSet fsSeguroIzquieda;
	private FieldSet fsSeguroDerecha;
	private Checkbox ckAsegurado;
	private TextField txtAseguradora, txtTelEmergencia, txtPoliza;
	private DateField dfFechaPoliza, dfVigenciaPoliza;

	private GenericAdminPanel pContactos, pAlarmas;
	
	private Panel pModem;
	
	private MyComboBox cmbPropietario;
	private MyComboBox cmbGeocerca;
	private TextField txtMin;
	private TextField txtPuk;
	private MyComboBox cmbTipoModem;
	private TextField txtImei;
	private TextField txtModemUser;
	private TextField txtModemPasswd;

	protected static QueriesServiceAsync serviceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private static AdminQueriesServiceAsync adminServiceProxy = null;

	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;
	}

	public MoreInfoWindowVehiculo(Record vehiculo, final ControlWebGUI gui) {		
		super(vehiculo.getAsString("ID Vehiculo"), true, false);		
		idvehiculo=vehiculo.getAsString("ID Vehiculo");		
		setLayout(new FitLayout());
		setSize(680,500);
		setTitle(idvehiculo);
		this.setButtonAlign(Position.CENTER);
		tpVehiculos=new TabPanel();		
			pGeneral=new Panel("Informaci\u00f3n general");
			pGeneral.setPaddings(15);
			pGeneral.setLayout(new HorizontalLayout(15));			
				fsGeneralIzquieda=new FieldSet();
				fsGeneralIzquieda.setPaddings(5);
				fsGeneralIzquieda.setLabelWidth(120);
				fsGeneralIzquieda.setBorder(false);		
				fsGeneralIzquieda.setLayout(new FormLayout());
			
				fsGeneralDerecha=new FieldSet();
				fsGeneralDerecha.setPaddings(5);
				
				fsGeneralDerecha.setLabelWidth(120);
				fsGeneralDerecha.setLayout(new FormLayout());
				fsGeneralDerecha.setBorder(false);
					txtPlaca=new TextField("Placa");
					fsGeneralIzquieda.add(txtPlaca);
					txtPlaca.setValue(vehiculo.getAsString("Placa"));
			
					txtMarca=new TextField("Marca");		
					fsGeneralIzquieda.add(txtMarca);
					txtMarca.setValue(vehiculo.getAsString("Marca"));
			
					txtTipo = new TextField("Tipo");
					fsGeneralDerecha.add(txtTipo);
					txtTipo.setValue(vehiculo.getAsString("Tipo vehiculo"));
			
					txtModelo= new TextField("Modelo");
					fsGeneralIzquieda.add(txtModelo);
					txtModelo.setValue(vehiculo.getAsString("Modelo"));
			
					txtCiudad = new TextField("Ciudad");
					fsGeneralDerecha.add(txtCiudad);
					txtCiudad.setValue("Ciudad");
			
					cmbTipoCarroceria=new MyComboBox("Tipo Carroceria");
					alCarroceria=new ArrayList<SimpleEntity>();
					alCarroceria.add(new SimpleEntityAdapter("Furg\u00f3n","Furg\u00f3n"));
					alCarroceria.add(new SimpleEntityAdapter("Estacas","Estacas"));
					alCarroceria.add(new SimpleEntityAdapter("Remolque","Remolque"));
					alCarroceria.add(new SimpleEntityAdapter("Otro","Otro"));
					
					new RPCFillComboBoxHandler(cmbTipoCarroceria,fsGeneralIzquieda,new RowLayoutData()).onSuccess(alCarroceria);						
										
					cmbTipoCarroceria.setValue(vehiculo.getAsString("Tipo Carroceria"));			
					txtColorCarroceria=new TextField("Color carrocer\u00eda");
					fsGeneralDerecha.add(txtColorCarroceria);
					txtColorCarroceria.setValue(vehiculo.getAsString("Color carroceria"));
					
					txtColorCabina=new TextField("Color cabina");
					fsGeneralDerecha.add(txtColorCabina);
					txtColorCabina.setValue(vehiculo.getAsString("Color"));
					
					cmbPropietario = new MyComboBox("Propietario");
					getStaticQueriesServiceAsync().getAllContactos(
					new RPCFillComplexComboHandler(cmbPropietario,
						fsGeneralIzquieda, new RowLayoutData()));
					
					cmbGeocerca= new MyComboBox("Geocerca Prpal:");
					getStaticQueriesServiceAsync().getGeocerca(
							idvehiculo,
							new RPCFillComplexComboHandler(cmbGeocerca,
								fsGeneralIzquieda, new RowLayoutData()));
					
					txtFechaIngreso=new TextField("Fecha de ingreso");
					txtFechaIngreso.setDisabled(true);
					fsGeneralDerecha.add(txtFechaIngreso);
			
			pGeneral.add(fsGeneralIzquieda, new ColumnLayoutData(250));
			pGeneral.add(fsGeneralDerecha, new ColumnLayoutData(1));
			tpVehiculos.add(pGeneral);
			
			pSeguro=new Panel("Seguro");
			pSeguro.setPaddings(15);
			pSeguro.setLayout(new HorizontalLayout(15));
			
			fsSeguroIzquieda=new FieldSet();
			fsSeguroIzquieda.setPaddings(5);
			fsSeguroIzquieda.setLabelWidth(120);
			fsSeguroIzquieda.setBorder(false);		
			fsSeguroIzquieda.setLayout(new FormLayout());
			
			fsSeguroDerecha=new FieldSet();
			fsSeguroDerecha.setPaddings(5);
			fsSeguroDerecha.setLabelWidth(120);
			fsSeguroDerecha.setBorder(false);		
			fsSeguroDerecha.setLayout(new FormLayout());
			
			ckAsegurado=new Checkbox("Asegurado");
			fsSeguroIzquieda.add(ckAsegurado);
			
			txtAseguradora=new TextField("Aseguradora");
			fsSeguroDerecha.add(txtAseguradora);
			
			txtTelEmergencia=new TextField("Tel. Emergencia");
			fsSeguroIzquieda.add(txtTelEmergencia);
						
			fsSeguroDerecha.add(ClientUtils.getSeparator());
			
			txtPoliza=new TextField("No. P\u00f3liza");
			fsSeguroIzquieda.add(txtPoliza);
			
			dfFechaPoliza=new DateField();
			dfFechaPoliza.setLabel("Fecha p\u00f3liza");
			fsSeguroDerecha.add(dfFechaPoliza);
			
			dfVigenciaPoliza= new DateField();
			dfVigenciaPoliza.setLabel("Vigencia p\u00f3liza");
			fsSeguroDerecha.add(dfVigenciaPoliza);
			pSeguro.add(fsSeguroIzquieda, new ColumnLayoutData(250));
			pSeguro.add(fsSeguroDerecha, new ColumnLayoutData(1));
			
			tpVehiculos.add(pSeguro);
						
			pContactos=new GenericAdminPanelAdapter("Contactos",null,false,false, true){
				@Override
				protected void loadGridFunction(
						AsyncCallback<ArrayList<RowModel>> rows) {
					getStaticQueriesServiceAsync().getContactosVehiculo(idvehiculo,rows);
				}
				
				@Override
				protected void addFunction(Record[] recs) {
					getStaticAdminQueriesServiceAsync().addContactoVehiculo(
						idvehiculo, getField(0).getValueAsString(),
						new MyBooleanAsyncCallback(reloadFunction));
				}
				
				@Override
				protected void deleteFunction(Record r) {
					getStaticAdminQueriesServiceAsync().delContactoVehiculo(idvehiculo, r.getAsString("id"), new MyBooleanAsyncCallback("Contacto desvinculado del vehiculo",reloadFunction));
				}
			};
			pContactos.addCombo("Contactos",new ComboLoader(){
				public void load(AsyncCallback<ArrayList<RowModel>> cb) {
					getStaticQueriesServiceAsync().getContactosProceso(gui.getProceso().getId(),cb);
				}
			}, true);
			pContactos.loadAllCombos();
			tpVehiculos.add(pContactos);
			
			pAlarmas = new GenericAdminPanelAdapter("Alarmas",null,false,false,true){
				@Override
				protected void loadGridFunction(
						AsyncCallback<ArrayList<RowModel>> rows) {
					getStaticQueriesServiceAsync().getAlarmas(idvehiculo, rows);
				}
				
				@Override
				protected void addFunction(Record[] recs) {
					getStaticAdminQueriesServiceAsync().addAlarma(idvehiculo,
						getField(0).getValueAsString(),
						getField(1).getValueAsString(),
						getField(2).getValueAsString(),
						new MyBooleanAsyncCallback("Alarma agregada",reloadFunction));
				}
				
				@Override
				protected void deleteFunction(Record r) {
					getStaticAdminQueriesServiceAsync().delAlarma(
						idvehiculo,
						r.getAsString("id"),
						new MyBooleanAsyncCallback(
								"Registro borrado exitosamente",reloadFunction));
				}
			};
			
			
			final ArrayList<RowModel> intensidad=new ArrayList<RowModel>();
			RowModelAdapter rma=new RowModelAdapter();
			rma.addValue("id", "hi");
			rma.addValue("desc", "hi");
			intensidad.add(rma);
			
			rma=new RowModelAdapter();
			rma.addValue("id", "low");
			rma.addValue("desc", "low");
			intensidad.add(rma);
			
			final ArrayList<RowModel> gpios=new ArrayList<RowModel>();
			RowModelAdapter rma2=new RowModelAdapter();
			rma2.addValue("id", "1");
			rma2.addValue("desc", "1");
			gpios.add(rma2);
			rma2=new RowModelAdapter();
			rma2.addValue("id", "2");
			rma2.addValue("desc", "2");
			gpios.add(rma2);
			pAlarmas.addCombo("Even" +
					"tos", new ComboLoader(){
				public void load(AsyncCallback<ArrayList<RowModel>> cb) {
					getStaticQueriesServiceAsync().getAllEventosVehiculo(idvehiculo,cb);
				}
			}, true);
			pAlarmas.addStaticCombo("Intensidad", intensidad);			
			/*pAlarmas.addCombo("Intensidad", new ComboLoader(){
				@Override
				public void load(AsyncCallback<ArrayList<RowModel>> cb) {
					getStaticQueriesServiceAsync().getArrayList(intensidad, cb);
				}
			},false);*/
			pAlarmas.addStaticCombo("GPIO",gpios);
			/*pAlarmas.addCombo("GPIO",new ComboLoader(){
				@Override
				public void load(AsyncCallback<ArrayList<RowModel>> cb) {
					getStaticQueriesServiceAsync().getArrayList(gpios, cb);					
				}
			},true);*/
						
			tpVehiculos.add(pAlarmas);
			
			
			
			
		this.add(tpVehiculos);
		
		pModem=new Panel("Modem");
		pModem.setLayout(new FormLayout());
		pModem.setPaddings(15);
		txtMin=new TextField("Min");
		pModem.add(txtMin);
		
		ckActivo=new Checkbox("Activo");
		pModem.add(ckActivo);
		
		txtPuk=new TextField("PUK");
		pModem.add(txtPuk);
		cmbTipoModem=new MyComboBox("Tipo");
		getStaticQueriesServiceAsync().getTiposModem(new RPCFillComboBoxHandler(cmbTipoModem,pModem,2));
		txtImei=new TextField("IMEI");
		pModem.add(txtImei);
		
		txtModemUser = new TextField("Usuario SIM");
		pModem.add(txtModemUser);
		
		txtModemPasswd = new TextField("Contrase\u00f1a SIM");
		pModem.add(txtModemPasswd);
		
		tpVehiculos.add(pModem);

		getStaticQueriesServiceAsync().getMoreInfoVehiculo(idvehiculo, new AsyncCallback<RowModel>(){
			public void onFailure(Throwable caught) {
				
			}
			public void onSuccess(RowModel result) {
				ckAsegurado.setValue(result.getRowValues()[0].equals("S"));
				
				try{ txtAseguradora.setValue(result.getRowValues()[1].toString());
				}catch(Exception e){}
				
				try{ txtTelEmergencia.setValue(result.getRowValues()[2].toString());
				}catch(Exception e){}
				
				try{ dfFechaPoliza.setValue(result.getRowValues()[3].toString());
				}catch(Exception e){}
				
				try{ dfVigenciaPoliza.setValue(result.getRowValues()[4].toString());
				}catch(Exception e){}
				
				try{ txtPoliza.setValue(result.getRowValues()[5].toString());
				}catch(Exception e){}
				
				try{ cmbPropietario.setValue(result.getRowValues()[6].toString());
				}catch(Exception e){}
				
				try{ cmbTipoCarroceria.setValue(result.getRowValues()[7].toString());
				}catch(Exception e){}
				
				try{ txtColorCarroceria.setValue(result.getRowValues()[8].toString());
				}catch(Exception e){}
				
				ckActivo.setValue(true);
				//estado, tipo_modem, modem_min, modem_puk, modem_imei
								
				cmbTipoModem.setValue(result.getRowValues()[10].toString());
				txtMin.setValue(result.getRowValues()[11].toString());
				txtPuk.setValue(result.getRowValues()[12].toString());
				txtImei.setValue(result.getRowValues()[13].toString());
				txtFechaIngreso.setValue(result.getRowValues()[14].toString());
				
				try{ txtModemUser.setValue(result.getRowValues()[15].toString());
				}catch(Exception e){}
				
				try{ txtModemPasswd.setValue(result.getRowValues()[16].toString());
				}catch(Exception e){}
				
			}		
		});
		
		
		
		Button guardar=new Button("Guardar");
		guardar.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				getStaticAdminQueriesServiceAsync().updateInsurance(idvehiculo,
						txtPlaca.getValueAsString(),
						txtTipo.getValueAsString(),
						txtMarca.getValueAsString(),
						txtCiudad.getValueAsString(),
						txtColorCabina.getValueAsString(),
						txtModelo.getValueAsString(),
						cmbTipoCarroceria.getValueAsString(),
						txtColorCarroceria.getValueAsString(),
						ckAsegurado.getValue(),
						txtAseguradora.getValueAsString(),
						txtTelEmergencia.getValueAsString(),
						dfFechaPoliza.getValueAsString(),
						dfVigenciaPoliza.getValueAsString(),
						txtPoliza.getValueAsString(), ckActivo.getValue(),
						cmbTipoModem.getValue(), txtMin.getValueAsString(),
						txtPuk.getValueAsString(), txtImei.getValueAsString(),
						cmbPropietario.getValue(), txtModemUser.getValueAsString(), 
						txtModemPasswd.getValueAsString(),
						cmbGeocerca.getValueAsString(),
						new MyBooleanAsyncCallback("Vehiculo actualizado"));
				
				//estado, tipo_modem, modem_min, modem_puk, modem_imei
			}
		});
		
		this.addButton(guardar);
		setVisible(true);
		pAlarmas.loadAllCombos();
	}
}
