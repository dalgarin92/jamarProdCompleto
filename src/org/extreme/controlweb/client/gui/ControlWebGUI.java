package org.extreme.controlweb.client.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.extreme.controlweb.client.admin.listeners.AdminTreeNodeClickListener;
import org.extreme.controlweb.client.core.Contratista;
import org.extreme.controlweb.client.core.GroupedRowModelAdapter;
import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.ProcesoLight;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.core.Turno;
import org.extreme.controlweb.client.core.Usuario;
import org.extreme.controlweb.client.handlers.LoginWindowHandler;
import org.extreme.controlweb.client.handlers.RPCFillGroupedGridPanel;
import org.extreme.controlweb.client.handlers.RPCTreeLoader;
import org.extreme.controlweb.client.handlers.ReloadTreeHandler;
import org.extreme.controlweb.client.listeners.AdminInterfaceListener;
import org.extreme.controlweb.client.listeners.AssignProgWorkItemListener;
import org.extreme.controlweb.client.listeners.ChangePriorityButtonListener;
import org.extreme.controlweb.client.listeners.ControldeFlotasItemListener;
import org.extreme.controlweb.client.listeners.DragAndDropTreePanelListener;
import org.extreme.controlweb.client.listeners.FavButtonListener;
import org.extreme.controlweb.client.listeners.LeafTreeClickListener;
import org.extreme.controlweb.client.listeners.LoginKeyListener;
import org.extreme.controlweb.client.listeners.MultiAssignListener;
import org.extreme.controlweb.client.listeners.NodeTreeClickListener;
import org.extreme.controlweb.client.listeners.PathStartButtonListener;
import org.extreme.controlweb.client.listeners.ReloadButtonListener;
import org.extreme.controlweb.client.listeners.RepsButtonListener;
import org.extreme.controlweb.client.listeners.ResetButtonListener;
import org.extreme.controlweb.client.listeners.RightClickTreeNodeListener;
import org.extreme.controlweb.client.listeners.SearchDirButtonListener;
import org.extreme.controlweb.client.listeners.SearchWorkButtonListener;
import org.extreme.controlweb.client.listeners.SincronizarGeocercasItemListener;
import org.extreme.controlweb.client.listeners.ViewClosedWorksItemListener;
import org.extreme.controlweb.client.listeners.ViewEventsItemListener;
import org.extreme.controlweb.client.listeners.ViewInterestPointListener;
import org.extreme.controlweb.client.listeners.ViewParkedVehsItemListener;
import org.extreme.controlweb.client.map.openlayers.MapClickEventListener;
import org.extreme.controlweb.client.map.openlayers.OpenLayersMap;
import org.extreme.controlweb.client.programmer.ViewRouteProgrammerItemListener;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.stats.listeners.CentralComparativeReportEventsListener;
import org.extreme.controlweb.client.stats.listeners.CentralComparativeReportItemListener;
import org.extreme.controlweb.client.stats.listeners.ComparativeManagerReportEventsListener;
import org.extreme.controlweb.client.stats.listeners.ComparativeManagerReportOdomListener;
import org.extreme.controlweb.client.stats.listeners.ComparativeReportItemListener;
import org.extreme.controlweb.client.stats.listeners.ComparativeReportOdoListener;
import org.extreme.controlweb.client.stats.listeners.ManagerReportItemListener;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.util.JSObjectWrapper;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.FxConfig;
import com.gwtext.client.core.Margins;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.Node;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.Store;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Component;
import com.gwtext.client.widgets.Container;
import com.gwtext.client.widgets.DatePicker;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.ToolbarTextItem;
import com.gwtext.client.widgets.Viewport;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.DatePickerListenerAdapter;
import com.gwtext.client.widgets.event.TabPanelListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.CheckItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.MenuItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.menu.event.CheckItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class ControlWebGUI implements EntryPoint {

	private PopupPanel messagePanel;
	private TabPanel centerPanel;
	private Menu menu;
	private TreePanel treePanel;
	private OpenLayersMap map;

	// private ComboBox cbEmpresas;
	private TabPanel westTabPanel;
	private Window loginWindow;
	private TextField tfUser;
	private TextField tfPasswd;
	private Toolbar treeOptionsToolbar;
	private String currentProcess;
	private String idPerfil;
	private boolean webadmin;
	// private boolean onlyTrack;
	private Item searchTrabButton;
	private Timer timer;
	private Store storeRecWithoutVeh;
	private ToolbarButton enableRefreshButton;
	protected boolean timerStarted;
	private Timer timeOutTimer;
	private ControlWebGUI gui;

	// private Boolean abortTreeLoad;

	/*
	 * private String nombreBD; private String userBD; private String passwdBD;
	 */

	private Proceso proc;
	private Usuario usuario;
	private ToolbarButton btnReload;
	private boolean doreload = false;
	private TreeNode noUbicTreeNode;
	private TreeNode noAsigTreeNode;
	private Toolbar toolbarMapPanel;
	private ToolbarButton favProcToggleButton;
	private ToolbarButton addFavButton;
	private ToolbarButton favUserToggleButton;
	public ReloadTreeHandler reloader;
	private ArrayList<Marker> procMarkers;
	private ArrayList<Marker> userMarkers;
	private QueriesServiceAsync serviceProxy;
	private AdminQueriesServiceAsync adminServiceProxy;
	private Menu mainMenu;
	private Menu menuTrab;
	private Menu menuRep;
	private Menu menuProceso;
	private Menu menuEstadisticas;

	private boolean isPrecerrado;
	private String fechaentrega;
	private String horaentrega;
	private String idjornada;
	private DateField dfDateProg;

	private ToolbarButton multiSelectToggleButton;
	private ToolbarButton acceptSelectionButton;

	private static String atraso1 = "";
	private static String atraso2 = "";

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ToolbarButton getFavProcToggleButton() {
		return favProcToggleButton;
	}

	public ToolbarButton getFavUserToggleButton() {
		return favUserToggleButton;
	}

	public void setProcMarkers(ArrayList<Marker> procMarkers) {
		this.procMarkers = procMarkers;
	}

	public void setUserMarkers(ArrayList<Marker> userMarkers) {
		this.userMarkers = userMarkers;
	}

	public ArrayList<Marker> getProcMarkers() {
		return procMarkers;
	}

	public ArrayList<Marker> getUserMarkers() {
		return userMarkers;
	}

	public void setStoreRecWithoutVeh(Store storeRecWithoutVeh) {
		this.storeRecWithoutVeh = storeRecWithoutVeh;
	}

	public Store getStoreRecWithoutVeh() {
		return storeRecWithoutVeh;
	}

	@Deprecated
	public String getCurrentProcess() {
		return currentProcess;
	}

	public String getPerfil() {
		return idPerfil;
	}

	public void setWebAdmin(boolean webadmin) {
		this.webadmin = webadmin;
	}

	public boolean getWebAdmin() {
		return webadmin;
	}

	public void setProceso(Proceso proc) {
		this.proc = proc;
	}

	public Proceso getProceso() {
		return proc;
	}

	public String getTxtUser() {
		return tfUser.getText();
	}

	public String getTxtPasswd() {
		return tfPasswd.getText();
	}

	public void setTxtPasswd(TextField tf) {
		tfPasswd = tf;
	}

	public DateField getDfDateProg() {
		return dfDateProg;
	}

	public void reloadTree() {
		reloader.reloadTree();
	}

	/*
	 * @Deprecated public void setComboEmp(ComboBox cb) { cbEmpresas = cb; }
	 */

	/*
	 * public String getNombreBD() { return nombreBD; }
	 * 
	 * public void setNombreBD(String nombreBD) { this.nombreBD = nombreBD; }
	 * 
	 * public String getUserBD() { return userBD; }
	 * 
	 * public void setUserBD(String userBD) { this.userBD = userBD; }
	 * 
	 * public String getPasswdBD() { return passwdBD; }
	 * 
	 * public void setPasswdBD(String passwdBD) { this.passwdBD = passwdBD; }
	 */

	public void addRecWithoutVeh(String idrec, String idcont, String nombrerec) {
		if (storeRecWithoutVeh.indexOfId(idrec + idcont) == -1) {
			Record record = ClientUtils.RECORD_DEF_RECS_NO_VEH.createRecord(new String[] { idrec, idcont, nombrerec });
			record.setId(idrec + idcont);
			storeRecWithoutVeh.add(record);
			storeRecWithoutVeh.commitChanges();
		}
	}

	/*
	 * public String getComboEmp() { return cbEmpresas.getValue(); }
	 */

	public void addInfoTab(Panel panel) {
		addInfoTab(panel, null);
	}

	public void addInfoTab(Panel panel, Long l) {

		if (getTab("info" + (l != null ? String.valueOf(l) : "")) != null) {
			centerPanel.remove(getTab("info" + (l != null ? String.valueOf(l) : "")), true);
			// getTab("info").destroy();
		}
		panel.setId("info" + (l != null ? String.valueOf(l) : ""));

		centerPanel.add(panel);
		centerPanel.setActiveTab("info" + (l != null ? String.valueOf(l) : ""));
	}

	public Panel[] addStatsTab(String title) {
		Panel p = new Panel(title);
		p.setLayout(new RowLayout());
		p.setClosable(true);

		Panel p1 = new Panel();
		p1.setLayout(new FitLayout());
		p1.setPaddings(5);
		// p1.setBorder(false);

		Panel chartPanel = new Panel();
		chartPanel.setLayout(new FitLayout());
		chartPanel.setBorder(false);

		Panel p2 = new Panel();
		p2.setLayout(new RowLayout());
		p2.setPaddings(5);
		p2.setBorder(false);

		p.add(p1, new RowLayoutData("60%"));
		p.add(p2, new RowLayoutData("40%"));

		p1.add(chartPanel);

		addInfoTab(p, new Date().getTime() % 10000);

		return new Panel[] { chartPanel, p2 };
	}

	public Component getTab(String id) {
		return centerPanel.findByID(id);
	}

	public void setTreeToolbarDisabled(boolean bool) {
		treeOptionsToolbar.setDisabled(bool);
	}

	public void getTreeNodes(TreeNode root, String tipo, ArrayList<TreeNode> nodes) {
		Node[] childs = root.getChildNodes();
		for (int i = 0; i < childs.length; i++) {
			getTreeNodes((TreeNode) childs[i], tipo, nodes);
		}
		// if(root.getAttribute("tiponodo") != null &&
		// root.getAttribute("tiponodo").equals(tipo))

		if (root.getAttributeAsObject("entity") != null
				&& root.getAttributeAsObject("entity").getClass().getName().equals(tipo)) {
			nodes.add(root);
		}
	}

	public void getTreeNodes(String tipo, ArrayList<TreeNode> nodes) {
		getTreeNodes(treePanel.getRootNode(), tipo, nodes);
	}

	public ArrayList<Object> getMarkers(TreeNode node) {
		ArrayList<Object> v = new ArrayList<Object>();
		Object obj = node.getAttributeAsObject("marker");
		if (obj instanceof Marker) {
			v.add(obj);
		}
		Node[] ns = node.getChildNodes();
		for (int i = 0; i < ns.length; i++) {
			obj = ns[i].getAttributeAsObject("marker");
			if (obj instanceof Marker) {
				v.add(obj);
			}
		}
		return v;
	}

	public QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
		}
		return serviceProxy;
	}

	public AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}

	/*
	 * public void refreshNoUbicNoAsigTrabs(TreeNode selectedNode) { final
	 * TreeNode parent = (TreeNode) selectedNode.getParentNode();
	 * treePanel.getEl().mask("Actualizando Registros", "x-mask-loading");
	 * serviceProxy.getNoUbicNoAsigTrabs(getProceso().getId(), getDfDateProg()
	 * .getEl().getValue(), new AsyncCallback<ProcesoLight>() {
	 * 
	 * public void onFailure(Throwable caught) { ClientUtils.alert("Error",
	 * caught.getMessage(), ClientUtils.ERROR); getTree().getEl().unmask();
	 * GWT.log("Error RPC", caught); }
	 * 
	 * public void onSuccess(ProcesoLight result) { clearNoUbicNoAsigNodes();
	 * addTrabTreeNodes(result.getTrabsNoUbic(), getNoUbicTreeNode(), false,
	 * false, ControlWebGUI.this); addTrabTreeNodes(result.getTrabsNoAsig(),
	 * getNoAsigTreeNode(), true, false, ControlWebGUI.this);
	 * getTree().getEl().unmask(); parent.expand(); } }); }
	 */

	public boolean searchAndSelectWork(String text) {
		ArrayList<TreeNode> a = new ArrayList<TreeNode>();
		getTreeNodes(Trabajo.class.getName(), a);
		int i = 0;
		boolean sw = false;
		while (i < a.size() && !sw) {
			TreeNode node = a.get(i);
			Trabajo trab = (Trabajo) node.getAttributeAsObject("entity");
			if (trab.getId().equals(text) || trab.getId().split("-")[1].equals(text)
					|| trab.getDireccion().equals(text)) {
				node.ensureVisible();
				node.select();
				new LeafTreeClickListener(this).onClick(node, null);
				sw = true;
			}
			i++;
		}
		return sw;
	}

	public boolean searchAndSelectWork(String text, boolean activeTabMap) {
		ArrayList<TreeNode> a = new ArrayList<TreeNode>();
		getTreeNodes(Trabajo.class.getName(), a);
		int i = 0;
		boolean sw = false;
		while (i < a.size() && !sw) {
			TreeNode node = a.get(i);
			Trabajo trab = (Trabajo) node.getAttributeAsObject("entity");
			if (trab.getId().equals(text)
					// || trab.getId().split("-")[1].equals(text)
					|| trab.getDireccion().equals(text)) {
				node.ensureVisible();
				node.select();
				new LeafTreeClickListener(this).onClick(node, null);
				sw = true;
				if (activeTabMap) {
					centerPanel.setActiveTab(0);
				}
			}
			i++;
		}
		return sw;
	}

	public TreeNode searchRecurso(String idrec, String idcont) {
		ArrayList<TreeNode> a = new ArrayList<TreeNode>();
		getTreeNodes(Recurso.class.getName(), a);
		int i = 0;
		boolean sw = false;
		TreeNode node = null;
		while (i < a.size() && !sw) {
			node = a.get(i);
			Recurso rec = (Recurso) node.getAttributeAsObject("entity");
			if (rec.getId().equals(idrec) && rec.getCont().getId().equals(idcont)) {
				node.ensureVisible();
				node.select();
				sw = true;
			}
			i++;
		}
		if (sw) {
			return node;
		} else {
			return null;
		}
	}

	@Override
	public void onModuleLoad() {
		ClientUtils.initialize(ClientUtils.CURRENT_ENVIRONMENT);
		loginWindow = new Window(ClientUtils.APP_NAME + " Login", true, false);
		loginWindow.setIconCls("icon-butterfly");
		loginWindow.setLayout(new FitLayout());
		reloader = new ReloadTreeHandler(this);
		storeRecWithoutVeh = new Store(ClientUtils.RECORD_DEF_RECS_NO_VEH);

		ClientUtils.CENTER_LAYOUT_DATA.setMargins(new Margins(5, 0, 5, 5));
		ClientUtils.CENTER_LAYOUT_DATA.setCMargins(new Margins(5, 5, 5, 5));
		FieldSet loginSet = new FieldSet();
		// loginSet.setLabelWidth(90);
		loginSet.setBorder(false);
		loginSet.setLayout(new FormLayout());
		loginSet.setPaddings(5, 5, 5, 0);

		tfUser = new TextField("Usuario", "user");
		loginSet.add(tfUser, new AnchorLayoutData("100%"));

		tfPasswd = new TextField("Contrase\u00f1a", "passwd");
		tfPasswd.setInputType("password");

		// keyCode 13 = ENTER
		tfPasswd.addKeyListener(13, new LoginKeyListener(this));

		loginSet.add(tfPasswd, new AnchorLayoutData("100%"));

		// cbEmpresas = new ComboBox("Empresa");

		loginWindow.setPaddings(0, 0, 0, 0);
		loginWindow.setClosable(false);

		loginWindow.setHeight(180);
		loginWindow.setWidth(410);

		Image image = new Image("images/utiles/login.png");
		Panel imagePanel = new Panel();
		imagePanel.setPaddings(15, 15, 0, 0);
		imagePanel.setBorder(false);
		imagePanel.add(image);
		MultiFieldPanel mfpLogin = new MultiFieldPanel();
		mfpLogin.setPaddings(15, 0, 0, 0);
		mfpLogin.addToRow(imagePanel, 70);
		mfpLogin.addToRow(loginSet, new ColumnLayoutData(1));
		loginWindow.add(mfpLogin);

		Button btnOK = new Button("Entrar", new LoginWindowHandler(loginWindow, this));
		loginWindow.setButtonAlign(Position.CENTER);
		loginWindow.addButton(btnOK);

		/*
		 * serviceProxy = (QueriesServiceAsync)
		 * GWT.create(QueriesService.class); ((ServiceDefTarget)
		 * serviceProxy).setServiceEntryPoint(ClientUtils.QUERIES_SERVICE_URL);
		 * serviceProxy.getEmpresas(new RPCFillComboBoxHandler(cbEmpresas,
		 * loginSet, new AnchorLayoutData("100%")));
		 */

		loginWindow.show();
	}

	public QueriesServiceAsync getQueriesServiceProxy() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
		}
		return serviceProxy;
	}

	public void loadMainUI(String perfil, String nomUser, ComboBox cbProceso) {
		/*
		 * MyDraggableGridForm mdgf=new MyDraggableGridForm("ejemplo");
		 * mdgf.show();
		 */
		loginWindow.destroy();
		Panel mainPanel = new Panel();
		mainPanel.setLayout(new BorderLayout());

		idPerfil = perfil;

		BorderLayoutData northLayoutData = new BorderLayoutData(RegionPosition.NORTH);
		northLayoutData.setSplit(false);

		BorderLayoutData centerLayoutData = new BorderLayoutData(RegionPosition.CENTER);
		centerLayoutData.setMargins(new Margins(5, 0, 5, 5));

		Panel centerPanelWrappper = new Panel();
		centerPanelWrappper.setLayout(new FitLayout());
		centerPanelWrappper.setBorder(false);
		centerPanelWrappper.setBodyBorder(false);

		centerPanel = new TabPanel();
		centerPanel.setBodyBorder(false);
		centerPanel.setEnableTabScroll(true);
		centerPanel.setAutoScroll(true);
		centerPanel.setAutoDestroy(false);
		centerPanel.setActiveTab(0);

		// hide the panel when the tab is closed
		centerPanel.addListener(new TabPanelListenerAdapter() {

			@Override
			public void onTabChange(TabPanel source, Panel tab) {
				if (tab.getTitle().indexOf("Mapa") != -1) {

					westTabPanel.setActiveTab(0);

				}
			}

			@Override
			public void onRemove(Container self, Component component) {
				component.destroy();
			}

			@Override
			public void onContextMenu(TabPanel source, Panel tab, EventObject e) {
				showMenu(tab, e);
			}
		});

		centerPanel.setLayoutOnTabChange(true);
		centerPanel.setTitle("   ");

		BorderLayoutData westLayoutData = new BorderLayoutData(RegionPosition.WEST);
		westLayoutData.setMargins(new Margins(5, 5, 0, 5));
		westLayoutData.setCMargins(new Margins(5, 5, 5, 5));
		westLayoutData.setMinSize(155);
		westLayoutData.setMaxSize(350);
		westLayoutData.setSplit(true);

		final Panel mapPanel = new Panel();
		mapPanel.setTitle("Mapa");
		mapPanel.setHeader(false);

		mapPanel.setId("mappanel");
		mapPanel.setPaddings(10);
		mapPanel.setLayout(new FitLayout());

		toolbarMapPanel = new Toolbar();
		ToolbarTextItem welcome = new ToolbarTextItem("  \tBienvenido, " + nomUser);
		toolbarMapPanel.addItem(welcome);

		toolbarMapPanel.addSeparator();

		multiSelectToggleButton = new ToolbarButton("Asig. M\u00faltiple", new ButtonListenerAdapter() {
			@Override
			public void onToggle(Button button, boolean pressed) {
				if (pressed) {
					ControlWebGUI.this.getMap().activateSelection();
					acceptSelectionButton.setVisible(true);
				} else {
					ControlWebGUI.this.getMap().deactivateSelection();
					button.setText(button.getText().split(" \\(")[0]);
					acceptSelectionButton.setVisible(false);
				}
			}
		}, "images/utiles/icons/select.png");
		multiSelectToggleButton.setTooltip("Asignaci\u00f3n M\u00faltiple");
		multiSelectToggleButton.setEnableToggle(true);
		multiSelectToggleButton.setToggleGroup("multiselect");
		toolbarMapPanel.addButton(multiSelectToggleButton);

		acceptSelectionButton = new ToolbarButton("OK", new MultiAssignListener(this), "images/utiles/icons/ok.png");
		acceptSelectionButton.setTooltip("Aceptar Asignaci\u00f3n M\u00faltiple");
		toolbarMapPanel.addButton(acceptSelectionButton);
		acceptSelectionButton.setVisible(false);

		toolbarMapPanel.addSpacer();
		toolbarMapPanel.addSeparator();

		favProcToggleButton = new ToolbarButton();
		favProcToggleButton.setCls("x-btn-icon icon-favproc-toolbar");
		favProcToggleButton.setTooltip("Puntos de Inter\u00e9s Globales");
		favProcToggleButton.setEnableToggle(true);
		favProcToggleButton.setToggleGroup("tmappanel");

		favUserToggleButton = new ToolbarButton();
		favUserToggleButton.setCls("x-btn-icon icon-favuser-toolbar");
		favUserToggleButton.setTooltip("Mis Puntos de Inter\u00e9s");
		favUserToggleButton.setEnableToggle(true);
		favUserToggleButton.setToggleGroup("tmappanel2");

		favProcToggleButton.addListener(new FavButtonListener(this, "proc"));
		favUserToggleButton.addListener(new FavButtonListener(this, "user"));

		addFavButton = new ToolbarButton();
		addFavButton.setCls("x-btn-icon icon-addfav-toolbar");
		addFavButton.setTooltip("Gestionar Puntos de Inter\u00e9s");

		// addFavButton.addListener(new AddFavButtonListener(this));
		addFavButton.addListener(new ViewInterestPointListener(this));

		toolbarMapPanel.addButton(favProcToggleButton);
		toolbarMapPanel.addButton(favUserToggleButton);
		toolbarMapPanel.addButton(addFavButton);

		toolbarMapPanel.addFill();

		ToolbarButton tbSupport = new ToolbarButton("Soporte", new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				Window wnd = new Window("Soporte");
				wnd.setHeight(345);
				wnd.setWidth(265);
				wnd.setResizable(false);
				// wnd.setMinimizable(true);
				/*
				 * wnd.setHtml("<object width='250' height='300' >" +
				 * "<param name='movie' value='http://widget.meebo.com/mm.swf?weQHSgNXIi'/>"
				 * + "<embed src='http://widget.meebo.com/mm.swf?weQHSgNXIi' " +
				 * "type='application/x-shockwave-flash' " +
				 * "width='250' height='300'></embed></object>");
				 */
				wnd.setHtml("<center><div class=\"vertical-column-40\" data-delay=\"0\">"
						+ "<br><br><h3>Linea Nacional</h3><p><img src=\"images/soporte/linea-tel.png\"></p></div>"
						+ "</br>" + "</br>" + "<div class=\"vertical-column-40\" data-delay=\"0\">"
						+ "<h3>Linea Local</h3><p><img src=\"images/soporte/linea-barranquilla.png\"></p></div>"
						+ "</br>" + "</br>" + "<div class=\"vertical-column-40\" data-delay=\"0\">"
						+ "<h3>Correo electr\u00f3nico</h3><p>soporte@extreme.com.co</p></div></center>");
				wnd.show();
			}
		}, "images/utiles/icons/help_16.png");
		
		
		toolbarMapPanel.addButton(tbSupport);
		toolbarMapPanel.addSeparator();

		ToolbarButton logout = new ToolbarButton("Cerrar Sesi\u00f3n", new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				logout();
			}
		}, "images/utiles/exit.png");

		toolbarMapPanel.addButton(logout);
		mapPanel.setTopToolbar(toolbarMapPanel);

		map = new OpenLayersMap(this);
		mapPanel.add(map, centerLayoutData);

		centerPanel.add(mapPanel, centerLayoutData);
		centerPanelWrappper.add(centerPanel);
		mainPanel.add(centerPanelWrappper, centerLayoutData);

		Panel westPanel = createWestPanel(ClientUtils.APP_NAME, nomUser, cbProceso);
		mainPanel.add(westPanel, westLayoutData);
		new Viewport(mainPanel);

		LonLat ll = ClientUtils.DEFAULT_CENTER;
		ll.transform("EPSG:4326", "EPSG:900913");
		map.getMap().setCenter(ll);
	}

	private String getAndTranslateDay(Date date) {
		String res = "";
		DateTimeFormat dtf = DateTimeFormat.getFormat("EEEE");
		String day = dtf.format(date);
		if (day.equals("Monday")) {
			res = "Lunes";
		} else if (day.equals("Tuesday")) {
			res = "Martes";
		} else if (day.equals("Wednesday")) {
			res = "Mi\u00e9rcoles";
		} else if (day.equals("Thursday")) {
			res = "Jueves";
		} else if (day.equals("Friday")) {
			res = "Viernes";
		} else if (day.equals("Saturday")) {
			res = "S\u00e1bado";
		} else if (day.equals("Sunday")) {
			res = "Domingo";
		}
		return res;
	}

	private Panel createWestPanel(String title, String nomUser, ComboBox cbProcesos) {
		westTabPanel = new TabPanel();
		Panel westPanel = new Panel();
		Panel innerPanel = new Panel("Procesos");
		innerPanel.setLayout(new FitLayout());
		innerPanel.setWidth(350);
		westPanel.setId("side-nav");
		westPanel.setTitle(title);
		westPanel.setLayout(new FitLayout());
		westPanel.setWidth(350);
		westPanel.setCollapsible(true);

		Panel innerPanel2 = new Panel();
		innerPanel2.setLayout(new FitLayout());
		innerPanel2.setBorder(false);

		Toolbar dateToolbar = new Toolbar();
		// dateToolbar.addFill();
		dateToolbar.addItem(new ToolbarTextItem("Fecha:"));

		dfDateProg = new DateField();
		dfDateProg.setFormat("Y/m/d");
		dfDateProg.setReadOnly(true);
		dfDateProg.setWidth(120);
		dfDateProg.setValue(new Date());

		dfDateProg.setDisabled(true);

		dateToolbar.addField(dfDateProg);
		dateToolbar.addSeparator();

		final ToolbarTextItem tti;
		dateToolbar.addItem(tti = new ToolbarTextItem("<u>" + getAndTranslateDay(dfDateProg.getValue()) + "</u>"));
		dfDateProg.addListener(new DatePickerListenerAdapter() {
			@Override
			public void onSelect(DatePicker dataPicker, Date date) {
				tti.setText("<u>" + getAndTranslateDay(date) + "</u>");
			}
		});

		innerPanel2.setTopToolbar(dateToolbar);

		Toolbar tbProceso = new Toolbar();
		tbProceso.addItem(new ToolbarTextItem("Proceso:"));

		cbProcesos.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				executeReload(record.getAsString("id"), null);
			}

		});
		cbProcesos.setWidth(200);
		tbProceso.addField(cbProcesos);

		tbProceso.addSeparator();

		btnReload = new ToolbarButton("Cargar");

		btnReload.addListener(new ReloadButtonListener(this));
		btnReload.setTooltip("Marque en el \u00e1rbol los recursos a visualizar.");
		btnReload.setTooltipType(true);

		tbProceso.addButton(btnReload);

		btnReload.setVisible(false);

		innerPanel.setTopToolbar(tbProceso);

		/*
		 * TabPanel tabPanel = new TabPanel(); tabPanel.setActiveTab(0);
		 * tabPanel.setDeferredRender(true);
		 * tabPanel.setTabPosition(Position.BOTTOM);
		 */
		TreePanel treePanel = getTreeNav();

		innerPanel2.add(treePanel);
		innerPanel.add(innerPanel2);
		westTabPanel.add(innerPanel);
		if (webadmin && !idPerfil.equals("user")
				|| !webadmin && (idPerfil.equals("admin") || idPerfil.equals("superadmin"))) {
			westTabPanel.add(createAdminPanel());
		}
		westPanel.add(westTabPanel);
		return westPanel;
	}

	public void highlightReloadButton() {
		btnReload.getEl().frame("#ffff9c", 5, new FxConfig());
	}

	public void setVisibleReloadButton(boolean v) {
		btnReload.setVisible(v);
	}

	public void startTimer() {
		if (!timerStarted && enableRefreshButton.isPressed()) {
			if (!getProceso().isSoloCentral()) {
				timer.scheduleRepeating(ClientUtils.DEFAULT_REFRESH_RATE);
			} else {
				timer.scheduleRepeating(ClientUtils.CENTRAL_REFRESH_RATE);
			}
		}
		timerStarted = true;
	}

	public void restartTimer() {
		enableRefreshButton.toggle(true);
		// loadProceso(treePanel, currentProcess, false);
		startTimer();
	}

	public void stopTimer() {
		timer.cancel();
		timerStarted = false;
		enableRefreshButton.toggle(false);
	}

	private Panel createAdminPanel() {
		TreePanel adminPanel = new TreePanel("Administraci\u00f3n");
		adminPanel.setId("admin-tree");
		adminPanel.setCollapsible(false);
		adminPanel.setAnimate(true);
		adminPanel.setAutoScroll(true);
		adminPanel.setContainerScroll(true);
		adminPanel.setRootVisible(true);
		adminPanel.setBorder(false);
		adminPanel.setUseArrows(true);
		// adminPanel.setTopToolbar(getTreeOptionsToolbar(false, adminPanel));

		TreeNode mainConfiguration = new TreeNode("Configuraci\u00f3n General");
		TreeNode jobsConfiguration = new TreeNode("Configuraci\u00f3n de Trabajos");
		TreeNode recsConfiguration = new TreeNode("Configuraci\u00f3n de Recursos");
		TreeNode prgvhConfiguration = new TreeNode("Configuraci\u00f3n de Flotas de Vehiculos");
		TreeNode fileConfiguration = new TreeNode("Archivos");
		TreeNode threePartyConfiguration = new TreeNode("Terceros");

		AdminTreeNodeClickListener l = new AdminTreeNodeClickListener(centerPanel, this);

		TreeNode root = new TreeNode("Administrar", "folder-icon");
		TreeNode progVeh = new TreeNode(AdminTreeNodeClickListener.PROG_VEHIC);
		TreeNode muelles = new TreeNode(AdminTreeNodeClickListener.MUELLES);
		TreeNode destinos = new TreeNode(AdminTreeNodeClickListener.DESTINOS);
		TreeNode trabNode = new TreeNode(AdminTreeNodeClickListener.JOBS_LABEL, "icon-work");
		TreeNode recNode = new TreeNode(AdminTreeNodeClickListener.RECS_LABEL, "icon-rec");
		TreeNode munNode = new TreeNode(AdminTreeNodeClickListener.MUN_LABEL);
		TreeNode tpoRecNode = new TreeNode(AdminTreeNodeClickListener.RECS_TYPES_LABEL);
		TreeNode tpoTrabNode = new TreeNode(AdminTreeNodeClickListener.JOBS_TYPES_LABEL);
		TreeNode contNode = new TreeNode(AdminTreeNodeClickListener.CONT_LABEL, "icon-cont");
		TreeNode cliNode = new TreeNode(AdminTreeNodeClickListener.CLIENTS_LABEL);
		TreeNode motRecNode = new TreeNode(AdminTreeNodeClickListener.REJECT_SUBJECT_LABEL);

		TreeNode jornadas = new TreeNode(AdminTreeNodeClickListener.JORNADAS_LABEL);

		TreeNode horarios = new TreeNode(AdminTreeNodeClickListener.HORARIOS_LABEL);

		TreeNode userNode = new TreeNode(AdminTreeNodeClickListener.USERS_LABEL);
		TreeNode procNode = new TreeNode(AdminTreeNodeClickListener.PROCESS_LABEL);
		TreeNode catListasNode = new TreeNode(AdminTreeNodeClickListener.LIST_CATEGORY_LABEL);
		TreeNode vehNode = new TreeNode(AdminTreeNodeClickListener.VEHICLES_LABEL);

		TreeNode uploadFilesNode = new TreeNode(AdminTreeNodeClickListener.UPLOAD_FILES_LABEL);
		TreeNode templatesNode = new TreeNode(AdminTreeNodeClickListener.TEMPLATES_FILES_LABEL);

		TreeNode ownersNode = new TreeNode(AdminTreeNodeClickListener.CONTACTS_LABEL);
		TreeNode driversNode = new TreeNode(AdminTreeNodeClickListener.DRIVERS_LABEL);

		trabNode.addListener(l);
		progVeh.addListener(l);
		muelles.addListener(l);
		destinos.addListener(l);
		recNode.addListener(l);
		munNode.addListener(l);
		tpoRecNode.addListener(l);
		tpoTrabNode.addListener(l);
		contNode.addListener(l);
		cliNode.addListener(l);
		motRecNode.addListener(l);
		userNode.addListener(l);
		procNode.addListener(l);
		catListasNode.addListener(l);
		vehNode.addListener(l);

		jornadas.addListener(l);
		horarios.addListener(l);

		uploadFilesNode.addListener(l);
		templatesNode.addListener(l);

		ownersNode.addListener(l);
		driversNode.addListener(l);

		if (webadmin && !idPerfil.equals("user") || idPerfil.equals("superadmin")) {
			jobsConfiguration.appendChild(trabNode);
			recsConfiguration.appendChild(recNode);
			recsConfiguration.appendChild(driversNode);
			root.appendChild(recsConfiguration);
			root.appendChild(jobsConfiguration);
		}

		if (idPerfil.equals("admin") || idPerfil.equals("superadmin")) {
			recsConfiguration.appendChild(tpoRecNode);
			jobsConfiguration.appendChild(tpoTrabNode);
			jobsConfiguration.appendChild(munNode);
			jobsConfiguration.appendChild(catListasNode);

			mainConfiguration.appendChild(motRecNode);
			mainConfiguration.appendChild(userNode);

			mainConfiguration.appendChild(horarios);
			mainConfiguration.appendChild(jornadas);

			fileConfiguration.appendChild(templatesNode);

			threePartyConfiguration.appendChild(contNode);
			threePartyConfiguration.appendChild(cliNode);

			recsConfiguration.appendChild(vehNode);

			root.appendChild(threePartyConfiguration);
			root.appendChild(mainConfiguration);

		}

		if (idPerfil.equals("oper") || idPerfil.equals("admin") || idPerfil.equals("superadmin")) {
			fileConfiguration.appendChild(uploadFilesNode);
			prgvhConfiguration.appendChild(progVeh);
			prgvhConfiguration.appendChild(muelles);
			prgvhConfiguration.appendChild(destinos);
			root.appendChild(prgvhConfiguration);
			root.appendChild(fileConfiguration);

		}

		if (idPerfil.equals("superadmin")) {
			recsConfiguration.appendChild(ownersNode);
			mainConfiguration.appendChild(procNode);
		}
		adminPanel.setRootNode(root);
		adminPanel.expandAll();
		return adminPanel;
	}

	private TreePanel getTreeNav() {
		treePanel = new TreePanel();
		treePanel.setHeader(false);
		treePanel.setId("nav-tree");
		treePanel.setWidth(180);
		treePanel.setCollapsible(false);
		treePanel.setAnimate(true);
		treePanel.setAutoScroll(true);
		treePanel.setContainerScroll(true);
		treePanel.setBorder(false);

		treePanel.setTopToolbar(getTreeOptionsToolbar());
		TreeNode root = new TreeNode(ClientUtils.APP_NAME);
		root.setAllowDrag(false);
		root.setAllowDrop(false);

		treePanel.setRootNode(root);
		root.setIconCls("icon-butterfly");
		treePanel.setUseArrows(true);
		if (!idPerfil.equals("user") && !idPerfil.equals("advuser")) {
			treePanel.setEnableDD(true);
			treePanel.setDdAppendOnly("true");
			treePanel.addListener(new DragAndDropTreePanelListener(this));
		}
		map.setTreePanel(treePanel);

		return treePanel;
	}

	public TreePanel getTree() {
		return treePanel;
	}

	public ArrayList<String[]> getCheckedRecs() {
		ArrayList<String[]> recs = new ArrayList<String[]>();

		TreeNode[] nodes = getTree().getChecked();

		for (int i = 0; i < nodes.length; i++) {
			String[] v = new String[2];
			Recurso rec = (Recurso) nodes[i].getAttributeAsObject("entity");
			v[0] = rec.getId();
			v[1] = rec.getCont().getId();
			recs.add(v);
		}
		return recs;
	}

	private void loadProceso(TreePanel treePanel, String proceso, boolean doExtend, Function f) {
		storeRecWithoutVeh = new Store(ClientUtils.RECORD_DEF_RECS_NO_VEH);

		treePanel.getEl().mask("Cargando el \u00e1rbol del proceso...");

		getTimeoutTimer(true);
		if (!getDoReload()) {
			RPCTreeLoader cb = new RPCTreeLoader(this, true, true);
			cb.setFunction(f);
			getQueriesServiceProxy().getProcesoMaqTime(proceso, true, getDfDateProg().getEl().getValue(), cb);
		} else {
			getQueriesServiceProxy().getInfoRecursosMaqTime(getCheckedRecs(), getProceso().getId(),
					getProceso().isSoloCentral(), getProceso().isConTurnos(), getDfDateProg().getEl().getValue(),
					reloader);
		}

	}

	private void loadProceso(TreePanel treePanel, String proceso, boolean doExtend) {
		storeRecWithoutVeh = new Store(ClientUtils.RECORD_DEF_RECS_NO_VEH);

		treePanel.getEl().mask("Cargando el \u00e1rbol del proceso...");

		getTimeoutTimer(true);
		// if(getCheckedRecs().size() > 0)
		if (!getDoReload()) {
			/*
			 * getQueriesServiceProxy().getProceso(proceso, true, new
			 * RPCTreeLoader(this, true, true));
			 */
			getQueriesServiceProxy().getProcesoMaqTime(proceso, true, getDfDateProg().getEl().getValue(),
					new RPCTreeLoader(this, true, true));
		} /*
			 * else {
			 * getQueriesServiceProxy().getInfoRecursosMaqTime(getCheckedRecs(),
			 * getProceso().getId(), getProceso().isSoloCentral(),
			 * getProceso().isConTurnos(), getDfDateProg().getEl().getValue(),
			 * new ReloadButtonListener(this));
			 * 
			 * }
			 */

		/*
		 * getQueriesServiceProxy().getInfoRecursos(getCheckedRecs(),
		 * getProceso().getId(), getProceso().isSoloCentral(),
		 * getProceso().isConTurnos(), new ReloadButtonListener(this));
		 */

	}

	public Timer getTimeoutTimer(boolean doCreate) {
		if (timeOutTimer == null && doCreate) {
			timeOutTimer = new Timer() {
				@Override
				public void run() {
					timeOutTimer = null;
					getTree().getEl().unmask();
					ClientUtils.alert("Error", "Tiempo de espera agotado. Intentelo nuevamente.", ClientUtils.ERROR);
				}
			};
			timeOutTimer.schedule(ClientUtils.TREE_LOAD_TIMEOUT);
		}
		return timeOutTimer;
	}

	public void cancelTimeoutTimer() {
		if (timeOutTimer != null) {
			timeOutTimer.cancel();
			timeOutTimer = null;
		}
	};

	private Toolbar getTreeOptionsToolbar() {
		treeOptionsToolbar = new Toolbar();
		treeOptionsToolbar.setDisabled(true);
		ToolbarButton expandButton = new ToolbarButton();
		expandButton.setCls("x-btn-icon expand-all-btn");
		expandButton.setTooltip("Abrir Todas");
		expandButton.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				treePanel.expandAll();
			}
		});
		treeOptionsToolbar.addButton(expandButton);

		ToolbarButton collapseButton = new ToolbarButton();
		collapseButton.setCls("x-btn-icon collapse-all-btn");
		collapseButton.setTooltip("Cerrar Todas");
		collapseButton.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				treePanel.collapseAll();
			}
		});

		treeOptionsToolbar.addButton(collapseButton);

		ToolbarButton showAllButton = new ToolbarButton();
		showAllButton.setCls("x-btn-icon icon-map");
		showAllButton.setTooltip("Mostrar Todos");
		showAllButton.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				map.showAll();
			}
		});

		treeOptionsToolbar.addButton(showAllButton);

		/*
		 * ToolbarButton refreshButton = new ToolbarButton();
		 * refreshButton.setCls("x-btn-icon icon-refresh-toolbar");
		 * refreshButton.setTooltip("Actualizar Informaci\u00f3n");
		 * refreshButton.addListener(new ButtonListenerAdapter(){ public void
		 * onClick(Button button, EventObject e) {
		 * 
		 * } }); treeOptionsToolbar.addButton(refreshButton);
		 */

		ToolbarButton btnMenu = new ToolbarButton("Men\u00fa");
		mainMenu = new Menu();
		menuTrab = new Menu();
		menuRep = new Menu();
		menuProceso = new Menu();
		menuEstadisticas = new Menu();
		Menu menuEstadisticasTra = new Menu();
		Menu menuEstadisticasVeh = new Menu();
		MenuItem miTrabs = new MenuItem("Trabajos", menuTrab);
		MenuItem miRep = new MenuItem("Reportes", menuRep);
		MenuItem miProceso = new MenuItem("Proceso", menuProceso);
		MenuItem miEstadisticas = new MenuItem("Estad\u00edsticas", menuEstadisticas);
		MenuItem miEstadisticasTra = new MenuItem("Tabajos", menuEstadisticasTra);
		MenuItem miEstadisticasVeh = new MenuItem("Veh\u00edculos", menuEstadisticasVeh);
		menuEstadisticas.addItem(miEstadisticasTra);
		menuEstadisticas.addItem(miEstadisticasVeh);
		// miTrabs.setCtCls("icon-work");
		mainMenu.addItem(miTrabs);
		mainMenu.addItem(miRep);
		mainMenu.addItem(miProceso);
		mainMenu.addItem(miEstadisticas);
		btnMenu.setMenu(mainMenu);

		Item viewEventsItem = new Item("Ver Posiciones Actuales");
		menuProceso.addItem(viewEventsItem);
		viewEventsItem.addListener(new ViewEventsItemListener(this, getProceso(), "posactual"));

		Item viewClosedWorksItem = new Item("Reporte de Entregas");
		// menuProceso.addItem(viewClosedWorksItem);
		viewClosedWorksItem.setIcon("./images/silk/page_white_excel.gif");
		viewClosedWorksItem.addListener(new ViewClosedWorksItemListener(this));

		Item viewParkedVehsItem = new Item("Veh\u00edculos Estacionados...");
		menuProceso.addItem(viewParkedVehsItem);
		viewParkedVehsItem.addListener(new ViewParkedVehsItemListener(this));

		Item viewControldeFlotasItem = new Item("Control de Flotas..");
		menuProceso.addItem(viewControldeFlotasItem);
		viewControldeFlotasItem.addListener(new ControldeFlotasItemListener(this, centerPanel));

		Item viewSincronizarGeocItem = new Item("Sincronizar datos Geocercas..");
		menuProceso.addItem(viewSincronizarGeocItem);
		viewSincronizarGeocItem.addListener(new SincronizarGeocercasItemListener(this, centerPanel));

		Item viewRouteProgrammerItem = new Item("Programador de rutas");
		menuProceso.addItem(viewRouteProgrammerItem);
		viewRouteProgrammerItem.addListener(new ViewRouteProgrammerItemListener(this));

		Item estCompRec = new Item("Comparativo de Recursos");
		Item estCompCont = new Item("Comparativo de Contratistas");
		Item estMgr = new Item("Gerencial");

		estCompRec.addListener(new ComparativeReportItemListener(this, ComparativeReportItemListener.RECURSO));
		estCompCont.addListener(new ComparativeReportItemListener(this, ComparativeReportItemListener.CONTRATISTA));
		estMgr.addListener(new ManagerReportItemListener(this, "Reporte Gerencial"));

		Item estCompOdo = new Item("Comparativo de Od\u00f3metros");
		estCompOdo.addListener(new ComparativeReportOdoListener(this));

		Item estCompOdoDia = new Item("Comparativos Diarios");
		estCompOdoDia.addListener(new CentralComparativeReportItemListener(this));

		Item estManagerEvents = new Item("An\u00e1lisis gerencial de Eventos");
		estManagerEvents.addListener(new ComparativeManagerReportEventsListener(this));

		Item estManagerOdom = new Item("An\u00e1lisis gerencial de Od\u00f3metros");
		estManagerOdom.addListener(new ComparativeManagerReportOdomListener(this));

		Item estCompEvents = new Item("Comparativo de Eventos");
		estCompEvents.addListener(new CentralComparativeReportEventsListener(this));

		// menuEstadisticas.addItem(estCompRec);
		menuEstadisticasVeh.addItem(estCompOdo);
		menuEstadisticasVeh.addItem(estCompOdoDia);
		menuEstadisticasVeh.addItem(estCompEvents);
		menuEstadisticasVeh.addSeparator();
		menuEstadisticasVeh.addItem(estManagerEvents);
		menuEstadisticasVeh.addItem(estManagerOdom);

		// menuEstadisticas.addSeparator();
		menuEstadisticasTra.addItem(estCompRec);
		menuEstadisticasTra.addItem(estCompCont);
		menuEstadisticasTra.addSeparator();
		menuEstadisticasTra.addItem(estMgr);

		if (!idPerfil.equals("user") && !idPerfil.equals("advuser")) {
			CheckItem ubicButton = new CheckItem();
			ubicButton.setCls("icon-no-ubic-toolbar");
			ubicButton.setText("Ubicar Trabajo");
			ubicButton.setChecked(false);
			// ubicButton.setToggleGroup("toolbar");

			ubicButton.addListener(new CheckItemListenerAdapter() {
				@Override
				public void onCheckChange(CheckItem item, boolean checked) {
					if (checked) {
						map.setCrosshairCursor();
					} else {
						map.setDefaultCursor();
					}
				}
			});

			map.getMap().addMapClickListener(new MapClickEventListener(ubicButton, this));

			// treeOptionsToolbar.addButton(ubicButton);
			menuTrab.addItem(ubicButton);

			Item pathStartButton = new Item();
			pathStartButton.setCls("icon-pathstart-toolbar");
			pathStartButton.setText("Fijar Inicio de Ruta");
			pathStartButton.addListener(new PathStartButtonListener(this));

			// treeOptionsToolbar.addButton(pathStartButton);
			menuTrab.addItem(pathStartButton);

			Item priorButton = new Item();
			priorButton.setCls("icon-prioridad-toolbar");
			priorButton.setText("Cambiar Prioridad");
			priorButton.addListener(new ChangePriorityButtonListener(this));

			// treeOptionsToolbar.addButton(priorButton);
			menuTrab.addItem(priorButton);

			Item resetButton = new Item();
			resetButton.setCls("icon-reset-toolbar");
			resetButton.setText("Reiniciar Trabajo");
			resetButton.addListener(new ResetButtonListener(this));

			// treeOptionsToolbar.addButton(resetButton);
			menuTrab.addItem(resetButton);

			Item interfazButton = new Item();
			interfazButton.setText("Administrar Interfaz...");
			interfazButton.addListener(new AdminInterfaceListener(this));
			// SOLO PARA SISTEMA UNO
			mainMenu.addItem(interfazButton);

			Item reporteTraficoButton = new Item();
			reporteTraficoButton.setText("Reporte de Trafico");
			reporteTraficoButton.addListener(new RepsButtonListener(this, RepsButtonListener.REP_TRAFICO));

			reporteTraficoButton.setIcon("./images/silk/page_white_excel.gif");
			// mainMenu.addItem(reporteTraficoButton);
			menuRep.addItem(reporteTraficoButton);
			menuRep.addItem(viewClosedWorksItem);
		}

		treeOptionsToolbar.addButton(btnMenu);

		treeOptionsToolbar.addSeparator();

		Item searchDirButton = new Item();
		searchDirButton.setCls("icon-search-toolbar");
		searchDirButton.setText("Buscar Direcci\u00f3n");
		searchDirButton.addListener(new SearchDirButtonListener(this));
		// treeOptionsToolbar.addButton(searchDirButton);
		mainMenu.addItem(searchDirButton);

		return treeOptionsToolbar;

	}

	public void addSearchWorkToolbarButton() {
		if (!getProceso().isSoloCentral() && searchTrabButton == null) {
			menuTrab.addSeparator();
			searchTrabButton = new Item();
			searchTrabButton.setCls("icon-searchwork-toolbar");
			searchTrabButton.setText("Buscar Trabajo");
			searchTrabButton.addListener(new SearchWorkButtonListener(this));
			menuTrab.addItem(searchTrabButton);
		}
		if (enableRefreshButton == null) {
			favProcToggleButton.setPressed(true);
			favUserToggleButton.setPressed(true);

			treeOptionsToolbar.addFill();
			enableRefreshButton = new ToolbarButton();
			enableRefreshButton.setCls("x-btn-icon icon-refresh-toolbar");

			enableRefreshButton.setEnableToggle(true);
			enableRefreshButton.setToggleGroup("refresh");
			// enableRefreshButton.setPressed(true);
			enableRefreshButton.setPressed(false);
			enableRefreshButton.setTooltip("Activar Actualizaci\u00f3n Autom\u00e1tica");
			enableRefreshButton.addListener(new ButtonListenerAdapter() {
				@Override
				public void onToggle(Button button, boolean pressed) {
					if (!pressed) {
						button.setTooltip("Activar Actualizaci\u00f3n Autom\u00e1tica");
						stopTimer();
					} else {
						button.setTooltip("Desactivar Actualizaci\u00f3n Autom\u00e1tica");
						restartTimer();
					}
				}
			});
			treeOptionsToolbar.addButton(enableRefreshButton);
		}
	}

	public void removeAllChilds(TreeNode node) {
		while (node.getChildNodes().length != 0) {
			if (node.item(0).getAttributeAsObject("marker") != null) {
				map.getMkTrab().removeMarker((Marker) node.item(0).getAttributeAsObject("marker"));
			}
			node.removeChild(node.item(0));
		}
	}

	private void showMenu(final Panel tab, EventObject e) {
		if (menu == null) {
			menu = new Menu();
			Item close = new Item("Cerrar pesta\u00f1a");
			close.setId("close-tab-item");
			close.addListener(new BaseItemListenerAdapter() {
				@Override
				public void onClick(BaseItem item, EventObject e) {
					centerPanel.remove(centerPanel.getActiveTab());
				}
			});
			menu.addItem(close);

			Item closeOthers = new Item("Cerrar otras pesta\u00f1as");
			closeOthers.setId("close-others-item");
			closeOthers.addListener(new BaseItemListenerAdapter() {
				@Override
				public void onClick(BaseItem item, EventObject e) {
					Component[] items = centerPanel.getItems();
					for (int i = 0; i < items.length; i++) {
						Panel panel = (Panel) items[i];
						if (panel.isClosable() && !panel.getId().equals(centerPanel.getActiveTab().getId())) {
							centerPanel.remove(panel);
						}
					}
				}
			});
			menu.addItem(closeOthers);
		}

		BaseItem close = menu.getItem("close-tab-item");
		if (!centerPanel.getActiveTab().isClosable()) {
			close.disable();
		} else {
			close.enable();
		}

		BaseItem closeOthers = menu.getItem("close-others-item");
		if (centerPanel.getItems().length == 1) {
			closeOthers.disable();
		} else {
			closeOthers.enable();
		}
		menu.showAt(e.getXY());
	}

	public void showMessage(String title, String message, Position position) {
		messagePanel = new PopupPanel(true);
		messagePanel.setWidget(new HTML(getMessageHtml(title, message)));
		messagePanel.setWidth("300px");
		if (position.equals(Position.CENTER)) {
			messagePanel.center();
		} else if (position.equals(Position.LEFT)) {
			messagePanel.setPopupPosition(0, 0);
			messagePanel.show();
		}

	}

	public void clearNoUbicNoAsigNodes() {
		removeAllChilds(noUbicTreeNode);
		removeAllChilds(noAsigTreeNode);
	}

	public TreeNode getNoAsigTreeNode() {
		return noAsigTreeNode;
	}

	public TreeNode getNoUbicTreeNode() {
		return noUbicTreeNode;
	}

	private ArrayList<Object> openedNodes;

	public ArrayList<Object> getOpenedNodes() {
		if (openedNodes == null) {
			openedNodes = new ArrayList<Object>();
		}
		return openedNodes;
	}

	private ArrayList<String> openedLabels;

	public ArrayList<String> getOpenedLabels() {
		if (openedLabels == null) {
			openedLabels = new ArrayList<String>();
		}
		return openedLabels;
	}

	private void expandChilds(TreeNode t) {
		if (t.getChildNodes().length != 0) {
			for (Node node : t.getChildNodes()) {
				if (getOpenedNodes().contains(node.getAttributeAsObject("entity"))
						|| getOpenedLabels().contains(((TreeNode) node).getText())) {
					((TreeNode) node).expand();
					GWT.log("Expandiendo:" + ((TreeNode) node).getText(), null);
				}
				expandChilds((TreeNode) node);
			}
		}
	}

	private ArrayList<Object> getExpandedNodes(TreeNode root) {
		ArrayList<Object> v = new ArrayList<Object>();
		if (root.getChildNodes().length != 0) {
			for (Node child : root.getChildNodes()) {
				TreeNode tn = (TreeNode) child;
				if (tn.isExpanded()) {
					if (tn.getAttributeAsObject("entity") != null) {
						v.add(tn.getAttributeAsObject("entity"));
					} else {
						v.add(tn.getText());
					}
					v.addAll(getExpandedNodes(tn));
				}
			}
		}
		return v;
	}

	public void reloadTree(boolean doExtend, boolean minInfo) {
		if (getTimeoutTimer(false) != null) {
			String procid = proc.getId();
			cancelTimeoutTimer();
			TreePanel tp = getTree();
			tp.getRootNode().setText(proc.getNombre());
			getOpenedNodes().removeAll(getOpenedNodes());
			getOpenedNodes().addAll(getExpandedNodes(tp.getRootNode()));
			removeAllChilds(tp.getRootNode());
			getMap().removeClearHandler();
			getMap().clearOverlays();
			// tp.getRootNode().setIconCls(proc.getIconTree());
			tp.getRootNode().setAttribute("entity", proc);
			getDfDateProg().setDisabled(false);
			if (!proc.isSoloCentral()) {
				ArrayList<Trabajo> noUbicTrabs = proc.getTrabsNoUbic();
				ArrayList<Trabajo> noAsigTrabs = proc.getTrabsNoAsig();

				if (procid.equals("010") || procid.equals("020") || procid.equals("030") || procid.equals("040")) {
					noUbicTreeNode = new TreeNode("Avisos sin alimentaci\u00f3n -- " + noUbicTrabs.size(),
							"icon-no-ubic");
					noAsigTreeNode = new TreeNode("Avisos No Asignados -- " + noAsigTrabs.size(), "icon-no-asig");

				} else {
					noUbicTreeNode = new TreeNode("Trabajos No Ubicados -- " + noUbicTrabs.size(), "icon-no-ubic");
					noAsigTreeNode = new TreeNode("Trabajos No Asignados -- " + noAsigTrabs.size(), "icon-no-asig");
				}

				noUbicTreeNode.setAllowDrag(false);
				noUbicTreeNode.setAllowDrop(false);

				noAsigTreeNode.setAllowDrag(true);
				noAsigTreeNode.setAllowDrop(false);

				noUbicTreeNode.setAttribute("id", "noUbic");
				noUbicTreeNode.setAttribute("nomrec", "noUbic");
				noUbicTreeNode.setAttribute("tiponodo", "noUbic");
				noUbicTreeNode.setAttribute("entity", "noUbic");
				noAsigTreeNode.addListener(new RightClickTreeNodeListener(ControlWebGUI.this));
				noAsigTreeNode.setAttribute("id", "noAsig");
				noAsigTreeNode.setAttribute("nomrec", "noAsig");
				noAsigTreeNode.setAttribute("tiponodo", "noAsig");
				noAsigTreeNode.setAttribute("entity", "noAsig");

				addTrabTreeNodes(noUbicTrabs, noUbicTreeNode, false, false, this);
				tp.getRootNode().appendChild(noUbicTreeNode);

				/*
				 * noUbicTreeNode.addListener(new TreeNodeListenerAdapter() {
				 * 
				 * @Override public void onClick(Node node, EventObject e) {
				 * selectedTreeNode = null; } });
				 */
				addTrabTreeNodes(noAsigTrabs, noAsigTreeNode, true, false, this);

				tp.getRootNode().appendChild(noAsigTreeNode);

				/*
				 * noAsigTreeNode.addListener(new TreeNodeListenerAdapter() {
				 * 
				 * @Override public void onClick(Node node, EventObject e) { try
				 * { selectedTreeNode = node; new
				 * NodeTreeClickListener(ControlWebGUI.this, false)
				 * .showMarkers(node, false, false); } catch (Exception e1) { }
				 * } });
				 */

				/*
				 * noUbicTreeNode .addListener(new
				 * RightClickTreeNodeListener(this)); noAsigTreeNode
				 * .addListener(new RightClickTreeNodeListener(this));
				 */

			}
			tp.getRootNode().addListener(new RightClickTreeNodeListener(this));
			addContratistas(proc.getContratistas(), proc, tp.getRootNode(), minInfo);

			setProceso(proc);
			// addButtonTrabajo();

			getTree().getEl().unmask();
			getTree().getRootNode().expand();

			if (!minInfo) {
				getMap().extend();
			}

			if (doExtend) {
				// getMap().extend();
				addSearchWorkToolbarButton();

				if (enableRefreshButton.isPressed()) {
					startTimer();
				}

				setTreeToolbarDisabled(false);

				if (proc.isSoloCentral()) {
					((TreeNode) tp.getRootNode().getFirstChild()).expand();
				}
			}
			refreshPtoInts();
			expandChilds(tp.getRootNode());
		}
	}

	public void refreshPtoInts() {
		if (favProcToggleButton.isPressed()) {
			favProcToggleButton.setPressed(false);
			favProcToggleButton.setPressed(true);
		}
		if (favUserToggleButton.isPressed()) {
			favUserToggleButton.setPressed(false);
			favUserToggleButton.setPressed(true);
		}
	}

	public void addContratistas(ArrayList<Contratista> conts, Proceso proc, TreeNode node, boolean minInfo) {
		for (Contratista cont : conts) {
			if (proc.isConContratistas()) {
				TreeNode contTreeNode = new TreeNode(cont.getNombre(), cont.getIcon());
				contTreeNode.setAllowDrag(false);
				contTreeNode.setAllowDrop(false);
				// contTreeNode.setAttribute("id", cont.getId());
				// contTreeNode.setAttribute("tiponodo", "contratista");
				contTreeNode.setAttribute("entity", cont);
				addTurnos(cont.getTurnos(), contTreeNode, minInfo);
				contTreeNode.addListener(new RightClickTreeNodeListener(this)); // listeners
				// !
				// !
				// !
				// !
				// !
				node.appendChild(contTreeNode);
			} else {
				addTurnos(conts.get(0).getTurnos(), node, minInfo);
			}

		}
	}

	public void addTurnos(ArrayList<Turno> turnos, TreeNode node, boolean minInfo) {
		for (Turno turno : turnos) {
			TreeNode turnoTreeNode = new TreeNode(turno.getTitle(), turno.getIconTree());
			turnoTreeNode.setAttribute("entity", turno.getNombre() + node.getText());
			turnoTreeNode.setAllowDrag(false);
			turnoTreeNode.setAllowDrop(false);
			ArrayList<Recurso> recs = turno.getRecs();
			for (Recurso recurso : recs) {
				addRecurso(recurso, turnoTreeNode, minInfo);
			}

			node.appendChild(turnoTreeNode);
		}
	}

	private void addRecurso(Recurso rec, TreeNode node, boolean minInfo) {
		TreeNode recTreeNode;

		if (!minInfo && !rec.isChecked()) {
			rec.clearTrabajos();
			rec.setVeh(null);
		}

		if (rec.getIconTree() != null) {
			recTreeNode = new TreeNode(rec.getTitle(), rec.getIconTree());
		} else {
			recTreeNode = new TreeNode(rec.getTitle());
		}

		recTreeNode.setAllowDrag(false);
		recTreeNode.setAllowDrop(true);
		recTreeNode.setChecked(false);
		if (!minInfo && rec.isChecked()) {
			if (rec.getVeh() != null && rec.getVeh().getLatitudAsDouble() != 0.0d
					&& rec.getVeh().getLongitudAsDouble() != 0.0d) {
				recTreeNode.setAttribute("marker", getMap().createVehicleMarker(rec.getVeh().getLatitudAsDouble(),
						rec.getVeh().getLongitudAsDouble(), rec.getMapIcon(), rec.getHTML()));
			}

			if (rec.getHTML() != null) {
				recTreeNode.setTooltip(rec.getHTML());
			}
			if (rec.getTrabajos() != null) {
				addTrabTreeNodes(rec.getTrabajos(), recTreeNode, true, false, this);
			}

			recTreeNode.setChecked(rec.isChecked());
		}
		recTreeNode.setAttribute("entity", rec);
		recTreeNode.addListener(new NodeTreeClickListener(this, true));
		recTreeNode.addListener(new RightClickTreeNodeListener(this));
		node.appendChild(recTreeNode);
	}

	public boolean verificarRangoFecha(DateField fromDate, DateField toDate) {
		if (!fromDate.getEl().getValue().equals("") && !toDate.getEl().getValue().equals("")) {
			if (fromDate.getValue().before(toDate.getValue()) || fromDate.getValue().equals(toDate.getValue())
					&& fromDate.getValue().compareTo(toDate.getValue()) <= 0) {

				return true;

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void addTrabTreeNodes(ArrayList<Trabajo> trabs, TreeNode parent, boolean allowDrag, boolean allowDrop,
			ControlWebGUI gui) {

		for (Trabajo trab : trabs) {
			// String title = trab.getMunicipio().getId() + trab.getId() +
			// " - "+ trab.getDireccion() + " [" + trab.getAtraso() + "]";
			String title = (trab.getPrioridad() == null ? "N" : trab.getPrioridad()) + "-" + trab.getId() + " - "
					+ trab.getTpoTrab().getId() + " - " + trab.getDireccion() + " [" + trab.getAtraso() + "]";

			TreeNode trabTreeNode = new TreeNode(title, trab.getIcono());
			trabTreeNode.setAllowDrag(allowDrag);
			trabTreeNode.setAllowDrop(allowDrop);
			trabTreeNode.addListener(new RightClickTreeNodeListener(gui));
			trabTreeNode.addListener(new LeafTreeClickListener(gui));
			parent.appendChild(trabTreeNode);

			if (trab.getLat() != 0.0d) {

				/*
				 * trabTreeNode.setAttribute("marker",
				 * gui.getMap().createTrabMarker(trab.getLat(), trab.getLon(),
				 * trab.getMapIcon(getProceso()), trab.getHTML()));
				 * 
				 * trabTreeNode.setAttribute("marker2",
				 * gui.getMap().createJobsMarker(trab.getLat(), trab.getLon(),
				 * trab.getMapIcon(getProceso())));
				 */
				JSObjectWrapper[] m = gui.getMap().createTrabMarker(trab.getLat(), trab.getLon(), trab.getMapIcon()

						, "" + trab.getHTML(), trab.getId().toString(), trab.getTpoTrab().getId().toString());
				trabTreeNode.setAttribute("marker", m[0]);
				trabTreeNode.setAttribute("marker2", m[1]);
			}

			trabTreeNode.setAttribute("tiponodo", "trabajo");
			trabTreeNode.setAttribute("entity", trab);
		}
		// String t = parent.getText();
		// parent.setText(t.split(" - ")[0] + (trabs.size() > 0 ? " - " +
		// trabs.size() : ""));
	}

	public void addTrabTreeNodesOld(ArrayList<Trabajo> trabs, TreeNode parent, boolean allowDrag, boolean allowDrop,
			ControlWebGUI gui) {

		for (Trabajo trab : trabs) {
			// String title = trab.getMunicipio().getId() + trab.getId() +
			// " - "+ trab.getDireccion() + " [" + trab.getAtraso() + "]";
			String title = (trab.getPrioridad() == null ? "N" : trab.getPrioridad()) + "-" + trab.getId() + " - "
					+ trab.getTpoTrab().getId() + " - " + trab.getDireccion() + " [" + trab.getAtraso() + "]";

			TreeNode trabTreeNode = new TreeNode(title, trab.getIcono());
			trabTreeNode.setAllowDrag(allowDrag);
			trabTreeNode.setAllowDrop(allowDrop);
			trabTreeNode.addListener(new RightClickTreeNodeListener(gui));
			trabTreeNode.addListener(new LeafTreeClickListener(gui));
			parent.appendChild(trabTreeNode);

			if (trab.getLat() != 0.0d) {
				trabTreeNode.setAttribute("marker",
						gui.getMap().createTrabMarker(trab.getLat(), trab.getLon(), trab.getMapIcon(), trab.getHTML(),trab.getId().toString(),trab.getTpoTrab().getId().toString()));
			}

			trabTreeNode.setAttribute("tiponodo", "trabajo");
			trabTreeNode.setAttribute("entity", trab);
			trabTreeNode.setTooltip("Estado:" + trab.getNombreEstado());
		}
		// String t = parent.getText();
		// parent.setText(t.split(" - ")[0] + (trabs.size() > 0 ? " - " +
		// trabs.size() : ""));
	}

	public void hideMessage() {
		messagePanel.hide();
	}

	public OpenLayersMap getMap() {
		return map;
	}

	public Window getTrabajoInfoPanel(final String trabajo, final String tipotrabajo, final Function afterLoadFunction,
			final String estado) {
		final Window wnd = new Window("M\u00e1s Informaci\u00f3n", true, true);
		wnd.setIconCls("icon-work");
		wnd.setLayout(new FitLayout());
		wnd.setHeight(385);
		wnd.setWidth(550);

		wnd.setButtonAlign(Position.CENTER);

		final Button btAssign = new Button("Asignar Prog.", new ButtonListenerAdapter() {
			@Override
			public void onClick(final Button button, EventObject e) {
				if (!isPrecerrado) {
					new AssignProgWorkItemListener(ControlWebGUI.this, trabajo, tipotrabajo, fechaentrega, horaentrega,
							idjornada, new Function() {
								@Override
								public void execute() {
									button.setDisabled(true);
								}
							}).onClick(null, null);
				} else {
					ClientUtils.alert("Error", "Este trabajo ya fue finalizado.", ClientUtils.ERROR);
				}
			}
		});

		if (estado.equals("30") || estado.equals("40")) {
			btAssign.setDisabled(true);
		}

		wnd.addButton(btAssign);

		wnd.addButton(new Button("Aceptar", new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				wnd.close();
			}
		}));

		showMessage("Por favor espere...", "Consultando informaci\u00f3n...", Position.CENTER);

		final TabPanel tp = new TabPanel();

		final Panel panel = new Panel("Informaci\u00f3n de OT");
		panel.setLayout(new BorderLayout());
		panel.setPaddings(5);

		final GroupedGridPanel grid = new GroupedGridPanel();

		// Trabajo trab = (Trabajo) node.getAttributeAsObject("entity");

		serviceProxy.getInfoBitacora(trabajo, tipotrabajo, new AsyncCallback<ArrayList<GroupedRowModelAdapter>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideMessage();
				ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
			};

			@Override
			public void onSuccess(ArrayList<GroupedRowModelAdapter> result) {
				// gui.hideMessage();
				grid.addProperties(result);
				if (afterLoadFunction != null) {
					afterLoadFunction.execute();
				}
			};
		});

		serviceProxy.getInfoTrabajo(trabajo, tipotrabajo, new AsyncCallback<ArrayList<GroupedRowModelAdapter>>() {
			@Override
			public void onFailure(Throwable caught) {
				hideMessage();
				ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
			}

			@Override
			public void onSuccess(ArrayList<GroupedRowModelAdapter> result) {
				hideMessage();
				grid.addProperties(result);

				fechaentrega = result.get(result.size() - 3).getRowValues()[0];
				idjornada = result.get(result.size() - 2).getRowValues()[0];
				horaentrega = result.get(result.size() - 1).getRowValues()[0];
				panel.add(grid, ClientUtils.CENTER_LAYOUT_DATA);
				tp.add(panel);

				wnd.show();
				wnd.doLayout();

				isPrecerrado = result.get(5).getRowValues()[0].contains("30");

				if (isPrecerrado) {
					tp.add(getPrecierreTrabajoInfoPanel(trabajo, tipotrabajo));
					btAssign.setDisabled(true);
				}
			}
		});

		wnd.add(tp);
		return wnd;
	}

	public Panel getPrecierreTrabajoInfoPanel(String trabajo, String tipotrabajo) {

		final Panel panelPrecierres = new Panel("Info. de Precierre");
		panelPrecierres.setLayout(new RowLayout());

		final GroupedGridPanel gridPrecierres = new GroupedGridPanel();

		getQueriesServiceProxy().getInfoTrabajoPrecierre(trabajo, tipotrabajo,
				new RPCFillGroupedGridPanel(gridPrecierres));

		panelPrecierres.add(gridPrecierres);
		return panelPrecierres;
	}

	private static native String getMessageHtml(String title, String message) /*-{
		return [
				'<div class="msg">',
				'<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
				'<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>',
				title,
				'</h3>',
				message,
				'</div></div></div>',
				'<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
				'</div>' ].join('');
	}-*/;

	public static native void logout() /*-{
		//$wnd.location = 'Main.html';
		$wnd.history.go();
	}-*/;

	public void setDoReload(boolean b) {
		doreload = b;
	}

	public boolean getDoReload() {
		return doreload;
	}

	public TabPanel getWestTabPanel() {
		return westTabPanel;
	}

	public void showJobPath(TreeNode node) {
		ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
		ArrayList<TreeNode> filteredNodes = new ArrayList<TreeNode>();
		ArrayList<Marker> mks = new ArrayList<Marker>();
		getTreeNodes(node, Trabajo.class.getName(), nodes);

		for (TreeNode treeNode : nodes) {
			if (((Trabajo) treeNode.getAttributeAsObject("entity")).getEstado().equals("30")
					|| ((Trabajo) treeNode.getAttributeAsObject("entity")).getEstado().equals("40")
					|| ((Trabajo) treeNode.getAttributeAsObject("entity")).getEstado().equals("50")) {
				filteredNodes.add(treeNode);
			}
		}

		if (filteredNodes.size() > 1) {
			Collections.sort(filteredNodes, new Comparator<TreeNode>() {
				@Override
				public int compare(TreeNode o1, TreeNode o2) {
					Trabajo t1 = (Trabajo) o1.getAttributeAsObject("entity");
					Trabajo t2 = (Trabajo) o2.getAttributeAsObject("entity");
					return t1.compareTo(t2);
				}
			});

			for (TreeNode treeNode : filteredNodes) {
				mks.add((Marker) treeNode.getAttributeAsObject("marker"));
			}
		}
	}

	public void refreshNoUbicNoAsigTrabs(TreeNode selectedNode) {
		final TreeNode parent = (TreeNode) selectedNode.getParentNode();
		treePanel.getEl().mask("Actualizando Registros", "x-mask-loading");
		serviceProxy.getNoUbicNoAsigTrabs(getProceso().getId(), getDfDateProg().getEl().getValue(),
				new AsyncCallback<ProcesoLight>() {

					@Override
					public void onFailure(Throwable caught) {
						ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
						getTree().getEl().unmask();
						GWT.log("Error RPC", caught);
					}

					@Override
					public void onSuccess(ProcesoLight result) {
						clearNoUbicNoAsigNodes();
						addTrabTreeNodes(result.getTrabsNoUbic(), getNoUbicTreeNode(), false, false,
								ControlWebGUI.this);
						addTrabTreeNodes(result.getTrabsNoAsig(), getNoAsigTreeNode(), true, false, ControlWebGUI.this);
						getTree().getEl().unmask();
						parent.expand();
					}
				});
	}

	public void executeReload(String proceso, Function f) {
		timerStarted = false;
		doreload = false;
		currentProcess = proceso;
		loadProceso(treePanel, currentProcess, true, f);
		if (timer != null) {
			timer.cancel();
		}

		timer = new Timer() {
			@Override
			public void run() {
				loadProceso(treePanel, currentProcess, false, null);
			}
		};
		// startTimer();
	}

	public Button getMultiSelectToggleButton() {
		return multiSelectToggleButton;
	}

	public Button getAcceptSelectionButton() {
		return acceptSelectionButton;
	}

}
