package org.extreme.controlweb.client.gui;

import java.util.Date;

import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.TimeField;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;

public class DateWindow extends Window {
	
	private DateField fromDate;
	private TimeField fromTime;
	private DateField toDate;
	private TimeField toTime;
	private boolean timed;
	private Button btCancel;
	private CheckBoxGridPanel checkGrid;
	private Panel gridPanel;
	private Panel northPanel;
	private Panel additionalPanel;

	public DateWindow(String title, boolean timed, boolean withGrid) {
		super(title, true, true);
		this.timed = timed;
		
		if(!withGrid) {
			this.setHeight(130);
			this.setWidth(300);
			this.setLayout(new FormLayout());
		} else {
			this.setHeight(400);
			this.setWidth(370);
			this.setLayout(new BorderLayout());
		}
			
		this.setButtonAlign(Position.CENTER);
		this.setPaddings(0, 5, 0, 0);
		MultiFieldPanel fromPanel = new MultiFieldPanel();
		fromPanel.setBorder(false);	
		
		MultiFieldPanel toPanel = new MultiFieldPanel();
		toPanel.setBorder(false);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		fromDate.setReadOnly(true);
		fromDate.setWidth(95);
		
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		toDate.setReadOnly(true);
		toDate.setWidth(95);
		
		fromPanel.addToRow(fromDate, 205);
		if(timed){
			fromTime = new TimeField();
			fromTime.setHideLabel(true);
			fromTime.setReadOnly(true);
			fromTime.setWidth(65);
			fromTime.setFormat("H:i");
			fromTime.setIncrement(5);
			fromTime.setValue("00:00");
			fromPanel.addToRow(fromTime, new ColumnLayoutData(1));
		}
		
		toPanel.addToRow(toDate, 205);
		
		if(timed){
			toTime = new TimeField();
			toTime.setHideLabel(true);
			toTime.setReadOnly(true);
			toTime.setWidth(65);
			toTime.setFormat("H:i");
			toTime.setIncrement(5);
			toTime.setValue("23:55");
			toPanel.addToRow(toTime, new ColumnLayoutData(1));
		}
		
		additionalPanel = new Panel();
		additionalPanel.setBorder(false);
		additionalPanel.setLayout(new FormLayout());
		
		northPanel = new Panel();
		northPanel.setBorder(false);
		northPanel.setPaddings(10, 0, 10, 25);
		//northPanel.setHeight(75);
		northPanel.add(fromPanel);
		northPanel.add(toPanel);
		northPanel.add(additionalPanel);
			
		if(withGrid) {			
			checkGrid = new CheckBoxGridPanel();
			gridPanel = new Panel("Geocercas");
			gridPanel.setLayout(new RowLayout());
			Panel datesPanel = new Panel();
			datesPanel.setBorder(false);
			datesPanel.setPaddings(10, 0, 10, 10);
			datesPanel.setHeight(75);
			datesPanel.add(fromPanel);
			datesPanel.add(ClientUtils.getSeparator());
			datesPanel.add(toPanel);
			
			this.add(gridPanel, new BorderLayoutData(RegionPosition.CENTER));
			this.add(northPanel, new BorderLayoutData(RegionPosition.NORTH));
		} else
			this.add(northPanel);
		
		
		btCancel = new Button("Cancelar", new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				close();
			}
		});
	}
	
	public DateWindow(String title){
		super(title, true, true);
	}
	
	public void createDateWindow(boolean timed, boolean withGrid)
	{
		this.timed = timed;
		
		if(!withGrid) {
			this.setHeight(130);
			this.setWidth(300);
			this.setLayout(new FormLayout());
		} else {
			this.setHeight(400);
			this.setWidth(370);
			this.setLayout(new BorderLayout());
		}
			
		this.setButtonAlign(Position.CENTER);
		this.setPaddings(0, 5, 0, 0);
		MultiFieldPanel fromPanel = new MultiFieldPanel();
		fromPanel.setBorder(false);	
		
		MultiFieldPanel toPanel = new MultiFieldPanel();
		toPanel.setBorder(false);
		
		fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		fromDate.setReadOnly(true);
		fromDate.setWidth(95);
		
		toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		toDate.setReadOnly(true);
		toDate.setWidth(95);
		
		fromPanel.addToRow(fromDate, 205);
		if(timed){
			fromTime = new TimeField();
			fromTime.setHideLabel(true);
			fromTime.setReadOnly(true);
			fromTime.setWidth(65);
			fromTime.setFormat("H:i");
			fromTime.setIncrement(5);
			fromTime.setValue("00:00");
			fromPanel.addToRow(fromTime, new ColumnLayoutData(1));
		}
		
		toPanel.addToRow(toDate, 205);
		
		if(timed){
			toTime = new TimeField();
			toTime.setHideLabel(true);
			toTime.setReadOnly(true);
			toTime.setWidth(65);
			toTime.setFormat("H:i");
			toTime.setIncrement(5);
			toTime.setValue("23:55");
			toPanel.addToRow(toTime, new ColumnLayoutData(1));
		}
		
		additionalPanel = new Panel();
		additionalPanel.setBorder(false);
		additionalPanel.setLayout(new FormLayout());
		
		northPanel = new Panel();
		northPanel.setBorder(false);
		northPanel.setPaddings(10, 0, 10, 10);
		//northPanel.setHeight(75);
		northPanel.add(fromPanel);
		northPanel.add(toPanel);
		northPanel.add(additionalPanel);
			
		if(withGrid) {			
			checkGrid = new CheckBoxGridPanel();
			gridPanel = new Panel("Geocercas");
			gridPanel.setLayout(new RowLayout());
			Panel datesPanel = new Panel();
			datesPanel.setBorder(false);
			datesPanel.setPaddings(10, 0, 10, 10);
			datesPanel.setHeight(75);
			datesPanel.add(fromPanel);
			datesPanel.add(ClientUtils.getSeparator());
			datesPanel.add(toPanel);
			
			this.add(gridPanel, new BorderLayoutData(RegionPosition.CENTER));
			this.add(northPanel, new BorderLayoutData(RegionPosition.NORTH));
		} else
			this.add(northPanel);
		
		
		btCancel = new Button("Cancelar", new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				close();
			}
		});
	}
	
	public void addOKListener(ButtonListenerAdapter onOK) {
		Button btOK = new Button("Aceptar", onOK);
		this.addButton(btOK);
		this.addButton(btCancel);
	}
	
	public void renderGrid(){
		gridPanel.add(checkGrid, new RowLayoutData());
	}
	
	public String getFromDate(){
		String res = fromDate.getEl().getValue();
		if(timed)
			 res += " " + fromTime.getText();
		return res;
	}
	
	public String getToDate(){
		String res = toDate.getEl().getValue();
		if(timed)
			 res += " " + toTime.getText();
		return res;
	}
	
	public Date getFromDateAsDate(){
		return fromDate.getValue();
	}
	
	public Date getToDateAsDate(){
		return toDate.getValue();
	}
	
	public String getFromTime(){
		return fromTime.getValue();
	}
	
	public String getToTime(){
		return toTime.getValue();
	}
	
	public CheckBoxGridPanel getCheckGrid(){
		return checkGrid;
	}
	
	public void setVisibleFromDate(boolean visible) {
		fromDate.setVisible(visible);
		if (timed)
			fromTime.setVisible(visible);
	}

	public void setVisibleToDate(boolean visible) {
		toDate.setVisible(visible);
		if (timed)
			toTime.setVisible(visible);
	}
	
	public Panel getAdditionalPanel() {
		return additionalPanel;
	}
	
	public Panel getPanelDate() {
		return northPanel;
	}
}
