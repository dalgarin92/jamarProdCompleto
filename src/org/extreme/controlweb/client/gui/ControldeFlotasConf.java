package org.extreme.controlweb.client.gui;




import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.listeners.ControldeFlotasItemListener;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.HorizontalLayout;




public class ControldeFlotasConf {
	
	public boolean isVisible;
	public ControlWebGUI gui;
	public String fechatot;
    public String tiempo;
	public String valorset="";
    private static AdminQueriesServiceAsync adminServiceProxy = null;
	public ComboBox cmbTipoLetra;
	public ComboBox cmbTamLetra;
	public Window wnd ;
	public Function CerrarVentana;
    public ControldeFlotasItemListener control;
	public ControldeFlotasConf conf;
	public Function obtenerValores;
	
	
	

	
	public ControldeFlotasConf(final ControlWebGUI gui,  String fechatot,  String tiempo, boolean isVisible,  ControldeFlotasItemListener control2) {
		this.gui = gui;
		this.fechatot=fechatot;
		this.tiempo=tiempo;
		this.isVisible=isVisible;
		this.control=control2;
		conf=this;

		wnd = new Window("Configuraci\u00f3n Control de Flotas", false, true);
		wnd.setLayout(new FitLayout());
		wnd.setSize(350,630);
		wnd.setTitle("Configuraci\u00f3n Control de Flotas");
		wnd.setButtonAlign(Position.CENTER);
		TabPanel config=new TabPanel();	
		Panel pGeneral=new Panel("Herramientas de Texto");
		pGeneral.setPaddings(15);
		pGeneral.setLayout(new HorizontalLayout(15));	
		FieldSet fsGeneralIzquieda=new FieldSet();
		fsGeneralIzquieda.setPaddings(5);
		fsGeneralIzquieda.setLabelWidth(50);
		fsGeneralIzquieda.setBorder(false);		
		fsGeneralIzquieda.setLayout(new FormLayout());
		
	    final String [] campos={"muelle","placa","destino","entrada","salida","retorno","estado","ubicacion","conductor","celular","trabajo","ultent","proxent","observ"};

		final CheckBox negrita=new CheckBox();
		negrita.setText(" Negrita ");
		
		final CheckBox cursiva=new CheckBox();
		cursiva.setText(" Cursiva ");
		
		final CheckBox subr=new CheckBox();
		subr.setText(" Subrayado ");
		
		
		
		
		cmbTipoLetra= new ComboBox ("Tipo de Letra");
		cmbTipoLetra = ClientUtils.createCombo("Tipo de Letra",
				new SimpleStore(new String[] {"id", "nombre" },
						new String[][] { 
						{ "arial", "Arial" },
						{ "tahoma", "Tahoma" }
						}));
		
				
		

		
	    cmbTamLetra= new ComboBox ("Tamano de Letra");
		cmbTamLetra = ClientUtils.createCombo("Tamano de Letra",
				new SimpleStore(new String[] {"id", "nombre" },
						new String[][] { 
						{ "8", "8" },
						{ "9", "9" },
						{  "10",  "10"},
						{   "12", "12"},
						{  "14", "14"},
						{ "16", "16" },
						{ "18", "18" },
						{  "20",  "20"},
						{   "24", "24"},
						{  "28", "28"},
						{ "32", "32" },
						{ "36", "36" },
						{  "42",  "42"},
						{   "48", "48"},
						{  "56", "56"},
						{ "76", "76" }
						}));
		
						
		

		Panel pan=new Panel();
		pan.add(negrita);
		
		
		
		fsGeneralIzquieda.add(cmbTamLetra);
		fsGeneralIzquieda.add(cmbTipoLetra);
		fsGeneralIzquieda.add(negrita);
		fsGeneralIzquieda.add(cursiva);
		fsGeneralIzquieda.add(subr);
		
		FieldSet fsGeneral=new FieldSet();
		fsGeneral.setTitle("Caracteristicas de la Fuente");
		fsGeneral.setWidth(300);
		fsGeneral.setHeight(400);
		fsGeneral.setPaddings(5);
		fsGeneral.setBorder(true);
		
		fsGeneral.add(fsGeneralIzquieda);
		pGeneral.add(fsGeneral);
		config.add(pGeneral);
		
		
		
		Panel pGeneral2=new Panel("Herramientas de Celdas");
		pGeneral2.setPaddings(5);
		pGeneral2.setLayout(new HorizontalLayout(5));	
		FieldSet fsGeneralCeldas=new FieldSet();
		fsGeneralCeldas.setTitle("Caracteristicas de las Celdas");
		fsGeneralCeldas.setWidth(300);
		fsGeneralCeldas.setHeight(250);
		fsGeneralCeldas.setPaddings(5);
		
		
		FieldSet fsGeneralCeldasH=new FieldSet();
		fsGeneralCeldasH.setTitle("Height");
		final TextField height=new TextField("Alto de Columnas");
		height.setWidth(50);
		
		FieldSet fsGeneralCeldasW=new FieldSet();
		fsGeneralCeldasW.setTitle("Width");
		
		final TextField  muelle=new TextField("Muelle");
		muelle.setWidth(50);
		
		final TextField  placa=new TextField("Placa");
		placa.setWidth(50);
		
		final TextField  destino=new TextField("Destino");
		destino.setWidth(50);
		
		final TextField  entrada=new TextField("Entrada");
		entrada.setWidth(50);
		
		final TextField  salida=new TextField("Salida");
		salida.setWidth(50);
		
		final TextField  retorno=new TextField("Retorno");
		retorno.setWidth(50);
		
		final TextField  estado=new TextField("Estado");
		estado.setWidth(50);
		
		final TextField  ubicacion=new TextField("Ubicacion");
		ubicacion.setWidth(50);
		
		final TextField  conductor=new TextField("Conductor");
		conductor.setWidth(50);
		
		final TextField  celular=new TextField("Celular");
		celular.setWidth(50);
		
		final TextField  trabajo=new TextField("Trabajos");
		trabajo.setWidth(50);
		
		final TextField  ultentrega=new TextField("Ult. Entrega");
		ultentrega.setWidth(50);
		
		final TextField  proxentrega=new TextField("Prox. Entrega");
		proxentrega.setWidth(50);
		
		final TextField  observacion=new TextField("Observacion");
		observacion.setWidth(50);
		
		obtenerValores = new Function() {
			public void execute() {
				gui.getQueriesServiceProxy().obtenerValores(
						 new AsyncCallback<String[]>() {
					@Override
					public void onSuccess(String[] result) {
						cmbTamLetra.setValue(result[0]);
						cmbTipoLetra.setValue(result[1]);
			
						boolean negritav=false;
						boolean cursivav=false;
						boolean subrv=false;
						if(result[3].equalsIgnoreCase("v")){
							negritav=true;
						}
						if(result[4].equalsIgnoreCase("v")){
							cursivav=true;
						}
						if(result[2].equalsIgnoreCase("v")){
							subrv=true;
						}
						
						subr.setValue(subrv);
						cursiva.setValue(cursivav);
						negrita.setValue(negritav);
						muelle.setValue(result[5]);
						placa.setValue(result[6]);
						destino.setValue(result[7]);
						entrada.setValue(result[8]);
						salida.setValue(result[9]);
						retorno.setValue(result[10]);
						estado.setValue(result[11]);
						ubicacion.setValue(result[12]);
						conductor.setValue(result[13]);
						celular.setValue(result[14]);
						trabajo.setValue(result[15]);
						ultentrega.setValue(result[16]);
						proxentrega.setValue(result[17]);
						observacion.setValue(result[18]);
						height.setValue(result[19]);
						
						
					}
					
					@Override
					public void onFailure(Throwable caught) {
					
								ClientUtils.alert("Error",
										"Error al cargar configuraci\u00f3n "
												+ caught.getMessage(),
										ClientUtils.ERROR);
								GWT.log("Error al cargar configuraci\u00f3n ",
												caught);
								wnd.close();
					}
				});
				
			}
		};
		
		obtenerValores.execute();
		
		
		fsGeneralCeldasW.add(muelle);
		fsGeneralCeldasW.add(placa);
		fsGeneralCeldasW.add(destino);
		fsGeneralCeldasW.add(entrada);
		fsGeneralCeldasW.add(salida);
		fsGeneralCeldasW.add(retorno);
		fsGeneralCeldasW.add(estado);
		fsGeneralCeldasW.add(ubicacion);
		fsGeneralCeldasW.add(conductor);
		fsGeneralCeldasW.add(celular);
		fsGeneralCeldasW.add(trabajo);
		fsGeneralCeldasW.add(ultentrega);
		fsGeneralCeldasW.add(proxentrega);
		fsGeneralCeldasW.add(observacion);

		
		fsGeneralCeldasH.add(height);
		fsGeneralCeldas.add(fsGeneralCeldasH);
		fsGeneralCeldas.add(fsGeneralCeldasW);
		pGeneral2.add(fsGeneralCeldas);
		
		
		config.add(pGeneral2);
		
		wnd.add(config);
		Button guardar=new Button("Guardar");
		guardar.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				for(int i=0; i<campos.length; i++){
					switch (i) {
					case 0:
						valorset= muelle.getValueAsString();
					break;
					case 1:
						valorset = placa.getValueAsString();
					break;
					case 2:
						valorset= destino.getValueAsString();
					break;
					case 3:
						valorset= entrada.getValueAsString();
					break;
					case 4:
						valorset= salida.getValueAsString();
					break;
					case 5:
						valorset= retorno.getValueAsString();
					break;
					case 6:
						valorset= estado.getValueAsString();
					break;
					case 7:
						valorset= ubicacion.getValueAsString();
					break;
					case 8:
						valorset= conductor.getValueAsString();
					break;
					case 9:
						valorset= celular.getValueAsString();
					break;
					case 10:
						valorset= trabajo.getValueAsString();
					break;
					case 11:
						valorset= ultentrega.getValueAsString();
					break;
					case 12:
						valorset= proxentrega.getValueAsString();
					break;	
					case 13:
						valorset= observacion.getValueAsString();
					break;	
					}
					
					UpdateCeldas(campos[i], height.getValueAsString(), valorset);
					
					
				}
				
				UpdatePantalla(cmbTipoLetra.getValueAsString(), cmbTamLetra.getValueAsString(), subr.getValue(),negrita.getValue(), cursiva.getValue());
				
				
				
			}
		});
		
		 CerrarVentana= new Function(){
			public void execute() {
			   // wnd.close();
				
				if(control.getTiempAct().getValueAsString().equalsIgnoreCase("1")){
					control.setTiempoAct(300000);
				}else{
					control.setTiempoAct(Integer.parseInt(control.getTiempAct().getValueAsString()));
				}
				control.getTimer().scheduleRepeating(control.getTiempoAct());
				control.cargarAllFlota();
			}};
		
		wnd.addButton(guardar);
		wnd.show();
		
		
	}
	
	
	

		
	
	
	
	
	public void UpdateCeldas(String campo, String h, String w){
		getStaticAdminQueriesServiceAsync().updateCaractColumnas(campo, h, w, new MyBooleanAsyncCallback("Error en la actualizacion del registro",null,null));
	}
	
	public void UpdatePantalla(String letra, String tamano, boolean sub, boolean negrita, boolean cursiva){
		System.out.println("Cursiva: "+String.valueOf(cursiva));
		getStaticAdminQueriesServiceAsync().updateCaractPantalla(letra, tamano, String.valueOf(sub),String.valueOf(negrita),String.valueOf(cursiva), new MyBooleanAsyncCallback("Error en la actualizacion de la configuracion",null,CerrarVentana));
	}
	
	private static AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}
	
	
	
	
	
	
	
	

}
