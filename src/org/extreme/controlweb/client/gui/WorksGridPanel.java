package org.extreme.controlweb.client.gui;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;

import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.DateFieldDef;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.SortState;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.util.DateUtil;
import com.gwtext.client.widgets.grid.BaseColumnConfig;
import com.gwtext.client.widgets.grid.CellMetadata;
import com.gwtext.client.widgets.grid.ColumnConfig;
import com.gwtext.client.widgets.grid.ColumnModel;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.Renderer;
import com.gwtext.client.widgets.grid.RowNumberingColumnConfig;
import com.gwtext.client.widgets.tree.TreeNode;

public class WorksGridPanel extends GridPanel {
	
	private RecordDef recordDefProperties;
	private Store store;

	public WorksGridPanel(boolean hideAsignado) {
		super();
		this.setTitle("Trabajos");
		
		recordDefProperties = new RecordDef(new FieldDef[] {
				new StringFieldDef("codigo"), 
				new StringFieldDef("tipotrabajo"),
				new StringFieldDef("direccion"),
				new StringFieldDef("municipio"),
				new StringFieldDef("estado"),
				new StringFieldDef("asignadoa"),
				new StringFieldDef("atraso"),
				new DateFieldDef("fecha", "Y/m/d H:i:s A")
		 });
		
		store = new Store(recordDefProperties);
		store.setSortInfo(new SortState("fecha", SortDir.ASC));
		
		ColumnConfig c1 = new ColumnConfig("C\u00f3digo", "codigo", 130, true);
		ColumnConfig c2 = new ColumnConfig("Tipo de Trabajo", "tipotrabajo", 130, true);
		ColumnConfig c3 = new ColumnConfig("Direcci\u00f3n", "direccion", 130, true);
		ColumnConfig c4 = new ColumnConfig("Municipio", "municipio", 130, true);
		ColumnConfig c5 = new ColumnConfig("Estado", "estado", 130, true);
		ColumnConfig c6 = new ColumnConfig("Asignado A", "asignadoa", 200, true);
		ColumnConfig c7 = new ColumnConfig("Atraso", "atraso", 80, true);
		ColumnConfig c8 = new ColumnConfig("Fecha", "fecha", 140, true, new Renderer(){
			public String render(Object value, CellMetadata cellMetadata,
					Record record, int rowIndex, int colNum, Store store) {
				Date date = (Date)value;
				return DateUtil.format(date, "Y/m/d H:i:s A");
			}
		});
		
		if(hideAsignado)
			c6.setHidden(true);
		
		ColumnModel columnModel = new ColumnModel(
				new BaseColumnConfig[] { new RowNumberingColumnConfig(), c1,
						c2, c3, c4, c5, c6, c7, c8 });
		
		this.setStore(store);
		this.setColumnModel(columnModel);
		this.setTrackMouseOver(true);
		this.setStripeRows(true);
		this.setEnableHdMenu(false);
	}
	
	public void addWorks(ArrayList<TreeNode> works) {
		for (int i = 0; i < works.size(); i++) {
		
			TreeNode node = (TreeNode) works.get(i);
			TreeNode parent = (TreeNode) node.getParentNode();
			
			Trabajo trab = (Trabajo) node.getAttributeAsObject("entity"); 
			Recurso rec = null;
			if(parent.getAttributeAsObject("entity") instanceof Recurso) 
				rec = (Recurso) parent.getAttributeAsObject("entity");

			/*String trabajo = node.getAttribute("trabajo");
			String tipotrabajo = node.getAttribute("nomtipotrabajo");
			String dir = node.getAttribute("dir");
			String estado = node.getAttribute("estado");
			String municipio = node.getAttribute("municipio");*/
			
			String trabajo = trab.getId();
			String tipotrabajo = trab.getTpoTrab().getNombre();
			String dir = trab.getDireccion();
			String estado = trab.getEstado();
			String municipio = trab.getMunicipio().getNombre();
			
			
			if (estado != null) {
				if (estado.equals("00"))
					estado = "Recien Creado";
				else if (estado.equals("10"))
					estado = "Pr\u00f3ximo Trabajo";
				else if (estado.equals("20"))
					estado = "En Ejecuci\u00f3n";
				else if (estado.equals("50"))
					estado = "Rechazado";
				else if (estado.equals("60"))
					estado = "En Pausa";
				else
					estado = "N/A";
			} else
				estado="N/A";
			
			String asignadoA = "";
			
			if(rec == null) {
				if (parent.getAttribute("id").equals("noUbic"))
					asignadoA = "Trabajo No Ubicado";
				else if (parent.getAttribute("id").equals("noAsig"))
					asignadoA = "Trabajo No Asignado";
				
			} else
				asignadoA = rec.getNombre() + " ["
						+ rec.getCont().getNombre() + "]";
			
			String atraso = trab.getAtraso();
			String fecha = trab.getFecha();

			store.addSorted(recordDefProperties.createRecord(new String[] { trabajo,
					tipotrabajo, dir, municipio, estado, asignadoA, atraso, fecha }));
		}
		store.commitChanges();

	}
	
}
