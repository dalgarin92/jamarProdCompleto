package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.admin.GenericAdminPanelAdapter;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Tool;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class SearchWorkButtonListener extends BaseItemListenerAdapter {
		
	private static QueriesServiceAsync serviceProxy;
	private ControlWebGUI gui;

	public SearchWorkButtonListener(ControlWebGUI gui) {
		this.gui = gui;
	}
	
	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
	
		Window wnd = new Window("Buscar Trabajo", false, true);
		
		wnd.addTool(new Tool(Tool.HELP, new Function(){
			@Override
			public void execute() {
				ClientUtils.alert("Ayuda", "Las b\u00fasquedas por C\u00f3digo OT y matr\u00edcula son de coincidencia exacta. <br/>" +
						"Al buscar por direcci\u00f3n, puede buscar adem\u00e1s por semejanza, <br/>" +
						"utilizando el caracter especial % al principio y/o al final de la cadena a buscar.", ClientUtils.INFO);
			}
		}));
		
		wnd.setLayout(new RowLayout());
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(450);
		wnd.setHeight(500);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");
		
		GenericAdminPanelAdapter gap = new GenericAdminPanelAdapter(null, "Ver en \u00e1rbol", false, true, false) {		
			@Override
			protected void loadGridFunction(AsyncCallback<ArrayList<RowModel>> rows) {
				
			}
			
			@Override
			protected void addFunction(Record[] recs) { //queryFunction
				getStaticQueriesServiceAsync().getTrabajos(getField(1).getValueAsString(), 
						getField(0).getValueAsString(), getField(2).getValueAsString(), 
						gui.getProceso().getId(), this);
			}

			@Override
			protected void detailFunction(final Record r) {
				if (r.getAsString("idrec") != null
						&& !r.getAsString("idrec").trim().equals("")) {
					new NodeTreeClickListener(gui, true).updateRecurso(
							r.getAsString("idrec"), r.getAsString("idcont"),
							new Function() {
						@Override
						public void execute() {
							gui.searchAndSelectWork(r.getAsString("C\u00f3digo OT"));
						}
					});
				} else {
					if (r.getAsString("Asignado a").equals("No Ubicado")
							|| r.getAsString("Asignado a").equals("No Asignado")) {
						gui.searchAndSelectWork(r.getAsString("C\u00f3digo OT"));
					}
				}
			}
			
		};
		gap.getButton().setText("Consultar");
		gap.setMsgNoResults("La consulta no arroj\u00f3 resultados con el criterio seleccionado.");
		
		
		ArrayList<RowModel> values = new ArrayList<RowModel>();
		
		RowModelAdapter val1 = new RowModelAdapter();
		val1.addValue("id", "1");
		val1.addValue("nombre", "C\u00f3digo OT");
		
		RowModelAdapter val2 = new RowModelAdapter();
		val2.addValue("id", "2");
		val2.addValue("nombre", "Matr\u00edcula");
		
		RowModelAdapter val3 = new RowModelAdapter();
		val3.addValue("id", "3");
		val3.addValue("nombre", "Direcci\u00f3n");
		
		values.add(val1);
		values.add(val2);
		values.add(val3);

		gap.addStaticCombo("Criterio", values);
		
		gap.addCombo("Municipio", new ComboLoader(){
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				getStaticQueriesServiceAsync().getMunicipiosDeProceso(gui.getProceso().getId(), cb);
			}
		}, false);		
				
		gap.addTextField("Par\u00e1metro B\u00fasqueda");
		
		gap.loadAllCombos();
		wnd.add(gap);
		
		wnd.show();
	}
}
