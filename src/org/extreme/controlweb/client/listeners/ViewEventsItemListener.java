package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.ProcInterface;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.ui.Frame;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.TimeField;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ViewEventsItemListener extends BaseItemListenerAdapter { // Se utiliza para configurar el reporte de  eventos
																	  // de uno o todos los carros de un proceso

	private ControlWebGUI gui;
	private String tipoReporte;
	private DateWindow wnd;
	private Recurso recData;
	private ProcInterface proceso;
	private Panel panel;
	private MyGridPanel grid;
		
	public ViewEventsItemListener(ControlWebGUI gui, Object obj, String tipoReporte) {		
		this.gui = gui;
		//Object obj = treeNode.getAttributeAsObject("entity");
		if(obj instanceof Recurso) {
			this.recData = (Recurso) obj;
			this.proceso =  recData.getProceso();
		} else
			this.proceso = (ProcInterface) obj;
		this.tipoReporte = tipoReporte;
		
	}
	

	public void onClick(BaseItem item, EventObject e) {
		panel = new Panel();
		panel.setLayout(new RowLayout());
		panel.setClosable(true);
		grid = new MyGridPanel();
		if (tipoReporte.equals("eventos")) {
			toEventos();			
		} else if(tipoReporte.equals("posactual")){
			toPosActual();
		} else if(tipoReporte.equals("geocerca")) {
			toGeocerca();			
		} else if(tipoReporte.equals("odometro")) {
			toOdometro();			
		}
	}

	private void toOdometro() {
		wnd = new DateWindow("Reporte de Lecturas de Od\u00f3metro", false, false);
		wnd.addOKListener(new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				if (!wnd.getFromDate().equals("")
						&& !wnd.getToDate().equals("")) {
					if (wnd.getFromDateAsDate().before(wnd.getToDateAsDate())
							|| wnd.getFromDateAsDate().equals(wnd.getToDateAsDate())){
						gui.getQueriesServiceProxy().getOdometro(
								recData.getId(),
								recData.getCont().getId(),
								wnd.getFromDate(),
								wnd.getToDate(),
								new RPCFillTable(grid, panel, null, false,
										false, true, null, null,
										null, new Function() {
											@Override
											public void execute() {
												gui.hideMessage();
												
												String msg = "";
												if (recData != null && recData.getVeh() != null
														&& recData.getVeh().getIdVeh() != null) {
													msg = recData.getVeh().getPlaca() + " ["
															+ recData.getVeh().getIdVeh() + "]";
												}
												panel.setTitle("Reporte de Lecturas de Od\u00f3metro - "
														+ msg);
												gui.addInfoTab(panel);
											}
										}));
					} else
						ClientUtils.alert("Error", "Rango de fechas inv\u00e1lido.", ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error", "Todas las opciones son requeridas.", ClientUtils.ERROR);
				wnd.close();
				gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);
			}
		});		
		wnd.show();		
	}

	private void toGeocerca() {
		wnd = new DateWindow("Reporte de Geocercas", true, true);
		
		gui.getQueriesServiceProxy().GeocercasDeRecurso(recData.getId(),
				recData.getCont().getId(), new RPCFillCheckboxGridPanel(wnd.getCheckGrid(), new Function() {
					@Override
					public void execute() {
						wnd.renderGrid();
					}
				}));
		
		wnd.addOKListener(new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				if (!wnd.getFromDate().equals("")
						&& !wnd.getToDate().equals("")) {
					if (wnd.getFromDateAsDate().before(wnd.getToDateAsDate())
							|| (wnd.getFromDateAsDate().equals(wnd.getToDateAsDate()) 
									&& wnd.getFromTime().compareTo(wnd.getToTime()) < 0)) {
						ArrayList<String> geos = new ArrayList<String>();
						String[] recs = wnd.getCheckGrid().getSelectedIDs();
						for (String record : recs) 
							geos.add(record);
												
						gui.getQueriesServiceProxy().getInfoGeocercas(
								recData.getId(), recData.getCont().getId(),
								wnd.getFromDate(), wnd.getToDate(), geos, 
								new RPCFillTable(grid, panel, null, false,
										false, true, null, null,
										null, new Function() {
											@Override
											public void execute() {
												gui.hideMessage();
												String msg = "";
												if (recData != null && recData.getVeh() != null
														&& recData.getVeh().getIdVeh() != null) {
													msg = recData.getVeh().getPlaca() + " ["
															+ recData.getVeh().getIdVeh() + "]";
												}
												panel.setTitle("Reporte de Geocercas - " + msg);

												gui.addInfoTab(panel);
											}
										}));
					} else
						ClientUtils.alert("Error", "Rango de fechas inv\u00e1lido.", ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error", "Todas las opciones son requeridas.", ClientUtils.ERROR);
				wnd.close();
				gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);
			}
		});
		
		wnd.show();
	}



	private void toPosActual() {
		gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);
		gui.getQueriesServiceProxy().getPosActualVehiculos("100",
				new RPCFillTable(grid, panel, null, false, false, true, null, null, null, new Function() {
					@Override
					public void execute() {
						gui.hideMessage();
						panel.setTitle("Posiciones Actuales de Veh\u00edculos");
						gui.addInfoTab(panel);
					}
				}));
	}
	
	private void toEventos() {
		final Window wnd = new Window("Reporte de Eventos", true, true);
		wnd.setHeight(400);
		wnd.setWidth(370);
		wnd.setButtonAlign(Position.CENTER);
		wnd.setLayout(new BorderLayout());
		wnd.setPaddings(5);
		MultiFieldPanel fromPanel = new MultiFieldPanel();
		fromPanel.setBorder(false);
		MultiFieldPanel toPanel = new MultiFieldPanel();
		toPanel.setBorder(false);
		final DateField fromDate = new DateField("Fecha Inicial", "Y/m/d");
		fromDate.setValue(new Date());
		fromDate.setReadOnly(true);
		fromDate.setWidth(95);
		final TimeField fromTime = new TimeField();
		fromTime.setHideLabel(true);
		fromTime.setReadOnly(true);
		fromTime.setWidth(65);
		fromTime.setFormat("H:i");
		fromTime.setIncrement(5);
		fromTime.setValue("00:00");
		final DateField toDate = new DateField("Fecha Final", "Y/m/d");
		toDate.setValue(new Date());
		toDate.setReadOnly(true);
		toDate.setWidth(95);
		final TimeField toTime = new TimeField();
		toTime.setHideLabel(true);
		toTime.setReadOnly(true);
		toTime.setWidth(65);
		toTime.setFormat("H:i");
		toTime.setIncrement(5);
		toTime.setValue("23:55");
		fromPanel.addToRow(fromDate, 205);
		fromPanel.addToRow(fromTime, new ColumnLayoutData(1));
		toPanel.addToRow(toDate, 205);
		toPanel.addToRow(toTime, new ColumnLayoutData(1));
		final CheckBoxGridPanel checkGrid = new CheckBoxGridPanel(true);
		final Panel gridPanel = new Panel("Eventos");
		gridPanel.setLayout(new RowLayout());
		Panel datesPanel = new Panel();
		datesPanel.setBorder(false);
		datesPanel.setPaddings(10, 40, 10, 10);
		datesPanel.setHeight(75);
		datesPanel.add(fromPanel);
		datesPanel.add(ClientUtils.getSeparator());
		datesPanel.add(toPanel);
		
		wnd.add(gridPanel, new BorderLayoutData(RegionPosition.CENTER));
		wnd.add(datesPanel, new BorderLayoutData(RegionPosition.NORTH));
		// gui.addInfoTab(gridPanel);
		
		gui.getQueriesServiceProxy().getEventosDesc(new RPCFillCheckboxGridPanel(checkGrid, new Function() {
			@Override
			public void execute() {
				gridPanel.add(checkGrid, new RowLayoutData());				
			}
		}));
		
		Button btOK = new Button("Aceptar", new ButtonListenerAdapter() {
			private String urlReport;
			public void onClick(Button button, EventObject e) {
				if (!fromDate.getEl().getValue().equals("")
						&& !toDate.getEl().getValue().equals("")
						&& !fromTime.getText().equals("")
						&& !toTime.getText().equals("")) {

					if (fromDate.getValue().before(toDate.getValue())
							|| (fromDate.getValue().equals(toDate
									.getValue()))
							&& fromTime.getValue().compareTo(
									toTime.getValue()) < 0) {
						String[] rec = checkGrid.getSelectedIDs();
						
						
						
						/*String url = ClientUtils.CENTRAL_QUERIES_SERVLET
								+ "?tipo=track&recurso="
								+ recData.getId()
								+ "&contratista="
								+ recData.getCont().getId()
								+ "&desde=" + fromDate.getEl().getValue()
								+ " " + fromTime.getText() + "&hasta="
								+ toDate.getEl().getValue() + " "
								+ toTime.getText();*/
						
						urlReport = ClientUtils.REPORT_EXPORTER_SERVLET
								+ "?tipo=repeventos&recurso="
								+ recData.getId()
								+ "&contratista="
								+ recData.getCont().getId()
								+ "&desde=" + fromDate.getEl().getValue()
								+ " " + fromTime.getText() + "&hasta="
								+ toDate.getEl().getValue() + " "
								+ toTime.getText();
					
						ArrayList<String> events = new ArrayList<String>();
						for (int i = 0; i < rec.length; i++) {
							events.add(rec[i]);
							urlReport += "&evento" + (i + 1) + "=" + rec[i];
						}
						
						Toolbar toolbar = new Toolbar();
						
						ToolbarButton reportButton = new ToolbarButton("Exportar a Excel");
						reportButton.setIconCls("x-btn-icon save-report-btn");
				        												
				        reportButton.addListener(new ButtonListenerAdapter(){
				        	public void onClick(Button button, EventObject e) {
				        		Frame frame = new Frame(urlReport);
				        		Panel panel = new Panel("Exportar Reporte");
				        		panel.setLayout(new FitLayout());
				        		panel.setId("xlsReport");
				        		panel.setClosable(true);
				        		panel.add(frame);
				        		gui.addInfoTab(panel);
				        	}
				        });

				        toolbar.addButton(reportButton);
						
						panel.setTopToolbar(toolbar);
						
						gui.getQueriesServiceProxy().getInfoEventos(recData.getId(), 
								recData.getCont().getId(), fromDate.getEl().getValue() + " " + fromTime.getText(), 
								toDate.getEl().getValue() + " " + toTime.getText(), events, 
								new RPCFillTable(grid, panel, null, false,
										false, true, null, null,
										null, new Function() {
											@Override
											public void execute() {
												gui.hideMessage();
												String msg = "";
												if (recData != null && recData.getVeh() != null
														&& recData.getVeh().getIdVeh() != null) {
													msg = recData.getVeh().getPlaca() + " ["
															+ recData.getVeh().getIdVeh() + "]";
												}
												panel.setTitle("Listado de Eventos - "  
														+ msg);
												gui.addInfoTab(panel);
											}
										}));
					} else
						ClientUtils.alert("Error", "Rango de fechas inv\u00e1lido.", ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error", "Todas las opciones son requeridas.", ClientUtils.ERROR);
				wnd.close();
				gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);
			}
		});
		Button btCancel = new Button("Cancelar",
				new ButtonListenerAdapter() {
					public void onClick(Button button, EventObject e) {
						wnd.close();
					}
				});
		wnd.addButton(btOK);
		wnd.addButton(btCancel);
		wnd.show();
	}
}
