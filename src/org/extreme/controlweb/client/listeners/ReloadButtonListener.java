package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;

public class ReloadButtonListener extends ButtonListenerAdapter  {

	private ControlWebGUI gui;


	public ReloadButtonListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	@Override
	public void onClick(Button button, EventObject e) {

		gui.reloadTree();
	}

}
