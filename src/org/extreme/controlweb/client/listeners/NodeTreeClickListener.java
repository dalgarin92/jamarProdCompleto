package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.RPCUpdateRecurso;
import org.extreme.controlweb.client.map.openlayers.OpenLayersMap;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Node;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;
import com.gwtext.client.widgets.tree.event.TreeNodeListenerAdapter;

public class NodeTreeClickListener extends TreeNodeListenerAdapter{
	private ControlWebGUI gui;
	private OpenLayersMap map;
	private TreePanel treePanel;
	private boolean runOnExpand;

	public NodeTreeClickListener(ControlWebGUI gui, boolean runOnExpand) {
		this.gui = gui;
		map = gui.getMap();
		treePanel = gui.getTree();
		this.runOnExpand = runOnExpand;
	}

	@Override
	public void onCheckChanged(Node node, boolean checked) {
		if(!checked) {
			((Recurso)node.getAttributeAsObject("entity")).setChecked(checked);
			map.getMkVeh().removeMarker((Marker)node.getAttributeAsObject("marker"));
			node.setAttribute("marker", null);
		}
	}

	@Override
	public void onExpand(Node node) {
		if(runOnExpand) {
			showMarkers(node);
		}
	}

	@Override
	public void onClick(Node node, EventObject e) {
		showMarkers(node);
	}

	private void showMarkers(Node node) {
		int first = 0;
		if (((TreeNode) node).getAttributeAsObject("marker") != null) {
			first = 1;
		}

		ArrayList<Object> v = gui.getMarkers((TreeNode) node);
		GWT.log("Entrando al evento onExpand: " + runOnExpand, null);
		((TreeNode) node).expand();
		((TreeNode) node).select();
		if (v.size() > 0) {
			map.clearOverlays();

			if (first == 1) {
				map.getMkVeh().addMarker((Marker) ((TreeNode) node).getAttributeAsObject("marker"));
			}

			for (int i = first; i < v.size(); i++) {
				map.getMkTrab().addMarker((Marker)v.get(i));
			}

			map.extend(v);
			map.traceRoute(v, first);
			gui.showJobPath((TreeNode) node);
		}
	}

	public void updateRecurso(Node node) {
		Recurso rec = (Recurso) node.getAttributeAsObject("entity");
		updateRecurso(rec.getId(), rec.getCont().getId(), null);
	}

	public void updateRecurso(String idrec, String idcont, Function afterUpdate) {
		TreeNode node = gui.searchRecurso(idrec, idcont);

		// ((TreeNode)node).getUI().toggleCheck(true);
		treePanel.getEl().mask(
				"Actualizando " + node.getText().split(" -")[0],
				"x-mask-loading");

		Recurso rec = (Recurso) node.getAttributeAsObject("entity");

		QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
				.create(QueriesService.class);
		serviceProxy.updateRecurso(rec.getId(), rec.getCont().getId(), gui
				.getProceso().getId(), gui.getProceso().isSoloCentral(),
				gui.getDfDateProg().getEl().getValue(),
				new RPCUpdateRecurso(gui, node, true, afterUpdate));
	}
}
