package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.gwtext.client.core.AnimationConfig;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ViewClosedWorksItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private DateWindow wnd;
	private MyGridPanel grid;
	private QueriesServiceAsync serviceProxy;
	private Recurso rec;
	private String urlReport;

	public ViewClosedWorksItemListener(ControlWebGUI gui) {
		this.gui = gui;		
		serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
		
	}
	
	public ViewClosedWorksItemListener(ControlWebGUI gui, Recurso rec) {
		this.gui = gui;
		this.rec = rec;
		serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);	
	}
	

	public void onClick(BaseItem item, EventObject e) {
		wnd = new DateWindow("Consulta de Trabajos");
		
		final Panel p = new Panel();
		p.setLayout(new FormLayout());
		p.setBorder(false);
		p.setPaddings(5, 0, 0, 0);
		
		final Panel p1 = new Panel();
		p1.setVisible(true);
		p1.setBorder(false);
		p1.setLayout(new FormLayout());
		
		final Panel p01 = new Panel();
		p01.setVisible(true);
		p01.setBorder(false);
		p01.setLayout(new FormLayout());
		
		final ArrayList<RowModel> values = new ArrayList<RowModel>();

		RowModelAdapter val1 = new RowModelAdapter();
		val1.addValue("id", "0");
		val1.addValue("nombre", "Pendientes");

		RowModelAdapter val2 = new RowModelAdapter();
		val2.addValue("id", "1");
		val2.addValue("nombre", "Precerrados");

		RowModelAdapter val3 = new RowModelAdapter();
		val3.addValue("id", "2");
		val3.addValue("nombre", "Todos");
		
		RowModelAdapter val4 = new RowModelAdapter();
		val4.addValue("id", "3");
		val4.addValue("nombre", "Rechazados");		
		
		values.add(val1);
		values.add(val2);
		values.add(val4);
		values.add(val3);
		
		final MyComboBox mcb = new MyComboBox("Buscar trabajos");
		mcb.setEditable(false);

		ComboLoader cl = new ComboLoader() {
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				new RPCFillComplexComboHandler(mcb, false, p,
						new RowLayoutData()).onSuccess(values);
			}
		};
		cl.load(new RPCFillComplexComboHandler(mcb, p, 0));
		mcb.setValue("0");
		
		final ArrayList<RowModel> buscar = new ArrayList<RowModel>();

		RowModelAdapter bus1 = new RowModelAdapter();
		bus1.addValue("id", "0");
		bus1.addValue("nombre", "Fecha de Creaci\u00f3n");

		RowModelAdapter bus2 = new RowModelAdapter();
		bus2.addValue("id", "1");
		bus2.addValue("nombre", "Fecha de Precierre");

		buscar.add(bus1);
		buscar.add(bus2);

		final MyComboBox mcbBuscar = new MyComboBox("Criterio");
		mcbBuscar.setEditable(false);

		ComboLoader cl1 = new ComboLoader() {
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				new RPCFillComplexComboHandler(mcbBuscar, false, p1,
						new RowLayoutData()).onSuccess(buscar);
			}
		};
		cl1.load(new RPCFillComplexComboHandler(mcbBuscar, p1, 0));
		mcbBuscar.setValue("0");
		
		final ArrayList<RowModel> buscar1 = new ArrayList<RowModel>();

		RowModelAdapter bus01 = new RowModelAdapter();
		bus01.addValue("id", "0");
		bus01.addValue("nombre", "Fecha de Creaci\u00f3n");

		RowModelAdapter bus02 = new RowModelAdapter();
		bus02.addValue("id", "1");
		bus02.addValue("nombre", "Fecha de Programaci\u00f3n");

		buscar1.add(bus01);
		buscar1.add(bus02);

		final MyComboBox mcbBuscar1 = new MyComboBox("Criterio");
		mcbBuscar1.setEditable(false);

		ComboLoader cl01 = new ComboLoader() {
			@Override
			public void load(AsyncCallback<ArrayList<RowModel>> cb) {
				new RPCFillComplexComboHandler(mcbBuscar1, false, p01,
						new RowLayoutData()).onSuccess(buscar1);
			}
		};
		cl01.load(new RPCFillComplexComboHandler(mcbBuscar1, p01, 0));
		mcbBuscar1.setValue("0");
		
		mcb.addListener(new ComboBoxListenerAdapter()
		{
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				if (mcb.getValue().equals("1")) {
					if (!wnd.getPanelDate().isVisible()) {
						wnd.getPanelDate().setVisible(true);
						wnd.getAdditionalPanel().setVisible(true);
						wnd.getPanelDate().getEl().show(true);
						wnd.getAdditionalPanel().getEl().show(true);
					}
					if (!p1.isVisible()) {
						p1.setVisible(true);
						p1.getEl().show(true);
					}
					if (p01.isVisible()) {
						/*AnimationConfig efect3 = new AnimationConfig();
						efect3.setCallback(new Function() {
							
							@Override
							public void execute() {
								p01.setVisible(false);
							}
						});
						p01.getEl().hide(efect3);*/
						p01.setVisible(false);
					}
					wnd.setSize(310, 218);
				/*} else if (mcb.getValue().equals("3")) {
					if (wnd.getPanelDate().isVisible()) {
						AnimationConfig efect1 = new AnimationConfig();
						efect1.setCallback(new Function() {
							
							@Override
							public void execute() {
								wnd.getPanelDate().setVisible(false);
							}
						});
						
						AnimationConfig efect2 = new AnimationConfig();
						efect2.setCallback(new Function() {
							
							@Override
							public void execute() {
								wnd.getAdditionalPanel().setVisible(false);
							}
						});
						
						AnimationConfig efect3 = new AnimationConfig();
						efect3.setCallback(new Function() {
							
							@Override
							public void execute() {
								p1.setVisible(false);
								wnd.setPosition(wnd.getPosition()[0],wnd.getPosition()[1]);
							}
						});
						
						AnimationConfig efect4 = new AnimationConfig();
						efect4.setCallback(new Function() {
							
							@Override
							public void execute() {
								p01.setVisible(false);
								//wnd.setPosition(wnd.getPosition()[0],wnd.getPosition()[1]);
							}
						});
						
						wnd.getPanelDate().getEl().hide(efect1);
						wnd.getAdditionalPanel().getEl().hide(efect2);
						p1.getEl().hide(efect3);
						p01.getEl().hide(efect4);
						
					}
					wnd.setSize(310, 100); //35*/
				} else if (mcb.getValue().equals("0")) {
					if (!wnd.getPanelDate().isVisible()) {
						wnd.getPanelDate().setVisible(true);
						wnd.getAdditionalPanel().setVisible(true);
						wnd.getPanelDate().getEl().show(true);
						wnd.getAdditionalPanel().getEl().show(true);
					}
					if (p1.isVisible()) {
						/*AnimationConfig efect3 = new AnimationConfig();
						efect3.setCallback(new Function() {
							
							@Override
							public void execute() {
								p1.setVisible(false);
							}
						});
						p1.getEl().hide(efect3);*/
						p1.setVisible(false);
					}
					
					if (!p01.isVisible()) {
						p01.setVisible(true);
						p01.getEl().show(true);
					}
					wnd.setSize(310, 218);
				} else {
					if (!wnd.getPanelDate().isVisible()) {
						wnd.getPanelDate().setVisible(true);
						wnd.getAdditionalPanel().setVisible(true);
						wnd.getPanelDate().getEl().show(true);
						wnd.getAdditionalPanel().getEl().show(true);
					}
					if (p1.isVisible()) {
						/*AnimationConfig efect3 = new AnimationConfig();
						efect3.setCallback(new Function() {
							
							@Override
							public void execute() {
								p1.setVisible(false);
							}
						});
						p1.getEl().hide(efect3);*/
						p1.setVisible(false);
					}
					
					if (p01.isVisible()) {
						p01.setVisible(false);
					}
					wnd.setSize(310, 204);
				}
			}
		});
		
		p.add(p1);
		p.add(p01);
		wnd.add(p);
		wnd.createDateWindow(true, false);
		wnd.setSize(310, 218);
		wnd.setResizable(false);
		
		
		
		final MyComboBox cbTTrab = new MyComboBox("Tipo Trabajo");
		gui.getQueriesServiceProxy().getTiposTrabajo(gui.getProceso().getId(), 
						new RPCFillComboBoxHandler(cbTTrab, wnd
								.getAdditionalPanel(), 2));
		
		wnd.addOKListener(new ButtonListenerAdapter() {	

			public void onClick(Button button, EventObject e) {
				if (!wnd.getFromDate().equals("")
						&& !wnd.getToDate().equals("")) {
					if (wnd.getFromDateAsDate().before(wnd.getToDateAsDate())
							|| (wnd.getFromDateAsDate().equals(wnd.getToDateAsDate()) 
									&& wnd.getFromTime().compareTo(wnd.getToTime()) < 0)) {
						
						boolean criterio = false;
						Panel p= new Panel("Reporte de Trabajos");
						p.setClosable(true);
						p.setLayout(new RowLayout());

						/*if(!mcb.getValue().equals("1"))
							urlReport = "";
						else*/
		     
						urlReport = ClientUtils.REPORT_EXPORTER_SERVLET
								+ "?tipo=repprecierre"
								+ "&proceso=" + gui.getProceso().getId()
								+ "&desde=" + wnd.getFromDate()
								+ "&hasta="	+ wnd.getToDate()
								+ "&ttrab=" + cbTTrab.getValue()
								+ "&mcb=" + mcb.getValue()
								+ "&criterio=" + criterio 
								+ ( rec != null ? "&recurso=" + rec.getId()
										+ "&contratista=" + rec.getCont().getId():"");
						
						Toolbar toolbar = new Toolbar();
						
						ToolbarButton reportButton = new ToolbarButton("Exportar Precierres a Excel");
						reportButton.setIconCls("x-btn-icon save-report-btn");
				        												
				        reportButton.addListener(new ButtonListenerAdapter(){
				        	public void onClick(Button button, EventObject e) {
				        		if (!urlReport.equals("")) {
									Frame frame = new Frame(urlReport);
									Panel panel = new Panel("Exportar Reporte");
									panel.setLayout(new FitLayout());
									panel.setId("xlsReport");
									panel.setClosable(true);
									panel.add(frame);
									gui.addInfoTab(panel);
								} else
									ClientUtils.alert("Error", "No se realizo una consulta de precierres.", ClientUtils.ERROR);
				        	}
				        });

				        toolbar.addButton(reportButton);
						
						p.setTopToolbar(toolbar);
						
						grid = new MyGridPanel();
						grid.addDetListener(new MyGridOnCellClickListener(){
							public void onCellClick(final Record r) {
								gui.getTrabajoInfoPanel(r.getAsString("id"), 
										r.getAsString("tipo"),  null, r.getAsString("Estado"));
								if(!(r.getAsString("Estado").equals("40") ||
										r.getAsString("Estado").equals("30")))
									gui.searchAndSelectWork(r.getAsString("C\u00f3digo OT"), true);
							}
						});
						
						if(mcb.getValue().equals("0"))
							criterio = mcbBuscar1.getValue().equals("0") ? true : false;
						else if(mcb.getValue().equals("1"))
							criterio = mcbBuscar.getValue().equals("0") ? true : false;
						if(rec != null)
							serviceProxy.getInfoTrabajosPrecerrados(gui
									.getProceso().getId(), wnd.getFromDate(), wnd
									.getToDate(), rec.getId(), rec.getCont().getId(), cbTTrab.getValue(),
									new RPCFillTable(grid, p,
									"Ver detalle de precierre", false, false, true,
									null, null, new RowLayoutData(), new Function(){
										public void execute() {
											if(grid.getStore()==null){
												ClientUtils.alert("Info", "No hay Datos, para este reporte", ClientUtils.INFO);
										
											}else{gui.hideMessage();}
										}
									}));
						else
							serviceProxy.getInfoTrabajosPrecerrados(gui
									.getProceso().getId(), wnd.getFromDate(), wnd
									.getToDate(), cbTTrab.getValue(),
									mcb.getValue(), criterio,
									new RPCFillTable(grid, p,
									"Ver detalle de precierre", false, false, true,
									null, null, new RowLayoutData(), new Function(){
										public void execute() {
											if(grid.getStore()==null){
												ClientUtils.alert("Info", "No hay Datos, para este reporte", ClientUtils.INFO);
												
										}else{gui.hideMessage();}
										}
									}));
						
						gui.addInfoTab(p);
						
					} else
						ClientUtils.alert("Error", "Rango de fechas inv\u00e1lido.", ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error", "Todas las opciones son requeridas.", ClientUtils.ERROR);
				wnd.close();
				gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);
			}
		});
		wnd.show();
		p1.setVisible(false);
	}
}
