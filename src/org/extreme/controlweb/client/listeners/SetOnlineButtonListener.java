package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class SetOnlineButtonListener extends ButtonListenerAdapter implements
		AsyncCallback<Boolean> {

	TreePanel panel;
	TreeNode selectedNode;
	private ControlWebGUI gui;
	private Recurso rec;

	public SetOnlineButtonListener(ControlWebGUI gui) {
		this.gui = gui;
		this.panel = gui.getTree();
	}

	public void onClick(Button button, EventObject e) {
		TreeNode selectedNode = gui.getMap().getSelectedRecursoNode();
		if (selectedNode != null) {
				this.selectedNode = selectedNode;
			if (Window.confirm("Realmente desea cambiar el modo de trabajo del recurso "
							+ selectedNode.getText().split(" -")[0])) {
				rec = (Recurso) selectedNode.getAttributeAsObject("entity");
				
				String recurso = rec.getId();
				String contratista = rec.getCont().getId();

				String online = rec.isOnline() ? "N" : "S";

				QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
						.create(QueriesService.class);
				serviceProxy.setResourceMode(recurso, contratista, online, this);
			}
		}

	}
	
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}
	
	public void onSuccess(Boolean result) {
		NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
		l.updateRecurso(selectedNode);
		ClientUtils.alert("Informaci\u00f3n", "El recurso trabaja ahora "
				+ (rec.isOnline() ? "OFFLINE" : "ONLINE"), ClientUtils.INFO);

		if (rec.isOnline())
			rec.setOnline(false);
		else
			rec.setOnline(true);
	}
}
