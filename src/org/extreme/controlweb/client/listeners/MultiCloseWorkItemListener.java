package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class MultiCloseWorkItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private TreeNode selectedNode;
	private Recurso rec;
	private Window wdmultiJobsClose;
	private CheckBoxGridPanel grid;

	public MultiCloseWorkItemListener(ControlWebGUI gui, TreeNode treeNode,
			Recurso rec) {
		this.gui = gui;
		this.selectedNode = treeNode;
		this.rec = rec;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {

		grid = new CheckBoxGridPanel();

		if (grid.isRendered())
			grid.getEl().mask("Por favor, espere...");
		wdmultiJobsClose = new Window("Cerrar Multiples trabajos", 830, 500,
				false, true);
		wdmultiJobsClose.setLayout(new FitLayout());
		Button jobsClos = new Button("Cerrar Trabajos");
		jobsClos.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				Record trabajos[] = grid.getSelected();
				ArrayList<String[]> trabajosArray = new ArrayList<String[]>();
				for (int i = 0; i < trabajos.length; i++)
					trabajosArray.add(new String[] {
							trabajos[i].getAsString("Trabajo"),
							trabajos[i].getAsString("Tipotrabajo") });

				if (com.google.gwt.user.client.Window
						.confirm("Realmente desea cerrar los trabajos ")) {
					gui.getQueriesServiceProxy().closeJobArray(trabajosArray,
							gui.getUsuario().getId(),
							new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							ClientUtils.alert("Error", caught
									.getMessage(), ClientUtils.ERROR);
							GWT.log("Error RPC", caught);
						}

						@Override
						public void onSuccess(Boolean result) {
							wdmultiJobsClose.close();
							Object obj = selectedNode
									.getAttributeAsObject("entity");
							if (obj instanceof Recurso) {
								NodeTreeClickListener l = new NodeTreeClickListener(
										gui, true);
								l.updateRecurso(selectedNode);
							}
							ClientUtils.alert("Informaci\u00f3n",
									"Los trabajos han sido cerrados.",
									ClientUtils.INFO);
						}
					});
				}
			}
		});
		Button cancel = new Button("Cancelar");
		cancel.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				wdmultiJobsClose.close();
			}
		});
		wdmultiJobsClose.addButton(jobsClos);

		wdmultiJobsClose.addButton(cancel);

		gui.getQueriesServiceProxy().multiCloseJob(
				MultiCloseWorkItemListener.this.rec.getId(),
				MultiCloseWorkItemListener.this.rec.getCont().getId(),
				MultiCloseWorkItemListener.this.gui.getDfDateProg().getEl()
				.getValue(),
				new RPCFillCheckboxGridPanel(grid, new Function() {

					@Override
					public void execute() {
						try {
							grid.checkAll();
						} catch (Exception e) {
						}

						wdmultiJobsClose.add(grid);
						wdmultiJobsClose.show();

						if (grid.isRendered())
							grid.getEl().unmask();
					}

				}));
	}
}