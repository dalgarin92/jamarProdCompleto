package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.SearchWindowHandler;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class SearchDirButtonListener extends BaseItemListenerAdapter {

//	private MyGMap2Widget map;
	private Window wnd;
	private ControlWebGUI gui;
	private TextField tfDir;
	private ComboBox cbMunip;
	private FieldSet dirSet;
	private Button btnOK;

	public SearchDirButtonListener(ControlWebGUI gui) {
		//this.map = map;
		this.gui = gui;
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		wnd = new Window("Buscar Direcci\u00f3n", true, false);
		wnd.setIconCls("icon-butterfly");
		wnd.setLayout(new FormLayout());
		dirSet = new FieldSet();
		dirSet.setBorder(false);
		dirSet.setLayout(new FormLayout());
		dirSet.setPaddings(5, 5, 5, 0);
		
		tfDir = new TextField("Direcci\u00f3n", "dir");
		dirSet.add(tfDir, new AnchorLayoutData("100%"));
		
		cbMunip = new ComboBox("Municipio");
		
		btnOK = new Button("Buscar");
		wnd.setButtonAlign(Position.CENTER);
		wnd.addButton(btnOK);
		Button btnCancel = new Button("Cancelar", new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				wnd.close();
			}
		});
		wnd.addButton(btnCancel);
		
		wnd.setHeight(155);
		wnd.setWidth(390);
		wnd.setPaddings(0, 0, 0, 0);
		wnd.setClosable(true);
		
		Image image = new Image("images/utiles/search_48.png");
		Panel imagePanel = new Panel();
		imagePanel.setPaddings(5, 10, 0, 0);
		imagePanel.setBorder(false);
		imagePanel.add(image);
		MultiFieldPanel mfpSearch = new MultiFieldPanel();
		mfpSearch.setPaddings(15, 0, 0, 0);
		mfpSearch.addToRow(imagePanel, 68);
		mfpSearch.addToRow(dirSet, new ColumnLayoutData(1));	
		wnd.add(mfpSearch);		
		
		/*Proceso proc = (Proceso) gui.getTree().getRootNode()
				.getAttributeAsObject("entity");*/
		
		QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
				.create(QueriesService.class);
		serviceProxy.getMunicipios(new RPCFillComboBoxHandler(cbMunip, dirSet, 1));
	
		btnOK.addListener(new SearchWindowHandler(wnd, gui, tfDir, cbMunip));
		wnd.show();	
	
	}
}
