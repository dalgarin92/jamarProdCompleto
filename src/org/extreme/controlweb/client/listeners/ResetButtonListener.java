package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class ResetButtonListener extends BaseItemListenerAdapter implements
		AsyncCallback<Boolean> {

	TreePanel panel;
	TreeNode selectedNode;
	private ControlWebGUI gui;

	public ResetButtonListener(ControlWebGUI gui) {
		this.panel = gui.getTree();
		this.gui = gui;
	}

	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		TreeNode selectedNode = gui.getMap().getSelectedTrabajoNode();
		if (selectedNode != null) {
			this.selectedNode = selectedNode;
			if (Window.confirm("Realmente desea reniciar el trabajo "
					+ selectedNode.getText())) {
				Trabajo trab = (Trabajo) selectedNode.getAttributeAsObject("entity");
				String trabajo = trab.getId();
				String tipotrabajo = trab.getTpoTrab().getId();

				QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
						.create(QueriesService.class);			
				serviceProxy.resetJob(trabajo, tipotrabajo, gui.getUsuario().getId(), this);
			}
		}

	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}
	
	public void onSuccess(Boolean result) {
		Trabajo trab = (Trabajo) selectedNode.getAttributeAsObject("entity");
		selectedNode.setIconCls(trab.getIcono());
		trab.resetEstado();
		ClientUtils.alert("Informaci\u00f3n", "El trabajo ha sido reiniciado.",
				ClientUtils.INFO);
	}
}
