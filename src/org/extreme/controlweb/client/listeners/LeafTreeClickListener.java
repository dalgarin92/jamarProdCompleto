package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.data.Node;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.event.TreeNodeListenerAdapter;

public class LeafTreeClickListener extends TreeNodeListenerAdapter{

	private ControlWebGUI gui;

	public LeafTreeClickListener(ControlWebGUI gui) {
		this.gui = gui;
	}
	
	
	
	public void onClick(Node node, EventObject e) {
		GWT.log("Entrando al evento Click", null);
		Marker marker = ((Marker)((TreeNode)node).getAttributeAsObject("marker"));
		if (marker != null) {
			/*if(marker.isVisible())
				gui.getMap().removeOverlay(marker);*/

			//gui.getMap().getGmap().addOverlay(marker);
			
			gui.getMap().getMap().setCenter(marker.getLonLat());
			gui.getMap().getMap().zoomTo(16);
			gui.getMap().setPopup(marker, ((Trabajo) node
					.getAttributeAsObject("entity")).getHTML());
		}
	
	}
	
	public void onDblClick(Node node, EventObject e) {
		GWT.log("LeafTreeClickListener DblClick", null);
		new MoreInfoItemListener(gui, (TreeNode) node).onClick(null, e);
	}
}
