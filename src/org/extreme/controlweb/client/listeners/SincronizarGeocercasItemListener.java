package org.extreme.controlweb.client.listeners;


import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.services.AdminQueriesService;
import org.extreme.controlweb.client.services.AdminQueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class SincronizarGeocercasItemListener extends BaseItemListenerAdapter {
	
	TabPanel tabPanel;
	private  AdminQueriesServiceAsync adminServiceProxy = null;
	
	public SincronizarGeocercasItemListener(ControlWebGUI gui, TabPanel tabPanel
	) {
		this.tabPanel = tabPanel;
		
		
		 
	}
	
	private  AdminQueriesServiceAsync getStaticAdminQueriesServiceAsync() {
		if (adminServiceProxy == null) {
			adminServiceProxy = (AdminQueriesServiceAsync) GWT
					.create(AdminQueriesService.class);
		}
		return adminServiceProxy;

	}
	
	public void onClick(BaseItem item, EventObject e) {

		getStaticAdminQueriesServiceAsync().ActDatosGeocercas(new MyBooleanAsyncCallback("Error en la Sincronizacion de Geocercas","Geocercas Sincronizadas Exitosamente",null));
		
	}
	

}
