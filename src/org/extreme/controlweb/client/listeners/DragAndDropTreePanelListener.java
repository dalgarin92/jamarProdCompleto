package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Contratista;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.dd.DragData;
import com.gwtext.client.dd.DragDrop;
import com.gwtext.client.widgets.tree.DropNodeCallback;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;
import com.gwtext.client.widgets.tree.event.TreePanelListenerAdapter;

public class DragAndDropTreePanelListener extends TreePanelListenerAdapter
		implements AsyncCallback<Boolean> {

	private TreeNode target;
	private TreePanel treePanel;
	private TreeNode oldParent;
	private ControlWebGUI gui;	
	
	public DragAndDropTreePanelListener(ControlWebGUI gui) {
		this.gui = gui;
	}
	
	@Override
	public boolean doBeforeNodeDrop(TreePanel treePanel, TreeNode target,
			DragData dragData, String point, DragDrop source,
			TreeNode dropNode, DropNodeCallback dropNodeCallback) {
		Trabajo dropTrab = (Trabajo) dropNode.getAttributeAsObject("entity");
		Recurso recTarget = (Recurso) target.getAttributeAsObject("entity");
		
		this.oldParent = (TreeNode)dropNode.getParentNode(); 
		this.treePanel = treePanel;
		this.target = target;
		String est = dropTrab.getEstado();
		ArrayList<Contratista> arrayCont = dropTrab.getPosiblesCont();
		GWT.log(est, null);
		boolean res;
		if (arrayCont.contains(recTarget.getCont())) {
			if(est.equals("00") /*|| est.equals("10")*/ || est.equals("") || est.compareTo("40") > 0) {
				res = Window.confirm("En realidad desea asignar el trabajo "+dropNode.getText()+
						" al recurso "+target.getText().split(" -")[0]+"?");
			}
			else {
				ClientUtils.alert("Error", "Este trabajo no puede ser reasignado.", ClientUtils.ERROR);
				//Window.alert("Este trabajo no puede ser reasignado.");
				return false;
			}
		}
		else {
			ClientUtils.alert("Error", "No es posible asignar trabajo a este contratista.", ClientUtils.ERROR);
			return false;
		}
		return res;
	}
	
	@Override
	public void onNodeDrop(TreePanel treePanel, TreeNode target,
			DragData dragData, String point, DragDrop source, TreeNode dropNode) {
		treePanel.getEl().mask("Asignando...");
		
		Trabajo dropTrab = (Trabajo) dropNode.getAttributeAsObject("entity");
		Recurso recTarget = (Recurso) target.getAttributeAsObject("entity");
				
		String trabajo = dropTrab.getId();
		String tipoTrabajo = dropTrab.getTpoTrab().getId();
		String recurso = recTarget.getId();
		String contratista = recTarget.getCont().getId();
		
		QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
				.create(QueriesService.class);
		serviceProxy.assignJob(trabajo, tipoTrabajo, recurso, contratista, gui
				.getUsuario().getId(), this);
	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}
	
	public void onSuccess(Boolean result) {
		if (treePanel.getEl().isMasked())
			treePanel.getEl().unmask();
		oldParent.setText(oldParent.getText().split("-")[0]	+ " - "
						+ (new Integer(oldParent.getText().split("- ")[1]).intValue() - 1));
		target.setText(target.getText().split("-")[0] + " - "
						+ (new Integer(target.getText().split("- ")[1]).intValue() + 1));
		target.select();
	}
}
