package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.core.PuntoInteres;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;

public class FavButtonListener extends ButtonListenerAdapter {

	private ControlWebGUI gui;
	private String tipo;
	private QueriesServiceAsync serviceProxy;
	private ArrayList<Marker> markers;

	public FavButtonListener(ControlWebGUI gui, String tipo) {
		this.gui = gui;
		this.tipo = tipo;
		this.markers = new ArrayList<Marker>();
		serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
	}
	
	@Override
	public void onToggle(Button button, boolean pressed) {
		if (pressed) {
			String t = null;
			
			if(markers != null) {
				if(tipo.equals("user"))
					gui.getMap().getMkPtosUsuario().removeMarkers(markers);
				else if(tipo.equals("proc"))
					gui.getMap().getMkPtosGlobal().removeMarkers(markers);
				markers.clear();
			} else		
				this.markers = new ArrayList<Marker>();
			
			if (this.tipo.equals("user"))
				t = "de Usuario...";
			else if (this.tipo.equals("proc"))
				t = " Globales...";
			if (t.equals("proc") && gui.getProceso() == null)
				ClientUtils.alert("Error", "Seleccione primero un proceso.",
						ClientUtils.ERROR);
			else {

				AsyncCallback<ArrayList<LocalizableObject>> cb = new AsyncCallback<ArrayList<LocalizableObject>>() {
					public void onFailure(Throwable caught) {
						ClientUtils.alert("Error", caught.getMessage(),
								ClientUtils.ERROR);
						gui.hideMessage();
						GWT.log("Error RPC", caught);
					}

					public void onSuccess(ArrayList<LocalizableObject> result) {
						gui.hideMessage();
						
						for (LocalizableObject lo : result){
							if(tipo.equals("user"))
								markers.add(/*m =*/ gui.getMap().createPtoUsuarioMarker(
										lo.getLatitudAsDouble().doubleValue(),
										lo.getLongitudAsDouble().doubleValue(), lo.getIcono(),
										((PuntoInteres) lo).getHTML()));
							else if(tipo.equals("proc"))
								markers.add(/*m =*/ gui.getMap().createPtoGlobalMarker(
										lo.getLatitudAsDouble().doubleValue(),
										lo.getLongitudAsDouble().doubleValue(), lo.getIcono(),
										((PuntoInteres) lo).getHTML()));
						}
						if (tipo.equals("proc"))
							gui.setProcMarkers(markers);
						else if (tipo.equals("user"))	
							gui.setUserMarkers(markers);
					}
				};
				gui.hideMessage();
				gui.showMessage("Por favor espere...",
						"Cargando Puntos de Inter\u00e9s " + t,
						Position.CENTER);
				if (this.tipo.equals("proc"))
					serviceProxy.getPtoIntProceso(gui.getProceso().getId(), cb);
				else if (this.tipo.equals("user"))
					serviceProxy.getPtoIntUsuario(
							gui.getUsuario() == null ? null : gui.getUsuario()
									.getId(), cb);
			}
		} else {
			gui.getMap().removeClearHandler();
			if (this.tipo.equals("proc") && gui.getProcMarkers() != null) {
				gui.getMap().getMkPtosGlobal().removeAll();
				gui.getProcMarkers().clear();
				
			} else if (this.tipo.equals("user") && gui.getUserMarkers() != null) {
				gui.getMap().getMkPtosUsuario().removeAll();
				gui.getUserMarkers().clear();
			}
			//gui.getMap().addClearHandler();
		}
	}
}
