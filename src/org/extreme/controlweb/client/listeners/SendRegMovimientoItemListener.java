package org.extreme.controlweb.client.listeners;


import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;





import org.extreme.controlweb.client.util.ClientUtils;


import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RadioButton;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;


import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;


import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.TimeField;

import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class SendRegMovimientoItemListener  extends BaseItemListenerAdapter {
	
	private TreeNode treeNode;
	private ControlWebGUI gui;
	
   public String fechamov="";
	private TimeField hora;
	private TextField fechaAct;
	private TextField vehSel;
	private TextField movimiento;
	private Window wnd;
	private String recurso;
	private MyComboBox cmbHoras;
	public FieldSet FsHoraEventos;
	

	public SendRegMovimientoItemListener(ControlWebGUI gui, TreeNode treeNode
			) {
		this.gui = gui;
		this.treeNode = treeNode;
		
		
	}
	
	public void onClick(BaseItem item, EventObject e) {
		Recurso rec = (Recurso) treeNode.getAttributeAsObject("entity");
		recurso = rec.getId();
		String nombre=rec.getNombre();
		System.out.println("Recurso: "+recurso);
		System.out.println("nombre: "+nombre);
		java.util.Date fecha=new java.util.Date();
		String fecha2="";
		fecha2=DateTimeFormat.getFormat("yyyy-MM-dd").format(fecha);
		System.out.println("fecha: "+fecha2);
		
		
			wnd = new Window("Registrar Movimiento", true, false);
			wnd.setLayout(new FormLayout());
			wnd.setPaddings(5);
			wnd.setHeight(170);
			wnd.setWidth(300);
			wnd.setButtonAlign(Position.CENTER);
			fechaAct = new TextField("Fecha");
			fechaAct.setValue(fecha2);
			fechaAct.setDisabled(true);
			vehSel = new TextField("Vehiculo");
			vehSel.setValue(recurso);
			vehSel.setDisabled(true);
			movimiento= new TextField("Movimiento");
			//final RadioButton segEventos= new RadioButton("eventos","Segun Eventos.. ");
			final RadioButton manual= new RadioButton("eventos","Manual.. ");
			hora = new TimeField("Hora");
			hora.setFormat("H:i");
			hora.setIncrement(15);
			hora.setWidth(80);
			cmbHoras= new MyComboBox("Hora ");
			cmbHoras.setVisible(true);
			FsHoraEventos=new FieldSet();
			FsHoraEventos.setBorder(false);
			FsHoraEventos.setLayout(new FormLayout());
		
			final Function obtenerCampo = new Function() {
				public void execute() {
					gui.getQueriesServiceProxy().obtenerCampo(fechaAct.getValueAsString(),
							vehSel.getValueAsString(),
							 new AsyncCallback<String>() {
						@Override
						public void onSuccess(String result) {
							if(!result.equalsIgnoreCase("vacio")){
							movimiento.setValue(result);
							}else{
								if(result.equalsIgnoreCase("vacio")){
								movimiento.setValue("Ninguno");
								}
							}
							
							/*if(!movimiento.getValueAsString().equalsIgnoreCase("Ninguno")){
							gui.getStaticQueriesServiceAsync().getHorasEventos(
									fechaAct.getValueAsString(), movimiento.getValueAsString(), vehSel.getValueAsString(),
									new RPCFillComplexComboHandler(cmbHoras, null, 1));
							}*/
						}
						
						@Override
						public void onFailure(Throwable caught) {
						
									ClientUtils.alert("Error",
											"Error al cargar Registro de movimiento "
													+ caught.getMessage(),
											ClientUtils.ERROR);
									GWT.log("Error al cargar registro de movimiento ",
													caught);
									wnd.close();
						}
					});
					
				}
			};
			
			obtenerCampo.execute();
			
			/*gui.getStaticQueriesServiceAsync().getHorasEventos(
					fechaAct.getValueAsString(), movimiento.getValueAsString(), vehSel.getValueAsString(),
					new RPCFillComplexComboHandler(cmbHoras,
							FsHoraEventos, 1));*/
			
			
		
			
			
			FsHoraEventos.add(cmbHoras);
			final Button btnEnviar = new Button("Enviar", new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
					
					if(manual.getValue()==true){
						fechamov=fechaAct.getValueAsString()+" "+hora.getValueAsString();
						}/*else{
							if(segEventos.getValue()==true){
								fechamov=fechaAct.getValueAsString()+" "+cmbHoras.getValueAsString();
							}
						}*/
					
					
				
					gui.getQueriesServiceProxy().regMovi(fechaAct.getValueAsString(),
							vehSel.getValueAsString(),
							fechamov, new AsyncCallback<String>() {
						@Override
						public void onSuccess(String result) {
							if(result.equalsIgnoreCase("OK")){
								ClientUtils.alert("Info.",
										"Registro de movimiento guardado exitosamente"
												,
										ClientUtils.INFO);
								wnd.close();
							}else{
								if(result.equalsIgnoreCase("vacio")){
								ClientUtils.alert("Info",
										"No se realizo ningun cambio para este registro"
												,
										ClientUtils.INFO);
								wnd.close();
								}else{
									if(result.equalsIgnoreCase("fecha mal")){
										ClientUtils.alert("Info",
												"Fecha ingresada menor que los registros de movimientos anteriores. Por favor vuelva a intentarlo"
														,
												ClientUtils.INFO);
										
									}
								}
							}
						}
						
						@Override
						public void onFailure(Throwable caught) {
						
									ClientUtils.alert("Error",
											"Error al enviar Registro de movimiento "
													+ caught.getMessage(),
											ClientUtils.ERROR);
									GWT.log("Error al enviar registro de movimiento ",
													caught);
									wnd.close();
						}
					});
					
					
				}
				
			});
			btnEnviar.setDisabled(true);
		
			Button btnOk =  new Button("OK", new ButtonListenerAdapter(){
				public void onClick(Button button, EventObject e) {
					btnEnviar.setDisabled(false);
					if(manual.getValue()==true){
						
					  // segEventos.setVisible(false);
						hora.setVisible(true);
						wnd.add(hora);
						wnd.setHeight(195);                   
					}/*else{
						if(segEventos.getValue()==true){
							manual.setVisible(false);
							cmbHoras.setVisible(true);
							wnd.add(cmbHoras);
							wnd.setHeight(205);
						}
					}*/
				}
			});
			
			Button btnCancel = new Button("Cancelar", new ButtonListenerAdapter(){
				public void onClick(Button button, EventObject e) {
					wnd.close();
				}
			});
			
			
			movimiento.setDisabled(true);
			wnd.add(fechaAct, new AnchorLayoutData("100%"));
			wnd.add(vehSel, new AnchorLayoutData("100%"));
			wnd.add(movimiento, new AnchorLayoutData("100%"));
		//	wnd.add(segEventos);
			wnd.add(manual);
			wnd.addButton(btnOk);
			wnd.addButton(btnEnviar);
			wnd.addButton(btnCancel);
			wnd.show();
	}
}
