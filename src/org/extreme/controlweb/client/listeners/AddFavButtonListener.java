package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.TabPanelListenerAdapter;
import com.gwtext.client.widgets.event.WindowListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.Radio;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.form.event.TextFieldListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayout;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayout;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;

public class AddFavButtonListener extends ButtonListenerAdapter {

	private ControlWebGUI gui;
	private String details;
	private boolean modify;
	private boolean delete;
	private boolean numerate;
	private int[] columnSizes;
	private boolean[] visible;
	
	private Window wnd;
	private TextField tfName;
	private TextField nfLat;
	private TextField nfLon;
	private Button buttonUbic;
	private TextField tfDireccion;
	private ComboBox cmbMunicipio;
	private Button btUbicar2;
	private ComboBox cmbIcon;
	private Image imgIcono;
	private VectorFeature mkPtoInteres;
	private Radio rbTipoUsuario;
	private Radio rbTipoProceso;
	private boolean drag = false;
	private MyGridPanel myGrid;
	private RPCFillTable fh;
	private TabPanel tp;
	
	private CompleteDragFeatureListener markerListener;
	
	Function recargarPuntosInteres = new Function() {
		public void execute() {
			serviceProxy.getPtoInt(gui.getUsuario() == null ? null : gui
					.getUsuario().getId(), gui.getProceso() == null ? null
					: gui.getProceso().getId(), fh);
			gui.refreshPtoInts();
		}
	};

	private boolean iconified;
	private MapClickListener listener;
	private static final String MESSAGE_MUNIP = "Seleccione municipio";

	protected static QueriesServiceAsync serviceProxy = null;

	private static QueriesServiceAsync getStaticQueriesServiceAsync() {
		if (serviceProxy == null) {
			serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
		}
		return serviceProxy;
	}

	private void calculateLatLng() {
		mkPtoInteres.getCenterLonLat().transform("EPSG:900913","EPSG:4326");
		nfLat.setValue(String.valueOf(mkPtoInteres.getCenterLonLat().lat()));
		nfLon.setValue(String.valueOf(mkPtoInteres.getCenterLonLat().lon()));
	}

	public AddFavButtonListener(ControlWebGUI gui) {
		this.gui = gui;
		iconified = false;
		details = null;
		modify = false;
		delete = true;
		numerate = true;
		columnSizes = new int[] { 80, 80, 80, 80, 80, 80, 80, 80 };
		visible = new boolean[] { false, true, false, false, true, true, true,
				false };

		serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);
		
		this.listener = new MapClickListener() {
			@Override
			public void onClick(MapClickEvent mapClickEvent) {
				buttonUbic.toggle();
				removePtoInteres();				
			
				LonLat ll = new LonLat(mapClickEvent.getLonLat().lon(), mapClickEvent.getLonLat().lat());
				ll.transform("EPSG:900913","EPSG:4326");
				
				LocalizableObject lo = new LocalizableObject(String
						.valueOf(mapClickEvent.getLonLat().lat()), String
						.valueOf(mapClickEvent.getLonLat().lon()));
				
				drag = true;
				relocate(lo, drag, false);
				calculateLatLng();
			}		
		};

		this.markerListener = new CompleteDragFeatureListener() {			
			@Override
			public void onComplete(VectorFeature vf, Pixel p) {
				calculateLatLng();
			}
		};
	}

	private void removePtoInteres() {
		if (mkPtoInteres != null) {
			gui.getMap().getVecAuxDraggable().removeFeature(mkPtoInteres);
			mkPtoInteres = null;
		}
	}

	private void relocate(LocalizableObject result, boolean drag, boolean center) {
		removePtoInteres();
		mkPtoInteres = gui.getMap().createDraggableMarker(
				result.getLatitudAsDouble(),
				result.getLongitudAsDouble(),
				"images/puntos_int/"
						+ cmbIcon.getStore().query("id", cmbIcon.getValue())[0]
								.getAsString("icono") + ".png");
		if (center) {
			gui.getMap().getMap().setCenter(mkPtoInteres.getCenterLonLat());
			//gui.getMap().getGmap().getInfoWindow().open(mkPtoInteres.getLatLng
			// (), new InfoWindowContent("<b>Punto de interes:</b>"+tfName.
			// getValueAsString()+"<br/>"));
		}
	}	
		
	public void repaintPtoInteres(boolean center) {
		if (mkPtoInteres != null) {
			mkPtoInteres.getCenterLonLat().transform("EPSG:4326","EPSG:900913");
			relocate(new LocalizableObject(String.valueOf(mkPtoInteres
					.getCenterLonLat().lat()), String.valueOf(mkPtoInteres
					.getCenterLonLat().lon())), drag, center);
			mkPtoInteres.getCenterLonLat().transform("EPSG:900913","EPSG:4326");
		}
	}

	@Override
	public void onClick(Button button, EventObject e) {

		wnd = new Window("Administrar Puntos de Interes", false, true);

		wnd.setHeight(600);
		wnd.setWidth(500);
		wnd.setLayout(new FitLayout());
		wnd.setPaddings(5);

		Panel mainPanel = new Panel();
		mainPanel.setBorder(false);
		mainPanel.setPaddings(5);
		mainPanel.setLayout(new AnchorLayout());

		FieldSet data = new FieldSet();
		data.setBorder(false);
		data.setPaddings(5);
		data.setLabelWidth(40);

		FieldSet fs = new FieldSet("Datos");
		fs.setButtonAlign(Position.CENTER);

		tfName = new TextField("Nombre");
		data.add(tfName, new AnchorLayoutData("95%"));
		tfName.addListener(new TextFieldListenerAdapter() {
			@Override
			public void onBlur(Field field) {
				repaintPtoInteres(true);
			}
		});

		MultiFieldPanel pSuperData = new MultiFieldPanel();
		pSuperData.setLayout(new ColumnLayout());
		pSuperData.addToRow(data, 250);
		final FieldSet pIcono = new FieldSet("Imagen Seleccionada");
		pSuperData.addToRow(pIcono, new ColumnLayoutData(1));
		// pIcono.setAutoHeight(true);
		// pIcono.setBorder(false);
		pIcono.setPaddings(0, 60, 0, 0);

		rbTipoUsuario = new Radio("Punto de Usuario");
		rbTipoUsuario.setChecked(true);
		rbTipoProceso = new Radio("Punto Global");

		rbTipoProceso.setName("tipo");
		rbTipoUsuario.setName("tipo");

		cmbIcon = new ComboBox("Imagen");
		cmbIcon.setEmptyText("Seleccione imagen");
		cmbIcon.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				imgIcono.setUrl("images/puntos_int/"
						+ record.getAsString("icono") + ".png");
				repaintPtoInteres(true);
				pIcono.setVisible(true);
				iconified = true;
			}

		});

		imgIcono = new Image("images/puntos_int/pal2/icon15.png");

		pIcono.add(imgIcono);
		data.add(rbTipoUsuario);

		rbTipoProceso.setDisabled(gui.getProceso() == null);

		data.add(rbTipoProceso);
		// data.add(imgIcono);
		// serviceProxy.getIconos(new RPCFillComboBoxHandler(cmbIcon,data,1));
		getStaticQueriesServiceAsync().getIconos(
				new RPCFillComboBoxHandler(cmbIcon, data, 1));

		// data.add(imgIcono);
		mainPanel.setLayout(new RowLayout());
		// mainPanel.add(data, new RowLayoutData("20%"));
		mainPanel.add(pSuperData, new RowLayoutData("25%"));
		tp = new TabPanel();

		tp.addListener(new TabPanelListenerAdapter() {

			@Override
			public boolean doBeforeTabChange(TabPanel source, Panel newPanel,
					Panel oldPanel) {
				boolean doAsk = !nfLat.getValueAsString().equals("")
						|| !tfDireccion.getValueAsString().equals("");
				boolean sw = true;
				if (doAsk)
					if (sw = com.google.gwt.user.client.Window
							.confirm("Se perder\u00e1n los datos que no se hayan guardado. Desea continuar?")) {
						removePtoInteres();
						nfLat.setValue("");
						nfLon.setValue("");
						tfDireccion.setValue("");
						cmbMunicipio.setValue("");
					}
				return sw;
			}
		});

		Panel tab1 = new Panel("Geograficamente");
		tab1.setPaddings(5);
		// tab1.setLayout(new AnchorLayout());

		nfLat = new TextField("Latitud");
		nfLat.setReadOnly(true);
		fs.add(nfLat, new AnchorLayoutData("75%"));

		nfLon = new TextField("Longitud");
		nfLon.setReadOnly(true);
		fs.add(nfLon, new AnchorLayoutData("75%"));
		buttonUbic = new Button("Ubicar");
		buttonUbic.setEnableToggle(true);
		buttonUbic.setToggleGroup("ubicar");
		buttonUbic.addListener(new ButtonListenerAdapter() {
			@Override
			public void onToggle(Button button, boolean pressed) {
				if (iconified) {
					if (pressed) {
						gui.getMap().setCrosshairCursor();
						gui.getMap().getMap().addMapClickListener(listener);
					} else {
						gui.getMap().setDefaultCursor();
						if (listener != null)
							gui.getMap().getMap().removeListener(listener);
					}
				} else {
					button.setPressed(false);
					ClientUtils.alert("Info",
							"Debe seleccionar primero un icono",
							ClientUtils.INFO);
				}
			}
		});
		fs.addButton(buttonUbic);

		tab1.add(fs);
		tp.add(tab1);

		FieldSet fs2 = new FieldSet("Datos");
		fs2.setButtonAlign(Position.CENTER);

		Panel tab2 = new Panel("Por Direcci\u00f3n");
		tab2.setPaddings(5);
		tfDireccion = new TextField("Direcci\u00f3n");
		fs2.add(tfDireccion, new AnchorLayoutData("75%"));
		cmbMunicipio = new ComboBox("Municipio");
		cmbMunicipio.setEmptyText(MESSAGE_MUNIP);
		getStaticQueriesServiceAsync().getMunicipios(
				new RPCFillComboBoxHandler(cmbMunicipio, fs2, 1));
		btUbicar2 = new Button("Ubicar");
		btUbicar2.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {

				if (iconified) {
					serviceProxy.getDireccion(tfDireccion.getValueAsString(),
							cmbMunicipio.getValue(),
							new AsyncCallback<LocalizableObject>() {
								public void onFailure(Throwable caught) {
									ClientUtils.alert("Error", caught
											.getMessage(), ClientUtils.ERROR);
									GWT.log("Error RPC", caught);
									removePtoInteres();
								}

								public void onSuccess(LocalizableObject result) {
									drag = false;
															
									LonLat ll = new LonLat(result.getLongitudAsDouble(), result.getLatitudAsDouble());
									ll.transform("EPSG:4326","EPSG:900913");
									
									LocalizableObject lo = new LocalizableObject(String
											.valueOf(ll.lat()), String
											.valueOf(ll.lon()));
									
									relocate(lo, drag, true);
									
									calculateLatLng();
								}
							});
				} else {
					ClientUtils.alert("Info",
							"Debe seleccionar primero un icono",
							ClientUtils.INFO);
				}
			}
		});
		fs2.addButton(btUbicar2);
		tab2.add(fs2);
		tp.add(tab2);
		mainPanel.add(tp, new RowLayoutData("35%"));
		Button bnNewPtoInt = new Button("Guardar");
		Button bnCancelar = new Button("Cancelar");
		bnCancelar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				wnd.close();
			}
		});

		myGrid = new MyGridPanel();

		fh = new RPCFillTable(myGrid, mainPanel, details, modify, delete,
				numerate, columnSizes, visible, new RowLayoutData("40%"));

		getStaticQueriesServiceAsync().getPtoInt(
				gui.getUsuario() == null ? null : gui.getUsuario().getId(),
				gui.getProceso() == null ? null : gui.getProceso().getId(), fh);

		bnNewPtoInt.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				String id = null;
				String tipo = null;

				AsyncCallback<Boolean> cb2 = new MyBooleanAsyncCallback(
						"Creaci\u00f3n exitosa...", recargarPuntosInteres);

				if (rbTipoProceso.getValue()) {
					id = gui.getProceso().getId();
					tipo = ClientUtils.PROCESS_POI;
				} else {
					id = gui.getUsuario().getId();
					tipo = ClientUtils.USER_POI;
				}

				gui.showMessage("Por favor espere...",
						"Creando el punto de inter\u00e9s...", Position.CENTER);

				if (mkPtoInteres != null) {
					String direccion = tfDireccion.getValueAsString();
					String municipio = cmbMunicipio.getEl() != null ? cmbMunicipio
							.getEl().getValue()
							: null;

					if (municipio != null && municipio.equals(MESSAGE_MUNIP))
						municipio = null;
									
					getStaticQueriesServiceAsync()
							.createPtoInt(
									id,
									tipo,
									String.valueOf(mkPtoInteres
											.getCenterLonLat().lat()),
									String.valueOf(mkPtoInteres
											.getCenterLonLat().lon()),
									tfName.getValueAsString(), direccion,
									municipio, null, cmbIcon.getValue(), cb2);
					
					nfLat.setValue("");
					nfLon.setValue("");

					mkPtoInteres = null;
					
				} else {
					ClientUtils.alert("Error",
							"Primero debe ubicar el punto de inter\u00e9s",
							ClientUtils.ERROR);
				}
			}
		});
		wnd.setButtonAlign(Position.CENTER);
		wnd.addButton(bnNewPtoInt);
		wnd.add(mainPanel, new RowLayoutData());
		wnd.addListener(new WindowListenerAdapter() {
			@Override
			public void onClose(Panel panel) {
				removePtoInteres();
				gui.getMap().deactivateDragControl();
			}
		});
		myGrid.addDeleteListener(new MyGridOnCellClickListener() {
			public void onCellClick(Record r) {
				/*if (com.google.gwt.user.client.Window
						.confirm("Realmente desea eliminar este punto de inter\u00e9s?"))*/
					getStaticQueriesServiceAsync().deletePtoInt(
							r.getAsString("id"),
							new MyBooleanAsyncCallback(
									"Punto de Inter\u00e9s eliminado.",
									recargarPuntosInteres));

			}
		});	
		pIcono.setVisible(false);
		wnd.show();
		gui.getMap().setCompleteDragListener(markerListener);
		gui.getMap().activateDragControl();
	}
}
