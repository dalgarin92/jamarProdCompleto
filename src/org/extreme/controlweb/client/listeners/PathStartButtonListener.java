package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class PathStartButtonListener extends BaseItemListenerAdapter implements
		AsyncCallback<Boolean> {

	private TreePanel panel;
	private TreeNode recTreeNode;
	private ControlWebGUI gui;

	public PathStartButtonListener(ControlWebGUI gui) {
		this.gui = gui;
		this.panel = gui.getTree();
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		TreeNode selectedNode = gui.getMap().getSelectedTrabajoNode();
		recTreeNode = (TreeNode)selectedNode.getParentNode();
		Object recObj = recTreeNode.getAttributeAsObject("entity");
		Trabajo trab = (Trabajo) selectedNode.getAttributeAsObject("entity");

		if (selectedNode != null && recTreeNode != null) {
			if (recObj instanceof Recurso) {  // esta asignado a un recurso
				Recurso rec = (Recurso) recObj;

				if (Window.confirm("Realmente desea "
						+ (!trab.isInicioRuta() ? "fijar" : "quitar")
						+ " este trabajo como inicio de ruta?")) {
					String trabajo = trab.getId();
					String tipotrabajo = trab.getTpoTrab().getId();
					String recurso = rec.getId();
					String cont = rec.getCont().getId();

					QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
							.create(QueriesService.class);
					serviceProxy.setPathStart(trabajo, tipotrabajo, recurso, cont, trab.isInicioRuta(), this);
				}
			} else
				ClientUtils.alert("Error", "Antes de fijar este trabajo como inicio de ruta, <br/> debe asign\u00e1rselo a alg\u00fan recurso.", ClientUtils.ERROR);
		}
	}
	
	public void onFailure(Throwable caught) {
		if (panel.getEl().isMasked())
			panel.getEl().unmask();
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}
	
	public void onSuccess(Boolean result) {
		ClientUtils.alert("Informaci\u00f3n", "Inicio de ruta cambiado.", ClientUtils.INFO);
		NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
		l.updateRecurso(recTreeNode);
	}
}
