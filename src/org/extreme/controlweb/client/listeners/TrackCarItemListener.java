package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.DateWindow;
import org.extreme.controlweb.client.handlers.TrackCarHandler;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class TrackCarItemListener extends BaseItemListenerAdapter { // Se utiliza para mostrar en el mapa el recorrido
																	// historico de un vehiculo

	private TreeNode treeNode;
	private ControlWebGUI gui;
	private DateWindow wnd;

	public TrackCarItemListener(ControlWebGUI gui, TreeNode treeNode) {
		this.gui = gui;
		this.treeNode = treeNode;
	}

	public void onClick(BaseItem item, EventObject e) {
				
		wnd = new DateWindow("Recorrido Hist\u00f3rico", true, false);
		wnd.addOKListener(new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				gui.showMessage("Por favor espere...", "Generando recorrido...", Position.CENTER);
				Recurso rec = (Recurso) treeNode.getAttributeAsObject("entity");
				String recurso = rec.getId();
				String contratista = rec.getCont().getId();

				if (!wnd.getFromDate().equals("") && !wnd.getToDate().equals("")) {
					if (wnd.getFromDateAsDate().before(wnd.getToDateAsDate())
							|| (wnd.getFromDateAsDate().equals(wnd.getToDateAsDate()) 
									&& wnd.getFromTime().compareTo(wnd.getToTime()) < 0)) {

						gui.getQueriesServiceProxy().getInfoEventos(
								recurso, contratista, wnd.getFromDate(),
								wnd.getToDate(), new ArrayList<String>(), new TrackCarHandler(gui, recurso,contratista,
										wnd.getFromDate(),wnd.getToDate()));
						
					} else
						ClientUtils.alert("Error", "Rango de fechas inv\u00e1lido.",
								ClientUtils.ERROR);
				} else
					ClientUtils.alert("Error", "Todas las opciones son requeridas.",
							ClientUtils.ERROR);
				wnd.close();
			}			
		});
		wnd.show();
	}	
}
