package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.FormPanel;
import com.gwtext.client.widgets.form.Radio;
import com.gwtext.client.widgets.form.event.CheckboxListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class PreCloseWorkItemListener extends BaseItemListenerAdapter implements AsyncCallback<Boolean> {

	private ControlWebGUI gui;
	private TreeNode selectedNode;
	private Trabajo trab;
	private Window wdPreCloseJobs;
	private Radio rbAccessory, rbWhim, rbContactNotFound, rbStructureOrDifference, rbMistreatment, rbPenniless,
			rbQuality, rbCustomer, rbTransport, rbSellers;

	private Radio chUnrealized, chRealized;
	private Checkbox chsatisfaction;

	public PreCloseWorkItemListener(ControlWebGUI gui, TreeNode treeNode, Trabajo trab) {
		this.gui = gui;
		this.selectedNode = treeNode;
		this.trab = trab;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {

		wdPreCloseJobs = new Window("Pre-Cerrar: " + trab.getNumViaje(), 300, 480, false, true);
		wdPreCloseJobs.setLayout(new FitLayout());

		FormPanel formPanel = new FormPanel();
		formPanel.setLabelWidth(30);
		formPanel.setPaddings(5);
		formPanel.setBorder(false);
		formPanel.setHeight(100);

		chRealized = new Radio("El trabajo fue realizado", "typeJob");
	   chsatisfaction = new Checkbox("Recibo a satisfaccion");

		final Panel satisfactionPanel = new Panel();
		satisfactionPanel.setLayout(new FormLayout());
		satisfactionPanel.setBorder(false);
		satisfactionPanel.setPaddings(0, 10, 0, 0);

		final FieldSet fsSource = new FieldSet("Fuente");
		final FieldSet fsCausal = new FieldSet("Causal");

		chRealized.addListener(new CheckboxListenerAdapter() {
			@Override
			public void onCheck(Checkbox field, boolean checked) {

				satisfactionPanel.setVisible(checked);
				fsCausal.setVisible(!checked);
				fsSource.setVisible(!checked);
				wdPreCloseJobs.setHeight(180);
				chsatisfaction.setChecked(checked);
			}

		});

		chUnrealized = new Radio("El trabajo no fue realizado", "typeJob");

		rbQuality = new Radio("Calidad", "type");
		rbCustomer = new Radio("Cliente", "type");
		rbTransport = new Radio("Transporte", "type");
		rbSellers = new Radio("Vendedores", "type");

		fsSource.setWidth(270);
		fsSource.add(rbQuality);
		fsSource.add(rbCustomer);
		fsSource.add(rbTransport);
		fsSource.add(rbSellers);

		rbAccessory = new Radio(/*"Asesoria"*/"Asesor\u00eda", "typeCausal");
		rbWhim = new Radio("Caprichos del cliente", "typeCausal");
		rbContactNotFound = new Radio("Contacto no efectivo", "typeCausal");
		rbStructureOrDifference = new Radio("Estructura, diferencia o tonos", "typeCausal");
		rbMistreatment = new Radio("Maltrato", "typeCausal");
		rbPenniless = new Radio("Sin dinero", "typeCausal");

		fsCausal.setWidth(270);
		fsCausal.add(rbAccessory);
		fsCausal.add(rbWhim);
		fsCausal.add(rbContactNotFound);
		fsCausal.add(rbStructureOrDifference);
		fsCausal.add(rbMistreatment);
		fsCausal.add(rbPenniless);

		chUnrealized.addListener(new CheckboxListenerAdapter() {
			@Override
			public void onCheck(Checkbox field, boolean checked) {
				satisfactionPanel.setVisible(!checked);
				fsCausal.setVisible(checked);
				fsSource.setVisible(checked);
				chsatisfaction.setChecked(!checked);
				wdPreCloseJobs.setHeight(480);

			}

		});
		formPanel.add(chRealized);

		satisfactionPanel.add(chsatisfaction);
		formPanel.add(satisfactionPanel);
		formPanel.add(chUnrealized);
		formPanel.add(fsSource);
		formPanel.add(fsCausal);
		wdPreCloseJobs.add(formPanel);

		Button jobsClos = new Button("Aceptar");

		jobsClos.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (com.google.gwt.user.client.Window.confirm("Realmente desea pre-cerrar este trabajo")) {

					if(!chUnrealized.getValue() || (getValue("causal")!= null && getValue("fuente") !=null)){
					gui.getStaticAdminQueriesServiceAsync().closeWorkAndAddJamarResult(trab.getId(),
							trab.getTpoTrab().getId(), getValue("calificacion"), getValue("causal"), getValue("fuente"),
							gui.getUsuario().getId(), PreCloseWorkItemListener.this);
					}else{
						ClientUtils.showError("Debe seleccionar una fuente y causal");
					}

				}
			}
		});

		Button cancel = new Button("Cancelar");
		cancel.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				wdPreCloseJobs.close();
			}
		});
		wdPreCloseJobs.addButton(jobsClos);
		wdPreCloseJobs.addButton(cancel);

		wdPreCloseJobs.show();
		chRealized.setChecked(true);

	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(Boolean result) {
		TreeNode papaTrabajo = (TreeNode) selectedNode.getParentNode().getParentNode();
		Object obj = papaTrabajo.getAttributeAsObject("entity");
		if (obj instanceof Recurso) {
			NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
			l.updateRecurso(papaTrabajo);
		}
		wdPreCloseJobs.close();
		ClientUtils.alert("Informaci\u00f3n", "El trabajo ha sido cerrado.", ClientUtils.INFO);

	}

	private String getValue(String type) {

		if (type.equals("calificacion")) {
			return chRealized.getValue() && chsatisfaction.getValue()? "S" : "N";

		} else if (chUnrealized.getValue() && type.equals("causal")) {
			if (rbAccessory.getValue()) {
				
				return rbAccessory.getBoxLabel();
				
			} else if (rbWhim.getValue()) {
				
				return rbWhim.getBoxLabel();
				
			} else if (rbContactNotFound.getValue()) {
				
				return rbContactNotFound.getBoxLabel();
				
			} else if (rbStructureOrDifference.getValue()) {
				
				return rbStructureOrDifference.getBoxLabel();
				
			} else if (rbMistreatment.getValue()) {
				
				return rbMistreatment.getBoxLabel();
			} else if (rbPenniless.getValue()) {
				
				return rbPenniless.getBoxLabel();
			}

		} else if (chUnrealized.getValue() &&type.equals("fuente")) {

			if (rbQuality.getValue()) {
				
				return rbQuality.getBoxLabel();
				
			} else if (rbCustomer.getValue()) {
				
				return rbCustomer.getBoxLabel();
				
			} else if (rbTransport.getValue()) {
				
				return rbTransport.getBoxLabel();
				
			} else if (rbSellers.getValue()) {
				
				return rbSellers.getBoxLabel();
				
			}

		}

		return null;
	}

}
