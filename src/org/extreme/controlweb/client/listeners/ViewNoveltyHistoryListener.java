

//public class ViewCellHistoryListener 
package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;

import com.google.gwt.core.client.GWT;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ViewNoveltyHistoryListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private Recurso rec;
	private MyGridPanel grid;
	private QueriesServiceAsync serviceProxy;
	//private String urlReport;

	public ViewNoveltyHistoryListener(ControlWebGUI gui, Recurso rec) {
		this.gui = gui;	
		this.rec = rec;
		serviceProxy = (QueriesServiceAsync) GWT.create(QueriesService.class);


	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
					
				/*	urlReport = ClientUtils.REPORT_EXPORTER_SERVLET
							+ "?tipo=bitacoracel"
							+ "&desde=" + wnd.getFromDate()
							+ "&hasta="	+ wnd.getToDate()
							+ "&celular="+ cbMovil.getValue()
							+ "&tnovedad="+criterio;*/
					
					
					
							Panel p= new Panel("");
							p.setClosable(true);
							p.setLayout(new RowLayout());
							 
							
						
						/*	urlReport = ClientUtils.REPORT_EXPORTER_SERVLET
									+ "?tipo=bitacoracel"
									+ "&desde=" + wnd.getFromDate()
									+ "&hasta="	+ wnd.getToDate()
									+ "&celular="+ cbMovil.getValue()
									+ "&tnovedad="+ criterio;*/
							
							Toolbar toolbar = new Toolbar();
							
							/*ToolbarButton reportButton = new ToolbarButton("Exportar a Excel");
							reportButton.setIconCls("x-btn-icon save-report-btn");*/
					        												
					       /* reportButton.addListener(new ButtonListenerAdapter(){
					        	public void onClick(Button button, EventObject e) {
					        		Frame frame = new Frame(urlReport);
					        		Panel panel = new Panel("Exportar Reporte");
					        		panel.setLayout(new FitLayout());
					        		panel.setId("xlsReport");
					        		panel.setClosable(true);
					        		panel.add(frame);
					        		Window w = new Window("Exportando...");
					    			w.setSize(200, 200);
					    			w.setLayout(new FitLayout());
					    			w.add(panel);
					    			w.show();

					    			if (Navigator.getAppName().contains("Explorer")) {
					    				w.setVisible(true);
					    				w.close();
					    			} else {
					    				w.setVisible(false);
					    			}
					        		//gui.addInfoTab(panel);
					        	}
					        });

					        toolbar.addButton(reportButton);*/
							
							p.setTopToolbar(toolbar);
							
							grid = new MyGridPanel();

								serviceProxy.getBitacoraNovedades(ViewNoveltyHistoryListener.this.rec.getId(),new RPCFillTable(grid, p,
											null, false, false, true,
											null, null, new RowLayoutData(), new Function(){
												public void execute() {
													gui.hideMessage();
												}
											},"No hay datos"));	 
								
							p.setTitle("Bit\u00e1cora de Novedades");
							gui.addInfoTab(p);
							

					gui.showMessage("Por favor espere...", "Generando el reporte...", Position.CENTER);

		
	}
}


