package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.map.openlayers.GeofenceOverlay;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.WindowListenerAdapter;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class AdminGeofenceItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private Recurso rec;
	private Window wnd;
	private String recurso;
	private String contratista;
	private MapClickListener listener;
	private Button buttonUbic;
	private TextField nfLat;
	private TextField nfLon;
	private GeofenceOverlay geofence;
	private NumberField nfRadio;
	private TextField tfName;
	private QueriesServiceAsync serviceProxy;
	private MyGridPanel myGrid;
	private Panel panel;
	private String details;
	private boolean modify;
	private boolean delete;
	private boolean numerate;
	private int[] columnSizes;
	private boolean[] visible;
	
	public AdminGeofenceItemListener(ControlWebGUI g, Recurso rec) {
		this.gui = g;
		this.rec = rec;
		
		details = null; 
		modify = false;
		delete = true;
		numerate = true;
		columnSizes = new int[] { 80, 80, 80, 80,
				80, 80, 80, 80, 80, 80 };
		visible = new boolean[] { false, false,
				true, true, false, false, true, true, true, false };
		
		serviceProxy = (QueriesServiceAsync) GWT
				.create(QueriesService.class);
		
		this.listener = new MapClickListener(){
			public void onClick(MapClickEvent event) {
				buttonUbic.toggle();
				if (nfRadio.isValid() && !nfRadio.getText().equals("")) {
					if (geofence != null) {
						geofence.removeAll();
					}
					geofence = new GeofenceOverlay(event.getLonLat().lat(), 
							event.getLonLat().lon(), 
							nfRadio.getValue().intValue(), gui.getMap()) {
						public void doBeforeDraw() {
							if (this.getIsUbic()) {
								nfLat.setValue(Double.toString(ClientUtils.trunc(this
										.getCenterLonLat().lat())));
								nfLon.setValue(Double.toString(ClientUtils.trunc(this
										.getCenterLonLat().lon())));
							} else {
								tfName.setValue("");
								nfLat.setValue("");
								nfLon.setValue("");
								nfRadio.setValue("");
							}
						}

					};
					geofence.setIsUbic(true);
					gui.getMap().getVecAuxDraggable().addFeature(geofence);
					geofence.drawGeofence();
				} else
					ClientUtils.alert("Error", "Especifique un radio v\u00e1lido para la geocerca", ClientUtils.ERROR);
			}
		};
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		recurso = rec.getId();
		gui.stopTimer();
		contratista = rec.getCont().getId();
		
		wnd = new Window("Administrar Geocercas v1.3", false, true);
		
		wnd.addListener(new WindowListenerAdapter(){
			
			@Override
			public void onClose(Panel panel) {
				if(listener != null) 
					gui.getMap().getMap().removeListener(listener);
				if(geofence != null)
					geofence.removeAll();
				gui.restartTimer();
			}
		});
		
		wnd.setHeight(400);
		wnd.setWidth(500);
		wnd.setLayout(new RowLayout());
		wnd.setPaddings(5);
		
		FieldSet fs = new FieldSet("Datos de Geocerca");
		//fs.setLabelWidth(40);

		tfName = new TextField("Nombre");
		fs.add(tfName, new AnchorLayoutData("75%"));
		
		nfLat = new TextField("Latitud");
		nfLat.setReadOnly(true);		
		fs.add(nfLat, new AnchorLayoutData("75%"));
		
		nfLon = new TextField("Longitud");
		nfLon.setReadOnly(true);
		fs.add(nfLon, new AnchorLayoutData("75%"));
		
		nfRadio = new NumberField("Radio (m.)");
		nfRadio.setAllowDecimals(false);
		nfRadio.setAllowNegative(false);
		//nfRadio.setAllowBlank(false);
		nfRadio.setMaxValue(100000);
		
		nfRadio.setMinValue(100);
		nfRadio.setFieldMsgTarget("under");
		nfRadio.setMaxText("El radio m\u00e1ximo es 100000 m.");
		nfRadio.setMinText("El radio m\u00ednimo es 100 m.");
		nfRadio.setBlankText("Escriba un radio entre 100 y 100000 m.");
		fs.add(nfRadio, new AnchorLayoutData("75%"));
		
		fs.setButtonAlign(Position.CENTER);
		
		buttonUbic = new Button();
		buttonUbic.setText("Ubicar");
		buttonUbic.addListener(new ButtonListenerAdapter(){
			public void onToggle(Button button, boolean pressed) {
				if(pressed) {
					if (nfLat.getText().equals("")) {
						gui.getMap().setCrosshairCursor();
						gui.getMap().getMap().addMapClickListener(listener);
					}
					else
					{
						geofence.setIsUbic(true);
						geofence.setRadious(nfRadio.getValue().intValue());
						
						geofence.drawGeofence();
					}
				} else {
					gui.getMap().setDefaultCursor();
					if(listener != null) 
						gui.getMap().getMap().removeListener(listener);
				}
			}
		});
		
		buttonUbic.setEnableToggle(true);
		buttonUbic.setToggleGroup("geotoggle");
		buttonUbic.setIconCls("icon-no-ubic");
		fs.addButton(buttonUbic);
		
		myGrid = new MyGridPanel();
		
		panel = new Panel();
		panel.setBorder(false);
		panel.setPaddings(5, 0, 0, 0);
		panel.setLayout(new FitLayout());
		
		Button btnOK = new Button("Guardar");
		btnOK.addListener(new ButtonListenerAdapter(){
			public void onClick(Button button, EventObject e) {
				gui.showMessage("Por favor espere...", "Creando geocerca en el m\u00f3dem...", Position.LEFT);
				serviceProxy.createGeofence(rec.getVeh().getIdVeh(), Integer
						.parseInt(nfRadio.getText()), Double.parseDouble(nfLat
						.getText()), Double.parseDouble(nfLon.getText()),
						tfName.getText(), new AsyncCallback<Boolean>(){
							public void onFailure(Throwable caught) {
								gui.hideMessage();
								ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
								GWT.log("Error RPC", caught);
							}

							public void onSuccess(Boolean result) {
								if(geofence != null) {
									geofence.removeAll();
									geofence = null;
								}
								gui.hideMessage();
								ClientUtils.alert("Geocercas",
										"Geocerca creada exitosamente.",
										ClientUtils.INFO);
								serviceProxy.getGeocercas(recurso, contratista, new RPCFillTable(myGrid,
										panel, details, modify, delete, numerate, columnSizes, visible, null));
							}					
				});
			}
		});
		btnOK.setIconCls("save-report-btn");
		fs.addButton(btnOK);
		
		wnd.add(fs, new RowLayoutData("50%"));
		
		serviceProxy.getGeocercas(recurso, contratista, new RPCFillTable(myGrid,
				panel, details, modify, delete, numerate, columnSizes, visible, null));
		
		wnd.add(panel, new RowLayoutData("50%"));
		
		myGrid.addGridCellListener(new GridCellListenerAdapter(){
			@Override
			public void onCellClick(GridPanel grid, int rowIndex, int colindex,
					EventObject e) {
				Record r = grid.getStore().getAt(rowIndex);
				if (colindex != 0) {
					if (geofence != null) {
						geofence.removeAll();
					}
					geofence = new GeofenceOverlay(r.getAsDouble("Latitud"), r.getAsDouble("Longitud"), r.getAsInteger("Radio"), gui.getMap()) {						
						public void doBeforeDraw() {
							if (this.getIsUbic()) {
								nfLat.setValue(Double.toString(ClientUtils.trunc(this.getCenterLonLat().lat())));
								nfLon.setValue(Double.toString(ClientUtils.trunc(this.getCenterLonLat().lon())));
							} else {
								tfName.setValue("");
								nfLat.setValue("");
								nfLon.setValue("");
								nfRadio.setValue("");
							}
						}
					};
					geofence.setIsUbic(false);
					gui.getMap().getVecAuxDraggable().addFeature(geofence);
					gui.getMap().getMap().setCenter(geofence.getCenterLonLat());
					geofence.drawGeofence();
				} else {
					if(com.google.gwt.user.client.Window.confirm("Realmente desea desactivar esta geocerca?"))
						serviceProxy.deactivateGeofence(rec.getVeh().getIdVeh(), r
								.getAsString("N\u00famero Geocerca"), new AsyncCallback<Boolean>(){
	
									public void onFailure(Throwable caught) {
										ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
										GWT.log("Error RPC", caught);
									}
	
									public void onSuccess(Boolean result) {
										ClientUtils.alert("Geocercas",
												"Geocerca desactivada exitosamente.",
												ClientUtils.INFO);
										serviceProxy.getGeocercas(recurso, contratista, new RPCFillTable(myGrid,
												panel, details, modify, delete, numerate, columnSizes, visible, null));	
									}							
						});
				}
			}
		});
		
		wnd.show();
		
		
	}
}