package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.LoginWindowHandler;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.event.KeyListener;

public class LoginKeyListener implements KeyListener {

	ControlWebGUI gui;
	
	public LoginKeyListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	public void onKey(int key, EventObject e) {
		if(key == 13){
			new LoginWindowHandler(null, gui).onClick(null, e);
		}
	}

}
