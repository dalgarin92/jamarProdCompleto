package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ViewParkedVehsItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;

	public ViewParkedVehsItemListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		gui.showMessage("Por favor espere...", "Consultando informaci\u00f3n...", Position.CENTER);
		final Window w = new Window("Veh\u00edculos Estacionados");
		w.setWidth(600);
		w.setHeight(300);
		w.setLayout(new FitLayout());
		
		Panel p = new Panel();
		p.setBorder(false);
		p.setLayout(new FitLayout());
		
		MyGridPanel grid = new MyGridPanel();
		w.add(p);
		
		grid.addDetListener(new MyGridOnCellClickListener() {
			
			@Override
			public void onCellClick(Record r) {
				new NodeTreeClickListener(gui, true).updateRecurso(
						r.getAsString("recurso"), r.getAsString("contratista"),
						null);
			}
		});
		
		gui.getQueriesServiceProxy().getParkedVehs(
				gui.getProceso().getId(),
				new RPCFillTable(grid, p, "Ver en \u00e1rbol", false, false,
						true, null, null, new RowLayoutData(), new Function() {
							
							@Override
							public void execute() {
								gui.hideMessage();
								w.show();
							}
						}));
		
	}
	
}
