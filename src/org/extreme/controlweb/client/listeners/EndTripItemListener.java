package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.FormPanel;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class EndTripItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	//private TreeNode selectedNode;
	private Recurso rec;
	//private Window wdmultiJobsClose;
	//private CheckBoxGridPanel grid;

	public EndTripItemListener(ControlWebGUI gui, TreeNode treeNode,
			Recurso rec) {
		this.gui = gui;
		//this.selectedNode = treeNode;
		this.rec = rec;
	}
	

	public void onClick(BaseItem item, EventObject e) {  

        final Window window = new Window();  
        window.setTitle("Ingresar Novedad de Viaje");  
        window.setWidth(500);  
        window.setHeight(300);  
        window.setMinWidth(300);  
        window.setMinHeight(200);  
        window.setLayout(new FitLayout());  
        window.setPaddings(5);  
        window.setButtonAlign(Position.CENTER); 
        Button ingresar = new Button("Ingresar");
        Button cancelar = new Button("Cancelar");
        window.addButton(ingresar);  
        window.addButton(cancelar);  
  
        window.setCloseAction(Window.HIDE);  
        window.setPlain(true);  
  
        FormPanel formPanel = new FormPanel();  
 
        formPanel.setBaseCls("x-plain");  
        formPanel.setLabelWidth(55);  
        formPanel.setUrl("save-form.php");  
  
        formPanel.setWidth(500);  
        formPanel.setHeight(300);  
  
        final TextArea textArea = new TextArea("Subject", "subject");  
        textArea.setHideLabel(true);  
 
        formPanel.add(textArea, new AnchorLayoutData("100% 100%"));  
        window.add(formPanel);  
        window.show();
        
        cancelar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				window.close();
			}
		});
        
        ingresar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				
					gui.getQueriesServiceProxy().addTripNovelty(EndTripItemListener.this.rec.getId(),
							textArea.getValueAsString(),
							new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							ClientUtils.alert("Error", caught
									.getMessage(), ClientUtils.ERROR);
							GWT.log("Error RPC", caught);
						}

						@Override
						public void onSuccess(Boolean result) {
							window.close();
							ClientUtils.alert("Informaci\u00f3n",
									"Novedad de viaje almacenada.",
									ClientUtils.INFO);
						}
					});
				}
		});
    }  
}