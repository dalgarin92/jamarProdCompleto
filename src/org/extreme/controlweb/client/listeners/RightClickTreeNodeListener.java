package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Contratista;
import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.stats.listeners.CentralIndividualReportEventsListener;
import org.extreme.controlweb.client.stats.listeners.CentralIndividualReportItemListener;
import org.extreme.controlweb.client.stats.listeners.IndividualReportItemListener;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.Window;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Node;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.CheckItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.MenuItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.menu.event.CheckItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.event.TreeNodeListenerAdapter;

public class RightClickTreeNodeListener extends TreeNodeListenerAdapter {

	private ControlWebGUI gui;

	public RightClickTreeNodeListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	@Override
	public void onContextMenu(Node node, EventObject e) {
		TreeNode treeNode = (TreeNode) node;
		treeNode.select();
		Object obj = treeNode.getAttributeAsObject("entity");
		//String tipo = treeNode.getAttribute("tiponodo");
		Menu menu = new Menu();
		String iconCls = "";
		if(obj != null){
			if(obj instanceof Trabajo){
				if(!gui.getPerfil().equals("user") && !gui.getPerfil().equals("advuser"))
					toTrabajoMenu(menu, treeNode);
				iconCls = "icon-work";
			} else if(obj instanceof Recurso) {
				toRecursoMenu(menu, treeNode);
				iconCls = "icon-rec";
			} else if(obj instanceof Contratista){
				toContratistaMenu(menu, treeNode);
				iconCls = treeNode.getIconCls();
			} else if(obj instanceof Proceso){
				toProcesoMenu(menu, treeNode);
				iconCls = "icon-butterfly";
			}
			if ((!gui.getProceso().isSoloCentral() && (obj instanceof Proceso
					|| obj instanceof Contratista || obj instanceof Trabajo || obj instanceof Recurso))
					|| (gui.getProceso().isSoloCentral() && obj instanceof Recurso)) {
				Item moreInfoItem = new Item("M\u00e1s Informaci\u00f3n");
				if (iconCls.equals(""))
					iconCls = ((TreeNode) node).getIconCls();
				moreInfoItem.setIconCls(iconCls);

				if (obj instanceof Recurso && ((Recurso) obj).getVeh() == null)
					moreInfoItem.setDisabled(true);

				menu.addItem(moreInfoItem);
				moreInfoItem.addListener(new MoreInfoItemListener(gui, (TreeNode) node));
			}
			menu.showAt(e.getXY());
		}
	}

	private void toTrabajoMenu(Menu menu, TreeNode treeNode) {			

		if (!treeNode.getParentNode().getAttribute("id").equals("noUbic")
				&& !treeNode.getParentNode().getAttribute("id").equals("noAsig")) {

			Trabajo trab = (Trabajo)treeNode.getAttributeAsObject("entity");

			/*	CheckItem priorItem = new CheckItem("Prioritario", trab
					.isPrioritario());
			priorItem.addListener(new CheckItemListenerAdapter() {
				@Override
				public void onCheckChange(CheckItem item, boolean checked) {
					ChangePriorityButtonListener l = new ChangePriorityButtonListener(gui);
					l.onClick(null, null);
				}
			});*/


			CheckItem pathStartItem = new CheckItem("Inicio de Ruta", trab.isInicioRuta());
			pathStartItem.addListener(new CheckItemListenerAdapter() {
				@Override
				public void onCheckChange(CheckItem item, boolean checked) {
					PathStartButtonListener l = new PathStartButtonListener(gui);
					l.onClick(null, null);
				}
			});

			//			menu.addItem(priorItem);

			menu.addItem(pathStartItem);

			Item assignProgItem = new Item("Asignar Programaci\u00f3n");
			assignProgItem.setIconCls("icon-time");

			if(trab.getEstado().equals("30") || trab.getEstado().equals("40"))
				assignProgItem.setDisabled(true);

			menu.addItem(assignProgItem);	
			assignProgItem.addListener(new AssignProgWorkItemListener(gui, trab));

			if (!treeNode.getParentNode().getAttributeAsObject("entity").equals(
					"pending")) {
				Item closeJob = new Item("Cerrar Trabajo");
				closeJob.setIconCls("icon-del");
				closeJob.addListener(new CloseWorkItemListener(gui, treeNode,
						false));
				menu.addItem(closeJob);

			}
			
			if(!trab.getEstado().equals("30") 
					&& !trab.getEstado().equals("40") && !trab.getEstado().equals("50")){
				Item precloseJob = new Item("Pre-Cerrar Trabajo");
				precloseJob.setIconCls("icon-check-menu");
				precloseJob.addListener(new PreCloseWorkItemListener(gui, treeNode,trab));
				menu.addItem(precloseJob);
			}
		}
	}

	private void toRecursoMenu(Menu menu, final TreeNode treeNode) {
		final Recurso rec = (Recurso) treeNode.getAttributeAsObject("entity");

		Item refreshRecurso = new Item("Actualizar");

		refreshRecurso.addListener(new BaseItemListenerAdapter(){
			@Override
			public void onClick(BaseItem item, EventObject e) {
				new NodeTreeClickListener(gui, true).updateRecurso(treeNode);
			}
		});

		menu.addItem(refreshRecurso);

		Item endTripItem = new Item("Finalizar Viaje");
		endTripItem.addListener(new BaseItemListenerAdapter(){
			@Override
			public void onClick(BaseItem item, EventObject e) {
				ArrayList<TreeNode> nodes = new ArrayList<TreeNode>();
				gui.getTreeNodes(treeNode, Trabajo.class.getName(), nodes);
				boolean sw = nodes.size() > 0;
				for (TreeNode tn: nodes) {
					Trabajo t = (Trabajo)tn.getAttributeAsObject("entity");
					sw &= (t.getEstado().equals("30")
							|| t.getEstado().equals("40") 
							|| t.getEstado().equals("50")); 
				}
				if(!sw)
					ClientUtils.alert("Finalizar Viaje",
							"No es posible finalizar el viaje mientras haya trabajos pendientes.",
							ClientUtils.ERROR);
				else if(Window.confirm("Realmente desea finalizar el viaje?"))
					gui.getQueriesServiceProxy()
					.endCurrentTrip(rec.getId(), rec.getCont().getId(),
							new MyBooleanAsyncCallback("Viaje finalizado.", new Function(){
								@Override
								public void execute() {
									treeNode.remove();
									gui.getMap().showAll();
								}
							}));
			}
		});
		menu.addItem(endTripItem);

		if (!gui.getPerfil().equals("user")
				&& !gui.getPerfil().equals("advuser") && !gui.getProceso().isSoloCentral()) {
			CheckItem onlineItem = new CheckItem("Trabaja Online", 
					rec.isOnline());
			onlineItem.addListener(new CheckItemListenerAdapter() {
				@Override
				public void onCheckChange(CheckItem item, boolean checked) {
					SetOnlineButtonListener l = new SetOnlineButtonListener(gui);
					l.onClick(null, null);
				}
			});
			menu.addItem(onlineItem);
		}


		Menu statsSubMenu = new Menu();
		Item indItem = new Item("An\u00e1lisis Individual");
		indItem.addListener(new IndividualReportItemListener(gui, rec.getId(),
				rec.getCont().getId(), "Recurso: " + rec.getNombre()));

		statsSubMenu.addItem(indItem);




		Menu reportsSubMenu = new Menu();

		Item otItem = new Item("Reporte de OTs Precerradas");
		otItem.addListener(new ViewClosedWorksItemListener(gui,
				(Recurso) treeNode.getAttributeAsObject("entity")));

		reportsSubMenu.addItem(otItem);
		reportsSubMenu.addSeparator();

		Item trackItem = new Item("Hist\u00f3rico de Recorrido");
		//trackItem.setIconCls("icon-del");
		reportsSubMenu.addItem(trackItem);	
		trackItem.addListener(new TrackCarItemListener(gui, treeNode));

		Item viewEventsItem = new Item("Reporte de Eventos");
		//viewEventsItem.setIconCls("icon-del");
		reportsSubMenu.addItem(viewEventsItem);	
		viewEventsItem.addListener(new ViewEventsItemListener(gui, rec, "eventos"));


		Item viewGeocercaItem = new Item("Reporte de Geocercas");
		//viewEventsItem.setIconCls("icon-del");
		reportsSubMenu.addItem(viewGeocercaItem);	
		viewGeocercaItem.addListener(new ViewEventsItemListener(gui, rec, "geocerca"));

		Item viewOdometerItem = new Item("Reporte de Od\u00f3metro");
		//viewEventsItem.setIconCls("icon-del");
		reportsSubMenu.addItem(viewOdometerItem);	
		viewOdometerItem.addListener(new ViewEventsItemListener(gui, rec, "odometro"));

		Item indAItem = new Item("An\u00e1lisis Individual de Vehiculo");
		indAItem.addListener(new CentralIndividualReportItemListener(gui, rec.getId(),
				rec.getCont().getId(), "An\u00e1lisis Individual: " + rec.getNombre()));

		Item indEvent = new Item("An\u00e1lisis Individual de Eventos");
		indEvent.addListener(new CentralIndividualReportEventsListener(gui, rec.getId(),
				rec.getCont().getId(), "An\u00e1lisis Individual de Eventos: " + rec.getNombre()));


		statsSubMenu.addItem(indAItem);
		statsSubMenu.addItem(indEvent);

		MenuItem reportsSubmenuItem = new MenuItem("Reportes", reportsSubMenu);
		menu.addItem(reportsSubmenuItem);

		MenuItem statsSubmenuItem = new MenuItem("Estad\u00edsticas", statsSubMenu);
		menu.addItem(statsSubmenuItem);
		
		

		/*Item PosActItem = new Item("Actualizar Posici\u00f3n Actual");
		//PosActItem.setIconCls("icon-del");
		menu.addItem(PosActItem);	
		PosActItem.addListener(new SendOTAItemListener(gui, treeNode, true));*/

		if (rec.getVeh() != null && !rec.getVeh().getTipoModem().equals("ANTARES")) {
			Item geoFenceItem = new Item("Administrar Geocercas");
			//PosActItem.setIconCls("icon-del");
			//menu.addItem(geoFenceItem);
			geoFenceItem.addListener(new AdminGeofenceItemListener(gui, rec));
		}

		Item regMov = new Item("Registrar Movimiento");
		//sendOTAItem.setIconCls("icon-del");
		menu.addItem(regMov);	
		regMov.addListener(new SendRegMovimientoItemListener(gui, treeNode));

		if(gui.getPerfil().equals("superadmin")){
			/*Item sendOTAItem = new Item("Enviar Comandos OTA");
			//sendOTAItem.setIconCls("icon-del");
			menu.addItem(sendOTAItem);	
			sendOTAItem.addListener(new SendOTAItemListener(gui, treeNode, false));*/
		}
		/*Item asigPriorItem = new Item("Asignar Prioridad");
		asigPriorItem.addListener(new PrioritiesAssignListener(gui, treeNode, rec));
		menu.addItem(asigPriorItem);*/

		Item multicloseJob = new Item("Cerrar Multiples Trabajos");
		multicloseJob.setIconCls("icon-del");
		multicloseJob.addListener(new MultiCloseWorkItemListener(gui, treeNode, rec));		
		menu.addItem(multicloseJob);

		Item observacionviaje = new Item("Observacion en Ruta");
		//observacionviaje.setIconCls("icon-del");
		observacionviaje.addListener(new EndTripItemListener(gui, treeNode, rec));		
		menu.addItem(observacionviaje);
		
		Item noveltylog = new Item("Bitacora de Novedades");
		//observacionviaje.setIconCls("icon-del");
		noveltylog.addListener(new ViewNoveltyHistoryListener(gui, rec));		
		menu.addItem(noveltylog);
	}

	private void toContratistaMenu(Menu menu, TreeNode treeNode) {
		Contratista cont = (Contratista) treeNode.getAttributeAsObject("entity");

		Menu statsSubMenu = new Menu();
		Item indItem = new Item("An\u00e1lisis Individual");
		indItem.addListener(new IndividualReportItemListener(gui,
				null, cont.getId(), "Contratista: " + cont.getNombre()));

		statsSubMenu.addItem(indItem);

		MenuItem statsSubmenuItem = new MenuItem("Estad\u00edsticas", statsSubMenu);
		menu.addItem(statsSubmenuItem);
	}

	private void toProcesoMenu(Menu menu, TreeNode treeNode) {
		Item viewEventsItem = new Item("Ver Posiciones Actuales");
		//viewEventsItem.setIconCls("icon-del");
		menu.addItem(viewEventsItem);	
		viewEventsItem.addListener(new ViewEventsItemListener(gui, treeNode
				.getAttributeAsObject("entity"), "posactual"));

		//if (!gui.getProceso().isSoloCentral()) {
		Item viewClosedWorksItem = new Item("Ver Trabajos Precerrados");
		//viewEventsItem.setIconCls("icon-del");
		menu.addItem(viewClosedWorksItem);
		viewClosedWorksItem.addListener(new ViewClosedWorksItemListener(gui));
		//}

	}
}
