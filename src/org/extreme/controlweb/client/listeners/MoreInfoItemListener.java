package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Contratista;
import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.GroupedGridPanel;
import org.extreme.controlweb.client.gui.WorksGridPanel;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class MoreInfoItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private String tipo;
	private TreeNode node;
	//private Window wnd;
	
	public MoreInfoItemListener(ControlWebGUI gui, TreeNode node) {
		this.gui = gui;
		this.tipo = node.getAttribute("tiponodo");
		this.node = node;		
	}
	
	public void onClick(BaseItem item, EventObject e) {
		final Object obj = node.getAttributeAsObject("entity");
		if (obj instanceof Trabajo) {
			gui.getTrabajoInfoPanel(((Trabajo) obj).getId(),
					((Trabajo) obj).getTpoTrab().getId(), null, ((Trabajo) obj).getEstado());			
		} else if(obj instanceof Recurso) {
			if(((Recurso)obj).getVeh() != null)
				gui.addInfoTab(getRecursoInfoPanel());
		} else if(obj instanceof Contratista || obj instanceof Proceso){
			gui.addInfoTab(getWorksInfoPanel("Trabajos "
					+ node.getText().split("-")[0], node.getIconCls()));
		} else if(tipo.equals("noUbic") || tipo.equals("noAsig")) {
			gui.addInfoTab(getWorksInfoPanel(node.getText().split("-")[0], node
					.getIconCls()));
		}

	}

	private Panel getWorksInfoPanel(String string, String iconCls) {
		Panel panel = new Panel("M\u00e1s Informaci\u00f3n");
		panel.setIconCls(iconCls);
		panel.setClosable(true);
		panel.setLayout(new RowLayout());
		WorksGridPanel workGrid = new WorksGridPanel(false);
		workGrid.setTitle(string);
		ArrayList<TreeNode> a = new ArrayList<TreeNode>();
		gui.getTreeNodes(node, Trabajo.class.getName(), a);
		workGrid.addWorks(a);
		panel.add(workGrid, new RowLayoutData("50%"));
		return panel;
	}

	private Panel getRecursoInfoPanel() {
		ArrayList<TreeNode> a = new ArrayList<TreeNode>();
		gui.getTreeNodes(node, Trabajo.class.getName(), a);
		
		Panel panel = new Panel("M\u00e1s Informaci\u00f3n");
		panel.setIconCls("icon-rec");
		panel.setClosable(true);
		panel.setLayout(new RowLayout());
		panel.setPaddings(0, 5, 0, 0);
		GroupedGridPanel grid = new GroupedGridPanel(); 
		
		Recurso rec = (Recurso) node.getAttributeAsObject("entity");
		
		grid.addProperty("C\u00f3digo", rec.getId(), "Informaci\u00f3n B\u00e1sica");
		grid.addProperty("Nombre", rec.getNombre(), "Informaci\u00f3n B\u00e1sica");
		grid.addProperty("Contratista", rec.getCont().getNombre(), "Informaci\u00f3n B\u00e1sica");
		grid.addProperty("Tipo de Recurso", rec.getTporec().getNombre(), "Informaci\u00f3n B\u00e1sica");
		
		grid.addProperty("Placa", rec.getVeh().getPlaca(), "Informaci\u00f3n de Veh\u00edculo");
		grid.addProperty("Marca", rec.getVeh().getMarca(), "Informaci\u00f3n de Veh\u00edculo");
		grid.addProperty("Color", rec.getVeh().getColor(), "Informaci\u00f3n de Veh\u00edculo");
		grid.addProperty("Tipo de Veh\u00edculo", rec.getVeh().getTipoVeh(), "Informaci\u00f3n de Veh\u00edculo");
		grid.addProperty("Estado de Veh\u00edculo", (rec.getVeh()
				.getEstadoVeh() != null ? (rec.getVeh().getEstadoVeh().equals(
				"E") ? "ENCENDIDO" : "APAGADO") : "N/A"),
				"Informaci\u00f3n de Veh\u00edculo");		
		
		grid.addProperty("Direcci\u00f3n", rec.getVeh().getDireccion(), "Informaci\u00f3n de Localizaci\u00f3n");
		grid.addProperty("Municipio", rec.getVeh().getMunicipio(), "Informaci\u00f3n de Localizaci\u00f3n");
		grid.addProperty("Departamento", rec.getVeh().getMunicipio(), "Informaci\u00f3n de Localizaci\u00f3n");
		grid.addProperty("\u00daltimo Reporte", rec.getVeh().getFechaUltRep(), "Informaci\u00f3n de Localizaci\u00f3n");
		
		panel.add(grid, new RowLayoutData("50%"));
		
		if (!gui.getProceso().isSoloCentral()) {
			WorksGridPanel workGrid = new WorksGridPanel(true);
			workGrid.addWorks(a);
			panel.add(workGrid, new RowLayoutData("50%"));
		}
		return panel;
	}
}
