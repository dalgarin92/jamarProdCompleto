package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.gui.ControlWebGUI;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Component;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.ButtonListener;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.Checkbox;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.layout.ColumnLayout;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.RowLayout;

public class InterfacePanel extends MultiFieldPanel {

	private HTML htmlLabel;
	private Button btn;
	private Checkbox ckDelete;
	private String name;
	private Panel p;
	private Timer timer;
	private ControlWebGUI gui;
	protected ButtonListener listener;

	private static ArrayList<Component> components;

	public InterfacePanel(final String title, ControlWebGUI gui, boolean checkDelete) {
		super();
		this.gui = gui;
		name = title;
		if(components == null)
			components = new ArrayList<Component>();

		
		createTimer();
		htmlLabel = new HTML("Presione \"Cargar...\" para iniciar el procedimiento de cargue de " + title + ".");

		gui.getQueriesServiceProxy().getDateUpdate(title, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				System.out.println("Error consultando fecha de "+title+" Error:"+caught.getMessage());
			}
			@Override
			public void onSuccess(String result) {
				String info = "";
				if(result.equals(""))
					info = " (No cargado)";
				else
					info = " ("+result+")";
				InterfacePanel.this.setTitle(title+info);
			}
		});

		this.setTitle(title);
		this.setPaddings(15);
		this.setLayout(new ColumnLayout());

		p = new Panel();
		p.setBorder(false);
		p.setPaddings(0);
		p.setHeight(150);
		p.setLayout(new RowLayout());

		if(checkDelete){
			ckDelete = new Checkbox("Borrar datos previos.");
			p.add(ckDelete);
		}

		p.add(htmlLabel);

		this.addToRow(p, 380);
		btn = new Button("Cargar...", new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				try {
					disableAllComponents();
					htmlLabel.setHTML("Se inici\u00f3 procedimiento de cargue de " + title + "...");
					
					if(listener != null) {
						listener.onClick(button, e);
						timer.scheduleRepeating(800);
					}
				} catch (Exception e1) {
					htmlLabel.setHTML("Se presentaron los siguientes errores:" + e1.getMessage() + "...");
				}

			}
		}); 
		this.addToRow(btn, new ColumnLayoutData(1));
		components.add(btn);
		if(checkDelete)
			components.add(ckDelete);		
	}

	private void createTimer() {
		timer = new Timer() {
			@Override
			public void run() {
				gui.getQueriesServiceProxy().getInterfaceState(
						new AsyncCallback<String>() {
							@Override
							public void onFailure(Throwable caught) {
								htmlLabel.setHTML(caught.getMessage());
							}

							@Override
							public void onSuccess(String result) {
								htmlLabel.setHTML(result);
								if (result.contains("100%")
										|| result.contains("ERROR")
										|| result.contains("select")
										|| result.contains("update")
										|| result.contains("insert")) {
									cancelTimer();
									enableAllComponents();
								}
							}
						});
			}
		};
	}

	public HTML getHtmlLabel() {
		return htmlLabel;
	}

	public void cancelTimer() {
		timer.cancel();
		createTimer();
	}

	public static void disableAllComponents(){
		if(components != null){
			for (Component btn : components) 
				btn.disable();
		}
	}

	public static void enableAllComponents(){
		if(components != null){
			for (Component btn : components) 
				btn.enable();
		}
	}

	public boolean isCheckedDelete() {
		return ckDelete != null ? ckDelete.getValue() : false;
	}

	public void setListener(ButtonListener listener) {
		this.listener = listener;
	}

	public String getName() {
		return name;
	}
}