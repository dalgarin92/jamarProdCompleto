package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class CloseWorkItemListener extends BaseItemListenerAdapter implements
AsyncCallback<Boolean> {

	private ControlWebGUI gui;
	private TreeNode selectedNode;
	private Trabajo trab;
	private boolean quit;

	public CloseWorkItemListener(ControlWebGUI gui, TreeNode treeNode, boolean quit) {
		this.gui = gui;
		this.selectedNode = treeNode;
		this.quit = quit;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {

		if (Window.confirm("Realmente desea cerrar el trabajo "
				+ selectedNode.getText().split("\\[")[0] + "?")) {
			trab = (Trabajo) selectedNode.getAttributeAsObject("entity");

			String trabajo = trab.getId();
			String tipotrabajo = trab.getTpoTrab().getId();
			String estadotrabajo=trab.getEstado();

			QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
			if(quit)
				serviceProxy.quitJob(trabajo, tipotrabajo, gui.getUsuario().getId(), this);
			else {
				if (!estadotrabajo.equals("30"))
					serviceProxy.closeJob(trabajo, tipotrabajo, gui.getUsuario().getId(), this);
				else
					ClientUtils.alert("Error",
							"El trabajo ya se encuentra cerrado",
							ClientUtils.ERROR);
			}

		}
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(Boolean result) {
		TreeNode papaTrabajo = (TreeNode) selectedNode.getParentNode().getParentNode();
		Object obj = papaTrabajo.getAttributeAsObject("entity");
		if(obj instanceof Recurso)
		{
			NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
			l.updateRecurso(papaTrabajo);
		}else
			gui.refreshNoUbicNoAsigTrabs(selectedNode);
		if(quit){
			gui.refreshNoUbicNoAsigTrabs(selectedNode);
			ClientUtils.alert("Informaci\u00f3n", "Asignacion eliminada.",
					ClientUtils.INFO);
		}else
			ClientUtils.alert("Informaci\u00f3n", "El trabajo ha sido cerrado.",
					ClientUtils.INFO);
		/*NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
		l.updateRecurso(selectedNode.getParentNode());
		ClientUtils.alert("Informaci\u00f3n", "El trabajo ha sido cerrado.",
				ClientUtils.INFO);*/
	}
}
