package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.gui.CheckBoxGridPanel;
import org.extreme.controlweb.client.gui.CheckBoxGridPanelPlaning;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanel;
import org.extreme.controlweb.client.stats.handlers.RPCFillCheckboxGridPanelPlaning;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.ToolbarTextItem;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.WindowListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class MultiAssignListener extends ButtonListenerAdapter {

	private ControlWebGUI gui;
	private Button multiSelectToggleButton;

	private Toolbar tb;
	private CheckBoxGridPanelPlaning grid;
	private Record recRecurso;
	private Window wnd;
	private MyComboBox cbRecursos;
	private String recurso;
	private String contratista;
	private Function reloadGrid;
	private ArrayList<String[]> sk;
	private boolean isTreeMultiAssign;

	public MultiAssignListener(ControlWebGUI gui) {
		this(gui, false);
	}
	
	public void setTrabajos(ArrayList<String[]> sk) {
		this.sk = sk;
	}
	
	public MultiAssignListener(ControlWebGUI gui, boolean isTreeMultiAssign) {
		this.gui = gui;
		this.multiSelectToggleButton = gui.getMultiSelectToggleButton();
		this.isTreeMultiAssign = isTreeMultiAssign;
		this.reloadGrid = new Function() {

			@Override
			public void execute() {
				try {
					grid.uncheckAll();

				} catch (Exception e) {
				}

				if (grid.isRendered())
					grid.getEl().mask("Por favor, espere...");
				MultiAssignListener.this.gui.getQueriesServiceProxy()
						.getInfoTrabMultiSelect(
								sk,
								recurso,
								contratista,
								new RPCFillCheckboxGridPanelPlaning(grid,
										new Function() {
											@Override
											public void execute() {
												try {
													grid.checkAll();
												} catch (Exception e) {
												}
												if (recurso == null) {
													MultiAssignListener.this.gui
															.hideMessage();
													// multiSelectToggleButton.toggle(false);
													MultiAssignListener.this.gui
															.getMap()
															.unSelectAll();

													grid.setTopToolbar(tb);
													wnd.add(grid);
													wnd.show();

												} else
													try {
													} catch (Exception e) {
													}
												grid.getStore().commitChanges();
												if (grid.isRendered())
													grid.getEl().unmask();
											}
										}));
			}
		};
	}

	@Override
	public void onClick(Button button, EventObject e) {

		if (gui.getMap().getSelectedKeys().size() > 0 || isTreeMultiAssign) {
			if(!isTreeMultiAssign)
				sk = this.gui.getMap().getSelectedKeys();
			
			try {
				gui.showMessage("Por favor espere...",
						"Cargando informaci\u00f3n...", Position.TOP);
			} catch (Exception e1) {
			
			}
			wnd = new Window("Trabajos Seleccionados...", 830, 500, false, true);
			wnd.setLayout(new FitLayout());
			wnd.setButtonAlign(Position.CENTER);
			wnd.setResizable(false);
			wnd.addListener(new WindowListenerAdapter() {
				@Override
				public void onClose(Panel panel) {

					recRecurso = null;
					recurso = null;
					contratista = null;
					multiSelectToggleButton.toggle(false);
				}
			});

			Button btnOk = new Button("Asignar");

			btnOk.addListener(new ButtonListenerAdapter() {

				private void assign(boolean conf) {
					Record[] gridTrabs = grid.getSelectedRows();
					if (gridTrabs.length > 0) {
						if (conf
								|| com.google.gwt.user.client.Window
										.confirm("Confirma que desea asignar estos "
												+ gridTrabs.length
												+ " trabajos a "
												+ recRecurso
														.getAsString("nombre")
												+ "?"))

						{
							ArrayList<String[]> trabs = new ArrayList<String[]>();
							for (int i = 0; i < gridTrabs.length; i++)
								trabs.add(new String[] {
										gridTrabs[i].getAsString("Trabajo"),
										gridTrabs[i].getAsString("ttrab")});
							// Record inicioRuta = null;

							try { // inicioRuta = queryInicioRuta();

								Function f = new Function() {
									@Override
									public void execute() {
										TreeNode node = gui.searchRecurso(
														recRecurso.getAsString("id"),
														recRecurso.getAsString("id_contratista"));
										node.setChecked(true);
										node.setAttribute("checked", true);
										node.expand();
										new ReloadButtonListener(gui).onClick(
												null, null);
										wnd.close();
									}
								};
								
								MyBooleanAsyncCallback cb = new MyBooleanAsyncCallback(
										"Trabajos asignados.", f);
								cb.setOnFailureFunction(f);
								
								gui.getQueriesServiceProxy().multiAssignJobs(
										trabs,
										recRecurso.getAsString("id"),
										recRecurso.getAsString("id_contratista"),
										gui.getUsuario().getId(), null, null,
										cb);

							} catch (Exception e) {
								ClientUtils.alert("Error", e.getMessage(),
										ClientUtils.ERROR);
							}
						}
					} else
						ClientUtils.alert("Error", "Debe seleccionar al menos un trabajo a asignar.",
										ClientUtils.ERROR);
				}

				@Override
				public void onClick(Button button, EventObject e) {
					// if(available >= pesoactual && recRecurso != null){
					if (recRecurso != null) {
						assign(false);
					} else {
						if (recRecurso != null) {

						} else
							ClientUtils.alert("Error",
									"Debe seleccionar un recurso.",
									ClientUtils.ERROR);
					}

				}
			});

			wnd.addButton(btnOk);

			grid = new CheckBoxGridPanelPlaning();		
			grid.addGridCellListener(new GridCellListenerAdapter() {
				@Override
				public void onCellContextMenu(final GridPanel grid,
						int rowIndex, int cellIndex, EventObject e) {
					e.stopEvent();
					Menu m = new Menu();
					Item mi = new Item("Fijar como Inicio de Ruta");
					Item trabDetalles = new Item("Ver Items del Trabajo");
					final Record record = grid.getStore().getAt(rowIndex);

					mi.addListener(new BaseItemListenerAdapter() {
						@Override
						public void onClick(BaseItem item, EventObject e) {
							Record[] recs = grid.getStore().getRecords();
							for (Record r : recs) {
								r.set("Inicio Ruta", "N");
							}
							record.set("Inicio Ruta", "S");
							grid.getStore().commitChanges();

						}
					});
					trabDetalles.addListener(new BaseItemListenerAdapter() {

						public void onClick(BaseItem item, EventObject e) {

						}
					});
					m.addItem(mi);
					m.addItem(trabDetalles);
					m.showAt(e.getXY());
				}
			});

			tb = new Toolbar();

			tb.addItem(new ToolbarTextItem("Asignar a:"));

			cbRecursos = new MyComboBox();
			tb.addField(cbRecursos);
			
			ToolbarButton expandButton = new ToolbarButton();
			expandButton.setCls("x-btn-icon expand-all-btn");
			expandButton.setTooltip("Seleccionar Todas");
			expandButton.addListener(new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
					grid.checkAll();
				}
			});
			tb.addButton(expandButton);
			ToolbarButton collapseButton = new ToolbarButton();
			collapseButton.setCls("x-btn-icon collapse-all-btn");
			collapseButton.setTooltip("Quitar Selecci\u00f3n");
			collapseButton.addListener(new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
					grid.uncheckAll();
				}
			});
			tb.addButton(collapseButton);
						
			cbRecursos.addListener(new ComboBoxListenerAdapter() {
				@Override
				public void onSelect(ComboBox comboBox, Record record, int index) {
					recRecurso = record;
					recurso = recRecurso.getAsString("id");
					contratista = recRecurso.getAsString("id_contratista");
					reloadGrid.execute();
				}
			});

			RPCFillComplexComboHandler cb = new RPCFillComplexComboHandler(
					cbRecursos, true, null, null);
			cb.setFunction(reloadGrid);
			gui.getQueriesServiceProxy().getAllRecursosProcesoConductores(
					gui.getProceso().getId(), cb);

		} else
			ClientUtils.alert("Error", "Debe seleccionar al menos un trabajo.",
					ClientUtils.ERROR);
	}

	public CheckBoxGridPanelPlaning getGrid() {
		return grid;
	}

	public Record getRecRecurso() {
		return recRecurso;
	}
}
