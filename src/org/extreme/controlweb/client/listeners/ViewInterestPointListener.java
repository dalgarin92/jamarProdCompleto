package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.map.openlayers.control.DragFeature.CompleteDragFeatureListener;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Pixel;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.event.TabPanelListenerAdapter;
import com.gwtext.client.widgets.event.WindowListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.Field;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.MultiFieldPanel;
import com.gwtext.client.widgets.form.Radio;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.form.event.TextFieldListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayout;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.ColumnLayout;
import com.gwtext.client.widgets.layout.ColumnLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ViewInterestPointListener extends ButtonListenerAdapter{

	private ControlWebGUI gui;
	private String details;
	private boolean modify;
	private boolean delete;
	private boolean numerate;
	private int[] columnSizes;
	private boolean[] visible;

	private Window wnd;
	private TextField txtName;
	private TextField txtLatitude;
	private TextField txtLongitude;
	private Button btLocateByCoordinates;
	private TextField txtAddress;
	private ComboBox cbMunicipality;
	private Button btLocateByAddress;
	private ComboBox cbImage;
	private Image imgIcono;
	private VectorFeature mkPtoInteres;
	private Radio rbTipoUsuario;
	private Radio rbTipoProceso;
	private boolean drag = false;
	private MyGridPanel myGrid;
	private RPCFillTable rpcFillTable;
	private TabPanel tabPanelLocation;
	private CompleteDragFeatureListener markerListener;
	private String idPunto;
	private String tipoPunto;
	private boolean iconified;
	private MapClickListener listener;
	private FieldSet fieldSetImage;
	private static final String MESSAGE_MUNIP = "Seleccione municipio";
	
	private boolean isModification;

	Function recargarPuntosInteres = new Function() {

		public void execute() {
			gui.getQueriesServiceProxy().getPtoInt(gui.getUsuario() == null ? null : gui
					.getUsuario().getId(), gui.getProceso() == null ? null
					: gui.getProceso().getId(), rpcFillTable);
			gui.refreshPtoInts();
			tabPanelLocation.setDisabled(false);
			txtName.setValue("");
			cbImage.setValue("");
			fieldSetImage.setVisible(false);
			txtAddress.setVisible(false);
			cbMunicipality.setValue("");
			txtLatitude.setValue("");;
			txtLongitude.setValue("");
			isModification = false;
			gui.getMap().getVecAuxDraggable().removeAllFeatures();
			
			
		}
	};

	private void setValuesLatLng() {
		
		LonLat ll = mkPtoInteres.getCenterLonLat();
		ll.transform("EPSG:900913","EPSG:4326");
		
		
		txtLatitude.setValue(String.valueOf(ll.lat()));
		txtLongitude.setValue(String.valueOf(ll.lon()));
	}

	public ViewInterestPointListener(ControlWebGUI gui) {
		
		this.gui = gui;
		iconified = false;
		details = null;
		modify = true;
		delete = true;
		numerate = true;
		columnSizes = new int[] { 80, 80, 80, 80, 80, 80, 80, 80, 80 };
		visible = new boolean[] { false, true, false, false, true, true, true,
				false, false };

		

		this.listener = new MapClickListener() {
			@Override
			public void onClick(MapClickEvent mapClickEvent) {
				btLocateByCoordinates.toggle();
				removePtoInteres();
				
				LonLat lonLat = new LonLat(mapClickEvent.getLonLat().lon(), mapClickEvent.getLonLat().lat());    					
				
				LocalizableObject lo = new LocalizableObject(String
						.valueOf(lonLat.lat()), String
						.valueOf(lonLat.lon()));
				
				drag = true;
				relocate(lo, drag, false);
				setValuesLatLng();
			}
		};

		this.markerListener = new CompleteDragFeatureListener() {
			@Override
			public void onComplete(VectorFeature vf, Pixel p) {
				setValuesLatLng();
			}
		};
	}

	private void removePtoInteres() {
		if (mkPtoInteres != null) {
			gui.getMap().getVecAuxDraggable().removeFeature(mkPtoInteres);
			mkPtoInteres = null;
		}
	}

	private void relocate(LocalizableObject result, boolean drag, boolean center) {
		removePtoInteres();
		
		mkPtoInteres = gui.getMap().createDraggableMarker(
				result.getLatitudAsDouble(),
				result.getLongitudAsDouble(),
				"images/puntos_int/"
						+ cbImage.getStore().query("id", cbImage.getValue())[0]
								.getAsString("icono") + ".png");
		if (center) {
			gui.getMap().getMap().setCenter(mkPtoInteres.getCenterLonLat());
		}
	}
	
	public void repaintPtoInteres(boolean center) {
		if (mkPtoInteres != null) {
			LonLat ll = mkPtoInteres.getCenterLonLat();
			ll.transform("EPSG:4326","EPSG:900913");
			
			relocate(new LocalizableObject(String.valueOf(ll.lat()),
					String.valueOf(ll.lon())), drag, center);
		}
	}
	
	@Override
	public void onClick(Button button, EventObject e) {

		wnd = new Window("Administrar Puntos de Interes", false, true);

		wnd.setHeight(600);
		wnd.setWidth(500);
		wnd.setLayout(new FitLayout());
		wnd.setPaddings(5);

		Panel mainPanel = new Panel();
		mainPanel.setBorder(false);
		mainPanel.setPaddings(5);
		mainPanel.setLayout(new AnchorLayout());

		FieldSet data = new FieldSet();
		data.setBorder(false);
		data.setPaddings(5);
		data.setLabelWidth(40);

		FieldSet fs = new FieldSet("Datos");
		fs.setButtonAlign(Position.CENTER);

		txtName = new TextField("Nombre");
		data.add(txtName, new AnchorLayoutData("95%"));
		txtName.addListener(new TextFieldListenerAdapter() {
			@Override
			public void onBlur(Field field) {
				repaintPtoInteres(true);
			}
		});

		MultiFieldPanel pSuperData = new MultiFieldPanel();
		pSuperData.setLayout(new ColumnLayout());
		pSuperData.addToRow(data, 250);
		fieldSetImage = new FieldSet("Imagen Seleccionada");
		pSuperData.addToRow(fieldSetImage, new ColumnLayoutData(1));
		// pIcono.setAutoHeight(true);
		// pIcono.setBorder(false);
		fieldSetImage.setPaddings(0, 60, 0, 0);

		rbTipoUsuario = new Radio("Punto de Usuario");
		rbTipoUsuario.setChecked(true);
		rbTipoProceso = new Radio("Punto Global");

		rbTipoProceso.setName("tipo");
		rbTipoUsuario.setName("tipo");

		cbImage = new ComboBox("Imagen");
		cbImage.setEmptyText("Seleccione imagen");
		cbImage.addListener(new ComboBoxListenerAdapter() {
			@Override
			public void onSelect(ComboBox comboBox, Record record, int index) {
				showIcon(record);
			}

		});

		imgIcono = new Image("images/puntos_int/pal2/icon15.png");

		fieldSetImage.add(imgIcono);
		data.add(rbTipoUsuario);

		rbTipoProceso.setDisabled(gui.getProceso() == null);

		data.add(rbTipoProceso);
		// data.add(imgIcono);
		// serviceProxy.getIconos(new RPCFillComboBoxHandler(cmbIcon,data,1));
		gui.getQueriesServiceProxy().getIconos(
				new RPCFillComboBoxHandler(cbImage, data, 1));

		// data.add(imgIcono);
		mainPanel.setLayout(new RowLayout());
		// mainPanel.add(data, new RowLayoutData("20%"));
		mainPanel.add(pSuperData, new RowLayoutData("25%"));
		tabPanelLocation = new TabPanel();

		tabPanelLocation.addListener(new TabPanelListenerAdapter() {

			@Override
			public boolean doBeforeTabChange(TabPanel source, Panel newPanel,
					Panel oldPanel) {
				boolean doAsk = !txtLatitude.getValueAsString().equals("")
						|| !txtAddress.getValueAsString().equals("");
				boolean sw = true;
				if (doAsk)
					if (sw = com.google.gwt.user.client.Window
							.confirm("Se perder\u00e1n los datos que no se hayan guardado. Desea continuar?")) {
						removePtoInteres();
						txtLatitude.setValue("");
						txtLongitude.setValue("");
						txtAddress.setValue("");
						cbMunicipality.setValue("");
					}
				return sw;
			}
		});

		Panel tab1 = new Panel("Geograficamente");
		tab1.setPaddings(5);
		// tab1.setLayout(new AnchorLayout());

		txtLatitude = new TextField("Latitud");
		txtLatitude.setReadOnly(true);
		fs.add(txtLatitude, new AnchorLayoutData("75%"));

		txtLongitude = new TextField("Longitud");
		txtLongitude.setReadOnly(true);
		fs.add(txtLongitude, new AnchorLayoutData("75%"));
		
		
		btLocateByCoordinates = new Button("Ubicar");
		btLocateByCoordinates.setEnableToggle(true);
		btLocateByCoordinates.setToggleGroup("ubicar");
		
		btLocateByCoordinates.addListener(new ButtonListenerAdapter() {
			@Override
			public void onToggle(Button button, boolean pressed) {
				
				if(txtName.getValueAsString()!=null
						&&!txtName.getValueAsString().isEmpty()){
					
				if (iconified) {
					if (pressed) {
						gui.getMap().setCrosshairCursor();
						gui.getMap().getMap().addMapClickListener(listener);
					} else {
						gui.getMap().setDefaultCursor();
						if (listener != null)
							gui.getMap().getMap().removeListener(listener);
					}
				} else {
					button.setPressed(false);
					ClientUtils.alert("Info",
							"Debe seleccionar primero un icono",
							ClientUtils.INFO);
				}
			}else {
						button.setPressed(false);
						ClientUtils.showInfo("Debe ingresar un nombre");
					}
			}
		});
		fs.addButton(btLocateByCoordinates);

		tab1.add(fs);
		tabPanelLocation.add(tab1);

		FieldSet fs2 = new FieldSet("Datos");
		fs2.setButtonAlign(Position.CENTER);

		Panel tab2 = new Panel("Por Direcci\u00f3n");
		tab2.setPaddings(5);
		txtAddress = new TextField("Direcci\u00f3n");
		fs2.add(txtAddress, new AnchorLayoutData("75%"));
	
		cbMunicipality = new ComboBox("Municipio");
		cbMunicipality.setEmptyText(MESSAGE_MUNIP);
	
		gui.getQueriesServiceProxy().getMunicipios(
				new RPCFillComboBoxHandler(cbMunicipality, fs2, 1));
		
		
		btLocateByAddress = new Button("Ubicar");
		btLocateByAddress.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {

				if (iconified) {
					gui.getQueriesServiceProxy().getDireccion(txtAddress.getValueAsString(),
							cbMunicipality.getValue(),
							new AsyncCallback<LocalizableObject>() {
								public void onFailure(Throwable caught) {
									ClientUtils.alert("Error", caught
											.getMessage(), ClientUtils.ERROR);
									GWT.log("Error RPC", caught);
									removePtoInteres();
								}

								public void onSuccess(LocalizableObject result) {
									
									LonLat lonLat = new LonLat(result.getLongitudAsDouble(), result.getLatitudAsDouble());    					
									lonLat.transform("EPSG:4326","EPSG:900913");
									
									LocalizableObject lo = new LocalizableObject(
											String.valueOf(lonLat.lat()), 
											String.valueOf(lonLat.lon()));
									
									drag = false;
									relocate(result, drag, false);
								}
							});
				} else {
					ClientUtils.alert("Info",
							"Debe seleccionar primero un icono",
							ClientUtils.INFO);
				}
			}
		});
		fs2.addButton(btLocateByAddress);
		tab2.add(fs2);
		tabPanelLocation.add(tab2);
		mainPanel.add(tabPanelLocation, new RowLayoutData("35%"));
	
		
		Button bnCancelar = new Button("Cancelar");
		bnCancelar.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				wnd.close();
			}
		});

		myGrid = new MyGridPanel();

		rpcFillTable = new RPCFillTable(myGrid, mainPanel, details, modify, delete,
				numerate, columnSizes, visible, new RowLayoutData("40%"));

		gui.getQueriesServiceProxy().getPtoInt(
				gui.getUsuario() == null ? null : gui.getUsuario().getId(),
				gui.getProceso() == null ? null : gui.getProceso().getId(), rpcFillTable);

		Button bnSavePoint = new Button("Guardar");

		bnSavePoint.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {

				/* si el panel de direcciones y coordenadas esta deshabilitado significa 
				que se va a actualizar un puntode interes*/
				if (!isModification) { 
					
					String id = null;
					String tipo = null;

					AsyncCallback<Boolean> myAsyncCallBack = new MyBooleanAsyncCallback(
							"Creaci\u00f3n exitosa...", recargarPuntosInteres);

					if (rbTipoProceso.getValue()) {
						id = gui.getProceso().getId();
						tipo = ClientUtils.PROCESS_POI;
					} else {
						id = gui.getUsuario().getId();
						tipo = ClientUtils.USER_POI;
					}

					gui.showMessage("Por favor espere...",
							"Creando el punto de inter\u00e9s...",
							Position.CENTER);

					if (mkPtoInteres != null) {
						String direccion = txtAddress.getValueAsString();
						String municipio = cbMunicipality.getEl() != null ? cbMunicipality
								.getEl().getValue()
								: null;

						if (municipio != null&& municipio.equals(MESSAGE_MUNIP))
							municipio = null;

						gui.getQueriesServiceProxy().createPtoInt(
								id,	tipo,
								String.valueOf(mkPtoInteres.getCenterLonLat().lat()),
								String.valueOf(mkPtoInteres.getCenterLonLat().lon()),
								txtName.getValueAsString(),
								direccion, municipio, null, cbImage.getValue(),	myAsyncCallBack);

					} else {
						ClientUtils.alert("Error",
								"Primero debe ubicar el punto de inter\u00e9s",
								ClientUtils.ERROR);
					}
				} else { // si se va a modificar un punto de interes y el cuadro
							// de la posicion esta deshabilitado
					String id = null;
					if (rbTipoProceso.getValue()) {
						id = gui.getProceso().getId();

					} else {
						id = gui.getUsuario().getId();

					}

					AsyncCallback<Boolean> cb = new MyBooleanAsyncCallback(
							"Actualizaci\u00f3n exitosa...",
							recargarPuntosInteres);
					gui.getQueriesServiceProxy().updatePtoInt(idPunto,
							txtName.getValueAsString(), cbImage.getValue(),
							tipoPunto, id, rbTipoProceso.getValue(),
							txtLatitude.getValueAsString(),txtLongitude.getValueAsString(), cb);

				}
				
		
			}

		});
		wnd.setButtonAlign(Position.CENTER);
		wnd.addButton(bnSavePoint);
		wnd.add(mainPanel, new RowLayoutData());
		wnd.addListener(new WindowListenerAdapter() {
			@Override
			public void onClose(Panel panel) {
				removePtoInteres();
				gui.getMap().deactivateDragControl();
				gui.getMap().setDefaultCursor();
			}
		});
		
		
		
		myGrid.addDeleteListener(new MyGridOnCellClickListener() {
			public void onCellClick(Record r) {
				/*
				 * if (com.google.gwt.user.client.Window
				 * .confirm("Realmente desea eliminar este punto de inter\u00e9s?"
				 * ))
				 */
				gui.getQueriesServiceProxy().deletePtoInt(
						r.getAsString("id"),
						new MyBooleanAsyncCallback(
								"Punto de Inter\u00e9s eliminado.",
								recargarPuntosInteres));

			}
		});

		myGrid.addModListener(new MyGridOnCellClickListener() {
			public void onCellClick(Record r) {

				isModification = true;		
				txtName.setValue(r.getAsString("Descripci\u00f3n"));
				cbImage.setValue(r.getAsString("ID Icono"));
				
				showIcon(cbImage.getStore().query("id",
						r.getAsString("ID Icono"))[0]);
				
				tabPanelLocation.setDisabled(true);

				idPunto = r.getAsString("id");
				tipoPunto = r.getAsString("Tipo");

				
				if (tipoPunto.equals("Punto Global")) {
					gui.getMap().getMkPtosGlobal().removeAll();
					rbTipoProceso.setValue(true);
					rbTipoUsuario.setValue(false);
				} else {
					gui.getMap().getMkPtosUsuario().removeAll();
					rbTipoProceso.setValue(false);
					rbTipoUsuario.setValue(true);
				}			
				
				removePtoInteres();
				
				LonLat lonLat = new LonLat(r.getAsDouble("Longitud"), r.getAsDouble("Latitud"));    					
				lonLat.transform("EPSG:4326","EPSG:900913");
				
				LocalizableObject lo = new LocalizableObject(
						String.valueOf(lonLat.lat()), 
						String.valueOf(lonLat.lon()));
				
				drag = true;
				relocate(lo, drag, false);
				setValuesLatLng();
				
				
				
				
				
			}
		});
		fieldSetImage.setVisible(false);
		wnd.show();
		gui.getMap().setCompleteDragListener(markerListener);
		gui.getMap().activateDragControl();
	}

	private void showIcon(Record record) {
		imgIcono.setUrl("images/puntos_int/" + record.getAsString("icono")
				+ ".png");
		repaintPtoInteres(true);
		fieldSetImage.setVisible(true);
		iconified = true;
	}
	
}
