package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;
import com.gwtext.client.widgets.tree.TreePanel;

public class ChangePriorityButtonListener extends BaseItemListenerAdapter
		implements AsyncCallback<Boolean> {

	TreePanel panel;
	TreeNode selectedNode;
	private ControlWebGUI gui;
	private Trabajo trab;

	public ChangePriorityButtonListener(ControlWebGUI gui) {
		this.gui = gui;
		this.panel = gui.getTree();
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		TreeNode selectedNode = gui.getMap().getSelectedTrabajoNode();
		if (selectedNode != null) {
			trab = (Trabajo) selectedNode.getAttributeAsObject("entity");
			if (!selectedNode.getParentNode().getAttribute("id").equals("noUbic") 
					&& !selectedNode.getParentNode().getAttribute("id").equals("noAsig")) {
				this.selectedNode = selectedNode;
				if (Window.confirm("Realmente desea cambiar prioridad del trabajo "
								+ selectedNode.getText())) {

					String trabajo = trab.getId();
					String tipotrabajo = trab.getTpoTrab().getId();

					String prioridad = trab.isPrioritario() ? "N" : "S";

					QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
							.create(QueriesService.class);
					serviceProxy.setJobPriority(trabajo, tipotrabajo, prioridad, this);
				}
			}
			else
				ClientUtils.alert("Error", "Antes de asignarle prioridad a un trabajo, <br/> debe asign\u00e1rselo a alg\u00fan recurso.", ClientUtils.ERROR);
		}

	}

	public void onFailure(Throwable caught) {	
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}
	
	public void onSuccess(Boolean result) {
		NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
		l.updateRecurso((TreeNode) selectedNode.getParentNode());
		ClientUtils.alert("Informaci\u00f3n", "El trabajo ahora "
				+ (trab.isPrioritario() ? "NO " : "") + "tiene prioridad.",
				ClientUtils.INFO);

		if (trab.isPrioritario())
			trab.setPrioritario(false);
		else
			trab.setPrioritario(true);
	}
}
