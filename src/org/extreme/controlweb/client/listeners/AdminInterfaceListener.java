package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextArea;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Margins;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.Store;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.grid.GridView;
import com.gwtext.client.widgets.grid.RowParams;
import com.gwtext.client.widgets.layout.AccordionLayout;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class AdminInterfaceListener extends BaseItemListenerAdapter {


	private ControlWebGUI gui;
	BorderLayoutData centerLayoutData;
	BorderLayoutData eastLayoutData;

	public AdminInterfaceListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		Window wnd = new Window("Interfaz...", 500, 220, false, false);
		wnd.setLayout(new AccordionLayout(true));

		final InterfacePanel i1 = new InterfacePanel("Conductores", gui, false);
		i1.setListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				gui.getQueriesServiceProxy().loadConductoresOracle(
						i1.isCheckedDelete(),
						new AsyncCallback<ArrayList<ArrayList<RowModel>>>() {

							@Override
							public void onSuccess(ArrayList<ArrayList<RowModel>> result) {
								if(result!=null)
								i1.setTitle(i1.getName() + " (Actualizado)");
								auditoriaInterfaz(result, "CONDUCTORES JAMAR", "CONDUCTORES XCONTROL", "Conductor");
							}

							@Override
							public void onFailure(Throwable caught) {
								gui.hideMessage();

								i1.setHtml("Log de Errores...<br>"+caught.getMessage());
								//ErrorWindow log=new ErrorWindow("Log de Errores...",300,220,caught.getMessage());
								//log.show();
								GWT.log("Error RPC", caught);

							}
						});
			}
		});

		final InterfacePanel i2 = new InterfacePanel("Entregas", gui, false);
		i2.setListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				gui.getQueriesServiceProxy().loadTrabajosOracle(
						i2.isCheckedDelete(), new AsyncCallback<ArrayList<ArrayList<RowModel>>>() {

							@Override
							public void onSuccess(ArrayList<ArrayList<RowModel>> result) {
								if(result!=null)
								i2.setTitle(i2.getName() + " (Actualizado)");
								auditoriaInterfaz(result, "ENTREGAS JAMAR", "ENTREGAS XCONTROL", "Nro Trabajo");
							}

							@Override
							public void onFailure(Throwable caught) {
								gui.hideMessage();
								i1.setHtml("Log de Errores...<br>"+caught.getMessage());
								//GWT.log("Error RPC", caught);
								gui.getWestTabPanel().getEl().unmask();

							}
						});				
			}
		});

		Button btnLog= new Button("Ver errores...", new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				
				//gui.getQueriesServiceProxy().getInterfaceState();
				gui.getQueriesServiceProxy().getInterfaceError("1",new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						
					}

					@Override
					public void onSuccess(String result) {
						Window wnd = new Window("Log Errores...", 500, 220, false, false);
						TextArea infoLog= new TextArea();
						infoLog.setWidth("480");
						infoLog.setHeight("180");
						infoLog.setText(result);
						wnd.add(infoLog);
						wnd.show();
						
					}
				});
				
			}
		}); 
		
	
		wnd.add(i1);
		wnd.add(i2);
		wnd.addButton(btnLog);
		wnd.show();
	}


	public void auditoriaInterfaz(ArrayList<ArrayList<RowModel>> result,String tittle1, String tittle2, String record ){
		/*
		 * tittle1 entregas jamar
		 * tittle2 entregas xcontrol
		 * tittle Nro Trabajo
		 * 
		 * */

		final Window wnd = new Window("Auditor\u00eda de interfaz", true, true);
		wnd.setLayout(new BorderLayout());
		wnd.setSize(750, 500);

		Panel p = new Panel();
		p.setWidth(550);
		p.setBorder(false);
		p.setLayout(new FitLayout());

		Panel p2 = new Panel();
		p2.setBorder(false);
		p2.setLayout(new FitLayout());
		p2.setWidth(350);

		ArrayList<RowModel> trabajosSteckerl=result.get(0);
		ArrayList<RowModel> trabajosCreadoSteckerl=result.get(1);
		//final TreeNode parent = gui.getNoAsigTreeNode();
		MyGridPanel gridSQL=new MyGridPanel();
		if (trabajosSteckerl.size()>0){
			gridSQL.configure(trabajosSteckerl.get(0).getColumnNames(), null, false, false,
					true, null, ((RowModelAdapter)trabajosSteckerl.get(0))
					.getVisibility());
			gridSQL.loadRows(trabajosSteckerl);
			gridSQL.getStore().sort(record);
		}
		gridSQL.setTitle(tittle1);

		p.add(gridSQL);
		MyGridPanel gridPG=new MyGridPanel();
		if (trabajosCreadoSteckerl.size()>0){
			gridPG.configure(trabajosCreadoSteckerl.get(0).getColumnNames(), null, false, false,
					true, null, ((RowModelAdapter)trabajosCreadoSteckerl.get(0))
					.getVisibility());
			gridPG.loadRows(trabajosCreadoSteckerl);
			gridPG.getStore().sort(record);
		}
		p2.add(gridPG);

		gridPG.setTitle(tittle2);

		Record [] recTrabSteckerl= gridSQL.getStore().getRecords();
		for (int i=0;i<recTrabSteckerl.length;i++){
			Record [] recTrabSteckerlExisten =gridPG.getStore().query(record, recTrabSteckerl[i].getAsString(record));

			if (recTrabSteckerlExisten!=null)
				if (recTrabSteckerlExisten.length>0){
					recTrabSteckerl[i].set("Existe", "S");
				}

				else
					recTrabSteckerl[i].set("Existe", "N");
			else
				recTrabSteckerl[i].set("Existe", "N");

		}
		gridSQL.getStore().commitChanges();
		gridSQL.setView(new GridView() {
			@Override
			public String getRowClass(Record record, int index, RowParams rowParams, Store store) {
				if (record.getAsString("Existe").equals("N"))
				{             
					return "RED";
				}
				return "BLUE";
			}

		});


		centerLayoutData = new BorderLayoutData(RegionPosition.CENTER);
		centerLayoutData.setMargins(new Margins(0, 0, 0, 0));
		centerLayoutData.setCMargins(new Margins(0, 0, 0, 0));

		eastLayoutData = new BorderLayoutData(RegionPosition.EAST);
		eastLayoutData.setMargins(new Margins(0, 0, 0, 0));
		eastLayoutData.setCMargins(new Margins(0, 0, 0, 0));
		eastLayoutData.setMinSize(155);
		eastLayoutData.setMaxSize(1000);
		eastLayoutData.setSplit(true);
		wnd.add(p, 	centerLayoutData);
		wnd.add(p2, eastLayoutData); 
		wnd.show();

	}



}
