package org.extreme.controlweb.client.listeners;

import java.util.Date;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.ControldeFlotasConf;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.gui.MyLabelClock;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Image;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.TabPanel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.ToolbarTextItem;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class ControldeFlotasItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private TabPanel tabPanel;
	private int tiempoAct = 900000;
	private Timer timer;
	private Panel panel;
	private Panel gridContainerPanel;
	private MyGridPanel grid;
	private ComboBox tiempAct;
	private Image controlF;
	private Image powerby;
	private Panel horPan;
	private DateField fecha;
	private ToolbarButton act;
	private MyLabelClock clock;
	private Function cargarAllFlota;
	private Panel footer;

	//private Panel eastPanel;
	private ToolbarButton config;
	private String fechatot;

	public ControldeFlotasItemListener(ControlWebGUI gui, TabPanel tabPanel) {
		this.tabPanel = tabPanel;
		this.gui = gui;
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	public void cargarAllFlota(){
		cargarAllFlota.execute();
	}
	
	public void setTiempoAct(int tiempoAct) {
		this.tiempoAct = tiempoAct;
	}
	
	public int getTiempoAct() {
		return tiempoAct;
	}
	
	public ComboBox getTiempAct() {
		return tiempAct;
	}
	
	public void onClick(BaseItem item, EventObject e) {
		fecha = new DateField("Fecha", "Y-m-d");
		fecha.setValue(new Date());

		if (!fecha.getValueAsString().equals("")
				&& fecha.getValueAsString() != null) {
			fechatot = DateTimeFormat.getFormat("yyyy-MM-dd").format(
					fecha.getValue());
		}

		crearPanel(String.valueOf(tiempoAct));

		timer = new Timer() {
			public void run() {
				String panelnom = panel.getTitle();
				Panel activo = new Panel();
				activo = tabPanel.getActiveTab();
				if (panelnom.equalsIgnoreCase(activo.getTitle())) {
					if (tiempAct.getValueAsString().equalsIgnoreCase("1")) {
						tiempoAct = 900000;
					} else {
						tiempoAct = Integer.parseInt(tiempAct
								.getValueAsString());
					}
					timer.scheduleRepeating(tiempoAct);
					cargarAllFlota.execute();
				} else {
					System.out.println("no activa");
				}

			}
		};

		timer.scheduleRepeating(tiempoAct);

	}

	public void crearPanel(String tiempoactuAct) {
		controlF = new Image("images/utiles/logoExtreme.PNG");
		powerby = new Image("images/utiles/poweredby.PNG");

		Panel imgPanel = new Panel();
		imgPanel.setLayout(new FitLayout());
		imgPanel.setBorder(false);
		imgPanel.add(controlF);
		
		panel = new Panel("Control de Flotas");
		panel.setLayout(new BorderLayout());
		
		horPan = new Panel();
		horPan.setBorder(false);
		horPan.setLayout(new FitLayout());
		horPan.add(imgPanel);
		horPan.setHeight(100);
		
	/*	eastPanel = new Panel();
		eastPanel.setBodyStyle("background-color:#DFE8F6; padding:5px 5px 5px 5px;");
		eastPanel.setLayout(new FormLayout());
		eastPanel.setBorder(false);
		eastPanel.setWidth(300);*/
		
		
		//horPan.add(eastPanel, new BorderLayoutData(RegionPosition.EAST));


		tiempAct = ClientUtils.createCombo("Actualizaci\u00f3n",
				new SimpleStore(new String[] { "id", "nombre" },
						new String[][] { { "60000", "1 Minuto" },
								{ "300000", "5 Minutos" },
								{ "900000", "15 Minutos" },
								{ "1800000", "30 Minutos" },
								{ "2700000", "45 Minutos" },
								{ "3600000", "60 Minutos" } }));
		if (tiempoactuAct.equalsIgnoreCase("1")) {
			tiempoAct = 900000;
		} else {
			tiempoAct = Integer.parseInt(tiempoactuAct);
		}
		tiempAct.setValue(tiempoactuAct);
		act = new ToolbarButton("Cargar");
		act.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				if (tiempAct.getValueAsString().equalsIgnoreCase("1")) {
					tiempoAct = 900000;
				} else {
					tiempoAct = Integer.parseInt(tiempAct.getValueAsString());
				}
				timer.scheduleRepeating(tiempoAct);
				cargarAllFlota.execute();

			}

		});
		
		clock = new MyLabelClock("Fecha Actual");
		
		gui.getQueriesServiceProxy().getServerDate(new AsyncCallback<Date>() {
			@Override
			public void onFailure(Throwable caught) {
				ClientUtils.alert("ERROR", caught.getMessage(), ClientUtils.ERROR);
			}

			@Override
			public void onSuccess(Date result) {
				clock.setDate(result);
				clock.startClock();
			}
		});

		/*eastPanel.add(fecha); 
		eastPanel.add(tiempAct);
		eastPanel.add(clock);
		eastPanel.setButtonAlign(Position.CENTER);
		eastPanel.addButton(act);*/
		
		config = new ToolbarButton("Configuraci\u00f3n..");
		config.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				new ControldeFlotasConf(gui, fechatot, String
						.valueOf(tiempoAct), true,
						ControldeFlotasItemListener.this);
			}

		});
		config.setDisabled(false);
		
		gridContainerPanel = new Panel();
		gridContainerPanel.setPaddings(5);
		gridContainerPanel.setLayout(new RowLayout());
		gridContainerPanel.setBorder(false);
		
		final Toolbar tb = new Toolbar();
		tb.addItem(new ToolbarTextItem("Fecha: "));
		tb.addField(fecha);
		tb.addSeparator();
		tb.addItem(new ToolbarTextItem("Actualizaci\u00f3n: "));
		tb.addField(tiempAct);
		tb.addSeparator();
		tb.addButton( act);
		tb.addSeparator();
		tb.addButton(config);
		tb.addFill();
		tb.addItem(new ToolbarTextItem("Fecha Actual: "));
		tb.addField(clock);
		
		
		gridContainerPanel.setTopToolbar(tb);

		cargarAllFlota = new Function() {
			public void execute() {
				try {
					if(grid != null && grid.getEl() != null)
						gridContainerPanel.remove(grid, true);
				} catch (Exception e) {}

				grid = new MyGridPanel();

				DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd");

				gui.getStaticQueriesServiceAsync().getControldeFlotasGeneral(
						fecha.getEl() != null ? fecha.getEl().getValue() : dtf
								.format(new Date()),
						new RPCFillTable(grid, gridContainerPanel, null, false,
								false, false, null, null, new RowLayoutData()));
			}
		};

		cargarAllFlota.execute();
		
		//eastPanel.addButton(config);
		footer = new Panel();
		footer.setBorder(false);
		footer.setLayout(new RowLayout());
		footer.setHeight(25);
		footer.add(powerby);
		panel.add(gridContainerPanel, ClientUtils.CENTER_LAYOUT_DATA);
		panel.add(horPan, new BorderLayoutData(RegionPosition.NORTH));
		panel.add(footer, new BorderLayoutData(RegionPosition.SOUTH));
		panel.setClosable(true);
		tabPanel.add(panel);
		tabPanel.setActiveItemID(panel.getId());
	}

}
