package org.extreme.controlweb.client.listeners;

import java.util.Date;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.handlers.FVReportsHandler;
import org.extreme.controlweb.client.util.ClientUtils;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.SimpleStore;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class RepsButtonListener extends BaseItemListenerAdapter {


	public static final int REP_TRAFICO = 1;
	private ControlWebGUI gui;
	private ComboBox cbtipo;

	private int rep; 
	public ControlWebGUI getGui() {
		return gui;
	}

	public void setGui(ControlWebGUI gui) {
		this.gui = gui;
	}

	public int getRep() {
		return rep;
	}

	public void setRep(int rep) {
		this.rep = rep;
	}




	public RepsButtonListener(ControlWebGUI gui, int rep) {
		this.gui = gui;
		this.rep = rep;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		/*showMessage("Por favor espere...","Generando Reporte...", Position.LEFT);
		getQueriesServiceProxy().generateReporteTrafico(
		"",
		new FVReportsHandler(ControlWebGUI.this, "xls"));*/
		final String title;
		switch (rep) {
			case REP_TRAFICO:
				title = "Reporte control de trafico";
				break;
			default:
				title = "";
				break;
		}

		final Window wnd =new Window(title, false, true);
		wnd.setHeight(125);
		wnd.setWidth(300);
		wnd.setModal(false);
		final Panel p;

		Button b = null;
		p = new Panel();
		p.setBorder(false);
		p.setPaddings(5,5,0,0);
		p.setLayout(new FormLayout());

		b = new Button("Aceptar");
		wnd.setButtonAlign(Position.CENTER);
		wnd.addButton(b);

		final DateField fecha = new DateField("Fecha", "Y/m/d");
		fecha.setValue(new Date());

		fecha.setReadOnly(true);
		fecha.setWidth(95);

		cbtipo= ClientUtils.createCombo("Tipo",
				new SimpleStore(new String[] { "id", "nombre" },
						new String[][] { 
						{ "pdf", "Pdf"},
						{ "xls", "Excel"},

				})); 

		cbtipo.setValue("xls");

		p.add(fecha);
		p.add(cbtipo);

		wnd.add(p);

		b.addListener(new ButtonListenerAdapter() {
			@Override
			public void onClick(Button button, EventObject e) {
				gui.showMessage("Por favor espere...",
						"Generando reporte...", Position.LEFT);
				switch (rep) {
					case REP_TRAFICO:
						gui.getQueriesServiceProxy().generateReporteTrafico(
								fecha.getValue(),
								cbtipo.getValueAsString(),
								new FVReportsHandler(gui, wnd,cbtipo.getValueAsString()));
						break;
				}
			}
		});

		wnd.show();


	} 

}
