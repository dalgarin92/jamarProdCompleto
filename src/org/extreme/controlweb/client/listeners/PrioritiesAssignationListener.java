package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.admin.ComboLoader;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class PrioritiesAssignationListener extends BaseItemListenerAdapter implements AsyncCallback<Boolean>  {


	private final ControlWebGUI gui;	
	private final String trabajo;
	private final String prioridad;
	private Record record;
	private GridPanel gridpanel;
	private String viaje;
	private String prioridadesTomadas;
	private Function f;
	private Recurso recurso;
	private int numpri;
	public PrioritiesAssignationListener(ControlWebGUI gui,String trabajo ,String prioridad ,Record record, GridPanel gridpanel,Recurso recurso ) {
		this.gui = gui;
		this.trabajo=trabajo;
		this.prioridad=prioridad;
		this.record=record;
		this.gridpanel=gridpanel;
		this.recurso= recurso;

	}
	public PrioritiesAssignationListener(ControlWebGUI gui,String trabajo,String prioridad ,String viaje,String prioridadesTomadas,Function f) {
		this.gui = gui;
		this.trabajo=trabajo;
		this.prioridad=prioridad;
		this.prioridadesTomadas=prioridadesTomadas;
		this.f=f;
		this.viaje=viaje;
	}




	@Override
	public void onClick(BaseItem item, EventObject e) {

		final Window wnd = new Window("Asignar Prioridad", false, true);

		wnd.setLayout(new RowLayout());
		wnd.setButtonAlign(Position.CENTER);
		wnd.setWidth(200);
		wnd.setHeight(100);
		wnd.setPaddings(5);
		wnd.setIconCls("icon-butterfly");

		final Panel panel=new Panel();


		final ArrayList<RowModel> values = new ArrayList<RowModel>();

		gui.getQueriesServiceProxy().getWorksAddress(recurso.getId(), recurso.getCont().getId(),null, new AsyncCallback<ArrayList<RowModel>>() {

			@Override
			public void onSuccess(ArrayList<RowModel> result) {
				numpri=result.size();
				if (gridpanel!=null){
					for (int i=1 ;i<=numpri;i++){
						RowModelAdapter val1 = new RowModelAdapter();
						Record[] recs =null ;

						if (gridpanel!=null){
							recs =gridpanel.getStore().query("Prioridad", ""+i);
						}

						if (recs==null  ){
							val1.addValue("id", ""+i);
							val1.addValue("nombre", ""+i);
							values.add(val1);

						} 
						else
						{ if(recs.length==0)
						{
							val1.addValue("id", ""+i);
							val1.addValue("nombre", ""+i);
							values.add(val1);	

						}
						}

					}	

				}

				if (prioridadesTomadas!=null) {
					for (int i=1 ;i<=numpri;i++){
						RowModelAdapter val1 = new RowModelAdapter();
						if (!prioridadesTomadas.contains(""+i)){
							val1.addValue("id", ""+i);
							val1.addValue("nombre", ""+i);
							values.add(val1);
						}

					}
				}

				RowModelAdapter val1 = new RowModelAdapter();
				val1.addValue("id", "NINGUNA");
				val1.addValue("nombre", "NINGUNA");
				values.add(val1);

				final MyComboBox cbPriorities = new MyComboBox("Prioridades");
				cbPriorities.setEditable(false);

				ComboLoader cl = new ComboLoader() {
					@Override
					public void load(AsyncCallback<ArrayList<RowModel>> cb) {
						new RPCFillComplexComboHandler(cbPriorities, false, panel,
								new RowLayoutData()).onSuccess(values);
					}
				};
				cl.load(new RPCFillComplexComboHandler( cbPriorities, panel, 0));


				if (prioridad!=null ) {
					if (!prioridad.equals("NINGUNA")) {
						cbPriorities.setValue(prioridad);
					}
				}else{
					//if (prioridad.equals("0")||prioridad==null) {
					cbPriorities.setValue("NINGUNA");
				} 


				Button btnOk = new Button("Asignar");


				btnOk.addListener(new ButtonListenerAdapter(){
					@Override
					public void onClick(Button button, EventObject e) {	


						String value=cbPriorities.getValueAsString();
						if (value.equals("NINGUNA")) {
							value="0";
						}	
						if (record==null) {
							gui.getQueriesServiceProxy().asignarPrioridad(trabajo  ,value,viaje, PrioritiesAssignationListener.this);
						}

						if (record!=null){
							record.set("Prioridad",cbPriorities.getValueAsString());
							String direccion = record.getAsString("Direccion");

							Record[] trabs = gridpanel.getStore().query("Direccion",direccion);

							for (Record trab : trabs) {
								trab.set("Prioridad", cbPriorities.getValueAsString());
							}

							//	if(cbPriorities.getValueAsString().equals("1")) {
							//	record.set("Inicio Ruta","S");
							//}

							gridpanel.getStore().sort("Prioridad",SortDir.ASC); 	
							gridpanel.getStore().commitChanges();

							Record[] t = gridpanel.getStore().getRecords();
							for(Record d: t){
								System.out.println(""+d.getAsString("Trabajo")+"-"+d.getAsString("Direccion")+"-"+d.getAsString("Prioritario"));


							}



						}

						wnd.close();
						gridpanel.getStore().load();

					}

				});

				wnd.add(panel);
				wnd.addButton(btnOk);
				wnd.show();
			}

			@Override
			public void onFailure(Throwable caught) {


			}
		});
		//int numpri=gui.getProceso().getPrioridades();






	}

	@Override 
	public void onFailure(Throwable caught) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSuccess(Boolean result) {
		if (f!=null) {
			f.execute();
		}	//gridpanel.getStore().commitChanges();

		gridpanel.getStore().load();
		ClientUtils.alert("Informaci\u00f3n", "Se actualiz\u00f3 prioridad del trabajo",
				ClientUtils.INFO);

	}
}
