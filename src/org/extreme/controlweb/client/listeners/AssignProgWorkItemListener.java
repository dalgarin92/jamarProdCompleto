package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.TimeField;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class AssignProgWorkItemListener extends BaseItemListenerAdapter {

	private ControlWebGUI gui;
	private String codtrab;
	private String ttrab;
	private String fechaentrega;
	private Function afterFunction;
	private String horaentrega;
	private String idjornada;

	public AssignProgWorkItemListener(ControlWebGUI gui, Trabajo trab) {
		this.gui = gui;
		this.codtrab = trab.getId();
		this.ttrab = trab.getTpoTrab().getId();
		this.fechaentrega = trab.getFechaEntrega();
		this.horaentrega = trab.getHoraEntrega();
		this.idjornada = trab.getCodJornada();
	}

	public AssignProgWorkItemListener(ControlWebGUI gui, String codtrab, String ttrab, String fechaentrega, String horaentrega, String idjornada, Function afterFunction) {
		this.gui = gui;
		this.codtrab = codtrab;
		this.ttrab = ttrab;
		this.fechaentrega = fechaentrega;
		this.afterFunction = afterFunction;
		this.horaentrega = horaentrega;
		this.idjornada = idjornada;
	}
	
	@Override
	public void onClick(BaseItem item, EventObject e) {
		final Window wnd = new Window("Asignar Programaci\u00f3n", 300, 150, false, true);
		wnd.setLayout(new FitLayout());
		wnd.setButtonAlign(Position.CENTER);
		
		Panel p = new Panel();
		p.setBorder(false);
		p.setPaddings(5);
		p.setLayout(new FormLayout());
		
		final DateField df = new DateField("Fecha Entrega", "Y/m/d");
		
		if(fechaentrega != null && !fechaentrega.equals(""))
			df.setValue(fechaentrega);
		/*else
			df.setValue(new Date());*/
		
		p.add(df);
		
		final TimeField tf = new TimeField("Hora Entrega");
		tf.setTypeAhead(true);
		tf.setForceSelection(true);
		tf.setFormat("H:i");
		tf.setIncrement(5);
		tf.setMinValue("08:00");
		tf.setMaxValue("22:00");
		
		if(horaentrega != null && !horaentrega.equals(""))
			tf.setValue(horaentrega);
		
		final MyComboBox cbJornada = new MyComboBox("Jornada");
		cbJornada.setEditable(false);
		cbJornada.setForceSelection(true);
		cbJornada.setDisabled(true);
		gui.getQueriesServiceProxy().getJornadas(
				new RPCFillComplexComboHandler(cbJornada, p, 1, new Function() {
					@Override
					public void execute() {
						cbJornada.getStore().sort("id");
						if(idjornada != null)
							cbJornada.setValue(idjornada);
					}
				}));
		
		p.add(tf);
		wnd.add(p);
		
		Button btn = new Button("Asignar", new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {
				gui.getQueriesServiceProxy().assignProg(
						df.getEl().getValue(),
						cbJornada.getValue(),
						tf.getEl().getValue(),
						codtrab,
						ttrab,
						new MyBooleanAsyncCallback("Programaci\u00f3n Asignada.", new Function(){
							@Override
							public void execute() {
								if(afterFunction != null)
									afterFunction.execute();
								wnd.close();
							}
						}));
			}
		});
		
		wnd.addButton(btn);
		
		wnd.show();
	}
}
