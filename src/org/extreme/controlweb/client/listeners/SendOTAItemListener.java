package org.extreme.controlweb.client.listeners;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.Position;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.TextArea;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.layout.AnchorLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.FormLayout;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class SendOTAItemListener extends BaseItemListenerAdapter {

	private TreeNode treeNode;
	private ControlWebGUI gui;
	private boolean isPosAct;

	private TextField tfOther;
	private MyComboBox cbCmd;
	private Window wnd;
	private String recurso;
	private String contratista;

	public SendOTAItemListener(ControlWebGUI gui, TreeNode treeNode,
			boolean isPosAct) {
		this.gui = gui;
		this.treeNode = treeNode;
		this.isPosAct = isPosAct;
		
	}
	
	public void onClick(BaseItem item, EventObject e) {
		Recurso rec = (Recurso) treeNode.getAttributeAsObject("entity");
		recurso = rec.getId();
		contratista = rec.getCont().getId();
		if(isPosAct){			
			gui.showMessage("Por favor espere...", "Enviando comando OTA...", Position.CENTER);
			
			gui.getQueriesServiceProxy().sendOtaPosActual(recurso, contratista, new AsyncCallback<String>() {
				@Override
				public void onSuccess(String result) {
					gui.hideMessage();
					String split[] = result.split("@");
					if(split[0].equals("ERROR")) {
						ClientUtils.alert("Error", split[1], ClientUtils.ERROR);
					} else
						new NodeTreeClickListener(gui, true).updateRecurso(treeNode);
				}
				
				@Override
				public void onFailure(Throwable caught) {
					gui.hideMessage();
							ClientUtils.alert("Error",
									"Error al enviar OTA de act. de posici\u00f3n: "
											+ caught.getMessage(),
									ClientUtils.ERROR);
							GWT.log("Error al enviar OTA de act. de posici\u00f3n...",
											caught);
				}
			});
		} else {
			wnd = new Window("Enviar Comando OTA", true, false);
			wnd.setLayout(new FormLayout());
			wnd.setPaddings(5);
			wnd.setHeight(125);
			wnd.setWidth(300);
			wnd.setButtonAlign(Position.CENTER);
			
			cbCmd = new MyComboBox("Comando");
			
			gui.getQueriesServiceProxy().getOTACommands(recurso,
					contratista, new RPCFillComplexComboHandler(cbCmd, wnd, 0, new Function() {
						@Override
						public void execute() {
							cbCmd.getStore().add(cbCmd.getRecordDef().createRecord(new String[]{"other", "Otro..."}));
						}
					}));
						
			tfOther = new TextField("Otro");
			tfOther.setDisabled(true);
			cbCmd.addListener(new ComboBoxListenerAdapter(){
				public void onSelect(ComboBox comboBox,
						Record record, int index) {
					tfOther.setValue("");
					if(record.getAsString("id").equals("other"))
						tfOther.setDisabled(false);
					else 
						tfOther.setDisabled(true);									
				}
			});
			Button btnOK = new Button("Enviar", new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
					
					AsyncCallback<String> otaCallback = new AsyncCallback<String>(){
						@Override
						public void onFailure(Throwable caught) {
							ClientUtils.alert("Error", "Error al enviar comando OTA: " + caught.getMessage(), ClientUtils.ERROR);
							GWT.log("Error al enviar comando OTA...", caught);
							gui.hideMessage();
						}

						@Override
						public void onSuccess(String result) {
							gui.hideMessage();
							String split[] = result.split("@");
							if (!split[0].equals("ERROR")) {
								Window wnd = new Window("Respuesta Comando OTA", true, true);
								wnd.setLayout(new FitLayout());
								wnd.setPaddings(5);
								wnd.setHeight(200);
								wnd.setWidth(300);
								TextArea ta = new TextArea();
								ta.setHideLabel(true);
								ta.setReadOnly(true);
								ta.setValue(split[1]);
								wnd.add(ta);
								wnd.show();
							} else
								ClientUtils.alert("Error", split[1], ClientUtils.ERROR);
						}
						
					};
					
					gui.showMessage("Por favor espere...", "Enviando comando OTA...", Position.CENTER);
					if(!cbCmd.getValue().equals("other"))
						gui.getQueriesServiceProxy().sendOTAPredef(
								recurso, contratista, cbCmd.getValue(),
								otaCallback);
					
					else 
						gui.getQueriesServiceProxy().sendOTACustom(
								recurso, contratista, tfOther.getText(),
								otaCallback);
					wnd.close();					
				}
			});
			
			Button btnCancel = new Button("Cancelar", new ButtonListenerAdapter(){
				public void onClick(Button button, EventObject e) {
					wnd.close();
				}
			});
			wnd.add(tfOther, new AnchorLayoutData("100%"));
			wnd.addButton(btnOK);
			wnd.addButton(btnCancel);
			
			wnd.show();
		}
	}

}
