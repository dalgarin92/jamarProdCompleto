package org.extreme.controlweb.client.listeners;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.handlers.RPCFillTable;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Tool;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.tree.TreeNode;

public class PrioritiesAssignListener extends BaseItemListenerAdapter{

	private ControlWebGUI gui;
	private TreeNode selectedNode;
	private Recurso rec;
	private Window wdmultiJobsClose;
	private MyGridPanel grid;

	public PrioritiesAssignListener(ControlWebGUI gui, TreeNode treeNode,
			Recurso rec) {
		this.gui = gui;
		this.selectedNode = treeNode;
		this.rec = rec;
	}
	Function f = new Function() {

		@Override
		public void execute() {
			ClientUtils.alert("Informaci\u00f3n","Se asignaron las prioridades.",ClientUtils.INFO);
			gui.reloadTree(true, true);

		}

	};
	@Override
	public void onClick(BaseItem item, EventObject e) {

		grid = new MyGridPanel();

		if (grid.isRendered())
			grid.getEl().mask("Por favor, espere...");
		wdmultiJobsClose = new Window("Asignar Prioridades.", 595, 400,false, true);
		wdmultiJobsClose.addTool(new Tool(Tool.HELP, new Function(){
			@Override
			public void execute() {
				ClientUtils.alert("Ayuda?", "Para asignar las prioridades, Se debe hacer click derecho sobre las filas de cada trabajo <br/>" +
						"se desplegar el men\u00fa  de asignaci\u00f3n y eliminaci\u00f3n de prioridades... <br/>" ,ClientUtils.INFO);
			}
		}));
		final Panel P = new Panel();
		P.setLayout(new FitLayout());
		wdmultiJobsClose.setLayout(new FitLayout());

		Button jobsClos = new Button("Aceptar");

		jobsClos.addListener(new ButtonListenerAdapter() {
			private boolean sw;

			@Override
			public void onClick(Button button, EventObject e) {
				Record trabajos[] = grid.getStore().getRecords();
				ArrayList<String[]> trabajosArray = new ArrayList<String[]>();
				for (int i = 0; i < trabajos.length; i++){
					trabajosArray.add(new String[] {trabajos[i].getAsString("Trabajo"),trabajos[i].getAsString("Prioridad")});

					if(trabajos[i].getAsString("Prioridad").contains("NINGUNA")){
						ClientUtils.alert("Info!", "Debe asignar prioridades a todos los trabajos...", ClientUtils.INFO);

						System.out.println("entro!");
						sw= false;
						break;
					}else{
						sw= true;
					}

				}

				if (sw)
					if (com.google.gwt.user.client.Window.confirm("Realmente desea asignar las Prioridades? ")) {

						gui.getQueriesServiceProxy().asignarPrioridad(trabajosArray,
								new AsyncCallback<Boolean>() {

							@Override
							public void onFailure(Throwable caught) {
								ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
								GWT.log("Error RPC", caught);
							}

							@Override
							public void onSuccess(Boolean result) {

								Object obj = selectedNode.getAttributeAsObject("entity");
								if (obj instanceof Recurso) {
									NodeTreeClickListener l = new NodeTreeClickListener(gui, true);
									l.updateRecurso(selectedNode);
								}
								wdmultiJobsClose.close();
								f.execute();
							}
						});
					}


				if(sw) 
					wdmultiJobsClose.close();

			}
		});
		Button bttClose = new Button("Cancelar");

		bttClose.addListener(new ButtonListenerAdapter(){
			@Override
			public void onClick(Button button, EventObject e) {

				wdmultiJobsClose.close();
			}

		});
		wdmultiJobsClose.addButton(jobsClos);
		wdmultiJobsClose.addButton(bttClose);

		gui.getQueriesServiceProxy().getWorksPriority(
				PrioritiesAssignListener.this.rec.getId(),
				PrioritiesAssignListener.this.rec.getCont().getId(),
				PrioritiesAssignListener.this.gui.getDfDateProg().getEl()
				.getValue(),
				new RPCFillTable(grid, P, null, false,
						false, true, null, null,
						new RowLayoutData(),new Function() {

					@Override
					public void execute() {


						if(grid.getStore() !=null){
							wdmultiJobsClose.add(P);
							wdmultiJobsClose.show();
						}

						if (grid.isRendered())
							grid.getEl().unmask();
					}

				},"No hay datos!"));


		grid.addGridCellListener(new GridCellListenerAdapter() {
			@Override
			public void onCellContextMenu(final GridPanel grid,
					final int rowIndex, int cellIndex, EventObject e) {
				e.stopEvent();
				Menu m = new Menu();
				Item priority = new Item("Fijar Prioridad");
				Item deletePriorities = new Item("Eliminar Prioridades");
				final Record record = grid.getStore().getAt(rowIndex);


				deletePriorities.addListener(new BaseItemListenerAdapter() {
					@Override
					public void onClick(BaseItem item, EventObject e) {
						Record[] recs = grid.getStore().getRecords();
						for (Record r : recs) {
							r.set("Prioridad", "NINGUNA");
						}

						grid.getStore().commitChanges();

					};
				});

				priority.addListener(new BaseItemListenerAdapter() {
					@Override
					public void onClick(BaseItem item, EventObject e) {
						PrioritiesAssignationListener PAL = new PrioritiesAssignationListener(
								gui, record.getAsString("Trabajo"), record
								.getAsString("Prioridad"), record,
								grid,rec);
						PAL.onClick(item, e);

					}
				});

				m.addItem(priority);
				m.addItem(deletePriorities);
				m.showAt(e.getXY());
			}
		});









	}
}
