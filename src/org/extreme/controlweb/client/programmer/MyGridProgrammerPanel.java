package org.extreme.controlweb.client.programmer;

import java.util.ArrayList;
import java.util.HashMap;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;

import com.google.gwt.user.client.Window;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.GroupingStore;
import com.gwtext.client.data.IntegerFieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.SortState;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.ColorPalette;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarTextItem;
import com.gwtext.client.widgets.form.NumberField;
import com.gwtext.client.widgets.form.TimeField;
import com.gwtext.client.widgets.grid.BaseColumnConfig;
import com.gwtext.client.widgets.grid.CellMetadata;
import com.gwtext.client.widgets.grid.CheckboxColumnConfig;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.ColumnConfig;
import com.gwtext.client.widgets.grid.ColumnModel;
import com.gwtext.client.widgets.grid.EditorGridPanel;
import com.gwtext.client.widgets.grid.GridEditor;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.GroupingView;
import com.gwtext.client.widgets.grid.Renderer;
import com.gwtext.client.widgets.grid.RowNumberingColumnConfig;
import com.gwtext.client.widgets.grid.RowSelectionModel;
import com.gwtext.client.widgets.grid.event.GridCellListener;
import com.gwtext.client.widgets.grid.event.GridCellListenerAdapter;
import com.gwtext.client.widgets.grid.event.RowSelectionListenerAdapter;
import com.gwtext.client.widgets.menu.ColorMenu;
import com.gwtext.client.widgets.menu.event.ColorMenuListener;

public class MyGridProgrammerPanel extends EditorGridPanel {

	private RecordDef rd;
	private boolean configured;
	private int delIndex = -1;
	private int modIndex = -1;
	private int detIndex = -1;
	private int downIndex = -1;
	private int upIndex = -1;
	@SuppressWarnings("unused")
	private int numIndex = -1;
	private int maxControlColumn = -1;
	private boolean[] visible;
	public static final int ALL_COLUMNS = -1;

	private HashMap<String, ArrayList<MyGridOnCellClickListener>> listeners =
			new HashMap<String, ArrayList<MyGridOnCellClickListener>>();
	private ArrayList<Integer> coloreableColumns = null;
	private ArrayList<String> fieldNames = null;

	private MyGridOnCellClickListener deleteFunction;
	private MyGridOnCellClickListener modFunction;
	private MyGridOnCellClickListener detFunction;
	private MyGridOnCellClickListener reorderFunction;

	private GridCellListener listener;
	private String reorderFieldName;
	private ToolbarTextItem size;
	private String label;
	
	private boolean grouped = false;
	private String sortField;
	private String groupField;
	private GroupingStore groupingStore;
	private ToolbarTextItem selected;
	private boolean manualSort = false;
	private ArrayList<RowModel> result;
	private NumberField nfOrden;
	

	public ArrayList<RowModel> getResult() {
		return result;
	}

	public void setResult(ArrayList<RowModel> result) {
		this.result = result;
	}

	public boolean isManualSort() {
		return manualSort;
	}

	public void setManualSort(boolean manualSort) {
		this.manualSort = manualSort;
	}

	private  static String HTML_MODIFY = "<img class=\"mod\" title=\"Modificar registro\" src=\"images/utiles/icons/edit.png\"/>";
	private static final String HTML_DELETE = "<img class=\"elim\" title=\"Eliminar registro\" src=\"images/utiles/icons/delete.png\"/>";
	private static final String HTML_DOWN = "<img class=\"elim\" title=\"Disminuir\" src=\"images/icons/arrow-down.gif\"/>";
	private static final String HTML_UP = "<img class=\"elim\" title=\"Aumentar\" src=\"images/icons/arrow-up.gif\"/>";

	public static final Renderer MODIFY_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			return HTML_MODIFY;
		}

	};

	public static final Renderer DOWN_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			// if(store.getRecords().length-1!=rowIndex)
			return HTML_DOWN;
			// else return "";
		}

	};

	public static final Renderer UP_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			if (rowIndex != 0)
				return HTML_UP;
			else
				return "";
		}

	};

	public static final Renderer DELETE_RENDERER = new Renderer() {

		@Override
		public String render(Object value, CellMetadata cellMetadata,
				Record record, int rowIndex, int colNum, Store store) {
			return HTML_DELETE;
		}

	};

	public static final Renderer getDetailsRenderer(String string) {
		final String s = string;
		return new Renderer() {
			@Override
			public String render(Object value, CellMetadata cellMetadata,
					Record record, int rowIndex, int colNum, Store store) {
				
				if(record.getAsString("Georreferenciado").equals("S")){
					
					return "<img class=\"attach\" title=\"" + s
							+ "\" src=\"images/utiles/icons/gpin.png\"/>";
				}
							
				return "<img class=\"attach\" title=\"" + s
					+ "\" src=\"images/utiles/icons/rpin.png\"/>";
			}
		};
	}

	public void setColoreableColumn(int i) {
		coloreableColumns.add(new Integer(i));
	}
	public MyGridProgrammerPanel() {

		super();
		configured = false;
		coloreableColumns = new ArrayList<Integer>();
		listener = new GridCellListenerAdapter() {
			@Override
			public void onCellClick(GridPanel grid, int rowIndex, int colindex,
					EventObject e) {
				boolean controlAction = false;
				if (delIndex >= 0) {
					if (colindex == delIndex) {
						controlAction = true;
						if (deleteFunction != null) {
							if (Window.confirm("Est\u00e1 seguro de borrar/desactivar este registro?"))
								deleteFunction.onCellClick(getStore()
										.getRecordAt(rowIndex));
						} else {
							getStore().remove(getStore().getRecordAt(rowIndex));
							getStore().commitChanges();
						}
					}
				}
				if (modIndex >= 0) {
					if (colindex == modIndex) {
						controlAction = true;
						if (modFunction != null) {
							modFunction.onCellClick(getStore().getRecordAt(
									rowIndex));
						}
					}
				}
				if (detIndex >= 0) {
					if (colindex == detIndex) {
						controlAction = true;
						if (detFunction != null) {
							detFunction.onCellClick(getStore().getRecordAt(
									rowIndex));
						}
					}
				}
				if (downIndex >= 0) {
					if (colindex == downIndex) {
						controlAction = true;
						if (reorderFieldName != null) {
							int maxOrderIndex = getStore() != null ? getStore()
									.getRecords() != null ? getStore()
											.getRecords().length : 0 : 0;
							if (rowIndex != maxOrderIndex - 1) {
								
								Record r = getStore().getRecordAt(rowIndex);
								Record r2 = getStore().getRecordAt(rowIndex + 1);
								
								String h1=r.getAsString("Hora");
								String h2=r2.getAsString("Hora");
								
								String order1= r.getAsString("Orden");
								String order2= r2.getAsString("Orden");
								
								
								r2.set(reorderFieldName, order1);
								r.set(reorderFieldName, order2);
								
								r2.set("Hora", h1);
								r.set("Hora", h2);
								
								
								
								getStore().commitChanges();
								getStore().sort(reorderFieldName, SortDir.ASC);
								if (reorderFunction != null) {
									reorderFunction.onCellClick(r);
									reorderFunction.onCellClick(r2);
								}

							}
						}
					}
				}
				if (upIndex >= 0) {
					if (colindex == upIndex) {
						controlAction = true;
						if (reorderFieldName != null) {
							if (rowIndex != 0) {
								Record r = getStore().getRecordAt(rowIndex);							
								Record r2 = getStore().getRecordAt(rowIndex - 1);
								//int orderIndex = Integer.parseInt(r.getAsString(reorderFieldName));
								
								String h1=r.getAsString("Hora");
								String h2=r2.getAsString("Hora");
								
								String order1= r.getAsString("Orden");
								String order2= r2.getAsString("Orden");
								
								
								r2.set(reorderFieldName, order1);
								r.set(reorderFieldName, order2);
		
								r2.set("Hora", h1);
								r.set("Hora", h2);
								
								getStore().commitChanges();
								getStore().sort(reorderFieldName, SortDir.ASC);
								if (reorderFunction != null) {
									reorderFunction.onCellClick(r);
									reorderFunction.onCellClick(r2);
								}
							}
						}
					}
				}
				if (colindex >= maxControlColumn) {
					ArrayList<MyGridOnCellClickListener> l = listeners.get(colindex + "");
					if (l != null) {
						for (MyGridOnCellClickListener myGridOnCellClickListener : l) {
							myGridOnCellClickListener.onCellClick(getStore()
									.getRecordAt(rowIndex));
						}
					}
				}
				if (!controlAction) {
					ArrayList<MyGridOnCellClickListener> l = listeners
							.get(ALL_COLUMNS + "");
					if (l != null) {
						for (MyGridOnCellClickListener myGridOnCellClickListener : l) {
							myGridOnCellClickListener.onCellClick(getStore()
									.getRecordAt(rowIndex));
						}
					}
				}
			}
		};
		this.addGridCellListener(listener);
		
	}

	public void addRow(RowModel rm) {
		this.getStore().add(rd.createRecord(rm.getRowValues()));
		this.getStore().commitChanges();
	}

	public void addListener(MyGridOnCellClickListener l, int i) {
		assert i >= maxControlColumn : " ";
		if (i >= maxControlColumn) {
			if (listeners.get(i + "") == null) {
				ArrayList<MyGridOnCellClickListener> a = new ArrayList<MyGridOnCellClickListener>();
				listeners.put(i + "", a);
			}
			ArrayList<MyGridOnCellClickListener> a = listeners.get(i + "");
			a.add(l);
		}
	}

	public void addDeleteListener(MyGridOnCellClickListener m) {
		deleteFunction = m;
	}

	public void addModListener(MyGridOnCellClickListener m) {
		modFunction = m;
	}

	public void addDetListener(MyGridOnCellClickListener m) {
		detFunction = m;
	}

	public void addReorderListener(MyGridOnCellClickListener m) {
		reorderFunction = m;
	}

	/**
	 * @param columnNames
	 * @param details
	 * @param modify
	 * @param delete
	 * @param numerate
	 * @param columnSizes
	 * @param visible
	 * @param reorderFieldName
	 * @param csm
	 * @param cbfieldName
	 */
	public void configure(String[] columnNames, String details, boolean modify,
			boolean delete, boolean numerate, int[] columnSizes,
			boolean[] visible, String reorderFieldName, 
			final CheckboxSelectionModel csm, String cbfieldName) {
		
		ArrayList<FieldDef> fd = new ArrayList<FieldDef>();
		ArrayList<BaseColumnConfig> bcc = new ArrayList<BaseColumnConfig>();

		this.reorderFieldName = reorderFieldName;
		this.visible =visible;
		int i = 0;

		if (csm != null) {
			bcc.add(null);
			i++;
		}
		if (details != null) {
			bcc.add(new ColumnConfig(" ", null, 25, true,
					getDetailsRenderer(details), "attach"));
			detIndex = i;
			i++;
		}
		if (modify) {
			bcc.add(new ColumnConfig(" ", null, 25, true, MODIFY_RENDERER,
					"mod"));
			modIndex = i;
			i++;
		}
		if (delete) {
			bcc.add(new ColumnConfig(" ", null, 25, true, DELETE_RENDERER,
					"del"));
			delIndex = i;
			i++;
		}

		if (reorderFieldName != null) {
			bcc.add(new ColumnConfig(" ", null, 25, true, UP_RENDERER, "up"));
			upIndex = i;
			i++;
			bcc
			.add(new ColumnConfig(" ", null, 25, true, DOWN_RENDERER,"down"));
			downIndex = i;
			i++;
		}
		if (numerate) {
			if (manualSort) {
					nfOrden = new NumberField();
					nfOrden.setMinValue(1);
					nfOrden.setAllowBlank(false);
					nfOrden.setBlankText("El ordenamiento es obligatorio");
					nfOrden.setAllowDecimals(false);
					nfOrden.setAllowNegative(false);
										
					ColumnConfig cc = new ColumnConfig("Orden", "Orden", 60,true);
					cc.setEditor(new GridEditor(nfOrden));
					bcc.add(cc);
					numIndex = i;
					i++;
				
				
			} else {
				bcc.add(new RowNumberingColumnConfig());
				numIndex = i;
				i++;
			}			
		}
		
	
		setClicksToEdit(1); 
		setTrackMouseOver(false);
		setShim(false);
		
		maxControlColumn = i;
		fieldNames = new ArrayList<String>();
		boolean sortable = reorderFieldName == null;
		for (int j = 0; j < columnNames.length; j++) {
			if(columnNames[j].equals("Orden")){
				
				fd.add(new IntegerFieldDef(columnNames[j]));
			}else{
				fd.add(new StringFieldDef(columnNames[j]));
			}
			if (visible != null) {
				if (visible[j]) {
					int size = 100;
					if (columnSizes != null) {
						size = columnSizes[j];
						
					}
					if(cbfieldName!=null&&columnNames[j].equals(cbfieldName)){
							TimeField myTime = new TimeField();
							myTime.setFormat("H:i");
							myTime.setIncrement(15);
										
					        ColumnConfig lightCol = new ColumnConfig(columnNames[j], columnNames[j], 60);  
					        lightCol.setEditor(new GridEditor(myTime)); 
					        bcc.add(lightCol);

						}else{
							bcc.add(new ColumnConfig(columnNames[j], columnNames[j],
							size, sortable));
						}
					
					
					fieldNames.add(columnNames[j]);
				}
			} else {
				if(cbfieldName!=null&&columnNames[j].equals(cbfieldName)){
					  
					TimeField myTime = new TimeField();
					myTime.setIncrement(15);
					myTime.setFormat("H:i");
			        ColumnConfig lightCol = new ColumnConfig(columnNames[j], columnNames[j], 60);  
			        lightCol.setEditor(new GridEditor(myTime)); 
			        bcc.add(lightCol);

				}else
				bcc.add(new ColumnConfig(columnNames[j], columnNames[j], 100,
						true));
			}
		}

		ColumnModel cm = new ColumnModel(toBaseColumnConfigArray(bcc, csm));
		rd = new RecordDef(toFieldDefArray(fd));
		cm.setDefaultSortable(true);
		Store s = null;
		if(!isGrouped()) {
			s = new Store(rd);
			
		} else {
			setStripeRows(true);
			s = new GroupingStore(rd);   
			if(sortField != null)
				s.setSortInfo(new SortState(sortField, SortDir.ASC));
			if(groupField != null)
				((GroupingStore)s).setGroupField(groupField);
			GroupingView gridView = new GroupingView(); 
			//gridView.setForceFit(true);
	
			gridView.setHideGroupedColumn(true);
			gridView.setGroupTextTpl("{text} ({[values.rs.length]} {[values.rs.length > 1 ? " +
					"\"Trabajos\" : \"Trabajos\"]})");  

			setView(gridView);  
		}
		if (s.getJsObj() != null) {
			this.setStore(s);
		}		
		this.setColumnModel(cm);
		this.setEnableHdMenu(false);
	
		if(csm != null)
		{
			csm.addListener(new RowSelectionListenerAdapter(){
				private boolean sw = true;
				private Record[] selectedRecords;
				@Override
				public void onRowSelect(RowSelectionModel sm, int rowIndex,
						Record record) {
					if(selectedRecords != null && !csm.isSelected(selectedRecords[0]) && sw)
					{
						sw = false;
						csm.selectRecords(selectedRecords);
						sw = true;
					}
				}
				@Override
				public boolean doBeforeRowSelect(RowSelectionModel sm,
						int rowIndex, boolean keepExisting, Record record) {
					if(sw){
						int size = csm.getSelections().length + 1;
						selectedRecords = new Record[size];
						Record[] temp = csm.getSelections();
						for (int k = 0; k < temp.length; k++) {
							selectedRecords[k] = temp[k];
						}
						selectedRecords[temp.length] = record;
					}
					return true;
				}
			});
			this.setSelectionModel(csm);
		}
		this.addGridCellListener(new GridCellListenerAdapter() {
			@Override
			public void onCellClick(final GridPanel grid, final int rowIndex,
					final int colIndex, EventObject e) {
				if (coloreableColumns.contains(new Integer(colIndex
						- maxControlColumn))) {
					ColorMenu colormenu = new ColorMenu();
					colormenu.addListener(new ColorMenuListener() {
						@Override
						public void onSelect(ColorPalette colorPalette,
								String color) {
							String cn = fieldNames.get(colIndex
									- maxControlColumn);
							grid
							.getStore()
							.getRecordAt(rowIndex)
							.set(
									cn,
									"<div width='100%' value='"
											+ color
											+ "' style='background-color:"
											+ color
											+ ";color:#"
											+ color
											+ "; background-image:none;'>&nbsp;</div>");
						}
					});
					colormenu.showAt(e.getPageX(), e.getPageY());
				}
			}
		});
		this.setEnableColumnMove(!(coloreableColumns.size() > 0));
		configured = true;
	}

	/*private String[][] getStoreManualSort(ArrayList<RowModel> store){
		
		String[][] data = new String[store.size()][2]; 
		
		for (int j = 0; j < store.size(); j++) {
			Object [] record = store.get(j).getRowValues();
			if (record.length > 0) {
				data[j][1]= String.valueOf((record[11]));
				data[j][0]= ClientUtils.lpad(String.valueOf((record[11])), "0", 6);	
			}					
		}
		
		//configured = false;
		return data;
	}*/
	
	public String getColorValue(Record r, String field) {
		String result = null;
		if (fieldNames.contains(field)) {
			if (coloreableColumns.contains(new Integer(fieldNames
					.indexOf(field)))) {
				String f = r.getAsString(field);
				if (!f.equals("")) {
					String sp1 = f.split("value='")[1];
					result = sp1.split("' style='")[0];
				} else {
					return "FFFFFF";
				}
			}
		}
		return result;
	}

	public void loadRows(ArrayList<RowModel> rows,boolean sw) {
		if (getStore() != null) {
			getStore().removeAll();

			for (RowModel row : rows){
				Record r = rd.createRecord(row.getRowValues());
				if(reorderFieldName != null)
					/*if(sw)
					r.set(reorderFieldName, ClientUtils.lpad(r
							.getAsString(reorderFieldName) == null
							|| r.getAsString(reorderFieldName).equals("") ? "0"
									: r.getAsString(reorderFieldName), "0", 6));
					 	*/
					this.getStore().add(r);
				
			}
			if(size!=null)
				size.setText(getStore().getRecords().length +" "+(label!=null?label:"filas"));;
			this.getStore().commitChanges();
			doLayout();			
		}
	}

	private BaseColumnConfig[] toBaseColumnConfigArray(
			ArrayList<BaseColumnConfig> bcc, CheckboxSelectionModel csm) {
		BaseColumnConfig[] a = new BaseColumnConfig[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			if(i == 0 && csm != null)
				a[i] = new CheckboxColumnConfig(csm);
			else
				a[i] = bcc.get(i);
		}
		return a;
	}

	private FieldDef[] toFieldDefArray(ArrayList<FieldDef> bcc) {
		FieldDef[] a = new FieldDef[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			a[i] = bcc.get(i);
		}
		return a;
	}

	public boolean isConfigured() {
		return configured;
	}
	public void setConfigured(boolean configured) {
		 this.configured=configured;
	}
	public ArrayList<RowModel> getRowData(){
		ArrayList<RowModel> rm = new ArrayList<RowModel>();
		if (getStore()!=null){
			
		Record[] recs = getStore().getRecords();		
		
		for (Record rec : recs) {
			RowModelAdapter rma = new RowModelAdapter();			
			for (int i = 0; i < rec.getFields().length; i++) 
				rma.addValue(rec.getFields()[i], rec.getAsString(rec.getFields()[i]));
			rm.add(rma);
		}
		return rm;
		}
		return null;

	}

	public ArrayList<String> getFieldNames() {
		return fieldNames;
	}

	public static String getHTML_MODIFY() {
		return HTML_MODIFY;
	}

	public void setHTML_MODIFY(String hTML_MODIFY) {
		HTML_MODIFY = hTML_MODIFY;
	}
	
	
	public void addBottomToolbar(final String label) {

		this.label = label;
		
		Toolbar bootomToolBar = new Toolbar();

		selected = new ToolbarTextItem("0 "+(label!=null?label:"filas")+" seleccionados(as)");
		bootomToolBar.addItem(selected);

		bootomToolBar.addFill();

		size = new ToolbarTextItem("0 "+(label!=null?label:"filas"));
		bootomToolBar.addItem(size);		

		getSelectionModel().addListener(new RowSelectionListenerAdapter() {

			@Override
			public void onRowDeselect(RowSelectionModel sm, int rowIndex,
					Record record) {
				selected.setText(sm.getCount() +" "+(label!=null?label:"filas")+" seleccionados(as)");

			}
			@Override
			public void onRowSelect(RowSelectionModel sm, int rowIndex,
					Record record) {
				selected.setText(sm.getCount() +" "+ (label!=null?label:"filas")+" seleccionados(as)");

			}
	
		});
		
		setBottomToolbar(bootomToolBar);
	
	}
	
	
	public boolean isGrouped() {
		return grouped;
	}

	public void setGrouped(boolean grouped) {
		this.grouped = grouped;
	}

	public void setGroupField(String groupField) {
		this.groupField = groupField;
	}

	public String getGroupField() {
		return groupField;
	}
	
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortField() {
		return sortField;
	}


	public GroupingStore getGroupingStore() {
		return groupingStore;
	}

	public void setGroupingStore(GroupingStore groupingStore) {
		this.groupingStore = groupingStore;
	}
	
	
	public void refreshLabel(){
		selected.setText(getSelectionModel().getCount() +" "+(label!=null?label:"filas")+" seleccionados(as)");
		size.setText(getStore().getRecords().length +" "+(label!=null?label:"filas"));;
	}

	public boolean[] getVisible() {
		return visible;
	}

	public void setVisible(boolean[] visible) {
		this.visible = visible;
	}
	
	private static boolean exists(Store s,String record, String text){
		try {
			Record[] r =  s.getRecords();
			for (Record b : r) {
				if(b.getAsString(record)!=null) {
					if(b.getAsString(record).equals(text)) {
						return true;
					}
				}
			}
			return false;

		} catch (Exception e) {
			return false;
		}

	}
	
}