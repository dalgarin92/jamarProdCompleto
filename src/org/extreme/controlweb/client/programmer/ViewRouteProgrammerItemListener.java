package org.extreme.controlweb.client.programmer;

import java.util.ArrayList;
import java.util.Date;

import org.extreme.controlweb.client.core.Horario;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.SimpleEntity;
import org.extreme.controlweb.client.core.SimpleEntityAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.handlers.MyBooleanAsyncCallback;
import org.extreme.controlweb.client.handlers.RPCFillComboBoxHandler;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.handlers.RPCFillTableProgrammer;
import org.extreme.controlweb.client.programmer.listener.GeoOrderListener;
import org.extreme.controlweb.client.programmer.listener.GeoreferenceListener;
import org.extreme.controlweb.client.programmer.listener.RouteStartPointLinstener;
import org.extreme.controlweb.client.programmer.listener.SeeRouteListener;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.RegionPosition;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StoreTraversalCallback;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.MessageBox;
import com.gwtext.client.widgets.MessageBoxConfig;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.ToolbarTextItem;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.DateField;
import com.gwtext.client.widgets.form.event.ComboBoxListenerAdapter;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.event.EditorGridListenerAdapter;
import com.gwtext.client.widgets.layout.BorderLayout;
import com.gwtext.client.widgets.layout.BorderLayoutData;
import com.gwtext.client.widgets.layout.FitLayout;
import com.gwtext.client.widgets.layout.RowLayout;
import com.gwtext.client.widgets.layout.RowLayoutData;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.CheckItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.MenuItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;
import com.gwtext.client.widgets.menu.event.CheckItemListenerAdapter;

public class ViewRouteProgrammerItemListener extends BaseItemListenerAdapter {

	private String type;
	private DateField date;
	private Recurso resource;
	private ControlWebGUI gui;
	private Panel worksPanel;
	private Panel resourceWorksPanel;
	private MyGridProgrammerPanel worksGrid;
	private MyGridProgrammerPanel resourceWorksGrid;
	private CheckItem filter;
	private CheckItem grupo;
	private CheckItemListenerAdapter Filterlistener;
	private CheckItemListenerAdapter groupedlistener;
	//private static String  HOUR_FORMAT = "HH:mm";
	//private String dateMyItem;
	private boolean isWorkOrdered = false;
	private Panel container;
	
	private boolean save = true;
	
	public DateField getDate() {
		return date;
	}

	public CheckItem getFilter() {
		return filter;
	}

	public CheckItem getGrupo() {
		return grupo;
	}

	public void setDate(DateField date) {
		this.date = date;
	}

	public Recurso getResource() {
		return resource;
	}

	public void setResource(Recurso resource) {
		this.resource = resource;
	}

	public ViewRouteProgrammerItemListener(ControlWebGUI gui) {
		this.gui = gui;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ControlWebGUI getGui() {
		return gui;
	}

	public void setGui(ControlWebGUI gui) {
		this.gui = gui;
	}

	public MyGridProgrammerPanel getWorksGrid() {
		return worksGrid;
	}

	public void setWorksGrid(MyGridProgrammerPanel worksGrid) {
		this.worksGrid = worksGrid;
	}

	public MyGridProgrammerPanel getResourceWorksGrid() {
		return resourceWorksGrid;
	}

	public void setResourceWorksGrid(MyGridProgrammerPanel resourceWorksGrid) {
		this.resourceWorksGrid = resourceWorksGrid;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		
		filter = null;
		grupo = null;
		worksPanel = null;
		worksGrid = null;
		resourceWorksGrid = null;
		resourceWorksPanel = null;
		resource = null;
		buildPanel();
	}

	private void buildPanel() {


		gui.getQueriesServiceProxy().getResourcesToSchedule(
				gui.getProceso().getId(),
				new AsyncCallback<ArrayList<Recurso>>() {

					@Override
					public void onSuccess(ArrayList<Recurso> result) {

						container = new Panel("Programador");
						container.setLayout(new FitLayout());
						container.setClosable(true);
						container.setBodyBorder(false);
						container.setBorder(false);

						Panel borderPanel = new Panel();
						borderPanel.setLayout(new BorderLayout());
						borderPanel.setBorder(false);

						worksPanel = new Panel();

						ArrayList<SimpleEntity> data = new ArrayList<SimpleEntity>();
						data.add(new SimpleEntityAdapter("asig", "Asignados"));
						data.add(new SimpleEntityAdapter("noAsig", "No asignados"));
						
						worksGrid = BuildWorksGrid(null);
						worksPanel.setTopToolbar(createToolbar(data, "Trabajos", true));
						// worksPanel.add(new Toolbar());
						worksPanel.setLayout(new RowLayout());
						worksPanel.setWidth(500);

						BorderLayoutData westLayout = new BorderLayoutData(RegionPosition.WEST);
						westLayout.setMinWidth(500);
						westLayout.setMinSize(500);

						borderPanel.add(worksPanel, westLayout);

						resourceWorksPanel = new Panel();

						createResourcesWorksGrid();
						
						resourceWorksPanel.setTopToolbar(createToolbarPanelResource(result,
								"Recursos", false));
											
						resourceWorksPanel.setHeader(false);
						resourceWorksPanel.setLayout(new RowLayout());

						borderPanel.add(resourceWorksPanel,
								new BorderLayoutData(RegionPosition.CENTER));
						resourceWorksPanel.doLayout();
						container.add(borderPanel);
						
						gui.addInfoTab(container);
						container.doLayout();
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}
				});

	
	}
	
	/**
	 * Crea el grid derecho de los recursos con sus trabajos
	 */
	public void createResourcesWorksGrid(){
		
		resourceWorksGrid = new MyGridProgrammerPanel();
		resourceWorksGrid.setManualSort(true);
		resourceWorksGrid.addEditorGridListener(new EditorGridListenerAdapter(){
			
			@Override
			public void onAfterEdit(GridPanel grid,Record record, String field,
					Object newValue, Object oldValue, int rowIndex, int colIndex) {
			
				if (!field.equals("Orden")) {
					
					if (rowIndex != 0) {
						String[] horaNueva = ((String)newValue).split(" ")[0].split(":");
						String[] horaOld = ((String)grid.getStore().getAt(rowIndex-1).getAsString("Hora")).split(" ")[0].split(":");
						
						if ((Integer.parseInt(horaNueva[0]) > Integer.parseInt(horaOld[0]))) {
																									
							autoHourUpdate((String)newValue, rowIndex, grid);
						
						} else if((Integer.parseInt(horaNueva[0]) == Integer.parseInt(horaOld[0]))){
							
							if ((Integer.parseInt(horaNueva[1]) > Integer.parseInt(horaOld[1]))) {
								
								autoHourUpdate((String)newValue, rowIndex, grid);
							}else{
								
								showErrorMsgAutoUpdate((String)oldValue, rowIndex, grid);
							}									
						}else {

							showErrorMsgAutoUpdate((String)oldValue, rowIndex, grid);
						}		
					}else{
						
						autoHourUpdate((String)newValue, rowIndex, grid);
					}
					
				} else {

					if(!exists(resourceWorksGrid.getStore(),record, String.valueOf(newValue))){
						
						if(Integer.parseInt(String.valueOf(newValue))<= resourceWorksGrid.getStore().getCount()){
							
							interchangeOrder(grid);
							
						}else{
							
							record.set("Orden", "");
							ClientUtils.showError("El ordenamiento supera el n\u00famero de registros");
						}
					}else{
						record.set("Orden", "");
						ClientUtils.showError("Ya existe un registro con este orden: "+ String.valueOf(newValue));
					}
					
				}
				save =false;						
			}
		});
		
		resourceWorksGrid.addDetListener(new GeoreferenceListener(ViewRouteProgrammerItemListener.this, false));

		resourceWorksGrid.setTopToolbar(createGridToolbar(false));

		
		resourceWorksGrid.addReorderListener(new MyGridOnCellClickListener() {

			@Override
			public void onCellClick(Record r) {
				resourceWorksGrid.getView().refresh();

			}
		});
		resourceWorksGrid.addGridRowListener(new RouteStartPointLinstener(ViewRouteProgrammerItemListener.this));
		
		
		
	}
	
	/**
	 * Ejecuta mensaje de error para las horas modificadas
	 * @param oldValue String con la antigua hora del combo
	 * @param index int con la posicion de registro
	 * @param grid GridPanel con todos los datos
	 */
	private void showErrorMsgAutoUpdate(String oldValue, int index, GridPanel grid){
		
		grid.getStore().getAt(index).set("Hora", getFormatHour((String)oldValue));
		ClientUtils.showInfo("Las hora modificada debe ser mayor que la anterior");
	}
	
	/**
	 * Actualiza automaticamente las horas del grid 
	 * @param newValue String con la nueva hora
	 * @param index int con la posicion de registro
	 * @param grid GridPanel con todos los datos
	 */
	private void autoHourUpdate(String newValue, int index, GridPanel grid){
		
		grid.getStore().getAt(index).set("Hora", getFormatHour((String)newValue));
		reCalulateHours(grid, true, index, (String)newValue);
		validateUniqueHourFieldsGrid(resourceWorksGrid, "Hora", index, (String)newValue);
		
		//resourceWorksGrid.getStore().sort("Hora",SortDir.ASC);
		resourceWorksGrid.getStore().commitChanges();
	}
	
	/**
	 * Actualiza el orden de los trabajos
	 * @param newValue
	 * @param oldValue
	 * @param grid
	 */
	private void interchangeOrder(GridPanel grid){

		if (grid != null && grid.getStore() != null) {		
			grid.getStore().sort("Orden", SortDir.ASC);
			calulateHours(grid,true);
			
		}		
	}
	
	/**
	 * Valida un campo dentro de un grid para que su valor sea unico, en caso de encontrar campos repetidos 
	 * les asigna ""
	 * @param grid MyGridProgrammerPanel con los datos que se desean validar
	 * @param field String con el campo que se desea validar
	 */
	private void validateUniqueHourFieldsGrid(MyGridProgrammerPanel grid, String field, int index, String newValue){
				
			Record r = grid.getStore().getAt(index);
			for (int j = 0; j < grid.getStore().getCount(); j++) {
				Record rcmp = grid.getStore().getAt(j);
				if (index != j) {
					if (r.getAsString(field).equals(rcmp.getAsString(field)) || "0".concat(r.getAsString(field)).equals(rcmp.getAsString(field))  ) {
						ClientUtils.showInfo("No pueden existir trabajos con la misma hora");
						grid.getStore().getAt(j).set(field, "");
						reCalulateHours(grid, true, index, newValue);
						grid.getStore().commitChanges();	
						break;
					}					
				}				
			}			
		
	}

	private Toolbar createGridToolbar(boolean left) {

		Toolbar topToolBar = new Toolbar();
		ToolbarButton mainButton = new ToolbarButton("Opciones");

		Menu mainMenu = new Menu();

		if (left) {
			Menu subMenuFilter = new Menu();
			subMenuFilter.setShadow(true);
			subMenuFilter.setMinWidth(10);

			final CheckItem checkGeoreferenceFilter = new CheckItem();		
			final CheckItem checkNotGeoreferenced = new CheckItem();
			final CheckItem checkNothing = new CheckItem();
						

			Filterlistener = new CheckItemListenerAdapter() {
				public void onCheckChange(CheckItem item, boolean checked) {
					String value = null;
					if (item.getText().equalsIgnoreCase("No georeferenciados")
							&& checked) {
						checkGeoreferenceFilter.setChecked(false);
						checkNothing.setChecked(false);
						value = "N";
					} else if (item.getText().equalsIgnoreCase("Georeferenciados")&& checked) {
						value = "S";
						checkNotGeoreferenced.setChecked(false);
						checkNothing.setChecked(false);
					}else{
						checkGeoreferenceFilter.setChecked(false);
						checkNotGeoreferenced.setChecked(false);
		
					}
					filter = item;
					//item.setChecked(checked);
					item.setChecked(false);
					if (value != null)
						worksGrid.getStore().filter("Georreferenciado", value);
					else
						worksGrid.getStore().clearFilter();
					worksGrid.getStore().commitChanges();
					worksGrid.refreshLabel();
					worksGrid.getView().refresh();
				}
			};

			
			checkGeoreferenceFilter.setText("Georeferenciados");
			checkGeoreferenceFilter.addListener(Filterlistener);
			checkGeoreferenceFilter.setGroup("filter");
			subMenuFilter.addItem(checkGeoreferenceFilter);
			
		
			checkNotGeoreferenced.setText("No georeferenciados");
			checkNotGeoreferenced.addListener(Filterlistener);
			checkNotGeoreferenced.setGroup("filter");
			subMenuFilter.addItem(checkNotGeoreferenced);
			
			
			checkNothing.setText("Ninguno");
			checkNothing.addListener(Filterlistener);
			checkNothing.setGroup("filter");
			subMenuFilter.addItem(checkNothing);
			
			
			
			

			MenuItem menuFilter = new MenuItem("Filtrar", subMenuFilter);
			menuFilter.setIconCls("icon-filter-data");
			mainMenu.addItem(menuFilter);

			Menu subMenuGroupBy = new Menu();
			subMenuGroupBy.setShadow(true);
			subMenuGroupBy.setMinWidth(10);

			groupedlistener = new CheckItemListenerAdapter() {
				public void onCheckChange(CheckItem item, boolean checked) {
					if (checked) {
						try {
							worksPanel.remove(worksGrid, true);
						} catch (Exception e) {}
						
						worksGrid = BuildWorksGrid(!item.getText().equals("Ninguno")?item.getText():null);
						reloadLeftGridFilterGrupo(getType(), null);
					}
					grupo = item;
				}
			};
			
			
			CheckItem checkNeighborhood = new CheckItem();
			checkNeighborhood.setText("Barrio");
			//checkNeighborhood.setId("Barrio");
			checkNeighborhood.setGroup("theme");
			checkNeighborhood.addListener(groupedlistener);
			subMenuGroupBy.addItem(checkNeighborhood);

			CheckItem checkCity = new CheckItem();
			checkCity.setText("Municipio");
			//checkCity.setId("Ciudad");
			checkCity.addListener(groupedlistener);
			checkCity.setGroup("theme");
			subMenuGroupBy.addItem(checkCity);

			CheckItem checkGeoreferenced = new CheckItem();
			checkGeoreferenced.setText("Georreferenciado");
			//checkGeoreferenced.setId("Georreferenciado");
			checkGeoreferenced.setGroup("theme");
			checkGeoreferenced.addListener(groupedlistener);
			subMenuGroupBy.addItem(checkGeoreferenced);

			CheckItem checkNothing2 = new CheckItem();
			checkNothing2.setText("Ninguno");
			//checkNothing.setId("Ninguno");
			checkNothing2.addListener(groupedlistener);
			checkNothing2.setGroup("theme");
			subMenuGroupBy.addItem(checkNothing2);

			MenuItem menuItemGroupBy = new MenuItem("Agrupar", subMenuGroupBy);
			menuItemGroupBy.setIconCls("icon-table-information");
			mainMenu.addItem(menuItemGroupBy);

		} else {
			
			Item geoItem = new Item("Geordenar");
			geoItem.addListener(new GeoOrderListener(left, gui, this, false));
			geoItem.setIconCls("icon-map");
			mainMenu.addItem(geoItem);
			
			Item manualSortItem = new Item("Calcular horas de entrega");
			manualSortItem.addListener(new GeoOrderListener(left, gui, this, true));
			manualSortItem.setIconCls("icon-map");
			mainMenu.addItem(manualSortItem);
			
			Item cleanOrderingItem = new Item("Limpiar ordenamientos");
			cleanOrderingItem.addListener(new BaseItemListenerAdapter(){
				@Override
				public void onClick(BaseItem item, EventObject e) {
					
					if(resourceWorksGrid.isRendered()&&resourceWorksGrid.getStore()!=null)
					resourceWorksGrid.getStore().each(new StoreTraversalCallback() {
						
						@Override
						public boolean execute(Record record) {
							 record.set("Orden", "");
							return true;
						}
					});}
			});
			cleanOrderingItem.setIconCls("delete-icon");
			mainMenu.addItem(cleanOrderingItem);
			
			Item routeItem = new Item("Ver ruta");
			routeItem.addListener(new SeeRouteListener(this,gui));
			routeItem.setIconCls("icon-route-map");
			mainMenu.addItem(routeItem);

			Item workingHoursItem = new Item("Horario de trabajo");
			workingHoursItem.addListener(new BaseItemListenerAdapter() {
				@Override
				public void onClick(BaseItem item, EventObject e) {

					final Window wnd = new Window("Horario de trabajo", 230,
							110);
					wnd.setPaddings(10);
					wnd.setResizable(false);

					final MyComboBox cbHorarios = new MyComboBox("Horario");
					gui.getQueriesServiceProxy().getHorariosCombo(
							new RPCFillComplexComboHandler(cbHorarios, wnd, 1,
									new Function() {

										@Override
										public void execute() {
											gui.getQueriesServiceProxy().getResourceHour(resource.getId(),
															resource.getCont().getId(),
															new AsyncCallback<String>() {

																@Override
																public void onSuccess(String result) {
																	if (result != null)
																		cbHorarios.setValue(result);
																	// wnd.add(p);
																	
																	wnd.show();
																}

																@Override
																public void onFailure(Throwable caught) {
																	
																	
																}
															});

										}
									}));

					wnd.addButton(new Button("Guardar",new ButtonListenerAdapter() {
								@Override
								public void onClick(Button button, EventObject e) {

									gui.getStaticAdminQueriesServiceAsync().updateResourceHour(cbHorarios.getValue(),
													resource.getId(),resource.getCont().getId(),
													new MyBooleanAsyncCallback("Horario actualizado",
															new Function() {

																@Override
																public void execute() {
																	
																	gui.getQueriesServiceProxy().getResourceHours(
																			getResource().getId(), 
																			getResource().getCont().getId(),
																			new AsyncCallback<ArrayList<Horario>>() {

																				@Override
																				public void onFailure(Throwable caught) {
																	
																				}

																				@Override
																				public void onSuccess(ArrayList<Horario> result) {
																					getResource().setHorarios(result);
																					calulateHours(resourceWorksGrid,true);
																					
																				}
																				
																				
																			});
																	
																	wnd.close();
																}
															}));
								}
							}));

				}
			});

			workingHoursItem.setIconCls("icon-clock-calendar");
			mainMenu.addItem(workingHoursItem);

		}
		mainButton.setMenu(mainMenu);
		if (left) {
			ToolbarButton worksButton = new ToolbarButton("Asignar trabajos");
			worksButton.setIcon("images/icons/arrow-right.gif");

			worksButton.addListener(new ButtonListenerAdapter() {
				@Override
				public void onClick(Button button, EventObject e) {
					
					final Function Asig = new   Function() {
						
						@Override
						public void execute() {

							
							Record[] leftRecords = worksGrid.getSelectionModel().getSelections();
		 					boolean msgNoGeoreferenciado = true;
							for (Record record : leftRecords) {
		 						
		 						if (record.getAsString("Georreferenciado").trim().equals("S")) {
		 							record.set("DuracionRecorrido", "0");
		 							record.set("Orden", "");
		 							
			 						resourceWorksGrid.getStore().add(record);
									resourceWorksGrid.getStore().commitChanges();
									
									worksGrid.getStore().remove(record);
									worksGrid.getStore().commitChanges();
								}else{
									msgNoGeoreferenciado = false;
								}
							}
							
							calulateHours(resourceWorksGrid, true);
			 				worksGrid.refreshLabel();
			 				resourceWorksGrid.refreshLabel();
			 				save = false;
		 					if (!msgNoGeoreferenciado) {
								ClientUtils.showInfo("Los trabajos no ubicados, no fueron asignados.");
							}
															
						}
					};
					
					if(getResource()!=null){
						if(getResource().getHorarios()!=null
								&&!getResource().getHorarios().isEmpty()){
							Asig.execute();
						}else{
							
							 MessageBox.show(new MessageBoxConfig() {  
				                    {  
				                        setTitle("Desea continuar?");  
				                        setMsg("El recurso seleccionado no tiene un horario asignado");  
				                        setButtons(MessageBox.YESNO);  
				                        setCallback(new MessageBox.PromptCallback() {  
				                            public void execute(String btnID, String text) {  
				                            	if(btnID.equalsIgnoreCase("Yes")){
				                            		Asig.execute();
				                            	}
				                            	
				                            	
				                            }  
				                        });  
				
				                    }  
				                });  
							//ClientUtils.alert("Error", "Se debe asignar un horario al recurso seleccionado", ClientUtils.ERROR);
						}
					}else{
						ClientUtils.alert("Error", "Se debe seleccionar un recurso previamente para la asignaci\u00f3n de trabajos", ClientUtils.ERROR);
					}
					}
			});

			topToolBar.addButton(mainButton);
			topToolBar.addFill();
			topToolBar.addButton(worksButton);
		} else {
			ToolbarButton worksButton = new ToolbarButton("Quitar trabajos");
			worksButton.setIcon("images/icons/arrow-left.gif");
			
			worksButton.addListener(new ButtonListenerAdapter() {
				@Override
				public void onClick(Button button, EventObject e) {
					Record[] leftRecords = resourceWorksGrid
							.getSelectionModel().getSelections();
			
					for (Record record : leftRecords) {
						record.set("DuracionRecorrido", "0");
						worksGrid.getStore().add(record);
						worksGrid.getStore().commitChanges();
						resourceWorksGrid.getStore().remove(record);
						resourceWorksGrid.getStore().commitChanges();
					
					}
					//Pendiente recalculo de tiempo cuando se retira un trabajo...
					//calulateHours(resourceWorksGrid);
					save = false;
					worksGrid.refreshLabel();
 					resourceWorksGrid.refreshLabel();
				}
			});
			topToolBar.addButton(worksButton);
			topToolBar.addFill();
			topToolBar.addButton(mainButton);
		}
		return topToolBar;

	}

	private Toolbar createToolbar(ArrayList<?> list, final String menuTitle,
			boolean isleft) {
		final Toolbar topToolBar = new Toolbar();

		if (!isleft) {
			date = new DateField();
			/*date.addListener(new DatePickerListenerAdapter(){
				@Override
				public void onSelect(DatePicker dataPicker, Date date) {
					if(resource)
					reloadRightGrid(resource.getId(),
							resource.getCont().getId(), null,  
							DateTimeFormat.getShortDateFormat().format(date));
				}
				
			});*/
			date.setValue(new Date());
			topToolBar.addItem(new ToolbarTextItem("Fecha:"));
			topToolBar.addField(date);
			topToolBar.addSeparator();
		}
		ToolbarButton mainButton = new ToolbarButton(menuTitle);
		// mainButton.setIconCls(menuIconCls);
		// mainButton.disable();
		
		ComboBox b = new ComboBox();
		if (list.size() > 0) {
			Object object = list.get(0);
			if (object instanceof Recurso) {
				RPCFillComboBoxHandler rpc = new RPCFillComboBoxHandler(b, topToolBar, 0);
				rpc.onSuccess((ArrayList<SimpleEntity>) list);
			}
		}
		
		Menu mainMenu = new Menu();

		mainButton.setMenu(mainMenu);
		topToolBar.addButton(mainButton);
		topToolBar.addSeparator();
		final ToolbarTextItem label = new ToolbarTextItem("...");
		topToolBar.addItem(label);
		if (list != null) {

			MyItem myItem = null;
			for (Object object : list) {

				if (object instanceof Recurso) {
					myItem = new MyItem(((Recurso) object), label, this);
					myItem.setGrid(resourceWorksGrid);
					myItem.setPanel(resourceWorksPanel);

				} else if (object instanceof SimpleEntity) {
					myItem = new MyItem(((SimpleEntity) object).getNombre(),
							((SimpleEntity) object).getId(), label, this);
					myItem.setGrid(worksGrid);
					myItem.setPanel(worksPanel);

				}

				mainMenu.addItem(myItem);

			}

		}

		if (!isleft) {
			topToolBar.addFill();
			ToolbarButton savebtn = new ToolbarButton();
			savebtn.setIcon("images/save.gif");
			savebtn.setCls("x-btn-icon");
			savebtn.setTooltip("<b>Guardar programacion</b><br/>Seleccione una fecha a programar y presione guardar");
			savebtn.addListener(new ButtonListenerAdapter(){
				@Override
				public void onClick(Button button, EventObject e) {
					
					if (isWorkOrdered) {
						final boolean sw = resourceWorksGrid.getStore()!=null&&resourceWorksGrid.getStore().getRecords().length>0;
						
						MessageBox.confirm(sw?"Notificaciones":"Mensaje","<b>Advertencia!</b><br/>" +
								(sw?"Desea notificar a todos los clientes por sms?":
									"No hay trabajos asignados, se borraran todo lo programado para la fecha seleccionada,<br/> desea continuar?"),
										new MessageBox.ConfirmCallback() {
											public void execute(final String btnID) {
								
												gui.getStaticAdminQueriesServiceAsync().addResourceJob(
														resourceWorksGrid.getRowData(),
														date.getValueAsString(),
														getResource().getId(),
														getResource().getCont().getId(),
														gui.getUsuario().getId(),
														sw&&btnID.equalsIgnoreCase("yes"),
														new MyBooleanAsyncCallback("Datos guardados con exito",
														new Function() {
															
															@Override
															public void execute() {
																setSave(true);
																reloadRightGrid(getResource().getId(), getResource().getCont().getId(), null, getDate().getValueAsString());																							
															}
														}));	
		
												
											}});
						isWorkOrdered = false;
					} else {
						isWorkOrdered = true;
					}					
				}
			});
			topToolBar.addButton(savebtn);
			/*
			 * cbpoint = new MyComboBox();
			 * gui.getQueriesServiceProxy().getPointsOfInterest(
			 * gui.getProceso().getId(), new RPCFillComplexComboHandler(cbpoint,
			 * null, 1,new Function() {
			 * 
			 * @Override public void execute() {
			 * cbpoint.getStore().add(cbpoint.getRecordDef().createRecord( new
			 * String[] { "all", "Todos", "Todos" , "Todos" }));
			 * cbpoint.getStore().commitChanges(); cbpoint.setValue("all");
			 * 
			 * } })); topToolBar.addItem(new ToolbarTextItem("Proceso:"));
			 * topToolBar.addField(cbpoint); topToolBar.addSeparator();
			 */
		}
		return topToolBar;

	}
	
	/*private void recreateResourceWorksGrid(){
		
		try{
			resourceWorksPanel.remove(resourceWorksGrid, true);
		}catch (Exception ex){
			
		}

		createResourcesWorksGrid();
	}*/
	
	private Toolbar createToolbarPanelResource(final ArrayList<?> list, final String menuTitle,
			boolean isleft) {
		final Toolbar topToolBar = new Toolbar();

		if (!isleft) {
			date = new DateField();
			/*date.addListener(new DatePickerListenerAdapter(){
				@Override
				public void onSelect(DatePicker dataPicker, Date date) {
					if(resource)
					reloadRightGrid(resource.getId(),
							resource.getCont().getId(), null,  
							DateTimeFormat.getShortDateFormat().format(date));
				}
				
			});*/
			date.setValue(new Date());
			topToolBar.addItem(new ToolbarTextItem("Fecha:"));
			topToolBar.addField(date);
			topToolBar.addSeparator();
		}
		//ToolbarButton mainButton = new ToolbarButton(menuTitle);
		// mainButton.setIconCls(menuIconCls);
		// mainButton.disable();
		
		ComboBox cbRecursos = new ComboBox();
		if (list.size() > 0) {
			topToolBar.addItem(new ToolbarTextItem("Recursos:"));
			Object object = list.get(0);
			if (object instanceof Recurso) {
				RPCFillComboBoxHandler rpc = new RPCFillComboBoxHandler(cbRecursos, topToolBar, 0);
				rpc.onSuccess((ArrayList<SimpleEntity>) list);
			}
		}
		
		cbRecursos.addListener(new ComboBoxListenerAdapter(){
			public void onSelect(ComboBox comboBox, Record record, int index) {
				
				resource = (Recurso)list.get(index);
				
				if (type != null)
					setType(type);

				if (getDate() != null)
					date = getDate();

				//if (getId().contains("left")) {
				if (type!=null && type.contains("left")) {
					reloadLeftGrid(type, null);
				} else {
					
					
					Store store = getResourceWorksGrid().getStore();
					if (store != null) {
						if (store.getCount() > 0) {
							MessageBox.confirm("Notificacion", "si continua y no ha guardado, los datos se perderan\n."
									+"Desea continuar?", new MessageBox.ConfirmCallback() {
										
										@Override
										public void execute(String btnID) {
											
											if (btnID.equalsIgnoreCase("yes")) {
												
												/*Record[] leftRecords = vrProgrammerItemListener.getResourceWorksGrid().getStore().getRecords();
												for (Record record : leftRecords) {
							 						
							 						if (!record.getAsString("saved").trim().equals("S")) {
							 							record.set("DuracionRecorrido", "0");
							 							vrProgrammerItemListener.getWorksGrid().getStore().add(record);
								 						vrProgrammerItemListener.getWorksGrid().getStore().commitChanges();
														
								 						vrProgrammerItemListener.getResourceWorksGrid().getStore().remove(record);
								 						vrProgrammerItemListener.getResourceWorksGrid().getStore().commitChanges();
													}
												}*/		
												
												
												reloadLeftGrid(getType(), null);
													
												//recreateResourceWorksGrid();											
																							
												reloadRightGrid(resource.getId(),
														resource.getCont().getId(), null, date.getValueAsString());
											}											
										}
									});
						}else{
							
							//recreateResourceWorksGrid();
							
							reloadRightGrid(resource.getId(),
									resource.getCont().getId(), null, date.getValueAsString());
						}
					}else{
						
						//recreateResourceWorksGrid();
						
						reloadRightGrid(resource.getId(),
								resource.getCont().getId(), null, date.getValueAsString());
					}
					
				}
				
		    }
		});
		
		
		//Menu mainMenu = new Menu();

		//mainButton.setMenu(mainMenu);
		//topToolBar.addButton(mainButton);
		//topToolBar.addSeparator();
		//final ToolbarTextItem label = new ToolbarTextItem("...");
		//topToolBar.addItem(label);
		/*if (list != null) {

			MyItem myItem = null;
			for (Object object : list) {

				if (object instanceof Recurso) {
					myItem = new MyItem(((Recurso) object), label, this);
					myItem.setGrid(resourceWorksGrid);
					myItem.setPanel(resourceWorksPanel);

				} else if (object instanceof SimpleEntity) {
					myItem = new MyItem(((SimpleEntity) object).getNombre(),
							((SimpleEntity) object).getId(), label, this);
					myItem.setGrid(worksGrid);
					myItem.setPanel(worksPanel);

				}

				mainMenu.addItem(myItem);

			}

		}*/

		if (!isleft) {
			topToolBar.addFill();
			ToolbarButton savebtn = new ToolbarButton();
			savebtn.setIcon("images/save.gif");
			savebtn.setCls("x-btn-icon");
			savebtn.setTooltip("<b>Guardar programacion</b><br/>Seleccione una fecha a programar y presione guardar");
			savebtn.addListener(new ButtonListenerAdapter(){
				@Override
				public void onClick(Button button, EventObject e) {

					
					if(!empty(resourceWorksGrid.getStore(), "Orden")){
						
						final boolean sw = resourceWorksGrid.getStore()!=null&&resourceWorksGrid.getStore().getRecords().length>0;
						MessageBox.confirm(sw?"Notificaciones":"Mensaje","<b>Advertencia!</b><br/>" +
								(sw?"Desea notificar a todos los clientes por sms?":
									"No hay trabajos asignados, se borraran todo lo programado para la fecha seleccionada,<br/> desea continuar?"),
										new MessageBox.ConfirmCallback() {
											public void execute(final String btnID) {
								
								
													gui.getStaticAdminQueriesServiceAsync().addResourceJob(
															resourceWorksGrid.getRowData(),
															date.getValueAsString(),
															getResource().getId(),
															getResource().getCont().getId(),
															gui.getUsuario().getId(),
															sw && btnID.equalsIgnoreCase("yes"),
															new MyBooleanAsyncCallback("Datos guardados con exito",
															new Function() {
																
																@Override
																public void execute() {
																	save = true;
																	reloadRightGrid(getResource().getId(), getResource().getCont().getId(), null, getDate().getValueAsString());																							
																}
															}));	
											
												
											}});
						
					}else{
						ClientUtils.showError("No pueden haber ordenamientos vacios");
						
					}
				}
			});
			topToolBar.addButton(savebtn);
			/*
			 * cbpoint = new MyComboBox();
			 * gui.getQueriesServiceProxy().getPointsOfInterest(
			 * gui.getProceso().getId(), new RPCFillComplexComboHandler(cbpoint,
			 * null, 1,new Function() {
			 * 
			 * @Override public void execute() {
			 * cbpoint.getStore().add(cbpoint.getRecordDef().createRecord( new
			 * String[] { "all", "Todos", "Todos" , "Todos" }));
			 * cbpoint.getStore().commitChanges(); cbpoint.setValue("all");
			 * 
			 * } })); topToolBar.addItem(new ToolbarTextItem("Proceso:"));
			 * topToolBar.addField(cbpoint); topToolBar.addSeparator();
			 */
		}
		return topToolBar;
	}

	/**
	 * Recarga la grilla izquierda con el filtro de grupo
	 * @param type
	 * @param r
	 */
	public void reloadLeftGrid(String type, Record r) {
		int index = r != null ? r.getAsInteger("Orden") - 1 : -1;
		reloadGrid(type, null, null, worksGrid, worksPanel, index, true, true,
				"Georeferenciar", null,worksGrid.getGroupField());

	}
	
	/**
	 * Recarga la grilla izquierda con el filtro de ubicacion
	 * @param type
	 * @param r
	 */
	public void reloadLeftGridFilterGrupo(String type, Record r) {
		int index = r != null ? r.getAsInteger("Orden") - 1 : -1;
		reloadGridFilterGrupo(type, null, null, worksGrid, worksPanel, index, true, true,
				"Georeferenciar", null,worksGrid.getGroupField());

	}

	public void reloadRightGrid(String resource, String contractor, Record r,
			String fecha) {
				
		int index = r != null ? r.getAsInteger("Orden") - 1 : -1;
		reloadGridRight(null, resource, contractor, resourceWorksGrid,
				resourceWorksPanel, index, true, true, "Georeferenciar", fecha, null);

	}	

	/**
	 * Recarga la grilla con el filtro de agrupacion
	 * @param type
	 * @param resource
	 * @param contractor
	 * @param grid
	 * @param panel
	 * @param index
	 * @param checkers
	 * @param addBottonToolbar
	 * @param details
	 * @param fecha
	 * @param orderBy
	 */
	public void reloadGrid(final String type, final String resource,
			final String contractor, final MyGridProgrammerPanel grid,
			final Panel panel, final int index, boolean checkers,
			boolean addBottonToolbar, String details, String fecha,String orderBy) {

		
		if (grupo != null) {
			groupedlistener.onCheckChange(grupo, true);
		}

		if (grupo == null) {
			gui.getQueriesServiceProxy().getJobsToSchedule(
					gui.getProceso().getId(),
					type,
					contractor,
					resource,
					fecha,
					orderBy,
					new RPCFillTableProgrammer(grid, panel, details, false, false,
							true, null, null, new RowLayoutData(), "Orden",
							checkers, addBottonToolbar, "trabajos", new Function() {

								@Override
								public void execute() {
									if (index >= 0)
										grid.getSelectionModel().selectRow(index);
									
																								
								}
							}));
		}
	}
	
	public void reloadGridRight(final String type, final String resource,
			final String contractor, final MyGridProgrammerPanel grid,
			final Panel panel, final int index, boolean checkers,
			boolean addBottonToolbar, String details, String fecha,String orderBy) {

			gui.getQueriesServiceProxy().getJobsToSchedule(
					gui.getProceso().getId(),
					type,
					contractor,
					resource,
					fecha,
					orderBy,
					new RPCFillTableProgrammer(grid, panel, details, false, false,
							true, null, null, new RowLayoutData(), "Orden",
							checkers, addBottonToolbar, "trabajos", new Function() {

								@Override
								public void execute() {
									if (index >= 0)
										grid.getSelectionModel().selectRow(index);
									
																								
								}
							}));
		
	}
	
	/**
	 * Recarga la grilla con el filtro de ubicacion
	 * @param type
	 * @param resource
	 * @param contractor
	 * @param grid
	 * @param panel
	 * @param index
	 * @param checkers
	 * @param addBottonToolbar
	 * @param details
	 * @param fecha
	 * @param orderBy
	 */
	public void reloadGridFilterGrupo(final String type, final String resource,
			final String contractor, final MyGridProgrammerPanel grid,
			final Panel panel, final int index, boolean checkers,
			boolean addBottonToolbar, String details, String fecha,String orderBy) {

		gui.getQueriesServiceProxy().getJobsToSchedule(
				gui.getProceso().getId(),
				type,
				contractor,
				resource,
				fecha,
				orderBy,
				new RPCFillTableProgrammer(grid, panel, details, false, false,
						true, null, null, new RowLayoutData(), "Orden",
						checkers, addBottonToolbar, "trabajos", new Function() {

							@Override
							public void execute() {
								
								if (filter != null) {
									Filterlistener.onCheckChange(filter, true);
								}
								
								if (index >= 0)
									grid.getSelectionModel().selectRow(index);
											
								
							}
						}));
	}



   
	@SuppressWarnings("deprecation")
	public  String getSumHour(String interval1,int minutes) {
    	Date date=	new Date();
    	
    	int hour1 = 0;
    	int minutes1 = 0;
    		
    	try {
    		String[] i1=interval1!=null&&!interval1.isEmpty()?interval1.split(":"):null;   
    		hour1=Integer.parseInt(i1[0]);
    		minutes1=Integer.parseInt(i1[1]);
    		
		} catch (Exception e) {}
    	
    	date.setHours(hour1);
		date.setMinutes(minutes1+minutes);	
		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm");
		return dtf.format(date);
		//return DateTimeFormat.getShortTimeFormat().format(date).split(" ")[0];
    	//SimpleDateFormat sdfHour = new SimpleDateFormat(HOUR_FORMAT);
    	//return sdfHour.format(date.getTime());
    }
	
	@SuppressWarnings("deprecation")
	public  String getFormatHour(String interval1) {
    	Date date=	new Date();
    	
    	int hour1 = 0;
    	int minutes1 = 0;
    		
    	try {
    		String[] i1=interval1!= null && !interval1.isEmpty() ? interval1.split(":") : null;   
    		hour1=Integer.parseInt(i1[0]);
    		minutes1=Integer.parseInt(i1[1]);
    		
		} catch (Exception e) {}
    	
    	date.setHours(hour1);
		date.setMinutes(minutes1);	
		DateTimeFormat dtf = DateTimeFormat.getFormat("HH:mm");
		return dtf.format(date);
		//return DateTimeFormat.getShortTimeFormat().format(date).split(" ")[0];
    	//SimpleDateFormat sdfHour = new SimpleDateFormat(HOUR_FORMAT);
    	//return sdfHour.format(date.getTime());
    }
    
    public static boolean isHourInInterval(String start, String end) {
        return (start.compareTo(end) < 0);
    }
    
	@SuppressWarnings("deprecation")
	public  Date createDate(String d){
		Date date=new Date();
		date.setHours(Integer.parseInt(d.split(":")[0]));
		date.setMinutes(Integer.parseInt(d.split(":")[1]));	
		return date;
	}
	
	
	
	public void calulateHours(GridPanel grid,boolean clear){

		int index = 0;
		int order = 0;
		int sum_duration = 0;
		int journey_time = 0;
		int duration = 0;
		
		Horario h =null; 

		if(grid!=null&&grid.getStore()!=null
				&&grid.getStore().getRecords().length>0){
			
			Record[] leftRecords= grid.getStore().getRecords();
			h = getResource().getHorarios().get(index);
			
			for (Record record : leftRecords) {
				if(clear){
					record.set("DuracionRecorrido", "0");
				}
				
				String dur = record.getAsString("Duracion") == null ? "0":record.getAsString("Duracion");
				String jtime = record.getAsString("DuracionRecorrido")== null ? "0":record.getAsString("DuracionRecorrido");
				
				 duration = Integer.parseInt(dur);
				 journey_time = Integer.parseInt(jtime);
				
				if(h!=null){
					if(sum_duration>h.getDuration()){
						index++;
						if(index<=getResource().getHorarios().size()-1){
							h = getResource().getHorarios().get(index);
							sum_duration=0;
					}
				}
				}
				record.set("Hora", getSumHour(h.getInitialTime(),sum_duration+journey_time));
				record.set("index",index);
				//record.set("Orden",order /*ClientUtils.lpad(String.valueOf(order), "0", 6)*/);
				record.set("DuracionRecorrido",""+sum_duration);
				order++;
				sum_duration+=(duration+journey_time);
			}
			grid.getStore().commitChanges();
		}		
	}
	
	/**
	 * Calcula las horas de los registros de la grilla dada una posicion en adelante
	 * @param grid GridPanel con los datos
	 * @param clear
	 * @param index2 int con la posicion  del registro
	 * @param newValue String con la nueva hora del registro de la posicion dada
	 */
	public void reCalulateHours(GridPanel grid, boolean clear, int index2, String newValue){

		int index = 0;
		int order = 0;
		int sum_duration = 0;
		int journey_time = 0;
		int duration = 0;
		boolean fistEntry = true;
		
		Horario h = null; 

		if(grid != null && grid.getStore()!=null
				&& grid.getStore().getRecords().length > 0){
			
			Record[] leftRecords= grid.getStore().getRecords();
			h = getResource().getHorarios().get(index);
	
			for (int i = index2; i < leftRecords.length; i++) {
				Record record = leftRecords[i];
				if(clear){
					record.set("DuracionRecorrido", "0");
				}
				
				String dur = record.getAsString("Duracion") == null ? "0":record.getAsString("Duracion");
				String jtime = record.getAsString("DuracionRecorrido")== null ? "0":record.getAsString("DuracionRecorrido");
				
				 duration = Integer.parseInt(dur);
				 journey_time = Integer.parseInt(jtime);
				
				if(h != null){
					if(sum_duration > h.getDuration()){
						index++;
						if(index <= getResource().getHorarios().size()-1){
							h = getResource().getHorarios().get(index);
							sum_duration=0;
						}
					}
				}
				
				record.set("Hora", getSumHour(newValue,sum_duration+journey_time));
				record.set("index",index);
				if (fistEntry) {
					order = Integer.parseInt(record.getAsString("Orden"));
					fistEntry = false;
				}				
				record.set("Orden",order);
				record.set("DuracionRecorrido",""+sum_duration);
				order++;
				sum_duration+=(duration+journey_time);
			}
			grid.getStore().commitChanges();
		}		
	}
	
	
	private MyGridProgrammerPanel BuildWorksGrid(String group){
		final MyGridProgrammerPanel	worksGrid = new MyGridProgrammerPanel();
	
		if(group!=null){
			worksGrid.setSortField("Georreferenciado");
			worksGrid.setGroupField(group);
			worksGrid.setGrouped(true);
		}
		worksGrid.setTopToolbar(createGridToolbar(true));

		worksGrid.addDetListener(new GeoreferenceListener(ViewRouteProgrammerItemListener.this, true));
		worksGrid.addReorderListener(new MyGridOnCellClickListener() {

			@Override
			public void onCellClick(Record r) {
				worksGrid.getView().refresh();

			}
		});
		
		
	return worksGrid;	
		
	}	
	
	
	private boolean exists(Store grid,Record record, String text){
		try {
			Record[] r =  grid.getRecords();
			for (Record b : r) {
				if(b.getAsString("Orden")!=null) {
					if(!b.getId().equals(record.getId())
							&&b.getAsString("Orden").equals(text)) {
						return true;
					}
				}
			}
			return false;

		} catch (Exception e) {
			return false;
		}

	}
	public boolean empty(Store grid,String text){
		try {
			Record[] r =  grid.getRecords();
			for (Record b : r) {
				if(b.getAsString(text)==null||b.getAsString(text).equals("")) {
					
					return true;
					
				}
			}
			return false;
			
		} catch (Exception e) {
			return false;
		}
		
	}

	public boolean isSave() {
		return save;
	}

	public void setSave(boolean save) {
		this.save = save;
	}
}
