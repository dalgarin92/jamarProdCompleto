package org.extreme.controlweb.client.programmer.listener;

import org.extreme.controlweb.client.programmer.ViewRouteProgrammerItemListener;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.core.SortDir;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.MessageBox;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.event.GridRowListenerAdapter;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.Item;
import com.gwtext.client.widgets.menu.Menu;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class RouteStartPointLinstener extends GridRowListenerAdapter{

	private ViewRouteProgrammerItemListener view;
	private Function f;
	
	public RouteStartPointLinstener(ViewRouteProgrammerItemListener view,Function f) {
		this.view=view;
		this.f=f;
	}
	
	
	public RouteStartPointLinstener(ViewRouteProgrammerItemListener view) {
		this.view=view;
	}
	
	 @Override
	public void onRowContextMenu(final GridPanel grid, int rowIndex, EventObject e) {


			e.stopEvent();

			final Record record = grid.getStore().getAt(rowIndex);
			final boolean estado = record.getAsString("Inicio Ruta").equals("S");
			
			Menu m = new Menu();
			Item mi = new Item(estado ? "Quitar como punto de inicio": "Establecer como punto de inicio");
			mi.setIconCls(estado ? "icon-desactivar" : "icon-activar");
			mi.addListener(new BaseItemListenerAdapter() {
				@Override
				public void onClick(BaseItem item, EventObject e) {
				
					MessageBox.confirm("Cambiar punto de inicio de ruta","<b>Advertencia!</b><br/>" +
							"Est\u00e1 opci\u00f3n cambiara el orden de todos los puntos, desea continuar?",
									new MessageBox.ConfirmCallback() {
										public void execute(String btnID) {
											/*
											String trabajo = record.getAsString("C\u00f3digo");
											String tipotrabajo = record.getAsString("_tipotrabajo_");
											String recurso = record.getAsString("Recurso");
											String contratista = record.getAsString("Contratista");
											*/
											if (btnID.equalsIgnoreCase("yes")) {
												
												Record[] records=grid.getStore().getRecords();
												for (Record r : records) {
													r.set("Inicio Ruta", "N");
												}
												grid.getStore().commitChanges();
												record.set("Inicio Ruta", estado?"N":"S");
												grid.getStore().sort("Inicio Ruta",SortDir.DESC);
												view.calulateHours(grid,true);
												grid.getStore().commitChanges();
												
												if(f!=null){
													f.execute();
												}
												
												/*
												gui.getQueriesServiceProxy()
														.setPathStart(
																trabajo,
																tipotrabajo,
																recurso,
																contratista,
																estado,
																new MyBooleanAsyncCallback(
																		(estado?"Se quito el ":"Se ha cambiado el ")+"punto de inicio",new Function(){

																			@Override
																			public void execute() {
																				view.reloadRightGrid(view.getResource().getId(), 
																						view.getResource().getCont().getId(), null,view.getDate().getValueAsString());
																				
																			}
																			
												}));
											*/}
										}
									});

				}
			});

			m.addItem(mi);
			m.showAt(e.getXY());
		
	}
}
