package org.extreme.controlweb.client.programmer.listener;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.handlers.RPCFillComplexComboHandler;
import org.extreme.controlweb.client.listeners.AddFavButtonListener;
import org.extreme.controlweb.client.listeners.ViewInterestPointListener;
import org.extreme.controlweb.client.programmer.MyGridProgrammerPanel;
import org.extreme.controlweb.client.programmer.ViewRouteProgrammerItemListener;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.MessageBox;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.menu.BaseItem;
import com.gwtext.client.widgets.menu.event.BaseItemListenerAdapter;

public class GeoOrderListener extends BaseItemListenerAdapter {

	//private MyGridProgrammerPanel grid;
	private ControlWebGUI gui;
	private int latIndex = 15;
	private int lonIndex = 16;
	private boolean isLeft;
	private ViewRouteProgrammerItemListener view;
	private boolean manualSort = false;

	public GeoOrderListener(/*MyGridProgrammerPanel grid, */boolean isLeft,
			ControlWebGUI gui, ViewRouteProgrammerItemListener view, boolean manualSort) {
		//this.grid = grid;
		this.isLeft = isLeft;
		this.gui = gui;
		this.view = view;
		this.manualSort = manualSort;
	}

	@Override
	public void onClick(BaseItem item, EventObject e) {
		if (isLeft) {

			final Window wnd = new Window("Punto de inicio", 230, 110);
			wnd.setPaddings(10);
			wnd.setResizable(false);
			final MyComboBox cb = new MyComboBox("Punto");
			gui.getQueriesServiceProxy().getPointsOfInterest(
					gui.getProceso().getId(),new AsyncCallback<ArrayList<RowModel>>() {

						@Override
						public void onFailure(Throwable caught) {
							ClientUtils.alert("ERROR.", "Se ha presentado un error: "+caught.getMessage(),ClientUtils.ERROR);
							
						}

						@Override
						public void onSuccess(ArrayList<RowModel> result) {
							if(result!=null &&result.size()>0){

								new RPCFillComplexComboHandler(cb, wnd, 1, new Function() {

									@Override
									public void execute() {
										wnd.show();
									}
								}).onSuccess(result);
							}else{
								adminPuntoInteres();
							}
							
						}
					});

			wnd.addButton(new Button("Aceptar", new ButtonListenerAdapter() {
				@Override
				public void onClick(Button button, EventObject e) {

					if (cb.getValue() != null && !cb.getValue().isEmpty()) {
						//view.get.getEl().mask("Geordenando...");
						try {

							ArrayList<RowModel> rm = view.getWorksGrid().getRowData();
							final Object[] values = new Object[rm.get(0)
									.getColumnNames().length];
							values[latIndex] = cb.getSelectedRecord().getAsString("lat");
							values[lonIndex] = cb.getSelectedRecord()
									.getAsString("lon");

							ArrayList<RowModel> rowModel = geoOrder(rm,
									new RowModel() {

										@Override
										public Object[] getRowValues() {
											// TODO Auto-generated method stub
											return values;
										}

										@Override
										public String[] getColumnNames() {
											// TODO Auto-generated method stub
											return null;
										}
									}, -1, latIndex, lonIndex);
							view.getWorksGrid().loadRows(rowModel,true);
							view.getWorksGrid().getView().refresh();
						} catch (Exception e2) {
							ClientUtils
									.showError("Se ha presentado un error, intente nuevamente");
						}

						//grid.getEl().unmask();
						wnd.close();
					}
				}
			}));
		} else {


			if(!view.empty(view.getResourceWorksGrid().getStore(), "Orden")){

				final Window wnd = new Window("Punto de inicio", 230, 110);
				wnd.setPaddings(10);
				wnd.setResizable(false);
				final MyComboBox cb = new MyComboBox("Punto");
				gui.getQueriesServiceProxy().getPointsOfInterest(
						gui.getProceso().getId(),
						new RPCFillComplexComboHandler(cb, wnd, 1, new Function() {
	
							@Override
							public void execute() {
								if (cb.getStore() != null) {
									wnd.show();
								}else{
									
									adminPuntoInteres();
								}							
							}
						}));
	
				wnd.addButton(new Button("Aceptar", new ButtonListenerAdapter() {
					@Override
					public void onClick(Button button, EventObject e) {
						
							if (cb.getValue() != null && !cb.getValue().isEmpty()) {						
								//view.getResourceWorksGrid().getEl().mask("Geordenando...");
								
								String start = manualSort ? "S":view.getResourceWorksGrid().getStore().getAt(0)
										.getAsString("Inicio Ruta");
		
								if (!start.equals("S")) {
									
									MessageBox.confirm(	"Confirm","No se ha establecido un punto de inicio\n "
															+ "por defecto se tomara el primer registro de tabla, desea continuar?",
													new MessageBox.ConfirmCallback() {
		
														public void execute(String btnID) {
		
															if (btnID.equalsIgnoreCase("yes")) {
		
																view.getResourceWorksGrid().getEl().mask(manualSort?"Calculando tiempos...":"Geordenando...");
		
																gui.getQueriesServiceProxy().getGoogleDistanceTime(
																		view.getResourceWorksGrid().getRowData(),
																				cb.getSelectedRecord().getAsString("lat"),
																				cb.getSelectedRecord().getAsString("lon"),
																				view.getResource().getId(),
																				view.getResource().getCont().getId(),
																				manualSort,
																				new AsyncCallback<ArrayList<RowModel>>() {
		
																					@Override
																					public void onSuccess(ArrayList<RowModel> result) {
																						view.getResourceWorksGrid().loadRows(result,true);
																						view.getResourceWorksGrid().getView().refresh();
																						view.calulateHours(view.getResourceWorksGrid(),false);
																						view.getResourceWorksGrid().getEl().unmask();
																					}
		
																					@Override
																					public void onFailure(Throwable caught) {
																						view.getResourceWorksGrid().getEl().unmask();
																						ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
																						
																					}
																				});
		
																wnd.close();
		
															}else{
																view.getResourceWorksGrid().getEl().unmask();
															}
														}
													});
								} else {
		
		
		
									view.getResourceWorksGrid().getEl().mask("Geordenando...");
		
									gui.getQueriesServiceProxy().getGoogleDistanceTime(
											view.getResourceWorksGrid().getRowData(),
													cb.getSelectedRecord().getAsString("lat"),
													cb.getSelectedRecord().getAsString("lon"),
													view.getResource().getId(),
													view.getResource().getCont().getId(),
													manualSort,
													new AsyncCallback<ArrayList<RowModel>>() {
		
														@Override
														public void onSuccess(ArrayList<RowModel> result) {
															view.getResourceWorksGrid().loadRows(result,true);
															view.calulateHours(view.getResourceWorksGrid(),false);
															view.getResourceWorksGrid().getEl().unmask();
															view.getResourceWorksGrid().getView().refresh();
														}
		
														@Override
														public void onFailure(Throwable caught) {
															view.getResourceWorksGrid().getEl().unmask();
														}
													});
		
									wnd.close();
								}
		
							}else{
								
								ClientUtils.alert("Error", "Seleccione un punto de partida", ClientUtils.ERROR);
							}
				
					}
				}));
		}else{
			ClientUtils.showError("Para poder continuar, no pueden haber ordenamientos vacios. Complete e intente nuevamente");
			
		}
		}

	}

	private void adminPuntoInteres(){
		
		MessageBox.confirm("Confirm", "No existe un punto de interes global establecido.\n "
				+"Desea crear un punto de interes?", new MessageBox.ConfirmCallback() {
					
					@Override
					public void execute(String btnID) {
						
						if (btnID.equalsIgnoreCase("yes")) {
							new ViewInterestPointListener(gui).onClick(null, null);	
						}																							
					}
				});
	}
	
	private ArrayList<RowModel> geoOrder(ArrayList<RowModel> rightData,
			RowModel r, int index, int latIndex, int lonIndex) {
		ArrayList<RowModel> res = new ArrayList<RowModel>();

		if (index >= 0) {
			res.add(rightData.get(index));
			rightData.remove(index);
		} else if (r != null) {
			res.add(r);
		}

		while (rightData.size() > 0) {
			RowModel next = null;
			double maxDist = Double.MAX_VALUE;
			double dist;
			int nextIndex = 0;
			for (int j = 0; j < rightData.size(); j++) {
				RowModel actual = rightData.get(j);
				Object[] data1 = actual.getRowValues();
				Object[] data2 = res.get(res.size() - 1).getRowValues();
				try {
					dist = ClientUtils.distance(
							Double.parseDouble((String) data2[latIndex]),
							Double.parseDouble((String) data2[lonIndex]),
							Double.parseDouble((String) data1[latIndex]),
							Double.parseDouble((String) data1[lonIndex]));
				} catch (Exception e) {
					dist = Double.MAX_VALUE / 2;
				}

				if (dist < maxDist) {
					next = actual;
					nextIndex = j;
					maxDist = dist;
				}
			}
			if (next != null) {
				res.add(next);
				rightData.remove(nextIndex);
			}
		}
		if (r != null) {
			res.remove(0);
		}
		return res;
	}

}
