package org.extreme.controlweb.client.programmer.listener;

import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.gui.MyGridOnCellClickListener;
import org.extreme.controlweb.client.map.openlayers.MyBounds;
import org.extreme.controlweb.client.map.openlayers.OpenLayersSingleMap;
import org.extreme.controlweb.client.programmer.ViewRouteProgrammerItemListener;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;

public class GeoreferenceListener implements MyGridOnCellClickListener {

	private ViewRouteProgrammerItemListener routeProg;
	private  boolean isLeft;
	private OpenLayersSingleMap map;
	private VectorFeature MyWorkMarker;

	public GeoreferenceListener(ViewRouteProgrammerItemListener ruoteProg, boolean isLeft) {
		this.routeProg= ruoteProg;
		this.isLeft= isLeft;
		
		
		}
	
	
	@Override
	public void onCellClick(final Record r) {

		Function showWindow =new Function() {
			
			@Override
			public void execute() {
				final Window georeferenceWindow = new Window("Ubicar trabajo:"+ r.getAsString("Cliente"), 700, 500);

				map = new OpenLayersSingleMap(routeProg.getGui());

				LonLat ll = ClientUtils.DEFAULT_CENTER;
				ll.transform("EPSG:4326", "EPSG:900913");
				map.getMap().setCenter(ll);

				georeferenceWindow.add(map);

				map.setCrosshairCursor();

				final MapClickListener listener = new MapClickListener() {
					private boolean drag = false;

					@Override
					public void onClick(MapClickEvent mapClickEvent) {
						LocalizableObject lo = new LocalizableObject(
								String.valueOf(mapClickEvent.getLonLat().lat()),
								String.valueOf(mapClickEvent.getLonLat().lon()));
						drag = true;
						relocate(lo, drag, false, 0);
					}
				};

				map.getMap().addMapClickListener(listener);

				georeferenceWindow.addButton(new Button("Guardar",
						new ButtonListenerAdapter() {
							@Override
							public void onClick(Button button, EventObject e) {

								LonLat ll = new LonLat(MyWorkMarker.getCenterLonLat()
										.lon(), MyWorkMarker.getCenterLonLat().lat());

								ll.transform("EPSG:900913", "EPSG:4326");

								routeProg.getGui().getQueriesServiceProxy().locateJobAddress(
										r.getAsString("Direcci\u00f3n"),
										r.getAsString("_municipio_"),
										String.valueOf(ll.lat()),
										String.valueOf(ll.lon()),
										r.getAsString("C\u00f3digo"),
										r.getAsString("_tipotrabajo_"),
										new AsyncCallback<Boolean>() {

											@Override
											public void onSuccess(Boolean result) {
												map.setDefaultCursor();
												georeferenceWindow.close();
												if (isLeft) {
													routeProg.reloadLeftGrid(routeProg.getType(), r);
												} else {
													routeProg.reloadLeftGrid(routeProg.getType(), r);
													routeProg.reloadRightGrid(routeProg.getResource().getId(), 
															routeProg.getResource().getCont().getId(), r,routeProg.getDate().getValueAsString());
												}

												ClientUtils
														.showInfo("Trabajo ubicado exitosamente!");
											}

											@Override
											public void onFailure(Throwable caught) {
												ClientUtils
														.showError("Se ha preentado un error al ubiacar al trabajo!");

											}
										});

							}
						}));
				georeferenceWindow.addButton(new Button("Cancelar",
						new ButtonListenerAdapter() {
							@Override
							public void onClick(Button button, EventObject e) {
								map.setDefaultCursor();
								georeferenceWindow.close();
							}
						}));
				georeferenceWindow.show();

				if (r.getAsString("Georreferenciado").equals("S")) {

					ll = new LonLat(r.getAsDouble("lon"), r.getAsDouble("lat"));
					ll.transform("EPSG:4326", "EPSG:900913");

					MyBounds bounds = new MyBounds(ll.lat(), ll.lon(), ll.lat(),
							ll.lon());
					int zoom = map.getMap().getZoomForExtent(
							bounds.transform(new Projection("EPSG:4326"),
									new Projection("EPSG:900913")), false) + 3;
					relocate(
							new LocalizableObject(String.valueOf(ll.lat()),
									String.valueOf(ll.lon())), true, true, zoom);
				}

				map.activateDragControl();				
			}
		};
		
	if(!routeProg.isSave()){
		
		if(com.google.gwt.user.client.Window.confirm("No ha guardado los cambios, todos los cambios se perderan, desea continuar?")){
			
			showWindow.execute();
		}
		
	}else{
		showWindow.execute();		
	}
		
	
	
		
	}
	
	private void relocate(LocalizableObject result, boolean drag,
			boolean center, int zoom) {
		removeMarker();
		MyWorkMarker = map.createDraggableMarker(result.getLatitudAsDouble(),
				result.getLongitudAsDouble(), "images/utiles/icons/gpin32.png");
		if (center) {

			map.getMap().setCenter(MyWorkMarker.getCenterLonLat(), zoom);

		}
	}
	
	private void removeMarker() {
		if (MyWorkMarker != null) {
			map.getVecAuxDraggable().removeFeature(MyWorkMarker);
			MyWorkMarker = null;
		}
	}



}
