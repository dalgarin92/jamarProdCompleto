package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.core.Recurso;
import org.extreme.controlweb.client.core.Turno;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.tree.TreeNode;

public class RPCUpdateRecurso implements AsyncCallback<Recurso> {

	private ControlWebGUI gui;
	private TreeNode node;
	private boolean save;
	private Function f;

	public RPCUpdateRecurso(ControlWebGUI gui, TreeNode node, boolean save, Function afterUpdate) {
		this.gui = gui;
		this.node = node;
		this.save = save;
		f = afterUpdate;
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	@Override
	public void onSuccess(Recurso result) {
		if (result != null) {
			Recurso oldRec = (Recurso)node.getAttributeAsObject("entity");
			result.setCont(oldRec.getCont());
			result.setProceso(gui.getProceso());
			result.setChecked(true);

			ArrayList<Turno> ts = gui.getProceso().getAllTurnos();
			for (Turno t : ts) {
				t.addRecIfExists(result);
			}

			ArrayList<Object> marks = gui.getMarkers(node);
			gui.getMap().removeMarkers(marks);

			node.setAttribute("entity", result);
			Marker m = gui.getMap().createVehicleMarker(result.getVeh().getLatitudAsDouble(),
					result.getVeh().getLongitudAsDouble(), result.getMapIcon(),
					result.getHTML());

			if (save && result.getVeh().getLatitudAsDouble() != 0.0d
					&& result.getVeh().getLongitudAsDouble() != 0.0d) {
				//Marker oldMarker = (Marker)node.getAttributeAsObject("marker");
				//gui.getMap().removeOverlay(oldMarker);
				node.setAttribute("marker", m);
				//gui.getMap().getGmap().addOverlay(m);
			} else {
				//gui.getMap().removeOverlay((Marker)node.getAttributeAsObject("marker"));
				node.setAttribute("marker", null);
				//gui.getMap().setTempMarker(m);
			}

			node.setText(result.getTitle());
			node.setIconCls(result.getIconTree());
			node.setTooltip(result.getHTML());
			node.setAttribute("qtip", result.getHTML());
			node.getUI().getTextEl().setAttribute("qtip", result.getHTML());

			if(!((Proceso)result.getProceso()).isSoloCentral()){
				gui.removeAllChilds(node);
				gui.addTrabTreeNodes(result.getTrabajos(), node,
						true, false, gui);
			}
			gui.getTree().getEl().unmask();
			node.getUI().toggleCheck(true);

			if(f != null) {
				f.execute();
			} else {
				node.expand();
			}
		}
	}
}
