package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.MyComboBox;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.widgets.BoxComponent;
import com.gwtext.client.widgets.Container;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.layout.LayoutData;

public class RPCFillComplexComboHandler implements
		AsyncCallback<ArrayList<RowModel>> {

	private MyComboBox combo;
	private BoxComponent fs;
	private int index;
	private Function f;
	private LayoutData ld;
	private boolean order = true;
	private boolean forceSelection = true;

	public RPCFillComplexComboHandler(MyComboBox cb, BoxComponent fs, int index) {
		this.combo = cb;
		this.fs = fs;
		this.index = index;
	}
	
	public RPCFillComplexComboHandler(MyComboBox cb, boolean order, BoxComponent fs,
			LayoutData ld) {
		this.order = order;
		this.combo = cb;
		this.fs = fs;
		this.ld = ld;
	}
	
	public RPCFillComplexComboHandler(MyComboBox cb, boolean order, BoxComponent fs,
			LayoutData ld, Function f) {
		this.order = order;
		this.combo = cb;
		this.fs = fs;
		this.ld = ld;
		this.f = f;
	}
	
	public RPCFillComplexComboHandler(MyComboBox cb, BoxComponent fs, int index, Function f) {
		this.combo = cb;
		this.fs = fs;
		this.index = index;
		this.f = f;
	}
	
	public RPCFillComplexComboHandler(MyComboBox cb, BoxComponent fs,
			int i, boolean forceSelection) {
		this.combo = cb;
		this.fs = fs;
		this.index = i;
		this.forceSelection = forceSelection;
	}
	
	public RPCFillComplexComboHandler(MyComboBox cb, BoxComponent fs, LayoutData ld) {
		this.combo = cb;
		this.fs = fs;
		this.ld = ld;
	}
	
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", this.getClass().toString() + ": "
				+ caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	public void onSuccess(ArrayList<RowModel> rma) {
		Store store = null;
		RecordDef rd = null;
		
		if (rma != null)
			if (rma.size() > 0)
				rd = ClientUtils.createRecordDef(rma.get(0));

		if (rd != null) {
			store = new Store(rd);
			for (RowModel rm : rma) 
				store.add(rd.createRecord(rm.getRowValues()));
			store.commitChanges();

			RowModel rm = rma.get(0);
			
			combo.setMinChars(1);
			if (order)
				store.sort(rm instanceof RowModelAdapter ? ((RowModelAdapter) rm)
						.getVisibleVariables().get(1) : rm.getColumnNames()[1]);
			combo.setStore(store);
			combo.setDisplayField(rm instanceof RowModelAdapter ? ((RowModelAdapter) rm)
							.getVisibleVariables().get(1)
							: rm.getColumnNames()[1]);
			combo.setMode(ComboBox.LOCAL);
			combo.setTriggerAction(ComboBox.ALL);		
			combo.setTypeAhead(true);
			combo.setValueField(rm instanceof RowModelAdapter ? ((RowModelAdapter) rm)
					.getVisibleVariables().get(0)
					: rm.getColumnNames()[0]);
			combo.setForceSelection(forceSelection);
			combo.setSelectOnFocus(true);

			if (fs != null) {
				//if (rma.size() != 1) {
					if (fs instanceof Toolbar)
						((Toolbar) fs).addField(combo);
					else if (ld == null)
						((Container) fs).insert(index, combo);
					else
						((Container) fs).add(combo, ld);
				//}
					if(rma.size() == 1)  {
						if(combo instanceof MyComboBox)
							((MyComboBox)combo).setSelected(0);
					combo.setValue(combo.getStore().getRecordAt(0).getAsString(
							rm instanceof RowModelAdapter ? ((RowModelAdapter) rm)
									.getVisibleVariables().get(0) : rm
									.getColumnNames()[0]));
				}
				((Container) fs).doLayout(true);
			}
			
			combo.setRecordDef(rd);
		}
		
		if(f != null)
			f.execute();
	}
	
	public void setFunction(Function f) {
		this.f = f;
	}

}
