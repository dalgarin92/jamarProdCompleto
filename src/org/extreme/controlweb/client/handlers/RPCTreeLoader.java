package org.extreme.controlweb.client.handlers;

import org.extreme.controlweb.client.core.Proceso;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;

public class RPCTreeLoader implements AsyncCallback<Proceso>{/*

	private ControlWebGUI gui;
	private boolean doExtend;
	private boolean minInfo;

	public RPCTreeLoader(ControlWebGUI gui) {
		this.gui = gui;
		doExtend = true;
	}

	public RPCTreeLoader(ControlWebGUI gui, boolean doExtend, boolean minInfo) {
		this.gui = gui;
		this.doExtend = doExtend;
		this.minInfo = minInfo;
	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		gui.getTree().getEl().unmask();
		GWT.log("Error RPC", caught);
		gui.cancelTimeoutTimer();
	}

	public void onSuccess(Proceso proc) {
		if (gui.getTimeoutTimer(false) != null) {
			gui.setProceso(proc);
			gui.reloadTree(doExtend, minInfo);
			gui.setVisibleReloadButton(true);
			gui.highlightReloadButton();
		}
	}*/


	private ControlWebGUI gui;
	private boolean doExtend;
	private boolean minInfo;
	private Function f; 

	public RPCTreeLoader(ControlWebGUI gui) {
		this.gui = gui;
		doExtend = true;
	}

	public RPCTreeLoader(ControlWebGUI gui, boolean doExtend, boolean minInfo) {
		this.gui = gui;
		this.doExtend = doExtend;
		this.minInfo = minInfo;
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", "Error al cargar el arbol. \nMensaje: "+caught.getMessage()+" \nIntentelo " +
				"nuevamente", ClientUtils.ERROR);
		gui.getTree().getEl().unmask();
		GWT.log("Error RPC", caught);
		gui.cancelTimeoutTimer();
	}

	public void setFunction(Function f) {
		this.f = f;
	}

	@Override
	public void onSuccess(Proceso proc) {
		if (gui.getTimeoutTimer(false) != null) {
			gui.setProceso(proc);
			gui.reloadTree(doExtend, minInfo);
			/*if (gui.getWestTabPanel().getComponent("admin-tree")!=null)
				gui.reloadAdminPanel( gui.getProceso().isManejaLbs());*/
			gui.setVisibleReloadButton(true);
			gui.highlightReloadButton();
			gui.getDfDateProg().setDisabled(false);

			if(f != null) {
				f.execute();
			}
		}
	}

}
