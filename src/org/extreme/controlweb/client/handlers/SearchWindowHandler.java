package org.extreme.controlweb.client.handlers;

import org.extreme.controlweb.client.core.LocalizableObject;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.map.openlayers.OpenLayersMap;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.core.Position;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.TextField;

public class SearchWindowHandler extends ButtonListenerAdapter implements
		AsyncCallback<LocalizableObject> {

	Window wnd;
	ControlWebGUI gui;
	TextField tfDir;
	ComboBox cbMunip;
	
	public SearchWindowHandler(Window wnd, ControlWebGUI gui,
			TextField tfDir, ComboBox cbMunip) {
		this.wnd = wnd;
		this.gui = gui;
		this.tfDir = tfDir;
		this.cbMunip = cbMunip;
	}
	
	public void onClick(Button button, EventObject e) {
		wnd.hide();
		gui.showMessage("Por favor espere...", "Buscando direcci\u00f3n.", Position.CENTER);
		
		QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
				.create(QueriesService.class);
		serviceProxy.getAddressCoords(tfDir.getText(), cbMunip.getValueAsString(), this);
	}
	
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
		gui.hideMessage();
	}
	
	public void onSuccess(LocalizableObject result) {
		gui.hideMessage();
		Marker m = gui.getMap().createAuxMarker(result.getLatitudAsDouble(),
				result.getLongitudAsDouble(), OpenLayersMap.GEOFENCE, null);
		gui.getMap().getMap().setCenter(m.getLonLat());
	}
}
