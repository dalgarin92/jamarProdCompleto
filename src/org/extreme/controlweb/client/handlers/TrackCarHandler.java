package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.Municipio;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.core.Trabajo;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;
import org.gwtopenmaps.openlayers.client.LonLat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.EventObject;
import com.gwtext.client.data.Record;
import com.gwtext.client.widgets.Component;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.event.PanelListenerAdapter;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.grid.RowSelectionModel;
import com.gwtext.client.widgets.grid.event.GridRowListenerAdapter;
import com.gwtext.client.widgets.grid.event.RowSelectionListenerAdapter;
import com.gwtext.client.widgets.layout.FitLayout;

public class TrackCarHandler implements AsyncCallback<ArrayList<RowModel>> {

	private ControlWebGUI gui;
	private MyGridPanel grid;
	private String recurso;
	private String contratista;
	private String fromDate;
	private String toDate;

	public TrackCarHandler(ControlWebGUI gui, String recurso,
			String contratista, String fromDate, String toDate) {
		this.gui = gui;
		this.recurso = recurso;
		this.contratista = contratista;
		this.fromDate = fromDate;
		this.toDate = toDate;
		grid = new MyGridPanel();
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
	}

	@Override
	public void onSuccess(ArrayList<RowModel> result) {
		gui.hideMessage();
		if (result.size() > 0) {
			ArrayList<LonLat> points = new ArrayList<LonLat>();
			final ArrayList<String> infos = new ArrayList<String>();
			Panel wnd = new Panel("Reporte de Eventos "
					+ result.get(0).getRowValues()[0]);
			wnd.setClosable(true);

			wnd.addListener(new PanelListenerAdapter() {
				@Override
				public void onDestroy(Component component) {
					gui.getWestTabPanel().setActiveTab(0);
					gui.getMap().showAll();
					gui.restartTimer();
				}
			});

			wnd.setHeight(480);
			wnd.setWidth(480);

			wnd.setLayout(new FitLayout());

			if (!grid.isConfigured() && result.size() > 0) {
				grid.configure(result.get(0).getColumnNames(), null, false,
						false, true, null, ((RowModelAdapter) result.get(0))
						.getVisibility());
			}
			grid.loadRows(result);

			for (int i = 0; i < result.size(); i++) {
				Object[] rv = result.get(i).getRowValues();
				double lat;
				double lon;
				try {
					lat = Double.parseDouble((String) rv[1]);
					lon = Double.parseDouble((String) rv[2]);
				} catch (NumberFormatException e) {
					lat = 0.0;
					lon = 0.0;
				}
				points.add(new LonLat(lon, lat));
				// String placa = (String)rv[0];
				String fecha = (String) rv[3];
				String vel = (String) rv[5];
				String dir = (String) rv[6];
				String munip = (String) rv[7];
				String depto = (String) rv[8];
				String nomevento = (String) rv[4];

				infos.add(ClientUtils.INFOWINDOW_FONT + "<b>Fecha:</b> "
						+ fecha + "<br/><b>Velocidad:</b> " + vel
						+ "<br/><b>Direcci\u00f3n:</b> " + dir
						+ "<br/><b>Municipio:</b> " + munip
						+ "<br/><b>Departamento:</b> " + depto
						+ "<b><br/>Evento:</b> " + nomevento + "</font>");
			}
			gui.getMap().clearOverlays();
			final ArrayList<Object> markers = gui.getMap().createTrackMarkers(
					points, infos);
			gui.getMap().traceRoute(markers, 0);
			gui.getMap().extend(markers);

			grid.addGridRowListener(new GridRowListenerAdapter() {
				@Override
				public void onRowClick(GridPanel grid, int rowIndex,
						EventObject e) {
					gui.getMap().setPopup(markers.get(rowIndex),
							infos.get(rowIndex));
				}
			});

			grid.getSelectionModel().addListener(
					new RowSelectionListenerAdapter() {
						@Override
						public void onRowSelect(RowSelectionModel sm,
								int rowIndex, Record record) {
							gui.getMap().setPopup(markers.get(rowIndex),
									infos.get(rowIndex));
						}
					});

			wnd.add(grid);

			gui.getWestTabPanel().add(wnd);
			gui.getWestTabPanel().setActiveTab(2);
			gui.stopTimer();

			gui.getStaticQueriesServiceAsync().searchWorks(recurso,contratista,
					fromDate,toDate,new AsyncCallback<ArrayList<RowModel>>() {

				@Override
				public void onSuccess(ArrayList<RowModel> result) {
					for(RowModel tr : result)
					{
						String[] camp = (String[])tr.getRowValues();

						Municipio mun = new Municipio(camp[3],camp[11],camp[12]);
						Trabajo works = new Trabajo(camp[0],
								camp[1],camp[2],mun,camp[4],Double.parseDouble(camp[9]),
								Double.parseDouble(camp[10]),camp[8],camp[7],
								camp[5].equals("N") ? false : true,
										camp[6].equals("N") ? false : true);

						markers.add(gui.getMap().createTrabMarker(
								works.getLat(), works.getLon(), works.getMapIcon(),
								works.getHTML2(),works.getId().toString(),works.getTpoTrab().getId().toString()));
					}
					gui.getMap().extend(markers);
				}

				@Override
				public void onFailure(Throwable caught) {
					ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
					GWT.log("Error RPC", caught);
				}
			});
		} else {
			ClientUtils.alert("Info",
					"No se encontraron eventos para esta consulta.",
					ClientUtils.INFO);
		}

	}

}
