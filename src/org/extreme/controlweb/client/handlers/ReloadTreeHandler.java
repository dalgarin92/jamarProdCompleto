package org.extreme.controlweb.client.handlers;

import org.extreme.controlweb.client.core.ProcesoLight;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.services.QueriesService;
import org.extreme.controlweb.client.services.QueriesServiceAsync;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ReloadTreeHandler implements AsyncCallback<ProcesoLight> {

	ControlWebGUI gui;

	public ReloadTreeHandler(ControlWebGUI controlWebGUI) {
		gui=controlWebGUI;
	}
	/*public void reloadTree(){
		gui.executeReload("100", null);
		}*/
	
	public void reloadTree(){
		if (gui.getCheckedRecs().size() > 0) {
			gui.getTree().getEl().mask("Cargando recursos seleccionados...");
			QueriesServiceAsync serviceProxy = (QueriesServiceAsync) GWT
					.create(QueriesService.class);
			gui.getTimeoutTimer(true);
			try {
				serviceProxy.getInfoRecursosMaqTime(gui.getCheckedRecs(),
						gui.getProceso().getId(), gui.getProceso().isSoloCentral(),
						gui.getProceso().isConTurnos(),
						gui.getDfDateProg().getEl().getValue(), this);}
			catch (Exception ex )

			{ ex.getMessage();

			}
			gui.setDoReload(true);
		}
	}
	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", "Error al cargar recuros. \nMensaje: "+caught.getMessage()+
				"Intentelo nuevamente.", ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
		gui.getTree().getEl().unmask();
	}

	@Override
	public void onSuccess(ProcesoLight procl) {
		gui.getProceso().updateProceso(procl);
		gui.reloadTree(true, false);
	}

}
