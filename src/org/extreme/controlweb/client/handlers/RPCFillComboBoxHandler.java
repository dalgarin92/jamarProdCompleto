package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.DrawableSimpleEntity;
import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.SimpleEntity;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Template;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.BoxComponent;
import com.gwtext.client.widgets.Container;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.layout.LayoutData;

public class RPCFillComboBoxHandler implements
		AsyncCallback<ArrayList<SimpleEntity>> {

	private ComboBox combo;
	private BoxComponent fs;
	private int index;
	private LayoutData ld;

	final Template template = new Template("<div class=\"x-combo-list-item\">"
			+ "<img src=\"images/puntos_int/{icono}.png\"> "
			+ "{nombre}<div class=\"x-clear\"></div></div>");

	public RPCFillComboBoxHandler(ComboBox cb, BoxComponent fs, int index) {
		this.combo = cb;
		this.fs = fs;
		this.index = index;
	}
	
	public RPCFillComboBoxHandler(ComboBox cb, BoxComponent fs, LayoutData ld) {
		this.combo = cb;
		this.fs = fs;
		this.ld = ld;
	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
	}

	public void onSuccess(ArrayList<SimpleEntity> se) {
		Store store = null;
		RecordDef rd = null;		
		if (se != null) {
			if (se.size() > 0) {
				if (se.get(0) instanceof DrawableSimpleEntity)
					rd = ClientUtils.RECORD_DEF_STYLED_COMBOS;
				else
				{
					if(se.get(0) instanceof RowModel)
					{
						String[] cn=((RowModel)se.get(0)).getColumnNames();						
						int l=cn.length;
						StringFieldDef[] sfd= new StringFieldDef[l];
						for (int i = 0; i < l; i++) {
							sfd[i]=new StringFieldDef(cn[i]);
						}
						rd=new RecordDef(sfd);
					}else{
						rd = ClientUtils.RECORD_DEF_COMBOS;
					}
				}

				store = new Store(rd);
				Record r = null;
				for (SimpleEntity simpleEntity : se) {
					if (rd == ClientUtils.RECORD_DEF_COMBOS)
						r = rd.createRecord(new String[] {
									simpleEntity.getId(),
									simpleEntity.getNombre() + " ["
										+ simpleEntity.getId() + "]" });
					else {
						if (rd == ClientUtils.RECORD_DEF_STYLED_COMBOS) {
							r = rd.createRecord(new String[] {
									simpleEntity.getId(),
									simpleEntity.getNombre(),
									((DrawableSimpleEntity) simpleEntity)
											.getIcon() });
						}
						else{
							Object g[]=((RowModel)simpleEntity).getRowValues();
							String s[]=new String[g.length];
							for (int i = 0; i < g.length; i++) {
								if(g[i]!=null){
									s[i]=g[i].toString();
								}
								else
								{
									s[i]="N/A";
								}
							}
							r = rd.createRecord(s);
						}						
					}
					store.add(r);
				}
				store.commitChanges();

				
				if(!combo.isRendered()){
					combo.setMinChars(1);
					//store.setDefaultSort("nombre", SortDir.ASC);
					store.sort("nombre");
					combo.setStore(store);
					combo.setDisplayField("nombre");
					combo.setMode(ComboBox.LOCAL);
					combo.setTriggerAction(ComboBox.ALL);
					combo.setTypeAhead(true);
					combo.setValueField("id");
					combo.setForceSelection(true);
					combo.setSelectOnFocus(true);
					
				}
				

				if (rd.equals(ClientUtils.RECORD_DEF_STYLED_COMBOS))
					combo.setTpl(template);
			}
		}

		if (fs != null) {
			if (fs instanceof Toolbar)
				((Toolbar) fs).addField(combo);
			else if (ld == null)
				((Container) fs).insert(index, combo);
			else
				((Container) fs).add(combo, ld);
			
			if (se.size() == 1) {
				combo.setValue(combo.getStore().getRecordAt(0)
						.getAsString("id"));
				// fs.add(ClientUtils.getSeparator());
			}
			if (fs instanceof Toolbar) {
				
			}else{
				((Container) fs).doLayout(true);
			}			
		}

	}
	
	public ComboBox getCombo() {
		return combo;
	}

}
