package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.gui.MyGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.layout.LayoutData;

public class RPCFillTable implements AsyncCallback<ArrayList<RowModel>> {

	private MyGridPanel grid;
	private Panel panel;
	private String details;
	private String mensOnEmpty;
	private boolean modify;
	private boolean delete;
	private boolean numerate;
	private int[] columnSizes;
	private boolean[] visible;
	private LayoutData ld;
	private Function f;
	
	public RPCFillTable(MyGridPanel grid, Panel panel, String details,
			boolean modify, boolean delete, boolean numerate,
			int[] columnSizes, boolean[] visible, LayoutData ld) {		
		this.grid = grid;
		this.panel = panel;		
		this.details = details;
		this.modify = modify;
		this.delete = delete;
		this.numerate = numerate;
		this.columnSizes = columnSizes;
		this.visible = visible;
		this.ld = ld;
		if(panel.isRendered())
			panel.getEl().mask("Cargando...");
	}

	public RPCFillTable(MyGridPanel grid, Panel panel, String details,
			boolean modify, boolean delete, boolean numerate,
			int[] columnSizes, boolean[] visible, LayoutData ld, Function f) {
		this.columnSizes = columnSizes;
		this.delete = delete;
		this.details = details;
		this.f = f;
		this.grid = grid;
		this.ld = ld;
		this.modify = modify;
		this.numerate = numerate;
		this.panel = panel;
		this.visible = visible;
	}

	public RPCFillTable(MyGridPanel grid, Panel panel, String details,
			boolean modify, boolean delete, boolean numerate,
			int[] columnSizes, boolean[] visible, LayoutData ld, Function f, String mensOnEmpty) {
		this(grid, panel, details, modify, delete, numerate, columnSizes, visible, ld, f);
		this.mensOnEmpty = mensOnEmpty;
	}
	
	private void finalFuntion(){
		if(panel.isRendered()){
			panel.getEl().unmask();
		}		
	}

	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		GWT.log("Error RPC", caught);
		finalFuntion();
	}

	public void onSuccess(ArrayList<RowModel> result) {
		boolean sw = false;
		
		if(grid.isRendered())
			grid.getEl().unmask();
		
		if(result!=null)
		{
			if(result.size()>0) {
				if(!grid.isConfigured()&&result.size()>0) {
					grid.configure(result.get(0).getColumnNames(), details, modify, delete,
							numerate, columnSizes != null ? columnSizes: 
								(result.get(0) instanceof RowModelAdapter ? 
										((RowModelAdapter)result.get(0)).getSizes() : columnSizes), 
										visible != null ? visible: 
								(result.get(0) instanceof RowModelAdapter ? 
										((RowModelAdapter)result.get(0)).getVisibility() : visible));
					sw = true;
				}
			}else
			{
				if(mensOnEmpty != null)
					ClientUtils.alert("Mensaje", mensOnEmpty, ClientUtils.WARNING);
			}
			grid.loadRows(result);
			if(sw){
				if(ld == null)
					panel.add(grid);
				else
					panel.add(grid, ld);		
			}			
		}
		if((sw || result.size() == 0) && f != null){
			f.execute();			
		}
		finalFuntion();
		if(sw)
			panel.doLayout();
	}

}