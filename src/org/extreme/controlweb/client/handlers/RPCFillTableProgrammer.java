package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.RowModel;
import org.extreme.controlweb.client.core.RowModelAdapter;
import org.extreme.controlweb.client.programmer.MyGridProgrammerPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.GridPanel;
import com.gwtext.client.widgets.layout.LayoutData;

public class RPCFillTableProgrammer implements AsyncCallback<ArrayList<RowModel>> {

	private GridPanel grid;
	private Panel panel;
	private String details;
	private boolean modify;
	private boolean delete;
	private boolean numerate;
	private int[] columnSizes;
	private boolean[] visible;
	private LayoutData ld;
	private Function f;
	private String reorderFieldName;
	private CheckboxSelectionModel csm;
	private String mensOnEmpty = null;
	private boolean addBottomToolbar = false;
	private String label = null;
	private boolean setOrder=true;

	public RPCFillTableProgrammer(GridPanel grid, Panel panel, String details,
			boolean modify, boolean delete, boolean numerate,
			int[] columnSizes, boolean[] visible, LayoutData ld, 
			String reorderFieldName, boolean checkers,boolean addBottomToolbar,String label, Function f) {		
		this.grid = grid;
		this.panel = panel;		
		this.details = details;
		this.addBottomToolbar = addBottomToolbar;
		this.modify = modify;
		this.delete = delete;
		this.numerate = numerate;
		this.columnSizes = columnSizes;
		this.visible = visible;
		this.label = label;
		this.ld = ld;
		this.reorderFieldName = reorderFieldName;
		this.f=f;
		if(panel.isRendered()) {
			panel.getEl().mask("Cargando...");
		}
		if (checkers) {
			csm = new CheckboxSelectionModel();
		}
	}

	public RPCFillTableProgrammer(GridPanel grid, Panel panel, String details,
			boolean modify, boolean delete, boolean numerate,
			int[] columnSizes, boolean[] visible, LayoutData ld, 
			String reorderFieldName, boolean checkers,boolean addBottomToolbar,String label,boolean setOrder) {		
		this.grid = grid;
		this.panel = panel;		
		this.details = details;
		this.addBottomToolbar = addBottomToolbar;
		this.modify = modify;
		this.delete = delete;
		this.numerate = numerate;
		this.columnSizes = columnSizes;
		this.visible = visible;
		this.label = label;
		this.ld = ld;
		this.reorderFieldName = reorderFieldName;
		this.setOrder = setOrder;
		if(panel.isRendered()) {
			panel.getEl().mask("Cargando...");
		}
		if (checkers) {
			csm = new CheckboxSelectionModel();
		}
	}



	private void finalFuntion(){
		if(panel.isRendered()){
			if(panel.getEl()!=null) {
				panel.getEl().unmask();
			}
		}		
	}

	@Override
	public void onFailure(Throwable caught) {
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
		finalFuntion();
		grid.getStore().removeAll();
	}

	@Override
	public void onSuccess(ArrayList<RowModel> result) {
		boolean sw = false;

		if(grid.isRendered()) {
			grid.getEl().unmask();
		}
		if (grid.getStore() != null) {
			grid.getStore().removeAll();
		}
		try {
			
			if(result!=null){
				((MyGridProgrammerPanel)grid).setResult(result);
				if(result.size()>=0) {
					if(!((MyGridProgrammerPanel)grid).isConfigured()) {
						((MyGridProgrammerPanel)grid).configure(result.get(0).getColumnNames(), details, modify, delete,
								numerate, columnSizes != null ? columnSizes : 
									result.get(0) instanceof RowModelAdapter ?((RowModelAdapter)result.get(0)).getSizes() : columnSizes, 
											visible != null ? visible : 
												result.get(0) instanceof RowModelAdapter ? ((RowModelAdapter)result.get(0)).getVisibility() : visible, reorderFieldName, csm,"Hora");
						if(addBottomToolbar){
							((MyGridProgrammerPanel)grid).addBottomToolbar(label);
						}
					}
					
					sw = true;
				}else
				{
					if(mensOnEmpty != null) {
						ClientUtils.alert("Mensaje", mensOnEmpty, ClientUtils.WARNING);
					}
				}
				if(result.get(0).getRowValues()!=null&&result.get(0).getRowValues().length>0)
					((MyGridProgrammerPanel)grid).loadRows(result,setOrder);
				
				if(sw){
					if(ld == null) {
						panel.add(grid);
					} else {
						panel.add(grid, ld);
					}		
				}			
			}else{
				GWT.log("no hay datos");
			}
		} catch (Exception e) {
			GWT.log(e.getMessage());
		}

		if((sw || result.size() == 0) && f != null){
			f.execute();			
		}

		finalFuntion();
		if(sw) {
			try {
				
				panel.doLayout();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

}
