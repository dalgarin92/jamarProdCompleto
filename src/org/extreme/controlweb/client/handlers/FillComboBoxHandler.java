package org.extreme.controlweb.client.handlers;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.listeners.LoginKeyListener;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;
import com.gwtext.client.data.Store;
import com.gwtext.client.widgets.form.ComboBox;
import com.gwtext.client.widgets.form.FieldSet;
import com.gwtext.client.widgets.form.TextField;
import com.gwtext.client.widgets.layout.AnchorLayoutData;

@Deprecated
public class FillComboBoxHandler implements RequestCallback {

	private Store store;
	private ControlWebGUI gui;
	private FieldSet set;
	
	public FillComboBoxHandler(Store store, FieldSet set, ControlWebGUI gui) {
		this.store = store; 
		this.gui = gui;
		this.set = set;
	}

	public void onError(Request request, Throwable exception) {
		GWT.log("Error buscando empresas", null);
		ClientUtils.alert("Error", "Hubo un error al consultar las informaci\u00f3n de inicio.", ClientUtils.ERROR);

	}

	public void onResponseReceived(Request request, Response response) {
		
		GWT.log(response.getText(), null);
		if (response.getStatusCode() != 200) {
			GWT.log("Codigo de Respuesta del Servidor: "
					+ response.getStatusCode(), new Exception());
			ClientUtils.alert("Error", "Hubo un error al consultar las informaci\u00f3n de inicio.",
							ClientUtils.ERROR);
		} else {
			GWT.log(response.getText(), null);
			Document doc = XMLParser.parse(response.getText());
			NodeList nodes = doc.getElementsByTagName("empresa");
			ComboBox cbEmp = new ComboBox("Empresa");
			int empCount = 0;
			for (int i = 0; i < nodes.getLength(); i++) {
				Node item = nodes.item(i);
				if (item.getNodeType() == Node.ELEMENT_NODE) {
					empCount++;
					String id = item.getAttributes().getNamedItem("id")
							.getNodeValue();
					String nombre = item.getAttributes().getNamedItem(
							"nombre").getNodeValue();
					store.add(ClientUtils.RECORD_DEF_COMBOS
							.createRecord(new String[] { id, nombre }));
				}
			}
			store.commitChanges();
			cbEmp.setStore(store);
			//cbEmp.getStore().load();
			cbEmp.setForceSelection(true);  
			cbEmp.setMinChars(1);
			cbEmp.setTypeAhead(true);  
			cbEmp.setSelectOnFocus(true);
			cbEmp.setMode(ComboBox.LOCAL);  
			cbEmp.setTriggerAction(ComboBox.ALL);
			cbEmp.setEmptyText("Selecccione una empresa...");
			cbEmp.setValueField("id");
			cbEmp.setDisplayField("nombre");
			
			TextField tfPasswd = new TextField("Contrase\u00f1a", "passwd");
			tfPasswd.setInputType("password");
					 
			//keyCode 13 = ENTER
			tfPasswd.addKeyListener(13, new LoginKeyListener(gui));
			
			set.add(tfPasswd, new AnchorLayoutData("100%"));
			gui.setTxtPasswd(tfPasswd);
			
			if(empCount != 1){
				set.add(cbEmp, new AnchorLayoutData("100%"));
			} else {
				cbEmp.setValue(cbEmp.getStore().getRecordAt(0).getAsString("id"));
				set.add(ClientUtils.getSeparator());
			}

			set.doLayout();
			//gui.setComboEmp(cbEmp);
		}
	}

}
