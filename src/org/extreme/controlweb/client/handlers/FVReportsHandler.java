/*package org.extreme.controlweb.client.handlers;

import java.util.Date;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.Window.Navigator;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.FitLayout;

public class FVReportsHandler implements AsyncCallback<Boolean>{

	private final ControlWebGUI gui;
	private Window wnd;
	private String tipo;
	public FVReportsHandler(ControlWebGUI gui, Window wnd) {
		this.gui = gui;
		this.setWnd(wnd);
		this.tipo="pdf";
	}
	public FVReportsHandler(ControlWebGUI gui,Window wnd, String tipo) {
		this.gui = gui;
		this.tipo = tipo;
		this.setWnd(wnd);
	}

	@Override
	public void onFailure(Throwable caught) {
		gui.hideMessage();
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
	}

	@Override
	public void onSuccess(Boolean result) {
		gui.hideMessage();
		Panel p2 = new Panel("Reporte");
		p2.setClosable(true);
		p2.setPaddings(5);
		p2.setIconCls("icon-pdf");
		p2.setLayout(new FitLayout());

		if(tipo.equalsIgnoreCase("xls")){

			String url = ClientUtils.SESSION_CHART_URL + "?tipo="+tipo+"&time="
					+ new Date().getTime();
			final Frame f = new Frame(url);
			p2.add(f);
			Window w = new Window("Exportando...");
			w.setSize(200, 200);
			w.setLayout(new FitLayout());
			w.add(p2);
			w.show();

			if (Navigator.getAppName().contains("Explorer")) {
				w.setVisible(true);
				w.close();
			} else {
				w.setVisible(false);
			}

			if(w != null) {
				w.close();
			}
		}else{

			String url = ClientUtils.SESSION_CHART_URL + "?tipo=pdf&time="
					+ new Date().getTime();
			final Frame f = new Frame(url);
			p2.add(f);
			//p2.doLayout();
			gui.addInfoTab(p2, new Date().getTime());
			//wnd.close();
		}

		if(wnd != null) {
			//p2.hide();
			wnd.close();
		}

	}


	public Window getWnd() {
		return wnd;
	}

	public void setWnd(Window wnd) {
		this.wnd = wnd;
	}





}*/

package org.extreme.controlweb.client.handlers;

import java.util.Date;

import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.Window.Navigator;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.gwtext.client.widgets.Panel;
import com.gwtext.client.widgets.Window;
import com.gwtext.client.widgets.layout.FitLayout;

public class FVReportsHandler implements AsyncCallback<Boolean>{

	private ControlWebGUI gui;
	private Window wnd;
	private String tipo;

	public FVReportsHandler(ControlWebGUI gui, Window wnd) {
		this.gui = gui;
		this.wnd = wnd;
		tipo=null;
	}

	public FVReportsHandler(ControlWebGUI gui, Window wnd, String tipo) {
		this.gui = gui;
		this.wnd = wnd;
		this.tipo = tipo;
	}
	@Override
	public void onFailure(Throwable caught) {
		gui.hideMessage();
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
	}

	@Override
	public void onSuccess(Boolean result) {
		gui.hideMessage();
		Panel p2 = new Panel("Reporte");
		p2.setClosable(true);
		p2.setPaddings(5);
		p2.setIconCls("icon-pdf");
		p2.setLayout(new FitLayout());
		if (tipo==null)
			tipo="pdf";
			
		String url = ClientUtils.SESSION_CHART_URL + "?tipo="+((tipo!=null)?tipo:"pdf")+"&time="+ new Date().getTime();
		final Frame f = new Frame(url);	
		p2.add(f);
	   if(tipo.equalsIgnoreCase("pdf")){		 	
			gui.addInfoTab(p2, new Date().getTime());
			wnd.close();	
	   }else{
	
		   
		   Window wnd2 = new Window("Exportando...");
		   wnd2.setSize(200,200);
		   wnd2.setLayout(new FitLayout());
		   wnd2.add(p2);
		   wnd2.show();
		   
		   if(Navigator.getAppName().contains("Explorer")){
			   wnd2.setVisible(true);
			   wnd2.close();
		   }else{
			   wnd2.setVisible(false);
		   }
		   if(wnd!=null)
			   wnd.close();
		   wnd2.close(); 
	   }
		
		
	}

	

}
