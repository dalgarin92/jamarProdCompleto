package org.extreme.controlweb.client.handlers;

import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.core.Function;

public class MyBooleanAsyncCallback implements AsyncCallback<Boolean> {

	private Function onSuccessFunction;
	private String messageOnSuccess;
	private String messageOnFailure;
	private Function onFailureFunction;
			
	public MyBooleanAsyncCallback(String messageOnFailure,
			String messageOnSuccess, Function onSuccessFunction) {
		this.messageOnFailure = messageOnFailure;
		this.messageOnSuccess = messageOnSuccess;
		this.onSuccessFunction = onSuccessFunction;
	}
	
	
	
	public MyBooleanAsyncCallback(String messageOnFailure,
			String messageOnSuccess, Function onFailureFunction,
			Function onSuccessFunction) {
		this.messageOnFailure = messageOnFailure;
		this.messageOnSuccess = messageOnSuccess;
		this.onFailureFunction = onFailureFunction;
		this.onSuccessFunction = onSuccessFunction;
	}



	public MyBooleanAsyncCallback(String messageOnSuccess){
		this.messageOnSuccess = messageOnSuccess;
	}
	
	public MyBooleanAsyncCallback(String messageOnSuccess, Function onSuccessFunction){
		this.messageOnSuccess = messageOnSuccess;
		this.onSuccessFunction = onSuccessFunction;
	}
	
	public MyBooleanAsyncCallback(Function onSuccessFunction){
		this.onSuccessFunction = onSuccessFunction;
	}

	public void onFailure(Throwable caught) {
		String m = caught.getMessage();
		if (messageOnFailure != null){
			m = messageOnFailure;
		}
		if(onFailureFunction!=null){
			onFailureFunction.execute();
		}
		ClientUtils.alert("Error", m, ClientUtils.ERROR);
	}

	public void onSuccess(Boolean result) {
		if(result){
			if (messageOnSuccess != null){
				ClientUtils.alert("Info", messageOnSuccess, ClientUtils.INFO);
			}
			if(onSuccessFunction!=null){
				onSuccessFunction.execute();				

			}	
		}else{
			if (messageOnFailure != null){
				ClientUtils.alert("Info", messageOnFailure, ClientUtils.ERROR);
			}
			if(onFailureFunction!=null){
				onFailureFunction.execute();				
			}	
		}
						
	}
	
	public void setOnFailureFunction(Function onFailureFunction) {
		this.onFailureFunction = onFailureFunction;
	}
	
	public void setOnSuccessFunction(Function onSuccessFunction) {
		this.onSuccessFunction = onSuccessFunction;
	}
}
