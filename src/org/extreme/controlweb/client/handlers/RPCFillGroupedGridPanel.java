package org.extreme.controlweb.client.handlers;

import java.util.ArrayList;

import org.extreme.controlweb.client.core.GroupedRowModelAdapter;
import org.extreme.controlweb.client.gui.ControlWebGUI;
import org.extreme.controlweb.client.gui.GroupedGridPanel;
import org.extreme.controlweb.client.util.ClientUtils;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gwtext.client.widgets.Window;

public class RPCFillGroupedGridPanel implements
		AsyncCallback<ArrayList<GroupedRowModelAdapter>> {

	private ControlWebGUI gui;
	private GroupedGridPanel grid;
	private Window wnd;
	private boolean doShow;

	public RPCFillGroupedGridPanel(GroupedGridPanel grid, ControlWebGUI gui,
			Window wnd, boolean doShow) {
		this.doShow = doShow;
		this.grid = grid;
		this.gui = gui;
		this.wnd = wnd;
	}

	public RPCFillGroupedGridPanel(GroupedGridPanel grid) {
		this.grid = grid;
	}

	public void onFailure(Throwable caught) {
		if(gui != null)
			gui.hideMessage();
		ClientUtils.alert("Error", caught.getMessage(), ClientUtils.ERROR);
	};

	public void onSuccess(ArrayList<GroupedRowModelAdapter> result) {
		if(gui != null)
			gui.hideMessage();
		
		grid.addProperties(result);
		if(wnd != null && doShow)
			wnd.show();
	};
}
