package org.extreme.controlweb.client.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.extreme.controlweb.client.core.RowModel;

import com.gwtext.client.core.EventObject;
import com.gwtext.client.data.BooleanFieldDef;
import com.gwtext.client.data.FieldDef;
import com.gwtext.client.data.Record;
import com.gwtext.client.data.RecordDef;
import com.gwtext.client.data.Store;
import com.gwtext.client.data.StringFieldDef;
import com.gwtext.client.widgets.Button;
import com.gwtext.client.widgets.Toolbar;
import com.gwtext.client.widgets.ToolbarButton;
import com.gwtext.client.widgets.event.ButtonListenerAdapter;
import com.gwtext.client.widgets.grid.BaseColumnConfig;
import com.gwtext.client.widgets.grid.CheckboxColumnConfig;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.CheckboxColumnConfig;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.CheckboxColumnConfig;
import com.gwtext.client.widgets.grid.CheckboxSelectionModel;
import com.gwtext.client.widgets.grid.ColumnConfig;
import com.gwtext.client.widgets.grid.ColumnModel;
import com.gwtext.client.widgets.grid.EditorGridPanel;

public class CheckBoxGridPanelPlaning extends EditorGridPanel {

	protected ArrayList<Integer> colsExclusionList;
	protected RecordDef recordDef;
	protected boolean configured;
	protected boolean tools;
	private CheckboxSelectionModel cbSelectionModel;

	
	public CheckBoxGridPanelPlaning(boolean tools){
		super();
		colsExclusionList = new ArrayList<Integer>();
		this.tools = tools;
	}
	
	public CheckBoxGridPanelPlaning() {
		super();
		colsExclusionList = new ArrayList<Integer>();
		this.tools = false;
	}
	

	
	public void addExcludedCol(int colIndex){
		colsExclusionList.add(colIndex);
	}
	
	public void configure(String[] columnNames, int[] columnSizes,
			boolean[] visible) {

		cbSelectionModel = new CheckboxSelectionModel();
		BaseColumnConfig ccChecked = new CheckboxColumnConfig(cbSelectionModel);

		ArrayList<FieldDef> fd = new ArrayList<FieldDef>();
		fd.add(new BooleanFieldDef("checked"));
		
		ArrayList<BaseColumnConfig> bcc = new ArrayList<BaseColumnConfig>();
		bcc.add(ccChecked);
		
		for (int j = 0; j < columnNames.length; j++) {
			fd.add(new StringFieldDef(columnNames[j]));
			if (visible != null) {
				if (visible[j] && !colsExclusionList.contains(j)) {
					int size = 200;
					if (columnSizes != null) {
						size = columnSizes[j];
					}
					bcc.add(new ColumnConfig(columnNames[j], columnNames[j],
							size, true));
				}
			} else if (!colsExclusionList.contains(j)) {
				bcc.add(new ColumnConfig(columnNames[j], columnNames[j], 200,
						true));
			}
		}

		ColumnModel cm = new ColumnModel(toBaseColumnConfigArray(bcc));

		cm.setDefaultSortable(true);
		recordDef = new RecordDef(toFieldDefArray(fd));
		Store s = new Store(recordDef);
		
		this.setStore(s);
		this.setColumnModel(cm);
		this.setEnableHdMenu(false);
		this.setSelectionModel(cbSelectionModel);  
		this.setStore(new Store(recordDef));
		this.setColumnModel(new ColumnModel(toBaseColumnConfigArray(bcc)));
		this.setEnableHdMenu(false);
		this.setHeight(150);
		this.setWidth(100);
		
		if(tools){
			Toolbar tools = new Toolbar();
			ToolbarButton expandButton = new ToolbarButton();
			expandButton.setCls("x-btn-icon expand-all-btn");
			expandButton.setTooltip("Seleccionar Todas");
			expandButton.addListener(new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
						CheckBoxGridPanelPlaning.this.checkAll();
				}
			});
			tools.addButton(expandButton);
			ToolbarButton collapseButton = new ToolbarButton();
			collapseButton.setCls("x-btn-icon collapse-all-btn");
			collapseButton.setTooltip("Quitar Selecci\u00f3n");
			collapseButton.addListener(new ButtonListenerAdapter() {
				public void onClick(Button button, EventObject e) {
					CheckBoxGridPanelPlaning.this.uncheckAll();
				}
			});
			tools.addButton(collapseButton);
			this.setTopToolbar(tools);
		}
		
		configured = true;
	}
	

	public Record[] getSelectedRows() {
		Record[] res = cbSelectionModel.getSelections();
		return res;
	}
	
	public String[] getSelectedIDs(){
		Record[] sel = getSelectedRows();
		String[] res = new String[sel.length];
		for (int i = 0; i < res.length; i++) 
			res[i] = sel[i].getAsString("id");
		return res;
	}
	
	public String[] getSelectedField(String Field){
		Record[] sel = getSelectedRows();
		String[] res = new String[sel.length];
		for (int i = 0; i < res.length; i++) 
			res[i] = sel[i].getAsString(Field);
		return res;
	}
	
	public String[][] getSelectedIDs(String[] cols){
		Record[] sel = getSelectedRows();
		String[][] res = new String[sel.length][cols.length];
		for (int i = 0; i < res.length; i++) 
			for (int j = 0; j < cols.length; j++) 
				res[i][j] = sel[i].getAsString(cols[j]);
		return res;
	}
	
	public RecordDef getRecordDef(){
		return recordDef;
	}

	public void checkAll() {
		cbSelectionModel.selectAll();

	}

	public void uncheckAll() {
		cbSelectionModel.clearSelections();
	}

	public void addAll(ArrayList<RowModel> result) {
		getStore().removeAll();
		getStore().commitChanges();
		for (RowModel rm : result) {
			Object[] rv = rm.getRowValues();
			Object[] row = new Object[rv.length + 1];
			row[0] = false;
			for (int i = 1; i < row.length; i++) 
				row[i] = rv[i - 1]; 
			this.getStore().add(getRecordDef().createRecord(row));
		}
		getStore().commitChanges();
	}
	
	protected BaseColumnConfig[] toBaseColumnConfigArray(
			ArrayList<BaseColumnConfig> bcc) {
		BaseColumnConfig[] a = new BaseColumnConfig[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			a[i] = bcc.get(i);
		}
		return a;
	}

	protected FieldDef[] toFieldDefArray(ArrayList<FieldDef> bcc) {
		FieldDef[] a = new FieldDef[bcc.size()];
		for (int i = 0; i < bcc.size(); i++) {
			a[i] = bcc.get(i);
		}
		return a;
	}

	public boolean isConfigured() {
		return configured;
	}

	public String  getSelectedIDString(){
		return getSelectedIDString("");
	}
	
	public String  getSelectedIDString(String separator){
		Record[] sel = getSelectedRows();
		String[] res = new String[sel.length];
		String dias="";
		for (int i = 0; i < res.length; i++) 
			dias+= sel[i].getAsString("id") + separator;
		return dias.substring(0, dias.length() - separator.length());
	}
	
	public void selectIDString(String ids, String separator) {
		uncheckAll();
		List<String> split = Arrays.asList(ids.split(separator));
		Record[] recs = new Record[split.size()];
		for (int i = 0; i < split.size(); i++) {
			recs[i] = getStore().query("id", split.get(i))[0];
		}
		cbSelectionModel.selectRecords(recs);
	}
	
	public void checkSome(String some) {
		Record[] all = this.getStore().getRecords();
		for (int j = 0; j < some.length(); j++)
		{
			String dia=	String.valueOf(some.charAt(j));
			for (int i = 0; i < all.length; i++) 
			{
			    if (all[i].getAsString("id").equals(dia))
			    	//all[i].set("checked", true);
			    	cbSelectionModel.selectRecords(all[i]);
			}
		}
		this.getStore().commitChanges();
	}


}
